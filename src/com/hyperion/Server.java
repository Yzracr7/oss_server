package com.hyperion;

import com.hyperion.cache.Cache;
import com.hyperion.game.Constants;
import com.hyperion.game.RS2Server;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.ls.LoginServerCore;
import com.hyperion.utility.logging.LoggerSetup;

/**
 * A class to speak both the file and game servers.
 *
 * @author Graham Edgecombe
 */
public class Server {

    /**
     * The protocol version.
     */
    public static final int VERSION = 464;

    /**
     * The world id.
     */
    public static int world = 1;

    /**
     * The instance of the login server
     */
    public static LoginServerCore loginServer;

    /**
     * The entry point of the application.
     *
     * @param args The command line arguments.
     */
    public static void main(String[] args) {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                System.out.println("Saving players..");
                for (Player p : World.getWorld().getPlayers()) {
                    if (p != null)
                        World.getWorld().getWorldLoader().savePlayer(p);
                }
                System.out.println("Players saved.");
            }
        });
        LoggerSetup.registerServerLoggers();
        Logger.getInstance().warning("Starting " + Constants.SERVER_NAME + ".");
        World.getWorld();
        try {
            Cache.init();
            if (Constants.ENABLE_LOGIN_SERVER) {
                loginServer = new LoginServerCore(world);
            }
            new RS2Server().bind(RS2Server.PORT).start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
