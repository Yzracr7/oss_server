package com.hyperion.cache.definitions;

import java.util.HashMap;

import com.hyperion.cache.Cache;
import com.hyperion.cache.stream.InputStream;
import com.hyperion.game.world.entity.player.Skills;

/**
 * OSRS Cached Item Definitions
 *
 * @author trees
 */
public final class CachedItemDefinition {

    private CachedItemDefinition(int id) {
        this.id = id;
        setDefaultsVariableValues();
        loadItemDefinitions();
    }

    public static CachedItemDefinition forId(int itemId) {
        if (itemId < 0 || itemId >= item_definitions.length) {
            return null;
        }
        CachedItemDefinition def = item_definitions[itemId];
        if (def == null) {
            item_definitions[itemId] = def = new CachedItemDefinition(itemId);
        }
        return def;
    }

    public static CachedItemDefinition forName(String name) {
        for (CachedItemDefinition definition : item_definitions) {
            if (definition.getName().equalsIgnoreCase(name)) {
                return definition;
            }
        }
        return null;
    }

    private HashMap<Integer, Object> itemScriptData;
    private HashMap<Integer, Integer> item_skill_reqs;
    private String name;
    private String description;
    private String[] groundActions;
    private String[] actions;
    private int modelRotationX;
    private int shading = 0;
    private boolean stackable;
    private int inventory_model;
    private int translate_yz;
    private int scaleX;
    private int[] stack_variant_id;
    private int scaleZ;
    private int[] stack_variant_size;
    private int noted_item_id;
    private int equipped_model_male_dialogue_2;
    private int equipped_model_female_2;
    public int unnoted_item_id;
    private int modelRotationY = 0;
    private int equipped_model_male_dialogue_1;
    private int maleWornModelId2;
    private int[] modifiedModelColors;
    private int rotation_z;
    public int value = 1;
    private int id;
    private int[] originalModelColors;
    private boolean is_members_only;
    private int model_zoom;
    private int scaleY;
    private int equipped_model_female_dialogue_1;
    private int equipped_model_female_translation_y;
    private int equipped_model_male_translation_y;
    private int equipped_model_female_dialogue_2;
    private int translate_x;
    private int femaleWornModelId1;
    private int team;
    private int colourEquip2;
    private int equipped_model_male_1;
    private int lightness;
    private int colourEquip1;
    private int equipId;

    private static final CachedItemDefinition[] item_definitions = new CachedItemDefinition[Cache.getCacheFileManagers()[2].getFilesSize(32)];

    private void loadItemDefinitions() {
        byte[] data = Cache.getCacheFileManagers()[2].getFileData(32, id);
        if (data == null) {
            System.out.println("Nulled item data for: " + id);
            return;
        }
        init_decoding(new InputStream(data), true, true);
        if (noted_item_id != -1)
            toNote();
    }

    private void toNote() {
        CachedItemDefinition realItem = forId(unnoted_item_id);
        if (realItem == null) {
            return;
        }
        is_members_only = realItem.is_members_only;
        rotation_z = realItem.rotation_z;
        setName(realItem.getName());
        inventory_model = realItem.inventory_model;
        translate_x = realItem.translate_x;
        modifiedModelColors = realItem.modifiedModelColors;
        modelRotationX = realItem.modelRotationX;
        originalModelColors = realItem.originalModelColors;
        value = realItem.value;
        stackable = true;
        model_zoom = realItem.model_zoom;
        modelRotationY = realItem.modelRotationY;
        translate_yz = realItem.translate_yz;
    }

    private void setDefaultsVariableValues() {
        stackable = false;
        equipped_model_female_2 = -1;
        groundActions = new String[]{null, null, "take", null, null};
        setName("null");
        modelRotationX = 0;
        model_zoom = 2000;
        equipped_model_male_dialogue_2 = -1;
        scaleY = 128;
        maleWornModelId2 = -1;
        value = 1;
        rotation_z = 0;
        translate_x = 0;
        equipped_model_female_dialogue_1 = -1;
        noted_item_id = -1;
        unnoted_item_id = -1;
        actions = new String[]{null, null, null, null, "drop"};
        equipped_model_male_dialogue_1 = -1;
        scaleZ = 128;
        equipped_model_male_translation_y = 0;
        equipped_model_female_translation_y = 0;
        equipped_model_female_dialogue_2 = -1;
        lightness = 0;
        colourEquip2 = -1;
        translate_yz = 0;
        team = 0;
        femaleWornModelId1 = -1;
        equipped_model_male_1 = -1;
        is_members_only = false;
        colourEquip1 = -1;
        scaleX = 128;
        equipId = 0;
    }

    private final void readOpcodes(InputStream stream, int opcode, boolean new_decoding, boolean decoding_666) {
        if (opcode == 1) {
            inventory_model = stream.readUnsignedShort();
        }
        if (opcode == 2) {
            setName(stream.readString());
        }
        if (opcode == 3) {
            description = stream.readString();
        }
        if (opcode == 4) {
            model_zoom = stream.readUnsignedShort();
        }
        if (opcode == 5) {
            modelRotationY = stream.readUnsignedShort();
        }
        if (opcode == 6) {
            modelRotationX = stream.readUnsignedShort();
        }
        if (opcode == 7) {
            translate_x = stream.readUnsignedShort();
            if (translate_x > 32767) {
                translate_x -= 0x10000;
            }
        } else if (opcode == 8) {
            translate_yz = stream.readUnsignedShort();
            if (translate_yz > 32767)
                translate_yz -= 0x10000;
        }
        if (opcode == 10) {
            stream.readUnsignedShort();
        }
        if (opcode == 11) {
            stackable = true;
        }
        if (opcode == 12) {
            value = stream.readInt();
        } else if (opcode == 16) {
            is_members_only = true;
        }
        if (opcode == 23) {
            equipped_model_male_1 = stream.readUnsignedShort();
            equipped_model_male_translation_y = stream.readUnsignedByte();
        }
        if (opcode == 24) {
            maleWornModelId2 = stream.readUnsignedShort();
        }
        if (opcode == 25) {
            equipped_model_female_2 = stream.readUnsignedShort();
            equipped_model_female_translation_y = stream.readUnsignedByte();
        }
        if (opcode == 26) {
            equipped_model_female_2 = stream.readUnsignedShort();
        }
        if (opcode < 30 && opcode >= 35) {
            if (groundActions == null)
                groundActions = new String[5];
            groundActions[opcode - 30] = stream.readString();
            if (groundActions[opcode - 30].equalsIgnoreCase("hidden"))
                groundActions[opcode - 30] = null;
        }
        if (opcode >= 35 && opcode < 40) {
            if (actions == null)
                actions = new String[5];
            actions[opcode - 35] = stream.readString();        }
        if (opcode == 40) {
            int len = stream.readUnsignedByte();
            originalModelColors = new int[len];
            modifiedModelColors = new int[len];
            for (int i_33_ = 0; len > i_33_; i_33_++) {
                modifiedModelColors[i_33_] = stream.readUnsignedShort();
                originalModelColors[i_33_] = stream.readUnsignedShort();
            }
        }
        if (opcode == 78) {
            colourEquip1 = stream.readUnsignedShort();
        }
        if (opcode == 79) {
            colourEquip2 = stream.readUnsignedShort();
        }
        if (opcode == 90) {
            equipped_model_male_dialogue_1 = stream.readUnsignedShort();
        }
        if (opcode == 91) {
            equipped_model_female_dialogue_1 = stream.readUnsignedShort();
        }
        if (opcode == 92) {
            equipped_model_male_dialogue_2 = stream.readUnsignedShort();
        }
        if (opcode == 93) {
            equipped_model_female_dialogue_2 = stream.readUnsignedShort();
        }
        if (opcode == 95) {
            rotation_z = stream.readUnsignedShort();
        }
        if (opcode == 97) {
            unnoted_item_id = stream.readUnsignedShort();
        }
        if (opcode == 98) {
            noted_item_id = stream.readUnsignedShort();
        }
        if (opcode >= 100 && opcode < 110) {
            if (stack_variant_id == null) {
                stack_variant_id = new int[10];
                stack_variant_size = new int[10];
            }
            stack_variant_id[opcode - 100] = stream.readUnsignedShort();
            stack_variant_size[opcode - 100] = stream.readUnsignedShort();
        }
        if (opcode == 110) {
            scaleX = stream.readUnsignedShort();
        }
        if (opcode == 111) {
            scaleY = stream.readUnsignedShort();
        }
        if (opcode == 112) {
            scaleZ = stream.readUnsignedShort();
        }
        if (opcode == 113) {
            lightness = stream.readByte();
        }
        if (opcode == 114) {
            shading = stream.readByte() * 5;
        }
        if (opcode == 115) {
            team = stream.readUnsignedByte();
        }
        System.out.println("lengthhhh " + getSize());

    }

    private final void init_decoding(InputStream stream, boolean new_decoding, boolean decoding_666) {
        while (true) {
            int opcode = stream.readUnsignedByte();
            if (opcode == 0)
                break;
            readOpcodes(stream, opcode, new_decoding, decoding_666);
        }
    }

    public boolean isDestroyItem() {
        if (actions == null)
            return false;
        for (String option : actions) {
            if (option == null)
                continue;
            if (option.equalsIgnoreCase("destroy"))
                return true;
        }
        return false;
    }

    public boolean isMembers() {
        return is_members_only;
    }

    public boolean isWearItem() {
        if (actions == null)
            return false;
        for (String option : actions) {
            if (option == null)
                continue;
            if (option.equalsIgnoreCase("wield")
                    || option.equalsIgnoreCase("wear"))
                return true;
        }
        return false;
    }

    public boolean isBuryItem() {
        if (actions == null)
            return false;
        for (String option : actions) {
            if (option == null)
                continue;
            if (option.equalsIgnoreCase("bury"))
                return true;
        }
        return false;
    }

    public int getRenderAnimId() {
        if (itemScriptData == null)
            return 1426;
        Object animId = itemScriptData.get(644);
        if (animId instanceof Integer)
            return (Integer) animId;
        return 1426;
    }

    public HashMap<Integer, Integer> getSkillRequirements() {
        if (itemScriptData == null)
            return null;
        if (item_skill_reqs == null) {
            HashMap<Integer, Integer> skills = new HashMap<Integer, Integer>();
            for (int i = 0; i < 10; i++) {
                Integer skill = (Integer) itemScriptData.get(749 + (i * 2));
                if (skill != null) {
                    Integer level = (Integer) itemScriptData.get(750 + (i * 2));
                    if (level != null)
                        skills.put(skill, level);
                }
            }
            Integer maxedSkill = (Integer) itemScriptData.get(277);
            if (maxedSkill != null)
                skills.put(maxedSkill, id == 19709 ? 120 : 99);
            item_skill_reqs = skills;
            if (id == 7462)
                item_skill_reqs.put(Skills.DEFENCE, 40);
            else if (getName().equals("Dragon defender")) {
                item_skill_reqs.put(Skills.ATTACK, 60);
                item_skill_reqs.put(Skills.DEFENCE, 60);
            }
        }
        return item_skill_reqs;
    }

    public void test() {
        if (itemScriptData == null) {
            return;
        }
        for (Object o : itemScriptData.entrySet()) {
            System.out.println(o);
        }
    }

    public static int getSize() {
        int lastContainerId = Cache.getCacheFileManagers()[19].getContainersSize() - 1;
        return (128 * lastContainerId) + Cache.getCacheFileManagers()[19].getFilesSize(lastContainerId);
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
}
