package com.hyperion.cache.definitions;

import com.hyperion.cache.Cache;
import com.hyperion.cache.stream.InputStream;

public final class CachedObjectDefinition {

    private static final CachedObjectDefinition[] objectDefinitions = new CachedObjectDefinition[Cache.getCacheFileManagers()[2].getFilesSize(6)];
    public static int[] renderIds = new int[Cache.getCacheFileManagers()[2].getFilesSize(6)];
    public static int[] renderIdsViseVersa = new int[Cache.getCacheFileManagers()[2].getFilesSize(6)];
    private int varbitId = -1;
    private boolean clippingFlag;
    private int moveZ;
    private int shading;
    private int moveX = 0;
    private int[] childrenIDs;
    public String name;
    public String description;
    private int groundDecorationSprite;
    private boolean delayShading;
    public int clipType = 2;
    private int id;
    public int sizeX = 1;
    private boolean isInteractive;
    public boolean walkable;
    private boolean contouredGround;
    public static int[] streamIndices;
    public boolean impenetrable;
    private int scaleX;
    private int[] originalModelColors;
    private int varpId;
    private String[] interactions;
    private boolean inverted;
    private int animationId;
    private int[] modifiedModelColors;
    private int mapSceneSprite;
    public int sizeY;
    private boolean obstructsGround;
    private boolean aBoolean1525;
    public int surroundings;
    public static boolean lowMemory;
    private int moveY;
    private int scaleY;
    private int anInt1673;
    private boolean hollow;
    private int[] modelTypes;
    private int scaleZ;
    private boolean aBoolean1677;
    private int[] object_model_ids;
    public int type;
    private int lightness;

    public int actionCount() {
        int count = 0;
        for (int i = 0; i < interactions.length; i++) {
            if (interactions[i] == null)
                continue;
            if (!interactions[i].equalsIgnoreCase("null") || !interactions[i].equalsIgnoreCase("hidden"))
                count++;
        }
        return count;
    }

    public boolean hasActions() {
        for (int i = 0; i < interactions.length; i++) {
            if (interactions[i] == null)
                continue;
            if (!interactions[i].equalsIgnoreCase("null") || !interactions[i].equalsIgnoreCase("hidden"))
                return true;
        }
        return false;
    }

    public String[] getInteractions() {
        String[] allActions = new String[interactions.length];
        for (int i = 0; i < interactions.length; i++) {
            if (interactions[i] == null) {
                if (name != null && name.contains("door")) {
                    interactions[0] = "fake_interaction";
                    interactions[1] = "fake_interaction";
                    interactions[2] = "fake_interaction";
                    //System.out.println("Interactions " + id + " " + interactions[0]);
                    continue;
                }
            }
            allActions[i] = interactions[i];
        }
        return allActions;
    }

    public static final CachedObjectDefinition forId(int objId) {
        if (objId < 0 || objId >= objectDefinitions.length)
            return null;
        CachedObjectDefinition def = objectDefinitions[objId];
        if (def == null) {
            objectDefinitions[objId] = new CachedObjectDefinition(objId);
            objectDefinitions[objId].interactions[0] = "fake";

            if (objectDefinitions[objId].rangableObject()) {
                objectDefinitions[objId].clippingFlag = false;
            }
            if (objectDefinitions[objId].clippingFlag) {
                objectDefinitions[objId].walkable = false;
                objectDefinitions[objId].clipType = 0;
            }
            if (objectDefinitions[objId].id == 1579 || objectDefinitions[objId].id == 2292 || objectDefinitions[objId].id == 10060 || objectDefinitions[objId].id == 10061) {
                objectDefinitions[objId].clippingFlag = false;
                objectDefinitions[objId].walkable = true;
                objectDefinitions[objId].clipType = 1;
                objectDefinitions[objId].sizeX = 2;
                objectDefinitions[objId].sizeY = 2;
            }
            if (objectDefinitions[objId].id == 14435 || objectDefinitions[objId].id == 2311) {
                objectDefinitions[objId].clipType = 1;
            }
            if (objectDefinitions[objId].id == 2883 || objectDefinitions[objId].id == 2882) {
                objectDefinitions[objId].isInteractive = true;
                objectDefinitions[objId].interactions[0] = "fake";
            }
        } else {
            if ((def.name != null && (def.name.equalsIgnoreCase("bank booth") || def.name.equalsIgnoreCase("counter")))) {
                def.clippingFlag = false;
            }
            if (def.rangableObject()) {
                def.clippingFlag = false;
            }
            if (def.clippingFlag) {
                def.walkable = false;
                def.clipType = 0;
            }
            if (def.id == 1579 || def.id == 2292 || def.id == 10060 || def.id == 10061) {
                def.clippingFlag = false;
                def.walkable = true;
                def.clipType = 1;
                def.sizeX = 2;
                def.sizeY = 2;
            }
            if (def.id == 14435 || def.id == 2311) {
                def.clipType = 1;
            }
            if (def.id == 2883 || def.id == 2882) {
                def.isInteractive = true;
                def.interactions[0] = "fake";
            }
        }
        return def;
    }

    private CachedObjectDefinition(int id) {
        this.id = id;
        setDefaultsVariableValues();
        loadObjectDefinition();
    }

    private final void loadObjectDefinition() {
        byte[] data = Cache.getCacheFileManagers()[2].getFileData(6, id);
        if (data == null) {
            System.out.println("Failed loading object " + id + ".");
            return;
        }
        readOpcodes(new InputStream(data));
        if (hollow) {
            walkable = false;
        }
    }

    /**
     * Reads the opcoode
     *
     * @param stream
     */
    private final void readOpcodes(InputStream stream) {
        while (true) {
            int opcode = stream.readUnsignedByte();
            if (opcode == 0)
                break;
            readValues(stream, opcode);
        }
    }

    private final void readValues(InputStream buffer, int opcode) {
        int len;
        if (opcode == 1) {
            len = buffer.getUByte();
            if (len > -1) {
                if (object_model_ids == null) {
                    modelTypes = new int[len];
                    object_model_ids = new int[len];
                    for (int k1 = 0; k1 < len; k1++) {
                        object_model_ids[k1] = buffer.getUShort();
                        modelTypes[k1] = buffer.getUByte();
                    }
                } else {
                    buffer.offset += len * 3;
                }
            }
        } else if (opcode == 2) {
            name = buffer.getOldString();
        } else if (opcode == 5) {
            len = buffer.getUByte();
            if (len > 0) {
                if (object_model_ids == null || lowMemory) {
                    modelTypes = null;
                    object_model_ids = new int[len];
                    for (int l1 = 0; l1 < len; l1++)
                        object_model_ids[l1] = buffer.getUShort();
                } else {
                    buffer.offset += len * 2;
                }
            }
        } else if (opcode == 14) {
            sizeX = buffer.getUByte();
        } else if (opcode == 15) {
            sizeY = buffer.getUByte();
        } else if (opcode == 17) {
            clipType = 0;
            walkable = false;
        } else if (opcode == 18) {
            walkable = false;
        } else if (opcode == 19) {
            isInteractive = (buffer.getUByte() == 1);
        } else if (opcode == 21) {
            contouredGround = true;
        } else if (opcode == 22) {
            delayShading = true;
        } else if (opcode == 23) {
            aBoolean1677 = true;
        } else if (opcode == 24) {
            animationId = buffer.readUnsignedShort();
            if (animationId == 65535) {
                animationId = -1;
            }
            animationId = -1;
        } else if (opcode == 27) {
            clipType = 1;
        } else if (opcode == 28) {
            anInt1673 = buffer.getUByte();//x offset for walls/wallsdecoration
        } else if (opcode == 29) {
            lightness = buffer.getByte();
        } else if (opcode == 39) {
            shading = buffer.getByte() * 5;
        } else if (opcode >= 30 && opcode < 35) {
            interactions[opcode - 30] = buffer.getOldString();
            if (interactions[opcode - 30].equalsIgnoreCase("Hidden"))
                interactions[opcode - 30] = null;
        } else if (opcode == 40) {
            int i_34_ = buffer.getUByte();
            modifiedModelColors = new int[i_34_];
            originalModelColors = new int[i_34_];
            for (int i_35_ = 0; i_34_ > i_35_; i_35_++) {
                originalModelColors[i_35_] = buffer.readUnsignedShort();
                modifiedModelColors[i_35_] = buffer.readUnsignedShort();
            }
        } else if (opcode == 41) {
            int i_34_ = buffer.readUnsignedShort();
            int[] modifiedModelColors = new int[i_34_];
            int[] originalModelColors = new int[i_34_];
            for (int i_35_ = 0; i_34_ > i_35_; i_35_++) {
                originalModelColors[i_35_] = buffer.readUnsignedShort();
                modifiedModelColors[i_35_] = buffer.readUnsignedShort();
            }
        } else if (opcode == 42) {
            buffer.getUByte();
            //aByteArray1513 = new byte[len];
            //for (var5 = 0; ~var5 > ~len; ++var5) {
            //  aByteArray1513[var5] = buffer.getByte();
            // }
        } else if (opcode == 62) {
            inverted = true;
        } else if (opcode == 64) {
            aBoolean1525 = false;
        } else if (opcode == 65) {
            scaleX = buffer.readUnsignedShort();
        } else if (opcode == 66) {
            scaleY = buffer.readUnsignedShort();
        } else if (opcode == 67) {
            scaleZ = buffer.readUnsignedShort();
        } else if (opcode == 68) {
            buffer.readUnsignedShort();
        } else if (opcode == 69) {
            surroundings = buffer.getUByte();
        } else if (opcode == 70) {
            moveX = buffer.getShort();
        } else if (opcode == 71) {
            moveY = buffer.getShort();
        } else if (opcode == 72) {
            moveZ = buffer.getShort();
        } else if (opcode == 73) {
            obstructsGround = true;
        } else if (opcode == 74) {
            hollow = true;
        } else if (opcode == 75) {
            surroundings = buffer.getUByte();
        } else if (opcode == 77 || opcode == 92) {
            len = -1;
            varbitId = buffer.readUnsignedShort();
            if (varbitId == 65535)
                varbitId = -1;
            varpId = buffer.readUnsignedShort();
            if (varpId == 65535)
                varpId = -1;

            int defaultId = -1;
            if (opcode == 92) {
                defaultId = buffer.readUnsignedShort();
                if (defaultId == 0xffff) {
                    defaultId = -1;
                }
            }

            len = buffer.getUByte();
            childrenIDs = new int[len + 2];
            for (int id = 0; (id <= len); id++) {
                childrenIDs[id] = buffer.readUnsignedShort();
                if (childrenIDs[id] == 65535)
                    childrenIDs[id] = -1;
            }
            childrenIDs[len + 1] = defaultId;
        } else if (opcode == 78) {
            buffer.readUnsignedShort();
            buffer.getUByte();
        } else if (opcode == 79) {
            buffer.readUnsignedShort();
            buffer.readUnsignedShort();
            buffer.getUByte();
            int var4 = buffer.getUByte();
            for (int var5 = 0; ~var4 < ~var5; ++var5) {
                buffer.readUnsignedShort();
            }
        }
        if (name != "null" && name != null) {
            isInteractive = object_model_ids != null
                    && (modelTypes == null || modelTypes[0] == 10);
            if (interactions != null)
                isInteractive = true;
        }
        if (hollow) {
            walkable = false;
            impenetrable = false;
        }
    }

    public boolean rangableObject() {
        int[] rangableObjects = {10060, 10061, 3457, 21369, 21600, 21376, 21366, 21365, 21381, 21364, 23268, 1264, 1246, 23265, 23273, 1257, 12928, 12929, 12930, 12925, 12932, 12931, 26975, 26977, 26978, 26979, 23271, 11754, 3007, 980, 997, 4262, 14437, 14438, 4437, 4439, 3487, 23053};
        for (int i = 0; i < rangableObjects.length; i++) {
            if (rangableObjects[i] == id) {
                //System.out.println("[" + name + " : " + id + "" + "] object is rangable");
                return true;
            }
        }
        if (name != null) {
            if (!name.equalsIgnoreCase("")) {
                final String name1 = name.toLowerCase();
                String[] rangables = {"Rocks", "Boulder", "Stones", "Flowerbed", "Daises", "grass", "fungus", "mushroom", "sarcophagus", "counter", "plant", "altar", "pew", "log", "stump", "stool", "sign", "cart", "chest", "rock", "bush", "hedge", "chair", "table", "crate", "barrel", "box", "skeleton", "corpse", "vent", "fence", "stone", "rockslide"};
                for (int i = 0; i < rangables.length; i++) {
                    if (name1.contains(rangables[i]) || name1.equalsIgnoreCase(rangables[i]) || name1.endsWith(rangables[i])) {
                        //System.out.println("[" + name + " : " + id + "" + "] object is rangable");
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void setDefaultsVariableValues() {
        groundDecorationSprite = -1;
        scaleX = 128;
        animationId = -1;
        //name = "null";
        contouredGround = false;
        mapSceneSprite = -1;
        inverted = false;
        obstructsGround = false;
        aBoolean1525 = true;
        isInteractive = false;
        moveZ = 0;
        scaleY = 128;
        shading = 0;
        varpId = -1;
        delayShading = false;
        sizeY = 1;
        hollow = false;
        surroundings = 0;
        interactions = new String[5];
        moveY = 0;
        scaleZ = 128;
        anInt1673 = 16;
        aBoolean1677 = false;
        lightness = 0;
        walkable = true;
    }
}
