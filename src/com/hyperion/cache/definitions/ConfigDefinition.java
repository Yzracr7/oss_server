package com.hyperion.cache.definitions;

import java.util.HashMap;

import com.hyperion.cache.Cache;
import com.hyperion.cache.stream.InputStream;

public class ConfigDefinition {
 
     
    public static HashMap<Integer,ConfigDefinition> definitions = new HashMap<Integer,ConfigDefinition>();
    public int id;
    public int defaultValue;
     
    public static ConfigDefinition forID(int cfgID) {
        if (definitions.containsKey(cfgID)) {
            return definitions.get(cfgID);
        }
        ConfigDefinition def = new ConfigDefinition();
        def.id = cfgID;
        try {
            byte[] data = Cache.getCacheFileManagers()[2].getFileData(16, cfgID);
            InputStream configStream = new InputStream(data);
            while (true) {
                int opcode = configStream.readUnsignedByte();
                if (opcode == 0) {
                    break;
                }
                switch (opcode) {
                case 5: {
                    def.defaultValue = configStream.readUnsignedShort();
                }
                }
            }
        } catch (Exception ex) {
        	//ex.printStackTrace();
            System.out.println("Failed to grab config:" + cfgID);
            return null;
        }
        definitions.put(cfgID, def);
        return def;
    }
     
    public static int getAmountOfConfigs() {
        return Cache.getCacheFileManagers()[2].getInformation().getContainers()[16].getFilesIndexes().length;
    }
}