package com.hyperion.cache.definitions;

import com.hyperion.cache.Cache;
import com.hyperion.cache.stream.InputStream;

public class IdentityKit {

	private static IdentityKit[] definitions = new IdentityKit[Cache.getCacheFileManagers()[2].getFilesSize(3)];
	public static int ikitLength = Cache.getCacheFileManagers()[2].getFilesSize(3);
	private int id;
	public int anInt466 = -1;
	private int[] anIntArray474;
	private short[] aShortArray460 = new short[6];
	private short[] aShortArray464 = new short[6];
	private int[] anIntArray475 = new int[]{-1, -1, -1, -1, -1};
	public boolean aBoolean476 = false;
	
	private IdentityKit(int id) {
		this.id = id;
		load();
	}
	
	public static IdentityKit list(int id) {
		if (id < 0 || id > definitions.length)
			return null;
		IdentityKit def = definitions[id];
		if (def == null) {
			definitions[id] = def = new IdentityKit(id);
		}
		return def;
	}
	
	private void load() {
		byte[] data = Cache.getCacheFileManagers()[2].getFileData(3, id);
		if (data == null) {
			return;
		}
		init_decode(new InputStream(data));
	}
	
	private void init_decode(InputStream stream) {
		 while(true) {
             int opcode = stream.readUnsignedByte();
             if(0 == opcode) {
                return;
             }
             this.decode(stream, opcode);
          }
	}
	
	private void decode(InputStream stream, int opcode) {
		if (opcode == 1) {
			 this.anInt466 = stream.readUnsignedByte();
		} else if (opcode == 2) {
			int var4 = stream.readUnsignedByte();
			this.anIntArray474 = new int[var4];
			for (int var5 = 0; var4 > var5; ++var5) {
				this.anIntArray474[var5] = stream.readUnsignedShort();
			}
		} else if (opcode == 3) {
			this.aBoolean476 = true;
		} else if(opcode >= 40 && opcode < 50) {
			   this.aShortArray460[opcode - 40] = (short)stream.readUnsignedShort();
		   } else if(opcode >= 50 && opcode < 60) {
	           this.aShortArray464[opcode - 50] = (short)stream.readUnsignedShort();
		   } else if(~opcode <= -61 && opcode < 70) {
			   this.anIntArray475[opcode - 60] = stream.readUnsignedShort();
		   }
	}
}
