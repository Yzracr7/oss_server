package com.hyperion.cache.json.impl;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.Objects;

import com.hyperion.cache.json.JsonLoader;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.item.RequirementHandler;
import com.hyperion.game.world.entity.combat.WeaponInterfaces.WeaponInterface;

/**
 * The {@link JsonLoader} implementation that loads all osrs item definitions.
 *
 * @author trees (www.rune-server.ee/members/trees)>
 */
public final class InfernoItemDefinitionLoader extends JsonLoader {

    /**
     * Creates a new {@link InfernoItemDefinitionLoader}.
     */
    public InfernoItemDefinitionLoader() {
        super("./data/json/items/inferno_item_definitions.json");
    }

    @Override
    public void load(JsonObject reader, Gson builder) {
        int index = reader.get("id").getAsInt();
        String name = Objects.requireNonNull(reader.get("name").getAsString());
        String examine = Objects.requireNonNull(reader.get("examine").getAsString());
        ItemDefinition.EquipmentType equipmentType = builder.fromJson(reader.get("equipmentType").getAsString(), ItemDefinition.EquipmentType.class);
        WeaponInterface weaponInterface = builder.fromJson(reader.get("equipmentType").getAsString(), WeaponInterface.class);
        boolean doubleHanded = reader.get("doubleHanded").getAsBoolean();
        boolean stackable = reader.get("stackable").getAsBoolean();
        boolean dropable = reader.get("dropable").getAsBoolean();
        boolean sellable = reader.get("sellable").getAsBoolean();
        boolean noted = reader.get("noted").getAsBoolean();
        int value = reader.get("value").getAsInt();
        int specialPrice = reader.get("specialPrice") != null ? reader.get("specialPrice").getAsInt() : -1;
        int highAlch = reader.get("highAlch").getAsInt();
        int lowAlch = reader.get("lowAlch").getAsInt();
        int dropValue = reader.get("dropValue").getAsInt();
        int noteId = reader.get("noteId").getAsInt();
        int attackAnim = reader.get("attackAnim") != null ? reader.get("attackAnim").getAsInt() : -1;
        if (attackAnim > -1) {
            //System.out.println("attack anim found for"+index);
        }
        int blockAnim = reader.get("blockAnim").getAsInt();
        int standAnim = reader.get("standAnim").getAsInt();
        int walkAnim = reader.get("walkAnim").getAsInt();
        int runAnim = reader.get("runAnim").getAsInt();
        int standTurnAnim = reader.get("standTurnAnim").getAsInt();
        int turn180Anim = reader.get("turn180Anim").getAsInt();
        int turn90CWAnim = reader.get("turn90CWAnim").getAsInt();
        int turn90CCWAnim = reader.get("turn90CCWAnim").getAsInt();
        double weight = reader.get("weight").getAsDouble();

        double[] bonus = reader.get("bonuses") == null ? null : builder.fromJson(reader.get("bonuses").getAsJsonArray(), double[].class);
        int[] requirements = reader.get("requirements") == null ? null : builder.fromJson(reader.get("requirements").getAsJsonArray(), int[].class);
        if (requirements != null) {
            for (int i = 0; i < requirements.length; i++) {
                RequirementHandler.addRequirement(index, i, requirements[i]);
            }
        }

        int equipment_slot = -1;
        if (equipmentType != null) {
            equipment_slot = equipmentType.ordinal();
        }

        /**
         *   Builds the definition
         */
        ItemDefinition.DEFINITIONS[index] = new ItemDefinition(index, name, examine, /*equipmentType*/equipment_slot, noteId == -1,noteId, stackable, specialPrice,
                value, lowAlch, highAlch, weight, bonus, doubleHanded, false, false, sellable, standAnim, runAnim, walkAnim, blockAnim, standTurnAnim, turn180Anim, turn90CWAnim, turn90CCWAnim, attackAnim);
    }
}