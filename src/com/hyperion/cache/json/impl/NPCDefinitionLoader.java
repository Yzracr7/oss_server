package com.hyperion.cache.json.impl;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.hyperion.cache.json.JsonLoader;
import com.hyperion.game.world.entity.npc.NPCDefinition;

import java.util.Objects;

/**
 * The {@link JsonLoader} implementation that loads all npc definitions.
 *
 * @author lare96 <http://github.com/lare96>
 */
public final class NPCDefinitionLoader extends JsonLoader {

    /**
     * Creates a new {@link NPCDefinitionLoader}.
     */
    public NPCDefinitionLoader() {

        super("./data/json/npcs/npc_definitions.json");
    }

    @Override
    public void load(JsonObject reader, Gson builder) {
        int index = reader.get("id").getAsInt();
        String name = Objects.requireNonNull(reader.get("name").getAsString());
        String description = Objects.requireNonNull(reader.get("examine").getAsString());
        int combatLevel = reader.get("combat").getAsInt();
        int size = reader.get("size").getAsInt();
        boolean attackable = reader.get("attackable").getAsBoolean();
        boolean aggressive = reader.get("aggressive").getAsBoolean();
        boolean retreats = reader.get("retreats").getAsBoolean();
        boolean poisonous = reader.get("poisonous").getAsBoolean();
        int respawnTime = reader.get("respawn").getAsInt();
        int maxHit = reader.get("maxHit").getAsInt();
        int hitpoints = reader.get("hitpoints").getAsInt();
        int attackSpeed = reader.get("attackSpeed").getAsInt();
        int attackAnim = reader.get("attackAnim").getAsInt();
//        int attackAnim2 = reader.get("attackAnim2").getAsInt();
        int defenceAnim = reader.get("defenceAnim").getAsInt();
        int deathAnim = reader.get("deathAnim").getAsInt();
        int attackBonus = reader.get("attackBonus").getAsInt();
        int meleeDefence = reader.get("defenceMelee").getAsInt();
        int rangedDefence = reader.get("defenceRange").getAsInt();
        int magicDefence = reader.get("defenceMage").getAsInt();

       // if (attackAnim2 > -1) {
        //    System.out.println("Attack anim " + attackAnim2 + " " + index + " name=" + name + " desc=" + description + " attackanim:" + attackAnim);
       // }
        NPCDefinition.definitions[index] = new NPCDefinition(index, name, description, combatLevel, size, attackable, aggressive, retreats,
                poisonous, respawnTime, maxHit, hitpoints, attackSpeed, attackAnim, defenceAnim, deathAnim, attackBonus, meleeDefence,
                rangedDefence, magicDefence);

        //  if (aggressive)
        // NPCDefinition.definitions[index].();
        //System.out.println("NPC IS AGRESSIVE" + index + " name=" + name + " desc=" + description);
        // NpcAggression.AGGRESSIVE.add(index);
    }
}