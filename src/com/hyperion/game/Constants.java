package com.hyperion.game;

import com.hyperion.game.world.Location;

public class Constants {

    public static final boolean SERVER_HOSTED = false;
    public static final boolean BETA_PLAYER_ONLY_ACCESS = true;

    public static final String SERVER_NAME = "Smite";
    public static boolean ENABLE_LOGIN_SERVER = false;
    public static final boolean DEBUG_MODE = true;//!SERVER_HOSTED;
    public static final int MAX_PLAYERS = 2000;
    public static final int MAX_NPCS = 10000;
    public static final String PLAYERS_FILE_PATH = "C:\\Users\\trees\\Desktop\\smite_assets\\savedgames\\";  // "./data/";
    public static final String FILES_PATH = "./data/";  // "./data/";
    public static final String webStoreURL = "https://smite.io/store";
    public static final int USERNAME_LENGTH_LIMIT = 16;
    public static final int PASSWORD_LENGTH_LIMIT = 20;
    public static final boolean USE_SEPARATE_FILE_SERVER = false;
    public static final boolean SQL_SAVING = false;
    public static final boolean USE_DEV_CACHE = true;
    public static final int MAX_ITEMS = 2147483647;
    public static final int MAX_STARTER_PER_IP = 10;
    public static final int MAX_STARTER_PER_MAC = 10;

    public static final int WELCOME_BOX_ID = 653;

    // Toggles
    public static boolean TRADE = true;
    public static boolean DROP = true;
    public static boolean DUEL = true;
    public static boolean MINIGAME = true;
    public static boolean NPC = true;
    public static boolean COMMANDS = true;
    public static boolean YELL = true;
    public static boolean SKILLING = true;
    public static boolean COMBAT = true;
    public static boolean SAFEMODE = false;
    public static boolean WALKING = true;
    public static boolean PVP_ZONE = false;

    public static int MAX_CLAN_MEMBERS = 50;

    public static final Location RESPAWN_LOCATION = Location.create(3094, 3468, 0);//Location.create(2347, 3798, 0);//
    public static final Location HOME_LOCATION = Location.create(3088, 3501, 0);//Location.create(2343, 3808, 0);//
    public static final Location START_LOCATION = Location.create(3094, 3107, 0);

    public static final int MAX_PACKETS_PER_CYCLE = 30;
    public static final int NON_COMBAT_EXP_MULTIPLIER = 27, IRONMAN_NONCOMBAT_EXP_MULTIPLER = 27;
    public static final int COMBAT_EXP_MULTIPLIER = 27, IRONMAN_COMBAT_EXP_MULTIPLIER = 27;

    public static final int BONUS_XP_MULTIPLIER = 1;

    public static final int QUEST_POINTS = 7;

    public static final String[] OWNERS = {"technotik", "wook", "trees"};

    public static final String[] TITLES = {"<col=cc8400>Novice</col>", "<col=cc8400>Fighter</col>", "<col=cc8400>Lord</col>", "<col=8800CC>The Worthy</col>", "<col=8800CC>The Untouchable</col>", "<col=8800CC>The Intimidating</col>", "<col=51BA00>Epic</col>", "<col=51BA00>Overlord</col>", "<col=D400C2>The Holy</col>", "<col=8800CC>Duke</col>", "<col=8800CC>Doctor</col>", "<col=8800CC>Big Cheese</col>", "<col=8800CC>Bigwig</col>", "<col=D40000>The Awesome</col>", "<col=D40000>The Magnificent</col>", "<col=D40000>The Undefeated</col>", "<col=D40000>The Strange</col>", "<col=D40000>The Divine</col>", "<col=D40000>The Fallen</col>", "<col=D40000>The Warrior</col>"};

    public static final int[] TITLE_COSTS = {5, 5, 5, 10, 10, 10, 20, 20, 100, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40};

    /**
     * Return codes.
     *
     * @author Graham
     */
    public static final class ReturnCodes {
        public static final int DISPLAY_ADVERTISEMENT = 1;
        public static final int LOGIN_OK = 2;
        public static final int INVALID_PASSWORD = 3;
        public static final int BANNED = 4;
        public static final int ALREADY_ONLINE = 5;
        public static final int GAME_UPDATED_RELOAD = 6;
        public static final int WORLD_FULL = 7;
        public static final int LOGIN_SERVER_OFFLINE = 8;
        public static final int LOGIN_LIMIT_EXCEEDED = 9;
        public static final int BAD_SESSION_ID = 10;
        public static final int FORCE_CHANGE_PASSWORD = 11;
        public static final int MEMBERS_WORLD = 12;
        public static final int COULD_NOT_COMPLETE = 13;
        public static final int UPDATE_IN_PROGRESS = 14;
        public static final int MEMBERS_ONLY_AREA = 17;
        /*
         * Anything else is 'unexpected response, please try again'.
		 */
    }

    public static final int[] MAP_SIZES = {104, 120, 136, 168, 72};

    public static final int MIN_FREE_MEM_ALLOWED = 30000000; // 30mb

    public static boolean STAR_ACTIVE = false;

    public final static int[] BARROWS_ITEMS = {4712, 4714, 4716, 4718, 4720, 4722, 4724, 4726, 4728, 4730, 4732, 4734, 4736, 4738, 4745, 4747, 4749, 4751, 4753, 4755, 4757, 4759,};

    public final static int[] REGULAR_ITEMS = {11732, 4151, 11283, 6918, 6920, 6922, 6924, 6570, 8850, 18662, 10551, 10548, 4087, 6737, 6731, 6733, 11716, 6889, 6914, 4585, 6568, 2581, 3140, 6916, 6918, 6920, 6922, 6924, 3481, 3483, 3486, 2577, 10452, 10454, 10456, 9470, 10450, 10446, 10448, 1037, 14595, 14603, 11732, 6585, 6570, 11694, 11696, 11698, 11700, 11724, 11726, 11728, 11718, 11720, 11722, 868, 15486, 11235,// 1033, 1035,// zam
            // robes(junk)
            13858, 13861, 13864, 13867,// zuriel
            13870, 13873, 13876, 13879, 13883,// morrigan
            15126,// 2364, 2579, 2460, 2462, 2464, 2466, 2468, 2470, 2472, 2474, 2476,// flowers
            2362, 1149, 11730, 15273, 11212, 14490, 14492, 14494,// elite black
            //	4611, 6384, 6386, 6388, 6390, // desert junk
    };

    public final static int[] RARE_ITEMS = {12954
    };

    public final static int[] SUPER_RARE_ITEMS = {1038, 1040, 1042, 1044, 1046, 1048,// phats/masks
            5607,// grain
    };

    public final static int[] RANDOM_EVENT_ITEMS = {3057, 3058, 3059, 3060, 3061, // mime
            // outfit
            6961, // baguette
            6188, // frog mask
            6184, 6185, 6186, 6187, // prince(m+f)
            6180, 6181, 6182, // lederhosen
            6654, 6655, 6656, // camo
            7592, 7593, 7594, 7595, 7596, // zombie outfit
    };


    public static int[] RANDOM_GEM = {1617, 1619, 1621, 1623, 1625, 1627, 1629}; // Doesn't
    // include
    // onyx/dstone

    public static int[] PVP_ARMOR = {13887, 13893, 13899, 13905, // vesta
            13870, 13873, 13876, 13879, 13883, // morrigan
            13858, 13861, 13864, 13867, // zuriel
            13884, 13890, 13896, 13902 // statius
    };

    public static final int BANK_TAB_HEADER = 786468;
    public static final int CRYPTION_ID = 496200008;


    public static final int[] IRONMAN_SET = new int[]{12810, 12811, 12812};
    public static final int[] ULT_IRONMAN_SET = new int[]{12813, 12814, 12815};

    public final static int[][] BROKEN_BARROWS = {{4708, 4860}, {4710, 4866}, {4712, 4872}, {4714, 4878}, {4716, 4884}, {4720, 4896}, {4718, 4890}, {4720, 4896}, {4722, 4902}, {4732, 4932}, {4734, 4938}, {4736, 4944}, {4738, 4950}, {4724, 4908}, {4726, 4914}, {4728, 4920}, {4730, 4926}, {4745, 4956}, {4747, 4926}, {4749, 4968}, {4751, 4974}, {4753, 4980}, {4755, 4986}, {4757, 4992}, {4759, 4998}};

}
