package com.hyperion.game;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import com.hyperion.game.task.BlockingExecutorService;
import com.hyperion.game.task.Task;
import com.hyperion.game.task.impl.NPCResetTask;
import com.hyperion.game.task.impl.NPCTickTask;
import com.hyperion.game.task.impl.NPCUpdateTask;
import com.hyperion.game.task.impl.PlayerResetTask;
import com.hyperion.game.task.impl.PlayerTickTask;
import com.hyperion.game.task.impl.PlayerUpdateTask;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.sql.login.LoginManager;
import com.hyperion.utility.NumberUtils;

/**
 * The 'core' class of the server which processes all the logic tasks in one
 * single logic <code>ExecutorService</code>. This service is scheduled which
 * means <code>Event</code>s are also submitted to it.
 * 
 * @author Graham Edgecombe
 *
 */
public class GameEngine implements Runnable {

	/**
	 * A queue of pending tasks.
	 */
	private final BlockingQueue<Task> tasks = new LinkedBlockingQueue<Task>();

	/**
	 * The sql service.
	 */
	 private final ScheduledExecutorService sqlService =
			 Executors.newScheduledThreadPool(1);

	/**
	 * The logic service.
	 */
	private final ScheduledExecutorService logicService = Executors
			.newScheduledThreadPool(1);

	/**
	 * The task service, used by <code>ParallelTask</code>s.
	 */
	private final BlockingExecutorService taskService = new BlockingExecutorService(
			Executors.newFixedThreadPool(Runtime.getRuntime()
					.availableProcessors()));

	/**
	 * The work service, generally for file I/O and other blocking operations.
	 */
	private final ExecutorService workService = Executors
			.newSingleThreadExecutor();

	/**
	 * The executor.
	 */
	private final Executor executor = Executors.newSingleThreadExecutor();

	/**
	 * Running flag.
	 */
	private boolean running = false;

	/**
	 * Thread instance.
	 */
	private Thread thread;

	/**
	 * Submits a new task which is processed on the logic thread as soon as
	 * possible.
	 * 
	 * @param task
	 *            The task to submit.
	 */
	public void pushTask(Task task) {
		tasks.offer(task);
	}

	/**
	 * Checks if this <code>GameEngine</code> is running.
	 * 
	 * @return <code>true</code> if so, <code>false</code> if not.
	 */
	public boolean isRunning() {
		return running;
	}

	/**
	 * Starts the <code>GameEngine</code>'s thread.
	 */
	public void start() {
		if (running) {
			throw new IllegalStateException("The engine is already running.");
		}
		running = true;
		executor.execute(GameEngine.this);
	}

	/**
	 * Stops the <code>GameEngine</code>'s thread.
	 */
	public void stop() {
		if (!running) {
			throw new IllegalStateException("The engine is already stopped.");
		}
		running = false;
		thread.interrupt();
	}

	@Override
	public void run() {
		while (running) {
			long currentTime = System.currentTimeMillis();
			try {
				World.getWorld().getTickableManager().pulse();
			} catch (Throwable e) {
				e.printStackTrace();
			}

			try {
				//buildFakeSales list
				for (Player player : World.getWorld().getActivePlayers()) {
					if (player != null) {
						if (player.getDuelSession() == null) {
							World.getWorld().setRandomIndex(player);
						}
					}
				}
				//if (!Constants.STAR_ACTIVE
				//		&& World.getWorld().getActivePlayers().size() >= 10) {
				//	World.getWorld().submit(new ShootingStarTask());
				//	Constants.STAR_ACTIVE = true;
				//}
				for (Player player : World.getWorld().getActivePlayers()) {
					if (player == null) {
						World.getWorld().getActivePlayers().remove(player); //new
						continue;
					}
					if ((player.getSession() == null || (!player.getSession().isConnected()) && player.getAttributes().isSet("sessionConnected"))
							&& !player.getAttributes().isSet("forcelogout")) {
						if(System.currentTimeMillis() - player.getCombatState().getLogoutTimer() >= 10000) {
							System.out.println("Kicked null session player(apparently sessionConnected attribute present) " + player.getName());
							LoginManager.unregister_player(player);
							World.getWorld().getActivePlayers().remove(player);
							continue;
						}
					}
					if(NumberUtils.random(0, 10000) < 2) {
						//TODO: Save a certain index in the list due to the fact that players are shuffled?
						World.getWorld().getWorldLoader().savePlayer(player);
						System.out.println("Randomly saving player: " + player.getName());
					}
					PlayerTickTask.execute(player);
				}
				for (NPC npc : World.getWorld().getNPCS()) {
					if (npc == null)
						continue;
					NPCTickTask.execute(npc);
				}
			} catch (Throwable e) {
				e.printStackTrace();
			}
			try {
				for (Player player : World.getWorld().getActivePlayers()) {
					if (player == null) {
						World.getWorld().getActivePlayers().remove(player); //new
						continue;
					}
					PlayerUpdateTask.execute(player);
					NPCUpdateTask.execute(player);
				}
			} catch (Throwable e) {
				e.printStackTrace();
			}
			try {
				for (Player player : World.getWorld().getActivePlayers()) {
					if (player == null) {
						World.getWorld().getActivePlayers().remove(player); //new
						continue;
					}
					PlayerResetTask.execute(player);
				}
				for (NPC npc : World.getWorld().getNPCS()) {
					if (npc == null)
						continue;
					NPCResetTask.execute(npc);
				}
			} catch (Throwable e) {
				e.printStackTrace();
			}
			long sleepTime = 600 + currentTime - System.currentTimeMillis();
			if (sleepTime <= 0)
				continue;
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Schedules a task to run in the logic service.
	 * 
	 * @param runnable
	 *            The runnable.
	 * @param delay
	 *            The delay.
	 * @param unit
	 *            The time unit.
	 * @return The <code>ScheduledFuture</code> of the scheduled logic.
	 */
	public ScheduledFuture<?> scheduleLogic(final Runnable runnable,
			long delay, TimeUnit unit) {
		return logicService.schedule(new Runnable() {
			public void run() {
				try {
					runnable.run();
				} catch (Throwable t) {
					World.getWorld().handleError(t);
				}
			}
		}, delay, unit);
	}

	/**
	 * Submits a task to run in the parallel task service.
	 * 
	 * @param runnable
	 *            The runnable.
	 */
	public void submitTask(final Runnable runnable) {
		taskService.submit(new Runnable() {
			public void run() {
				try {
					runnable.run();
				} catch (Throwable t) {
					World.getWorld().handleError(t);
				}
			}
		});
	}

	/**
	 * Submits a task to run in the work service.
	 * 
	 * @param runnable
	 *            The runnable.
	 */
	public void submitWork(final Runnable runnable) {
		workService.submit(new Runnable() {
			public void run() {
				try {
					runnable.run();
				} catch (Throwable t) {
					World.getWorld().handleError(t);
				}
			}
		});
	}

	/**
	 * Submits a task to run in the misc service.
	 * 
	 * @param runnable
	 *            The runnable.
	 */
	public void submitMisc(final Runnable runnable) {
		workService.submit(new Runnable() {
			public void run() {
				try {
					runnable.run();
				} catch (Throwable t) {
					World.getWorld().handleError(t);
				}
			}
		});
	}

	/**
	 * Submits a task to run in the logic service.
	 * 
	 * @param runnable
	 *            The runnable.
	 */
	public void submitLogic(final Runnable runnable) {
		logicService.submit(new Runnable() {
			public void run() {
				try {
					runnable.run();
				} catch (Throwable t) {
					World.getWorld().handleError(t);
				}
			}
		});
	}

	
	 public void submitSQL(final Runnable runnable) { 
		 sqlService.submit(new Runnable() {
			 public void run() { 
				 try { 
					 runnable.run(); 
				 } catch (Throwable t) {
					 World.getWorld().handleError(t);
					 }
				 }
			 }); 
		 }
	 

	/**
	 * Waits for pending parallel tasks.
	 * 
	 * @throws ExecutionException
	 *             If an error occurred during a task.
	 */
	public void waitForPendingParallelTasks() throws ExecutionException {
		taskService.waitForPendingTasks();
	}

}
