package com.hyperion.game;

import com.hyperion.Logger;
import com.hyperion.Server;
import com.hyperion.game.discord.Discord;
import com.hyperion.game.net.ConnectionHandler;
import com.hyperion.game.world.World;
import com.hyperion.sql.SQLController;
import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.ExecutionException;

/**
 * Starts everything else including MINA and the <code>GameEngine</code>.
 *
 * @author Graham Edgecombe
 */
public class RS2Server {

    /**
     * The port to listen on.
     */
    public static final int PORT = 43593 + Server.world;

    /**
     * The <code>IoAcceptor</code> instance.
     */
    private final IoAcceptor acceptor = new NioSocketAcceptor();

    /**
     * The <code>GameEngine</code> instance.
     */
    private static final GameEngine engine = new GameEngine();

    /**
     * Creates the server and the <code>GameEngine</code> and initializes the
     * <code>World</code>.
     *
     * @throws Exception
     */
    public RS2Server() throws Exception {
        World.getWorld().init(engine);
        SQLController.init();
       // Discord.speak("```Server has been restarted... You may now login.```");
        acceptor.setHandler(new ConnectionHandler());
        acceptor.getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, 5);
    }

    /**
     * Binds the server to the specified port.
     *
     * @param port The port to bind to.
     * @return The server instance, for chaining.
     * @throws IOException
     */
    public RS2Server bind(int port) throws IOException {
        Logger.getInstance().warning("Binding to port : " + port + "...");
        acceptor.bind(new InetSocketAddress(port));
        return this;
    }

    /**
     * Starts the <code>GameEngine</code>.
     *
     * @throws ExecutionException if an error occured during background loading.
     */
    public void start() throws ExecutionException {
        // ScriptManager.getScriptManager().loadScripts(Constants.SCRIPTS_DIRECTORY);
        if (World.getWorld().getBackgroundLoader().getPendingTaskAmount() > 0) {
            Logger.getInstance().warning("Waiting for pending background loading tasks...");
            World.getWorld().getBackgroundLoader().waitForPendingTasks();
        }
        World.getWorld().getBackgroundLoader().shutdown();
        engine.start();
        Logger.getInstance().warning("Ready on " + (Constants.SERVER_HOSTED ? "Game" : "Local") + " Server.");
    }

    /**
     * Gets the <code>GameEngine</code>.
     *
     * @return The game engine.
     */
    public static GameEngine getEngine() {
        return engine;
    }

}
