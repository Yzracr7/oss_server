package com.hyperion.game;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.mina.core.buffer.IoBuffer;

import com.hyperion.game.Constants.ReturnCodes;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.PlayerDetails;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.utility.TextUtils;
import com.hyperion.utility.json.JsonManager;
import com.hyperion.utility.json.impl.PunishmentLoader;
import com.hyperion.utility.json.sub.Punishment.PunishmentType;

public class SQLWorldLoader implements WorldLoader {

    /**
     * Handles login properly and efficiently
     *
     * @param details The details for login
     * @return
     */
    @Override
    public LoginResult checkLogin(PlayerDetails details) {
        int code = ReturnCodes.LOGIN_OK;
        String username = details.getName();
        registerDetails(details);
        if (username.length() < 3 || username.length() > 12 || !TextUtils.isValidName(username)) {
            code = ReturnCodes.INVALID_PASSWORD;
        } else if (World.getWorld().isUpdateInProgress()) {
            //code = Constants.ReturnCodes.INVALID_PASSWORD;
            //code = Constants.ReturnCodes.UPDATE_IN_PROGRESS;
            code = ReturnCodes.GAME_UPDATED_RELOAD;
        } else if (World.getWorld().isPlayerOnline(username)) {
            //code = Constants.ReturnCodes.INVALID_PASSWORD;
            code = ReturnCodes.ALREADY_ONLINE;
        }
        if (JsonManager.getSingleton().getLoader(PunishmentLoader.class).isPunished(details.getIP(), PunishmentType.IPBAN)
                //Below is a new addition - eg. is Angel Dust is IP banned but logs in with another IP it'll still deny
                || JsonManager.getSingleton().getLoader(PunishmentLoader.class).isPunished(username, PunishmentType.IPBAN)
                || JsonManager.getSingleton().getLoader(PunishmentLoader.class).isPunished(username, PunishmentType.BAN)
                || JsonManager.getSingleton().getLoader(PunishmentLoader.class).isPunished(details.getMacAddress(), PunishmentType.MACBAN)
                || details.hasGroupRights(Rights.BANNED)) {
            code = ReturnCodes.BANNED;
        }
        return new LoginResult(code, new Player(details));
    }

    /**
     * @param details The {@link PlayerDetails} instance
     */
    public static void registerDetails(PlayerDetails details) {
        if (Constants.DEBUG_MODE) {
            details.setPrimaryGroup(Rights.OWNER.getMemberGroupId());
            //List<Integer> secondary = new ArrayList<Integer>();
            //secondary.add(Rights.LEGENDARY_DONATOR.getMemberGroupId());
            //details.setSecondaryGroups(secondary);
            if (details.getName().toLowerCase().contains("jobbernowl")) {
                details.setPrimaryGroup(Rights.DONATOR.getMemberGroupId());
                details.setDonator(true);
            }
            return;
        }
        List<Integer> rights = details.getAllGroups();
        if (rights == null) {
            return;
        }
        for (Integer rightId : rights) {
            Rights right = Rights.getRights(rightId);
            if (right == null) {
                continue;
            }
            switch (right) {
                case OWNER:
                case ADMINISTRATOR:
                    details.setAdmin(true);
                    break;
                case MODERATOR:
                    details.setModerator(true);
                    break;
                case DONATOR:
                    details.setDonator(true);
                    break;
                case SUPER_DONATOR:
                    details.setSuperDonator(true);
                    break;
                case EXTREME_DONATOR:
                    details.setExtremeDonator(true);
                    break;
                case LEGENDARY_DONATOR:
                    details.setLegendaryDonator(true);
                    break;
                case IRONMAN:
                    details.setIronman(true);
                    break;
                default:
                    details.setPrimaryGroup(Rights.PLAYER.getMemberGroupId());
                    break;
            }
        }
    }

    @Override
    public boolean loadPlayer(Player player) {
        try {
            File f = new File(ACCOUNTS_DIRECTORY + player.getDetails().getName() + ".dat.gz");
            InputStream is = new FileInputStream(f);
            IoBuffer buf = IoBuffer.allocate(1024);
            buf.setAutoExpand(true);
            while (true) {
                byte[] temp = new byte[1024];
                int read = is.read(temp, 0, temp.length);
                if (read == -1) {
                    break;
                } else {
                    buf.put(temp, 0, read);
                }
            }
            buf.flip();
            player.deserialize(buf);
            is.close();
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    @Override
    public boolean savePlayer(Player player) {
        //TODO: If player changed their name, delete old charfile before saving to a new one.
        try {
            OutputStream os = new FileOutputStream(ACCOUNTS_DIRECTORY + player.getDetails().getName() + ".dat.gz");
            IoBuffer buf = IoBuffer.allocate(1024);
            buf.setAutoExpand(true);
            player.serialize(buf);
            buf.flip();
            byte[] data = new byte[buf.limit()];
            buf.get(data);
            os.write(data);
            os.flush();
            os.close();
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    public static final String ACCOUNTS_DIRECTORY = System.getProperty("user.home") + "/Desktop/smite_assets/" + "savedGames/";//Constants.FILES_PATH + "savedGames/";
}