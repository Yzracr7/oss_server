package com.hyperion.game.content;

import com.hyperion.game.Constants;
import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.MACStarterMap;
import com.hyperion.utility.ProtocolUtils;
import com.hyperion.utility.StarterMap;
import com.hyperion.utility.game.ChatColors;

/**
 * @author Mack/Hoodlum // Konceal
 */
public class BeginnerTutorial {

    // TODO: Add "continue" button to move to next step
    // TODO: client sided please wait like soulsplit, please wait for region

    private final static Location[] LOCATIONS = {
            Location.create(2568, 3847, 0),
            Location.create(2661, 2648, 0),
            Location.create(2873, 3546, 0),
            Location.create(2789, 9996, 0),
            Location.create(3429, 3538, 0),
            Location.create(3375, 3284, 0),
            Location.create(3104, 3487, 0)
    };

    /**
     * Starts the tutorial for new players
     */
    public static void start(final Player player) {
        // TODO skip tutorial?
        // TODO: use config 1021 to flash tab and then sendTeleportTitles it to player when we
        // go on to the next one.
        // TODO show sailor location
        player.getAttributes().set("stopMovement", true);
        player.getAttributes().set("stopActions", true);
        player.getAttributes().set("inTutorial", true);
        player.getAttributes().set("tutorialStage", 0);
        World.getWorld().submit(new Tickable(2) {
            @Override
            public void execute() {
                handle(player, 0);
                World.getWorld().submit(new Tickable(1) {
                    @Override
                    public void execute() {
                        int stage = player.getAttributes().getInt("tutorialStage") + 1;
                        player.getAttributes().set("tutorialStage", stage);
                        if (stage < 8) {
                            handle(player, stage);
                        }
                        if (stage >= 4) {
                            player.getPacketSender().sendChatboxDialogue(false, "<col=0000FF>Iron Man Selection", "Select your game mode above.");
                            player.getPacketSender().displayInterface(649);//615
                            //player.getAttributes().set("selectedIronMan", 0);
                            QuestTab.showPlayerTab(player);
                            this.stop();
                        } else {
                            if (stage > 2) {
                                // player.sendMessage("Do you need help? You can always ask on discord.");
                            }
                            this.setTickDelay(10);
                        }
                    }
                });
                this.stop();
            }
        });
    }

    private static void handle(final Player player, int stage) {
        if (stage == 1) {
            player.getPacketSender().sendChatboxDialogue(false, "<col=0000FF>Founders", "Welcome to smite, enjoy your time with us!", "We have worked hard for you to experience oldschool emulation", "with high definition rendering and quality.");
        } else if (stage == 2) {
            player.getPacketSender().sendChatboxDialogue(false, "<col=0000FF>Founders", "For any assistance,", "You can later join 'Help' Clan Chat or you", "may join our discord where we have all the answers!");
        } else if (stage == 3) {
            player.getPacketSender().sendChatboxDialogue(false, "<col=0000FF>Founders", "Good luck on your adventure!", "May you find wealth and glory...");
        }
    }

    /**
     * The starter items placed in the user's bank upon completion of tutorial.
     */
    public static void giveStarter(Player player) {
        if (!player.getVariables().hasReceivedStarter()) {
            player.getVariables().setStarted(true);
            String ip = player.getDetails().getIP();
            int count = StarterMap.getSingleton().getCount(ip);
            String mac = ProtocolUtils.getMacAddress(player);
            int count2 = MACStarterMap.getSingleton().getCount(mac);
            if (count >= Constants.MAX_STARTER_PER_IP || count2 >= Constants.MAX_STARTER_PER_MAC) {
                player.getPacketSender().sendMessage("Your address has claimed " + count + " starters, that's too many for another.");
            } else {
                StarterMap.getSingleton().addIP(ip);
                MACStarterMap.getSingleton().addMAC(mac);
                for (Item item : STARTER_KIT) {
                    player.getInventory().addItem(item);
                }
                player.getPacketSender().sendMessage("Your starter package has been added to your inventory.");
                if (player.getAttributes().isSet("selectedIronMan") && player.getAttributes().get("selectedIronMan").equals(1)) {
                    if (player.getDetails().isIronman()) {
                        for (int i = 0; i < Constants.IRONMAN_SET.length; i++) {
                            player.getInventory().addItem(new Item(Constants.IRONMAN_SET[i], 1));
                        }
                        player.getPacketSender().sendMessage("Your Ironman equipment has been added to your inventory.");
                    }
                }
            }
        }
        player.getInventory().refresh();
        player.getPacketSender().sendMessage("<col=" + ChatColors.MAROON + ">Need help? Visit the 'Help' chat or check our online website at http://smite.io!");
        player.getAttributes().remove("inStartTut");
        player.getAttributes().remove("selectedIronMan");
        QuestTab.refresh(player);
    }

    private static final Item[] STARTER_KIT = new Item[]{
            new Item(841, 1),//1x Shortbow
            new Item(1129, 1),//1x Leather body
            new Item(1095, 1),//1x Leather chaps
            new Item(1725, 1),//1x Amulet of strength
            new Item(1101, 1),//1x Iron chainbody
            new Item(1067, 1),//1x Iron platelegs
            new Item(3105, 1),//1x Rock climbing boots
            new Item(1323, 1),//1x Iron scimitar
            new Item(884, 100),//100x Iron arrow
            new Item(577, 1),//1x Wizard robe
            new Item(1011, 1),//1x Blue skirt
            new Item(554, 100),
            new Item(555, 100),
            new Item(556, 100),
            new Item(557, 100),
            new Item(558, 100),
            new Item(559, 100),
            new Item(562, 100),
            new Item(563, 100),
            new Item(1381, 1),//1x Staff of air
            new Item(379, 100),//100x Lobster
            new Item(995, 2500),//
    };
}
