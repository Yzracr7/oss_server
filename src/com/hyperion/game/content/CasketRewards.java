package com.hyperion.game.content;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.hyperion.cache.Cache;
import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;

public class CasketRewards {
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		Cache.init();
		//Loaders.loadItemDefinitions();
		printItems("Low Rewards", low_rewards);
		printItems("Medium Rewards", medium_rewards);
		printItems("Rare Rewards", rare_rewards);
	}
	
	public static void printItems(String name, Item[] items) {
		System.out.println("==================================================");
		System.out.println(name);
		for (Item item : items) {
			System.out.println("new Item(" + item.getId() + ", " + item.getCount() + ")//" + item.getDefinition().getName());
		}
	}

	private static int CASKET = 2714;

	private static Item[] rare_rewards = { new Item(10330, 1), new Item(10332, 1), new Item(10334, 1), new Item(10336, 1), new Item(10338, 1), new Item(10340, 1), new Item(10342, 1), new Item(10344, 1), new Item(10346, 1), new Item(10348, 1), new Item(10350, 1), new Item(10352, 1), new Item(2581, 1), new Item(2577, 1) };

	private static Item[] medium_rewards = { new Item(10452, 1), new Item(10454, 1), new Item(10456, 1), new Item(10446, 1), new Item(10448, 1), new Item(10450, 1), new Item(2599, 1), new Item(2601, 1), new Item(2603, 1), new Item(2605, 1), new Item(2607, 1), new Item(2609, 1), new Item(2611, 1), new Item(2613, 1), new Item(3474, 1), new Item(3475, 1), new Item(7319, 1), new Item(7321, 1), new Item(7323, 1), new Item(7325, 1), new Item(7327, 1), new Item(10400, 1), new Item(10402, 1), new Item(10404, 1), new Item(10406, 1), new Item(10408, 1), new Item(10410, 1), new Item(10412, 1), new Item(10414, 1), new Item(10416, 1), new Item(10418, 1), new Item(10420, 1), new Item(10422, 1), new Item(10424, 1), new Item(10426, 1), new Item(10428, 1), new Item(10430, 1), new Item(10432, 1), new Item(10434, 1), new Item(10436, 1), new Item(10438, 1), new Item(7370, 1), new Item(7372, 1), new Item(7374, 1), new Item(7376, 1), new Item(7378, 1), new Item(7380, 1), new Item(7382, 1), new Item(7384, 1), new Item(2577, 1), new Item(2579, 1), new Item(13107, 1), new Item(13109, 1), new Item(13111, 1), new Item(13113, 1), new Item(13115, 1), new Item(13169, 1), new Item(13170, 1), new Item(13171, 1), new Item(13172, 1), new Item(13173, 1), new Item(7112, 1), new Item(7124, 1), new Item(7130, 1), new Item(7136, 1), new Item(13370, 1), new Item(13372, 1), new Item(13374, 1), new Item(986, 6), new Item(988, 6), new Item(13354, 1) };

	private static Item[] low_rewards = { new Item(2595, 1), new Item(2591, 1), new Item(2593, 1), new Item(3473, 1), new Item(2597, 1), new Item(7390, 1), new Item(7392, 1), new Item(7394, 1), new Item(7396, 1), new Item(7386, 1), new Item(7388, 1), new Item(2583, 1), new Item(2585, 1), new Item(2587, 1), new Item(2589, 1), new Item(7362, 1), new Item(7364, 1), new Item(7366, 1), new Item(7368, 1), new Item(10392, 1), new Item(10396, 1), new Item(10392, 1), new Item(10394, 1), new Item(10398, 1), new Item(2633, 1), new Item(2635, 1), new Item(2637, 1), new Item(2631, 1), new Item(10458, 1), new Item(10460, 1), new Item(10462, 1), new Item(10464, 1), new Item(10466, 1), new Item(10468, 1), new Item(10452, 1), new Item(10454, 1), new Item(10456, 1), new Item(986, 3), new Item(988, 3) };

	public static void open_casket(Player player) {
		if (player.getInventory().deleteItem(CASKET)) {
			int medium_chance = NumberUtils.random(100);
			int rare_chance = NumberUtils.random(800);
			int randomized_index = NumberUtils.random(low_rewards.length - 1);
			Item reward = low_rewards[randomized_index];
			if (medium_chance == 1) {
				randomized_index = NumberUtils.random(medium_rewards.length - 1);
				reward = medium_rewards[randomized_index];
			} else if (rare_chance == 1) {
				randomized_index = NumberUtils.random(rare_rewards.length - 1);
				reward = rare_rewards[randomized_index];
			}
			player.getInventory().addItem(reward);
			player.getInventory().refresh();
		}
	}
}
