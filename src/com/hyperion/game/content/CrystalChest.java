package com.hyperion.game.content;

import java.util.ArrayList;
import java.util.List;

import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;
import com.hyperion.utility.Utils;

public class CrystalChest {

    /**
     * The array of rewards the players can receive. The first index is the item
     * id, the second index is the chance out of 100 they have for that reward.
     */
    private static final double[][] REWARDS = new double[][]{

            {6571, 5}, // uncutonyx
            {2572, 10}, // ring of wealth
            //{ 4151, 1 }, //Abyssal whip
            {2581, 2}, //Robin hood hat
            {2577, 2}, //Ranger boots
            {11732, 5}, //Dragon boots
            {4087, 8}, //Dragon platelegs
            //{ 6737, 5 }, //zerk ring
            //{ 6733, 5 }, //archer ring
            //{ 6731, 5 }, //Seers ring
            //{ 6735, 5 }, //warriors ring
            //{ 11128, 1 }, //Zerk necklace
            {7394, 12.5}, // wiz g
            {7386, 12.5}, // wiz g
            {7390, 12.5}, // wiz g
            {7388, 20.5}, // wiz t
            {7392, 20.5}, // wiz t
            {7396, 20.5}, // wiz t
            {2595, 10}, // black g
            {2591, 10}, // black g
            {2593, 10}, // black g
            {2597, 10}, // black g
            {2587, 15}, // black t
            {2583, 15}, // black t
            {2585, 15}, // black t
            {2589, 15}, // black t
            {1731, 50}, {1725, 50}, {1127, 25}, {1093, 25}, {1079, 25}, {1163, 25}, {1201, 25}, {2358, 30}, {1759, 80}, {1249, 25}, {5698, 25}, {1305, 25}, {1149, 25}, {4587, 25}};

    /**
     * Junk rewards that the player can get. The first index is the item id, the
     * second index is the max amount of the item they can receive.
     */
    private static final int[][] JUNK_REWARDS = new int[][]{
            {1632, 3}, // dragon-stones
            {1713, 3}, // Glory
            {2364, 250}, // rune bar
            {995, 500000}, // coins
            {1514, 300}, //Magic logs
            {3143, 100}, //Raw karambwan
    };

    /**
     * Handling the opening of the crystal chest
     *
     * @param player The player
     */
    public static void open(final Player player) {
        if (player.getInventory().deleteItem(989)) {
            int uncutDragChance = NumberUtils.random(2);
            if (uncutDragChance == 1) {
                if (player.getInventory().hasEnoughRoomFor(1631)) {
                    player.getInventory().addDroppable(new Item(1631));
                }
            }
            List<Item> rewards = new ArrayList<Item>();
            for (int i = 0; i < Utils.random(1, 3); i++) {
                double chance = Utils.random(1, 100);
                int index = Utils.random(REWARDS.length);
                if (chance <= REWARDS[index][1]) {
                    rewards.add(new Item((int) REWARDS[index][0], 1));
                }
            }
            int index = Utils.random(JUNK_REWARDS.length);
            int amt = JUNK_REWARDS[index][1];
            rewards.add(new Item(JUNK_REWARDS[index][0], amt == 1 ? amt : Utils.random(1, amt)));
            for (Item item : rewards) {
                player.getInventory().addDroppable(item);
            }
            player.animate(536);
        } else {
            player.getPacketSender().sendChatboxDialogue(true, "The chest appears to be locked..", "Maybe I should get a key?");
        }
    }
}
