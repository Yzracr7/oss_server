package com.hyperion.game.content;

import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;

public class DaggsGate {

	public static void openGate(final Player player, final GameObject object) {
		World.removeObjectTemporary(object, 9);
	}
}
