package com.hyperion.game.content;

import com.hyperion.game.content.clues.ClueScrollManagement;
import com.hyperion.game.world.entity.player.Player;

/**
 * Emotes helper class.
 * @author Graham
 *
 */
public class Emotes {
	
	/**
	 * Handles a player emote: does the appropriate animation.
	 * @param player
	 * @param buttonId
	 * @return
	 */
	public static boolean emote(Player player, int buttonId) {
		if (player.getCombatState().isDead() || player.getAttributes().isSet("stopActions")) {
			return false;
		}
		player.getPacketSender().softCloseInterfaces();
		ClueScrollManagement.confirmPerformance(player, buttonId);
		
		if (buttonId == 1) {
			player.animate(855, 0);
		} else if (buttonId == 2) {
			player.animate(856, 0);
		} else if (buttonId == 3) {
			int bowEmote = 858;
			if (player.getEquipment().hasItem(10396)) {
				bowEmote = 5312;
			}
			player.animate(bowEmote, 0);
		} else if (buttonId == 4) {
			int angryEmote = 859;
			if (player.getEquipment().hasItem(10392)) {
				angryEmote = 5315;
			}
			player.animate(angryEmote, 0);
		} else if (buttonId == 5) {
			player.animate(857, 0);
		} else if (buttonId == 6) {
			player.animate(863, 0);
		} else if (buttonId == 7) {
			player.animate(2113, 0);
		} else if (buttonId == 8) {
			player.animate(862, 0);
		} else if (buttonId == 9) {
			player.animate(864, 0);
		} else if (buttonId == 10) {
			player.animate(861, 0);
		} else if (buttonId == 11) {
			player.animate(2109, 0);
		} else if (buttonId == 12) {
			int yawnEmote = 2111;
			if (player.getEquipment().hasItem(10398)) {
				yawnEmote = 5313;
			}
			player.animate(yawnEmote, 0);
		} else if (buttonId == 13) {
			int danceEmote = 866;
			if (player.getEquipment().hasItem(10394)) {
				danceEmote = 5316;
			}
			player.animate(danceEmote, 0);
		} else if (buttonId == 14) {
			player.animate(2106, 0);
		} else if (buttonId == 15) {
			player.animate(2107, 0);
		} else if (buttonId == 16) {
			player.animate(2108, 0);
		} else if (buttonId == 17) {
			player.animate(860, 0);
		} else if (buttonId == 18) {
			player.animate(0x558, 0);
			player.playGraphic(574, 0);
		} else if (buttonId == 19) {
			player.animate(2105, 0);
		} else if (buttonId == 20) {
			player.animate(2110, 0);
		} else if (buttonId == 21) {
			player.animate(865, 0);
		} else if (buttonId == 22) {
			player.animate(2112, 0);
		} else if (buttonId == 23) {
			player.animate(0x84F, 0);
		} else if (buttonId == 24) {
			player.animate(0x850, 0);
		} else if (buttonId == 25) {
			player.animate(1131, 0);
		} else if (buttonId == 26) {
			player.animate(1130, 0);
		} else if (buttonId == 27) {
			player.animate(1129, 0);
		} else if (buttonId == 28) {
			player.animate(1128, 0);
		} else if (buttonId == 29) {
			player.animate(4275, 0);
		} else if (buttonId == 30) {
			player.animate(1745, 0);
		} else if (buttonId == 31) {
			player.animate(4280, 0);
		} else if (buttonId == 32) {
			player.animate(4276, 0);
		} else if (buttonId == 33) {
			player.animate(3544, 0);
		} else if (buttonId == 34) {
			player.animate(3543, 0);
		} else if (buttonId == 35) {
			player.animate(2836, 0);
		} else if (buttonId == 36) {
			player.animate(6111);
		} else if (buttonId == 37) {
			Skillcape.emote(player);
		} else {
			return false;
		}
		return true;
	}

}