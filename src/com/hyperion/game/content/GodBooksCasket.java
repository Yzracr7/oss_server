package com.hyperion.game.content;

import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.player.Player;

public class GodBooksCasket {
	
	private static int[] books = {3840, 3842, 3844};
	
	public static void openCasket(Player player, int item) {
		player.getAttributes().set("godbookcasket", true);
		player.getPacketSender().modifyText("Pick an Option", 230, 0);
		player.getPacketSender().modifyText("Unholy book", 230, 1);
		player.getPacketSender().modifyText("Holy book", 230, 2);
		player.getPacketSender().modifyText("Book of balance", 230, 3);
		player.getPacketSender().sendChatboxInterface(230);
	}
	
	public static void handleOption(Player player, int option) {
		int index = -1;
		if (option == 1) {
			index = 1;
		} else if (option == 2) {
			index = 0;
		} else if (option == 3) {
			index = 2;
		}
		if (index != -1) {
			player.getInventory().deleteItem(new Item(405, 1));
			player.getInventory().addItem(new Item(books[index], 1));
			player.getInventory().refresh();
		}
		player.getInterfaceSettings().closeInterfaces(true);
	}
}
