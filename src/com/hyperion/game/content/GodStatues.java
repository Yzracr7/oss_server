package com.hyperion.game.content;

import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.game.world.grounditems.GroundItem;
import com.hyperion.game.world.grounditems.GroundItemManager;

public class GodStatues {

    private static int[] capes = {2412, 2413, 2414};

	/*private static int getIndex(int obj) {
		switch (obj) {
		case 2873://sara
			return 1;
		case 2875://guthix
			return 2;
		case 2874://zammy
			return 3;
		}
		return -1;
	}
	
	private static String getName(int index) {
		switch (index) {
		case 1://sara
			return "Saradomin";
		case 2://guthix
			return "Guthix";
		case 3://zammy
			return "Zamorak";
		}
		return "";
	}*/

    private static int getItemId(int obj) {
        switch (obj) {
            case 2875://guthix
                return 2413;
            case 2874://zammy
                return 2414;
            case 2873://sara
                return 2412;
        }
        return -1;
    }

    private static boolean containsGodCape(Player player) {
        for (int i = 0; i < capes.length; i++) {
            if (player.getEquipment().hasItem(capes[i]) ||
                    player.getBank().hasItem(capes[i]) ||
                    player.getInventory().hasItem(capes[i])) {
                return true;
            }
        }
        return false;
    }

    public static void executeChanting(final Player player, final GameObject object) {
        player.getAttributes().set("stopActions", true);
        int oX = object.getLocation().getX();
        int oY = object.getLocation().getY();
        if (!player.getAttributes().isSet("statuewalk")) {
            player.getWalkingQueue().addStep(oX, oY - 2);
            player.getWalkingQueue().finish();
            player.getAttributes().set("statuewalk", true);
            World.getWorld().submit(new Tickable(3) {
                @Override
                public void execute() {
                    this.stop();
                    executeChanting(player, object);
                }
            });
            return;
        }
        player.face(object.getLocation());
        player.animate(645);
        World.getWorld().submit(new Tickable(player, 2) {
            @Override
            public void execute() {
                this.stop();
                executeCapeCheck(player, object);
            }
        });
    }

    private static void executeCapeCheck(Player player, GameObject object) {
        player.getAttributes().remove("statuewalk");
        player.getAttributes().remove("stopActions");
        Location capeDrop = Location.create(object.getLocation().getX(), object.getLocation().getY() - 1, 0);
        if (containsGodCape(player)) {
            player.getPacketSender().sendMessage("...but there is no response.");
            return;
        }
        final int id = getItemId(object.getId());
        GroundItem toDrop = new GroundItem(new Item(id, 1), capeDrop, player);
        GroundItemManager.create(toDrop, player);
        player.getPacketSender().sendStillGraphics(capeDrop, new Graphic(86, 0, 100), 0);
    }
}
