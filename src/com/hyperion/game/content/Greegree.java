package com.hyperion.game.content;

import com.hyperion.cache.definitions.CachedNpcDefinition;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.player.Player;

/**
 * Contains data for using monkey greegrees.
 * @author Konceal/Chex
 *
 */
public class Greegree {
	private static enum MonkeyData {
		SMALL_NINJA(4024, 1480, 1386, 1380, 1381, 1383, -1),
		LARGE_NINJA(4025, 1481, 1386, 1380, 1381, 1383, -1),
		MONKEY_GUARD(4026, 1482, 1401, 1399, 1400, 1402, 1403),
		BEARDED_MONKEY_GUARD(4027, 1483, 1401, 1399, 1400, 1402, 1403),
		BLUE_FACE_MONKEY_GUARD(4028, 1484, 1401, 1399, 1400, 1402, 1403),
		SMALL_ZOMBIE(4029, 1485, 1386, 1382, 1381, 1383,-1),
		LARGE_MONKEY(4030, 1486, 1386, 1382, 1381, 1383, -1),
		KARAMAJA_MONKEY(4031, 1487, 222, 219, 220, 220, 221);
		
		int greegreeID, npcID, standAnim, walkAnim, runAnim, attackAnim, blockAnim;
		
		private MonkeyData(int greegreeID, int npcID, int standAnim, int walkAnim, int runAnim, int attackAnim, int blockAnim) {
			this.greegreeID = greegreeID;
			this.npcID = npcID;
			this.standAnim = standAnim;
			this.walkAnim = walkAnim;
			this.runAnim = runAnim;
			this.attackAnim = attackAnim;
			this.blockAnim = blockAnim;
		}

		public int getGreegreeID() {
			return greegreeID;
		}
		
		public int getNpcID() {
			return npcID;
		}
		
		public int getStandAnim() {
			return standAnim;
		}
		
		public int getWalkAnim() {
			return walkAnim;
		}
		
		public int getRunAnim() {
			return runAnim;
		}
		
		public int getBlockAnim() {
			return blockAnim;
		}

		public int getAttackAnim() {
			return attackAnim;
		}

		public static MonkeyData forId(int id) {
			for (MonkeyData data: MonkeyData.values())
				if (data.greegreeID == id)
					return data;
			return null;
		}

		public static boolean isAnim(int animId) {
			for (MonkeyData data: MonkeyData.values())
				if (data.attackAnim == animId || data.blockAnim == animId)
					return true;
			return false;
		}
	}
	
	//TODO:
	//When player equips a weapon, if its a greegree convert him. If its not a greegree, check if the attribute is 
	//true and if so make it false and reset animations.
	//When player removes weapon, check if attribute is true and if so make it false and resert animations
	
	public static void handleGreegree(Player player, int weaponID, boolean wield) {
		boolean allowed = !(System.currentTimeMillis() - player.getCombatState().getLogoutTimer() < 10000);
		if(player.getDuelSession() != null || player.getAttributes().isSet("clansession") || player.getAttributes().isSet("wildy")) {
			player.getPacketSender().sendMessage("You can't do that right now.");
			return;
		}
		if (wield) {
			if(MonkeyData.forId(weaponID) == null) { //Not a greegree, lets check if we are in monkey mode
				if(player.getAttributes().isSet("isMonkey")) {
					resetAnimations(player); //Not a monkey weapon, but we are a monkey. So we reset animations
					return;
				} else { //not a monkey weapon, we're not monkeys. everything good
					return;
				}
			} else { //Item is greegree
				MonkeyData data = MonkeyData.forId(weaponID);
				if (data != null) {
					if(!allowed) {
						player.getPacketSender().sendMessage("You cannot use your Monkey greegree while in combat!");
						return;
					}
					setAnimations(player, data);
					player.playGraphic(359);
					return;
				} else {
					resetAnimations(player);
				}
			}
		} else { //Unequipping
			if(player.getAttributes().isSet("isMonkey")) {
				resetAnimations(player); //Unequipping
				return;
			}
		}
		return;
	}
	
	private static void setAnimations(Player player, MonkeyData data) {
		if (data.npcID != -1) {
			player.getAttributes().set("isMonkey", true);
			player.getAppearance().setNpcId(data.npcID);
			player.getVariables().setWalkAnimation(CachedNpcDefinition.getNPCDefinitions(data.npcID).walkAnimation);
			player.getVariables().setStandAnimation(CachedNpcDefinition.getNPCDefinitions(data.npcID).idleAnimation);
			player.getVariables().setRunAnimation(data.runAnim);
			player.getVariables().setTurn180Animation(CachedNpcDefinition.getNPCDefinitions(data.npcID).turn180Animation);
			player.getVariables().setTurn90Clockwise(CachedNpcDefinition.getNPCDefinitions(data.npcID).turn90CWAnimation);
			player.getVariables().setTurn90CounterClockwise(CachedNpcDefinition.getNPCDefinitions(data.npcID).turn90CCAnimation);
		
		}
		player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
	}
	
	public static void resetAnimations(Player player) {
		player.getAttributes().remove("isMonkey");
		player.getAppearance().setNpcId(-1);
		player.getVariables().setWalkAnimation(-1);
		player.getVariables().setStandAnimation(-1);
		player.getVariables().setRunAnimation(-1);
		player.getVariables().setTurn180Animation(-1);
		player.getVariables().setTurn90Clockwise(-1);
		player.getVariables().setTurn90CounterClockwise(-1);
		player.playGraphic(359);
		player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
	}
}