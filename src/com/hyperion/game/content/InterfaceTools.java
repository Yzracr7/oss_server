package com.hyperion.game.content;

import com.hyperion.cache.Cache;
import com.hyperion.game.world.entity.player.Player;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 25, 2015
 */
public class InterfaceTools {

	/**
	 * Getting the amount of lines the inter has to write text on
	 * 
	 * @param interfaceId
	 *            The inter id
	 * @return
	 */
	public static int getInterfaceDefinitionsComponentsSize(int interfaceId) {
		return Cache.getCacheFileManagers()[3].getFilesSize(interfaceId);
	}

	/**
	 * Sends the quest scroll with text written over it.
	 * 
	 * @param player
	 *            The player to operate the inter and modifiers to
	 * @param title
	 *            The title to operate on the inter
	 * @param lines
	 *            The lines to operate on the inter
	 */
	public static void sendQuestScroll(Player player, String title, String[] lines) {
		final int interfaceId = 275;
		final int startLine = 4;
		final int endLine = 133;
		final int totalCount = endLine - startLine;
		final int lineCount = lines.length;
		int currentLine = startLine;
		
		if (lineCount > totalCount) {
			System.err.println("Requested " + lineCount + " lines on the quest inter but only have space for " + totalCount);
		}
		
		for (int line = 0; line < getInterfaceDefinitionsComponentsSize(interfaceId); line++) {
			player.getPacketSender().modifyText("", interfaceId, line);
		}
		
		for (String line : lines) {
			player.getPacketSender().modifyText(line, interfaceId, currentLine);
			currentLine++;
		}
		
		player.getPacketSender().modifyText(title, interfaceId, 2);
		player.getPacketSender().displayInterface(interfaceId);
	}
	
	public static class InterfaceComponents {

		public InterfaceComponents(int interfaceId, int childId, String text) {
			this.setInterfaceId(interfaceId);
			this.setChildId(childId);
			this.setText(text);
		}

		/**
		 * @return the childId
		 */
		public int getChildId() {
			return childId;
		}

		/**
		 * @param childId the childId to set
		 */
		public void setChildId(int childId) {
			this.childId = childId;
		}

		/**
		 * @return the text
		 */
		public String getText() {
			return text;
		}

		/**
		 * @param text the text to set
		 */
		public void setText(String text) {
			this.text = text;
		}
		
		/**
		 * @return the interfaceId
		 */
		public int getInterfaceId() {
			return interfaceId;
		}

		/**
		 * @param interfaceId the interfaceId to set
		 */
		public void setInterfaceId(int interfaceId) {
			this.interfaceId = interfaceId;
		}

		private int interfaceId;

		private int childId;

		private String text;
	}

}
