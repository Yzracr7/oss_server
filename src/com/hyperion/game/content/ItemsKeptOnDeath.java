package com.hyperion.game.content;

import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.Container;

public class ItemsKeptOnDeath {

    public static void display(Player p) {
        if (!p.getCombatState().isOutOfCombat("You can't do this during combat!", true)) {
            return;
        }
        if (p.getAttributes().isSet("stopActions")) {
            return;
        }
        int count = 3;
        if (p.getPrayers().isPrayerActive("protect item")) {
            count++;
        }
        if (p.getSettings().isSkulled()) {
            count -= 3;
        }
        Container[] itemsKeptOnDeath = p.getItemsKeptOnDeath();
        Object[] keptItems = new Object[]{-1, -1, "null", 0, 0, (itemsKeptOnDeath[0].getSize() >= 4 && itemsKeptOnDeath[0].get(3) != null) ? itemsKeptOnDeath[0].get(3).getId() : -1,
                (itemsKeptOnDeath[0].getSize() >= 3 && itemsKeptOnDeath[0].get(2) != null) ? itemsKeptOnDeath[0].get(2).getId() : -1,
                (itemsKeptOnDeath[0].getSize() >= 2 && itemsKeptOnDeath[0].get(1) != null) ? itemsKeptOnDeath[0].get(1).getId() : -1,
                (itemsKeptOnDeath[0].getSize() >= 1 && itemsKeptOnDeath[0].get(0) != null) ? itemsKeptOnDeath[0].get(0).getId() : -1, count, 0};
        p.getWalkingQueue().reset();
        p.getPacketSender().clearMapFlag();
        p.getInterfaceSettings().closeInterfaces(true);
        p.getPacketSender().displayInterface(102);
        p.getPacketSender().sendAccessMask(210, 102, 18, 0, 42);
        p.getPacketSender().sendAccessMask(210, 102, 21, 0, 42);
        p.getPacketSender().sendAccessMask(211, 102, 3, 0, 4);
        p.getPacketSender().sendClientScript(118, keptItems, "iiooooiisii");
    }
}
