package com.hyperion.game.content;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;

public class Ladders {

    public static void executeLadder(final Player player, boolean down, final int x, final int y, final int z) {
        player.getAttributes().set("stopActions", true);
        if (!down) {//up
            player.animate(828);
        } else {//down
            player.animate(827);
        }
        World.getWorld().submit(new Tickable(player, 1) {
            @Override
            public void execute() {
                this.stop();
                player.teleport(x, y, z);
                player.getAttributes().remove("stopActions");
            }
        });
    }
}