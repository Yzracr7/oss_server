package com.hyperion.game.content;

import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;

public class LevelUp {
	
	private enum Type {
		AGILITY_LEVEL_UP(16, 157),
		ATTACK_LEVEL_UP(0, 158),
		COOKING_LEVEL_UP(7, 159),
		CRAFTING_LEVEL_UP(12, 160),
		DEFENCE_LEVEL_UP(1, 161),
		FARMING_LEVEL_UP(19, 162),
		FIREMAKING_LEVEL_UP(11, 163),
		FISHING_LEVEL_UP(10, 164),
		FLETCHING_LEVEL_UP(9, 165),
		HERBLORE_LEVEL_UP(15, 166),
		HITPOINT_LEVEL_UP(3, 167),
		MAGIC_LEVEL_UP(6, 168),
		MINING_LEVEL_UP(14, 169),
		PRAYER_LEVEL_UP(5, 170),
		RANGING_LEVEL_UP(4, 171),
		RUNECRAFTING_LEVEL_UP(20, 172),
		SLAYER_LEVEL_UP(18, 173),
		SMITHING_LEVEL_UP(13, 174),
		STRENGTH_LEVEL_UP(2, 175),
		THIEVING_LEVEL_UP(17, 176),
		WOODCUTTING_LEVEL_UP(8, 177),
		;
		
		private int skill;
		private int interfaceId;
		
		public int getSkill() {
			return skill;
		}
		
		public int getInterface() {
			return interfaceId;
		}
		
		private Type(int id, int interfaceId) {
			this.skill = id;
			this.interfaceId = interfaceId;
		}
	}
	
	public static void send(Player player, int skill) {
		Type type = forId(skill);
		if (type == null) {
			return;
		}
		//MainCombat.endCombat(player, 2); // not sure if this is right
		player.playGraphic(199, 2);
		int chatbox = type.getInterface();
		String congrats = "Congratulations, You've just advanced a " + Skills.SKILL_NAME[skill] + " level!</col>";
		String new_level = "You have now reached level " + player.getSkills().getLevelForXp(skill)+".";
		player.getPacketSender().modifyText("<col=0000FF>"+congrats, chatbox, 0);
		player.getPacketSender().modifyText(new_level, chatbox, 1);
		player.getPacketSender().sendChatboxInterface(chatbox);
		player.getAttributes().set("nextDialogue", -1);
		player.getPacketSender().sendMessage(congrats);
		player.getPacketSender().sendMessage(new_level);
		if(player.getSkills().getLevelForXp(skill) >= 99) {
			player.getPacketSender().sendNewsMessage(player.getName().substring(0, 1).toUpperCase() + player.getName().substring(1) +" has reached level " 
		+ player.getSkills().getLevelForXp(skill) + " in " + Skills.SKILL_NAME[skill] + "!");
		}
	}
	
	private static Type forId(int skill) {
		for (Type types : Type.values()) {
			if (types.getSkill() == skill) {
				return types;
			}
		}
		return null;
	}
}
