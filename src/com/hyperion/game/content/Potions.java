package com.hyperion.game.content;

import com.hyperion.game.content.minigames.clanwars.ClanWarsData;
import com.hyperion.game.content.minigames.clanwars.session.WarSession;
import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills.Skill;

public class Potions {

    private static final String[] potionStrings = {
            "Super defence",
            "Super strength",
            "Super attack",
            "Super restore",
            "Ranging potion",
            "Magic potion",
            "Prayer potion",
            "Attack potion",
            "Defence potion",
            "Strength potion",
            "Saradomin brew",
            "Zamorak brew",
            "Antipoison",
            "Superantipoison",
            "Sanfew serum",
            "Super energy",
            "Antifire potion",
            "Extended antifire",
            "Energy potion"
    };

    public static boolean canDrink(Player player, Item item, int slot) {
        if (item == null || item.getDefinition() == null) {
            return false;
        }
        if (player.getCombatState().isDead()) {
            return false;
        }
        if (player.getAttributes().isSet("stopActions")) {
            return false;
        }
        String originalName = item.getDefinition().getName();
        String name = item.getDefinition().getName().replace("(4)", "").replace("(3)", "").replace("(2)", "").replace("(1)", "");
        for (String potions : potionStrings) {

            if (name.contains(potions)) {
                handlePotions(player, potions, originalName, item, slot);
                return true;
            }
        }
        return false;
    }

    private static void handlePotions(final Player player, String potion, String originalName, Item item, int slot) {
        if (player.getVariables().getPotionTimer() > 0) {
            return;
        }
        if (potion.equalsIgnoreCase("Antifire potion") && player.getSettings().isUsingAntifire()) {
            player.getPacketSender().sendMessage("Antifire is already active, no need to drink another one.");
            return;
        }
        if (potion.equalsIgnoreCase("Extended antifire") && player.getSettings().isUsingAntifire()) {
            player.getPacketSender().sendMessage("Antifire is already active, no need to drink another one.");
            return;
        }
        if (player.getDuelSession() != null) {
            if (player.getDuelSession().ruleEnabled(5)) {
                player.getPacketSender().sendMessage("Drinks have been disabled in this duel.");
                return;
            }
        }

        if (player.getAttributes().isSet("clansession")) {
            WarSession session = player.getAttributes().get("clansession");
            if (session != null && session.getRules()[ClanWarsData.Groups.DRINKS.ordinal()] == ClanWarsData.Rules.DRINKS_OFF) {
                player.getPacketSender().sendMessage("Drinks have been disabled in this war.");
                return;
            }
        }
        for (Hit hit : player.getCombatState().getHitQueue()) {
            if (!hit.isPotDelaying()) {
                hit.setPotDelay(true);
                hit.setDelay(hit.getDelay() + 3);
            }
        }
        if (player.getCombatState().getTarget() != null) {
            MainCombat.endCombat(player, 1);
        } else {
            MainCombat.endCombat(player, 0);
        }
        final int delay = 3;
        player.getVariables().setPotionTimer(delay);
        final int toAdd = getNewPotion(item.getDefinition().getName(), item.getId());
        player.getInventory().set(new Item(toAdd, 1), slot, true);
        player.animate(829);
        String potionMsg = getPotionMessage(player, potion);
        final String dosageLeft = getDosageLeft(originalName);
        player.getPacketSender().sendMessage(potionMsg);
        final int[] combatStats = {0, 1, 2, 4, 5, 6};
        switch (potion) {
            case "Super energy":
            case "Energy potion":
                int total = (int) (player.getVariables().getRunningEnergy() + (potion.contains("Super") ? 30 : 10));
                if (total > 100) {
                    total = 100;
                }
                player.getVariables().setRunningEnergy(total, true);
                break;
            case "Sanfew serum":
                if (player.getCombatState().isPoisoned()) {
                    player.getCombatState().setPoisonAmount(0);
                    player.getPacketSender().sendConfig(428, 0);
                }
                for (int cmbStats : combatStats) {
                    if (cmbStats == 5) {
                        int modification = (int) (player.getSkills().getLevelForXp(5) * .33);
                        modification += 1;
                        restoreStat(player, 5, modification);
                    } else {
                        int currentLevel = player.getSkills().getLevel(cmbStats);
                        if (currentLevel > 99)
                            continue;
                        player.getSkills().setLevel(cmbStats, currentLevel + 10 + (int) (Math.floor((double) player.getSkills().getLevelForXp(cmbStats) * .30)));
                        if (player.getSkills().getLevel(cmbStats) > player.getSkills().getLevelForXp(cmbStats)) {
                            player.getSkills().setLevel(cmbStats, player.getSkills().getLevelForXp(cmbStats));
                        }
                        player.getPacketSender().sendSkillLevel(cmbStats);
                    }
                }
                break;
            case "Superantipoison":
                player.getAttributes().set("super_antipoison_active", 1250);
                player.getCombatState().setPoisonAmount(0);
                player.getPacketSender().sendConfig(428, 0);
                break;
            case "Antipoison":
                if (player.getCombatState().isPoisoned()) {
                    player.getCombatState().setPoisonAmount(0);
                    player.getPacketSender().sendConfig(428, 0);
                }
                break;
            case "Strength potion":
            case "Super strength":
                setStat(player, potion, 2);
                break;
            case "Attack potion":
            case "Super attack":
                setStat(player, potion, 0);
                break;
            case "Super defence":
            case "Defence potion":
                setStat(player, potion, 1);
                break;
            case "Ranging potion":
                setStat(player, potion, 4);
                break;
            case "Magic potion":
                setStat(player, potion, 6);
                break;
            case "Prayer potion":
                final int pray = player.getSkills().getLevelForXp(5);
                final int amount = 7 + (int) (Math.floor((double) pray / 4));
                restoreStat(player, 5, amount);
                break;
            case "Super restore":
                for (int cmbStats : combatStats) {
                    if (cmbStats == 5) {
                        int modification = (int) (player.getSkills().getLevelForXp(5) * .33);
                        modification += 1;
                        restoreStat(player, 5, modification);
                    } else {
                        int currentLevel = player.getSkills().getLevel(cmbStats);
                        if (currentLevel > player.getSkills().getLevelForXp(cmbStats))
                            continue;
                        player.getSkills().setLevel(cmbStats, currentLevel + 10 + (int) (Math.floor((double) player.getSkills().getLevelForXp(cmbStats) * .30)));
                        if (player.getSkills().getLevel(cmbStats) > player.getSkills().getLevelForXp(cmbStats)) {
                            player.getSkills().setLevel(cmbStats, player.getSkills().getLevelForXp(cmbStats));
                        }
                        player.getPacketSender().sendSkillLevel(cmbStats);
                    }
                }
                int total1 = (int) (player.getVariables().getRunningEnergy() + 25);
                if (total1 > 100) {
                    total1 = 100;
                }
                player.getVariables().setRunningEnergy(total1, true);
                break;
            case "Antifire potion":
                player.getSettings().setUsingAntifire(true);
                World.getWorld().submit(new Tickable(550) {
                    @Override
                    public void execute() {
                        player.getPacketSender().sendMessage("<col=751dcd>Your resistance to dragonfire is about to run out!</col>");
                        World.getWorld().submit(new Tickable(50) {

                            @Override
                            public void execute() {
                                player.getPacketSender().sendMessage("<col=751dcd>Your resistance to dragonfire has run out.</col>");
                                player.getSettings().setUsingAntifire(false);
                                stop();
                            }

                        });
                        stop();
                    }

                });
                break;
            case "Extended antifire":
                player.getSettings().setUsingSuperAntifire(true);
                World.getWorld().submit(new Tickable(600) {

                    @Override
                    public void execute() {
                        player.getPacketSender().sendMessage("<col=751dcd>Your resistance to dragonfire has run out.</col>");
                        player.getSettings().setUsingAntifire(false);
                        stop();
                    }

                });
                break;
            case "Zamorak brew":
                final Skill[] skillDecrease = {Skill.DEFENCE, Skill.HITPOINTS};
                for (Skill skill : skillDecrease) {
                    int newLevel = (int) (player.getSkills().getLevel(skill.getId()) * 0.9) + 2;
                    int amountToAdd = newLevel - player.getSkills().getLevel(skill.getId());
                    player.getSkills().setLevel(skill.getId(), player.getSkills().getLevel(skill.getId()) + amountToAdd);
                    if (player.getSkills().getLevel(skill.getId()) > player.getSkills().getLevelForXp(skill.getId()) + amountToAdd) {
                        player.getSkills().setLevel(skill.getId(), player.getSkills().getLevelForXp(skill.getId()) + amountToAdd);
                    }
                    player.getPacketSender().sendSkillLevel(skill.getId());
                }
                int amountToAdd1 = (int) (player.getSkills().getLevel(Skill.ATTACK.getId()) * 0.2) + 2;
                int amountToAdd2 = (int) (player.getSkills().getLevel(Skill.STRENGTH.getId()) * 0.12) + 2;
                int amountToAdd3 = (int) (player.getSkills().getLevel(Skill.PRAYER.getId()) * 0.10);
                player.getSkills().setLevel(Skill.ATTACK.getId(), player.getSkills().getLevel(Skill.ATTACK.getId()) + amountToAdd1);
                if (player.getSkills().getLevel(Skill.ATTACK.getId()) > player.getSkills().getLevelForXp(Skill.ATTACK.getId()) + amountToAdd1) {
                    player.getSkills().setLevel(Skill.ATTACK.getId(), player.getSkills().getLevelForXp(Skill.ATTACK.getId()) + amountToAdd1);
                }
                player.getPacketSender().sendSkillLevel(Skill.ATTACK.getId());
                player.getSkills().setLevel(Skill.STRENGTH.getId(), player.getSkills().getLevel(Skill.STRENGTH.getId()) + amountToAdd2);
                if (player.getSkills().getLevel(Skill.STRENGTH.getId()) > player.getSkills().getLevelForXp(Skill.STRENGTH.getId()) + amountToAdd2) {
                    player.getSkills().setLevel(Skill.STRENGTH.getId(), player.getSkills().getLevelForXp(Skill.STRENGTH.getId()) + amountToAdd2);
                }
                player.getPacketSender().sendSkillLevel(Skill.STRENGTH.getId());
                player.getSkills().setLevel(Skill.PRAYER.getId(), player.getSkills().getLevel(Skill.PRAYER.getId()) + amountToAdd3);
                if (player.getSkills().getLevel(Skill.PRAYER.getId()) > player.getSkills().getLevelForXp(Skill.PRAYER.getId()) + amountToAdd3) {
                    player.getSkills().setLevel(Skill.PRAYER.getId(), player.getSkills().getLevelForXp(Skill.PRAYER.getId()) + amountToAdd3);
                }
                player.getPacketSender().sendSkillLevel(Skill.PRAYER.getId());


                break;
            case "Saradomin brew":
                final int[] toDecrease = {0, 2, 4, 6};
                final int[] toIncrease = {1, 3};
                for (int skill : toDecrease) {
                    int currentLevel = player.getSkills().getLevel(skill);
                    player.getSkills().setLevel(skill, (int) (currentLevel - Math.floor((double) player.getSkills().getLevelForXp(skill) * .10)));
                    if (player.getSkills().getLevel(skill) < 0) {
                        player.getSkills().setLevel(skill, 1);
                    }
                    player.getPacketSender().sendSkillLevel(skill);
                }
                for (int skill : toIncrease) {
                    if (skill == 3) {
                        final int amountToAdd = (2 + (int) Math.floor((double) player.getSkills().getLevelForXp(skill) * .15));
                        player.getSkills().setLevel(skill, player.getSkills().getLevel(skill) + amountToAdd);
                        if (player.getSkills().getLevel(skill) > (int) Math.floor((double) player.getSkills().getLevelForXp(skill) * 1.17)) {
                            player.getSkills().setLevel(skill, (int) Math.floor((double) player.getSkills().getLevelForXp(skill) * 1.17));
                        }
                        player.getPacketSender().sendSkillLevel(skill);
                    } else {
                        final int amountToAdd = (int) Math.floor((double) player.getSkills().getLevelForXp(skill) * .25);
                        player.getSkills().setLevel(skill, player.getSkills().getLevel(skill) + amountToAdd);
                        if (player.getSkills().getLevel(skill) > (int) Math.floor((double) player.getSkills().getLevelForXp(skill) * 1.25)) {
                            player.getSkills().setLevel(skill, (int) Math.floor((double) player.getSkills().getLevelForXp(skill) * 1.25));
                        }
                        player.getPacketSender().sendSkillLevel(skill);
                    }
                }
                break;
        }
        World.getWorld().submit(new Tickable(2) {
            @Override
            public void execute() {
                player.getPacketSender().sendMessage(dosageLeft);
                this.stop();
            }
        });
    }

    private static void setStat(Player player, String potion, int skill) {
        int amountToAdd = getPotionBoost(player, potion.toLowerCase(), skill);
        player.getSkills().setLevel(skill, player.getSkills().getLevel(skill) + amountToAdd);
        if (player.getSkills().getLevel(skill) > player.getSkills().getLevelForXp(skill) + amountToAdd) {
            player.getSkills().setLevel(skill, player.getSkills().getLevelForXp(skill) + amountToAdd);
        }
        player.getPacketSender().sendSkillLevel(skill);
    }

    private static void restoreStat(Player player, int skill, int amount) {
        player.getSkills().setLevel(skill, player.getSkills().getLevel(skill) + amount);
        if (player.getSkills().getLevel(skill) > player.getSkills().getLevelForXp(skill)) {
            player.getSkills().setLevel(skill, player.getSkills().getLevelForXp(skill));
        }
        player.getPacketSender().sendSkillLevel(skill);
    }

    private static int getNewPotion(String name, int id) {
        switch (id) {
            case 2428://Attack Potion
                return 121;
            case 2430://Restore Potion
                return 127;
            case 2432://Defence Potion
                return 133;
            case 2434://Prayer Potion
                return 139;
            case 15308:
                return 15309;
            case 2436://Super Attack
                return 145;
            case 2438://Fishing Potion
                return 151;
            case 2440://Super Strength
                return 157;
            case 2442://Super Defence
                return 163;
            case 2444://Ranging Potion
                return 169;
            case 2446://Antiposion
                return 175;
            case 2448://Super Antiposion
                return 181;
            case 2450://Zamorak Brew
                return 189;
        }
        if (name.endsWith("(1)"))
            return 229;
        else
            return id + 2;
    }

    private static int getPotionBoost(Player player, String potion, int skillId) {
        final int skill = player.getSkills().getLevelForXp(skillId);
        if (potion.contains("magic"))
            return 5;
        if (potion.contains("prayer"))
            return 7 + (int) (Math.floor((double) skill / 4));
        if (potion.contains("ranging"))
            return 4 + (int) (Math.floor((double) skill * .10));


        //        if(potion.contains("extreme"))
//            return 5 + (int)(Math.floor((double)skill * .));
        if (!potion.contains("super"))
            return 3 + (int) (Math.floor((double) skill * .10));
        else
            return 5 + (int) (Math.floor((double) skill * .15));
    }

    private static String getPotionMessage(Player player, String potion) {
        String name = potion.toLowerCase().replace("potion", "");
        name = name.trim();
        if (name.equalsIgnoreCase("prayer")) {
            name = "restore prayer";
        } else if (name.equalsIgnoreCase("saradomin brew")) {
            return "You drink some of the foul liquid.";
        }
        //System.out.println(name);
        return "You drink some of your " + name + " potion.";
    }

    private static String getDosageLeft(String original_name) {
        if (original_name.endsWith("(4)")) {
            return "You have 3 doses of potion left.";
        } else if (original_name.endsWith("(3)")) {
            return "You have 2 doses of potion left.";
        } else if (original_name.endsWith("(2)")) {
            return "You have 1 dose of potion left.";
        }
        return "You have finished your potion.";
    }
}
