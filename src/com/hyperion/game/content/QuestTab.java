package com.hyperion.game.content;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.hyperion.game.content.achievements.*;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.utility.TextUtils;


public class QuestTab {

	private static final int QUEST_TAB_ID = 274;
	private static final int QUEST_LOG_ID = 275;
	private static final int QUEST_COMPLETE = 276;
	
	private static final DecimalFormat df = new DecimalFormat("#.##");
	
	private static String getKDR(Player player) {
		if(player.getVariables().getKills() == 0) 
			return "0.00";
		else if (player.getVariables().getDeaths() == 0)
			return player.getVariables().getKills() + ".00";
		return "" + df.format(((double)player.getVariables().getKills() / (double)player.getVariables().getDeaths()));
	}

	public static void refresh(Player player) {
		int tab = player.getAttributes().getInt("questTab");
		if (tab == -1) { // Not yet set
			tab = 0; // Player tab
		}

		if (tab == 0)
			showPlayerTab(player);
		else if (tab == 1)
			showQuestTab(player);
		else if (tab == 2)
			showAchievementTab(player);
	}

	public static void showPlayerTab(Player player) {
		List<String> text = new ArrayList<>();
	}

	public static void showQuestTab(Player player) {
		List<String> text = new ArrayList<>();
	}

	private static void showAchievementTab(Player player) {
		List<String> text = new ArrayList<>();
	}

	public static void openQuestInterface(Player player, int i) {
		if (i > 7)
			return;
		
		int start = 0;
		String stripe, req1, req2, req3, req4, req5, req6;
		switch (i) {
		case 0: // Mage Arena
			stripe = player.getVariables().finishedMageArena ? "<str>" : "";
			req1 = player.getSkills().getLevelForXp(Skills.MAGIC) >= 60 ? "<str>" : "";

			player.getPacketSender().modifyText("<col=8B0000>Mage Arena</col>", QUEST_LOG_ID, 2);
			player.getPacketSender().modifyText(stripe + "<col=000080>I can speak this mini-quest by speaking to <col=8B0000>Kolodion</col> <col=000080>at", QUEST_LOG_ID, 4);
			player.getPacketSender().modifyText(stripe + "<col=000080>the <col=8B0000>Mage Arena</col><col=000080>, which is located in the <col=8B0000>deep wilderness</col><col=000080>. ", QUEST_LOG_ID, 5);
			player.getPacketSender().modifyText("", QUEST_LOG_ID, 6);
			player.getPacketSender().modifyText(stripe + "<col=000080>To complete this mini-quest, I need the following skills:", QUEST_LOG_ID, 7);
			player.getPacketSender().modifyText(stripe + req1 + "<col=8B0000>60 Magic", QUEST_LOG_ID, 8);
			player.getPacketSender().modifyText("", QUEST_LOG_ID, 9);
			player.getPacketSender().modifyText(stripe == "<str>" ? "<col=000080>I have completed this mini-quest! I can now use <col=8B0000>God spells</col>" : "", QUEST_LOG_ID, 10);
			player.getPacketSender().modifyText(stripe == "<str>" ? "<col=000080>in the <col=8B0000>Modern Spellbook</col><col=000080>." : "", QUEST_LOG_ID, 11);
			start = 12;
			break;
		case 1: // RFD
			stripe = player.getVariables().finishedRfd == true ? "<str>" : "";
			req1 = player.getSkills().getCombatLevel() >= 60 ? "<str>" : "";
			req2 = player.getSkills().getLevelForXp(Skills.MAGIC) >= 59 ? "<str>" : "";
			req3 = player.getSkills().getLevelForXp(Skills.THIEVING) >= 53 ? "<str>" : "";
			req4 = player.getSkills().getLevelForXp(Skills.COOKING) >= 70 ? "<str>" : "";
			req5 = player.getSkills().getLevelForXp(Skills.HERBLORE) >= 45 ? "<str>" : "";
			req6 = player.getVariables().getQuestPoints() >= 2 ? "<str>" : "";
			player.getPacketSender().modifyText("<col=8B0000>Recipe for Disaster</col>", QUEST_LOG_ID, 2);
			player.getPacketSender().modifyText(stripe + "<col=000080>I can speak this quest by speaking to <col=8B0000>Gypsy</col> <col=000080>at", QUEST_LOG_ID, 4);
			player.getPacketSender().modifyText(stripe + "<col=000080><col=8B0000>the Lumbridge Castle.</col><col=000080>", QUEST_LOG_ID, 5);
			player.getPacketSender().modifyText("", QUEST_LOG_ID, 6);
			player.getPacketSender().modifyText(stripe + "<col=000080>To complete this quest, I need the following skills:", QUEST_LOG_ID, 7);
			player.getPacketSender().modifyText(stripe + req1 + "<col=8B0000>Combat Level 60+", QUEST_LOG_ID, 8);
			player.getPacketSender().modifyText(stripe + req2 + "<col=8B0000>59 Magic", QUEST_LOG_ID, 9);
			player.getPacketSender().modifyText(stripe + req3 + "<col=8B0000>53 Thieving", QUEST_LOG_ID, 10);
			player.getPacketSender().modifyText(stripe + req4 + "<col=8B0000>70 Cooking", QUEST_LOG_ID, 11);
			player.getPacketSender().modifyText(stripe + req5 + "<col=8B0000>45 Herblore", QUEST_LOG_ID, 12);
			player.getPacketSender().modifyText(stripe + req6 + "<col=8B0000>2 Quest Points", QUEST_LOG_ID, 13);
			// TODO: stripe out requirements if reached
			player.getPacketSender().modifyText("", QUEST_LOG_ID, 14);
			player.getPacketSender().modifyText(stripe == "<str>" ? "<col=000080>I have completed this quest! I can now use purchase" : "", QUEST_LOG_ID, 15);
			player.getPacketSender().modifyText(stripe == "<str>" ? "<col=000080>purchase <col=8B0000>Gloves</col> from the <col=8B0000>Chest</col><col=000080>!" : "", QUEST_LOG_ID, 16);
			start = 17;
			break;
		case 2: // Lunar Diplomacy
			stripe = player.getVariables().finishedLunars == true ? "<str>" : "";

			req1 = player.getSkills().getCombatLevel() >= 60 ? "<str>" : "";
			req2 = player.getSkills().getLevelForXp(Skills.MAGIC) >= 65 ? "<str>" : "";
			req3 = player.getSkills().getLevelForXp(Skills.DEFENCE) >= 40 ? "<str>" : "";

			player.getPacketSender().modifyText("<col=8B0000>Lunar Diplomacy</col>", QUEST_LOG_ID, 2);
			player.getPacketSender().modifyText(stripe + "<col=000080>I can speak this quest by speaking to <col=8B0000>Oneiromancer</col> <col=000080>at", QUEST_LOG_ID, 4);
			player.getPacketSender().modifyText(stripe + "<col=000080>the <col=8B0000>Watch Tower</col><col=000080>, which is accessible in your <col=8B0000>modern spellbook</col><col=000080>. ", QUEST_LOG_ID, 5);
			player.getPacketSender().modifyText("", QUEST_LOG_ID, 6);
			player.getPacketSender().modifyText(stripe + "<col=000080>To complete this quest, I need the following skills:", QUEST_LOG_ID, 7);
			player.getPacketSender().modifyText(stripe + req1 + "<col=8B0000>Combat Level 60+", QUEST_LOG_ID, 8);
			player.getPacketSender().modifyText(stripe + req2 + "<col=8B0000>65 Magic", QUEST_LOG_ID, 9);
			player.getPacketSender().modifyText(stripe + req3 + "<col=8B0000>40 Defence", QUEST_LOG_ID, 10);
			// TODO: stripe out requirements if reached
			player.getPacketSender().modifyText("", QUEST_LOG_ID, 11);
			player.getPacketSender().modifyText(stripe == "<str>" ? "<col=000080>I have completed this quest! I can now use <col=8B0000>Lunar Spells</col><col=000080> and <col=8B0000>Lunar Equipment</col>" : "", QUEST_LOG_ID, 12);
			player.getPacketSender().modifyText(stripe == "<str>" ? "<col=000080>by switching to the <col=8B0000>Lunar Spellbook</col><col=000080>." : "", QUEST_LOG_ID, 13);
			start = 14;
			break;
		case 3: // Mountain Daughter
			stripe = player.getVariables().isKendal_killed() == true ? "<str>" : "";

			player.getPacketSender().modifyText("<col=8B0000>Mountain Daughter</col>", QUEST_LOG_ID, 2);
			player.getPacketSender().modifyText(stripe + "<col=000080>I can speak this quest by speaking to <col=8B0000>Hamal the Chieftain</col>", QUEST_LOG_ID, 4);
			player.getPacketSender().modifyText(stripe + "<col=000080>at the <col=8B0000>Draynor Village</col><col=000080>, which is accessible with a <col=8B0000>Amulet of DummyDialogue</col> ", QUEST_LOG_ID, 5);
			player.getPacketSender().modifyText("", QUEST_LOG_ID, 6);
			player.getPacketSender().modifyText(stripe + "<col=000080>To complete this quest, I need the following skills:", QUEST_LOG_ID, 7);
			player.getPacketSender().modifyText(stripe + "<col=8B0000>None", QUEST_LOG_ID, 8);
			player.getPacketSender().modifyText("", QUEST_LOG_ID, 9);
			player.getPacketSender().modifyText(stripe == "<str>" ? "<col=000080>I have completed this quest! I received a <col=8B0000>Bearhead</col>" : "", QUEST_LOG_ID, 10);
			player.getPacketSender().modifyText(stripe == "<str>" ? "<col=000080>which I can retrieve from <col=8B0000>Hamal the Chieftain</col><col=000080> if lost." : "", QUEST_LOG_ID, 11);
			start = 12;
			break;
		case 4: // Desert Treasure
			stripe = player.getVariables().isDtFinished() == true ? "<str>" : "";

			req1 = player.getSkills().getLevelForXp(Skills.MAGIC) >= 49 ? "<str>" : "";
			req2 = player.getSkills().getLevelForXp(Skills.THIEVING) >= 53 ? "<str>" : "";

			player.getPacketSender().modifyText("<col=8B0000>Desert Treasure</col>", QUEST_LOG_ID, 2);
			player.getPacketSender().modifyText(stripe + "<col=000080>I can speak this quest by speaking to <col=8B0000>Eblis</col>", QUEST_LOG_ID, 4);
			player.getPacketSender().modifyText(stripe + "<col=000080>at the <col=8B0000>Duel Arena</col><col=000080> altar room.", QUEST_LOG_ID, 5);
			player.getPacketSender().modifyText("", QUEST_LOG_ID, 6);
			player.getPacketSender().modifyText(stripe + "<col=000080>To complete this quest, I need the following skills:", QUEST_LOG_ID, 7);
			player.getPacketSender().modifyText(stripe + req1 + "<col=8B0000>49 Magic", QUEST_LOG_ID, 8);
			player.getPacketSender().modifyText(stripe + req2 + "<col=8B0000>53 Thieving", QUEST_LOG_ID, 9);
			player.getPacketSender().modifyText("", QUEST_LOG_ID, 10);
			player.getPacketSender().modifyText(stripe == "<str>" ? "<col=000080>I have completed this quest! I can now use <col=8B0000>Ancient Magicks</col>" : "", QUEST_LOG_ID, 11);
			player.getPacketSender().modifyText(stripe == "<str>" ? "<col=000080>by switching to the <col=8B0000>Ancient Spellbook</col><col=000080>, and can purchase" : "", QUEST_LOG_ID, 12);
			player.getPacketSender().modifyText(stripe == "<str>" ? "<col=8B0000>Ghostly Robes</col><col=000080> from <col=8B0000>Eblis</col><col=000080>." : "", QUEST_LOG_ID, 13);
			start = 14;
			break;
		case 5: // Monkey Madness
			stripe = player.getVariables().isDemon_killed() == true ? "<str>" : "";

			req1 = player.getSkills().getLevelForXp(Skills.ATTACK) >= 60 ? "<str>" : "";

			player.getPacketSender().modifyText("<col=8B0000>Monkey Madness</col>", QUEST_LOG_ID, 2);
			player.getPacketSender().modifyText(stripe + "<col=000080>I can speak this quest by speaking to <col=8B0000>King Narnode</col>", QUEST_LOG_ID, 4);
			player.getPacketSender().modifyText(stripe + "<col=000080>at the <col=8B0000>Grand Tree</col><col=000080>, which I can reach by speaking to the <col=8B0000>Sailor</col><col=000080>. ", QUEST_LOG_ID, 5);
			player.getPacketSender().modifyText("", QUEST_LOG_ID, 6);
			player.getPacketSender().modifyText(stripe + "<col=000080>To complete this quest, I need the following skills:", QUEST_LOG_ID, 7);
			player.getPacketSender().modifyText(stripe + req1 + "<col=8B0000>60 Attack", QUEST_LOG_ID, 8);
			player.getPacketSender().modifyText("", QUEST_LOG_ID, 9);
			player.getPacketSender().modifyText(stripe == "<str>" ? "<col=000080>I have completed this quest! I can now use <col=8B0000>Dragon Scimitars</col> <col=000080>and" : "", QUEST_LOG_ID, 10);
			player.getPacketSender().modifyText(stripe == "<str>" ? "<col=8B0000>Monkey Greegrees</col> <col=000080>which I can purchase in the <col=8B0000>Tree Gnome Shop</col><col=000080>." : "", QUEST_LOG_ID, 11);
			start = 12;
			break;
		case 6: // HFD
			stripe = player.getVariables().isMother_killed() == true ? "<str>" : "";

			req1 = player.getSkills().getLevelForXp(Skills.AGILITY) >= 35 ? "<str>" : "";

			player.getPacketSender().modifyText("<col=8B0000>Horror From The Deep</col>", QUEST_LOG_ID, 2);
			player.getPacketSender().modifyText(stripe + "<col=000080>I can speak this quest by speaking to <col=8B0000>Jossik</col>", QUEST_LOG_ID, 4);
			player.getPacketSender().modifyText(stripe + "<col=000080>at <col=8B0000>Relleka</col><col=000080>, which I can reach by speaking to the <col=8B0000>Sailor</col><col=000080>. ", QUEST_LOG_ID, 5);
			player.getPacketSender().modifyText("", QUEST_LOG_ID, 6);
			player.getPacketSender().modifyText(stripe + "<col=000080>To complete this quest, I need the following skills:", QUEST_LOG_ID, 7);
			player.getPacketSender().modifyText(stripe + req1 + "<col=8B0000>35 Agility", QUEST_LOG_ID, 8);
			player.getPacketSender().modifyText("", QUEST_LOG_ID, 9);
			player.getPacketSender().modifyText(stripe == "<str>" ? "<col=000080>I have completed this quest! I can now purchase <col=8B0000>Holy Books</col>" : "", QUEST_LOG_ID, 10);
			player.getPacketSender().modifyText(stripe == "<str>" ? "<col=000080>by speaking to <col=8B0000>Jossik</col><col=000080> again." : "", QUEST_LOG_ID, 11);
			start = 11;
			break;
		case 7:
			stripe = player.getVariables().isTasFinished == true ? "<str>" : "";

			req1 = player.getSkills().getLevelForXp(Skills.PRAYER) >= 50 ? "<str>" : "";
			req2 = player.getVariables().isDtFinished() == true ? "<str>" : "";
			req3 = Achievements.getTotalAchieved(player) >= 1 ? "<str>": "";

			player.getPacketSender().modifyText("<col=8B0000>Temple At Senntisten</col>", QUEST_LOG_ID, 2);
			player.getPacketSender().modifyText(stripe + "<col=000080>I can speak this quest by speaking to <col=8B0000>Noterazzo</col>", QUEST_LOG_ID, 4);
			player.getPacketSender().modifyText(stripe + "<col=000080>at <col=8B0000>Barrows</col><col=000080>, which I can reach by speaking to the <col=8B0000>Sailor</col><col=000080>. ", QUEST_LOG_ID, 5);
			player.getPacketSender().modifyText("", QUEST_LOG_ID, 6);
			player.getPacketSender().modifyText(stripe + "<col=000080>To complete this quest, I need the following requirments:", QUEST_LOG_ID, 7);
			player.getPacketSender().modifyText(stripe + req1 + "<col=8B0000>50 Prayer", QUEST_LOG_ID, 8);
			player.getPacketSender().modifyText(stripe + req2 + "<col=8B0000>Must have completed Deserts Treasure", QUEST_LOG_ID, 9);
			player.getPacketSender().modifyText(stripe + req3 + "<col=8B0000>Must have completed 15 Achievements", QUEST_LOG_ID, 10);
			player.getPacketSender().modifyText("", QUEST_LOG_ID, 11);
			player.getPacketSender().modifyText(stripe == "<str>" ? "<col=000080>I have completed this quest! I can now switch to <col=8B0000>Curses</col>" : "", QUEST_LOG_ID, 12);
			player.getPacketSender().modifyText(stripe == "<str>" ? "<col=000080>by speaking to <col=8B0000>Noterazzo</col><col=000080> again." : "", QUEST_LOG_ID, 13);
			start = 11;
		break;
		}
		for (int x = start; x < 134; x++)
			player.getPacketSender().modifyText("", QUEST_LOG_ID, x);
		
		player.getPacketSender().displayInterface(QUEST_LOG_ID);
	}

	public static void openQuestComplete(Player player, int i) {
		switch (i) {
		case 0: // mage arena
			player.getPacketSender().itemOnInterface(QUEST_COMPLETE, 3, 200, 2417);
			player.getPacketSender().modifyText("<col=857B5D>You have completed Mage Arena!<br><br>" + "<col=857B5D>You are awarded:<br><col=857B5D>1 Quest Point<br><col=857B5D>Access to God spells<br><col=857B5D>Access to God capes", QUEST_COMPLETE, 2);

			player.getPacketSender().modifyText("<col=FFB112>Quest Points:", QUEST_COMPLETE, 4);
			player.getPacketSender().modifyText("<col=FFB112>" + player.getVariables().getQuestPoints(), QUEST_COMPLETE, 5);

			break;
		case 1: // RFD
			player.getPacketSender().itemOnInterface(QUEST_COMPLETE, 3, 200, 1949);
			player.getPacketSender().modifyText("<col=857B5D>You have completed Recipe for Disaster!<br><br>" + "<col=857B5D>You are awarded:<br><col=857B5D>1 Quest Point<br><col=857B5D>Access to Gloves from the Chest", QUEST_COMPLETE, 2);

			player.getPacketSender().modifyText("<col=FFB112>Quest Points:", QUEST_COMPLETE, 4);
			player.getPacketSender().modifyText("<col=FFB112>" + player.getVariables().getQuestPoints(), QUEST_COMPLETE, 5);

			break;

		case 2: // Lunar Diplomacy
			player.getPacketSender().itemOnInterface(QUEST_COMPLETE, 3, 200, 9084);
			player.getPacketSender().modifyText("<col=857B5D>You have completed Lunar Diplomacy!<br><br>" + "<col=857B5D>You are awarded:<br><col=857B5D>1 Quest Point<br><col=857B5D>Access to Lunar Spells<br><col=857B5D>Access to Lunar Equipment", QUEST_COMPLETE, 2);
			
			player.getPacketSender().modifyText("<col=FFB112>Quest Points:", QUEST_COMPLETE, 4);
			player.getPacketSender().modifyText("<col=FFB112>" + player.getVariables().getQuestPoints(), QUEST_COMPLETE, 5);

			break;
		case 3: // Mountain Daughter
			player.getPacketSender().itemOnInterface(QUEST_COMPLETE, 3, 200, 4502);
			player.getPacketSender().modifyText("<col=857B5D>You have completed Mountain Daughter!<br><br>" + "<col=857B5D>You are awarded:<br><col=857B5D>1 Quest Point<br><col=857B5D>Bearhead", QUEST_COMPLETE, 2);

			player.getPacketSender().modifyText("<col=FFB112>Quest Points:", QUEST_COMPLETE, 4);
			player.getPacketSender().modifyText("<col=FFB112>" + player.getVariables().getQuestPoints(), QUEST_COMPLETE, 5);

			break;

		case 4: // Desert Treasure
			player.getPacketSender().itemOnInterface(QUEST_COMPLETE, 3, 200, 4675);
			player.getPacketSender().modifyText("<col=857B5D>You have completed Desert Treasure!<br><br>" + "<col=857B5D>You are awarded:<br><col=857B5D>1 Quest Point<br><col=857B5D>Access to Ancient Spells<br><col=857B5D>Access to Ghostly Robes", QUEST_COMPLETE, 2);

			player.getPacketSender().modifyText("<col=FFB112>Quest Points:", QUEST_COMPLETE, 4);
			player.getPacketSender().modifyText("<col=FFB112>" + player.getVariables().getQuestPoints(), QUEST_COMPLETE, 5);

			break;
		case 5: // Monkey Madness
			// TODO: GREE GREE
			player.getPacketSender().itemOnInterface(QUEST_COMPLETE, 3, 200, 4024);
			player.getPacketSender().modifyText("<col=857B5D>You have completed Monkey Madness!<br><br>" + "<col=857B5D>You are awarded:<br><col=857B5D>1 Quest Point<br><col=857B5D>Access to Dragon Scimitars<br><col=857B5D>Access to Monkey Greegrees", QUEST_COMPLETE, 2);

			player.getPacketSender().modifyText("<col=FFB112>Quest Points:", QUEST_COMPLETE, 4);
			player.getPacketSender().modifyText("<col=FFB112>" + player.getVariables().getQuestPoints(), QUEST_COMPLETE, 5);

			break;
		case 6: // HFD
			player.getPacketSender().itemOnInterface(QUEST_COMPLETE, 3, 200, 3840);
			player.getPacketSender().modifyText("<col=857B5D>You have completed Horror From The Deep!<br><br>" + "<col=857B5D>You are awarded:<br><col=857B5D>1 Quest Point<br><col=857B5D>Access to Holy Books", QUEST_COMPLETE, 2);

			player.getPacketSender().modifyText("<col=FFB112>Quest Points:", QUEST_COMPLETE, 4);
			player.getPacketSender().modifyText("<col=FFB112>" + player.getVariables().getQuestPoints(), QUEST_COMPLETE, 5);

			break;
		case 7: /* TAS */
			player.getPacketSender().itemOnInterface(QUEST_COMPLETE, 3, 200, 15378);
			player.getPacketSender().modifyText("<col=857B5D>You have completed Temple At Senntisten!<br><br>" + "<col=857B5D>You are awarded:<br><col=857B5D>1 Quest Point<br><col=857B5D>Access to curses", QUEST_COMPLETE, 2);
			
			player.getPacketSender().modifyText("<col=FFB112>Quest Points:", QUEST_COMPLETE, 4);
			player.getPacketSender().modifyText("<col=FFB112>" + player.getVariables().getQuestPoints(), QUEST_COMPLETE, 5);
			break;
		}
		player.getPacketSender().modifyText("", QUEST_COMPLETE, 7);
		player.getPacketSender().modifyText("", QUEST_COMPLETE, 8);
		player.getPacketSender().modifyText("", QUEST_COMPLETE, 9);
		player.getPacketSender().displayInterface(QUEST_COMPLETE);
	}
}
