package com.hyperion.game.content;

import com.hyperion.game.world.entity.player.Player;

public class Scoreboard {

    public static void openPVPScoreboard(Player player) {

        player.getPacketSender().modifyText("Wilderness Kills", 108, 91); // Title
        player.getPacketSender().modifyText("Latest Activity", 108, 90);

        player.getPacketSender().modifyText("Snowman has killed frosty", 108, 93); // Line 1
        player.getPacketSender().modifyText("nh nightmare has killed 1d00d", 108, 88); // Line 2

        for (int i = 94; i < 141; i++) {
            player.getPacketSender().modifyText("", 108, i);
        }
        player.getPacketSender().sendInterface(108, false);
    }

    public static void openDuelingScoreboard() {

    }
}
