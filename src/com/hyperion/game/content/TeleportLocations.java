package com.hyperion.game.content;

import com.hyperion.game.world.Location;
import com.hyperion.utility.NumberUtils;

public class TeleportLocations {

    /* NORMAL SPELL BOOK */
    public static final Location HOME_LOCATION = Location.create(3222, 3218, 0);
    public static final Location VARROCK = Location.create(3213, 3423, 0);
    public static final Location LUMBRIDGE = Location.create(3222, 3218, 0);
    public static final Location FALADOR = Location.create(2965, 3379, 0);
    public static final Location CAMELOT = Location.create(2757, 3478, 0);
    public static final Location EAST_ARDOUGNE = Location.create(2662, 3306, 0);
    public static final Location WATCHTOWER = Location.create(2544, 3115, 2);
    public static final Location TROLLHEIM = Location.create(2892, 3679, 0);
    public static final Location APE_ATOLL = Location.create(2785, 2786, 0);
    public static final Location EDGEVILLE = Location.create(3087, 3496, 0);
    public static final Location YANILLE = Location.create(2575, 3090, 0);

    /* ANCIENTS SPELL BOOK */
    public static final Location PADDEWWA = Location.create(3098, 9882, 0);
    public static final Location SENNTISTEN = Location.create(3320, 3338, 0);
    public static final Location KHARYLL = Location.create(3493, 3472, 0);
    public static final Location LASSAR = Location.create(3003, 3470, 0);
    public static final Location DEREEYAK = Location.create(2966, 3696, 0);
    public static final Location CARRALANGER = Location.create(3163, 3664, 0);
    public static final Location ANNAKARL = Location.create(3287, 3883, 0);
    public static final Location GHORROCK = Location.create(2972, 3873, 0);

    /* MISC */
    public static Location CLAN_WARS_LOBBY = Location.create(6471, 3685, 0);
    public static final Location DRAYNOR_VILLAGE = Location.create(3105, 3251, 0);
    public static final Location SAILOR = Location.create(3103, 3487, 0);//Location.create(2310, 3782, 0);

    public static final int EDGEVILLE_X = 3087;
    public static final int EDGEVILLE_Y = 3489;

    public static final int AL_KHARID_X = 3293;
    public static final int AL_KHARID_Y = 3174;

    public static final int KARAMJA_X = 3087;
    public static final int KARAMJA_Y = 3500;

    public static final int MAGE_BANK_X = 2538;
    public static final int MAGE_BANK_Y = 4716;

    public static final int FREMENNIK_X = 2728;
    public static final int FREMENNIK_Y = 3713;

    public static final int SLAYER_TOWER_X = 3428;
    public static final int SLAYER_TOWER_Y = 3537;

    public static final int KELDAGRIM_X = 2837;
    public static final int KELDAGRIM_Y = 10210;

    public static final int SHANTY_PASS_X = 3303;
    public static final int SHANTY_PASS_Y = 3133;

    public static final int ZANARIS_X = 2452;
    public static final int ZANARIS_Y = 4466;

    public static final int SHILO_VILLAGE_X = 2865;
    public static final int SHILO_VILLAGE_Y = 2955;

    public static final int DUEL_ARENA_X = 3365;
    public static final int DUEL_ARENA_Y = 3266;

    public static final int FIGHT_CAVES_X = 2400;
    public static final int FIGHT_CAVES_Y = 5179;

    public static final int BURTHORPE_GAMES_ROOM_X = 2899 - NumberUtils.random(1);
    public static final int BURTHORPE_GAMES_ROOM_Y = 3549;
}
