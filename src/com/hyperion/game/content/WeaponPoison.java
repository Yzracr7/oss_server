package com.hyperion.game.content;

import java.util.HashMap;

import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.player.Player;

/**
 * Manages weapon poisoning.
 * 
 * @author Hybris
 * 
 */
public class WeaponPoison {

	/**
	 * Represents a weapon that can be poisoned. Stores the initial weapon item
	 * id, the type of poison used on the weapon and the new poisoned weapon
	 * that will be obtained.
	 * 
	 */
	private enum Weapon {
		/**
		 * Dragon dagger.
		 */
		DRAGON_DAGGER(1215, new int[][] { { 5940, 5698 }, { 5937, 5680 }, { 187, 1231 } }),

		/**
		 * Rune dagger.
		 */
		RUNE_DAGGER(1213, new int[][] { { 5940, 5696 }, { 5937, 5678 }, { 187, 1229 } }),

		/**
		 * Adamant dagger.
		 */
		ADAMANT_DAGGER(1211, new int[][] { { 5940, 5694 }, { 5937, 5676 }, { 187, 1227 }  }),

		/**
		 * Mithril dagger.
		 */
		MITHRIL_DAGGER(1209, new int[][] { { 5940, 5692 }, { 5937, 5674 }, { 187, 1225 } }),

		/**
		 * Black dagger.
		 */
		BLACK_DAGGER(1217, new int[][] { { 5940, 5700 }, { 5937, 5682 }, { 187, 1233 } }),

		/**
		 * Steel dagger.
		 */
		STEEL_DAGGER(1207, new int[][] { { 5940, 5690 }, { 5937, 5672 }, { 187, 1223 } }),

		/**
		 * Iron dagger.
		 */
		IRON_DAGGER(1203, new int[][] { { 5940, 5686 }, { 5937, 5668 }, { 187, 1219 } }),

		/**
		 * Bronze dagger.
		 */
		BRONZE_DAGGER(1205, new int[][] { { 5940, 5688 }, { 5937, 5670 }, { 187, 1221 } });

		/**
		 * Creates the weapon.
		 * 
		 * @param itemId
		 *            The weapon item id.
		 * @param newItemId
		 *            The poisoned weapon item id.
		 */
		private Weapon(int itemId, int[][] newItemId) {
			this.itemId = itemId;
			this.newItemId = newItemId;
		}

		/**
		 * Gets the item id.
		 * 
		 * @return the itemId
		 */
		public int getItemId() {
			return itemId;
		}

		/**
		 * The weapon item id.
		 */
		private int itemId;

		/**
		 * The poisoned weapon item id.
		 */
		private int[][] newItemId;

		/**
		 * Represents a map for the weapon item ids.
		 */
		public static HashMap<Integer, Weapon> weapon = new HashMap<Integer, Weapon>();

		/**
		 * Gets the weapon id by the item.
		 * 
		 * @param id
		 *            The item id.
		 * @return returns null if itemId is not a weapon.
		 */
		public static Weapon forId(int id) {
			return weapon.get(id);
		}

		/**
		 * @return the newItemId
		 */
		public int[][] getNewItemId() {
			return newItemId;
		}

		/**
		 * Populates a map for the weapons.
		 */
		static {
			for (Weapon w : Weapon.values())

				weapon.put(w.getItemId(), w);

		}
	}

	/**
	 * The item id for Vial.
	 */
	private static final int VIAL = 229;

	/**
	 * Starts the weapon poison event for each individual weapon item from the
	 * enumeration <code>Weapon</code>.
	 * 
	 * @param player
	 *            The Player player.
	 * @param itemUse
	 *            The first item use.
	 * @param useWith
	 *            The second item use.
	 */
	public static void execute(final Player player, int itemUse, int useWith) {
		final Weapon weapon = Weapon.weapon.get(useWith);
		if (weapon != null) {
			for (int element[] : weapon.getNewItemId())
				if (itemUse == element[0]) {
					player.getPacketSender().sendMessage("You make "
							+ new Item(element[1], 1).getDefinition().getName() + ".");

					player.getInventory().deleteItem(element[0], 1);
					player.getInventory().deleteItem(weapon.getItemId(), 1);
					player.getInventory().addItem(VIAL, 1);
					player.getInventory().addItem(element[1], 1);
				}
		}

	}
}