package com.hyperion.game.content.achievements;

/**
 * Handles functionality for {@link Achievements} difficulty's.
 * 
 * @author <a href="http://www.rune-server.org/members/Stand+Up/">Stan</a>
 */
public enum AchievementDifficulty {
	EASY, MEDIUM, HARD;
}
