package com.hyperion.game.content.achievements;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;

public class Achievements {

	/**
	 * The max amount of achievements
	 */
	public static final int MAX_ACHIEVEMENTS = 100;

	/**
	 * The achievement points rewarded for completing the achievement
	 */
	private static final int[] POINTS_REWARDED = { 
			10, 10, 10, 
			10, 10, 50,
			10, 10, 10, // Easy page 1
		
			15, 10, 20,
			10, 15, 20,
			15, 20, 30, //Easy page 2
			
			30, 30, 30,
			30, 30, 30,
			50, 50, 50, //Easy page 3
			
			
			40, 50, 20, 
			20, 20, 20,
			20, 20, 20, 
		
			30, 20, 30, 
			30, 35, 35,
			30, 50, 50, 
			
			30, 50, 50, 
			30, 30, 20,
			25, 40, 30, 
			
			
			};

	/**
	 * The achievement name
	 */
	private static final String[] ACHIEVEMENT_NAME = { 
			"Chop Chop", "Big Shrimp", "The Caveman",
			"Manslaughter", "The Resurrected", "Ruthless",
			"In The Mines", "Smeltsman", "Pickpocket", // Easy page 1
			
			"Muncher", "Window Shopper","This is How it's Done",
			//12, 13, 14
			"Not the Weakest Link", "Climbing The Ladder", "One Thousand",
			"Crabslayer", "Infected", "Worthy Magician", // Easy page 2
			
			"Gnomsayin'", "Almost Professional", "Chef Ramsay",
			"Where's My Talisman", "Cold Blooded", "Carpal Tunnel",
			"The Walking Dead", "Duelist", "Tzhaar-Ket-Novice", // Easy page 3
			
			
			"Voter", "Skillful", "Knockout",
			"Time for a New Axe", "Abyssal Slayer", "Infectious",
			"Master Slayer", "The Jeweller", "The Agile", // Page 1
			
			"The Tormented", "Oblivion","Grudge",
			//12, 13, 14
			"Big Fish", "Karambwhat?", "Clueless",
			"Molten", "Danger zone", "Baby Jad", // Page 2
			
			"On a Roll", "Rolling Stone", "Slaughterhouse",
			"The Bounty Hunter", "Mysterious", "Heat of the Moment",
			"Warzone", "High stakes", "Dragon Defender", // Page 3
			};
	
	private static final String[] ACHIEVEMENT_DES = { 
		"Cut 50 logs", "Catch 50 fish", "Start 100 fires",
		"Kill 20 Men", "Die 5 times", "Kill 5 players",
		"Mine 50 ores", "Smelt 50 bars", "Pickpocket 100 Men", // Easy page 1
		
		"Eat any food 100 times", "View a shop 25 times", "Kill any 100 monsters",
		"Get 100 total level", "Get 500 total level",  "Get 1000 total level", 
		"Kill 100 Rock Crabs", "Complete 3 Pest Control games", "Complete the Mage Arena miniquest", // Easy page 2
		
		"Complete the Gnome Agility Course 50 times", "Steal from 500 stalls", "Cook any food 250 times",
		"Craft any 500 runes", "Complete 5 slayer tasks", "Fletch arrows 300 times",
		"Reach wave 7 in Pest Control", "Duel 10 people at the Duel Arena", "Reach at least wave 30 in Fight Caves", // Easy page 3

		
		"Claim 10 vote auths", "Reach 30M XP in any Non-Combat Skill", "Hit an enemy for 60+ Melee damage",
		"Cut down 1000 logs", "Kill 100 Abyssal Demons", "Complete 30 Zombie games",
		"Complete 100 Slayer tasks", "Craft 250 amulets", "250 Wilderness agility course loops",
		
		"Kill 200 Tormented Demons", "Fight in full Void armour", "Kill all 5 Barrows brothers 100 times",
		"Sucessfully cook 1000 Sharks", "Fish 1000 Karambwans", "Complete 10 Medium Clue scrolls",
		"Kill 100 Lava dragons", "Kill all 4 Wilderness bosses without logging", "Kill TzTok-Jad 10 times", 
		
		"Reach a 5 player consecutive killstreak", "Reach a 15 player consecutive killstreak", "Kill 50 random players",
		"Kill 10 Bounty targets", "Reach 5,000,000 Bounties", "Kill 2 players within one minute",
		"Win 3 Clan Wars battles", "Stake more than 50M GP in one duel", "Kill 250 Cyclops"// page 3
};
	
	private static final int[] REQUIRED_AMOUNT = { 
			50, 50, 100,
			20, 5, 5,
			50, 50, 100, //Easy page 1
		
			100, 25, 100,
			100, 500, 1000,
			100, 3, 1, // Easy page 2
			
			50, 500, 250,
			500, 5, 300,
			7, 10, 30, // Easy page 3
			
			
			10, 1, 1,
			1000, 100, 30,
			100, 250, 250, //Med page 1
		
			200, 1, 100,
			1000, 1000, 10,
			100, 1, 10, // Med page 2
			
			5, 15, 50,
			10, 5, 1,
			3, 1, 250, // Med page 3
			};
	
	
	/**
	 * Increasing a set of achievements
	 * 
	 * @param player
	 *            the player
	 * @param from
	 *            the from
	 * @param to
	 *            the to
	 */
	//public static void increase(final Player player, int from, int to) {
		//for (int i = from; i < to; i++) {
			//increase(player, i);
		//}
	//}

	/**
	 * Increasing the achievement
	 * 
	 * @param p
	 *            the player
	 * @param achievement
	 *            the achievement
	 */
	
	//Increase by 1
	
	
	public static void increase(final Player p, int achievement) {
		handleAchievement(p, achievement, 1, false);
	}
	
	//Increase by amount
	public static void increase(final Player p, int achievement, int amount) {
		handleAchievement(p, achievement, amount, false);
	}
	
	//Set as amount
	public static void set(final Player p, int achievement, int amount) {
		handleAchievement(p, achievement, amount, true);
	}
	
	public static void handleAchievement(final Player p, int achievement, int amount, boolean set) {
		/**
		 * Increases the achievement
		 */
		
		if(set) {
			p.getVariables().getAchievementAmounts()[achievement] = amount;
		} else {
			p.getVariables().getAchievementAmounts()[achievement] = p.getVariables().getAchievementAmounts()[achievement] + amount;
		}
			/**
		 * Checkes for required amount
		 */
		if (p.getVariables().getAchieved()[achievement] != true 
				&& p.getVariables().getAchievementAmounts()[achievement] >= REQUIRED_AMOUNT[achievement]) {
			
			p.getVariables().getAchievementAmounts()[achievement] = REQUIRED_AMOUNT[achievement];
			/**
			 * Achievement is completed
			 */
			p.getVariables().getAchieved()[achievement] = true;
			p.getAttributes().set("achievementInterfaceOpen", true);
			if(p.getInterfaceSettings().getCurrentOverlay() == -1) { // to not interfere with pc
				p.getPacketSender().sendOverlay(606);
				p.getPacketSender().modifyText(""+ACHIEVEMENT_NAME[achievement], 606, 4);
				String diary;
				if(achievement > 18) //9-18 9 med tasks
					diary = "Hard";
				else if(achievement > 8) //0-8 9 easy taks
					diary = "Med";
				else
					diary = "Easy";
				p.getPacketSender().modifyText("Diary: "+diary, 606, 7);
				World.getWorld().submit(new Tickable(25) {
					@Override
					public void execute() {
						this.stop();
						p.getAttributes().remove("achievementInterfaceOpen");
						p.getPacketSender().sendRemoveOverlay();
					}
				});
			}
			p.getPacketSender().sendMessage(
					"<col=09587A>Congratulations! You've completed the achievement <col=1279A6>"
							+ ACHIEVEMENT_NAME[achievement] + "!");
			p.getVariables().setAchievementPoints(
					p.getVariables().getAchievementPoints()
							+ POINTS_REWARDED[achievement]);
			p.getPacketSender().sendMessage(
					"<col=09587A>You recieve <col=1279A6>" + POINTS_REWARDED[achievement]
							+ " points! <col=09587A>You now have the total of <col=1279A6>"
							+ p.getVariables().getAchievementPoints()
							+ " achievement points.");
		} else if(p.getVariables().getAchieved()[achievement] == true) {
			p.getVariables().getAchievementAmounts()[achievement] = REQUIRED_AMOUNT[achievement];
		}
	}

	/**
	 * Checks and advanced skill total level
	 * 
	 * @param player
	 *            the player
	 */
	public static void checkTotalLevel(Player player) {
		int total = player.getSkills().getTotalLevel();
		if (total >= 100) {
			if(player.getVariables().getAchieved()[12] == false) {
				player.getVariables().getAchieved()[12] = true;
				player.getVariables().getAchievementAmounts()[12] = 100;
				Achievements.increase(player, 12);
			}
		}
		if (total >= 500) {
			if(player.getVariables().getAchieved()[13] == false) {
				player.getVariables().getAchieved()[13] = true;
				player.getVariables().getAchievementAmounts()[13] = 500;
				Achievements.increase(player, 13);
			}
		}
		if (total >= 1000) {
			if(player.getVariables().getAchieved()[14] == false) {
				player.getVariables().getAchieved()[14] = true;
				player.getVariables().getAchievementAmounts()[14] = 500;
				Achievements.increase(player, 14);
			}
		}
		//else if (player.getSkill().getTotalLevel() == 2000) {
		//	player.getAchievement().getAchieved()[17] = true;
		//}
	}

	/**
	 * Gets the total achievements acheived
	 * 
	 * @param player
	 *            the player
	 * @return the total completed
	 */
	public static int getTotalAchieved(Player player) {
		int total = 0;
		for (int i = 0; i < MAX_ACHIEVEMENTS; i++) {
			if (player.getVariables().getAchieved()[i]) {
				total++;
			}
		}
		return total;
	}
	public static void openJournal(Player player, int page)  {
		int interfaceId;
		//609 - both buttons
		//610 - only next button
		//611 - only previous button
		int type = player.getAttributes().isSet("achievementType") ? player.getAttributes().getInt("achievementType") : 0;
		int start = type == 0 ? 0 : 27;
		switch(page) {
		case 0:
			interfaceId = 610;
			player.getPacketSender().displayInterface(interfaceId);
			player.getPacketSender().modifyText("Achievements - " + (type == 0 ? "Easy" : "Medium"), interfaceId, 14);
			
			for(int x = 0; x < 9; x++) {
				player.getPacketSender().modifyText(""+ACHIEVEMENT_NAME[0+x+start], interfaceId, 28+x);
			}
			
			for(int x = 0; x < 9; x++) {
				player.getPacketSender().modifyText(ACHIEVEMENT_DES[0+x+start] + "<br>" + (player.getVariables().getAchievementAmounts()[0+x+start] >= REQUIRED_AMOUNT[0+x+start] ? ("<col=00FF00>" + REQUIRED_AMOUNT[0+x+start] +"/" + REQUIRED_AMOUNT[0+x+start]) : player.getVariables().getAchievementAmounts()[0+x+start] > 0 ?  ("<col=FFFF00>"+player.getVariables().getAchievementAmounts()[0+x+start] +"/" + REQUIRED_AMOUNT[0+x+start]) : ("<col=FF0000>"+player.getVariables().getAchievementAmounts()[0+x+start] +"/" + REQUIRED_AMOUNT[0+x+start])), interfaceId, 37+x);
			}
			player.getPacketSender().modifyText("Page: 1 / 3<br><br>" + getTotalAchieved(player) +"/" + ACHIEVEMENT_NAME.length +" total tasks completed<br><br>Points: " + player.getVariables().getAchievementPoints(), interfaceId, 47);
			break;
			
		case 1:
			interfaceId = 609;
			player.getPacketSender().displayInterface(interfaceId);
			player.getPacketSender().modifyText("Achievements - " + (type == 0 ? "Easy" : "Medium"), interfaceId, 14);
			
			for(int x = 0; x < 9; x++) {
				player.getPacketSender().modifyText(""+ACHIEVEMENT_NAME[9+x+start], interfaceId, 28+x);
			}
			
			for(int x = 0; x < 9; x++) {
				player.getPacketSender().modifyText(ACHIEVEMENT_DES[9+x+start] + "<br>" + (player.getVariables().getAchievementAmounts()[9+x+start] >= REQUIRED_AMOUNT[9+x+start] ? ("<col=00FF00>" + REQUIRED_AMOUNT[9+x+start] +"/" + REQUIRED_AMOUNT[9+x+start]): player.getVariables().getAchievementAmounts()[9+x+start] > 0 ?  ("<col=FFFF00>"+player.getVariables().getAchievementAmounts()[9+x+start] +"/" + REQUIRED_AMOUNT[9+x+start]) : ("<col=FF0000>"+player.getVariables().getAchievementAmounts()[9+x+start] +"/" + REQUIRED_AMOUNT[9+x+start])), interfaceId, 37+x);
			}
			player.getPacketSender().modifyText("Page: 2 / 3<br><br>" + getTotalAchieved(player) +"/" + ACHIEVEMENT_NAME.length +" total tasks completed<br><br>Points: " + player.getVariables().getAchievementPoints(), interfaceId, 47);
			break;
		case 2:
			interfaceId = 611;
			player.getPacketSender().displayInterface(interfaceId);
			player.getPacketSender().modifyText("Achievements - " + (type == 0 ? "Easy" : "Medium"), interfaceId, 14);
			
			for(int x = 0; x < 9; x++) {
				player.getPacketSender().modifyText(""+ACHIEVEMENT_NAME[18+x+start], interfaceId, 28+x);
			}
			
			for(int x = 0; x < 9; x++) {
				player.getPacketSender().modifyText(ACHIEVEMENT_DES[18+x+start] + "<br>" + (player.getVariables().getAchievementAmounts()[18+x+start] >= REQUIRED_AMOUNT[18+x+start] ? ("<col=00FF00>" + REQUIRED_AMOUNT[18+x+start] +"/" + REQUIRED_AMOUNT[18+x+start]) : player.getVariables().getAchievementAmounts()[18+x+start] > 0 ?  ("<col=FFFF00>"+player.getVariables().getAchievementAmounts()[18+x+start] +"/" + REQUIRED_AMOUNT[18+x+start]) : ("<col=FF0000>"+player.getVariables().getAchievementAmounts()[18+x+start] +"/" + REQUIRED_AMOUNT[18+x+start])), interfaceId, 37+x);
			}
			
			player.getPacketSender().modifyText("Page: 3 / 3<br><br>" + getTotalAchieved(player) +"/" + ACHIEVEMENT_NAME.length +" total tasks completed<br><br>Points: " + player.getVariables().getAchievementPoints(), interfaceId, 47);
			break;
			
		}
		player.getAttributes().set("achievementPage", page);
	}

	
	
	public static String completionString(Player player) {
		String string = "";
		if(getTotalAchieved(player) == ACHIEVEMENT_NAME.length)
			string = "<col=00FF00>";
		else 
			string = "";
		return string + getTotalAchieved(player) + "/" + ACHIEVEMENT_NAME.length;
	}
}
