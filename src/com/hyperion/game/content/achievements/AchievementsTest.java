package com.hyperion.game.content.achievements;

import java.util.Arrays;
import java.util.Optional;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.TextUtils;

/**
 * Handles functionality to complete achievements. 
 * This should be rewritten because it's still shit.
 * 
 * @author <a href="http://www.rune-server.org/members/Stand+Up/">Stan</a>
 */
public enum AchievementsTest {
	ACHIEVEMENT_1(0, "Chop chop", "Cut 50 logs", 10, 50, AchievementDifficulty.EASY);

	/**
	 * The identification for this achievement.
	 */
	private final int identification;

	/**
	 * The name for this achievement.
	 */
	private final String name;

	/**
	 * The description for this achievement.
	 */
	private final String description;

	/**
	 * The reward upon completing for this achievement.
	 */
	private final int reward;

	/**
	 * The amount required for completion for this achievement.
	 */
	private final int requirement;

	/**
	 * The difficulty of this achievement.
	 */
	private final AchievementDifficulty difficulty;

	/**
	 * @param identification
	 * 		The identification for this achievement.
	 * @param name
	 * 		The name for this achievement.
	 * @param description
	 * 		The description for this achievement.
	 * @param reward
	 * 		The reward for this achievement.
	 * @param requirement
	 * 		The requirement needed to complete this achievement.
	 */
	AchievementsTest(int identification, String name, String description, int reward, int requirement, AchievementDifficulty difficulty) {
		this.identification = identification;
		this.name = name;
		this.description = description;
		this.reward = reward;
		this.requirement = requirement;
		this.difficulty = difficulty;
	}

	/**
	 * The max amount of achievements
	 */
	public static final int MAX_ACHIEVEMENTS = 100;

	/**
	 * Get's an achievement.
	 * 
	 * @param identification
	 * 		The identification to check for.
	 * @return The achievement.
	 */
	private static Optional<AchievementsTest> getAchievement(int identification) {
		return Arrays.stream(values()).filter(a -> a.identification == identification).findAny();
	}
	
	private static int getAllAchievements() {
		for(AchievementsTest test : AchievementsTest.values()) {
			return test.name.length();
		}
		return 0;
	}
	
	public static void increment(Player player, int achievement) {

	}

	/**
	 * Increases the amount left for this specific achievement.
	 * 
	 * @param player
	 * 		The player we are incrementing the achievement for.
	 * @param achievement
	 * 		The achievement we are incrementing.
	 * @param amount
	 * 		The amount we are incrementing it by.
	 * @param set
	 * 		<true> if we are setting the value, <false> otherwise.
	 */
	public static void handleAchievement(final Player player, AchievementsTest achievements, int amount, boolean set) {	
		Optional<AchievementsTest> achievement = getAchievement(achievements.identification);
		
		if(!achievement.isPresent())
			throw new IllegalArgumentException("Invalid argument parameter for this achievement!");
		
		int incrementOrSet = set ? amount : player.getVariables().getAchievementAmounts()[achievement.get().identification] + amount;

		/** Either set or increment the achievement */
		player.getVariables().getAchievementAmounts()[achievement.get().identification] = incrementOrSet;

		/** Check if this achievement has been completed. */
		if (!player.getVariables().getAchieved()[achievement.get().identification] && player.getVariables().getAchievementAmounts()[achievement.get().identification] >= achievement.get().requirement) {		

			/** Set the achievement to the maximum possible value incase it added multiple values. */
			player.getVariables().getAchievementAmounts()[achievement.get().identification] = achievement.get().identification;

			/** Set the achievement to true. */
			player.getVariables().getAchieved()[achievement.get().identification] = true;

			/** Open the achievements inter */
			player.getAttributes().set("achievementInterfaceOpen", true);

			if(player.getInterfaceSettings().getCurrentOverlay() == -1) {
				player.getPacketSender().sendOverlay(606);
				player.getPacketSender().modifyText(""+achievement.get().name, 606, 4);
				player.getPacketSender().modifyText("Diary: "+TextUtils.capitalize(achievement.get().difficulty.name()), 606, 7);

				World.getWorld().submit(new Tickable(25) {

					@Override
					public void execute() {
						this.stop();
						player.getAttributes().remove("achievementInterfaceOpen");
						player.getPacketSender().sendRemoveOverlay();
					}
				});
			}
			player.getPacketSender().sendMessage("<col=09587A>Congratulations! You've completed the achievement <col=1279A6>" + achievement.get().name + "!");
			player.getVariables().setAchievementPoints(player.getVariables().getAchievementPoints() + achievement.get().reward);
			player.getPacketSender().sendMessage("<col=09587A>You recieve <col=1279A6>" + achievement.get().reward + " points! <col=09587A>You now have the total of <col=1279A6>" + player.getVariables().getAchievementPoints() + " achievement points.");
			
		}
	}
	
	/**
	 * Checks and advanced skill total level
	 * 
	 * @param player
	 *            the player
	 */
	public static void checkTotalLevel(Player player) {
		int total = player.getSkills().getTotalLevel();
		if (total >= 100) {
			if(player.getVariables().getAchieved()[12] == false) {
				player.getVariables().getAchieved()[12] = true;
				player.getVariables().getAchievementAmounts()[12] = 100;
				Achievements.increase(player, 12);
			}
		}
		if (total >= 500) {
			if(player.getVariables().getAchieved()[13] == false) {
				player.getVariables().getAchieved()[13] = true;
				player.getVariables().getAchievementAmounts()[13] = 500;
				Achievements.increase(player, 13);
			}
		}
		if (total >= 1000) {
			if(player.getVariables().getAchieved()[14] == false) {
				player.getVariables().getAchieved()[14] = true;
				player.getVariables().getAchievementAmounts()[14] = 500;
				Achievements.increase(player, 14);
			}
		}
	}
	
	/**
	 * Gets the total achievements acheived
	 * 
	 * @param player
	 *            the player
	 * @return the total completed
	 */
	public static int getTotalAchieved(Player player) {
		int total = 0;
		for (int i = 0; i < MAX_ACHIEVEMENTS; i++) {
			if (player.getVariables().getAchieved()[i]) {
				total++;
			}
		}
		return total;
	}
	
	public static void openJournal(Player player, int page)  {
		int interfaceId;
		//609 - both buttons
		//610 - only next button
		//611 - only previous button
		int type = player.getAttributes().isSet("achievementType") ? player.getAttributes().getInt("achievementType") : 0;
		int start = type == 0 ? 0 : 27;
		switch(page) {
		case 0:
			interfaceId = 610;
			player.getPacketSender().displayInterface(interfaceId);
			player.getPacketSender().modifyText("Achievements - " + (type == 0 ? "Easy" : "Medium"), interfaceId, 14);
			
			for(int x = 0; x < 9; x++) {
				player.getPacketSender().modifyText(""+getAchievement(0+x+start).get().name, interfaceId, 28+x);
			}
			
			for(int x = 0; x < 9; x++) {
				player.getPacketSender().modifyText(getAchievement(0+x+start).get().description + "<br>" + (player.getVariables().getAchievementAmounts()[0+x+start] >= getAchievement(0+x+start).get().requirement ? ("<col=00FF00>" + getAchievement(0+x+start).get().requirement +"/" + getAchievement(0+x+start).get().requirement) : player.getVariables().getAchievementAmounts()[0+x+start] > 0 ?  ("<col=FFFF00>"+player.getVariables().getAchievementAmounts()[0+x+start] +"/" + getAchievement(0+x+start).get().requirement) : ("<col=FF0000>"+player.getVariables().getAchievementAmounts()[0+x+start] +"/" + getAchievement(0+x+start).get().requirement)), interfaceId, 37+x);
			}
			player.getPacketSender().modifyText("Page: 1 / 3<br><br>" + getTotalAchieved(player) +"/" + values().length +" total tasks completed<br><br>Points: " + player.getVariables().getAchievementPoints(), interfaceId, 47);
			break;
			
		case 1:
			interfaceId = 609;
			player.getPacketSender().displayInterface(interfaceId);
			player.getPacketSender().modifyText("Achievements - " + (type == 0 ? "Easy" : "Medium"), interfaceId, 14);
			
			for(int x = 0; x < 9; x++) {
				player.getPacketSender().modifyText(""+getAchievement(9+x+start).get().name, interfaceId, 28+x);
			}
			
			for(int x = 0; x < 9; x++) {
				player.getPacketSender().modifyText(getAchievement(9+x+start).get().description + "<br>" + (player.getVariables().getAchievementAmounts()[9+x+start] >= getAchievement(9+x+start).get().requirement ? ("<col=00FF00>" + getAchievement(9+x+start).get().requirement +"/" + getAchievement(9+x+start).get().requirement): player.getVariables().getAchievementAmounts()[9+x+start] > 0 ?  ("<col=FFFF00>"+player.getVariables().getAchievementAmounts()[9+x+start] +"/" + getAchievement(9+x+start).get().requirement) : ("<col=FF0000>"+player.getVariables().getAchievementAmounts()[9+x+start] +"/" + getAchievement(9+x+start).get().requirement)), interfaceId, 37+x);
			}
			player.getPacketSender().modifyText("Page: 2 / 3<br><br>" + getTotalAchieved(player) +"/" + values().length +" total tasks completed<br><br>Points: " + player.getVariables().getAchievementPoints(), interfaceId, 47);
			break;
		case 2:
			interfaceId = 611;
			player.getPacketSender().displayInterface(interfaceId);
			player.getPacketSender().modifyText("Achievements - " + (type == 0 ? "Easy" : "Medium"), interfaceId, 14);
			
			for(int x = 0; x < 9; x++) {
				player.getPacketSender().modifyText(""+getAchievement(18+x+start).get().name, interfaceId, 28+x);
			}
			
			for(int x = 0; x < 9; x++) {
				player.getPacketSender().modifyText(getAchievement(18+x+start).get().description + "<br>" + (player.getVariables().getAchievementAmounts()[18+x+start] >= getAchievement(18+x+start).get().requirement ? ("<col=00FF00>" + getAchievement(18+x+start).get().requirement +"/" + getAchievement(18+x+start).get().requirement) : player.getVariables().getAchievementAmounts()[18+x+start] > 0 ?  ("<col=FFFF00>"+player.getVariables().getAchievementAmounts()[18+x+start] +"/" + getAchievement(18+x+start).get().requirement) : ("<col=FF0000>"+player.getVariables().getAchievementAmounts()[18+x+start] +"/" + getAchievement(18+x+start).get().requirement)), interfaceId, 37+x);
			}
			
			player.getPacketSender().modifyText("Page: 3 / 3<br><br>" + getTotalAchieved(player) +"/" + values().length +" total tasks completed<br><br>Points: " + player.getVariables().getAchievementPoints(), interfaceId, 47);
			break;
			
		}
		player.getAttributes().set("achievementPage", page);
	}

	
	
	public static String completionString(Player player) {
		String string = "";
		if(getTotalAchieved(player) == values().length)
			string = "<col=00FF00>";
		else 
			string = "";
		return string + getTotalAchieved(player) + "/" + values().length;
	}
}
