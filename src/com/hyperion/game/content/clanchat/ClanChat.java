package com.hyperion.game.content.clanchat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.hyperion.Logger;
import com.hyperion.game.Constants;
import com.hyperion.game.content.clanchat.loot.LootshareLadder;
import com.hyperion.game.item.Item;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.TextUtils;

import java.util.Objects;

/**
 * This file manages clan channels, where members can interact with each other,
 * doing things such as activities, lootshare and coinshare, events and so on
 * with the convenience of easier communication.
 *
 * @author relex lawl
 */

// TODO: change name of clan thru inter.

public class ClanChat {

    /**
     * The directory where the clan files will be stored upon server shutdown.
     */
    private static final String DIRECTORY = Constants.FILES_PATH + "clans/";

    /**
     * clancha The list containing all the valid clan chat channels in the
     * world.
     */
    private static List<ClanChat> clans = new ArrayList<ClanChat>();

    final static private int childId = 33;
    final static private int childId2 = 84;

    final static private int MAX_MEMBERS = 47; // limited to World1 ids atm

    /**
     * Loads all the clan chat channel data from files in the {@code DIRECTORY}
     * folder.
     */
    public static void init() {
        final long startup = System.currentTimeMillis();
        try {
            final File directory = new File(DIRECTORY);
            // for(File files : directory.listFiles())
            // Logger.getInstance().warning(files.getName());

            for (final File files : directory.listFiles()) {
                final DataInputStream input = new DataInputStream(
                        new FileInputStream(DIRECTORY + files.getName()));
                /*
				 * Initializes clan instance with the name(UTF) read on input.
				 */
                final ClanChat clan = new ClanChat(files.getName());
				/*
				 * Reads the clan's basic info; name, rank requirement to join,
				 * rank requirement to speak and rank requirement to kick,
				 * respectively.
				 */
                clan.owner = input.readUTF();
                clan.name = input.readUTF();
				/*
				 * clan.minimalRankToJoin = Rank.forId(input.read());
				 * clan.minimalRankToSpeak = Rank.forId(input.read());
				 * clan.minimalRankToKick = Rank.forId(input.read()); /* Reads
				 * the amount of rank members the clan has and loops through the
				 * amount.
				 */
				/*
				 * for (int i = 0; i < input.read(); i++) { /* Gets the ranked
				 * member's name and rank integer and adds it to {@code
				 * clan.ranks} list.
				 */
				/*
				 * String name = input.readUTF(); int rankId = input.read();
				 * Member member = new Member(clan, name); member.rank =
				 * Rank.forId(rankId); clan.ranks.put(member.name, member.rank);
				 * } /* Reads the amount of kick delays the clan has and loops
				 * through the amount.
				 */
				/*
				 * for (int i = 0; i < input.read(); i++) { /* Gets the name of
				 * the ex-member who was kicked and the delay in milliseconds.
				 */
				/*
				 * String name = input.readUTF(); long delay = input.readLong();
				 * clan.kickDelays.put(name, delay); } /* Adds the {@code clan}
				 * instance to {@code clans} list.
				 */
                clans.add(clan);
                input.close();
            }
            Logger.getInstance().warning("Loaded " + clans.size()
                    + " clan chat channels in "
                    + (System.currentTimeMillis() - startup) + "ms.");
        } catch (final IOException exception) {

        }
    }

    public static Player getPlayerFromMember(Member member) {
        final Player player = World.getWorld().find_player_by_name(
                member.getName());
        return player;

    }

    public static boolean isClanLeader(Player player) {
        if (player.getAttributes().isSet("clan_chat_id")) {
            final int currentClan = player.getAttributes().getInt(
                    "clan_chat_id");
            final ClanChat clan = ClanChat.getClanById(currentClan);
            final String leader = clan.getOwner();
            // if(Constants.DEBUG_MODE) {
            // Logger.getInstance().warning("Leader name: " + clan.getOwner() +
            // ", player name: " + player.getName());
            // }
            if (!leader.equalsIgnoreCase(player.getName())) {
                player.getPacketSender()
                        .sendMessage(
                                "Only the leader of the clan can operate a challenge request.");
                return false;
            }
        } else {
            player.getPacketSender()
                    .sendMessage(
                            "You must be in a clan to challenge someone to a clan war.");
            return false;
        }
        return true;
    }

    /**
     * Saves all the clan chat channel data upon server shut down to the
     * {@code DIRECTORY} folder.
     */
    public static void save() {
        final long startup = System.currentTimeMillis();
        try {
            for (final ClanChat clans : ClanChat.clans) {
                final FileOutputStream outputFile = new FileOutputStream(
                        DIRECTORY + clans.owner);
                outputFile.flush();
                final DataOutputStream output = new DataOutputStream(outputFile);
				/*
				 * Writing the clans basic info; owner's user name and the
				 * clan's name.
				 */
                output.writeUTF(clans.owner);
                output.writeUTF(clans.name);
				/*
				 * Writing the clan's rank requirements (in integer form) to:
				 * join, talk and kick, respectively.
				 */
                output.write(clans.minimalRankToJoin.ordinal());
                output.write(clans.minimalRankToSpeak.ordinal());
                output.write(clans.minimalRankToKick.ordinal());
				/*
				 * Writing the amount of ranked players to later loop through.
				 */
                output.write(clans.ranks.size());
				/*
				 * Looping through the clan's ranked members and saving their
				 * name as well as their rank integer.
				 */
                final Iterator<?> rankIterator = clans.ranks.entrySet()
                        .iterator();
                while (rankIterator.hasNext()) {
                    @SuppressWarnings("rawtypes") final Entry entry = (Entry) rankIterator.next();
                    final String name = (String) entry.getKey();
                    final int rank = ((Rank) entry.getValue()).ordinal();
                    output.writeUTF(name);
                    output.write(rank);
                    rankIterator.remove();
                }
				/*
				 * Writing the amount of members currently banned from the
				 * channel.
				 */
                output.write(clans.kickDelays.size());
				/*
				 * Iterating through the kick delays map.
				 */
                final Iterator<?> iterator = clans.kickDelays.entrySet()
                        .iterator();
                while (iterator.hasNext()) {
                    @SuppressWarnings("rawtypes") final Map.Entry entry = (Map.Entry) iterator.next();
                    final String name = (String) entry.getKey();
                    final long delay = (Long) entry.getValue();
                    output.writeUTF(name);
                    output.writeLong(delay);
                    iterator.remove();
                }
                output.close();
            }
            Logger.getInstance().warning("Successfully saved " + clans.size()
                    + " clan chat channels in "
                    + (System.currentTimeMillis() - startup) + "ms.");
        } catch (final IOException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Creates a new clan, if {@code clans} already contains the clan, it will
     * not be created.
     *
     * @param player Player attempting to create a new clan.
     */
    public static void create(Player player) {
        create(player, player.getName());

    }

    public static void create(Player player, String name) {
        if (clanExists(player.getName()))
            player.getPacketSender()
                    .sendMessage("This channel already exists!");
        else {
            final ClanChat clan = new ClanChat(player.getName());
            clan.name = name;
            if (name.length() > 16) {
                player.getPacketSender()
                        .sendMessage(
                                "Your channel name cannot be more than 16 characters in length!");
                return;
            }
            clans.add(clan);
            player.getPacketSender().sendMessage(
                    "You have created your channel: " + clan.name);
            join(player, player.getName());
            save();
        }
    }

    public static void changeClanName(Player player, String name) {
        if (clanExists(player.getName())) {
            final ClanChat clan = getClanByOwner(player.getName());
            clan.name = name;
            if (name.length() > 16) {
                player.getPacketSender()
                        .sendMessage(
                                "Your channel name cannot be more than 16 characters in length!");
                return;
            }
            if (name.contains("<") || name.contains(">")) {
                player.getPacketSender().sendMessage(
                        "Your channel name cannot contain any tags!");
                return;
            }
            for (final Member members : clan.members) {
                final Player players = World.getWorld().find_player_by_name(
                        members.name);
                players.getPacketSender().modifyText(
                        "<col=FAF88E>" + clan.name, 612, 23);
                players.getPacketSender().sendMessage(
                        "Clan name changed to: " + name);
                save();
            }
        } else
            player.getPacketSender().sendMessage("You do not own a clan yet.");
    }

    public static void joinById(Player player, int id) {
        final ClanChat clan = getClanById(id);
        if (clan != null)
            join(player, clan.owner);
        else {
            player.getPacketSender().sendMessage(
                    "Attempting to join channel...");
            player.getPacketSender()
                    .sendMessage("This channel does not exist!");
        }
    }

    public static void join(Player player, String name) {
        player.getPacketSender().sendMessage("Attempting to join channel...");
        if (name.equalsIgnoreCase("help"))
            name = "Help";
        if (clanExists(name)) {
            final ClanChat clan = getClanByOwner(name);
            final Member targetMember = getMemberInClan(
                    getClanIndexByOwner(name), player.getName());
            if (clan.members.contains(targetMember))
                player.getPacketSender().sendMessage(
                        "Your request cannot be completed.");
            if (!canJoin(player, clan))
                return;
            if (clan.members.size() < MAX_MEMBERS) {
                clan.members.add(new Member(clan, player.getName()));

                // int clanIndex = getClanIndexByOwner(clan.owner);
                // Member member = getMemberInClan(clanIndex, player.getName());
                // TODO: they're saved ordinally, must be a better way to do
                // this...
                if (!clan.ranks.containsKey(player.getName()))
                    clan.ranks.put(player.getName(), Rank.NONE);
                if (player.getName().equalsIgnoreCase(clan.owner))
                    // if (!clan.ranks.containsKey(player.getName())) {
                    clan.ranks.put(player.getName(), Rank.OWNER);

                // }

                player.getAttributes().set("clan_chat_id",
                        getClanIndexByOwner(clan.owner));
                for (final Member members : clan.members) {
                    final Player players = World.getWorld()
                            .find_player_by_name(members.name);
                    updateMembersList(players, clan, false);
                }
                player.getPacketSender().sendMessage(
                        "Now talking in clan channel " + clan.name);
                player.getPacketSender().sendMessage(
                        "To talk, speak each line of chat with the / symbol.");
            } else
                player.getPacketSender().sendMessage(
                        "This channel is currently full!");
        } else if (player.getName().equalsIgnoreCase(name))
            player.getPacketSender()
                    .sendMessage(
                            "This channel doesn't exist! Click Clan Setup to configure your channel.");
        else
            player.getPacketSender()
                    .sendMessage("This channel does not exist!");
    }

    /**
     * Sends the text spoken by {@code player} to all {@code clan.members} in
     * channel.
     *
     * @param player Player currently speaking.
     * @param text   The text player has sent.
     */
    public static void speak(Player player, String text) {
        TextUtils.formatName(text);

        final int currentClan = player.getAttributes().getInt("clan_chat_id");
        final ClanChat clan = getClanById(currentClan);
        if (player.getAttributes().isSet("clan_chat_id")/* !clan.equals(null) */) {
            if (!canSpeak(player, clan))
                return;
            String sprite = "";
            if (player != null) {
                if (player.getDetails().isDonator()
                        && !player.getVariables().isRankHidden()
                        || player.getDetails().isIronman())
                    sprite = "<img="
                            + (player.getDetails().isIronman() ? "14"
                            : player.getDetails().isLegendaryDonator() ? "16"
                            : player.getDetails()
                            .isExtremeDonator() ? "4"
                            : player.getDetails()
                            .isSuperDonator() ? "15"
                            : "3") + ">";
                if (player.getDetails().getRights() == 1
                        || player.getDetails().getRights() == 2)
                    sprite = "<img="
                            + (player.getDetails().isAdmin() ? "1" : "0") + ">";

                // TODO: Support rank: img=2

                final List<Member> membersList = new ArrayList<Member>(
                        clan.members);// Collections.copy(clan.members, arg1);
                for (final Member members : membersList) { // concurrent mod?
                    final Player players = World.getWorld()
                            .find_player_by_name(members.name);

                    if (players == null)
                        continue;

                    if (!clan.ranks.containsKey(players.getName()))
                        clan.ranks.put(players.getName(), Rank.NONE);

                    final String message = "[<col=0000CC>" + clan.name
                            + "</col>] " + sprite
                            + player.getName().substring(0, 1).toUpperCase()
                            + player.getName().substring(1) + ":<col=8B0000> "
                            + text;
                    players.getPacketSender().sendMessage(message);
                }
            }
        } else
            player.getPacketSender().sendMessage(
                    "You are not currently in a channel.");
    }

    public static void lootshareMessage(Player player, Item item) {
        final int currentClan = player.getAttributes().getInt("clan_chat_id");
        final ClanChat clan = getClanById(currentClan);
        if (player.getAttributes().isSet("clan_chat_id")/* !clan.equals(null) */) {
            for (final Member members : clan.members) { // concurrent mod?
                final Player players = World.getWorld()
                        .find_player_by_name(members.name);

                if (players == null)
                    continue;
                final String message = "[" + clan.getName() + "]" + " " + "<img=0></img>" + "Lootshare: " + player.getName() + " has received:" + item.getCount() + "x " + item.getDefinition().getName();
                players.getPacketSender().sendMessage(message);
            }
        }
    }

    private static int getSpriteId(Player player) {
        final int currentClan = player.getAttributes().getInt("clan_chat_id");
        final ClanChat clan = getClanById(currentClan);
        if (player.getAttributes().isSet("clan_chat_id")) {
            final Rank rank = clan.ranks.get(player.getName());
            if (player.getDetails().isAdmin())
                return 10;
            else if (player.getDetails().isModerator())
                return 12;
            else
                return 6;
        }
        return 0;
    }

    /**
     * Handles {@code player}'s attempt to kick {@code target} from the channel.
     *
     * @param player Player attempting to kick {@code target}.
     * @param target Player being kicked from channel.
     */
    public static void kick(Player player, Player target) {
        if (target == null) {
            player.getPacketSender().sendMessage(
                    "This player is not currently in the channel.");
            return;
        }
        if (target == player) {
            player.getPacketSender().sendMessage("You can't kick yourself!");
            return;
        }
        final int currentClan = player.getAttributes().getInt("clan_chat_id");
        final ClanChat clan = getClanById(currentClan);
        if (clan != null) {
            final Member targetMember = getMemberInClan(currentClan,
                    target.getName());
            if (clan.members.contains(targetMember)) {
                if (!canKick(player, target, clan))
                    return;
                clan.members.remove(targetMember);
                player.getAttributes().remove("clan_chat_id");
                player.getPacketSender().sendMessage(
                        "You were kicked from the channel.");
                clan.kickDelays.put(player.getName(),
                        System.currentTimeMillis());
                for (final Member members : clan.members) {
                    final Player players = World.getWorld()
                            .find_player_by_name(members.name);
                    updateMembersList(players, clan, true);
                }
                cleanInterface(target, true, true, true, true);
            } else
                player.getPacketSender().sendMessage(
                        "This player is not currently in the channel.");
        } else
            player.getPacketSender().sendMessage(
                    "You are not currently in a channel.");
    }

    public static void leave(Player player, boolean logout) {
        if (player == null)
            return;
        final int currentClan = player.getAttributes().getInt("clan_chat_id");
        final ClanChat clan = getClanById(currentClan);
        if (player.getAttributes().isSet("clan_chat_id")) {
            final Member member = getMemberInClan(currentClan, player.getName()/*
																				 * new
																				 * Member
																				 * (
																				 * clan
																				 * ,
																				 * player
																				 * .
																				 * getName
																				 * (
																				 * )
																				 */);
			/*
			 * if (clan.members.contains(member)) { clan.members.remove(member);
			 */
            if (member != null) {
                clan.members.remove(member);
                player.getAttributes().remove("clan_chat_id");
                if (logout)
                    player.getAttributes().set("last_clan", clan.owner); // when
                    // relogging
                    // in
                else
                    player.getAttributes().remove("last_clan");
                for (final Member members : clan.members) {
                    final Player players = World.getWorld()
                            .find_player_by_name(members.name);
                    updateMembersList(players, clan, true);
                }
                cleanInterface(player, true, true, true, true);
                if (!logout)
                    player.getInterfaceSettings().closeInterfaces(true);
            } else
                player.getPacketSender().sendMessage(
                        "You are currently not in a channel.");
        } else
            player.getPacketSender().sendMessage(
                    "You are not currently in a channel.");
    }

    /**
     * Handles the promotion of {@code target} in a channel.
     *
     * @param player Owner of the clan chat channel.
     * @param target The player who is being given the promotion/demotion.
     * @param rank   The rank to give {@code target}.
     */
    public static void giveRank(Player player, Player target, Rank rank) {
        final ClanChat clan = new ClanChat(player.getName());
        // Member targetMember = new Member(clan, target.getName());
        final Member targetMember = getMemberInClan(
                getClanIndexByOwner(clan.owner), target.getName());
        if (clans.contains(clan)) {
            if (rank.equals(Rank.NONE)) {
                if (clan.ranks.containsKey(targetMember.name))
                    clan.ranks.remove(targetMember.name);
            } else
                clan.ranks.put(targetMember.name, rank);
        } else
            player.getPacketSender().sendMessage("You do not have a channel.");
    }

    private static void updateMembersList(Player player, ClanChat clan,
                                          boolean playerLeft) {
        if (player == null)
            return;
        int childId = 33;
        int childId2 = 84;
        if (playerLeft)
            cleanInterface(player, false, true, true, false);
        else
            cleanInterface(player, false, false, true, false);

        player.getPacketSender()
                .modifyText("<col=FAF88E>" + clan.name, 612, 23);
        player.getPacketSender().modifyText("<col=EBEBEB>" + clan.owner, 612,
                25);
        player.getPacketSender().modifyText("Leave Chat", 612, 143);

        final List<Member> membersCopy = new ArrayList<Member>(clan.members);
        for (final Member member : membersCopy) {
            final int rankId = clan.ranks.get(member.name) != null ? clan.ranks
                    .get(member.name).ordinal() : 0;
            String rank = rankId > 0 ? "<img=" + (rankId + 10) + ">" : "";
            final Player memberPlayer = World.getWorld().find_player_by_name(
                    member.name);
            if (memberPlayer == null)
                return;
            boolean hasRank = false;
            // if (memberPlayer.getDetails().isAdmin() ||
            // memberPlayer.getDetails().isModerator()) {
            rank = "<img="
                    + (memberPlayer.getDetails().isAdmin() || memberPlayer.getDetails().isOwner() ? "10"
                    : memberPlayer.getDetails().isModerator() ? "12"
                    : "6") + ">";
            hasRank = true;
            // }
            if (memberPlayer.getName().equalsIgnoreCase(clan.owner)) {
                rank = "<img=9>";
                hasRank = true;
            }
            player.getPacketSender().modifyText(
                    "<col=EBEBEB>" + (hasRank ? "" : "    ") + rank
                            + member.name, 612, childId);
            player.getPacketSender().modifyText("<col=FAF88E>World 1", 612,
                    childId2);
            childId++;
            childId2++;
        }
    }

    public static void cleanInterface(Player player, boolean top,
                                      boolean names, boolean worlds, boolean buttons) {
        if (top) {
            player.getPacketSender().modifyText("<col=FAF88E>None", 612, 23);
            player.getPacketSender().modifyText("<col=EBEBEB>None", 612, 25);
        }
        if (names)
            for (int i = childId; i <= childId + 50; i++)
                player.getPacketSender().modifyText("", 612, i);
        if (worlds)
            for (int i = childId2; i < 134; i++)
                player.getPacketSender().modifyText("", 612, i);
        if (buttons) {
            player.getPacketSender().modifyText("Join Chat", 612, 143);
            player.getPacketSender().modifyText("Clan Setup", 612, 144);
        }
    }

    public static void listClans() {
        for (int i = 0; i < clans.size(); i++)
            Logger.getInstance().warning(i + " - " + clans.get(i).name + " owned by "
                    + clans.get(i).owner);
    }

    public static boolean clanExists(String o) {
        for (int i = 0; i < clans.size(); i++)
            if (o.equalsIgnoreCase(clans.get(i).owner))
                return true;
        return false;
    }

    public static boolean isOwner(Player player, int clanId) {
        if (player.getName().equalsIgnoreCase(clans.get(clanId).owner))
            return true;
        return false;
    }

    private static ClanChat getClanByOwner(String o) {
        for (int i = 0; i < clans.size(); i++)
            if (o.equalsIgnoreCase(clans.get(i).owner))
                return clans.get(i);
        return null;
    }

    private static ClanChat getClanByName(String o) {
        for (int i = 0; i < clans.size(); i++) {
            ClanChat clan = clans.get(i);
            for (Member member : clan.getMembers()) {
                if (member.getName().equalsIgnoreCase(o))
                    return clan;
            }
        }
        return null;
    }

    public static ClanChat getClanById(int id) {
        for (int i = 0; i < clans.size(); i++)
            if (id == i)
                return clans.get(i);
        return null;
    }

    public static int getClanIndexByOwner(String o) {
        for (int i = 0; i < clans.size(); i++)
            if (o.equalsIgnoreCase(clans.get(i).owner))
                return i;
        return -1;
    }

    private static Member getMemberInClan(int clanId, String memberName) {
        for (int i = 0; i < clans.get(clanId).members.size(); i++)
            if (memberName
                    .equalsIgnoreCase(clans.get(clanId).members.get(i).name))
                return clans.get(clanId).members.get(i);
        return null;
    }

    public String getOwner() {
        return owner;
    }

    /**
     * Checks if {@code player} has the required attributes to join {@code clan}
     * .
     *
     * @param player Player attempting to join clan chat channel.
     * @param clan   The clan chat channel {@code player} is attempting to join.
     * @return Can player can join the channel.
     */
    private static boolean canJoin(Player player, ClanChat clan) {
        if (player.getAttributes().isSet("clan_chat_id")) {
            player.getPacketSender().sendMessage(
                    "You are already in a channel!");
            return false;
        }
        if (clan.kickDelays.get(player.getName()) != null
                && System.currentTimeMillis()
                - clan.kickDelays.get(player.getName()) > 0
                && System.currentTimeMillis()
                - clan.kickDelays.get(player.getName()) < 1800000) {
            final long diff = System.currentTimeMillis()
                    - clan.kickDelays.get(player.getName());
            final int minutes = (int) (diff / (1000 * 60) % 60);
            player.getPacketSender().sendMessage(
                    "You are currently banned from this channel for another "
                            + minutes);
            return false;
        }
        if (clan.minimalRankToJoin.ordinal() <= 0)
            return true;
        final int rankId = clan.ranks.get(player.getName()).ordinal();
        if (clan.minimalRankToJoin.ordinal() > rankId) {
            player.getPacketSender().sendMessage(
                    "You do not have the required rank to join this channel.");
            return false;
        }
        return true;
    }

    /**
     * Checks if {@code player} has the attributes required to speak in
     * {@code clan}.
     *
     * @param player Player attempting to speak in said clan.
     * @param clan   Said ClanChat player is attempting to speak in.
     * @return Can player speak in channel.
     */
    private static boolean canSpeak(Player player, ClanChat clan) {
        if (clan.minimalRankToSpeak.ordinal() <= 0)
            return true;
        final int rankId = clan.ranks.get(player.getName()).ordinal();
        if (clan.minimalRankToSpeak.ordinal() > rankId) {
            player.getPacketSender().sendMessage(
                    "You do not have the required rank to talk.");
            return false;
        }
        return true;
    }

    /**
     * Checks if {@code player} has the attributes required to kick
     * {@code target}.
     *
     * @param player Player attempting to kick {@code target}.
     * @param target Player being kicked from {@code clan}.
     * @param clan   Said ClanChat player's are participating in.
     * @return Can {@code player} kick {@code target}.
     */
    private static boolean canKick(Player player, Player target, ClanChat clan) {
        final Rank rank = clan.ranks.get(player.getName());
        if (rank == null)
            return false;
        final int rankId = rank.ordinal();
        final int targetRankId = clan.ranks.get(target.getName()).ordinal();
        if (rankId <= 0)
            return false;
        if (clan.minimalRankToKick.ordinal() > rankId) {
            player.getPacketSender().sendMessage(
                    "You do not have the required rank to kick.");
            return false;
        }
        if (targetRankId > rankId) {
            player.getPacketSender()
                    .sendMessage("You cannot kick this player!");
            return false;
        }
        return true;
    }

    /**
     * The clan chat constructor.
     *
     * @param owner The name of the owner.
     */
    private ClanChat(String owner) {
        this.owner = owner;
        // this.name = owner; //handled?
    }

    /**
     * The name of the owner.
     */
    private String owner;

    /**
     * The name of the clan chat.
     */
    private String name;

    public String getName() {
        return name;
    }

    /**
     * The minimal rank required to join this channel.
     */
    private final Rank minimalRankToJoin = Rank.NONE;

    /**
     * The minimal rank required to kick others from this channel.
     */
    private final Rank minimalRankToKick = Rank.OWNER;

    /**
     * The minimal rank required to speak in this channel.
     */
    private final Rank minimalRankToSpeak = Rank.NONE;

    /**
     * The list containing all current members in this channel.
     */
    private final List<Member> members = new ArrayList<Member>(MAX_MEMBERS);

    public List<Member> getMembers() {
        return members;
    }

    public LootshareLadder getLootshareLadder() {
        return lootshareLadder;
    }

    public static void enableLootshare(Player player) {
        if (isClanLeader(player)) {
            final ClanChat clan = getClanByOwner(player.getName());
            Objects.requireNonNull(clan);
            final List<Player> players = new ArrayList<>();
            clan.members.forEach(memb -> players.add(memb.asPlayer()));
            players.removeIf(memb -> !memb.getVariables().isEnabled());
            clan.lootshareLadder = new LootshareLadder(players);
        } else if (!isClanLeader(player)) {
            final ClanChat clan = ClanChat.getClanByName(player.getName());
            if (clan.lootshareLadder != null)
                clan.lootshareLadder.add(player);
        }
    }

    public static void disableLootshare(Player player) {
        if (isClanLeader(player)) {
            final ClanChat clan = getClanByOwner(player.getName());
            Objects.requireNonNull(clan);
            clan.lootshareLadder = null;
        } else if (!isClanLeader(player)) {
            final ClanChat clan = ClanChat.getClanByName(player.getName());
            Objects.requireNonNull(clan);
            Objects.requireNonNull(clan.lootshareLadder);
            clan.lootshareLadder.remove(player);
        }

    }

    /**
     * The map containing the rank members of the channel.
     */
    private final Map<String, Rank> ranks = new HashMap<String, Rank>();

    /**
     * The map containing all the kicked members and time (milliseconds) in
     * which they were kicked.
     */
    private final Map<String, Long> kickDelays = new HashMap<String, Long>();

    /**
     * This enum contains all the valid ranks existing in the clan chat channel.
     *
     * @author relex lawl
     */
    private enum Rank {
        /*
         * A regular non-ranked member.
         */
        NONE,

        /*
         * One of the owner's friends. sprite = 5
         */
        FRIEND,

        /*
         * The lowest-ranked member. sprite = 6
         */
        RECRUIT,

        /*
         * A low-ranked member. sprite = 7
         */
        CORPORAL,

        /*
         * An intermediate-ranked member. sprite = 8
         */
        SERGEANT,

        /*
         * A high-ranked member. sprite = 11
         */
        LIEUTENANT,

        /*
         * A high-ranked member. sprite = 12
         */
        CAPTAIN,

        /*
         * The highest-ranked member, excluding the owner. sprite = 13
         */
        GENERAL,

        /*
         * The owner of the clan chat channel, who has the highest priority of
         * said clan. sprite = 9
         */
        OWNER;

        /**
         * Loops through all the rank objects until one with the same value
         * (ordinal()) is matched with {@code id}.
         *
         * @param id The int to match to the rank values (ordinal()).
         * @return The rank object matched.
         */
        private static Rank forId(int id) {
            for (final Rank rank : Rank.values())
                if (rank.ordinal() == id)
                    return rank;
            return null;
        }
    }

    /**
     * An inner class, which defines a specific member in a clan chat channel.
     *
     * @author relex lawl
     */
    public static class Member {

        /**
         * The member's constructor.
         *
         * @param clanChat The clan chat channel to which member is belonging to.
         * @param name     The user name of the member.
         */
        private Member(ClanChat clanChat, String name) {
            this.clanChat = clanChat;
            this.name = name;
        }

        /**
         * The member's clan chat channel to which they belong.
         */
        @SuppressWarnings("unused")
        private final ClanChat clanChat;

        /**
         * The user name of the member.
         */
        private final String name;

        /**
         * The member's rank in {@code clanChat}.
         */
        private final Rank rank = Rank.NONE;

        public String getName() {
            return name;
        }

        public Player asPlayer() {
            return World.getWorld().find_player_by_name(name);
        }

    }

    private LootshareLadder lootshareLadder;
}