package com.hyperion.game.content.clanchat.loot;

import com.hyperion.game.tools.editors.npc.NPCItemDrop;

public class Lootshare {

	private int points;
	private LootshareStatus status;

	public Lootshare(LootshareStatus status, int points) {
		this.points = points;
		this.status = status;
	}

	/**
	 * @return the points
	 */
	public int getPoints() {
		return points;
	}

	public void update(NPCItemDrop item, LootshareDropStatus status) {
		final int value = item.getItem().getDefinition().getGeneralPrice();
		switch (status) {
		case REWARD:
			points -= value;
			break;
		case REWARD_FOR_ANOTHER_MEMBER:
			points += value;
			break;
		default:
			break;

		}
	}

	/**
	 * @return the status
	 */
	public LootshareStatus getStatus() {
		return status;
	}

	public void setStatus(LootshareStatus status) {
		this.status = status;
	}

	public void setPoints(int points) {
		this.points = points;
	}
}
