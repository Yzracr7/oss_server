package com.hyperion.game.content.clanchat.loot;

public enum LootshareDropStatus {

	REWARD, REWARD_FOR_ANOTHER_MEMBER;

}
