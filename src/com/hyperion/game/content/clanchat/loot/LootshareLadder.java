package com.hyperion.game.content.clanchat.loot;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.hyperion.game.tools.editors.npc.NPCItemDrop;
import com.hyperion.game.world.entity.player.Player;

public class LootshareLadder {

    public static class NPCItemDropComparator implements
            Comparator<NPCItemDrop> {

        @Override
        public int compare(NPCItemDrop firstItem, NPCItemDrop secondItem) {
            final int firstValue = firstItem.getItem().getDefinition()
                    .getGeneralPrice();
            final int secondValue = secondItem.getItem().getDefinition()
                    .getGeneralPrice();
            return secondValue - firstValue;
        }

    }

    public static class PlayerComparator implements Comparator<Player> {

        @Override
        public int compare(Player arg0, Player arg1) {
            final int firstPoints = arg0.getVariables().getLootshare()
                    .getPoints();
            final int secondPoints = arg1.getVariables().getLootshare()
                    .getPoints();
            final int sum = secondPoints - firstPoints;
            return sum;
        }

    }

    private final List<Player> members;

    public LootshareLadder(List<Player> members) {
        this.members = members;
    }

    public List<LootsharePair> fromLootshareLadder(List<NPCItemDrop> items) {
        items.sort(new NPCItemDropComparator());
        int factor = 1;
        int factorPosition = 0;
        int playerPointer = 0;
        if (items.size() % 2 == 1)
            items.remove(Math.random() * items.size());
        if (items.size() > members.size())
            factor = items.size() / members.size();
        final List<LootsharePair> rewards = new ArrayList<>();
        final NPCItemDrop[] factoredDrops = new NPCItemDrop[factor];
        for (final NPCItemDrop drop : items) {
            factoredDrops[factorPosition++] = drop;
            if (factorPosition == factor) {
                final Player player = members.get(playerPointer++);
                for (final NPCItemDrop d : factoredDrops) {
                    rewards.add(new LootsharePair(player, d));
                    player.getVariables().getLootshare()
                            .update(d, LootshareDropStatus.REWARD);
                }
                for (int j = playerPointer; j < members.size(); j++) {
                    final Player otherPlayer = members.get(j);
                    for (final NPCItemDrop d : factoredDrops)
                        otherPlayer
                                .getVariables()
                                .getLootshare()
                                .update(d,
                                        LootshareDropStatus.REWARD_FOR_ANOTHER_MEMBER);
                }
                reset(factoredDrops, factorPosition, playerPointer);
            }
        }
        return rewards;
    }

    private void reset(NPCItemDrop[] factoredDrops, int factorPointer,
                       int playerPointer) {
        for (int i = 0; i < factoredDrops.length; i++)
            factoredDrops[i] = null;
        factorPointer = 0;
        playerPointer = 0;
        sort();
    }

    public void add(Player player) {
        members.add(player);
        sort();
    }

    public void remove(Player player) {
        members.remove(player);
        sort();
    }

    public void sort() {
        members.sort(new PlayerComparator());
    }

}
