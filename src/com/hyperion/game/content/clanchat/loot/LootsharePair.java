package com.hyperion.game.content.clanchat.loot;

import com.hyperion.game.tools.editors.npc.NPCItemDrop;
import com.hyperion.game.world.entity.player.Player;

public class LootsharePair {

	private Player player;
	private NPCItemDrop drop;

	public LootsharePair(Player player, NPCItemDrop drop) {
		this.player = player;
		this.drop = drop;
	}

	public NPCItemDrop getDrop() {
		return drop;
	}

	public void setDrop(NPCItemDrop drop) {
		this.drop = drop;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

}
