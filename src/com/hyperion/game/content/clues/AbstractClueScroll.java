package com.hyperion.game.content.clues;

import java.util.Optional;

import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.player.Player;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 27, 2015
 */
public abstract class AbstractClueScroll {

	/**
	 * The inter id of the clue scroll
	 * 
	 * @return
	 */
	public abstract int getInterfaceId();

	/**
	 * The location that the clue scroll ends at
	 * 
	 * @return A {@link Location} {@code Object}
	 */
	public abstract Optional<Location> endLocation();

	/**
	 * The type of clue scroll this is
	 * 
	 * @return
	 */
	public abstract ClueScrollType scrollType();

	/**
	 * If the player has the requirements to do this clue scroll. Some scrolls
	 * require equipment to be worn or some stats
	 * 
	 * @param player
	 *            The player
	 * @return
	 */
	public abstract boolean playerHasRequirements(Player player);

	/**
	 * The animation the player must perform to complete this scroll
	 * 
	 * @return
	 */
	public abstract Optional<ClueAnimations> animationToPerform();

	/**
	 * Gets the riddle text to display over the {@link #getInterfaceId()}. This
	 * is only applicable for {@link ClueScrollType#OBJECT} type clue scrolls
	 * 
	 * @return
	 */
	public abstract Optional<String[]> getRiddle();
	
	@Override
	public String toString() {
		return "[" + getClass().getSimpleName() + ", " + scrollType() + "]";
	}
}
