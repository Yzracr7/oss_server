package com.hyperion.game.content.clues;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 27, 2015
 */
public enum ClueAnimations {

	YES, NO, BOW, ANGRY, THINK, WAVE, SHRUG, CHEER, BECKON, LAUGH, JUMP_FOR_JOY, YAWN, DANCE, JIG, SPIN, HEADBANG, CRY, BLOW_KISS, PANIC, RASPBERRY, CLAP, SALUTE, GOBLIN_BOW, GOBLIN_SALUTE, GLASS_BOX, CLIMB_ROPE, LEAN, GLASS_WALL, IDEA {
		@Override
		public int getButtonId() {
			return 32;
		}
	},
	STAMP {
		@Override
		public int getButtonId() {
			return 30;
		}
	},
	FLAP {
		@Override
		public int getButtonId() {
			return 31;
		}
	},
	SLAP_HEAD {
		@Override
		public int getButtonId() {
			return 29;
		}
	},
	ZOMBIE_WALK {
		@Override
		public int getButtonId() {
			return 33;
		}
	},
	ZOMBIE_DANCE {
		@Override
		public int getButtonId() {
			return 34;
		}
	},
	SCARED {
		@Override
		public int getButtonId() {
			return 35;
		}
	},
	RABBIT_HOP {
		@Override
		public int getButtonId() {
			return 36;
		}
	},
	SKILLCAPE {
		@Override
		public int getButtonId() {
			return 37;
		}
	};

	/**
	 * Gets the button id of the animation
	 * 
	 * @return
	 */
	public int getButtonId() {
		return ordinal() + 1;
	}

	/**
	 * Gets a {@link ClueAnimations} {@code Object} by a button id
	 * 
	 * @param buttonId
	 *            The button id of the clue
	 * @return
	 */
	public static ClueAnimations getScrollByButtonId(int buttonId) {
		for (ClueAnimations animation : ClueAnimations.values()) {
			if (animation.getButtonId() == buttonId) {
				return animation;
			}
		}
		return null;
	}
}
