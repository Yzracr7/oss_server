package com.hyperion.game.content.clues;

import static com.hyperion.game.content.clues.ClueScrollManagement.*;

import java.io.IOException;

import com.hyperion.cache.Cache;
import com.hyperion.cache.definitions.CachedItemDefinition;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 27, 2015
 */
public enum ClueScrollComplexity {

	EASY(70D, 3D) {
		@Override
		public int getCasketId() {
			return EASY_CASKET;
		}

		@Override
		public int[] getRewards() {
			return new int[] { 2587, 2583, 2585, 2589, 2595, 2591, 2593, 2597, 2633, 2635, 2637, 10366, 7396, 7392, 7388, 7394, 7390, 7386, 7364, 7368, 7362, 7366, 10404, 10406, 10424, 10426, 10408, 10410, 10428, 10430, 10412, 10414, 10432, 10434, 10458, 10464, 10470, 10460, 10468, 10444, 10474, 10462, 10466, 10472, 10442 };
		}
	},

	MEDIUM(40D, 5D) {
		@Override
		public int getCasketId() {
			return MEDIUM_CASKET;
		}

		@Override
		public int[] getRewards() {
			return new int[] { 2605, 2599, 2601, 2603, 2613, 2607, 2609, 2611, 10446, 10448, 10450, 10452, 10454, 10456, 10736, 13107, 13109, 13111, 13113, 13115, 7380, 7372, 7370, 7378, 2577, 2579, 7319, 7321, 7323, 7325, 7327 };
		}
	},

	HARD(30D, 4D) {
		@Override
		public int getCasketId() {
			return HARD_CASKET;
		}

		@Override
		public int[] getRewards() {
			return new int[] { 2627, 2623, 2625, 2629, 2619, 2627, 2623, 2625, 2629, 2619, 2615, 2617, 2621, 2673, 2669, 2671, 2675, 2657, 2653, 2655, 2659, 3486, 3481, 3483, 3485, 3488, 7376, 7384, 7382, 7374, 2581, 8950, 7400, 7399, 7398, 2639, 2641, 2643, 10362, 10350, 10334, 10342, 10348, 10352, 10346, 10330, 10332, 10336, 10334, 10338, 10340, 10470, 10472, 10474, 10440, 10442, 10444, 10368, 10370, 10372, 10374, 10384, 10386, 10388, 10390, 10376, 10378, 10380, 10382 };
		}
	},

	ELITE(40.0D, 2D) {
		@Override
		public int getCasketId() {
			return ELITE_CASKET;
		}

		@Override
		public int[] getRewards() {
			return new int[] { 18669, 18670, 18671, 18672, 18673, 18674, 18675, 18676, 18678, 18679, 18680, 18681, 18682,
					14743, 14745, 14747, 14749, 14751, 14753, 14755, 14757, 14759, 14761, 14763, 14765, 14767, 14769, 14771, 14773, 14775, 14777, 14779, 14781, 14783, 14785, 14787, 14789, 14791, 13101, 3481, 3483, 3485, 3486, 3488, };
		}
	};

	ClueScrollComplexity(Double chance, Double maxRewards) {
		this.maxRewards = maxRewards;
		this.chance = chance;
	}

	/**
	 * @return the chance
	 */
	public Double getChance() {
		return chance;
	}

	/**
	 * @return the rewards
	 */
	public abstract int[] getRewards();

	/**
	 * @return the maxRewards
	 */
	public Double getMaxRewards() {
		return maxRewards;
	}

	/**
	 * The chance to get a reward
	 */
	private final Double chance;

	/**
	 * The maximum amount of rewards they can get of this type,
	 */
	private final Double maxRewards;

	/**
	 * The casket reward id
	 * 
	 * @return
	 */
	public abstract int getCasketId();

	/**
	 * Gets the casket complexity
	 * 
	 * @param casketId
	 *            The id of a casket
	 * @return
	 */
	public static ClueScrollComplexity getCasketComplexity(int casketId) {
		for (ClueScrollComplexity complexity : ClueScrollComplexity.values()) {
			if (complexity.getCasketId() == casketId) {
				return complexity;
			}
		}
		return null;
	}

	public static void main(String[] args) throws IOException {
		Cache.init();
		for (ClueScrollComplexity complexity : ClueScrollComplexity.values()) {
			int[] rewards = complexity.getRewards();
			System.out.println("=================================================================");
			System.out.println(complexity + " REWARDS");
			for (int reward : rewards) {
				try {
					System.out.println("new Item(" + reward + ")//" + CachedItemDefinition.forId(reward).getName());
				} catch (Exception e) {
					System.err.println("Error with " + reward);
				}
			}
		}
	}
}
