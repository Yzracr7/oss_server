package com.hyperion.game.content.clues;

import com.hyperion.game.Constants;
import com.hyperion.game.content.InterfaceTools;
import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.content.clues.types.ObjectClueScroll;
import com.hyperion.game.item.Item;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.game.world.grounditems.GroundItem;
import com.hyperion.game.world.grounditems.GroundItemManager;
import com.hyperion.utility.Utils;

import java.util.*;

/**
 * @author Tyluur<itstyluur @ gmail.com>
 * @since Mar 27, 2015
 */
public class ClueScrollManagement {

    /**
     * The clue scrolls and ids
     */
    public static final int EASY_SCROLL = 10210, MEDIUM_SCROLL = 10212, HARD_SCROLL = 10214, ELITE_SCROLL = 10216;

    /**
     * The casket reward ids
     */
    public static final int EASY_CASKET = 13052, MEDIUM_CASKET = 13054, HARD_CASKET = 13057, ELITE_CASKET = 13059;

    /**
     * The array of junk items that are possible to be received
     */
    private static final Item[] JUNK_ITEMS = new Item[]{new Item(556, Utils.random(500, 1000)), // air
            // runes
            new Item(995, Utils.random(100, 15000)), // coins
            new Item(1079, 1), // rune platebody
            new Item(1127, 1), // rune platelegs
            new Item(9185, 1), // rune crossbow
            new Item(1275, 1), // rune pickaxe
            new Item(2435, Utils.random(5, 15)), // prayer pots
            new Item(1273, 1), // mith pick
            new Item(1291), // bronze longsword
            new Item(1217), // black dagger
            new Item(10476, Utils.random(30, 100)), // purple sweets
            new Item(10327, Utils.random(10, 100)), // gnomish firelighters
            new Item(861), // magic short
            new Item(892, Utils.random(1, 15)), // rune arrows
            new Item(882, Utils.random(1, 100)), // bronze arrow
            new Item(336, Utils.random(10)), // raw trout
            new Item(1446), // body talisman
            new Item(1442), // fire talisman
            new Item(561, Utils.random(20, 50)), // nature runes
            new Item(2503), // black d'hide body
            new Item(565, Utils.random(20, 50)), // blood runes,
            new Item(1725), // str ammy
            new Item(1478), // accuracy ammy
            new Item(1727), // mage ammy
            new Item(841), // shortbow
            new Item(849), // willow shortbow
            new Item(558, Utils.random(20, 200)), // mind rune
            new Item(386, 10), // sharks
            new Item(332, Utils.random(1, 100)), // raw salmons
            new Item(1381), // air staff
            new Item(1383), // water staff
            new Item(1169), // coif
            new Item(860, Utils.random(10, 30)), // magic longbow
            new Item(7948) // burnt monkfish
    };

    /**
     * Loading all clue scrolls into the {@link #CLUE_SCROLLS} list
     */
    public static void load() {
        String[] directories = {"coordinates", "emotes", "maps", "objects"};
        for (String directory : directories) {
            for (Object clazz : Utils.getClassesInDirectory(ClueScrollManagement.class.getPackage().getName() + ".types." + directory)) {
                CLUE_SCROLLS.add((AbstractClueScroll) clazz);
            }
        }
    }

    /**
     * Gets a random scroll from the {@link #CLUE_SCROLLS} list
     *
     * @return A {@code AbstractClueScroll} {@code Object} instance
     */
    public static AbstractClueScroll getRandomScroll() {
        List<AbstractClueScroll> localList = new ArrayList<>(CLUE_SCROLLS);
        Collections.shuffle(localList);
        return localList.get(0);
    }

    public static AbstractClueScroll getRandomScroll(AbstractClueScroll lastScroll) {
        if (lastScroll == null)
            return getRandomScroll();
        List<AbstractClueScroll> localList = new ArrayList<>(CLUE_SCROLLS);
        Collections.shuffle(localList);

        if (localList.get(0) == lastScroll)
            return getRandomScroll(lastScroll);
        return localList.get(0);
    }

    /**
     * Gets the complexity of a clue scroll based on its id
     *
     * @param itemId The item id
     * @return
     */
    public static ClueScrollComplexity getComplexity(int itemId) {
        switch (itemId) {
            case EASY_SCROLL:
                return ClueScrollComplexity.EASY;
            case MEDIUM_SCROLL:
                return ClueScrollComplexity.MEDIUM;
            case HARD_SCROLL:
                return ClueScrollComplexity.HARD;
            case ELITE_SCROLL:
                return ClueScrollComplexity.ELITE;
            default:
                return null;
        }
    }

    /**
     * Gets the amount of steps until the clue is over.
     *
     * @param complexity The complexity of the scroll
     * @return A {@code Integer} {@code Array}. The first index is the least
     * amount of steps, the second index is the max amount of steps.
     */
    public static int[] getStepsCount(ClueScrollComplexity complexity) {
        switch (complexity) {
            case EASY:
                return new int[]{2, 4};
            case MEDIUM:
                return new int[]{4, 8};
            case HARD:
                return new int[]{6, 10};
            case ELITE:
                return new int[]{8, 14};
            default:
                throw new IllegalStateException("Unexpected complexity type " + complexity);
        }
    }

    /**
     * The list of game clue scrolls
     */
    public static final List<AbstractClueScroll> CLUE_SCROLLS = new ArrayList<AbstractClueScroll>();

    /**
     * This handles the reading of a clue scroll
     *
     * @param player The player
     * @param itemId The item id
     * @return
     */
    public static boolean readScroll(Player player, int itemId) {
        ClueScrollComplexity complexity = getComplexity(itemId);
        if (complexity == null) {
            return false;
        }
        if (player.getVariables().getClueScrolls()[complexity.ordinal()] == null) {
            giveRandomScroll(player, complexity, true);
        }
        AbstractClueScroll scroll = player.getVariables().getClueScrolls()[complexity.ordinal()];
        if (scroll == null) {
            throw new IllegalStateException("Illegal state! Clue scroll was null.");
        }
        if (scroll.scrollType() == ClueScrollType.COORDINATES || scroll.scrollType() == ClueScrollType.EMOTES) {
            for (int child = 0; child < InterfaceTools.getInterfaceDefinitionsComponentsSize(scroll.getInterfaceId()); child++) {
                player.getPacketSender().modifyText("", scroll.getInterfaceId(), child);
            }
            Optional<String[]> optional = scroll.getRiddle();
            if (optional.isPresent()) {
                String[] texts = optional.get();
                for (int i = 0; i < texts.length; i++) {
                    String text = texts[i];
                    player.getPacketSender().modifyText(text, scroll.getInterfaceId(), 1 + i);
                }
            }
        }
        if (Constants.DEBUG_MODE) {
            player.teleport(scroll.endLocation().get());
            System.out.println("[scrollSteps=" + Arrays.toString(player.getVariables().getScrollSteps()) + ", scrollProgression=" + Arrays.toString(player.getVariables().getScrollProgression()) + "]");
        }
        player.getPacketSender().displayInterface(scroll.getInterfaceId());
        return false;
    }

    /**
     * @param player     The player
     * @param complexity The complexity
     */
    private static void giveRandomScroll(Player player, ClueScrollComplexity complexity, boolean setSteps) {
        player.getVariables().getClueScrolls()[complexity.ordinal()] = getRandomScroll(player.getVariables().getClueScrolls()[complexity.ordinal()]);

        if (setSteps) {
            int[] steps = getStepsCount(complexity);
            int result = Utils.random(steps[0], steps[1]);
            player.getVariables().getScrollSteps()[complexity.ordinal()] = result;
        }
    }

    /**
     * Handles an emote being performed
     *
     * @param player     The player
     * @param parameters The button id
     */
    public static boolean confirmPerformance(Player player, Object... parameters) {
        AbstractClueScroll scroll = null;
        for (AbstractClueScroll scrolls : player.getVariables().getClueScrolls()) {
            if (scrolls == null) {
                continue;
            }
            Optional<Location> optional = scrolls.endLocation();
            if (optional.isPresent()) {

                Location endLocation = optional.get();
                Location targetLocation = player.getLocation();

                if (scrolls instanceof ObjectClueScroll && parameters[0] instanceof GameObject) {
                    targetLocation = ((GameObject) parameters[0]).getLocation();
                    if (targetLocation.distanceToPoint(endLocation) != 0) {
                        if (Constants.DEBUG_MODE)
                            player.getPacketSender().sendMessage("Too far from clue object: " + endLocation + ", obj: " + targetLocation);
                        return false;
                    } else {
                        if (scrolls.scrollType() == ClueScrollType.OBJECT) {
                            scroll = scrolls;
                            break;
                        }
                    }
                } else if (!(parameters[0] instanceof GameObject)) {
                    if (endLocation.distance(targetLocation) <= 3) {
                        scroll = scrolls;
                        break;
                    }
                }


            }
        }
        if (scroll == null) {
            return false;
        }
        if (!scroll.playerHasRequirements(player)) {
            if (Constants.DEBUG_MODE)
                player.getPacketSender().sendMessage("Player does not have clue scroll reqs.");
            return false;
        }
        ClueScrollComplexity complexity = null;
        Item scrollFound = null;

        //TODO: find the correct scroll -- must match complexity
        //Basically you're digging at the MEDIUM location
        //but because easy scroll comes first in the loop
        //it uses that complexity
        for (Item item : player.getInventory().getItems()) {
            if (item == null) {
                continue;
            }
            ClueScrollComplexity itemComplexity = getComplexity(item.getId());
            if (itemComplexity == null) {
                continue;
            }
            if (player.getVariables().getClueScrolls()[itemComplexity.ordinal()] == null) {
                System.out.println("Clue scroll issue -- no clue set for complexity: " + itemComplexity.ordinal() + " for player " + player.getName());
                continue;
            }
            if (player.getVariables().getClueScrolls()[itemComplexity.ordinal()].getInterfaceId() == scroll.getInterfaceId()) {
                scrollFound = item;
                complexity = itemComplexity;
                break;
            } else {
                //Scroll not null but it is not our set scroll
                //player.getVariables().getClueScrolls()[itemComplexity.ordinal()] = scroll;
                if (Constants.DEBUG_MODE) {
                    System.out.println("Not our clue: " + item.getId() + ", complexity: " + itemComplexity.ordinal() + ", our scroll type: " + scroll.scrollType());
                    System.out.println("Our should be: " + player.getVariables().getClueScrolls()[itemComplexity.ordinal()].scrollType() + ", name: " + player.getVariables().getClueScrolls()[itemComplexity.ordinal()].toString());
                }
                //	scrollFound = item;
                //complexity = itemComplexity;
                continue;
            }
        }
        if (complexity != null) {
            if (Constants.DEBUG_MODE)
                player.getPacketSender().sendMessage("Scroll complexity: " + complexity + ", type: " + scroll.scrollType());
            switch (scroll.scrollType()) {
                case EMOTES:
                    ClueAnimations animation = ClueAnimations.getScrollByButtonId((int) parameters[0]);
                    if (Constants.DEBUG_MODE)
                        player.getPacketSender().sendMessage("Clue emote confirmation, buttonId: " + parameters[0] + ", " + (animation == null ? "anim not found" : "anim: " + animation.toString()));
                    if (animation == null) {
                        return false;
                    }
                    Optional<ClueAnimations> optional = scroll.animationToPerform();
                    if (optional.isPresent()) {
                        ClueAnimations requiredAnimation = optional.get();
                        if (requiredAnimation.equals(animation)) {
                            executeCorrectPerformance(player, scroll, complexity, scrollFound);
                            return true;
                        }
                    }
                    break;
                case COORDINATES:
                case MAP:
                    if ((int) parameters[0] == -1) {
                        executeCorrectPerformance(player, scroll, complexity, scrollFound);
                    }
                    return true;
                case OBJECT:
                    if (parameters.length == 3 && (boolean) parameters[2] == true) {
                        executeCorrectPerformance(player, scroll, complexity, scrollFound);
                    }
                    return true;
                default:
                    return false;
            }
            return true;
        }
        return false;
    }

    /**
     * @param player     The player
     * @param scroll     The scroll
     * @param complexity The complexity
     * @param item       The item
     */
    private static void executeCorrectPerformance(Player player, AbstractClueScroll scroll, ClueScrollComplexity complexity, Item item) {
        int stepsToCompletion = player.getVariables().getScrollSteps()[complexity.ordinal()];
        int currentSteps = player.getVariables().getScrollProgression()[complexity.ordinal()];
        if (currentSteps < stepsToCompletion) {
            player.getPacketSender().sendChatboxDialogue(true, "You've found another clue!");

            player.getVariables().getScrollProgression()[complexity.ordinal()]++;
            player.getInventory().deleteItem(item);
            player.getInventory().addItem(item);
            player.getInventory().refresh();

            giveRandomScroll(player, complexity, false);
        } else {
            player.getPacketSender().sendItemDialogue(player, complexity.getCasketId(), 1, "You've found a casket!");

            player.getInventory().deleteItem(item);
            player.getInventory().addItem(complexity.getCasketId());
            player.getInventory().refresh();

            removeScroll(player, scroll, complexity);
        }
    }

    /**
     * @param player     The player
     * @param scroll     The scroll
     * @param complexity The complexity
     */
    private static void removeScroll(Player player, AbstractClueScroll scroll, ClueScrollComplexity complexity) {
        player.getVariables().getClueScrolls()[complexity.ordinal()] = null;
        player.getVariables().getScrollSteps()[complexity.ordinal()] = 0;
        player.getVariables().getScrollProgression()[complexity.ordinal()] = 0;
    }

    /**
     * Opens the casket and gives the player the rewards
     *
     * @param player   The player
     * @param casketId The casket id
     */
    public static boolean openCasket(Player player, int casketId) {
        ClueScrollComplexity complexity = ClueScrollComplexity.getCasketComplexity(casketId);
        if (complexity == null) {
            return false;
        } else if (complexity == ClueScrollComplexity.MEDIUM) {
            Achievements.increase(player, 41);
        }
        int tableRewards = 0;
        final int numRewards = Utils.random(2, 5);
        List<Item> rewardsList = new ArrayList<>();

        for (int i = 0; i < numRewards; i++) {
            if (tableRewards < complexity.getMaxRewards() && Utils.percentageChance(complexity.getChance().intValue())) {
                rewardsList.add(new Item(complexity.getRewards()[Utils.random(complexity.getRewards().length)], 1));
                tableRewards++;
            } else {
                rewardsList.add(JUNK_ITEMS[Utils.random(JUNK_ITEMS.length - 1)]);
            }
        }
        player.getInventory().deleteItem(casketId);
        for (Item item : rewardsList) {
            if (!player.getInventory().addItem(item)) {
                //if (!player.getInventory().addItem(item)) {
                GroundItemManager.create(new GroundItem(new Item(item.getId(), item.getCount()), player), player);
                player.getPacketSender().sendMessage("The items were dropped at your feet.");
                //	} else {
                //		player.getPacketSender().sendMessage("The items were deposited into your bank.");
                //	}
            }
        }
        player.getInventory().refresh();
        player.getPacketSender().sendClueScroll(rewardsList.toArray(new Item[rewardsList.size()]));
        return true;
    }
}
