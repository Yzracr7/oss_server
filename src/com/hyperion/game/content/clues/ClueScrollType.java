package com.hyperion.game.content.clues;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 27, 2015
 */
public enum ClueScrollType {

	MAP, OBJECT, EMOTES, COORDINATES

}
