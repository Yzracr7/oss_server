package com.hyperion.game.content.clues.types;

import java.util.Optional;

import com.hyperion.game.content.clues.AbstractClueScroll;
import com.hyperion.game.content.clues.ClueAnimations;
import com.hyperion.game.content.clues.ClueScrollType;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.player.Player;

/**
 * To complete this type of clue scroll, players must navigate to the
 * coordinates after they decrypt them and dig at the spot.
 * 
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 27, 2015
 */
public abstract class CoordinateClueScroll extends AbstractClueScroll {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.clues.AbstractClueScroll#getInterfaceId()
	 */
	@Override
	public int getInterfaceId() {
		return 345;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.clues.AbstractClueScroll#scrollType()
	 */
	@Override
	public ClueScrollType scrollType() {
		return ClueScrollType.COORDINATES;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.clues.AbstractClueScroll#endLocation()
	 */
	@Override
	public abstract Optional<Location> endLocation();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.clues.AbstractClueScroll#playerHasRequirements
	 * (com.hyperion.game.world.entity.player.Player)
	 */
	@Override
	public boolean playerHasRequirements(Player player) {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.clues.AbstractClueScroll#animationToPerform()
	 */
	@Override
	public Optional<ClueAnimations> animationToPerform() {
		return Optional.empty();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.clues.AbstractClueScroll#getRiddle()
	 */
	@Override
	public abstract Optional<String[]> getRiddle();

}
