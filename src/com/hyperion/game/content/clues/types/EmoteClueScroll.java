package com.hyperion.game.content.clues.types;

import java.util.Optional;

import com.hyperion.game.content.clues.AbstractClueScroll;
import com.hyperion.game.content.clues.ClueAnimations;
import com.hyperion.game.content.clues.ClueScrollType;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.player.Player;

/**
 * To complete this clue scroll, players must navigate to the location this clue
 * points them to and perform the desired emote
 * 
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 27, 2015
 */
public abstract class EmoteClueScroll extends AbstractClueScroll {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.clues.AbstractClueScroll#getInterfaceId()
	 */
	@Override
	public int getInterfaceId() {
		return 345;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.clues.AbstractClueScroll#scrollType()
	 */
	@Override
	public ClueScrollType scrollType() {
		return ClueScrollType.EMOTES;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.clues.AbstractClueScroll#getRiddle()
	 */
	@Override
	public abstract Optional<String[]> getRiddle();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.clues.AbstractClueScroll#endLocation()
	 */
	@Override
	public abstract Optional<Location> endLocation();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.clues.AbstractClueScroll#playerHasRequirements
	 * (com.hyperion.game.world.entity.player.Player)
	 */
	@Override
	public abstract boolean playerHasRequirements(Player player);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.clues.AbstractClueScroll#animationToPerform()
	 */
	@Override
	public abstract Optional<ClueAnimations> animationToPerform();

}
