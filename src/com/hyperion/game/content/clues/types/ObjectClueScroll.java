package com.hyperion.game.content.clues.types;

import java.util.Optional;

import com.hyperion.game.content.clues.AbstractClueScroll;
import com.hyperion.game.content.clues.ClueAnimations;
import com.hyperion.game.content.clues.ClueScrollType;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.player.Player;

/**
 * To complete this type of clue scroll, players must
 * 
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 27, 2015
 */
public abstract class ObjectClueScroll extends AbstractClueScroll {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.clues.AbstractClueScroll#getInterfaceId()
	 */
	@Override
	public abstract int getInterfaceId();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.clues.AbstractClueScroll#scrollType()
	 */
	@Override
	public ClueScrollType scrollType() {
		return ClueScrollType.OBJECT;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.clues.AbstractClueScroll#animationToPerform()
	 */
	@Override
	public Optional<ClueAnimations> animationToPerform() {
		return Optional.empty();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.clues.AbstractClueScroll#playerHasRequirements
	 * (com.hyperion.game.world.entity.player.Player)
	 */
	@Override
	public boolean playerHasRequirements(Player player) {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.clues.AbstractClueScroll#endLocation()
	 */
	@Override
	public abstract Optional<Location> endLocation();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.clues.AbstractClueScroll#getRiddle()
	 */
	@Override
	public Optional<String[]> getRiddle() {
		return Optional.empty();
	}

}
