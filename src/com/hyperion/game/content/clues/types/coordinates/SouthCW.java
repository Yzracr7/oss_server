package com.hyperion.game.content.clues.types.coordinates;

import java.util.Optional;

import com.hyperion.game.content.clues.types.CoordinateClueScroll;
import com.hyperion.game.world.Location;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 27, 2015
 */
public class SouthCW extends CoordinateClueScroll {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.clues.types.CoordinateClueScroll#endLocation()
	 */
	@Override
	public Optional<Location> endLocation() {
		return Optional.of(Location.create(2322, 3060, 0));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.clues.types.CoordinateClueScroll#getRiddle()
	 */
	@Override
	public Optional<String[]> getRiddle() {
		return Optional.of(new String[] { "03 degrees 07 minutes south", "03 degrees 41 minutes west" });
	}

}
