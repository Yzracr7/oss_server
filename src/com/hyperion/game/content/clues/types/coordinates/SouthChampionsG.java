package com.hyperion.game.content.clues.types.coordinates;

import java.util.Optional;

import com.hyperion.game.content.clues.types.CoordinateClueScroll;
import com.hyperion.game.world.Location;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 27, 2015
 */
public class SouthChampionsG extends CoordinateClueScroll {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.clues.types.CoordinateClueScroll#endLocation()
	 */
	@Override
	public Optional<Location> endLocation() {
		return Optional.of(Location.create(3179, 3343, 0));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.clues.types.CoordinateClueScroll#getRiddle()
	 */
	@Override
	public Optional<String[]> getRiddle() {
		return Optional.of(new String[] { "05 degrees 43 minutes north", "23 degrees 05 minutes east " });
	}

}
