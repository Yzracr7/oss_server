package com.hyperion.game.content.clues.types.coordinates;

import java.util.Optional;

import com.hyperion.game.content.clues.types.CoordinateClueScroll;
import com.hyperion.game.world.Location;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 27, 2015
 */
public class TreeGnomeVillage extends CoordinateClueScroll {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.clues.types.CoordinateClueScroll#endLocation()
	 */
	@Override
	public Optional<Location> endLocation() {
		return Optional.of(Location.create(2416, 3516, 0));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.clues.types.CoordinateClueScroll#getRiddle()
	 */
	@Override
	public Optional<String[]> getRiddle() {
		return Optional.of(new String[] { "11 degrees 05 minutes north", "00 degrees 45 minutes west" });
	}

}
