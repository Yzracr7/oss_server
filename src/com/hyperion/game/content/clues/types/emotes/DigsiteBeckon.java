package com.hyperion.game.content.clues.types.emotes;

import java.util.Optional;

import com.hyperion.game.content.clues.ClueAnimations;
import com.hyperion.game.content.clues.types.EmoteClueScroll;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.impl.Equipment;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 27, 2015
 */
public class DigsiteBeckon extends EmoteClueScroll {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.clues.types.EmoteClueScroll#getRiddle()
	 */
	@Override
	public Optional<String[]> getRiddle() {
		return Optional.of(new String[] { "Beckon in the Digsite, near the", "eastern winch.", "Equip an iron pickaxe." });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.clues.types.EmoteClueScroll#endLocation()
	 */
	@Override
	public Optional<Location> endLocation() {
		return Optional.of(Location.create(3369, 3428, 0));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.clues.types.EmoteClueScroll#playerHasRequirements
	 * (com.hyperion.game.world.entity.player.Player)
	 */
	@Override
	public boolean playerHasRequirements(Player player) {
		return player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.WEAPON.ordinal()) == 1267;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.clues.types.EmoteClueScroll#animationToPerform
	 * ()
	 */
	@Override
	public Optional<ClueAnimations> animationToPerform() {
		return Optional.of(ClueAnimations.BECKON);
	}

}
