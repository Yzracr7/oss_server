package com.hyperion.game.content.clues.types.emotes;

import java.util.Optional;

import com.hyperion.game.content.clues.ClueAnimations;
import com.hyperion.game.content.clues.types.EmoteClueScroll;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.impl.Equipment;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 27, 2015
 */
public class LegendsGuild extends EmoteClueScroll {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.clues.types.EmoteClueScroll#getRiddle()
	 */
	@Override
	public Optional<String[]> getRiddle() {
		return Optional.of(new String[] { "Bow outside the entrance", "to the Legends' Guild.", "Equip iron platelegs, an", "emerald amulet and an oak longbow." });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.clues.types.EmoteClueScroll#endLocation()
	 */
	@Override
	public Optional<Location> endLocation() {
		return Optional.of(Location.create(2728, 3349, 0));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.clues.types.EmoteClueScroll#playerHasRequirements
	 * (com.hyperion.game.world.entity.player.Player)
	 */
	@Override
	public boolean playerHasRequirements(Player player) {
		return player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.LEGS.ordinal()) == 1067 && player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.AMULET.ordinal()) == 1696 && player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.WEAPON.ordinal()) == 845;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.clues.types.EmoteClueScroll#animationToPerform
	 * ()
	 */
	@Override
	public Optional<ClueAnimations> animationToPerform() {
		return Optional.of(ClueAnimations.BOW);
	}

}
