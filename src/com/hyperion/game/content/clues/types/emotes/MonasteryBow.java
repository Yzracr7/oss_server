package com.hyperion.game.content.clues.types.emotes;

import java.util.Optional;

import com.hyperion.game.content.clues.ClueAnimations;
import com.hyperion.game.content.clues.types.EmoteClueScroll;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.player.Player;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 27, 2015
 */
public class MonasteryBow extends EmoteClueScroll {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.clues.types.EmoteClueScroll#getRiddle()
	 */
	@Override
	public Optional<String[]> getRiddle() {
		return Optional.of(new String[] { "Bow upstairs in the Monastery." });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.clues.types.EmoteClueScroll#endLocation()
	 */
	@Override
	public Optional<Location> endLocation() {
		return Optional.of(Location.create(3056, 3484, 1));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.clues.types.EmoteClueScroll#playerHasRequirements
	 * (com.hyperion.game.world.entity.player.Player)
	 */
	@Override
	public boolean playerHasRequirements(Player player) {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.clues.types.EmoteClueScroll#animationToPerform
	 * ()
	 */
	@Override
	public Optional<ClueAnimations> animationToPerform() {
		return Optional.of(ClueAnimations.BOW);
	}

}
