package com.hyperion.game.content.clues.types.maps;

import java.util.Optional;

import com.hyperion.game.content.clues.types.MapClueScroll;
import com.hyperion.game.world.Location;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 27, 2015
 */
public class AlkharidX extends MapClueScroll {

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.clues.types.MapClueScroll#getInterfaceId()
	 */
	@Override
	public int getInterfaceId() {
		return 623;
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.clues.types.MapClueScroll#endLocation()
	 */
	@Override
	public Optional<Location> endLocation() {
		return Optional.of(Location.create(3300, 3291, 0));
	}

}
