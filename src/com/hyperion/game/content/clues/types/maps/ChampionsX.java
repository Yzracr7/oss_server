package com.hyperion.game.content.clues.types.maps;

import java.util.Optional;

import com.hyperion.game.content.clues.types.MapClueScroll;
import com.hyperion.game.world.Location;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 27, 2015
 */
public class ChampionsX extends MapClueScroll {

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.clues.types.MapClueScroll#getInterfaceId()
	 */
	@Override
	public int getInterfaceId() {
		return 625;
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.clues.types.MapClueScroll#endLocation()
	 */
	@Override
	public Optional<Location> endLocation() {
		return Optional.of(Location.create(3166, 3360, 0));
	}

}
