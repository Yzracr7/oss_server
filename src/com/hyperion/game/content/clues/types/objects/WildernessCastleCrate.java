package com.hyperion.game.content.clues.types.objects;

import java.util.Optional;

import com.hyperion.game.content.clues.types.ObjectClueScroll;
import com.hyperion.game.world.Location;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 27, 2015
 */
public class WildernessCastleCrate extends ObjectClueScroll {

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.clues.types.ObjectClueScroll#getInterfaceId()
	 */
	@Override
	public int getInterfaceId() {
		return 628;
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.clues.types.ObjectClueScroll#endLocation()
	 */
	@Override
	public Optional<Location> endLocation() {
		return Optional.of(Location.create(3026, 3628, 0));
	}

}
