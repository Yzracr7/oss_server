package com.hyperion.game.content.commands;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.utility.Utils;
import com.hyperion.utility.logger.Log;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class CommandHandler {

	/**
	 * All commands in the subdirectories below this class are loaded into the
	 * {@link #COMMANDS} list for usage
	 * 
	 * @throws Exception
	 */
	public static void initialize() {
		for (String directory : Utils.getSubDirectories(CommandHandler.class)) {
			try {
				for (Object clazz : Utils.getClassesInDirectory(CommandHandler.class.getPackage().getName() + "." + directory)) {
					getCommands().add((CommandSkeleton) clazz);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * This method handles a command sent to the server by a player
	 * 
	 * @param player
	 *            The player
	 * @param command
	 *            The command sent, unformatted
	 * @return
	 */
	public static boolean handleCommand(Player player, String command) {
		try {
			String[] cmd = command.split(" ");
			String commandKey = cmd[0];
			CommandSkeleton instance = getCommand(commandKey);
			if (instance == null) {
				return false;
			}
			
			World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Used command " + command));
			instance.handle(player, cmd);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Gets the right the player needs in order to use this command. If the
	 * player has access to the command, we will return a null object.
	 * 
	 * @param player
	 *            The player
	 * @param command
	 *            The command
	 * @return
	 */
	public static Rights accessibleRightRequired(Player player, CommandSkeleton command) {
		if (player.getDetails().isOwner()) {
			return null;
		}
		Rights rightRequired = null;
		Rights[] rights = command.getRightsAllowed();
		Rights right = rights[0];
		if (!right.hasPrivileges(player)) {
			return right;
		}
		return rightRequired;
	}

	/**
	 * @param key
	 *            The key we are looking for the in commands
	 * @return A {@code CommandSkeleton} instance
	 */
	public static CommandSkeleton getCommand(String key) {
		for (Iterator<CommandSkeleton> it$ = getCommands().iterator(); it$.hasNext();) {
			CommandSkeleton command = it$.next();
			for (String keys : command.getKeys()) {
				if (keys.equalsIgnoreCase(key)) {
					return command;
				}
			}
		}
		return null;
	}

	/**
	 * @return the commands
	 */
	public static List<CommandSkeleton> getCommands() {
		return COMMANDS;
	}

	/**
	 * The list of server commands
	 */
	private static final List<CommandSkeleton> COMMANDS = new ArrayList<>();

}
