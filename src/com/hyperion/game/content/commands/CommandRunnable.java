package com.hyperion.game.content.commands;

import com.hyperion.game.world.entity.player.Player;

public class CommandRunnable implements Runnable {
	
	@Override
	public void run() {
		
	}
	
	public void run(Player player, String[] cmd) {
		
	}
	
	public int index = 1;
	
	public int read(String[] cmd) {
		int integer = getInteger(cmd[index++]);
		return integer;
	}
	
	public int read(String[] cmd, int Default) {
		int integer = cmd.length <= index ? Default : getInteger(cmd[index++]);
		return integer;
	}
	
	public boolean read(String[] cmd, boolean Default) {
		boolean bool = cmd.length <= index ? Default : getBoolean(cmd[index++]);
		return bool;
	}
	
	public String read(String[] cmd, String Default, boolean... underscores) {
		String username = "";
		if (underscores.length > 0 && underscores[0])
			return cmd[index++];
		for (int i = index; i < cmd.length; i++)
			username += cmd[i] + ((i == cmd.length - 1) ? "" : " ");
		return username;
	}
	
	//public Player read(Player player, String[] cmd, boolean... underscores) {
	//	return getPlayer(player, read(cmd, "", underscores), underscores);
	//}
	
	public static Integer getInteger(String s) {
		return Integer.parseInt(s);
	}
	
	public static Boolean getBoolean(String s) {
		return Boolean.parseBoolean(s);
	}
	
	public static Long getLong(String s) {
		return Long.parseLong(s);
	}
	
	/*public static Player getPlayer(Player player, String[] cmd) {
		return getPlayer(player, cmd, 1, cmd.length);
	}
	
	public static Player getPlayer(Player player, String[] cmd, int startIndex) {
		return getPlayer(player, cmd, startIndex, cmd.length);
	}
	
	public static Player getPlayer(Player player, String[] cmd, int startIndex, int endIndex) {
		String username = "";
		for (int i = startIndex; i < endIndex; i++)
			username += cmd[i] + ((i == endIndex - 1) ? "" : " ");
		Player other = getPlayer(player, username);
		return other;
	}
	
	public static Player getPlayer(Player player, String username, boolean... underscores) {
		Player other = World.getPlayerByDisplayName(username);
		if (other == null) {
			other = SerializableFilesManager.loadPlayer(Utils
					.formatPlayerNameForProtocol(username));
			if (other != null) {
				other.check();
				other.setManagers();
				other.link();
				other.initEntity();
				other.setUsername(Utils.formatPlayerNameForProtocol(username));
			} else {
				player.sm("The player '"+username+"' doesn't exist.");
				player.sm("Be sure to use the player's username, not the display name.");
				if (underscores.length == 1 && underscores[0]) {
					player.sm("Also, this command requires underscores in the player name.");
				}
			}
		}
		return other;
	}
	
	public static boolean isLoggedIn(Player player) {
		return World.getPlayerAmount(player) != 0;
	}*/
	
}
