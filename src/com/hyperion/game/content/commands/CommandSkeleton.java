package com.hyperion.game.content.commands;

import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public interface CommandSkeleton {

	/**
	 * The identifiers for the command
	 * 
	 * @return
	 */
	public String[] getKeys();

	/**
	 * The array of rights that have access to the command
	 * 
	 * @return
	 */
	public Rights[] getRightsAllowed();

	/**
	 * Each command must be handled in its own specific way, this method does
	 * that.
	 * 
	 * @param player
	 *            The player doing the command
	 * @param cmd
	 *            The command array
	 */
	public void handle(Player player, String[] cmd);
	
	/**
	 * The custom owner rights id, not actually set
	 */
	public static final Rights OWNER = Rights.OWNER;
	
	/**
	 * Adminstrator rights id
	 */
	public static final Rights ADMIN = Rights.ADMINISTRATOR;

	/**
	 * Moderator rights id
	 */
	public static final Rights MOD = Rights.MODERATOR;

	/**
	 * Pker rights id
	 */
	public static final Rights PKER = Rights.IRONMAN;
	
	/**
	 * The legendary donator id
	 */
	public static final Rights LEGENDARY_DONATOR = Rights.LEGENDARY_DONATOR;
	
	/**
	 * The extreme donator id
	 */
	public static final Rights EXTREME_DONATOR = Rights.EXTREME_DONATOR;

	/**
	 * Super donator id
	 */
	public static final Rights SUPER_DONATOR = Rights.SUPER_DONATOR;

	/**
	 * Donator rights id
	 */
	public static final Rights DONATOR = Rights.DONATOR;
	
	/**
	 * If everyone can use this command
	 */
	public static final Rights ALL = Rights.PLAYER;
}
