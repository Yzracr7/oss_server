package com.hyperion.game.content.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.hyperion.game.Constants;
import com.hyperion.game.content.InterfaceTools;
import com.hyperion.game.content.skills.magic.MagicBookHandler;
import com.hyperion.game.content.skills.magic.MagicBookHandler.TeleportType;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.data.magic.MagicEffects;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.game.world.entity.player.container.impl.Bank;
import com.hyperion.utility.Censor;
import com.hyperion.utility.Utils;
import com.hyperion.utility.game.ChatColors;
import com.hyperion.utility.game.Tile;
import com.hyperion.utility.json.JsonManager;
import com.hyperion.utility.json.impl.PunishmentLoader;
import com.hyperion.utility.json.sub.Punishment.PunishmentType;

public class PlayerCommands {
	
	public boolean useCommand(Player player, String fullCommand) {
		return PlayerCommand.useCommand(player, fullCommand);
	}
	
	public enum PlayerCommand {
		
		
		COMMANDS(new CommandRunnable() {
			public void run(Player player, String[] cmd) {
				List<CommandSkeleton> commands = CommandHandler.getCommands();
				Collections.sort(commands, new ComparatorImplementation());
				List<String> textToSend = new ArrayList<>();
				textToSend.add("<col=" + ChatColors.MAROON + ">Below are the list of commands available to you.");
				for (Iterator<CommandSkeleton> it$ = commands.listIterator(); it$.hasNext();) {
					CommandSkeleton command = it$.next();
					Rights req = CommandHandler.accessibleRightRequired(player, command);
					if (req == null) {
						StringBuilder bldr = new StringBuilder();
						String[] keys = command.getKeys();
						for (int i = 0; i < keys.length; i++) {
							String key = keys[i];
							bldr.append("::" + (key + "" + (i == keys.length - 1 ? "" : ", ")));
						}
						textToSend.add(bldr.toString());
					}			
				}
				InterfaceTools.sendQuestScroll(player, Constants.SERVER_NAME + " Commands", textToSend.toArray(new String[textToSend.size()]));
			}
			
			final class ComparatorImplementation implements Comparator<CommandSkeleton> {
				@Override
				public int compare(CommandSkeleton o1, CommandSkeleton o2) {
					Rights highestRight1 = null;
					int ordinal1 = -1;
					for (Rights right : o1.getRightsAllowed()) {
						if (ordinal1 == -1 || right.ordinal() < ordinal1) {
							ordinal1 = right.ordinal();
							highestRight1 = right;
						}
					}
					Rights highestRight2 = null;
					int ordinal2 = -1;
					for (Rights right : o2.getRightsAllowed()) {
						if (ordinal2 == -1 || right.ordinal() < ordinal1) {
							ordinal2 = right.ordinal();
							highestRight2 = right;
						}
					}
					return Integer.compare(highestRight1.ordinal(), highestRight2.ordinal());
				}
			}
			
		}),
		
		EMPTY(new CommandRunnable() {
			public void run(Player player, String[] cmd) {
				player.getInventory().clear();
		}}),
		
		FIXBANK(new CommandRunnable() {
			public void run(Player player, String[] cmd) {
				Bank.fixTabs(player);
		}}),
		
		MAXHIT(new CommandRunnable() {
			public void run(Player player, String[] cmd) {
				int weaponId = player.getEquipment().getItemInSlot(3);
				int ammo = player.getEquipment().getItemInSlot(13);
				player.getPacketSender().sendMessage("Melee maxhit: " + HitFormula.calculateMeleeMaxHit(player, null, weaponId, false, false) + ", spec: " + HitFormula.calculateMeleeMaxHit(player, null, weaponId, true, false));
				player.getPacketSender().sendMessage("Range maxhit: " + HitFormula.calculateRangeMaxHit(player, null, weaponId, ammo, false, false) + ", spec: " + HitFormula.calculateRangeMaxHit(player, null, weaponId, ammo, true, false));
				
				int spell = player.getAttributes().isSet("autocastspell") ? player.getAttributes().getInt("autocastspell") : 53;
				int max = MagicEffects.getMaxHit(player, spell, weaponId);
				
				player.getPacketSender().sendMessage("Magic maxhit(" + (spell == 53 ? "Ice Barrage" : "Autocast Spell " + spell) + "): " + max);
		}}),
		
		PLAYERS(new CommandRunnable() {
			public void run(Player player, String[] cmd) {
				int count = World.getWorld().getPlayerCount();
				String addon = count == 1 ? "is" : "are";
				String addon2 = count == 1 ? "" : "s";
				List<String> players = new ArrayList<>();
				for (Player p : World.getWorld().getPlayers()) {
					if (p == null) {
						continue;
					}
					players.add(p.getName());
				}
				InterfaceTools.sendQuestScroll(player, "Players Online", players.toArray(new String[players.size()]));
				player.getPacketSender().sendMessage("There " + addon + " currently " + count + " player" + addon2 + " online.");
		}}),
		
		STAFFONLINE(new CommandRunnable() {
			public void run(Player player, String[] cmd) {
				List<String> staff = new ArrayList<>();
				for (Player p : World.getWorld().getPlayers()) {
					if (p == null)
						continue;
					if(p.getDetails().isModerator() || p.getDetails().isAdmin() || p.getDetails().isOwner())
						staff.add(p.getName());
				}
				int count = staff.size();
				String addon = count == 1 ? "is" : "are";
				String addon2 = count == 1 ? "" : "s";
				InterfaceTools.sendQuestScroll(player, "Staff Members Online", staff.toArray(new String[count]));
				player.getPacketSender().sendMessage("There " + addon + " currently " + count + " staff member" + addon2 + " online.");
		}}, "staff"),
		
		YELL("", new CommandRunnable() {
			public void run(Player player, String[] cmd) {
				if(!player.getDetails().isDonator() && !player.getDetails().isStaff()) {
					player.getPacketSender().sendMessage("You must be a Donator to use the Yell Channel.");
					return;
				}
				if (JsonManager.getSingleton().getLoader(PunishmentLoader.class).isPunished(player.getDetails().getIP(), PunishmentType.IPMUTE) || JsonManager.getSingleton().getLoader(PunishmentLoader.class).isPunished(player.getName(), PunishmentType.MUTE)) {
					long duration = JsonManager.getSingleton().getLoader(PunishmentLoader.class).getPunishmentTime(player.getDetails().getIP(), PunishmentType.IPMUTE);
					if (duration == -1)
						duration = JsonManager.getSingleton().getLoader(PunishmentLoader.class).getPunishmentTime(player.getName(), PunishmentType.MUTE);
					player.getPacketSender().sendMessage("You are muted for " + (TimeUnit.MILLISECONDS.toHours(duration - System.currentTimeMillis())) + " more hours.");
					return;
				}
				String text = Utils.getCompleted(cmd, 1);
				String color =  player.getVariables().isRankHidden() ? "<col=3E7A02>" : player.getDetails().isAdmin() ? "<col=" + ChatColors.DARK_RED + ">" : player.getDetails().isModerator() ? "<col=" + ChatColors.DARK_GREEN + ">" :
					player.getDetails().isLegendaryDonator() ? "<col=66008F>":
						player.getDetails().isExtremeDonator() ? "<col=C90000>" :
							player.getDetails().isSuperDonator() ? "<col=0053BF>" :
								"<col=3E7A02>";
				for (Player p : World.getWorld().getPlayers()) {
					if (p != null) {
						if(player.getDetails().isCensorOn())
							text = Censor.filter(text);
						p.getPacketSender().sendMessage(rankTag[player.getDetails().getRights()] + "" + player.getName() + ":" + color + " " + (text.substring(0, 1).toUpperCase() + text.substring(1)));
					}
				}
			}
			
			final String rankTag[] = { "", 
				"<img=0><shad=1>[<col=" + ChatColors.DARK_GREEN + ">Moderator</col>] ", //Mod
				"<img=1><shad=1>[<col=" + ChatColors.DARK_RED + ">Administrator</col>] ", //Admin
				"", //Ironman
				"<img=3>[<col=3E7A02>Regular</col>] ", //Reg don
				"<img=4>[<col=C90000>Extreme</col>] ", //Extreme donator
				"Support ", //Support
				"<img=15>[<col=0053BF>Super</col>] ",//Super donator
				"<img=16>[<col=66008F>Legendary</col>] " //Legendary donator
			};
			
		});
		
		private String[] keys;
		private String useage;
		private CommandRunnable runnable;
		private List<Class<?>> classes;
		
		private static Map<String, PlayerCommand> commandMap = new HashMap<String, PlayerCommand>();
		
		static {
			for (PlayerCommand command : PlayerCommand.values()) {
				for(String key : command.getCommands()) {
					commandMap.put(key, command);
				}
			}
		}
		
		public static PlayerCommand forCommand(String command) {
			return commandMap.get(command);
		}
		
		private PlayerCommand(CommandRunnable runnable, String... altKeys) {
			this.keys = new String[1 + altKeys.length];
			keys[0] = name().toLowerCase();
			for(int i = 0; i < altKeys.length; i++) {
				keys[i + 1] = altKeys[i];
			}
			this.runnable = runnable;
			this.classes = null;
		}
		
		private PlayerCommand(String useage, CommandRunnable runnable, String... altKeys) {
			this.keys = new String[1 + altKeys.length];
			keys[0] = name().toLowerCase();
			for(int i = 0; i < altKeys.length; i++) {
				keys[i + 1] = altKeys[i];
			}
			this.useage = useage;
			this.runnable = runnable;
			this.classes = null;
		}
		
		private PlayerCommand(String key, String useage, CommandRunnable runnable, boolean safe, Class<?>... classes) {
			this.keys = new String[] {key};
			this.useage = useage;
			this.runnable = runnable;
			this.classes = Arrays.asList(classes);
		}
		
		private PlayerCommand(String[] keys, String useage, CommandRunnable runnable, boolean safe, Class<?>... classes) {
			this.keys = keys;
			this.useage = useage;
			this.runnable = runnable;
			this.classes = Arrays.asList(classes);
		}
		
		public String getCondensedCommand() {
			boolean condensedCommand = keys[0].length() < getFormalCommand().length();
			return getFormalCommand() + (condensedCommand ? ( " (" + getCommand() + ")" ) : "");
		}
		
		public String getFormalCommand() {
			return name().toLowerCase();
		}
		
		public String getCommand() {
			return keys[0];
		}
		
		public String[] getCommands() {
			return keys;
		}
		
		public String getUseage() {
			return "::"+getCommand() + " " + useage;
		}
		
		public CommandRunnable getRunnable() {
			return runnable;
		}
		
		public int getMinimumLength() {
			return 1 + classes.size();
		}
		
		public static boolean useCommand(Player player, String fullCommand) {
			return useCommand(player, fullCommand, false);
		}
		
		public static boolean useCommand(Player player, String fullCommand, boolean console) {
			String[] cmd = fullCommand.split(" ");
			PlayerCommand command = PlayerCommand.forCommand(cmd[0].toLowerCase());
			if (command == null)
				return false;
			//if (cmd.length < command.getMinimumLength()) {
			//	sendErrorMessage(player, console, "The command must look like: "+command.getUseage());
			//	return true;
			//}
			try {
				command.getRunnable().index = 1;
				command.getRunnable().run(player, cmd);
			} catch(Exception e) {
				sendErrorMessage(player, console, "The command must look like: "+command.getUseage());
				sendErrorMessage(player, console, "Error: "+e.getMessage());
			}
			return true;
		}
		
		public static boolean isCommand(String fullCommand) {
			String[] cmd = fullCommand.split(" ");
			PlayerCommand command = PlayerCommand.forCommand(cmd[0].toLowerCase());
			if (command == null)
				return false;
			return true;
		}
		
		public static void sendErrorMessage(Player player, boolean console, String message) {
			player.sm(message);
		}
	}
}
