package com.hyperion.game.content.commands.admin;

import java.util.concurrent.TimeUnit;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.utility.ProtocolUtils;
import com.hyperion.utility.Utils;
import com.hyperion.utility.game.InputAction;
import com.hyperion.utility.json.JsonManager;
import com.hyperion.utility.json.impl.PunishmentLoader;
import com.hyperion.utility.json.sub.Punishment.PunishmentType;

public class MacBan implements CommandSkeleton {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "macban" };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { ADMIN };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion
	 * .game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(final Player player, String[] cmd) {
		final String user = Utils.getCommandText(cmd);
		final Player target = World.getWorld().find_player_by_name(user);
		if (target == null) {
			player.getPacketSender().sendMessage("No such player by name: " + user);
			return;
		}
		player.getPacketSender().sendMessage("You have just macbanned " + user);
		final String address = ProtocolUtils.getMacAddress(target);
		if (address == "")
			return;
		for (Player pl : World.getWorld().getPlayers()) {
			if (pl == null) {
				continue;
			}
			if (pl.getMacAddress() != "" && pl.getMacAddress().equalsIgnoreCase(address)) {
				pl.getPacketSender().forceLogout();
			}
		}

		player.getPacketSender().requestIntegerInput(new InputAction<Integer>("How many days?") {
			@Override
			public void handleInput(Integer value) {
				JsonManager.getSingleton().getLoader(PunishmentLoader.class).addPunishment(target.getName() + ":" + address, PunishmentType.MACBAN, TimeUnit.DAYS.toMillis(value));
				player.getPacketSender().sendMessage("You have just MAC banned " + user + " for " + value + " days");
			}
		});
	}

}
