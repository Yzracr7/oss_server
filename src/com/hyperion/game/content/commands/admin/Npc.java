package com.hyperion.game.content.commands.admin;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class Npc implements CommandSkeleton {

    /* (non-Javadoc)
     * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
     */
    @Override
    public String[] getKeys() {
        return new String[] { "npc" };
    }

    /* (non-Javadoc)
     * @see com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
     */
    @Override
    public Rights[] getRightsAllowed() {
        return new Rights[] { ADMIN };
    }

    /* (non-Javadoc)
     * @see com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion.game.world.entity.player.Player, java.lang.String[])
     */
    @Override
    public void handle(Player player, String[] cmd) {
        NPC npc = new NPC(Integer.valueOf(cmd[1]));
        npc.setLocation(player.getLocation());
        npc.setLastKnownRegion(player.getLocation());
        player.getVariables().addOwnedNPC(npc);
        World.getWorld().addNPC(npc);
    }

}
