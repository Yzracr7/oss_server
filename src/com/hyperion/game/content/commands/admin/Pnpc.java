package com.hyperion.game.content.commands.admin;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class Pnpc implements CommandSkeleton {

    /* (non-Javadoc)
     * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
     */
    @Override
    public String[] getKeys() {
        return new String[]{"pnpc"};
    }

    /* (non-Javadoc)
     * @see com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
     */
    @Override
    public Rights[] getRightsAllowed() {
        return new Rights[]{ADMIN};
    }

    /* (non-Javadoc)
     * @see com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion.game.world.entity.player.Player, java.lang.String[])
     */
    @Override
    public void handle(Player player, String[] cmd) {
        int npc = Integer.valueOf(cmd[1]);//Constants.USE_DEV_CACHE ? Integer.valueOf(cmd[1]) : NPCDefinition.renderIds[Integer.valueOf(cmd[1])];
        System.out.println("npc id: " + Integer.valueOf(cmd[1]) + ", switched: " + npc);
        player.getPacketSender().transformToNpc(player, npc);
    }

}
