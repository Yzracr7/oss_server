package com.hyperion.game.content.commands.admin;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

public class SwitchMagic implements CommandSkeleton {

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "switchmagic", "switchmage", "switchspells" };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { ADMIN };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion.game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		if (Integer.valueOf(cmd[1]) == 2) {
			if (player.getSkills().getLevelForXp(1) < 40) {
				player.getPacketSender().sendMessage("You need 40 Defence to use lunars.");
				return;
			}
			player.getPacketSender().sendTab(92, 430);
			player.getSettings().setMagicType(3);
		} else if (Integer.valueOf(cmd[1]) == 1) {
			player.getSettings().setMagicType(2);
			player.getPacketSender().sendTab(92, 193);
		} else if (Integer.valueOf(cmd[1]) == 0) {
			player.getPacketSender().sendTab(92, 665);
			player.getSettings().setMagicType(1);
		}
	}

}
