package com.hyperion.game.content.commands.admin;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class TestGfx implements CommandSkeleton {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "testgfx" };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { ADMIN };
	}
	
	public int i;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion
	 * .game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		player.getAppearance().setInvisible(true);
		player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
		i = 0;
		World.getWorld().submit(new Tickable(4) {
            @Override
            public void execute() {
                //this.stop();
                player.playForcedChat("GFX: " + i + " - " + (i + 15));
                for (int x = -2; x < 3; x++) {
        			player.getPacketSender().sendStillGraphics(new Location(player.getLocation().getX() + x, player.getLocation().getY() + 3, player.getLocation().getZ()), Graphic.create(i), 0);
        			i++;
        			player.getPacketSender().sendStillGraphics(new Location(player.getLocation().getX() + x, player.getLocation().getY() + 1, player.getLocation().getZ()), Graphic.create(i), 0);
        			i++;
        			player.getPacketSender().sendStillGraphics(new Location(player.getLocation().getX() + x, player.getLocation().getY() - 1, player.getLocation().getZ()), Graphic.create(i), 0);
        			i++;
        		}
            }
        });
	}

}
