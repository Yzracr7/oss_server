package com.hyperion.game.content.commands.admin;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.utility.Utils;
import com.hyperion.utility.json.JsonManager;
import com.hyperion.utility.json.impl.PunishmentLoader;
import com.hyperion.utility.json.sub.Punishment.PunishmentType;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class UnIPBan implements CommandSkeleton {

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "unipban" };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { ADMIN };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion.game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		String user = Utils.getCommandText(cmd);
		Player target = World.getWorld().find_player_by_name(user);
		if (target != null) {
			player.getPacketSender().sendMessage("You can't unipban an online player.");
			return;
		}
		if (!JsonManager.getSingleton().getLoader(PunishmentLoader.class).removePunishment(user, PunishmentType.IPBAN)) {
			player.getPacketSender().sendMessage("Unable to remove punishment for " + user + ", may not exist.");
		} else {
			player.getPacketSender().sendMessage("You have unipbanned " + user);
		}
	}

}
