package com.hyperion.game.content.commands.mod;

import java.util.concurrent.TimeUnit;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.utility.Utils;
import com.hyperion.utility.game.InputAction;
import com.hyperion.utility.json.JsonManager;
import com.hyperion.utility.json.impl.PunishmentLoader;
import com.hyperion.utility.json.sub.Punishment.PunishmentType;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class Ban implements CommandSkeleton {

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "ban" };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { MOD };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion.game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(final Player player, String[] cmd) {
		final String user = Utils.getCommandText(cmd);
		final Player target = World.getWorld().find_player_by_name(user);
		System.out.println(System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(30));
		player.getPacketSender().requestIntegerInput(new InputAction<Integer>("How many days?") {
			@Override
			public void handleInput(Integer value) {
				if (target != null) {
					target.getPacketSender().forceLogout();
				}
				JsonManager.getSingleton().getLoader(PunishmentLoader.class).addPunishment(user, PunishmentType.BAN, TimeUnit.DAYS.toMillis(value));
				player.getPacketSender().sendMessage("You have just banned " + user + " for " + value + " days");
			}
		});
	}

}
