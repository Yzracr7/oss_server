package com.hyperion.game.content.commands.mod;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.utility.Utils;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Apr 5, 2015
 */
public class CheckBank implements CommandSkeleton {

	@Override
	public String[] getKeys() {
		return new String[] { "checkbank" };
	}

	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { MOD };
	}

	@Override
	public void handle(Player player, String[] cmd) {
		String user = Utils.getCommandText(cmd);
		if(user.length() <= 0) {
			System.out.println("Usage: ::checkbank username");
			return;
		}
		Player other = World.getWorld().find_player_by_name(user);
		if (other == null) {
			player.getPacketSender().sendMessage("Could not find player " + user);
			return;
		}
		player.getPacketSender().displayInterface(12);
		player.getPacketSender().sendItems(12, 7, -1, other.getBank().getItems());
	}

}
