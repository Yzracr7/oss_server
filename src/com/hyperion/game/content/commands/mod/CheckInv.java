package com.hyperion.game.content.commands.mod;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.utility.Utils;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Apr 5, 2015
 */
public class CheckInv implements CommandSkeleton {

	@Override
	public String[] getKeys() {
		return new String[] { "checkinv", "checkinvy" };
	}

	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { MOD };
	}

	@Override
	public void handle(Player player, String[] cmd) {
		String user = Utils.getCommandText(cmd);
		Player other = World.getWorld().find_player_by_name(user);
		if (other == null) {
			player.getPacketSender().sendMessage("Could not find player " + user);
			return;
		}
		player.getPacketSender().displayInterface(11);
		player.getPacketSender().sendItems(11, 61, 91, other.getInventory().getItems());
		player.getPacketSender().modifyText(other.getName() + "'s Inventory", 11, 60);
	}

}
