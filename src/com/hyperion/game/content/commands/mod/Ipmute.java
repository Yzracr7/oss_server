package com.hyperion.game.content.commands.mod;

import java.util.concurrent.TimeUnit;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.utility.Utils;
import com.hyperion.utility.game.InputAction;
import com.hyperion.utility.json.JsonManager;
import com.hyperion.utility.json.impl.PunishmentLoader;
import com.hyperion.utility.json.sub.Punishment.PunishmentType;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class Ipmute implements CommandSkeleton {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "ipmute" };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { MOD };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion
	 * .game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		String user = Utils.getCommandText(cmd);
		Player target = World.getWorld().find_player_by_name(user);
		if (target == null) {
			player.getPacketSender().sendMessage("No such player: " + user);
			return;
		}
		target.getPacketSender().sendMessage(player.getName() + " just ipmuted you.");
		player.getPacketSender().requestIntegerInput(new InputAction<Integer>("How many days?") {
			@Override
			public void handleInput(Integer value) {
				JsonManager.getSingleton().getLoader(PunishmentLoader.class).addPunishment(target.getDetails().getIP(), PunishmentType.IPMUTE, TimeUnit.DAYS.toMillis(value));
				player.getPacketSender().sendMessage("You have just ipmuted " + user + " for " + value + " days");
			}
		});
	}

}
