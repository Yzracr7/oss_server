package com.hyperion.game.content.commands.mod;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.utility.Utils;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class Jail implements CommandSkeleton {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "jail" };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { MOD };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion
	 * .game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		String user = Utils.getCommandText(cmd);
		Player other = World.getWorld().find_player_by_name(user);
		if (other != null) {
			if (other.getSession() != null && other.getSession().getRemoteAddress() != null) {
				Location loc = null;
				int random = Utils.random(0, 3);
				switch (random) {
				case 0:
					loc = Location.create(3013, 3189, 0);
					break;
				case 1:
					loc = Location.create(3013, 3192, 0);
					break;
				case 2:
					loc = Location.create(3013, 3195, 0);
					break;
				case 3:
					loc = Location.create(3018, 3188, 0);
					break;
				default:
					loc = Location.create(3013, 3189, 0);
					break;
				}
				other.setLocation(loc);
				other.getVariables().setNoTeleport(true);
				other.getPacketSender().sendMessage("You have been jailed by: " + player.getName());
				player.getPacketSender().sendMessage("You have jailed: " + other.getName());
				return;
			}
		}
		player.getPacketSender().sendMessage("Could not find player " + user);
	}

}
