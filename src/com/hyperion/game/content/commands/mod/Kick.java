package com.hyperion.game.content.commands.mod;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.sql.login.LoginManager;
import com.hyperion.utility.Utils;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class Kick implements CommandSkeleton {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "kick" };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { MOD };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion
	 * .game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		String user = Utils.getCommandText(cmd);
		Player other = World.getWorld().find_player_by_name(user);
		if (other == null) {
			player.getPacketSender().sendMessage("Could not find player " + user);
			return;
		}
		//other.getPacketSender().forceLogout();
		LoginManager.unregister_player(other);
		player.getPacketSender().sendMessage("Kicked " + user + " successfully");
	}

}
