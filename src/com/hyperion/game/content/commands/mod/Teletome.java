package com.hyperion.game.content.commands.mod;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.utility.Utils;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class Teletome implements CommandSkeleton {

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "teletome" };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { MOD };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion.game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		String text = Utils.getCommandText(cmd);
		Player other = World.getWorld().find_player_by_name(text);
		if (other != null) {
			other.teleport(player.getLocation());
			player.getPacketSender().sendMessage("You teleported " + text + " to you.");
			other.getPacketSender().sendMessage("You have been teleported to " + player.getDetails().getName() + ".");
		} else {
			player.getPacketSender().sendMessage("Can't find player.");
		}
	}

}
