package com.hyperion.game.content.commands.mod;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.utility.Utils;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class Teletoplayer implements CommandSkeleton {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "teleto" };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { MOD };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion
	 * .game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		String text = Utils.getCommandText(cmd);
		Player other = World.getWorld().find_player_by_name(text);
		if (other != null) {
			player.getPacketSender().sendMessage("You teleported to " + text + /*"., ip: " + other.getSession().getRemoteAddress()*/"");
			player.teleport(other.getLocation());
			if(!player.getAppearance().isInvisible())
				other.getPacketSender().sendMessage("" + player.getDetails().getName() + " teleported to you.");
		} else {
			player.getPacketSender().sendMessage("Can't find player: " + text);
		}
	}

}
