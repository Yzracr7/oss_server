package com.hyperion.game.content.commands.mod;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.utility.Utils;
import com.hyperion.utility.json.JsonManager;
import com.hyperion.utility.json.impl.PunishmentLoader;
import com.hyperion.utility.json.sub.Punishment.PunishmentType;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class Unban implements CommandSkeleton {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "unban" };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { MOD };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion
	 * .game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		String user = Utils.getCommandText(cmd);
		if (!JsonManager.getSingleton().getLoader(PunishmentLoader.class).removePunishment(user, PunishmentType.BAN)) {
			player.getPacketSender().sendMessage("No such punishment existed for " + user);
		} else {
			player.getPacketSender().sendMessage("Successfully unbanned " + user);
		}
	}

}
