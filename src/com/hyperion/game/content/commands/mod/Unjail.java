package com.hyperion.game.content.commands.mod;

import com.hyperion.game.Constants;
import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.utility.Utils;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class Unjail implements CommandSkeleton {

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "unjail" };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { MOD };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion.game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		String user = Utils.getCommandText(cmd);
		Player other = World.getWorld().find_player_by_name(user);
		if (other != null) {
			if (other.getSession() != null && other.getSession().getRemoteAddress() != null) {
				other.setLocation(Constants.RESPAWN_LOCATION);
				other.getVariables().setNoTeleport(false);
				other.getPacketSender().sendMessage("You have been unjailed by: " + player.getName());
				return;
			}
		}
		player.getPacketSender().sendMessage("Could not find player " + user);
	}

}
