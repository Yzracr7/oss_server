package com.hyperion.game.content.commands.owner;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Apr 4, 2015
 */
public class AddPestPointsO implements CommandSkeleton {

	@Override
	public String[] getKeys() {
		return new String[] { "addpestpo", "appo" };
	}

	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { OWNER };
	}

	@Override
	public void handle(Player player, String[] cmd) {
		String user = cmd[1].replaceAll("_", " ");
		int points = Integer.parseInt(cmd[2]);
		Player other = World.getWorld().find_player_by_name(user);
		if (other == null) {
			player.getPacketSender().sendMessage("Could not find player " + user);
			return;
		}
		other.getVariables().increasePestPoints(points);
		player.getPacketSender().sendMessage("Added " + points + " pest points to " + user);
	}

}
