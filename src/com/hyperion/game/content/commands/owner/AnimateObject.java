package com.hyperion.game.content.commands.owner;

import java.util.ArrayList;
import java.util.List;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.game.world.region.Region;

public class AnimateObject implements CommandSkeleton {

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "animateobj", "animateobject", "objanim" };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { OWNER };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion.game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		if(cmd.length < 2) {
			player.getPacketSender().sendMessage("Usage: ::animateobj id anim");
			return;
		}
		int id = Integer.parseInt(cmd[1]);
		int anim = Integer.parseInt(cmd[2]);

		Region region = World.getWorld().getRegion(player.getLocation().getRegionId(), false);
		if (region == null) {
			throw new RuntimeException();
		}
		List<GameObject> objects = new ArrayList<>(region.getAllObjects());
		List<GameObject> spawnedObjects = new ArrayList<>(region.getSpawnedObjects());
		for(GameObject o : objects) {
			if(o.getId() == id) {
				player.getPacketSender().sendMessage("Playing anim " + anim + " on object at: " + o.getLocation().toString() + " with id: " + id);
				player.getPacketSender().sendObjectAnimation(anim, o);
			} else {
				int distance = o.getLocation().distanceToPoint(player.getLocation());
				if(distance < 2)
					System.out.println("Object not same id, distance: " + distance + " and id: " + o.getId());
			}
		}
		for(GameObject o : spawnedObjects) {
			if(o.getId() == id) {
				player.getPacketSender().sendMessage("Playing anim " + anim + " on spawned object at: " + o.getLocation().toString() + " with id: " + id);
				player.getPacketSender().sendObjectAnimation(anim, o);
			} else {
				int distance = o.getLocation().distanceToPoint(player.getLocation());
				if(distance < 2)
					System.out.println("Spawned Object not same id, distance: " + distance + " and id: " + o.getId());
			}
		}
	}

}
