package com.hyperion.game.content.commands.owner;

import com.hyperion.cache.definitions.CachedAnimationDefinition;
import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 25, 2015
 */
public class AnimationDebug implements CommandSkeleton {

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "ad" };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { OWNER };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion.game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		int animId = Integer.parseInt(cmd[1]);
		player.animate(animId);
		CachedAnimationDefinition anim = CachedAnimationDefinition.forId(animId);
		System.out.println("[animId=" + animId + ", emoteTime=" + anim.getEmoteTime() + ", priority=" + anim.priority + "]");
	}

}
