package com.hyperion.game.content.commands.owner;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Apr 5, 2015
 */
public class BotTest implements CommandSkeleton {

	@Override
	public String[] getKeys() {
		return new String[] { "bottest" };
	}

	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { OWNER };
	}

	@Override
	public void handle(Player player, String[] cmd) {
		
	}

}
