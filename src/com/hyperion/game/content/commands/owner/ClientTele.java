package com.hyperion.game.content.commands.owner;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class ClientTele implements CommandSkeleton {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getIdentifiers()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "tele" };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.commands.CommandSkeleton#rightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { OWNER };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion
	 * .game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		int x, y, z;
		if(cmd.length < 2) {
			player.getPacketSender().sendMessage("Usage: ::tele x y z");
			return;
		}
		if (cmd[1].contains(",")) {
			String[] args = cmd[1].split(",");
			x = Integer.parseInt(args[1]) << 6 | Integer.parseInt(args[3]);
			y = Integer.parseInt(args[2]) << 6 | Integer.parseInt(args[4]);
			z = Integer.parseInt(args[0]);
		} else {
			x = Integer.parseInt(cmd[1]);
			y = Integer.parseInt(cmd[2]);
			z = player.getLocation().getZ();
			if (cmd.length > 3) {
				z = Integer.parseInt(cmd[3]);
			}
		}
		player.teleport(x, y, z);
	}

}
