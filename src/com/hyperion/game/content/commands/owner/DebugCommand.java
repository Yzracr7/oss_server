package com.hyperion.game.content.commands.owner;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 28, 2015
 */
public class DebugCommand implements CommandSkeleton {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "dbg" };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { OWNER };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion
	 * .game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		int[] RULE_CONFIGS = {
				1, // no forfeit
				2, // no movement
				16, // no range
				32, // no melee
				64, // no magic
				128, // no drinks
				256, // no food
				512, // no prayer
				1024, // obstacles
				4096, // fun weapons
				8192, // special attacks
				8192 * 8, // amulet
				131072, // weapon
				262144, // body
				524288, // shield
				2097152, // legs
				8388608, // gloves
				16777216,// boots
				67108864, // ring
				134217728, // arrows
				268435456, // summoning
				16384, // Hat
				32768, // Cape
			};
			
			
			World.getWorld().submit(new Tickable(1) {
				int config = 2;
				int minus = 0;
				@Override
				public void execute() {
					System.out.println("Id: " + config + " minus " + minus);
					config = config * 2;
					int toSend = config - minus;
					player.getPacketSender().sendConfig(643, toSend);
					if(config == 268435456 && minus <= 7) {
						minus += 1;
						config = 2;
					} else if (minus >= 7) {
						this.stop();
					}
						
				}
			});
			
		}
		

}
