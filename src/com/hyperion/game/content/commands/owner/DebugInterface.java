package com.hyperion.game.content.commands.owner;

import com.hyperion.game.content.InterfaceTools;
import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 25, 2015
 */
public class DebugInterface implements CommandSkeleton{

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "dbi" };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { OWNER };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion.game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		if(cmd.length < 2) {
			player.getPacketSender().sendMessage("Usage: ::dbi id");
			return;
		}
		int interfaceId = Integer.parseInt(cmd[1]);
		int components = InterfaceTools.getInterfaceDefinitionsComponentsSize(interfaceId);
		for (int i = 0; i < components; i++) {
			player.getPacketSender().modifyText("" + i, interfaceId, i);
		}
		player.getPacketSender().displayInterface(interfaceId);
		System.out.println("Interface " + interfaceId + " has " + components + " components.");
	}

}
