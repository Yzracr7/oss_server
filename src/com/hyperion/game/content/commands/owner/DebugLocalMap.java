package com.hyperion.game.content.commands.owner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.game.world.region.Region;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class DebugLocalMap implements CommandSkeleton {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "dlm", "listobjects" };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { OWNER };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion
	 * .game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(final Player player, String[] cmd) {
		Region region = World.getWorld().getRegion(player.getLocation().getRegionId(), false);
		if (region == null) {
			throw new RuntimeException();
		}
		List<GameObject> objects = new ArrayList<>(region.getAllObjects());
		Collections.sort(objects, (o1, o2) -> Integer.compare(o1.getLocation().getDistance2(player.getLocation()), o2.getLocation().getDistance2(player.getLocation())));
		objects = objects.stream().filter(object -> object.getLocation().withinActualDistance(player.getLocation(), 3)).collect(Collectors.toList());
		objects.stream().forEach(obj -> {
			System.out.println("Object: [name=" + obj.getName() + ", id=" + obj.getId() + ", location=" + obj.getLocation() + ", type=" + obj.getType() + ", rotation=" + obj.getRotation() + ", distance=" + obj.getLocation().getDistance2(player.getLocation()) + "]");
		});
		
	}

}
