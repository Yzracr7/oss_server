package com.hyperion.game.content.commands.owner;

import com.hyperion.cache.definitions.CachedItemDefinition;
import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.item.Item;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.game.world.grounditems.GroundItem;
import com.hyperion.game.world.grounditems.GroundItemManager;
import com.hyperion.utility.Utils;
import com.hyperion.utility.game.ChatColors;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Apr 5, 2015
 */
public class DropItem implements CommandSkeleton {

	@Override
	public String[] getKeys() {
		return new String[] { "eventitemdrop" };
	}

	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { OWNER };
	}

	@Override
	public void handle(Player player, String[] cmd) {
		try {
			int itemId = Integer.parseInt(cmd[1]);
			int itemAmount = Integer.parseInt(cmd[2]);
			String locationName = Utils.getCompleted(cmd, 3);
			String itemName = CachedItemDefinition.forId(itemId).getName();
			GroundItemManager.create(new GroundItem(new Item(itemId, itemAmount), player).setSpawnDelay(-1).setGlobal(true).setRemovalDelay(-1), player);
			World.getWorld().sendGlobalMessage("[<col=" + ChatColors.BLUE + ">Event</col>]<col=" + ChatColors.MAROON + ">" + itemAmount + "x " + itemName + " were dropped at " + locationName, false);
		} catch (Exception e) {
			player.getPacketSender().sendMessage("Usage ::eventitemdrop [id] [amt] [location] (name can be as long as u want)");
		}
	}

}
