package com.hyperion.game.content.commands.owner;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.game.world.pathfinders.Directions.NormalDirection;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class DumpNpcSpawn implements CommandSkeleton {

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "n" };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { OWNER };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion.game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		int id = Integer.valueOf(cmd[1]);
		int x = player.getLocation().getX();
		int y = player.getLocation().getY();
		int z = player.getLocation().getZ();
		boolean walk = Integer.valueOf(cmd[2]) == 1;
		String dir = NormalDirection.forStringValue(cmd[3].toUpperCase()).stringValue();
		PrintWriter print = null;
		try {
			print = new PrintWriter(new FileWriter("data/world/npcs/spawnsout.xml", true), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		print.println("<npcSpawn>");
		print.println("\t<id>" + id + "</id>");
		print.println("\t<x>" + x + "</x>");
		print.println("\t<y>" + y + "</y>");
		print.println("\t<height>" + z + "</height>");
		print.println("\t<canWalk>" + walk + "</canWalk>");
		print.println("\t<direction>" + dir + "</direction>");
		print.println("</npcSpawn>");
		print.flush();
		print.close();
	}

}
