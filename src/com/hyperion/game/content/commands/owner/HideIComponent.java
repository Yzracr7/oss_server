package com.hyperion.game.content.commands.owner;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

public class HideIComponent implements CommandSkeleton {

	@Override
	public String[] getKeys() {
		return new String[] { "hidei" };
	}

	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { OWNER };
	}

	@Override
	public void handle(Player player, String[] cmd) {
		if (cmd == null || cmd[1] == null || cmd[2] == null || cmd[3] == null) {
			player.getPacketSender().sendMessage("Usage: ::hidei inter component 0/1 (0 = show, 1 = hide)");
			return;
		}
		try {
			player.getPacketSender().sendInterfaceConfig(Integer.valueOf(cmd[1]), Integer.valueOf(cmd[2]), Integer.valueOf(cmd[3]) == 0 ? true : false);
		} catch (Exception e) {

		}
	}

}