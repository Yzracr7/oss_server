package com.hyperion.game.content.commands.owner;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.entity.npc.impl.zulrah.ZulrahBossRoom;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

public class Invisible implements CommandSkeleton {

    /*
     * (non-Javadoc)
     *
     * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
     */
    @Override
    public String[] getKeys() {
        return new String[]{"invisible"};
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
     */
    @Override
    public Rights[] getRightsAllowed() {
        return new Rights[]{OWNER};
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion
     * .game.world.entity.player.Player, java.lang.String[])
     */
    @Override
    public void handle(Player player, String[] cmd) {
        //player.getAppearance().setInvisible(player.getAppearance().isInvisible() ? false : true);
        //player.getPacketSender().sendMessage("You are now " + (player.getAppearance().isInvisible() ? "invisible." : "visible."));
        //player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
        ZulrahBossRoom zul = new ZulrahBossRoom();
        zul.enterInstancedRoom(player);
    }
}
