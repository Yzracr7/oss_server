package com.hyperion.game.content.commands.owner;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.utility.Utils;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class Itemn implements CommandSkeleton {

    /*
     * (non-Javadoc)
     *
     * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
     */
    @Override
    public String[] getKeys() {
        return new String[]{"itemn"};
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
     */
    @Override
    public Rights[] getRightsAllowed() {
        return new Rights[]{ADMIN};
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion
     * .game.world.entity.player.Player, java.lang.String[])
     */
    @Override
    public void handle(Player player, String[] cmd) {
        String nameToFind = Utils.getCommandText(cmd);
        int count = 0;
        StringBuilder bldr = new StringBuilder();
        for (int i = 0; i < ItemDefinition.DEFINITIONS.length; i++) {
            ItemDefinition def = ItemDefinition.forId(i);
            if (def == null || def.getName() == null || def.isNoted()) {
                continue;
            }
            if (def.getName().toLowerCase().contains(nameToFind.toLowerCase())) {
                player.getPacketSender().sendMessage("Found item: [itemId=" + i + ", name=" + def.getName() + "]");
                bldr.append(i + ", ");
                count++;
            }
        }
        System.out.println(bldr);
        player.getPacketSender().sendMessage("Found " + count + " results by name: " + nameToFind);
    }

}
