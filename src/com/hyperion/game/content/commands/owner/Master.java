package com.hyperion.game.content.commands.owner;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.game.world.entity.player.Skills;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class Master implements CommandSkeleton {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "master" };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { OWNER };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion
	 * .game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		for (int i = 0; i < Skills.SKILL_COUNT; i++) {
			player.getSkills().setLevel(i, 99);
			player.getSkills().setXp(i, player.getSkills().getXpForLevel(99));
		}
		player.getPacketSender().sendSkillLevels();
		player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
	}
}
