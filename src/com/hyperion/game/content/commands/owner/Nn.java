package com.hyperion.game.content.commands.owner;

import com.hyperion.cache.Cache;
import com.hyperion.cache.definitions.CachedNpcDefinition;
import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.utility.Utils;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 28, 2015
 */
public class Nn implements CommandSkeleton {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "nn" };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { Rights.OWNER };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion
	 * .game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		String nameToFind = Utils.getCommandText(cmd);
		int count = 0;
		StringBuilder bldr = new StringBuilder();
		for (int npcId = 0; npcId < Cache.getTotalNPCCount(); npcId++) {
			CachedNpcDefinition def = CachedNpcDefinition.getNPCDefinitions(npcId);
			if (def == null) {
				continue;
			}
			if (def.getName().toLowerCase().contains(nameToFind.toLowerCase())) {
				player.getPacketSender().sendMessage("Found npc: [npcId=" + npcId + ", name=" + def.getName() + "]");
				bldr.append(npcId + ", ");
				count++;
			}
		}
		player.getPacketSender().sendMessage("Found " + count + " results by name: " + nameToFind);
	}

}
