package com.hyperion.game.content.commands.owner;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

public class PlaySound implements CommandSkeleton {

    /* (non-Javadoc)
     * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
     */
    @Override
    public String[] getKeys() {
        return new String[]{"sound"};
    }

    /* (non-Javadoc)
     * @see com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
     */
    @Override
    public Rights[] getRightsAllowed() {
        return new Rights[]{OWNER};
    }

    /* (non-Javadoc)
     * @see com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion.game.world.entity.player.Player, java.lang.String[])
     */
    @Override
    public void handle(Player player, String[] cmd) {
        if (cmd[1] == null) {
            player.getPacketSender().sendMessage("Usage ::sound id");
            return;
        }
        player.getPacketSender().sendSoundEffect(Integer.parseInt(cmd[1]), Integer.parseInt(cmd[2]), Integer.parseInt(cmd[3]));
        player.getPacketSender().sendMessage("Playing music effect: " + Integer.parseInt(cmd[1]));
    }

}
