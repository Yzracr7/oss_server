package com.hyperion.game.content.commands.owner;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

public class SendConfig implements CommandSkeleton {

	@Override
	public String[] getKeys() {
		return new String[] { "config" };
	}

	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { OWNER };
	}

	@Override
	public void handle(Player player, String[] cmd) {
		if (cmd == null || cmd[1] == null || cmd[2] == null) {
			player.getPacketSender().sendMessage("Usage: ::config id value");
			return;
		}
		try {
			player.getPacketSender().sendConfig(Integer.valueOf(cmd[1]), Integer.valueOf(cmd[2]));
			player.getPacketSender().sendMessage("Sending config " + Integer.valueOf(cmd[1]) + " value " + Integer.valueOf(cmd[2]));
		} catch (Exception e) {

		}
	}

}