package com.hyperion.game.content.commands.owner;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class SetLevel implements CommandSkeleton {

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "setlevel" };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { OWNER };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion.game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		if (cmd[1] == null || cmd == null) {
			player.getPacketSender().sendMessage("You did not specify a skill ID.");
			return;
		}
		if (Integer.valueOf(cmd[2]) < 10 && Integer.valueOf(cmd[1]) == 3) {
			player.getPacketSender().sendMessage("You cannot make this level less than 10.");
			return;
		}
		if (Integer.valueOf(cmd[2]) > 99) {
			player.getPacketSender().sendMessage("You cannot make this level any higher.");
			return;
		}
		if (Integer.valueOf(cmd[2]) < 1) {
			player.getPacketSender().sendMessage("You cannot make this level any lower.");
			return;
		}
		try {
			player.getSkills().setLevel(Integer.valueOf(cmd[1]), Integer.valueOf(cmd[2]));
			player.getSkills().setXp(Integer.valueOf(cmd[1]), player.getSkills().getXpForLevel(Integer.valueOf(cmd[2])));
		} catch (Exception e) {

		}
		player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
		player.getPacketSender().sendSkillLevel(Integer.valueOf(cmd[1]));
	}

}
