package com.hyperion.game.content.commands.owner;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class SetLevelOther implements CommandSkeleton {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "setlevelo" };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { OWNER };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion
	 * .game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		if (cmd[1] == null) {
			player.getPacketSender().sendMessage("You did not specify a player name.");
			return;
		}
		Player other = World.getWorld().find_player_by_name(cmd[1]);
		if (other == null) {
			player.getPacketSender().sendMessage("Other player " + cmd[1] + " cannot be found.");
			return;
		}
		if (cmd[2] == null || cmd == null) {
			player.getPacketSender().sendMessage("You did not specify a skill ID.");
			return;
		}
		if (Integer.valueOf(cmd[3]) < 10 && Integer.valueOf(cmd[2]) == 3) {
			player.getPacketSender().sendMessage("You cannot make this level less than 10.");
			return;
		}
		if (Integer.valueOf(cmd[3]) > 99) {
			player.getPacketSender().sendMessage("You cannot make this level any higher.");
			return;
		}
		if (Integer.valueOf(cmd[3]) < 1) {
			player.getPacketSender().sendMessage("You cannot make this level any lower.");
			return;
		}
		try {
			other.getSkills().setLevel(Integer.valueOf(cmd[2]), Integer.valueOf(cmd[3]));
			other.getSkills().setXp(Integer.valueOf(cmd[2]), player.getSkills().getXpForLevel(Integer.valueOf(cmd[3])));

		} catch (Exception e) {

		}
		other.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
		other.getPacketSender().sendSkillLevel(Integer.valueOf(cmd[1]));
		player.getPacketSender().sendMessage("Other player's skill " + Integer.valueOf(cmd[2]) + " set to level " + Integer.valueOf(cmd[3]));
		other.getPacketSender().sendMessage("Skill " + Integer.valueOf(cmd[2]) + " set to level " + Integer.valueOf(cmd[3]));

	}

}
