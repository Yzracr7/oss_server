package com.hyperion.game.content.commands.owner;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

public class SetLoyaltyPoints implements CommandSkeleton {

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "setloyaltypoints", "setlp" };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { OWNER };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion.game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		if (cmd[1] == null || cmd == null) {
			player.getPacketSender().sendMessage("You did not specify an amount.");
			return;
		}
		try {
			player.getVariables().setLoyaltyPoints(Integer.valueOf(cmd[1]));
			player.getPacketSender().sendMessage("Your loyalty points have been set to: " + Integer.valueOf(cmd[1]));
		} catch (Exception e) {

		}
	}

}
