package com.hyperion.game.content.commands.owner;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Apr 4, 2015
 */
public class SetVotePoints implements CommandSkeleton {

	@Override
	public String[] getKeys() {
		return new String[] { "svp"};
	}

	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { OWNER };
	}

	@Override
	public void handle(Player player, String[] cmd) {
		player.getVariables().setVotePoints(Integer.parseInt(cmd[1]));
	}

}
