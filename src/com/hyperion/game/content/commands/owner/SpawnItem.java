package com.hyperion.game.content.commands.owner;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.item.Item;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class SpawnItem implements CommandSkeleton {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "item", "pickup", "spawnitem" };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { OWNER };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion
	 * .game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		int itemId = Integer.valueOf(cmd[1]);
		ItemDefinition def = ItemDefinition.forId(itemId);
		if (def == null) {
			return;
		}
		if (def.getName() != null && def.getName().equalsIgnoreCase("null")) {
			player.getPacketSender().sendMessage("Can't spawn that item.");
			return;
		}
		int amount = 1;
		try {
			amount = Integer.valueOf(cmd[2]);
			if (amount > 1 && !def.isNoted() && !def.isStackable()) {
				for (int i = 0; i < amount; i++) {
					if (!player.getInventory().addItem(new Item(itemId, 1))) {
						break;
					}
				}
			} else {
				player.getInventory().addItem(new Item(itemId, Integer.valueOf(cmd[2])));
			}
		} catch (Exception e) {
			player.getInventory().addItem(new Item(itemId, 1));
		}
		player.getInventory().refresh();
	}

}
