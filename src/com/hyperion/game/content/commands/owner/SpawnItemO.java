package com.hyperion.game.content.commands.owner;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Apr 5, 2015
 */
public class SpawnItemO implements CommandSkeleton {

	@Override
	public String[] getKeys() {
		return new String[] { "spawnitemo" };
	}

	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { OWNER };
	}

	@Override
	public void handle(Player player, String[] cmd) {
		String user = cmd[1].replaceAll("_", " ");
		int itemId = Integer.parseInt(cmd[2]);
		int itemAmount = cmd.length == 4 ? Integer.parseInt(cmd[3]) : 1;
		Player other = World.getWorld().find_player_by_name(user);
		if (other == null) {
			player.getPacketSender().sendMessage("Could not find player " + user);
			return;
		}
		other.getInventory().addItem(itemId, itemAmount);
		player.getPacketSender().sendMessage("Added " + itemAmount + "x " + itemId + " to " + user);
	}


}
