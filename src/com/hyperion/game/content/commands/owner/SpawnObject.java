package com.hyperion.game.content.commands.owner;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.game.world.gameobjects.GameObject;

public class SpawnObject implements CommandSkeleton {

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "obj" };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { OWNER };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion.game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		int id = Integer.parseInt(cmd[1]);
		int face = 0;
		int type = 10;
		if (cmd.length == 4) {
			face = Integer.parseInt(cmd[2]);
			type = Integer.parseInt(cmd[3]);
		}
		World.spawnObjectTemporary(new GameObject(player.getLocation(), id, type, face), 100);
	}

}
