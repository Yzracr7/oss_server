package com.hyperion.game.content.commands.owner;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Apr 5, 2015
 */
public class setModeratorStatus implements CommandSkeleton {

	@Override
	public String[] getKeys() {
		return new String[] { "setmod" };
	}

	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { OWNER };
	}

	@Override
	public void handle(Player player, String[] cmd) {
		String user = cmd[1].replaceAll("_", " ");
		Player other = World.getWorld().find_player_by_name(user);
		if (other == null) {
			player.getPacketSender().sendMessage("Could not find player " + user);
			return;
		}
		other.getDetails().setModerator(true);
		player.getPacketSender().sendMessage("Successfully given moderator status to: "+user+".");
	}


}
