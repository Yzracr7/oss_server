package com.hyperion.game.content.commands.player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import com.hyperion.game.Constants;
import com.hyperion.game.content.InterfaceTools;
import com.hyperion.game.content.commands.CommandHandler;
import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.utility.game.ChatColors;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 25, 2015
 */
public class Commands implements CommandSkeleton {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "commands", "cmds" };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { ALL };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion
	 * .game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		List<CommandSkeleton> commands = CommandHandler.getCommands();
		Collections.sort(commands, new ComparatorImplementation());
		List<String> textToSend = new ArrayList<>();
		textToSend.add("<col=" + ChatColors.MAROON + ">Below are the list of commands available to you.");
		for (Iterator<CommandSkeleton> it$ = commands.listIterator(); it$.hasNext();) {
			CommandSkeleton command = it$.next();
			Rights req = CommandHandler.accessibleRightRequired(player, command);
			if (req == null) {
				StringBuilder bldr = new StringBuilder();
				String[] keys = command.getKeys();
				for (int i = 0; i < keys.length; i++) {
					String key = keys[i];
					bldr.append("::" + (key + "" + (i == keys.length - 1 ? "" : ", ")));
				}
				textToSend.add(bldr.toString());
			}			
		}
		InterfaceTools.sendQuestScroll(player, Constants.SERVER_NAME + " Commands", textToSend.toArray(new String[textToSend.size()]));
	}

	/**
	 * @author Tyler
	 *
	 */
	private final class ComparatorImplementation implements Comparator<CommandSkeleton> {
		@Override
		public int compare(CommandSkeleton o1, CommandSkeleton o2) {
			Rights highestRight1 = null;
			int ordinal1 = -1;
			for (Rights right : o1.getRightsAllowed()) {
				if (ordinal1 == -1 || right.ordinal() < ordinal1) {
					ordinal1 = right.ordinal();
					highestRight1 = right;
				}
			}
			Rights highestRight2 = null;
			int ordinal2 = -1;
			for (Rights right : o2.getRightsAllowed()) {
				if (ordinal2 == -1 || right.ordinal() < ordinal1) {
					ordinal2 = right.ordinal();
					highestRight2 = right;
				}
			}
			return Integer.compare(highestRight1.ordinal(), highestRight2.ordinal());
		}
	}

}
