package com.hyperion.game.content.commands.player;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.data.magic.MagicEffects;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class Maxhit implements CommandSkeleton {

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "maxhit" };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { ALL };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion.game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		int weaponId = player.getEquipment().getItemInSlot(3);
		int ammo = player.getEquipment().getItemInSlot(13);
		player.getPacketSender().sendMessage("Melee maxhit: " + HitFormula.calculateMeleeMaxHit(player, null, weaponId, false, false) + ", spec: " + HitFormula.calculateMeleeMaxHit(player, null, weaponId, true, false));
		player.getPacketSender().sendMessage("Range maxhit: " + HitFormula.calculateRangeMaxHit(player, null, weaponId, ammo, false, false) + ", spec: " + HitFormula.calculateRangeMaxHit(player, null, weaponId, ammo, true, false));
		
		int spell = player.getAttributes().isSet("autocastspell") ? player.getAttributes().getInt("autocastspell") : 53;
		int max = MagicEffects.getMaxHit(player, spell, weaponId);
		
		player.getPacketSender().sendMessage("Magic maxhit(" + (spell == 53 ? "Ice Barrage" : "Autocast Spell " + spell) + "): " + max);
	}

}
