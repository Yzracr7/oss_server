package com.hyperion.game.content.commands.player;

import java.util.ArrayList;
import java.util.List;

import com.hyperion.game.content.InterfaceTools;
import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class Players implements CommandSkeleton {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getIdentifiers()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "players" };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.commands.CommandSkeleton#rightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { ALL };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion
	 * .game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		int count = World.getWorld().getPlayerCount();
		String addon = count == 1 ? "is" : "are";
		String addon2 = count == 1 ? "" : "s";
		List<String> players = new ArrayList<>();
		for (Player p : World.getWorld().getPlayers()) {
			if (p == null) {
				continue;
			}
			players.add(p.getName());
		}
		InterfaceTools.sendQuestScroll(player, "Players Online", players.toArray(new String[players.size()]));
		player.getPacketSender().sendMessage("There " + addon + " currently " + count + " player" + addon2 + " online.");
	}

}
