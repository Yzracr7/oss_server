package com.hyperion.game.content.commands.player;

import java.util.ArrayList;
import java.util.List;

import com.hyperion.game.content.InterfaceTools;
import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;

public class StaffOnline implements CommandSkeleton {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.commands.CommandSkeleton#getIdentifiers()
	 */
	@Override
	public String[] getKeys() {
		return new String[] { "staff", "staffonline" };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.content.commands.CommandSkeleton#rightsAllowed()
	 */
	@Override
	public Rights[] getRightsAllowed() {
		return new Rights[] { ALL };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion
	 * .game.world.entity.player.Player, java.lang.String[])
	 */
	@Override
	public void handle(Player player, String[] cmd) {
		List<String> staff = new ArrayList<>();
		for (Player p : World.getWorld().getPlayers()) {
			if (p == null)
				continue;
			if(p.getDetails().isModerator() || p.getDetails().isAdmin() || p.getDetails().isOwner())
				staff.add(p.getName());
		}
		int count = staff.size();
		String addon = count == 1 ? "is" : "are";
		String addon2 = count == 1 ? "" : "s";
		InterfaceTools.sendQuestScroll(player, "Staff Members Online", staff.toArray(new String[count]));
		player.getPacketSender().sendMessage("There " + addon + " currently " + count + " staff member" + addon2 + " online.");
	}

}
