package com.hyperion.game.content.commands.player;

import java.util.concurrent.TimeUnit;

import com.hyperion.game.content.commands.CommandSkeleton;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.utility.Censor;
import com.hyperion.utility.Utils;
import com.hyperion.utility.game.ChatColors;
import com.hyperion.utility.json.JsonManager;
import com.hyperion.utility.json.impl.PunishmentLoader;
import com.hyperion.utility.json.sub.Punishment.PunishmentType;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class Yell implements CommandSkeleton {

    public static final String rankTag[] = {"",
            "<img=0><shad=1>[<col=" + ChatColors.DARK_GREEN + ">Moderator</col>] ", //Mod
            "[<col=" + ChatColors.MEDIUM_BLUE + ">Administrator</col>] <img=1> ", //Admin
            "", //Ironman
            "<img=3>[<col=3E7A02>Regular</col>] ", //Reg don
            "<img=4>[<col=C90000>Extreme</col>] ", //Extreme donator
            "Support ", //Support
            "<img=15>[<col=0053BF>Super</col>] ",//Super donator
            "<img=16>[<col=66008F>Legendary</col>] " //Legendary donator
    };

    /*
     * (non-Javadoc)
     *
     * @see com.hyperion.game.content.commands.CommandSkeleton#getKeys()
     */
    @Override
    public String[] getKeys() {
        return new String[]{"yell"};
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.hyperion.game.content.commands.CommandSkeleton#getRightsAllowed()
     */
    @Override
    public Rights[] getRightsAllowed() {
        return new Rights[]{ALL};
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.hyperion.game.content.commands.CommandSkeleton#handle(com.hyperion
     * .game.world.entity.player.Player, java.lang.String[])
     */
    @Override
    public void handle(Player player, String[] cmd) {
        if (!player.getDetails().isDonator() && !player.getDetails().isStaff()) {
            player.getPacketSender().sendMessage("You must be a Donator to use the Yell Channel.");
            return;
        }
        if (JsonManager.getSingleton().getLoader(PunishmentLoader.class).isPunished(player.getDetails().getIP(), PunishmentType.IPMUTE) || JsonManager.getSingleton().getLoader(PunishmentLoader.class).isPunished(player.getName(), PunishmentType.MUTE)) {
            long duration = JsonManager.getSingleton().getLoader(PunishmentLoader.class).getPunishmentTime(player.getDetails().getIP(), PunishmentType.IPMUTE);
            if (duration == -1)
                duration = JsonManager.getSingleton().getLoader(PunishmentLoader.class).getPunishmentTime(player.getName(), PunishmentType.MUTE);
            player.getPacketSender().sendMessage("You are muted for " + (TimeUnit.MILLISECONDS.toHours(duration - System.currentTimeMillis())) + " more hours.");
            return;
        }
        String text = Utils.getCompleted(cmd, 1);
        String color = player.getVariables().isRankHidden() ? "<col=3E7A02>" : player.getDetails().isAdmin() ? "<col=" + ChatColors.DARK_RED + ">" : player.getDetails().isModerator() ? "<col=" + ChatColors.DARK_GREEN + ">" :
                player.getDetails().isLegendaryDonator() ? "<col=66008F>" :
                        player.getDetails().isExtremeDonator() ? "<col=C90000>" :
                                player.getDetails().isSuperDonator() ? "<col=0053BF>" :
                                        "<col=3E7A02>";
        for (Player p : World.getWorld().getPlayers()) {
            if (p != null) {
                if (player.getDetails().isCensorOn())
                    text = Censor.filter(text);
                p.getPacketSender().sendMessage(rankTag[player.getDetails().getRights()] + "" + player.getName() + ":" + color + " " + (text.substring(0, 1).toUpperCase() + text.substring(1)));
            }
        }
    }

}
