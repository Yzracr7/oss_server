package com.hyperion.game.content.dialogue;

import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

public abstract class Dialogue extends DialogueManager {

	/**
	 * Called when you finish talking/click option.
	 * 
	 * @param player
	 */
	public abstract void endDialogue(Player player, int lastStage);

	/**
	 * Handles options inter
	 * 
	 * @param player
	 * @param option
	 */
	public abstract void handleOptions(Player player, int option);

	/**
	 * Handles the dialogue for npc option 1.
	 * 
	 * @param player
	 *            The player.
	 * @param npc
	 *            The npc.
	 * @throws Exception
	 *             The exception.
	 */
	public abstract void handleDialogue1(Player player, NPC npc) throws Exception;

	/**
	 * Handles the dialogue for npc option 2.
	 * 
	 * @param player
	 *            The player.
	 * @param npc
	 *            The npc.
	 * @throws Exception
	 *             The exception.
	 */
	public abstract void handleDialogue2(Player player, NPC npc) throws Exception;

}