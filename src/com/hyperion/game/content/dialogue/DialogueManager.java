package com.hyperion.game.content.dialogue;

import com.hyperion.game.content.dialogue.impl.*;
import com.hyperion.game.content.dialogue.impl.home.*;
import com.hyperion.game.content.dialogue.impl.quests.*;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.NPCDefinition;
import com.hyperion.game.world.entity.player.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class DialogueManager {

    /**
     * The current stage of dialogue.
     */
    private int currentStage = 0;

    /**
     * The list of dialogues.
     */
    private static Map<Integer, Dialogue> dialogues = new HashMap<Integer, Dialogue>();

    /**
     * The list of dialogues.
     */
    private static Map<Integer[], MiscDialogue> misc = new HashMap<Integer[], MiscDialogue>();

    public static void load() {
        getDialogues().put(5523, new TitleMasterDialogue());
        getDialogues().put(548, new ThessaliaDialogue());
        getDialogues().put(403, new VannakaDialogue());
        getDialogues().put(7663, new VannakaDialogue());
        getDialogues().put(308, new EmblemTrader());
        getDialogues().put(3231, new TannerDialogue());
        getDialogues().put(3248, new TeleportWizard());
        getDialogues().put(4287, new BorderGaurd());
        getDialogues().put(4288, new BorderGaurd());
        getDialogues().put(2549, new GnomeGuerilla());
        getDialogues().put(3650, new CaptianBarnaby());
        getDialogues().put(4288, new BorderGaurd());

        getDialogues().put(394, new Banker());
        getDialogues().put(395, new Banker());
        getDialogues().put(5453, new Banker());
        getDialogues().put(5455, new Banker());
        getDialogues().put(5456, new Banker());

        getDialogues().put(526, new Trader());

        getMisc().put(new Integer[]{-1}, new ClanChatSetup());
        getMisc().put(new Integer[]{-3}, new ClanWarsLeave());
        getDialogues().put(4289, new KamfreenaDialogue());
        getDialogues().put(198, new CombatMasterDialogue());
        getDialogues().put(553, new AuburyDialogue());
        getDialogues().put(1783, new RichardDialogue());
        getDialogues().put(905, new KolodionDialogue());
        getDialogues().put(318, new GypsyDialogue());
        getDialogues().put(1923, new EblisDialogue());
        getDialogues().put(1807, new HamalChiefDialogue());
        getDialogues().put(1334, new JossikDialogue());
        getDialogues().put(597, new NoterazzoDialogue());
        // getDialogues().put(494, new BankDialogue());
        // getDialogues().put(902, new BankDialogue());
        getDialogues().put(670, new MonkeyMadnessKing());
        //getDialogues().put(4511, new LunarDialogue());
        getDialogues().put(6527, new RewardsTrader());
        getDialogues().put(519, new BobDialogue());
        getDialogues().put(2479, new EvilBob());
        getDialogues().put(709, new MeleeTutorD());
        getDialogues().put(1835, new EasterBunnyD());
        //getDialogues().put(4657, new ZombiesLeaveDialogue());
        getDialogues().put(8000, new RingEnchanter());
        //Logger.getInstance().warning("Loaded " + dialogues.size() + " Dialogues.");
    }

    public static boolean handleMisc(Player player, int item, boolean operate) {
        for (Entry<Integer[], MiscDialogue> jewls : misc.entrySet()) {
            for (int i = 0; i < jewls.getKey().length; i++) {
                if (item == jewls.getKey()[i]) {
                    MiscDialogue jewl = misc.get(jewls.getKey());
                    player.getAttributes().set("jewlItem", item);
                    jewl.handleOperateOrRub(player, operate);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Handles the dialogues.
     *
     * @param npc The npc.
     */
    public static void handleDialogues(Player player, NPC npc, int option) {
        if (npc == null || npc.getDefinition() == null) {
            return;
        }
        try {
            Dialogue dialogue = getDialogues().get(npc.getDefinition().getId());
            if (dialogue == null) {
                //dialogue = new Default();
                //player.getAttributes().set("npcOption", option);
                //dialogue.handleDialogue1(player, npc);
                return;
            } else {
                player.getAttributes().set("npcOption", option);
                if (player.getAttributes().isSet("nextDialogue")) {
                    if (player.getAttributes().getInt("nextDialogue") == -1) {
                        dialogue.endDialogue(player, player.getAttributes().getInt("previousDialogue"));
                        return;
                    }
                }
                player.getAttributes().set("dialogueNPC", npc);
                if (option == 1) {
                    dialogue.handleDialogue1(player, npc);
                } else if (option == 2) {
                    dialogue.handleDialogue2(player, npc);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Handles Dialogue Option
     *
     * @param player
     * @param npc
     * @param child
     */
    public static void handleOptions(Player player, NPC npc, int child) {
        if (npc == null || npc.getDefinition() == null) {
            return;
        }
        try {
            Dialogue dialogue = getDialogues().get(npc.getDefinition().getId());
            if (dialogue == null) {
                return;
            } else {
                dialogue.handleOptions(player, child);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Handles the option chatbox for jewllery.
     *
     * @param player
     * @param child
     */
    public static void handleOptions(Player player, int item, int child) {
        for (Entry<Integer[], MiscDialogue> jewls : misc.entrySet()) {
            for (int i = 0; i < jewls.getKey().length; i++) {
                if (item == jewls.getKey()[i]) {
                    MiscDialogue jewl = misc.get(jewls.getKey());
                    jewl.handleOptions(player, child);
                    break;
                }
            }
        }
    }

    /**
     * Sends a dialogue.
     *
     * @param player     The player.
     * @param expression The expression.
     * @param text       The text.
     */
    public void sendNPCOldDialogue(Player player, NPC npc, FacialAnimation expression, String... text) {
        if (text.length > 4 || text.length < 1) {
            return;
        }
        int interfaceId = 240 + text.length;
        if (interfaceId <= 240) {
            interfaceId = 241;
        }
        interfaceId = 241;
        // for now
        expression = FacialAnimation.HAPPY;

        player.getPacketSender().sendNPCHead(npc.getDefinition().getId(), interfaceId, 0);
        player.getPacketSender().modifyText(npc.getDefinition().getName(), interfaceId, 1);
        for (int i = 0; i < text.length; i++) {
            player.getPacketSender().modifyText(text[i], interfaceId, 2 + i);
        }
        int facialAnim = -1;
        facialAnim = expression.getAnimation().getId();
        player.getPacketSender().animateInterface(facialAnim, interfaceId, 0);
        player.getPacketSender().sendChatboxInterface(interfaceId);
    }

    /**
     * Sends a player dialogue.
     *
     * @param player     The player.
     * @param expression The expression.
     * @param text       The text.
     */
    public void sendNPCDialogue(Player player, NPC npc, FacialAnimation expression, String... text) {
        if (text.length > 4 || text.length < 1) {
            return;
        }
        int interfaceId = 240 + text.length;
        if (interfaceId <= 240) {
            interfaceId = 241;
        }

        String name = NPCDefinition.forId(npc.getId()).getName();
        player.getPacketSender().sendNPCHead(npc.getId(), interfaceId, 0);
        player.getPacketSender().modifyText(name, interfaceId, 1);
        for (int i = 0; i < text.length; i++) {
            player.getPacketSender().modifyText(text[i], interfaceId, 2 + i);
        }

        player.getPacketSender().animateInterface(expression.getAnimation().getId(), interfaceId, 0);
        player.getPacketSender().sendChatboxInterface(interfaceId);
    }

    /**
     * Sends a player dialogue.
     *
     * @param player     The player.
     * @param expression The expression.
     * @param text       The text.
     */
    public void sendPlayerDialogue(Player player, FacialAnimation expression, String... text) {
        if (text.length > 4 || text.length < 1) {
            return;
        }
        int interfaceId = 63 + text.length;
        if (interfaceId <= 63) {
            interfaceId = 64;
        }
        for (int i = 0; i < text.length; i++) {
            player.getPacketSender().modifyText(text[i], interfaceId, 2 + i);
        }
        int facialAnim = -1;
        if (player.getDetails().getClientMode() <= 474) {
            facialAnim = expression.getAnimation().getId();
        } else {
            facialAnim = NewFacialAnimation.valueOf(expression.name()).getAnimation().getId();
        }
        player.getPacketSender().animateInterface(facialAnim, interfaceId, 0);
        player.getPacketSender().sendPlayerHead(interfaceId, 0);
        player.getPacketSender().modifyText(player.getDetails().getName(), interfaceId, 1);
        player.getPacketSender().sendChatboxInterface(interfaceId);
    }

    /**
     * Sends an option dialogue.
     *
     * @param player The player.
     * @param title  The title.
     * @param text   The text.
     */
    public void sendOptionDialogue(Player player, String title, String... text) {
        if (text.length > 5 || text.length < 2) {
            return;
        }
        int interfaceId = 224 + (text.length * 2);
        player.getPacketSender().modifyText(title, interfaceId, 0);
        for (int i = 0; i < text.length; i++) {
            player.getPacketSender().modifyText(text[i], interfaceId, i + 1);
        }
        player.getPacketSender().sendChatboxInterface(interfaceId);
    }

    /**
     * Gets the current stage.
     *
     * @return the currentStage
     */
    public int getCurrentStage() {
        return currentStage;
    }

    /**
     * Sets the current stage.
     *
     * @param currentStage the currentStage to set
     */
    public void setNextStage(int currentStage) {
        this.currentStage = currentStage;
    }

    /**
     * Resets the dialogue stage.
     */
    public void resetDialogueStage() {
        setNextStage(0);
    }

    /**
     * Gets the dialogues.
     *
     * @return the dialogues
     */
    private static Map<Integer, Dialogue> getDialogues() {
        return dialogues;
    }

    /**
     * Sets the dialogues.
     *
     * @param dialogues the dialogues to set
     */
    public static void setDialogues(Map<Integer, Dialogue> dialogues) {
        DialogueManager.dialogues = dialogues;
    }

    private static Map<Integer[], MiscDialogue> getMisc() {
        return misc;
    }

    public static void setJewllery(Map<Integer[], MiscDialogue> jewllery) {
        DialogueManager.misc = jewllery;
    }

    public enum NewFacialAnimation {

        HAPPY(Animation.create(9850)), // - Joyful/happy
        CALM_1(Animation.create(9760)), // - Speakingly calmly
        CALM_2(Animation.create(9760)), // - Calm talk
        DEFAULT(Animation.create(9810)), // - Default speech
        EVIL(Animation.create(9870)), // - Evil
        EVIL_CONTINUED(Animation.create(9870)), // - Evil continued
        DELIGHTED_EVIL(Animation.create(9870)), // - Delighted evil
        ANNOYED(Animation.create(9820)), // - Annoyed
        DISTRESSED(Animation.create(9827)), // - Distressed
        DISTRESSED_CONTINUED(Animation.create(9827)), // - Distressed continued
        ALMOST_CRYING(Animation.create(9780)), // - Almost crying
        BOWS_HEAD_WHILE_SAD(Animation.create(9827)), // - Bows head while sad
        DRUNK_TO_LEFT(Animation.create(9835)), // - Talks and looks sleepy/drunk
        // to left
        DRUNK_TO_RIGHT(Animation.create(9835)), // - Talks and looks
        // sleepy/drunk
        // to right
        DISINTERESTED(Animation.create(9860)), // - Sleepy or disinterested
        SLEEPY(Animation.create(9815)), // - Tipping head as if sleepy.
        PLAIN_EVIL(Animation.create(9870)), // - Plain evil
        // (Animation.create(Grits teeth and
        // moves eyebrows)
        LAUGH_1(Animation.create(9840)), // - Laughing or yawning
        LAUGH_2(Animation.create(9840)), // - Laughing or yawning for longer
        LAUGH_3(Animation.create(9840)), // - Laughing or yawning for longer
        LAUGH_4(Animation.create(9840)), // - Laughing or yawning
        EVIL_LAUGH(Animation.create(9870)), // - Evil laugh then plain evil
        SAD(Animation.create(9775)), // - Slightly sad
        MORE_SAD(Animation.create(9775)), // - Quite sad
        ON_ONE_HAND(Animation.create(9845)), // - On one hand...
        NEARLYC_RYING(Animation.create(9765)), // - Close to crying
        ANGER_1(Animation.create(9790)), // - Angry
        ANGER_2(Animation.create(9785)), // - Angry
        ANGER_3(Animation.create(9795)), // - Angry
        ANGER_4(Animation.create(9800)) // - Angry
        ;
        /**
         * A map of facial animations.
         */
        private static Map<Animation, NewFacialAnimation> newFacialAnimations = new HashMap<Animation, NewFacialAnimation>();

        /**
         * Gets a facial animation by its ID.
         *
         * @param newFacialAnimation The facial animation item id.
         * @return The facial animation, or <code>null</code> if the id is not a
         * facial animation.
         */
        public static NewFacialAnimation forId(Animation newFacialAnimation) {
            return newFacialAnimations.get(newFacialAnimation);
        }

        /**
         * Populates the facial animation map.
         */
        static {
            for (NewFacialAnimation newFacialAnimation : NewFacialAnimation.values()) {
                newFacialAnimations.put(newFacialAnimation.animation, newFacialAnimation);
            }
        }

        private Animation animation;

        private NewFacialAnimation(Animation animation) {
            this.animation = animation;
        }

        /**
         * @return the animation
         */
        public Animation getAnimation() {
            return animation;
        }

    }

    public enum FacialAnimation {
        /**
         * Dialogue animations.
         */
        HAPPY(Animation.create(588)), // - Joyful/happy
        CALM_1(Animation.create(589)), // - Speakingly calmly
        CALM_2(Animation.create(590)), // - Calm talk
        DEFAULT(Animation.create(591)), // - Default speech
        EVIL(Animation.create(592)), // - Evil
        EVIL_CONTINUED(Animation.create(593)), // - Evil continued
        DELIGHTED_EVIL(Animation.create(594)), // - Delighted evil
        ANNOYED(Animation.create(595)), // - Annoyed
        DISTRESSED(Animation.create(596)), // - Distressed
        DISTRESSED_CONTINUED(Animation.create(597)), // - Distressed continued
        ALMOST_CRYING(Animation.create(598)), // - Almost crying
        BOWS_HEAD_WHILE_SAD(Animation.create(599)), // - Bows head while sad
        DRUNK_TO_LEFT(Animation.create(600)), // - Talks and looks sleepy/drunk
        // to left
        DRUNK_TO_RIGHT(Animation.create(601)), // - Talks and looks sleepy/drunk
        // to right
        DISINTERESTED(Animation.create(602)), // - Sleepy or disinterested
        SLEEPY(Animation.create(603)), // - Tipping head as if sleepy.
        PLAIN_EVIL(Animation.create(604)), // - Plain evil
        // (Animation.create(Grits teeth and
        // moves eyebrows)
        LAUGH_1(Animation.create(605)), // - Laughing or yawning
        LAUGH_2(Animation.create(606)), // - Laughing or yawning for longer
        LAUGH_3(Animation.create(607)), // - Laughing or yawning for longer
        LAUGH_4(Animation.create(608)), // - Laughing or yawning
        EVIL_LAUGH(Animation.create(609)), // - Evil laugh then plain evil
        SAD(Animation.create(610)), // - Slightly sad
        MORE_SAD(Animation.create(611)), // - Quite sad
        ON_ONE_HAND(Animation.create(612)), // - On one hand...
        NEARLYC_RYING(Animation.create(613)), // - Close to crying
        ANGER_1(Animation.create(614)), // - Angry
        ANGER_2(Animation.create(615)), // - Angry
        ANGER_3(Animation.create(616)), // - Angry
        ANGER_4(Animation.create(617)) // - Angry
        ;

        /**
         * A map of facial animations.
         */
        private static Map<Animation, FacialAnimation> facialAnimations = new HashMap<Animation, FacialAnimation>();

        /**
         * Gets a facial animation by its ID.
         *
         * @param facialAnimation The facial animation item id.
         * @return The facial animation, or <code>null</code> if the id is not a
         * facial animation.
         */
        public static FacialAnimation forId(Animation facialAnimation) {
            return facialAnimations.get(facialAnimation);
        }

        /**
         * Populates the facial animation map.
         */
        static {
            for (FacialAnimation facialAnimation : FacialAnimation.values()) {
                facialAnimations.put(facialAnimation.animation, facialAnimation);
            }
        }

        private Animation animation;

        private FacialAnimation(Animation animation) {
            this.animation = animation;
        }

        /**
         * @return the animation
         */
        public Animation getAnimation() {
            return animation;
        }

    }
}
