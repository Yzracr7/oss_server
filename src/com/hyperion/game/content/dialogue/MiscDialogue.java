package com.hyperion.game.content.dialogue;

import com.hyperion.game.content.skills.magic.TeleportRequirements;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.player.Player;

public abstract class MiscDialogue extends DialogueManager {

	private static int getNewItem(int item) {
		if (item >= 1704 && item <= 1712) {
			if (item != 1704) {//no charges
				return item - 2;
			}
		}
		if (item >= 3853 && item <= 3867) {
			if (item != 3867) {
				return item + 2;
			}
		}
		return -1;
	}
	
	/**
	 * The teleport locations.
	 * @return
	 */
	public abstract Location[][] locations();
	
	/**
	 * Handles options inter
	 * @param player
	 * @param option
	 */
	public abstract void handleOptions(Player player, int option);
	
	/**
	 * Called when rubbed/operated.
	 * @param player
	 * @param option
	 */
	public abstract void handleOperateOrRub(Player player, boolean operate);
	
	/**
	 * Level this jewl stops working in wilderness.
	 */
	public abstract int wildernessLevel();
	
	/**
	 * Can we tele?
	 * @param player
	 * @return
	 */
	public boolean canTeleport(Player player) {
		if (player.getCombatState().isDead()) {
			return false;
		}
		if (player.getAttributes().isSet("stopActions")) {
			return false;
		}
		if (TeleportRequirements.wildernessChecks(player, wildernessLevel())) {
			return false;
		}
		if (TeleportRequirements.isInMinigames(player)) {
			return false;
		}
		return true;
	}
	/**
	 * Handles jewllery replacing.
	 * @param player
	 * @param item
	 * @param operate
	 */
	public boolean handleJewlReplacing(Player player, int item, boolean operate) {
		int newItem = getNewItem(item);
		boolean operating = player.getAttributes().isSet("operating") && player.getAttributes().is("operating");
		if (item != 3867) {//Games neck(1) turns into dust..
			if (newItem == -1) {
				player.getPacketSender().sendMessage("This amulet has no charges remaining.");
				return false;
			}
		} else if (item == 3867) {
			if (operating) {
				player.getEquipment().replaceItem(item, -1);
			} else {
				player.getInventory().replaceItem(item, -1);
			}
			player.getPacketSender().sendMessage("Your Games Necklace turns into dust..");
			return true;
		}
		if (operating) {
			player.getEquipment().replaceItem(item, newItem);
		} else {
			player.getInventory().replaceItem(item, newItem);
		}
		return true;
	}
	
	/**
	 * pretty obvious what this is..
	 * @param option
	 * @return
	 */
	public int getLocationIndexForOption(int option) {
		switch (option) {
		case 1:
			return 0;
		case 2:
			return 1;
		case 3:
			return 2;
		case 4:
			return 3;
		}
		return -1;
	}
}
