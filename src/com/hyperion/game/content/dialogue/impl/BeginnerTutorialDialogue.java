package com.hyperion.game.content.dialogue.impl;

import com.hyperion.game.Constants;
import com.hyperion.game.content.BeginnerTutorial;
import com.hyperion.game.content.dialogue.MiscDialogue;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.player.Player;

public class BeginnerTutorialDialogue extends MiscDialogue {

    @Override
    public void handleOptions(final Player player, int option) {
        if (option == 1) {
            player.getInterfaceSettings().closeInterfaces(false);
            player.getPacketSender().displayInterface(649);//615
        } else if (option == 2) {
            player.getInterfaceSettings().closeInterfaces(false);
            BeginnerTutorial.start(player);
        } else {
            player.getInterfaceSettings().closeInterfaces(false);
        }
    }

    @Override
    public void handleOperateOrRub(Player player, boolean operate) {
        switch (getCurrentStage()) {
            case 0:
                this.sendOptionDialogue(player, "Skip " + Constants.SERVER_NAME + " Tutorial?", "Yes, I already know what I'm doing.", "No, take me through it.");
                break;
        }
    }

    @Override
    public Location[][] locations() {
        return null;
    }

    @Override
    public int wildernessLevel() {
        return 100;
    }

}
