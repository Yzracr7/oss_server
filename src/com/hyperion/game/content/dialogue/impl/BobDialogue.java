package com.hyperion.game.content.dialogue.impl;

import com.hyperion.game.Constants;
import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

public class BobDialogue extends Dialogue {
    
	

	@Override
	public void endDialogue(final Player player, int lastStage) {
		// TODO Auto-generated method stub
		player.getInterfaceSettings().closeInterfaces(true);
	}

	@Override
	public void handleOptions(final Player player, int option) {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("previousDialogue")) {
			if (player.getAttributes().getInt("previousDialogue") == 0) {
				if (option == 1) {
					player.getAttributes().set("nextDialogue", -1);
					endDialogue(player, -1);
					player.getPacketSender().sendChatboxDialogue(false,
							"Fixing barrows items...");
					// ItemStoreConnector.claimPayment(player,
					// player.getName());
					// ForumIntegration.claimDonations(player);
					fixBarrows(player);
				} else if (option == 2) {
					endDialogue(player, -1);
					// player.getPacketSender().sendMessage("Coming soon!");
				}
			}
		}
		DialogueManager
				.handleDialogues(player,
						(NPC) player.getAttributes().get("dialogueNPC"), player
								.getAttributes().getInt("npcOption"));
	}


	private boolean fixBarrows(final Player player) {
		boolean repaired = false;
		for (int g = 0; g <= 4; g++) {
			int cost = 100000 - (g * 25000) + (g == 4 ? 15000 : 0);
			for (int i = 0; i < Constants.BROKEN_BARROWS.length; i++) {
				if (player.getInventory().hasItem(Constants.BROKEN_BARROWS[i][1] - g)) {
					if (player.getInventory().getItemAmount(995) >= cost) {
						int amt = player.getInventory().getCount(Constants.BROKEN_BARROWS[i][1] - g);
						for (int r = 0; r < amt; r++) {
							Item item = player.getInventory().getSlot(player.getInventory().getSlotById(Constants.BROKEN_BARROWS[i][1] - g));
							if (item != null)
								if (player.getInventory().deleteItem(995, cost)) {
										player.getPacketSender().sendMessage("Your " + item.getDefinition().getName() + " has been repaired for "
																+ cost + ".");
										item.setItemId(Constants.BROKEN_BARROWS[i][0]);
										repaired = true;
										
							}
						}
					} else {
						player.getPacketSender().sendChatboxDialogue(true, "You have run out of money.");
						return false;
					}
				}
			}
		}
		player.getInventory().refresh();
		if (repaired) {
			player.getPacketSender().sendChatboxDialogue(true,
					"Your barrows items have been repaired.");
		} else {
			player.getPacketSender().sendChatboxDialogue(true,
					"You don't have any items to repair.");
		}
		return repaired;
	}

	@Override
	public void handleDialogue1(final Player player, NPC npc) throws Exception {
		this.sendOptionDialogue(player, "Select an Option",
				"Fix Barrows Equipment", "Nevermind");
		player.getAttributes().set("previousDialogue", getCurrentStage());
	}

	@Override
	public void handleDialogue2(final Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub

	}
}
