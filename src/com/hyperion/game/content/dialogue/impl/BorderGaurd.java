package com.hyperion.game.content.dialogue.impl;

import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.content.skills.agility.AgilityMovement;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

public class BorderGaurd extends Dialogue {

    // 3268 3228
    // 3268 3227

    @Override
    public void endDialogue(Player player, int lastStage) {
        player.getInterfaceSettings().closeInterfaces(true);
    }

    @Override
    public void handleOptions(Player player, int option) {
        if (player.getAttributes().isSet("previousDialogue")) {
            if (player.getAttributes().getInt("previousDialogue") == 2) {
                if (option == 1) {
                    player.getAttributes().set("nextDialogue", 3);
                } else if (option == 2) {
                    player.getAttributes().set("nextDialogue", -1);
                }
            }
        }
        DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
    }

    @Override
    public void handleDialogue1(Player player, NPC npc) throws Exception {
        if (player.getAttributes().isSet("nextDialogue")) {
            setNextStage(player.getAttributes().getInt("nextDialogue"));
        }
        switch (getCurrentStage()) {
            case 0:
                this.sendPlayerDialogue(player, FacialAnimation.DEFAULT, "Can i come through this gate?");
                player.getAttributes().set("nextDialogue", 1);
                break;
            case 1:
                this.sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "You must pay a toll of 10 coins to pass.");
                player.getAttributes().set("nextDialogue", 2);
                break;
            case 2:
                player.getAttributes().set("boarder_guard_dialogue", -1);
                this.sendOptionDialogue(player, "Select an Option", "No thank you, I'll walk around.", "Who does my money go to?", "Yes, ok.");
                player.getAttributes().set("nextDialogue", 3);
                player.getAttributes().set("dialogueNPC", npc);
                break;
            case 3:
                final int dialogue_option = player.getAttributes().getInt("boarder_guard_dialogue");
                switch (dialogue_option) {
                    case 3:
                        if (player.getInventory().hasItemAmount(995, 10)) {
                            boolean east_side = player.getLocation().getX() < 3267;
                            Location destination = null;
                            if (player.getLocation() == new Location(3267, 3228, 0) || player.getLocation() == new Location(3267, 3227, 0)) {
                                AgilityMovement.execute_force_walking(player, 819, 3266, player.getLocation().getY(), 1, 2, true, -1, null);
                                AgilityMovement.execute_force_walking(player, 819, 3268, player.getLocation().getY(), 1, 2, true, -1, null);
                            } else {
                                if (east_side) {
                                    AgilityMovement.execute_force_walking(player, 819, 3267, player.getLocation().getY() == 3227 ? 3227 : 3228, 1, 2, true, -1, null);
                                    AgilityMovement.execute_force_walking(player, 819, 3268, player.getLocation().getY() == 3227 ? 3227 : 3228, 1, 2, true, -1, null);
                                } else {
                                    AgilityMovement.execute_force_walking(player, 819, 3268, player.getLocation().getY() == 3227 ? 3227 : 3228, 1, 2, true, -1, null);
                                    AgilityMovement.execute_force_walking(player, 819, 3267, player.getLocation().getY() == 3227 ? 3227 : 3228, 1, 2, true, -1, null);
                                }
                            }
                        } else {
                            this.sendPlayerDialogue(player, FacialAnimation.SAD, "Oh dear i don't seem to have any money.");
                        }
                        break;
                    case 2:
                        this.sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "The money goes to the city of Al-Kharid.");
                        break;
                    default:
                        System.out.println("dialogue option : " + dialogue_option);
                        break;
                }
                player.getAttributes().remove("boarder_guard_dialogue");
                player.getAttributes().remove("nextDialogue");
                player.getAttributes().remove("dialogueNPC");

                this.endDialogue(player, -1);
                break;
        }
        player.getAttributes().set("previousDialogue", getCurrentStage());
    }

    @Override
    public void handleDialogue2(Player player, NPC npc) throws Exception {
        // TODO Auto-generated method stub
    }
}
