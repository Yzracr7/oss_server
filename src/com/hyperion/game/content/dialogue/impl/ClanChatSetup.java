package com.hyperion.game.content.dialogue.impl;

import com.hyperion.game.content.dialogue.MiscDialogue;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.player.Player;

public class ClanChatSetup extends MiscDialogue {
	
	@Override
	public void handleOptions(final Player player, int option) {
		if(option == 1) {
			player.getAttributes().set("enterNewName", true);
			player.getInterfaceSettings().openEnterTextInterface(612, 0, "Change Clan Name:");
			player.getInterfaceSettings().closeInterfaces(false);
		} else if (option == 2) {
			player.getAttributes().set("kickPlayer", true);
			player.getInterfaceSettings().openEnterTextInterface(612, 0, "Enter name of player to kick:");
			player.getInterfaceSettings().closeInterfaces(false);
		}
	}

	@Override
	public void handleOperateOrRub(Player player, boolean operate) {
		switch (getCurrentStage()) {
		case 0:
			this.sendOptionDialogue(player, "Clan Chat Setup", "Change clan name", "Kick player");
			break;
		}
	}

	@Override
	public Location[][] locations() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int wildernessLevel() {
		// TODO Auto-generated method stub
		return 100;
	}

}
