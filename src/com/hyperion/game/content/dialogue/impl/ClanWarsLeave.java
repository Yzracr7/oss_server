package com.hyperion.game.content.dialogue.impl;

import com.hyperion.game.Constants;
import com.hyperion.game.content.dialogue.MiscDialogue;
import com.hyperion.game.content.minigames.clanwars.session.WarSession;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.player.Player;

public class ClanWarsLeave extends MiscDialogue {
	
	@Override
	public void handleOptions(final Player player, int option) {
		if(!player.getAttributes().isSet("clansession")) {
			player.teleport(Location.create(6471, 3675, 0));
		}
		WarSession war = player.getAttributes().get("clansession");
		if(war == null) {
			player.getAttributes().remove("clansession");
			player.teleport(Location.create(6471, 3675, 0));
			player.getInterfaceSettings().closeInterfaces(false);
			return;
		}
		if(option == 1) {
			war.leave_portal(player, false, true);
			player.getInterfaceSettings().closeInterfaces(false);
			return;
		} else if (option == 2) {
			if(!this.canRejoin) {
				player.getInterfaceSettings().closeInterfaces(false);
				return;
			}
			if(war.isSpectator(player)) {
				if(war.canRejoin()) {
					//TODO: Dialogue ?
					int team = war.removeSpectatorStatus(player);
					if(team == -1) {
						player.getPacketSender().sendMessage("A fatal error has occured. Please contact a staff member.");
						System.out.println("[FATAL] Spectator has no team!");
						return;
					}if(team == 1)
						war.getTeam1().add(player);
					else 
						war.getTeam2().add(player);
					player.teleport(war.getStartLocation(player, team));
					player.getInterfaceSettings().closeInterfaces(false);
					return;
				} else {
					//should never be called?
					if(Constants.DEBUG_MODE)
						player.getPacketSender().sendMessage("war is set to kill limit, leave");
					war.leave_portal(player, false, true);
					player.getPacketSender().sendMessage("This war has no kill limit set, which mean you can only spectate after death.");
					player.getInterfaceSettings().closeInterfaces(false);
					return;
				}
			}
		} else {
			player.getInterfaceSettings().closeInterfaces(false);
		}
	}

	boolean canRejoin = false;
	
	@Override
	public void handleOperateOrRub(Player player, boolean canRejoin) {
		this.canRejoin = canRejoin;
		switch (getCurrentStage()) {
		case 0:
			if(canRejoin)
				this.sendOptionDialogue(player, "Select an Option", "Leave Arena", "Rejoin" , "Cancel");
			else
				this.sendOptionDialogue(player, "Select an Option", "Leave Arena", "Cancel");
			break;
		}
	}

	@Override
	public Location[][] locations() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int wildernessLevel() {
		// TODO Auto-generated method stub
		return 100;
	}

}
