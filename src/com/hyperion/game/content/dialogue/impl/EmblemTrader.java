package com.hyperion.game.content.dialogue.impl;

import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.content.wilderness.BountyHunterEmblem;
import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.shopping.ShopManager;

public class EmblemTrader extends Dialogue {

	@Override
	public void endDialogue(Player player, int lastStage) {
		// TODO Auto-generated method stub
		player.getInterfaceSettings().closeInterfaces(true);
	}

	@Override
	public void handleOptions(final Player player, int option) {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("previousDialogue")) {
			if (player.getAttributes().getInt("previousDialogue") == 0) {
				if (option == 1) {
					player.getAttributes().set("nextDialogue", -1);
					endDialogue(player, -1);
					player.getPacketSender().sendChatboxDialogue(false,
							"Searching inventory for Mysterious emblems..");
					
					if(exchangeBounties(player))
					    player.getPacketSender().sendChatboxDialogue(true,
							"Your Mysterious emblems have been exchanged.");
					else 
					    player.getPacketSender().sendChatboxDialogue(true,
							"You do not have any Mysterious emblems in your inventory.");
				} else if (option == 2) {
				    	endDialogue(player, -1);
				    	ShopManager.openShop(player, 37);
				} else if (option == 3) {
					endDialogue(player, -1);
				}
			}
		}
		DialogueManager
				.handleDialogues(player,
						(NPC) player.getAttributes().get("dialogueNPC"), player
								.getAttributes().getInt("npcOption"));
	}


	private boolean exchangeBounties (final Player player) {
	    boolean found = false;
		for(Item item : player.getInventory().getItems()) {
		    if(item == null)
			continue;
		    BountyHunterEmblem emblem = BountyHunterEmblem.getEmblemForId(item.getId());
		    if(emblem != null && player.getInventory().deleteItem(item)) {
			player.getVariables().increaseBounties(emblem.getBounties());
			if(player.getVariables().getBounties() >= 5_000_000)
				Achievements.increase(player, 49);
			player.getPacketSender().sendMessage("Your " + item.getDefinition().getName() + " has been exchanged for " + emblem.getBounties() + " bounties.");
			found = true;
		    }
		}
		if(!found)
		    return false;
		player.getInventory().refresh();
		return true;
	}

	@Override
	public void handleDialogue1(Player player, NPC npc) throws Exception {
		this.sendOptionDialogue(player, "Select an Option",
				"Exchange Emblems", "View Bounty Hunter Shop", "Nevermind");
		player.getAttributes().set("previousDialogue", getCurrentStage());
	}

	@Override
	public void handleDialogue2(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub

	}
}
