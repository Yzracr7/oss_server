package com.hyperion.game.content.dialogue.impl;

import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

public class EvilBob extends Dialogue {

	@Override
	public void endDialogue(Player player, int lastStage) {
		// TODO Auto-generated method stub
		player.getInterfaceSettings().closeInterfaces(true);
	}

	@Override
	public void handleOptions(Player player, int option) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleDialogue1(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("nextDialogue")) {
			setNextStage(player.getAttributes().getInt("nextDialogue"));
		} else {//just started dialogue so we set next dialogue to 1.
			player.getAttributes().set("nextDialogue", 0);
		}
		switch (getCurrentStage()) {
		case 0:
			if (player.getInventory().hasItem(6200)) {
				player.getInventory().deleteItem(6200);
				sendNPCDialogue(player, npc, FacialAnimation.DELIGHTED_EVIL, "Mmm.. mm.. that's delicious... Now let me take..", "a little cat nap...");
				player.getAttributes().set("nextDialogue", -1);
				npc.playForcedChat("ZZZzzzz");
				player.getPacketSender().sendMessage("Evil bob seems slightly less attentive of you.");
				player.getAttributes().set("canLeaveRandom", true);
			} else if(player.getInventory().hasItem(6202)) {
				sendNPCDialogue(player, npc, FacialAnimation.ANGER_1, "What are you giving me, cooked fish? What am I going", "to do with that? Uncook it first!");
				player.getAttributes().set("nextDialogue", 8);
			} else {
				sendPlayerDialogue(player, FacialAnimation.ANGER_1, "Let me out of here!");
				player.getAttributes().set("nextDialogue", 1);
			}
			break;
		case 1:
			sendNPCDialogue(player, npc, FacialAnimation.DELIGHTED_EVIL, "I will never let you go, " + player.getName() + "!");
			player.getAttributes().set("nextDialogue", 2);
			break;
		case 2:
			sendPlayerDialogue(player, FacialAnimation.DEFAULT, "Why not?");
			player.getAttributes().set("nextDialogue", 3);
			break;
		case 3:
			sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "Because I say so! And because I can never have", "enough servants!");
			player.getAttributes().set("nextDialogue", 4);
			break;
		case 4:
			sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "Now catch me some fish, I'm hungry.");
			player.getAttributes().set("nextDialogue", 5);
			break;
		case 5:
			sendPlayerDialogue(player, FacialAnimation.ANNOYED, "What fish, where?");
			player.getAttributes().set("nextDialogue", 6);
			break;
		case 6:
			sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "Hmm..");
			player.getAttributes().set("nextDialogue", 7);
			break;
		case 7:
			sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "I'm in the mood for some fish from the northern", "part of this island. Now begone, servant!");
			player.getAttributes().set("nextDialogue", -1);
			break;
		case 8:
			sendPlayerDialogue(player, FacialAnimation.DEFAULT, "Err... uncook it?");
			player.getAttributes().set("nextDialogue", 9);
			break;
		case 9:
			sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "You heard me! There's the cold fire, by the trees, now", "get uncooking!");
			player.getAttributes().set("nextDialogue", -1);
			break;
		}
		player.getAttributes().set("previousDialogue", getCurrentStage());
	}

	@Override
	public void handleDialogue2(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	private void tanHides(Player player) {
		for (int i = 0; i < 28; i++) {
			if (player.getInventory().getItemInSlot(i) == 1739) {
				player.getInventory().replaceItem(1739, 1741);
			}
		}
	}

}
