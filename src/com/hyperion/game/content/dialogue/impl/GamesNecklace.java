package com.hyperion.game.content.dialogue.impl;

import com.hyperion.game.content.dialogue.MiscDialogue;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.player.Player;

public class GamesNecklace extends MiscDialogue {

	@Override
	public Location[][] locations() {
		// TODO Auto-generated method stub
		return new Location[][] {
				{Location.create(3377, 3275, 0)},//Pest control - TODO
				{Location.create(3377, 3275, 0)}//Duel Arena
		};
	}

	@Override
	public void handleOptions(final Player player, int option) {
		// TODO Auto-generated method stub
		final int jewlItem = player.getAttributes().getInt("jewlItem");
		player.getInterfaceSettings().closeInterfaces(true);
		if (!this.canTeleport(player)) {
			return;
		}
		if (option == -1) {//operating.
			if (!this.handleJewlReplacing(player, jewlItem, true)) {
				return;
			}
			option = 2;//so we can tele after replacing properly.
		} else {
			if (!this.handleJewlReplacing(player, jewlItem, false)) {
				return;
			}
		}
		final Location toGo = this.locations()[getLocationIndexForOption(option)][0];
		MainCombat.endCombat(player, 1);
		player.getAttributes().set("stopActions", true);
		player.getWalkingQueue().reset();
		player.getPacketSender().clearMapFlag();
		player.animate(9603);
		player.playGraphic(1684);
		World.getWorld().submit(new Tickable(player, 5) {
			@Override
			public void execute() {
				this.stop();
				player.teleport(toGo);
				player.animate(65535);
				World.getWorld().submit(new Tickable(player, 2) {
					@Override
					public void execute() {
						this.stop();
						player.getAttributes().remove("stopActions");
					}
				});
			}
		});
	}

	@Override
	public void handleOperateOrRub(Player player, boolean operate) {
		// TODO Auto-generated method stub
		if (operate) {//we go straight to edge.
			this.handleOptions(player, -1);
			return;
		}
		switch (getCurrentStage()) {
		case 0:
			this.sendOptionDialogue(player, "Where do you want to go?", "Pest Control", "Duel Arena");
			break;
		}
	}

	@Override
	public int wildernessLevel() {
		// TODO Auto-generated method stub
		return 20;
	}

}
