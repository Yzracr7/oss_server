package com.hyperion.game.content.dialogue.impl;

import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.content.skills.agility.AgilityMovement;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

public class GnomeGuerilla extends Dialogue {

    // 3268 3228
    // 3268 3227

    @Override
    public void endDialogue(Player player, int lastStage) {
        player.getInterfaceSettings().closeInterfaces(true);
    }

    @Override
    public void handleOptions(Player player, int option) {
        if (player.getAttributes().isSet("previousDialogue")) {
            if (player.getAttributes().getInt("previousDialogue") == 2) {
                if (option == 1) {
                    player.teleport(Location.create(1989, 5570, 0));
                    player.getAttributes().set("nextDialogue", 3);
                } else if (option == 2) {
                    player.getAttributes().set("nextDialogue", -1);
                }
            }
        }
        DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
    }

    @Override
    public void handleDialogue1(Player player, NPC npc) throws Exception {
        if (player.getAttributes().isSet("nextDialogue")) {
            setNextStage(player.getAttributes().getInt("nextDialogue"));
        }
        switch (getCurrentStage()) {
            case 0:
                this.sendPlayerDialogue(player, FacialAnimation.DEFAULT, "Can you let me over the gate?");
                player.getAttributes().set("nextDialogue", 1);
                break;
            case 1:
                this.sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "That will be 25k please.");
                player.getAttributes().set("nextDialogue", 2);
                break;
            case 2:
                player.getAttributes().set("demonic_gnome_dialogue", -1);
                this.sendOptionDialogue(player, "Select an Option", "Yeah, alright.", "No, thanks.");
                player.getAttributes().set("nextDialogue", 3);
                player.getAttributes().set("dialogueNPC", npc);
                break;
            case 3:
                final int dialogue_option = player.getAttributes().getInt("demonic_gnome_dialogue");
                switch (dialogue_option) {
                    case 3:
                        if (player.getInventory().hasItemAmount(995, 15000)) {
                        } else {
                            this.sendPlayerDialogue(player, FacialAnimation.SAD, "Oh dear i don't seem to have any money.");
                        }
                        break;
                    case 2:
                        this.sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "The money goes to the city of Al-Kharid.");
                        break;
                    default:
                        System.out.println("dialogue option : " + dialogue_option);
                        break;
                }
                player.getAttributes().remove("demonic_gnome_dialogue");
                player.getAttributes().remove("nextDialogue");
                player.getAttributes().remove("dialogueNPC");

                this.endDialogue(player, -1);
                break;
        }
        player.getAttributes().set("previousDialogue", getCurrentStage());
    }

    @Override
    public void handleDialogue2(Player player, NPC npc) throws Exception {
        // TODO Auto-generated method stub
    }
}
