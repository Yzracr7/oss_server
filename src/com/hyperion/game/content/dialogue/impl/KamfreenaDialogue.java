package com.hyperion.game.content.dialogue.impl;

import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

public class KamfreenaDialogue extends Dialogue {

	@Override
	public void endDialogue(Player player, int lastStage) {
		// TODO Auto-generated method stub
		player.getInterfaceSettings().closeInterfaces(true);
	}

	@Override
	public void handleOptions(Player player, int option) {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("previousDialogue")) {
			if (player.getAttributes().getInt("previousDialogue") == 1) {
				switch (option) {
				case 1:// I'd like to kill some Cyclops
					player.getAttributes().set("nextDialogue", 2);
					break;
				case 2:// nvm
					player.getAttributes().set("nextDialogue", -1);
					break;
				}
			}
		}
		DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
	}

	@Override
	public void handleDialogue1(final Player player, final NPC npc) throws Exception {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("nextDialogue")) {
			setNextStage(player.getAttributes().getInt("nextDialogue"));
			player.getAttributes().set("nextDialogue", 0);
		}
		switch (getCurrentStage()) {
		case 0:
			this.sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "Hello! What can I do for you?");
			player.getAttributes().set("nextDialogue", 1);
			break;
		case 1:
			this.sendOptionDialogue(player, "Pick an Option", "I'd like to kill some Cyclops please.", "Never mind, sorry to bother you.");
			break;
		case 2:
			if (player.getInventory().getItemAmount(8851) >= 100) {
				//int currentDefenderStatus = WarriorsGuild.getNextDefender(player);
				//String defenderName = ItemDefinition.forId(WarriorsGuild.getNextDefender(player)).getName();
				//player.getVariables().setDefenderWave(currentDefenderStatus);
			//	this.sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "The Cyclops are currently dropping:", "" + defenderName + ".");
				player.teleport(2847, player.getLocation().getY(), 2);
				player.getAttributes().set("cyclopsroom", true);
				player.getAttributes().set("nextDialogue", 5);
			} else {
				this.sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "You need at least 100 tokens to enterInstancedRoom the cyclops' room.");
				player.getAttributes().set("nextDialogue", -1);
			}
			break;
		case 5:
			this.sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "Speak to me once you have obtained a defender", "to advance to the next.");
			player.getAttributes().set("nextDialogue", -1);
			break;
		}
		player.getAttributes().set("previousDialogue", getCurrentStage());
	}

	@Override
	public void handleDialogue2(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub

	}
}
