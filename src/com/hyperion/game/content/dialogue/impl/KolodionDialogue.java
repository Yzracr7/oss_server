package com.hyperion.game.content.dialogue.impl;

import com.hyperion.game.Constants;
import com.hyperion.game.content.achievements.*;
import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.content.miniquests.impl.MageArena;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

public class KolodionDialogue extends Dialogue {

	@Override
	public void endDialogue(Player player, int lastStage) {
		// TODO Auto-generated method stub
		player.getInterfaceSettings().closeInterfaces(true);
	}

	@Override
	public void handleOptions(Player player, int option) {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("previousDialogue")) {
			if (player.getAttributes().getInt("previousDialogue") == 2) {
				if (option == 1) {
					player.getAttributes().set("nextDialogue", 3);
				}
			} else if (player.getAttributes().getInt("previousDialogue") == 5) {
				if (option == 1) {
					player.getAttributes().set("nextDialogue", 6);
				} else if (option == 2) {
					player.getAttributes().set("nextDialogue", 21);
				}
			} else if (player.getAttributes().getInt("previousDialogue") == 10) {
				if (option == 1) {
					player.getAttributes().set("nextDialogue", 11);
				} else if (option == 2) {
					player.getAttributes().set("nextDialogue", 22);
				}
			}
		}
		DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
	}

	@Override
	public void handleDialogue1(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("nextDialogue")) {
			setNextStage(player.getAttributes().getInt("nextDialogue"));
		} else {//just started dialogue so we set next dialogue to 1.
			player.getAttributes().set("nextDialogue", 0);
		}
		switch (getCurrentStage()) {
		case 0:
			if (player.getVariables().finishedMageArena) {
				break;
			}
			if (player.getVariables().getMageArenaStage() == 5) {
				this.sendNPCDialogue(player, npc, FacialAnimation.EVIL, "Well done, young adventurer; you truly are a worthy", "battle mage.");
				player.getAttributes().set("nextDialogue", 17);
				break;
			}
			this.sendPlayerDialogue(player, FacialAnimation.EVIL, "Hello there. What is this place?");
			player.getAttributes().set("nextDialogue", 1);
			break;
		case 1:
			this.sendNPCDialogue(player, npc, FacialAnimation.EVIL, "I am the great Kolodion, master of battle magic, and", "this is my battle arena. Top wizards travel from all over", ""+Constants.SERVER_NAME+" to fight here.");
			player.getAttributes().set("nextDialogue", 2);
			break;
		case 2:
			this.sendPlayerDialogue(player, FacialAnimation.EVIL, "Can I fight here?");
			player.getAttributes().set("nextDialogue", 3);
			break;
		case 3:
			this.sendNPCDialogue(player, npc, FacialAnimation.EVIL, "My arena is open to any high level wizard, but this is", "no game. Many wizards fall in this arena, never to rise", "again. The strongest mages have been destroyed.");
			player.getAttributes().set("nextDialogue", 4);
			break;
		case 4:
			this.sendNPCDialogue(player, npc, FacialAnimation.EVIL, "If you're sure you want in?");
			player.getAttributes().set("nextDialogue", 5);
			break;
		case 5:
			this.sendOptionDialogue(player, "Select an Option", "Yes indeedy.", "No I don't.");
			break;
		case 6:
			this.sendPlayerDialogue(player, FacialAnimation.EVIL, "Yes indeedy.");
			player.getAttributes().set("nextDialogue", 7);
			break;
		case 7:
			this.sendNPCDialogue(player, npc, FacialAnimation.EVIL, "Good good. You have a healthy sense of competition.");
			player.getAttributes().set("nextDialogue", 8);
			break;
		case 8:
			this.sendNPCDialogue(player, npc, FacialAnimation.EVIL, "Remember traveler - in my arena, hand-to-hand", "combat is useless. Your strength will diminish as you", "enterInstancedRoom the arena, but the spells you can learn are", "amongst the most powerful in all of "+Constants.SERVER_NAME+".");
			player.getAttributes().set("nextDialogue", 9);
			break;
		case 9:
			this.sendNPCDialogue(player, npc, FacialAnimation.EVIL, "Before I accept you in, we must duel.");
			player.getAttributes().set("nextDialogue", 10);
			break;
		case 10:
			this.sendOptionDialogue(player, "Select an Option", "Okay, let's fight.", "No thanks.");
			break;
		case 11:
			this.sendPlayerDialogue(player, FacialAnimation.EVIL, "Okay, let's fight.");
			player.getAttributes().set("nextDialogue", 12);
			break;
		case 12:
			this.sendNPCDialogue(player, npc, FacialAnimation.EVIL, "I must first check that you are up to scratch.");
			player.getAttributes().set("nextDialogue", 13);
			break;
		case 13:
			if (player.getSkills().getLevel(6) < 60) {
				this.sendNPCDialogue(player, npc, FacialAnimation.EVIL, "You don't meet the requirements to enterInstancedRoom,", "come back with 60 or higher Mage to enterInstancedRoom.");
				player.getAttributes().set("nextDialogue", -1);
			} else {
				this.sendPlayerDialogue(player, FacialAnimation.EVIL, "You don't need to worry about that.");
				player.getAttributes().set("nextDialogue", 14);
			}
			break;
		case 14:
			this.sendNPCDialogue(player, npc, FacialAnimation.EVIL, "Not just any magician can enterInstancedRoom - only the most", "powerful and most feared. Before you can use the", "power of this arena, you must prove yourself against", "me.");
			player.getAttributes().set("nextDialogue", 15);
			break;
		case 15:
			this.endDialogue(player, -1);
			MageArena arena_instance = new MageArena();
			arena_instance.enter(player);
			player.getVariables().setCurrentRoundMiniquest(arena_instance);
			player.getVariables().setSessionName("magearena");
			break;
		case 16:
			this.sendPlayerDialogue(player, FacialAnimation.EVIL, "Hello, Kolodion.");
			player.getAttributes().set("nextDialogue", 17);
			break;
		case 17:
			this.sendNPCDialogue(player, npc, FacialAnimation.EVIL, "Hello, young mage. You're a tough one.");
			player.getAttributes().set("nextDialogue", 18);
			break;
		case 18:
			this.sendPlayerDialogue(player, FacialAnimation.EVIL, "What now?");
			player.getAttributes().set("nextDialogue", 19);
			break;
		case 19:
			this.sendNPCDialogue(player, npc, FacialAnimation.EVIL, "Step into the magic pool. It will take you to a chamber.", "There, you must decide which god you will represent in", "the arena.");
			player.getAttributes().set("nextDialogue", 20);
			break;
		case 20:
			this.sendPlayerDialogue(player, FacialAnimation.EVIL, "Thanks, Kolodion.");
			player.getAttributes().set("nextDialogue", -1);
			player.getVariables().finishedMageArena = true;
			Achievements.increase(player, 17);
			break;
		case 21:
			this.sendPlayerDialogue(player, FacialAnimation.EVIL, "No I don't.");
			player.getAttributes().set("nextDialogue", -1);
			break;
		case 22:
			this.sendPlayerDialogue(player, FacialAnimation.EVIL, "No thanks.");
			player.getAttributes().set("nextDialogue", -1);
			break;
		}
		player.getAttributes().set("previousDialogue", getCurrentStage());
	}

	@Override
	public void handleDialogue2(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
