package com.hyperion.game.content.dialogue.impl;

import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

public class MageOfZamarok extends Dialogue {

	@Override
	public void endDialogue(Player player, int lastStage) {
		// TODO Auto-generated method stub
		player.getInterfaceSettings().closeInterfaces(true);
	}

	@Override
	public void handleOptions(Player player, int option) {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("previousDialogue")) {
			if (player.getAttributes().getInt("previousDialogue") == 2) {
				if (option == 2) {
					player.getAttributes().set("nextDialogue", 3);
				} else if (option == 3) {
					player.getAttributes().set("nextDialogue", 4);
				}
			}
		}
		DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
	}

	@Override
	public void handleDialogue1(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("nextDialogue")) {
			setNextStage(player.getAttributes().getInt("nextDialogue"));
		} else {//just started dialogue so we set next dialogue to 1.
			player.getAttributes().set("nextDialogue", 0);
		}
		switch (getCurrentStage()) {
		case 0:
			this.sendPlayerDialogue(player, FacialAnimation.DEFAULT, "What are you doing here?");
			player.getAttributes().set("nextDialogue", 1);
			break;
		case 1:
			this.sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "I'm teleporting players into the", "abyss. Do you want to go?");
			player.getAttributes().set("nextDialogue", 2);
			break;
		case 2:
			this.sendOptionDialogue(player, "Pick an Option", "Yes, let's go.", "No thanks.");
			break;
		case 3://Yes
			this.sendPlayerDialogue(player, FacialAnimation.DEFAULT, "Yes, let's go.");
			player.getAttributes().set("nextDialogue", 5);
			break;
		case 4://no
			this.sendPlayerDialogue(player, FacialAnimation.DEFAULT, "No thanks.");
			player.getAttributes().set("nextDialogue", -1);
			break;
		case 5:
			player.teleport(3029, 4834, 0);
			//HorrorFromTheDeep.enterGame(player);
			endDialogue(player, -1);
			break;
		}
		player.getAttributes().set("previousDialogue", getCurrentStage());
	}

	@Override
	public void handleDialogue2(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
