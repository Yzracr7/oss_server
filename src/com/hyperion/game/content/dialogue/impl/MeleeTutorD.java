package com.hyperion.game.content.dialogue.impl;

import com.hyperion.game.Constants;
import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.shopping.ShopManager;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Apr 3, 2015
 */
public class MeleeTutorD extends Dialogue {


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.dialogue.Dialogue#endDialogue(com.hyperion.
	 * game.world.entity.player.Player, int)
	 */
	@Override
	public void endDialogue(Player player, int lastStage) {
		player.getInterfaceSettings().closeInterfaces(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.dialogue.Dialogue#handleOptions(com.hyperion
	 * .game.world.entity.player.Player, int)
	 */
	@Override
	public void handleOptions(Player player, int option) {
		if (player.getAttributes().isSet("previousDialogue")) {
			if (player.getAttributes().getInt("previousDialogue") == 1) {
				switch (option) {
				case 1:
					player.getAttributes().set("nextDialogue", 2);
					break;
				case 2:
					for(int i = 0; i < Constants.IRONMAN_SET.length; i ++) {
						player.getInventory().addItem(new Item(Constants.IRONMAN_SET[i], 1));
					}
					player.getInventory().refresh();
					player.getPacketSender().sendMessage("Your Ironman set has been added to your inventory.");
					player.getAttributes().set("nextDialogue", -1);
					player.getPacketSender().sendChatboxDialogue(true, "Your Ironman set has been added to your inventory.");
					break;
				case 3:
					// TODO: help
					endDialogue(player, -1);
					break;
				}
			}
		}
		DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.dialogue.Dialogue#handleDialogue1(com.hyperion
	 * .game.world.entity.player.Player, com.hyperion.game.world.entity.npc.NPC)
	 */
	@Override
	public void handleDialogue1(Player player, NPC npc) throws Exception {
		if (player.getAttributes().isSet("nextDialogue")) {
			setNextStage(player.getAttributes().getInt("nextDialogue"));
			player.getAttributes().set("nextDialogue", 0);
		}
		switch (getCurrentStage()) {
		case -1:
			endDialogue(player, -1);
			break;
		case 0:
			if (player.getDetails().isIronman()) {
				sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "Hello, ironman " + player.getName() + ". How can I help you?");
				player.getAttributes().set("nextDialogue", 1);
			} else {
				sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "I can only offer support for ironman accounts.");
				player.getAttributes().set("nextDialogue", -1);
			}
			break;
		case 1:
			sendOptionDialogue(player, "Select an Option", "View Ironman Supplies", "Can I have another Ironman armor set?", "I need help.");
			break;
		case 2:
			endDialogue(player, -1);
			ShopManager.getShopManager().openShop(player, 36);
			break;
		}
		player.getAttributes().set("previousDialogue", getCurrentStage());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.content.dialogue.Dialogue#handleDialogue2(com.hyperion
	 * .game.world.entity.player.Player, com.hyperion.game.world.entity.npc.NPC)
	 */
	@Override
	public void handleDialogue2(Player player, NPC npc) throws Exception {
	}

}
