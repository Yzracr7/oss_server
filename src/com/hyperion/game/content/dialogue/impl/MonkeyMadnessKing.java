package com.hyperion.game.content.dialogue.impl;

import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.content.miniquests.impl.MonkeyMadness;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.shopping.ShopManager;

public class MonkeyMadnessKing extends Dialogue {

	@Override
	public void endDialogue(Player player, int lastStage) {
		// TODO Auto-generated method stub
		player.getInterfaceSettings().closeInterfaces(true);
	}

	@Override
	public void handleOptions(Player player, int option) {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("previousDialogue")) {
			if (player.getAttributes().getInt("previousDialogue") == 4) {
				if (option == 1) {
					player.getAttributes().set("nextDialogue", 5);
				} else if (option == 2) {
					player.getAttributes().set("nextDialogue", 6);
				}
			} else if (player.getAttributes().getInt("previousDialogue") == 10) {
				if (option == 1) {
					player.getAttributes().set("nextDialogue", 11);
				} else if (option == 2) {
					player.getAttributes().set("nextDialogue", 12);
				}
			} else if (player.getAttributes().getInt("previousDialogue") == 17) {
					if (option == 1) {
						if (player.getVariables().isDemon_killed()) {
							endDialogue(player, -1);
							ShopManager.getShopManager().openShop(player, 21);
						} else {
							player.getPacketSender().sendMessage("You can't view this shop yet.");
							endDialogue(player, -1);
						}
					} else if (option == 2) {
						player.getAttributes().set("nextDialogue", 8);
					}
			}
		}
		DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
	}

	@Override
	public void handleDialogue1(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("nextDialogue")) {
			setNextStage(player.getAttributes().getInt("nextDialogue"));
		} 
		switch (getCurrentStage()) {
		case 0:
			if (player.getVariables().isDemon_killed()) {
				this.sendNPCDialogue(player, npc, FacialAnimation.HAPPY, "You showed us great bravery down there "+player.getName()+".", "We can't thank you enough.");
				player.getAttributes().set("nextDialogue", 15);
				break;
			}
			this.sendPlayerDialogue(player, FacialAnimation.HAPPY, "King, you look worried. Is anything the matter?");
			player.getAttributes().set("nextDialogue", 1);
			break;
		case 1:
			this.sendNPCDialogue(player, npc, FacialAnimation.HAPPY, "Nothing in particular ... Well actually, yes - there is.");
			player.getAttributes().set("nextDialogue", 2);
			break;
		case 2:
			this.sendPlayerDialogue(player, FacialAnimation.HAPPY, "What is it?");
			player.getAttributes().set("nextDialogue", 3);
			break;
		case 3:
			this.sendNPCDialogue(player, npc, FacialAnimation.HAPPY, "My envoy of Royal guards, the 10th squad, were attacked", "and killed. I need a brave warrior to destroy the monster.", "Can you help?");
			player.getAttributes().set("nextDialogue", 4);
			break;
		case 4:
			this.sendOptionDialogue(player, "Pick an Option", "Yes, I'm strong enough.", "Sorry, sounds like a suicide mission.");
			break;
		case 5://yes
			this.sendPlayerDialogue(player, FacialAnimation.HAPPY, "Yes, I'm strong enough.");
			player.getAttributes().set("nextDialogue", 7);
			break;
		case 6://no
			this.sendPlayerDialogue(player, FacialAnimation.HAPPY, "Sorry, sounds like a suicide mission.");
			player.getAttributes().set("nextDialogue", 8);
			break;
		case 7:
			this.sendNPCDialogue(player, npc, FacialAnimation.HAPPY, "Thank you "+player.getName()+", you are indeed brave.", "I shall teleport you to battle.");
			player.getAttributes().set("nextDialogue", 9);
			break;
		case 8:
			this.endDialogue(player, -1);
			break;
		case 9:
			this.sendNPCDialogue(player, npc, FacialAnimation.HAPPY, "Are you ready?");
			player.getAttributes().set("nextDialogue", 10);
			break;
		case 10:
			this.sendOptionDialogue(player, "Pick an Option", "Yes, take me.", "No, not yet.");
			break;
		case 11:
			this.sendPlayerDialogue(player, FacialAnimation.HAPPY, "Yes, take me.");
			player.getAttributes().set("nextDialogue", 13);
			break;
		case 12:
			this.sendPlayerDialogue(player, FacialAnimation.HAPPY, "No, not yet.");
			player.getAttributes().set("nextDialogue", 8);
			break;
		case 13:
			if (player.getSkills().getLevel(Skills.ATTACK) < 60) {
				this.sendNPCDialogue(player, npc, FacialAnimation.HAPPY, "You don't meet the requirements to enterInstancedRoom,", "check your quest journal and come back again when you do.");
				player.getAttributes().set("nextDialogue", -1);
			} else {
				this.sendNPCDialogue(player, npc, FacialAnimation.EVIL, "Very well then, good luck adventurer.");
				player.getAttributes().set("nextDialogue", 14);
			}
			break;
		case 14:
			this.endDialogue(player, -1);
			MonkeyMadness.enterGame(player);
			break;
		case 15:
			this.sendPlayerDialogue(player, FacialAnimation.LAUGH_1, "I'm sure there's a reward for such a task?");
			player.getAttributes().set("nextDialogue", 16);
			break;
		case 16:
			this.sendNPCDialogue(player, npc, FacialAnimation.HAPPY, "Of course, would you like to view the Tree Gnome shop?");
			player.getAttributes().set("nextDialogue", 17);
			break;
		case 17:
			this.sendOptionDialogue(player, "Pick an Option", "Yes.", "No thanks.");
			break;
		}
		player.getAttributes().set("previousDialogue", getCurrentStage());
	}

	@Override
	public void handleDialogue2(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
