package com.hyperion.game.content.dialogue.impl;

import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.shopping.ShopManager;

public class RewardsTrader extends Dialogue {

	@Override
	public void endDialogue(Player player, int lastStage) {
		// TODO Auto-generated method stub
		player.getInterfaceSettings().closeInterfaces(true);
	}

	@Override
	public void handleOptions(final Player player, int option) {
		if (player.getAttributes().isSet("previousDialogue")) {
			if (player.getAttributes().getInt("previousDialogue") == 0) {
				if (option == 1) {
					player.getAttributes().set("nextDialogue", -1);
					endDialogue(player, -1);
					boolean lastClaimSet = false;
					long lastCheck = 0;
					if (player.getAttributes().isSet("last_claim")) {
						lastClaimSet = true;
						lastCheck = player.getAttributes().getLong("last_claim");
					}
					if (lastClaimSet && System.currentTimeMillis() - lastCheck < 60000) {
						player.getPacketSender().sendChatboxDialogue(true, "You must wait 1 minute before trying to claim items again.");
						return;
					}
					player.getAttributes().set("last_claim", System.currentTimeMillis());
					player.getPacketSender().sendChatboxDialogue(false, "Checking for Token purchases...");
					//World.getWorld().forumIntegration.claimDonations(player);
				} else if (option == 2) {
					endDialogue(player, -1);
					ShopManager.getShopManager().openShop(player, 35);
				}
			}
		}
		DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
	}

	@Override
	public void handleDialogue1(Player player, NPC npc) throws Exception {
		this.sendOptionDialogue(player, "Select an Option", "Claim Item Shop", "Exchange Vote Points");
		player.getAttributes().set("previousDialogue", getCurrentStage());
	}

	@Override
	public void handleDialogue2(Player player, NPC npc) throws Exception {

	}
}
