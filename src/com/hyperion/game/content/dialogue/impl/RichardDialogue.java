package com.hyperion.game.content.dialogue.impl;

import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.shopping.ShopManager;

public class RichardDialogue extends Dialogue {

	@Override
	public void endDialogue(Player player, int lastStage) {
		// TODO Auto-generated method stub
		player.getPacketSender().closeChatboxInterface();
	}

	@Override
	public void handleOptions(Player player, int option) {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("previousDialogue")) {
			if (player.getAttributes().getInt("previousDialogue") == 0) {
				if (option == 1) {
					ShopManager.getShopManager().openShop(player, 10);
				} else if (option == 2) {
					ShopManager.getShopManager().openShop(player, 11);
				}
				player.getAttributes().set("nextDialogue", -1);
			}
		}
		DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
	}

	@Override
	public void handleDialogue1(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		sendOptions(player, npc);
	}

	@Override
	public void handleDialogue2(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		sendOptions(player, npc);
	}
	
	private void sendOptions(Player player, NPC npc) throws Exception {
		if (player.getAttributes().isSet("nextDialogue")) {
			setNextStage(player.getAttributes().getInt("nextDialogue"));
		} else {//just started dialogue so we set next dialogue to 1.
			player.getAttributes().set("nextDialogue", 0);
		}
		switch (getCurrentStage()) {
		case 0:
			sendOptionDialogue(player, "Pick a Shop", "Richard's Teamcapes 1", "Richard's Teamcapes 2");
			break;
		}
		player.getAttributes().set("previousDialogue", getCurrentStage());
	}

}
