package com.hyperion.game.content.dialogue.impl;

import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

public class RingEnchanter extends Dialogue {

	@Override
	public void endDialogue(Player player, int lastStage) {
		// TODO Auto-generated method stub
		player.getInterfaceSettings().closeInterfaces(true);
	}

	@Override
	public void handleOptions(final Player player, int option) {
		if (player.getAttributes().isSet("previousDialogue")) {
			if (player.getAttributes().getInt("previousDialogue") == 0) {
				if (option == 1) {
					player.getAttributes().set("nextDialogue", -1);
					endDialogue(player, -1);
					player.getPacketSender().sendChatboxDialogue(false,
							"Searching inventory for dagganoth rings..");
						if(exchangeRings(player))
						    player.getPacketSender().sendChatboxDialogue(true, "Your rings have been exchanged.");   					
				} else if (option == 2) {
					endDialogue(player, -1);
				}
			}
		}
		DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
	}
	
	int[][] ringArray = {{6737, 15220},{6735, 15020}, {6733, 15019}, {6731, 15018}};
	private boolean exchangeRings(Player player)
	{
		boolean found = false;
		for (Item item: player.getInventory().getItems())
		{
			if (item == null)
				continue;
			
			for (int i = 0; i < ringArray.length;i++)
			{
				if (item.getId() == ringArray[i][0])
				{
					if (!player.getInventory().hasItemAmount(995, 5_000_000))
					{
						player.getPacketSender().sendChatboxDialogue(true, "You do not have enough coins (5M per ring) to imbue all of your rings.");
						return false;
					}
					player.getInventory().deleteItem(995, 5_000_000);
					player.getInventory().deleteItem(item.getId());
					player.getInventory().addItem(ringArray[i][1]);
					player.getPacketSender().sendMessage("Your " + item.getDefinition().getName() + " has been imbued.");
					found = true;
					break;
				}
			}
		}
		if (!found)
		    player.getPacketSender().sendChatboxDialogue(true, "You do not have any important rings in your inventory.");   					
		return found;
	}

	@Override
		public void handleDialogue1(Player player, NPC npc) throws Exception {
		if (player.getAttributes().isSet("nextDialogue")) {
			setNextStage(player.getAttributes().getInt("nextDialogue"));
		} else {//just started dialogue so we set next dialogue to 1.
		//player.getAttributes().set("nextDialogue", 0);
		}
		switch(getCurrentStage())
		{
		case 0:
		this.sendNPCDialogue(player, npc, FacialAnimation.HAPPY, "I am enchanting rings for five million gp.", "Do you want me to enchant yours?");
		player.getAttributes().set("nextDialogue", 1);
		break;
		case 1:
		this.sendOptionDialogue(player, "Select an option.", "Imbue my rings for 5m each.", "Nevermind.");
		player.getAttributes().set("previousDialogue", getCurrentStage());
		break;
		}
		
		}

	@Override
	public void handleDialogue2(Player player, NPC npc) throws Exception {

	}
}
