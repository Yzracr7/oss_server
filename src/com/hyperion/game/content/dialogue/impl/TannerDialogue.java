package com.hyperion.game.content.dialogue.impl;

import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

public class TannerDialogue extends Dialogue {

	@Override
	public void endDialogue(Player player, int lastStage) {
		player.getInterfaceSettings().closeInterfaces(true);
	}

	@Override
	public void handleOptions(Player player, int option) {

	}

	@Override
	public void handleDialogue1(Player player, NPC npc) throws Exception {
		if (player.getAttributes().isSet("nextDialogue")) {
			setNextStage(player.getAttributes().getInt("nextDialogue"));
		} else {// just started dialogue so we set next dialogue to 1.
			player.getAttributes().set("nextDialogue", 0);
		}
		switch (getCurrentStage()) {
		case 0:
			sendPlayerDialogue(player, FacialAnimation.DEFAULT, "Can you tan my hides?");
			player.getAttributes().set("nextDialogue", 1);
			break;
		case 1:
			sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "Yes, one moment please.");
			player.getAttributes().set("nextDialogue", 2);
			break;
		case 2:
			tanHides(player);
			sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "Your hides have been tanned.");
			player.getAttributes().set("nextDialogue", -1);
			break;
		}
		player.getAttributes().set("previousDialogue", getCurrentStage());
	}

	@Override
	public void handleDialogue2(Player player, NPC npc) throws Exception {

	}

	private void tanHides(Player player) {
		for (int i = 0; i < 28; i++) {
			int item = player.getInventory().getItemInSlot(i);
			if (item == -1) {
				continue;
			}
			for (int j = 0; j < HIDES.length; j++) {
				if (item == HIDES[j][1]) {
					player.getInventory().replaceItem(item, HIDES[j][0]);
				}
			}
		}
	}
	
	private static final int[][] HIDES = new int[][] {
		{ 1741, 1739 },
		{ 1745, 1753 },
		{ 2505, 1751 },
		{ 2507, 1749 },
		{ 2509, 1747 }
		//Tanned, untanned
	};

}
