package com.hyperion.game.content.dialogue.impl;

import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.TeleportAreaLocations;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.game.ChatColors;

public class TeleportWizard extends Dialogue {

    @Override
    public void endDialogue(Player player, int lastStage) {
        // TODO Auto-generated method stub
        player.getInterfaceSettings().closeInterfaces(true);
    }

    @Override
    public void handleOptions(Player player, int option) {
        // TODO Auto-generated method stub
        if (player.getAttributes().isSet("previousDialogue")) {
            if (player.getAttributes().getInt("previousDialogue") == 2) {
                if (option == 1) {
                    player.getAttributes().set("nextDialogue", 3);
                } else if (option == 2) {
                    player.getAttributes().set("nextDialogue", -1);
                }
            }
        }
        DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
    }

    @Override
    public void handleDialogue1(Player player, NPC npc) throws Exception {
        // TODO Auto-generated method stub
        if (player.getAttributes().isSet("nextDialogue")) {
            setNextStage(player.getAttributes().getInt("nextDialogue"));
        } else {//just started dialogue so we set next dialogue to 1.
            //	player.getAttributes().set("nextDialogue", 0);
        }
        switch (getCurrentStage()) {
            case 0:
                this.sendPlayerDialogue(player, FacialAnimation.DEFAULT, "What are you doing here?");
                player.getAttributes().set("nextDialogue", 1);
                break;
            case 1:
                this.sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "I am the teleport wizard!", "BUT not like most..");
                player.getAttributes().set("nextDialogue", 2);
                break;
            case 2:
                this.sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "Everyday i have a different location,", "that i may operate you for a fee.", "Todays Location : <col=" + ChatColors.MAROON + ">KBD</col>");
                player.getAttributes().set("nextDialogue", 3);
                break;
            case 3:
                player.getAttributes().set("teleport_wizard_option", true);
                this.sendOptionDialogue(player, "Select an Option", "Teleport Me (5,000gp)", "I'll Pass");
                player.getAttributes().set("nextDialogue", 4);
                player.getAttributes().set("dialogueNPC", npc);
                break;
            case 4:
                if ((boolean) player.getAttributes().get("teleport_wizard_option")) {
                    if (player.getInventory().hasItemAmount(995, 5000)) {
                        Location loc = TeleportAreaLocations.TeleportWizardLocations.KING_BLACK_DRAGON.getLoc();
                        player.teleport(loc.getX(), loc.getY(), loc.getZ());
                        player.getInventory().deleteItem(995, 5000);
                    } else {
                        player.sendMessage("Come back when you have 5,000 gold.");
                    }
                    player.getAttributes().remove("teleport_wizard_option");
                    player.getAttributes().remove("nextDialogue");
                    player.getAttributes().remove("dialogueNPC");
                }
                this.endDialogue(player, -1);
                break;
        }
        player.getAttributes().set("previousDialogue", getCurrentStage());
    }

    @Override
    public void handleDialogue2(Player player, NPC npc) throws Exception {
        // TODO Auto-generated method stub

    }

}
