package com.hyperion.game.content.dialogue.impl;

import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

public class Trader extends Dialogue {

    @Override
    public void endDialogue(Player player, int lastStage) {
        player.getInterfaceSettings().closeInterfaces(true);
    }

    @Override
    public void handleOptions(Player player, int option) {
        if (player.getAttributes().isSet("previousDialogue")) {
            if (player.getAttributes().getInt("previousDialogue") == 2) {
                if (option == 1) {
                    player.getAttributes().set("nextDialogue", 3);
                } else if (option == 2) {
                    player.getAttributes().set("nextDialogue", -1);
                }
            }
        }
        DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
    }

    @Override
    public void handleDialogue1(Player player, NPC npc) throws Exception {
        if (player.getAttributes().isSet("nextDialogue")) {
            setNextStage(player.getAttributes().getInt("nextDialogue"));
        }
        npc.setInteractingEntity(player);
        npc.face(player.getLocation());
        switch (getCurrentStage()) {
            case 0:
                this.sendNPCDialogue(player, npc, FacialAnimation.HAPPY, "Trade me to see my what i have for sale.");
                player.getAttributes().set("nextDialogue", -1);
                break;
        }
        player.getAttributes().set("previousDialogue", getCurrentStage());
    }

    @Override
    public void handleDialogue2(Player player, NPC npc) throws Exception {
        // TODO Auto-generated method stub
    }
}
