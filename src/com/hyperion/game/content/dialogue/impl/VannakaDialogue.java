package com.hyperion.game.content.dialogue.impl;

import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.content.skills.slayer.SlayerTasks;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;

public class VannakaDialogue extends Dialogue {

    @Override
    public void endDialogue(Player player, int lastStage) {
        // TODO Auto-generated method stub
        player.getInterfaceSettings().closeInterfaces(true);
    }

    @Override
    public void handleOptions(Player player, int option) {
        // TODO Auto-generated method stub
        if (player.getAttributes().isSet("previousDialogue")) {
            if (player.getAttributes().getInt("previousDialogue") == 0) {
                if (option == 1) {
                    player.getAttributes().set("nextDialogue", 1);
                } else if (option == 2) {
                    player.getAttributes().set("nextDialogue", 4);
                }
            } else if (player.getAttributes().getInt("previousDialogue") == 1) {
                if (option == 1) {
                    player.getAttributes().set("nextDialogue", 2);
                } else if (option == 2) {
                    player.getAttributes().set("nextDialogue", 3);
                }
            } else if (player.getAttributes().getInt("previousDialogue") == 4) {
                if (option == 1) {
                    player.getAttributes().set("nextDialogue", 5);
                } else if (option == 2) {
                    player.getAttributes().set("nextDialogue", 6);
                }
            }
        }
        DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
    }

    @Override
    public void handleDialogue1(Player player, NPC npc) throws Exception {
        int combat = player.getSkills().getCombatLevel();
        // TODO Auto-generated method stub
        if (player.getAttributes().isSet("nextDialogue")) {
            setNextStage(player.getAttributes().getInt("nextDialogue"));
        } else {//just started dialogue so we set next dialogue to 1.
            player.getAttributes().set("nextDialogue", 0);
        }
        String title = "Pick an Option";
        switch (getCurrentStage()) {
            case 0:
                if (!player.getDetails().isDonator()) {
                    this.sendOptionDialogue(player, title, "New Task", "Reset Task (" + NumberUtils.price_with_commas(com.hyperion.game.content.skills.slayer.SlayerTasks.resetAmount) + " coins)");
                } else {
                    this.sendOptionDialogue(player, title, "New Task", "Reset Task (" + NumberUtils.price_with_commas(com.hyperion.game.content.skills.slayer.SlayerTasks.resetAmount / 2) + " coins)");
                }
                break;
            case 1://Low tasks
                if (combat <= 45) {
                    SlayerTasks.assign(player, 1);
                    this.endDialogue(player, -1);
                } else if (combat > 45 && combat <= 65) {
                    this.sendOptionDialogue(player, "Pick an Option", "Easy Task", "Medium Task");
                } else if (combat > 65 && combat <= 75) {
                    SlayerTasks.assign(player, 2);
                    this.endDialogue(player, -1);
                } else if (combat > 75 && combat <= 80) {
                    this.sendOptionDialogue(player, "Pick an Option", "Medium Task", "Hard Task");
                } else if (combat > 80 && combat <= 110) {
                    SlayerTasks.assign(player, 3);
                    this.endDialogue(player, -1);
                } else if (combat > 110) {
                    this.sendOptionDialogue(player, "Pick an Option", "Hard Task", "Extreme Task");
                }
                break;
            case 2://Task option 1
                if (combat > 45 && combat <= 65) {
                    SlayerTasks.assign(player, 1);
                    this.endDialogue(player, -1);
                } else if (combat > 75 && combat <= 80) {
                    SlayerTasks.assign(player, 2);
                    this.endDialogue(player, -1);
                } else if (combat > 80) {
                    SlayerTasks.assign(player, 3);
                    this.endDialogue(player, -1);
                }
                break;
            case 3://Task option 2
                if (combat > 45 && combat <= 65) {
                    SlayerTasks.assign(player, 2);
                    this.endDialogue(player, -1);
                } else if (combat > 75 && combat <= 80) {
                    SlayerTasks.assign(player, 3);
                    this.endDialogue(player, -1);
                } else if (combat > 110) {
                    SlayerTasks.assign(player, 4);
                    this.endDialogue(player, -1);
                }
                break;
            case 4://Reset task
                this.sendOptionDialogue(player, "Pick an Option", "Yes, reset.", "No, nevermind.");
                break;
            case 5://Yes
                SlayerTasks.reset_task(player);
                this.endDialogue(player, -1);
                break;
            case 6://No
                this.endDialogue(player, -1);
                break;
        }
        player.getAttributes().set("previousDialogue", getCurrentStage());
    }

    @Override
    public void handleDialogue2(Player player, NPC npc) throws Exception {
        // TODO Auto-generated method stub

    }

}