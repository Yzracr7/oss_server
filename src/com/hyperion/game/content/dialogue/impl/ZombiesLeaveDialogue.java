package com.hyperion.game.content.dialogue.impl;

import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.content.minigames.pestcontrol.PestSession;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

public class ZombiesLeaveDialogue extends Dialogue {

	@Override
	public void endDialogue(Player player, int lastStage) {
		// TODO Auto-generated method stub
		player.getPacketSender().closeChatboxInterface();
	}

	@Override
	public void handleOptions(Player player, int option) {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("previousDialogue")) {
			if (player.getAttributes().getInt("previousDialogue") == 0) {
				if (option == 1) {
					player.getAttributes().set("nextDialogue", -1);
					if(player.getVariables().getGameSession() instanceof PestSession) {
						player.getVariables().getGameSession().handle_death(player, null);
					}
				} else if (option == 2) {
					player.getAttributes().set("nextDialogue", -1);
					endDialogue(player, -1);
				}
			}
		}
		DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
	}

	@Override
	public void handleDialogue1(Player player, NPC npc) throws Exception {
		if (player.getAttributes().isSet("nextDialogue")) {
			setNextStage(player.getAttributes().getInt("nextDialogue"));
		} else {// just started dialogue so we set next dialogue to 1.
			player.getAttributes().set("nextDialogue", 0);
		}
		switch (getCurrentStage()) {
		case 0:
			this.sendOptionDialogue(player, "Would you like to leave?", "Yes, get me out of here.", "Nevermind.");
			break;
		default:// When theres no more dialogue..
			player.getAttributes().set("nextDialogue", -1);
			DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
			break;
		}
		final int currentDialogue = player.getAttributes().getInt("nextDialogue");
		if (currentDialogue != -1) {
			player.getAttributes().set("nextDialogue", currentDialogue + 1);
		}
		player.getAttributes().set("previousDialogue", getCurrentStage());
	}

	@Override
	public void handleDialogue2(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
	}

}
