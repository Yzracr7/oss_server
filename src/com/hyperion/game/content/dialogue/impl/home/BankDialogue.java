package com.hyperion.game.content.dialogue.impl.home;

import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.impl.Bank;

public class BankDialogue extends Dialogue {

	@Override
	public void endDialogue(Player player, int lastStage) {
		// TODO Auto-generated method stub
		player.getInterfaceSettings().closeInterfaces(true);
	}

	@Override
	public void handleOptions(Player player, int option) {
		if (player.getAttributes().isSet("previousDialogue")) {
			if (player.getAttributes().getInt("previousDialogue") == 2) {
				if (option == 2) {
					player.getAttributes().set("nextDialogue", 3);
				} else if (option == 3) {
					player.getAttributes().set("nextDialogue", -1);
				}
			}
		}
		DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
	}

	@Override
	public void handleDialogue1(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("nextDialogue")) {
			setNextStage(player.getAttributes().getInt("nextDialogue"));
		} else {//just started dialogue so we set next dialogue to 1.
			player.getAttributes().set("nextDialogue", 0);
		}
		switch (getCurrentStage()) {
		case 0:
			this.sendPlayerDialogue(player, FacialAnimation.DEFAULT, "I'd like to view my pin settings please.");
			player.getAttributes().set("nextDialogue", 1);
			break;
		case 1:
			this.sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "Sure thing.");
			player.getAttributes().set("nextDialogue", 2);
			break;
		case 2:
			Bank.openPinSettings(player, 1);
			player.getPacketSender().closeChatboxInterface();
			break;
		}
		player.getAttributes().set("previousDialogue", getCurrentStage());
	}

	@Override
	public void handleDialogue2(Player player, NPC npc) throws Exception {
		
	}

}
