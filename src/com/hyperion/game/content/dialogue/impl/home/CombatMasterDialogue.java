package com.hyperion.game.content.dialogue.impl.home;

import com.hyperion.game.content.QuestTab;
import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.utility.game.ChatColors;
import com.hyperion.utility.game.InputAction;

public class CombatMasterDialogue extends Dialogue {

	@Override
	public void endDialogue(Player player, int lastStage) {
		// TODO Auto-generated method stub
		player.getPacketSender().closeChatboxInterface();
	}

	@Override
	public void handleOptions(Player player, int option) {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("previousDialogue")) {
			if (player.getAttributes().getInt("previousDialogue") == 0) {
				if (option == 1) {
					player.getVariables().setExpLocked(!player.getVariables().isExpLocked());
					QuestTab.refresh(player);
					player.getAttributes().set("nextDialogue", -1);
					player.getPacketSender().sendMessage("Your experience has been " + (player.getVariables().isExpLocked() ? "locked." : "unlocked"));
				} else if (option == 2) {
					if (player.getEquipment().getTotalItems() == 0) {
						player.getPacketSender().requestStringInput(new InputAction<String>("Enter the Skill Name to Reset (500K Cost)") {

							@Override
							public void handleInput(String input) {
								if (input == null || input.length() <= 0) {
									endDialogue(player, -1);
									return;
								}
								int skillId = -1;
								String[] skillName = Skills.SKILL_NAME;
								for (int i = 0; i < skillName.length; i++) {
									String name = skillName[i];
									if (name.equalsIgnoreCase(input)) {
										skillId = i;
										break;
									}
								}
								if (skillId == -1) {
									player.getPacketSender().sendMessage("<col=" + ChatColors.RED + ">No such skill as " + input + ", try again.");
									endDialogue(player, -1);
									return;
								}
								if (skillId > 6) {
									player.getPacketSender().sendMessage("<col=" + ChatColors.RED + ">You can only reset a combat skill.");
									endDialogue(player, -1);
									return;
								}
								if (player.takeMoney(500000)) {
									player.getAttributes().set("nextDialogue", -1);
									endDialogue(player, -1);
									player.getPacketSender().sendChatboxDialogue(true, "Your " + input + " experience has been reset.");
									player.getSkills().setXp(skillId, skillId == 3 ? player.getSkills().getXpForLevel(10) : 0);
									player.getSkills().setLevel(skillId, skillId == 3 ? 10 : 1);
									player.getPacketSender().sendSkillLevels();
									player.getPacketSender().sendMessage("Your " + input + " experience has been reset.");
								} else {
									endDialogue(player, -1);
									player.getPacketSender().sendChatboxDialogue(true, "You don't have 500K coins to pay for this.");
								}
							}
						});
						// player.getAttributes().set("nextDialogue", -1);
						// endDialogue(player, -1);
						// player.getPacketSender().sendChatboxDialogue(true,
						// "Your Defence Exp has been reset.");
						// player.getSkills().setXp(Skills.DEFENCE, 0);
						// player.getSkills().setLevel(Skills.DEFENCE, 1);
						// player.getPacketSender().sendSkillLevels();
						// player.getPacketSender().sendMessage("Your Defence Exp has been reset.");
					} else {
						player.getAttributes().set("nextDialogue", -1);
						endDialogue(player, -1);
						player.getPacketSender().sendChatboxDialogue(true, "You must take off your Equipment to do this.");
						player.getPacketSender().sendMessage("You must take off your equipment to do this.");
					}
				}
			}
		}
		DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
	}

	@Override
	public void handleDialogue1(Player player, NPC npc) throws Exception {
		if (player.getAttributes().isSet("nextDialogue")) {
			setNextStage(player.getAttributes().getInt("nextDialogue"));
		} else {// just started dialogue so we set next dialogue to 1.
			player.getAttributes().set("nextDialogue", 0);
		}
		switch (getCurrentStage()) {
		case 0:
			this.sendOptionDialogue(player, "Pick an Option", "Lock/unlock Combat Exp", "Reset A Combat Skill's Exp");
			break;
		default:// When theres no more dialogue..
			player.getAttributes().set("nextDialogue", -1);
			DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
			break;
		}
		final int currentDialogue = player.getAttributes().getInt("nextDialogue");
		if (currentDialogue != -1) {
			player.getAttributes().set("nextDialogue", currentDialogue + 1);
		}
		player.getAttributes().set("previousDialogue", getCurrentStage());
	}

	@Override
	public void handleDialogue2(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
	}

}
