package com.hyperion.game.content.dialogue.impl.home;

import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Apr 5, 2015
 */
public class EasterBunnyD extends Dialogue {

	@Override
	public void endDialogue(Player player, int lastStage) {
		player.getPacketSender().closeChatboxInterface();
	}

	@Override
	public void handleOptions(Player player, int option) {
	}

	@Override
	public void handleDialogue1(Player player, NPC npc) throws Exception {
		if (player.getAttributes().isSet("nextDialogue")) {
			setNextStage(player.getAttributes().getInt("nextDialogue"));
		} else {//just started dialogue so we set next dialogue to 1.
			player.getAttributes().set("nextDialogue", 0);
		}
		switch(getCurrentStage()) {
		case 0:
			if (!player.getVariables().isReceivedEasterRewards()) {
				player.getInventory().addDroppable(new Item(1037));
				player.getVariables().setReceivedEasterRewards(true);
				sendNPCDialogue(player, npc, FacialAnimation.HAPPY, "Happy easter, " + player.getName() + ". Enjoy the treats.");
			} else {
				sendNPCDialogue(player, npc, FacialAnimation.EVIL, "I don't have any more treats for you.");
			}
			player.getAttributes().set("nextDialogue", 1);
			break;
		case 1:
			endDialogue(player, -1);
			break;
		}
	}

	@Override
	public void handleDialogue2(Player player, NPC npc) throws Exception {
	}

}
