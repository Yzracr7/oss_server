package com.hyperion.game.content.dialogue.impl.home;

import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.game.InputAction;


public class TitleMasterDialogue extends Dialogue {

	@Override
	public void endDialogue(Player player, int lastStage) {
		// TODO Auto-generated method stub
		player.getPacketSender().closeChatboxInterface();
	}
 
	@Override
	public void handleOptions(Player player, int option) {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("previousDialogue")) {
			if (player.getAttributes().getInt("previousDialogue") == 0) {
				if (option == 1) {
					for(int i = 0; i <= 20; i++) {
						player.getPacketSender().sendInterfaceConfig(648, 166 + i, false);
					}
					player.getPacketSender().sendInterfaceConfig(648, 165, false);
					player.getPacketSender().modifyText("None", 648, 116);
					player.getPacketSender().modifyText("Cost: 0 Loyalty Points", 648, 118);
					player.getPacketSender().modifyText("Title selection (Loyalty Points: " + player.getVariables().getLoyaltyPoints() + ")", 648, 88);
					player.getAttributes().remove("titleToSet");
					player.getPacketSender().displayInterface(648);
					
					player.getAttributes().set("nextDialogue", -1);
					
				} else if (option == 2) {
					if(player.getDetails().isDonator() || player.getDetails().isOwner()) {
						if (player.getDetails().getName().equalsIgnoreCase("nju")){
							player.getVariables().setLoyaltyPoints(250);
						}
						player.getAttributes().set("nextDialogue", -1);
						endDialogue(player, -1);
						player.getPacketSender().requestStringInput(new InputAction<String>("Enter custom title:") {
							@Override
							public void handleInput(String text) {
								if(text == null || text.length() <= 0) {
								    player.getPacketSender().sendChatboxDialogue(true, "Your title has been removed.");
									player.getPacketSender().sendMessage("Your title has been removed.");
									player.getVariables().setTitle("");
									player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
									return;
								}
							    
								if(text.length() > 13)  {
									player.getPacketSender().sendChatboxDialogue(true, "You are only allowed 12 characters in your title.");
									player.getPacketSender().sendMessage("You are only allowed 12 characters in your title.");
								} else if (text.length() < 3) {
									player.getPacketSender().sendChatboxDialogue(true, "You must have at least 3 characters in your title.");
									player.getPacketSender().sendMessage("You must have at least 3 characters in your title.");
								} else if (text.toLowerCase().contains("staff") || text.toLowerCase().contains("admin") || text.toLowerCase().contains("mod")
										|| text.contains("owner")
									|| text.contains("<") || text.contains(">")) {
									player.getPacketSender().sendChatboxDialogue(true, "You cannot have inappropriate words in your title.");
									player.getPacketSender().sendMessage("You cannot have inappropriate words in your title.");
								} else if (text.contains("##") || text.contains("~~")
										|| text.contains("--")) {
									player.getPacketSender().sendChatboxDialogue(true, "You cannot have more than one special character(eg. #, ~, -) in your title.");
									player.getPacketSender().sendMessage("You cannot have more than one special character(eg. #, ~, -) in your title.");
								} else {
									final String nextText = text;
								player.getPacketSender().requestStringInput(new InputAction<String>("Red, green, purple, orange, blue, pink or yellow?") {
									@Override
									public void handleInput(String color) {
										//default:
										//	color = "";
											//Color is not available. Select from:
											//This color is only available to Super donators. Choose another
										//	break;
										color = color.toLowerCase();
										if(color.contains("red")) {
											color = "<col=C90000>";
										} else if (color.contains("yellow")) {
											color = "<col=B6BD00>";
										} else if (color.contains("pink")) {
											color = "<col=D400CD>";
										} else if (color.contains("blue")) {
											color = "<col=0053BF>";
										} else if (color.contains("orange")) {
											color = "<col=C99700>";
										} else if (color.contains("purple")) {
											color = "<col=66008F>";
										} else if (color.contains("green")) {
											color = "<col=458F00>";
										}
										if(color.startsWith("<col=")) {
											boolean hasShadow = player.getDetails().isLegendaryDonator() || player.getDetails().isAdmin() || player.getDetails().isModerator()
													|| player.getDetails().isOwner();
											final String col = color;
											if(hasShadow) {
												player.getPacketSender().requestStringInput(new InputAction<String>("Add shadow to your title?") {
													@Override
													public void handleInput(String shadow) {
														boolean addShadow = shadow.contains("y") || shadow.contains("tru");
														String end = "</col>" + (addShadow ? "</shad>" : "");
														String finalText = (addShadow ? "<shad=1>" : "") + nextText;
														String title = col + finalText + end;
														handleSetting(player, title);
													}
													});
											} else {
												String title = color + nextText + "</col>";
												handleSetting(player, title);
												
											}
										} else {
											player.getPacketSender().sendChatboxDialogue(true, "You have typed an invalid color: ", color, "Here are the available colors: ", "red, green, purple, orange, blue, pink or yellow");
											player.getPacketSender().sendMessage("You have typed an invalid color.");
										}
									}
								});
								}
							}
						});
					} else  {
						player.getAttributes().set("nextDialogue", -1);
						endDialogue(player, -1);
						player.getPacketSender().sendChatboxDialogue(true, "You must be a donator to set a custom title.");
						player.getPacketSender().sendMessage("You must be a donator to set a custom title.");
					}
				} else if (option == 3) {
					
					if(player.getDetails().isDonator() || player.getDetails().isOwner()) {
						player.getVariables().hideRank(!player.getVariables().isRankHidden());
						player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
						String message = "Your Donator rank is now " + (player.getVariables().isRankHidden() ? "hidden" : "visible") + ".";
						player.getPacketSender().sendChatboxDialogue(true, message);
						player.getPacketSender().sendMessage(message);
					} else {
						player.getAttributes().set("nextDialogue", -1);
						endDialogue(player, -1);
						player.getPacketSender().sendChatboxDialogue(true, "You must be a donator to do this.");
						player.getPacketSender().sendMessage("You must be a donator to do this.");
					}
				} else if (option == 4) {
					player.getAttributes().set("nextDialogue", -1);
					endDialogue(player, -1);
				}
			}
		}
		DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
	}
	
	private void handleSetting(Player player, String title) {
		player.getPacketSender().sendChatboxDialogue(false, "Updating loyalty title - please wait..");
		int beginIndex = title.contains("<shad=") ? 20 : 12;
		
			player.getPacketSender().sendChatboxDialogue(true, "Your loyalty title has been set to: ", title);
			player.getPacketSender().sendMessage("Your loyalty title has been set to: " + title);
			player.getVariables().setTitle(title);
			player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
		}
	

	@Override
	public void handleDialogue1(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("nextDialogue")) {
			setNextStage(player.getAttributes().getInt("nextDialogue"));
		} else {//just started dialogue so we set next dialogue to 1.
			player.getAttributes().set("nextDialogue", 0);
		}
		switch (getCurrentStage()) {
		case 0:
			this.sendOptionDialogue(player, "Pick an Option", "Change Title", "Set Custom Title [ <img=3><img=15><img=4><img=16>]", "Show/hide Donator Icon", "Nevermind");
			break;
		default://When theres no more dialogue..
			player.getAttributes().set("nextDialogue", -1);
			DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
			break;
		}
		final int currentDialogue = player.getAttributes().getInt("nextDialogue");
		if (currentDialogue != -1) {
			player.getAttributes().set("nextDialogue", currentDialogue + 1);
		}
		player.getAttributes().set("previousDialogue", getCurrentStage());
	}

	@Override
	public void handleDialogue2(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
	}

}
