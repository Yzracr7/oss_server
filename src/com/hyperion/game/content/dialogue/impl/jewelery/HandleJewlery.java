package com.hyperion.game.content.dialogue.impl.jewelery;

import com.hyperion.game.Constants;
import com.hyperion.game.content.dialogue.DummyDialogue;
import com.hyperion.game.content.dialogue.impl.jewelery.impl.AmuletOfGlory;
import com.hyperion.game.content.dialogue.impl.jewelery.impl.GamesNecklace;
import com.hyperion.game.content.dialogue.impl.jewelery.impl.RingOfDueling;
import com.hyperion.game.content.dialogue.impl.jewelery.impl.RingOfSlaying;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.world.entity.player.Player;

/**
 * Handle the jewlery item action
 *
 * @author trees
 */
public class HandleJewlery {

    public static boolean operate(Player player, int jwlry, int slot, int itrface) {
        if (Constants.DEBUG_MODE) {
            System.out.println("[Jewlery Operate] [player]" + player + " [jewlery]" + ItemDefinition.forId(jwlry).getName() + " [slot]" + slot);
        }

        if (AmuletOfGlory.isGlory(jwlry)) {
            DummyDialogue dialogue = new DummyDialogue();
            player.getAttributes().set("glory_dialogue", -1);
            player.getAttributes().set("jewelery_using_id", jwlry);
            player.getAttributes().set("jewelery_using_slot", slot);
            player.getAttributes().set("jewelery_using_int", itrface);
            dialogue.sendOptionDialogue(player, "Select an Option", "Grand Exchange", "Great Kourend", "Draynor Village", "Lands End");
            return true;
        } else if (RingOfDueling.isDuelingRing(jwlry)) {
            DummyDialogue dialogue = new DummyDialogue();
            player.getAttributes().set("dueling_ring_dialogue", -1);
            player.getAttributes().set("jewelery_using_id", jwlry);
            player.getAttributes().set("jewelery_using_slot", slot);
            player.getAttributes().set("jewelery_using_int", itrface);
            dialogue.sendOptionDialogue(player, "Select an Option", "Dueling Arena", "Caste Wars");
            return true;
        } else if (RingOfSlaying.isSlayingRing(jwlry)) {
            DummyDialogue dialogue = new DummyDialogue();
            player.getAttributes().set("slaying_ring_dialogue", -1);
            player.getAttributes().set("jewelery_using_id", jwlry);
            player.getAttributes().set("jewelery_using_slot", slot);
            player.getAttributes().set("jewelery_using_int", itrface);
            dialogue.sendOptionDialogue(player, "Select an Option", "Relleka Cave", "Canafis Ghouls", "Brimhaven Beach");
            return true;
        } else if (GamesNecklace.isGames(jwlry)) {
            DummyDialogue dialogue = new DummyDialogue();
            player.getAttributes().set("games_dialogue", -1);
            player.getAttributes().set("jewelery_using_id", jwlry);
            player.getAttributes().set("jewelery_using_slot", slot);
            player.getAttributes().set("jewelery_using_int", itrface);
            dialogue.sendOptionDialogue(player, "Select an Option", "Barbarian Outpost", "Burthorpe", "Corpreal Beast");
            return true;
        }
        return false;
    }
}
