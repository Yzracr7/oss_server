package com.hyperion.game.content.dialogue.impl.jewelery.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
import com.hyperion.utility.NumberUtils;

/**
 * Amulet of glory
 *
 * @author trees
 */
public class AmuletOfGlory {

    public static int[][] GLORY_CONFIG = {
            {1712, 1710}, {1710, 1708}, {1708, 1706}, {1706, 1704}
    };

    public static boolean isGlory(int item) {
        if (item <= 1712 && item >= 1705) {
            return true;
        }
        return false;
    }

    /**
     * Handes the post dialogue operation process
     *
     * @param player
     */
    public static boolean operateJewelery(Player player) {
        if (player.getAttributes().get("jewelery_using_id") != null && player.getAttributes().get("jewelery_using_slot") != null) {

            //Get configs from action button handler
            int dialogue_option = player.getAttributes().get("glory_dialogue");
            int jewel = player.getAttributes().get("jewelery_using_id");
            int slot = player.getAttributes().get("jewelery_using_slot");
            int interf = player.getAttributes().get("jewelery_using_int");
            Location destination = null;
            switch ((dialogue_option)) {
                case 1: // Grand Exchange
                    destination = new Location(3163 + NumberUtils.random(1), 3476 - NumberUtils.random(1), 0);
                    break;
                case 2: // Great Kourend
                    destination = new Location(1657 - NumberUtils.random(1), 3672 + NumberUtils.random(1), 0);
                    break;
                case 3: // Draynor Village
                    destination = new Location(3104 - NumberUtils.random(1), 3248 + NumberUtils.random(1), 0);
                    break;
                case 4: // Lands End
                    destination = new Location(1503 - NumberUtils.random(1), 3415 + NumberUtils.random(1), 0);
                    break;
            }

            //Destination is not null doAction and degrade amulet
            if (destination != null) {
                doAction(player, jewel, slot, interf, destination);
            }
            player.getAttributes().remove("glory_dialogue");
            player.getAttributes().remove("jewelery_using_id");
            player.getAttributes().remove("jewelery_using_slot");
            player.getAttributes().remove("jewelery_using_int");
        }
        return false;
    }

    /**
     * Does the jewelery teleport and action process
     *
     * @param player
     * @param itemId
     * @param slot
     * @param intrface
     * @param dest
     * @return
     */
    public static boolean doAction(Player player, int itemId, int slot, int intrface, Location dest) {
        if (isGlory(itemId)) {
            MainCombat.endCombat(player, 1);
            player.getAttributes().set("stopActions", true);
            player.getPacketSender().clearMapFlag();
            player.animate(714);
            player.playGraphic(1039, 0, 0);
            World.getWorld().submit(new Tickable(player, 4) {
                @Override
                public void execute() {
                    this.stop();
                    player.teleport(dest);
                    player.animate(65535);
                    World.getWorld().submit(new Tickable(player, 2) {
                        @Override
                        public void execute() {
                            this.stop();
                            player.getAttributes().remove("stopActions");
                        }
                    });
                }
            });
            degradeAmulet(player, itemId, slot, intrface == 387);
            return true;
        } else if (itemId == 1704) {
            player.sendMessage("Your amulet has no more charges left!");
            return true;
        }
        return false;
    }

    /**
     * Degrates/Replaces the jewelery
     *
     * @param player
     * @param id
     * @param slot
     * @param neck
     */
    public static void degradeAmulet(Player player, int id, int slot, boolean neck) {

        if (slot < Equipment.EQUIPMENT.values().length) {
            slot = Equipment.EQUIPMENT.AMULET.ordinal();
        }
        String message = (player.getEquipment().getItemInSlot(slot) == 1706 || player.getInventory().getItemInSlot(slot) == 1706) ? "Your amulet has now fully degraded!" : "As you rub the amulet one of the charges is lost!";
        for (int[] data : GLORY_CONFIG) {
            if (data[0] == id) {
                player.getAttributes().remove("stopActions");
                player.animate(716);
                if (neck) {
                    player.getEquipment().replaceItem(data[0], data[1]);
                    player.sendMessage(message);
                    return;
                } else {
                    player.getInventory().deleteItem(id, 1, slot);
                    player.getInventory().addItem(data[1]);
                    player.sendMessage(message);
                    return;
                }
            }
        }
    }
}
