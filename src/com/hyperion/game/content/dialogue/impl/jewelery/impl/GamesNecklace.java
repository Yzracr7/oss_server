package com.hyperion.game.content.dialogue.impl.jewelery.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
import com.hyperion.utility.NumberUtils;

/**
 * Games Necklace
 *
 * @author trees
 */
public class GamesNecklace {

    public static int[][] GAMES_NEKKLACE = {
            {3853, 3855}, {3855, 3857}, {3857, 3859}, {3859, 3861}, {3861, 3863}, {3863, 3865}, {3865, 3867}
    };

    public static boolean isGames(int item) {
        if (item >= 3853 && item <= 3867) {
            return true;
        }
        return false;
    }

    /**
     * Handes the post dialogue operation process
     *
     * @param player
     */
    public static boolean operateJewelery(Player player) {
        if (player.getAttributes().get("jewelery_using_id") != null && player.getAttributes().get("jewelery_using_slot") != null) {

            //Get configs from action button handler
            int dialogue_option = player.getAttributes().get("games_dialogue");
            int jewel = player.getAttributes().get("jewelery_using_id");
            int slot = player.getAttributes().get("jewelery_using_slot");
            int interf = player.getAttributes().get("jewelery_using_int");
            Location destination = null;
            switch ((dialogue_option)) {
                case 1: // Barbarian Outpost
                    destination = new Location(2519 + NumberUtils.random(1), 3573 - NumberUtils.random(1), 0);
                    break;
                case 2: // Burthorpe
                    destination = new Location(2898 + NumberUtils.random(1), 3546 - NumberUtils.random(1), 0);
                    break;
                case 3: // Corpreal Beast
                    destination = new Location(2966, 4383, 2);
                    break;
            }

            //Destination is not null doAction and degrade amulet
            if (destination != null) {
                doAction(player, jewel, slot, interf, destination);
            }
            player.getAttributes().remove("games_dialogue");
            player.getAttributes().remove("jewelery_using_id");
            player.getAttributes().remove("jewelery_using_slot");
            player.getAttributes().remove("jewelery_using_int");
        }
        return false;
    }

    /**
     * Does the jewelery teleport and action process
     *
     * @param player
     * @param itemId
     * @param slot
     * @param intrface
     * @param dest
     * @return
     */
    public static boolean doAction(Player player, int itemId, int slot, int intrface, Location dest) {
        if (isGames(itemId)) {
            MainCombat.endCombat(player, 1);
            player.getAttributes().set("stopActions", true);
            player.getPacketSender().clearMapFlag();
            player.animate(714);
            player.playGraphic(1039, 0, 0);
            World.getWorld().submit(new Tickable(player, 4) {
                @Override
                public void execute() {
                    this.stop();
                    player.teleport(dest);
                    player.animate(65535);
                    World.getWorld().submit(new Tickable(player, 2) {
                        @Override
                        public void execute() {
                            this.stop();
                            player.getAttributes().remove("stopActions");
                        }
                    });
                }
            });
            degradeAmulet(player, itemId, slot, intrface == 387);
            return true;
        } else if (itemId == 1704) {
            player.sendMessage("Your amulet has no more charges left!");
            return true;
        }
        return false;
    }

    /**
     * Degrates/Replaces the jewelery
     *
     * @param player
     * @param id
     * @param slot
     * @param neck
     */
    public static void degradeAmulet(Player player, int id, int slot, boolean neck) {

        if (slot < Equipment.EQUIPMENT.values().length) {
            slot = Equipment.EQUIPMENT.AMULET.ordinal();
        }
        String message = (player.getEquipment().getItemInSlot(slot) == 1706 || player.getInventory().getItemInSlot(slot) == 1706) ? "Your amulet has now fully degraded!" : "As you rub the amulet one of the charges is lost!";
        for (int[] data : GAMES_NEKKLACE) {
            if (data[0] == id) {
                player.getAttributes().remove("stopActions");
                player.animate(716);
                if (neck) {
                    player.getEquipment().replaceItem(data[0], data[1]);
                    player.sendMessage(message);
                    return;
                } else {
                    player.getInventory().deleteItem(id, 1, slot);
                    player.getInventory().addItem(data[1]);
                    player.sendMessage(message);
                    return;
                }
            }
        }
    }
}
