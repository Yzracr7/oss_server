package com.hyperion.game.content.dialogue.impl.jewelery.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
import com.hyperion.utility.NumberUtils;

/**
 * Ring Of Dueling
 *
 * @author trees
 */
public class RingOfDueling {

    public static int[][] DUELING_RING_CONFIG = {
            {2552, 2554}, {2554, 2556}, {2556, 2558}, {2558, 2560}, {2562, 2564}, {2564, 2566}
    };

    public static boolean isDuelingRing(int item) {
        if (item >= 2552 && item <= 2566) {
            return true;
        }
        return false;
    }

    /**
     * Handes the post dialogue operation process
     *
     * @param player
     */
    public static void operateJewelery(Player player) {
        if (player.getAttributes().get("jewelery_using_id") != null && player.getAttributes().get("jewelery_using_slot") != null) {

            //Get configs from action button handler
            int dialogue_option = player.getAttributes().get("dueling_ring_dialogue");
            int jewel = player.getAttributes().get("jewelery_using_id");
            int slot = player.getAttributes().get("jewelery_using_slot");
            int interf = player.getAttributes().get("jewelery_using_int");
            Location destination = null;
            switch ((dialogue_option)) {
                case 1: // Dueling Arena
                    destination = new Location(3367 + NumberUtils.random(1), 3267 - NumberUtils.random(1), 0);
                    break;
                case 2: // Castle Wars
                    destination = new Location(2456 - NumberUtils.random(1), 3088 + NumberUtils.random(1), 0);
                    break;
            }

            //Destination is not null doAction and degrade amulet
            if (destination != null) {
                doAction(player, jewel, slot, interf, destination);
            }
            player.getAttributes().remove("dueling_ring_dialogue");
            player.getAttributes().remove("jewelery_using_id");
            player.getAttributes().remove("jewelery_using_slot");
            player.getAttributes().remove("jewelery_using_int");
        }
    }

    /**
     * Does the jewelery teleport and action process
     *
     * @param player
     * @param itemId
     * @param slot
     * @param intrface
     * @param dest
     * @return
     */
    public static boolean doAction(Player player, int itemId, int slot, int intrface, Location dest) {
        if (isDuelingRing(itemId)) {
            MainCombat.endCombat(player, 1);
            player.getAttributes().set("stopActions", true);
            player.getPacketSender().clearMapFlag();
            player.animate(714);
            player.playGraphic(1039, 0, 0);
            World.getWorld().submit(new Tickable(player, 4) {
                @Override
                public void execute() {
                    this.stop();
                    player.teleport(dest);
                    player.animate(65535);
                    World.getWorld().submit(new Tickable(player, 2) {
                        @Override
                        public void execute() {
                            this.stop();
                            player.getAttributes().remove("stopActions");
                        }
                    });
                }
            });
            degradeAmulet(player, itemId, slot, intrface == 387);
            return true;
        } else if (itemId == 1704) {
            player.sendMessage("Your amulet has no more charges left!");
            return true;
        }
        return false;
    }

    /**
     * Degrates/Replaces the jewelery
     *
     * @param player
     * @param id
     * @param slot
     * @param finger
     */
    public static void degradeAmulet(Player player, int id, int slot, boolean finger) {
        int last_use = 2566;
        if (slot < Equipment.EQUIPMENT.values().length) {
            slot = Equipment.EQUIPMENT.RING.ordinal();
        }
        String message = "As you rub the amulet one of the charges is lost!";
        for (int[] data : DUELING_RING_CONFIG) {
            if (data[0] == id) {
                player.getAttributes().remove("stopActions");
                player.animate(716);
                if (finger) {
                    if (id == last_use) {
                        player.getEquipment().deleteItem(data[0]);
                    } else {
                        player.getEquipment().replaceItem(data[0], data[1]);
                    }
                    player.sendMessage(message);
                    return;
                } else {
                    player.getInventory().deleteItem(data[0], 1, slot);
                    if (id == last_use) {
                        player.sendMessage("Your ring has turned into ashes.");
                        return;
                    }
                    player.getInventory().addItem(data[1]);
                    player.sendMessage(message);
                    return;
                }
            }
        }
    }
}
