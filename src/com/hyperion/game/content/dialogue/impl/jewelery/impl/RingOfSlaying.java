package com.hyperion.game.content.dialogue.impl.jewelery.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.impl.Equipment;

/**
 * Ring Of Slaying
 *
 * @author trees
 */
public class RingOfSlaying {

    public static int[][] SLAYING_RING_CONFIG = {
            {11866, 11867}, {11867, 11868}, {11868, 11869}, {11870, 11871}, {11871, 11872}, {11872, 11873}
    };

    public static boolean isSlayingRing(int item) {
        if (item >= 11866 && item <= 11873) {
            return true;
        }
        return false;
    }

    /**
     * Handes the post dialogue operation process
     *
     * @param player
     */
    public static void operateJewelery(Player player) {
        if (player.getAttributes().get("jewelery_using_id") != null && player.getAttributes().get("jewelery_using_slot") != null) {

            //Get configs from action button handler
            int dialogue_option = player.getAttributes().get("slaying_ring_dialogue");
            int jewel = player.getAttributes().get("jewelery_using_id");
            int slot = player.getAttributes().get("jewelery_using_slot");
            int interf = player.getAttributes().get("jewelery_using_int");
            Location destination = null;
            switch ((dialogue_option)) {
                default:
                    destination = player.getLocation();
                    player.sendMessage("dialogue option - " + dialogue_option);
                    break;
            }

            //Destination is not null doAction and degrade amulet
            if (destination != null) {
                System.out.println("Do Action " + interf);
                doAction(player, jewel, slot, interf, destination);
            }
            player.getAttributes().remove("slaying_ring_dialogue");
            player.getAttributes().remove("jewelery_using_id");
            player.getAttributes().remove("jewelery_using_slot");
            player.getAttributes().remove("jewelery_using_int");
        }
    }

    /**
     * Does the jewelery teleport and action process
     *
     * @param player
     * @param itemId
     * @param slot
     * @param intrface
     * @param dest
     * @return
     */
    public static boolean doAction(Player player, int itemId, int slot, int intrface, Location dest) {
        if (isSlayingRing(itemId)) {
            MainCombat.endCombat(player, 1);
            player.getAttributes().set("stopActions", true);
            player.getPacketSender().clearMapFlag();
            player.animate(714);
            player.playGraphic(1039, 0, 0);
            World.getWorld().submit(new Tickable(player, 4) {
                @Override
                public void execute() {
                    this.stop();
                    player.teleport(dest);
                    player.animate(65535);
                    World.getWorld().submit(new Tickable(player, 2) {
                        @Override
                        public void execute() {
                            this.stop();
                            player.getAttributes().remove("stopActions");
                        }
                    });
                }
            });
            degradeAmulet(player, itemId, slot, intrface == 387);
            return true;
        } else if (itemId == 1704) {
            player.sendMessage("Your amulet has no more charges left!");
            return true;
        }
        return false;
    }

    /**
     * Degrates/Replaces the jewelery
     *
     * @param player
     * @param id
     * @param slot
     * @param neck
     */
    public static void degradeAmulet(Player player, int id, int slot, boolean neck) {
        boolean last_use = (id == 11873);
        if (neck) {
            if (slot < Equipment.EQUIPMENT.values().length) {
                slot = Equipment.EQUIPMENT.RING.ordinal();
            }
        }
        String message = "As you rub the amulet one of the charges is lost!";
        for (int[] data : SLAYING_RING_CONFIG) {
            if (data[0] == id) {
                player.getAttributes().remove("stopActions");
                player.animate(716);
                if (neck) {
                    player.getEquipment().replaceItem(data[0], data[1]);
                    player.sendMessage(message);
                    return;
                } else {
                    player.getInventory().deleteItem(id, 1, slot);
                    if (!last_use) {
                        player.getInventory().addItem(data[1]);
                    }
                    player.sendMessage(message);
                    return;
                }
            }
        }
    }
}
