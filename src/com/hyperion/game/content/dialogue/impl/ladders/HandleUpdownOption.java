package com.hyperion.game.content.dialogue.impl.ladders;

import com.hyperion.game.content.dialogue.DummyDialogue;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;

/**
 * Handle the up or down option for a ladder
 *
 * @author trees
 */
public class HandleUpdownOption {

    // Does Object Use This Dialogue
    public static boolean usesThisDialogue(GameObject var0) {
        if (var0.getId() == 16672 || var0.getId() == 16684) {
            return true;
        }
        return false;
    }

    // Send The Dialogue
    public static boolean operate(Player player, GameObject object) {

        // Stair message check
        boolean stairs = object.getName() != null && object.getName().contains("stairs");
        if (object.getName() == null) {
            stairs = false;
        }

        // Set attributes and sendTeleportTitles dialogue
        if (usesThisDialogue(object)) {
            DummyDialogue dialogue = new DummyDialogue();
            player.getAttributes().set("ladder_up_down", -1);
            player.getAttributes().set("ladder_up_down_obj", object);
            dialogue.sendOptionDialogue(player, "Climb up or down" + (stairs ? " the stairs?" : "."), "Climb up" + (stairs ? " the stairs." : "."), "Climb Down" + (stairs ? " the stairs." : "."));
            return true;
        }
        return false;
    }
}
