package com.hyperion.game.content.dialogue.impl.ladders;

import com.hyperion.game.content.Ladders;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.utility.NumberUtils;

/**
 * Ladder Dialogue Option
 *
 * @author trees
 */
public class LadderOption {

    // handle option process
    public static void handleOptionSelection(Player player) {
        if (player.getAttributes().get("ladder_up_down") != null && player.getAttributes().get("ladder_up_down_obj") != null) {

            //Get configs from action button handler
            int dialogue_option = player.getAttributes().get("ladder_up_down");
            GameObject obj = player.getAttributes().get("ladder_up_down_obj");
            Location destination = getLocationForObject(player, obj, dialogue_option);

            //Destination is not null doAction and degrade amulet
            if (destination != null) {
                doAction(player, destination, dialogue_option);
            }
            player.getAttributes().remove("ladder_up_down");
            player.getAttributes().remove("ladder_up_down_obj");
        }
    }

    // Initiate the ladder action (do checks here)
    public static void doAction(Player plr, Location loc, int option) {
        Ladders.executeLadder(plr, option == 2, loc.getX(), loc.getY(), loc.getZ());
    }

    // Returns a location based on dialogue option and object location
    public static Location getLocationForObject(Player plr, GameObject object, int option) {
        if (object.getLocation().isWithinDistance(new Location(3204, 3207, 1))) {
            switch (option) {
                case 1:
                    return new Location(3205, 3209, 2);
                case 2:
                    return new Location(3206, 3209, 0);
            }
        } else if (object.getLocation().isWithinDistance(new Location(3205, 3229, 1))) {
            switch (option) {
                case 1:
                    return new Location(3206, 3229, 2);
                case 2:
                    return new Location(3205, 3228, 0);
            }
        } else if (object.getLocation().isWithinDistance(new Location(3229, 3213, 1)) && plr.getLocation().getY() < 3220) {
            switch (option) {
                case 1:
                    return new Location(3229, 3214, 2);
                case 2:
                    return new Location(3229, 3214, 0);
            }
        } else if (object.getLocation().isWithinDistance(new Location(3229, 3224, 1)) && plr.getLocation().getY() > 3220) {
            switch (option) {
                case 1:
                    return new Location(3229, 3223, 2);
                case 2:
                    return new Location(3229, 3223, 0);
            }
        }
        return null;
    }
}
