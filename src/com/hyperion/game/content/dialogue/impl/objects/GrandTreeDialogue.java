package com.hyperion.game.content.dialogue.impl.objects;

import com.hyperion.game.content.dialogue.DummyDialogue;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;

public class GrandTreeDialogue {

    // Does Object Use This Dialogue
    public static boolean usesThisDialogue(GameObject var0) {
        if (var0.getId() == 1295) {
            return true;
        }
        return false;
    }

    // Send The Dialogue
    public static boolean operate(Player player, GameObject object) {

        // Set attributes and sendTeleportTitles dialogue
        if (usesThisDialogue(object)) {
            DummyDialogue dialogue = new DummyDialogue();
            player.getAttributes().set("grand_tree_dialogue", -1);
            dialogue.sendOptionDialogue(player, "Pick a destination", "Gnome Stronghold", "Feldip Hills", "to be added");
            return true;
        }
        return false;
    }
}

