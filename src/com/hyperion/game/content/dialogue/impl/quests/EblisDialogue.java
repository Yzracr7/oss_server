package com.hyperion.game.content.dialogue.impl.quests;

import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.content.miniquests.impl.DesertTreasure;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.shopping.ShopManager;

public class EblisDialogue extends Dialogue {

	@Override
	public void endDialogue(Player player, int lastStage) {
		// TODO Auto-generated method stub
		player.getInterfaceSettings().closeInterfaces(true);
	}

	@Override
	public void handleOptions(final Player player, int option) {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("previousDialogue")) {
			if (player.getAttributes().getInt("previousDialogue") == 0) {
				if (option == 1) {//shop
					player.getInterfaceSettings().closeInterfaces(false);
					if (player.getVariables().isDtFinished()) {
						ShopManager.getShopManager().openShop(player, 20);
					} else {
						player.getPacketSender().sendMessage("You can't view this shop yet.");
						endDialogue(player, -1);
					}
				} else if (option == 2) {
					player.getAttributes().set("nextDialogue", -1);
					if (player.getVariables().getMode() == 1 || player.getVariables().isDtFinished()) {
						if (player.getSettings().getMagicType() != 1) {
							player.getPacketSender().sendTab(92, 665);
							player.getSettings().setMagicType(1);
						} else {
							player.getSettings().setMagicType(2);
							player.getPacketSender().sendTab(92, 193);
						}
					} else {
						player.getPacketSender().sendMessage("You must finish Desert treasure to access this.");
						endDialogue(player, -1);
					}
				} else if (option == 3) {
					if (!player.getVariables().isDtFinished()) {
						player.getAttributes().set("nextDialogue", 1);
					} else {
						player.getAttributes().set("nextDialogue", -1);
					}
				}
			} else if (player.getAttributes().getInt("previousDialogue") == 1) {
				if (option == 1) {
					if (!player.getVariables().isDessousKilled()) {
						player.teleport(3570, 3406, 0);
						World.getWorld().submit(new Tickable(2) {
							@Override
							public void execute() {
								this.stop();
								DesertTreasure.enterDessous(player);
							}
						});
					}
				} else if (option == 2) {
					if (!player.getVariables().isKamilKilled()) {
						player.teleport(2850, 3810, 2);
						World.getWorld().submit(new Tickable(2) {
							@Override
							public void execute() {
								this.stop();
								DesertTreasure.enterKamil(player);
							}
						});
					}
				} else if (option == 3) {
					if (!player.getVariables().isFareedKilled()) {
						player.teleport(3306, 9375, 0);
						World.getWorld().submit(new Tickable(2) {
							@Override
							public void execute() {
								this.stop();
								DesertTreasure.enterFareed(player);
							}
						});
					}
				} else if (option == 4) {
					if (!player.getVariables().isDamisKilled()) {
						player.teleport(2740, 5104, 0);
						World.getWorld().submit(new Tickable(2) {
							@Override
							public void execute() {
								this.stop();
								DesertTreasure.enterDamis(player);
							}
						});
					}
				}
				player.getAttributes().set("nextDialogue", -1);
			}
		}
		DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
	}

	@Override
	public void handleDialogue1(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("nextDialogue")) {
			setNextStage(player.getAttributes().getInt("nextDialogue"));
		} else {//just started dialogue so we set next dialogue to 1.
	//		player.getAttributes().set("nextDialogue", 0);
		}
		switch (getCurrentStage()) {
		case 0:
			this.sendOptionDialogue(player, "Select an Option", "Shop", "Switch spellbook", "Fight monsters");
			break;
		case 1:
			this.sendOptionDialogue(player, "Select an Option", "Dessous", "Kamil", "Fareed", "Damis");
			break;
		}
		player.getAttributes().set("previousDialogue", getCurrentStage());
	}

	@Override
	public void handleDialogue2(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
