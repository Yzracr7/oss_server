package com.hyperion.game.content.dialogue.impl.quests;

import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.content.miniquests.impl.RecipeForDisaster;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;

public class GypsyDialogue extends Dialogue {

	@Override
	public void endDialogue(Player player, int lastStage) {
		// TODO Auto-generated method stub
		player.getInterfaceSettings().closeInterfaces(true);
	}

	@Override
	public void handleOptions(Player player, int option) {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("previousDialogue")) {
			if (player.getAttributes().getInt("previousDialogue") == 2) {
				if (option == 1) {
					player.getAttributes().set("nextDialogue", 3);
				} else if (option == 2) {
					player.getAttributes().set("nextDialogue", -1);
				}
			}
		}
		DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
	}

	@Override
	public void handleDialogue1(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("nextDialogue")) {
			setNextStage(player.getAttributes().getInt("nextDialogue"));
		} else {// just started dialogue so we set next dialogue to 1.
			// player.getAttributes().set("nextDialogue", 0);
		}
		switch (getCurrentStage()) {
		case 0:
			if (player.getVariables().finishedRfd) {
				this.sendNPCDialogue(player, npc, FacialAnimation.HAPPY, "Did you know? You will lose your gloves", "if you should ever die in a PvP area.");
				player.getAttributes().set("nextDialogue", -1);
				break;
			}
			this.sendPlayerDialogue(player, FacialAnimation.HAPPY, "What are you doing here?");
			player.getAttributes().set("nextDialogue", 1);
			break;
		case 1:
			this.sendNPCDialogue(player, npc, FacialAnimation.HAPPY, "I have sets of powerful gloves. You can", "unlock different pairs if you participate", "in my game. Are you interested?");
			player.getAttributes().set("nextDialogue", 2);
			break;
		case 2:
			this.sendOptionDialogue(player, "Select an Option", "Yes", "No");
			break;
		case 3:
			this.sendPlayerDialogue(player, FacialAnimation.HAPPY, "Yes, I'll play your game.");
			player.getAttributes().set("nextDialogue", 4);
			break;
		case 4:
			if (player.getSkills().getCombatLevel() < 60 || player.getSkills().getLevel(6) < 59 || player.getSkills().getLevel(Skills.THIEVING) < 53 || player.getSkills().getLevel(Skills.COOKING) < 70 || player.getSkills().getLevel(Skills.HERBLORE) < 45 || player.getVariables().getQuestPoints() < 2) {
				this.sendNPCDialogue(player, npc, FacialAnimation.HAPPY, "You don't meet the requirements to enterInstancedRoom,", "check your quest journal and come back again when you do.");
				player.getAttributes().set("nextDialogue", -1);
			} else {
				this.sendNPCDialogue(player, npc, FacialAnimation.EVIL, "Very well then, I will teleport you into the arena.");
				player.getAttributes().set("nextDialogue", 5);
			}
		case 5:
			RecipeForDisaster game_instance = new RecipeForDisaster();
			game_instance.enter(player);
			player.getVariables().setCurrentRoundMiniquest(game_instance);
			player.getVariables().setSessionName("recipefordisaster");
			this.endDialogue(player, -1);
			break;
		}
		player.getAttributes().set("previousDialogue", getCurrentStage());
	}

	@Override
	public void handleDialogue2(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub

	}

}
