package com.hyperion.game.content.dialogue.impl.quests;

import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.content.miniquests.impl.MountainDaughter;
import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

public class HamalChiefDialogue extends Dialogue {
	
	@Override
	public void endDialogue(Player player, int lastStage) {
		// TODO Auto-generated method stub
		player.getInterfaceSettings().closeInterfaces(true);
	}

	@Override
	public void handleOptions(Player player, int option) {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("previousDialogue")) {
			if (player.getAttributes().getInt("previousDialogue") == 5) {
				if (option == 1) {
					player.getAttributes().set("nextDialogue", 6);
				} else if (option == 2) {
					player.getAttributes().set("nextDialogue", 7);
				}
			}
		}
		DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
	}

	@Override
	public void handleDialogue1(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("nextDialogue")) {
			setNextStage(player.getAttributes().getInt("nextDialogue"));
		} else {//just started dialogue so we set next dialogue to 1.
	//		player.getAttributes().set("nextDialogue", 0);
		}
		switch (getCurrentStage()) {
		case 0:
			if (player.getVariables().isKendal_killed()) {
				this.sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "Thanks again for your bravery "+player.getName()+"..");
			} else {
				this.sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "Hello outlander..");
			}
			player.getAttributes().set("nextDialogue", 1);
			break;
		case 1:
			if (player.getVariables().isKendal_killed()) {
				this.sendPlayerDialogue(player, FacialAnimation.DEFAULT, "Not a problem, Would you happen to have", "the kendals head? I'd like one as a reminder.");
			} else {
				this.sendPlayerDialogue(player, FacialAnimation.DEFAULT, "What's wrong?");
			}
			player.getAttributes().set("nextDialogue", 2);
			break;
		case 2:
			if (player.getVariables().isKendal_killed()) {
				this.sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "Of course! here you go.");
			} else {
				this.sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "My daughter was killed by an evil Monster.", "It must be slain!");
			}
			player.getAttributes().set("nextDialogue", 3);
			break;
		case 3:
			if (player.getVariables().isKendal_killed()) {
				boolean hasHelm = player.getInventory().hasItem(4502) || player.getBank().hasItem(4502);
				if (hasHelm) {
					player.getPacketSender().sendMessage("You already have the kendals head!");
					endDialogue(player, -1);
					break;
				}
				player.getInventory().addItem(new Item(4502, 1));
				player.getInventory().refresh();
				endDialogue(player, -1);
				break;
			}
			this.sendPlayerDialogue(player, FacialAnimation.DEFAULT, "Is there anything I can do to help?");
			player.getAttributes().set("nextDialogue", 4);
			break;
		case 4:
			this.sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "Well..the best of my men were defeated by", "the monster. Would you be willing to fight on our behalf?");
			player.getAttributes().set("nextDialogue", 5);
			break;
		case 5:
			this.sendOptionDialogue(player, "Pick an Option", "Yes, I'll slay the monster.", "Haha, no way!");
			break;
		case 6://Yes
			this.sendPlayerDialogue(player, FacialAnimation.DEFAULT, "Yes, I'll slay the monster.");
			player.getAttributes().set("nextDialogue", 8);
			break;
		case 7://No
			this.sendPlayerDialogue(player, FacialAnimation.DEFAULT, "Haha, no way!");
			player.getAttributes().set("nextDialogue", -1);
			break;
		case 8:
			MountainDaughter.enterGame(player);
			endDialogue(player, -1);
			player.getAttributes().set("nextDialogue", -1);
			break;
		}
		player.getAttributes().set("previousDialogue", getCurrentStage());
	}

	@Override
	public void handleDialogue2(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
