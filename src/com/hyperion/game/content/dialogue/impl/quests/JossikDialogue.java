package com.hyperion.game.content.dialogue.impl.quests;

import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.content.miniquests.impl.HorrorFromTheDeep;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;

public class JossikDialogue extends Dialogue {

	@Override
	public void endDialogue(Player player, int lastStage) {
		// TODO Auto-generated method stub
		player.getInterfaceSettings().closeInterfaces(true);
	}

	@Override
	public void handleOptions(Player player, int option) {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("previousDialogue")) {
			if (player.getAttributes().getInt("previousDialogue") == 2) {
				if (option == 1) {
					player.getAttributes().set("nextDialogue", 3);
				} else if (option == 2) {
					player.getAttributes().set("nextDialogue", 4);
				}
			}
		}
		DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
	}

	@Override
	public void handleDialogue1(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("nextDialogue")) {
			setNextStage(player.getAttributes().getInt("nextDialogue"));
		} else {//just started dialogue so we set next dialogue to 1.
	//		player.getAttributes().set("nextDialogue", 0);
		}
		switch (getCurrentStage()) {
		case 0:
			if (player.getVariables().isMother_killed()) {
				this.sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "Great job in there, trade me to buy God books.");
				player.getAttributes().set("nextDialogue", -1);
				break;
			} else {
				this.sendPlayerDialogue(player, FacialAnimation.DEFAULT, "What are you doing here?");
			}
			player.getAttributes().set("nextDialogue", 1);
			break;
		case 1:
			this.sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "I'm teleporting warriors to fight the Dagannoth", "mother. Do you want to go?");
			player.getAttributes().set("nextDialogue", 2);
			break;
		case 2:
			this.sendOptionDialogue(player, "Pick an Option", "Yes, let's go.", "No thanks.");
			break;
		case 3://Yes
			this.sendPlayerDialogue(player, FacialAnimation.DEFAULT, "Yes, let's go.");
			player.getAttributes().set("nextDialogue", 5);
			break;
		case 4://no
			this.sendPlayerDialogue(player, FacialAnimation.DEFAULT, "No thanks.");
			player.getAttributes().set("nextDialogue", -1);
			break;
		case 5:
			if (player.getSkills().getLevel(Skills.ATTACK) < 35) {
				this.sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "You don't meet the requirements to enterInstancedRoom,", "come back when you have at least 35 Agility.");
				player.getAttributes().set("nextDialogue", -1);
			} else {
				this.sendNPCDialogue(player, npc, FacialAnimation.EVIL, "I will take you in now.");
				player.getAttributes().set("nextDialogue", 6);
			}
		case 6:
			HorrorFromTheDeep.enterGame(player);
			endDialogue(player, -1);
			break;
		}
		player.getAttributes().set("previousDialogue", getCurrentStage());
	}

	@Override
	public void handleDialogue2(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
