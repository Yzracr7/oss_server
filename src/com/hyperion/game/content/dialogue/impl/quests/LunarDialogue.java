package com.hyperion.game.content.dialogue.impl.quests;

import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.content.miniquests.impl.LunarDiplomacy;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;

public class LunarDialogue extends Dialogue {

	@Override
	public void endDialogue(Player player, int lastStage) {
		// TODO Auto-generated method stub
		player.getInterfaceSettings().closeInterfaces(true);
	}

	@Override
	public void handleOptions(Player player, int option) {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("previousDialogue")) {
			if (player.getAttributes().getInt("previousDialogue") == 0) {
				if (option == 1) {
					player.getAttributes().set("nextDialogue", 1);
				} else if (option == 2) {
					player.getAttributes().set("nextDialogue", 2);
				} else if (option == 3) {
					player.getAttributes().set("nextDialogue", 3);
				}
			}
		}
		DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
	}

	@Override
	public void handleDialogue1(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("nextDialogue")) {
			setNextStage(player.getAttributes().getInt("nextDialogue"));
		} else {//just started dialogue so we set next dialogue to 1.
			//player.getAttributes().set("nextDialogue", 0);
		}
		switch (getCurrentStage()) {
		case 0:
			if (!player.getVariables().finishedLunars) {
				this.sendOptionDialogue(player, "Pick an Option", "Shop", "Switch spellbook", "Begin Battle");
			} else if (player.getVariables().finishedLunars) {
				this.sendOptionDialogue(player, "Pick an Option", "Shop", "Switch spellbook");
			}
			break;
		case 1:
			this.endDialogue(player, -1);
			//TODO - shop
			break;
		case 2:
			if (player.getVariables().getMode() == 1 || player.getVariables().finishedLunars) {
				if (player.getSettings().getMagicType() != 3) {
					player.getPacketSender().sendTab(92, 430);
					player.getSettings().setMagicType(3);
				} else {
					player.getSettings().setMagicType(2);
					player.getPacketSender().sendTab(92, 193);
				}
			} else {
				player.getPacketSender().sendMessage("You must complete the Lunar Diplomacy Quest to use this.");
			}
			this.endDialogue(player, -1);
			break;
		case 3:
			if (player.getSkills().getCombatLevel() < 60 || player.getSkills().getLevel(6) < 65 || player.getSkills().getLevel(Skills.DEFENCE) < 40) {
				this.sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "You don't meet the requirements to enterInstancedRoom,", "check your quest journal and come back again when you do.");
				player.getAttributes().set("nextDialogue", -1);
			} else {
				this.sendNPCDialogue(player, npc, FacialAnimation.EVIL, "Very well then, I will teleport you into the arena.");
				player.getAttributes().set("nextDialogue", 4);
			}
		case 4:
			//LunarDiplomacy.enterGame(player);
			LunarDiplomacy game_instance = new LunarDiplomacy();
			game_instance.enter(player);
			player.getVariables().setCurrentRoundMiniquest(game_instance);
			player.getVariables().setSessionName("lunardiplomacy");
			this.endDialogue(player, -1);
			break;
		}
		player.getAttributes().set("previousDialogue", getCurrentStage());
	}

	@Override
	public void handleDialogue2(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
