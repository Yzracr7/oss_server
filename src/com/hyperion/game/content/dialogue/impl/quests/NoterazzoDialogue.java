package com.hyperion.game.content.dialogue.impl.quests;

import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.content.dialogue.Dialogue;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

public class NoterazzoDialogue extends Dialogue {

	@Override
	public void endDialogue(Player player, int lastStage) {
		// TODO Auto-generated method stub
		player.getInterfaceSettings().closeInterfaces(true);
	}
	@Override
	public void handleOptions(Player player, int option) {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("previousDialogue")) {
			if (player.getAttributes().getInt("previousDialogue") == 0) {
				if (option == 1) {
					player.getAttributes().set("nextDialogue", 1);
				} else if (option == 2) {
					player.getAttributes().set("nextDialogue", 2);
				} else if (option == 3) {
					player.getAttributes().set("nextDialogue", 3);
				}
			}
		}
		DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
	}

	@Override
	public void handleDialogue1(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		if (player.getAttributes().isSet("nextDialogue")) {
			setNextStage(player.getAttributes().getInt("nextDialogue"));
		} else {//just started dialogue so we set next dialogue to 1.
			//player.getAttributes().set("nextDialogue", 0);
		}
		switch (getCurrentStage()) {
		case 0:
			if (!player.getVariables().isTasFinished) {
				this.sendOptionDialogue(player, "Pick an Option", "Switch prayerbook", "Quest");
			} else if (player.getVariables().isTasFinished) {
				this.sendOptionDialogue(player, "Pick an Option", "Switch prayerbook");																			
			}
			break;
		case 1:
			this.endDialogue(player, -1);
			//TODO - prayerbook
			break;
		case 2:
			if (player.getSkills().getLevel(5) <= 49 || player.getVariables().isDtFinished() || Achievements.getTotalAchieved(player) == 0) {
				this.sendNPCDialogue(player, npc, FacialAnimation.DISTRESSED, "You don't meet the requirements to speak,", "check your quest journal and come back again when you do.");
				player.getAttributes().set("nextDialogue", -1);
			} else {
				this.sendNPCDialogue(player, npc, FacialAnimation.DEFAULT, "Pss'd, hey you!", "You seem to be a strapping young" + (player.getAppearance().getGender() == 0 ? "lad" : "woman") + "that could help me out.");
				player.getAttributes().set("nextDialogue", 3);
			}
			break;
		case 3:
			this.sendPlayerDialogue(player, FacialAnimation.DISINTERESTED, "Okay...", "What can I do you for?");
			player.getAttributes().set("nextDialogue", 4);
		case 4:
			this.sendNPCDialogue(player, npc, FacialAnimation.DELIGHTED_EVIL, "Those barrows brothers are hiding something,", "Something very RARE indeed.", "I need you to do he right thing and get me RARE icon I seek", "It will be held by one of them.");
			player.getAttributes().set("nextDialogue", 5);
			break;
		case 5:
			this.sendPlayerDialogue(player, FacialAnimation.DEFAULT, "I'll see what I can do.");
			player.getVariables().getIcon = 1;
			player.getAttributes().set("nextDialogue", -1);
			break;
		case 6:
			if (player.getInventory().hasItem(15378)){
				this.sendPlayerDialogue(player, FacialAnimation.HAPPY, "I got the icon!");
				player.getAttributes().set("nextDialogue", 7);
			}else{
				player.getAttributes().set("nextDialogue", 8);
			}
			break;
		case 7:
			this.sendNPCDialogue(player, npc, FacialAnimation.EVIL_LAUGH, "At last I can finally fix the temple of senntisten,", "For the cause I will give you", "Some special powers you can use anywhere.");
			player.getAttributes().set("nextDialogue", -1);
			endDialogue(player, -1);
			break;
		case 8:
			this.sendNPCDialogue(player, npc, FacialAnimation.BOWS_HEAD_WHILE_SAD, "I see you don't have the icon, please come back when you have it.");
			break;
		}
		player.getAttributes().set("previousDialogue", getCurrentStage());
	}			
	@Override
	public void handleDialogue2(Player player, NPC npc) throws Exception {
		// TODO Auto-generated method stub
		
	}
}