package com.hyperion.game.content.godwars;

import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

/**
 * This class handles all godwars monsters
 * 
 * @author Nando
 * @author Tyluur<itstyluur@gmail.com>
 */
public class Godwars {

	public enum ChamberTypes {

		ARMADYL(new int[] { -1, -1, -1, -1 }) {
			@Override
			public int[] getKillcountNpcs() {
				return new int[] { 6246, 6233, 6257, 6256, 6255, 6229, 6230, 6231, 6232, 6234, 6235, 6236, 6237, 6238, 6239, 6240, 6241, 6242, 6243, 6246, 6244, 6221, 6276, 6277, 6219 };
			}

			@Override
			public boolean isInRegion(Player player) {
				return player.getLocation().getRegionId() == 11602 || player.getLocation().getRegionId() == 11346;
			}

			@Override
			public boolean isChamberNPC(int id) {
				return id >= 6228 && id <= 6246;
			}
		},
		BANDOS(new int[] { 6260, 6263, 6265, 6261 }) {
			@Override
			public int[] getKillcountNpcs() {
				return new int[] { 6269, 6210, 6212, 6257, 6256, 6255, 6229, 6230, 6231, 6268, 6274, 6283, 6282, 6275, 6221, 6273, 6276, 6272, 6213, 6277, 6219 };
			}

			@Override
			public boolean isInRegion(Player player) {
				return player.getLocation().getRegionId() == 11347;
			}

			@Override
			public boolean isChamberNPC(int id) {
				return id >= 6268 && id <= 6283;
			}
		},
		SARADOMIN(new int[] { -1, -1, -1, -1 }) {
			@Override
			public int[] getKillcountNpcs() {
				return new int[] { 6257, 6256, 6259, 6258, 6254, 6255, 6229, 6230, 6231, 6221, 6276, 6277, 6219 };
			}

			@Override
			public boolean isInRegion(Player player) {
				return player.getLocation().getRegionId() == 11602;
			}

			@Override
			public boolean isChamberNPC(int id) {
				return id >= 6254 && id <= 6259;
			}
		},
		ZAMORAK(new int[] { -1, -1, -1, -1 }) {
			@Override
			public int[] getKillcountNpcs() {
				return new int[] { 6218, 6257, 6256, 6255, 6229, 6230, 6231, 6270, 6216, 6214, 6211, 6221, 6276, 6277, 6219 };
			}

			@Override
			public boolean isInRegion(Player player) {
				return false;
			}
			@Override
			public boolean isChamberNPC(int id) {
				return id >= 6210 && id <= 6221;
			}
		};

		ChamberTypes(int[] npcIds) {
			this.npcIds = npcIds;
		}

		private int[] npcIds;

		public int[] getNpcIds() {
			return npcIds;
		}

		public int getKillsComponent() {
			return ordinal() + 6;
		}

		/**
		 * Checking if the player is in the region to get killcount for kills
		 * 
		 * @param player
		 * @return
		 */
		public abstract boolean isInRegion(Player player);

		/**
		 * If the npc is a chamber npc
		 * 
		 * @param npcId
		 *            The id of the npc
		 * @return
		 */
		public boolean isChamberNPC(int npcId) {
			return false;
		}

		/**
		 * The array of npcs which will increment killcount
		 * 
		 * @return
		 */
		public abstract int[] getKillcountNpcs();

		/**
		 * The amount of killcount players need to enterInstancedRoom chambers
		 */
		public static final int KILLCOUNT_REQUIREMENT = 10;

		/**
		 * Gets the type of chamber this npc belongs to. This is for killcount
		 * usage only.
		 * 
		 * @param npcId
		 *            The npc id
		 * @return
		 */
		public static ChamberTypes getKillcountChamberType(Player player, int npcId) {
			for (ChamberTypes type : ChamberTypes.values()) {
				if (!type.isInRegion(player)) {
					continue;
				}
				for (int id : type.getKillcountNpcs()) {
					if (id == npcId) {
						return type;
					}
				}
				if (type.isChamberNPC(npcId)) {
					return type;
				}
			}
			return null;
		}
	}

	/**
	 * Handles the death of a godwars killcount npc by incrementing total kills
	 * .
	 * 
	 * @param player
	 *            The player
	 * @param npc
	 *            The npc
	 * @return
	 */
	public static void handleKillcountNPCDeath(Player player, NPC npc) {
		int npcId = npc.getId();
		ChamberTypes type = ChamberTypes.getKillcountChamberType(player, npcId);
		if (type == null) {
			return;
		}
		player.getVariables().getGwdKillcount()[type.ordinal()] += 1;
		updateKills(player);
	}

	/**
	 * This method updates the total kills for all {@link ChamberTypes} over the
	 * inter
	 * 
	 * @param player
	 */
	public static void updateKills(Player player) {
		for (ChamberTypes type : ChamberTypes.values()) {
			player.getPacketSender().modifyText("" + player.getVariables().getGwdKillcount()[type.ordinal()], 599, type.getKillsComponent());
		}
	}

}
