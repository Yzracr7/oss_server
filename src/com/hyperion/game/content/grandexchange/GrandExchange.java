package com.hyperion.game.content.grandexchange;

import com.hyperion.Logger;
import com.hyperion.game.content.grandexchange.inter.GrandExchangeMainInterface;
import com.hyperion.game.content.grandexchange.inter.GrandExchangeOpenOffer;
import com.hyperion.game.content.grandexchange.inter.GrandExchangeSetupTemporary;
import com.hyperion.game.content.grandexchange.offer.GrandExchangeOffer;
import com.hyperion.game.content.grandexchange.offer.OfferState;
import com.hyperion.game.item.Item;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.tickable.impl.GrandExchangeMap;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;
import com.hyperion.utility.game.InputAction;

import java.nio.ByteBuffer;

/**
 * Grand exchange
 *
 * @author trees
 */
public class GrandExchange {

    /**
     * The player.
     */
    private final Player player;

    /**
     * The grand exchange offers.
     */
    private final GrandExchangeOffer[] offers = new GrandExchangeOffer[8];

    /**
     * The offer the player is currently constructing.
     */
    private GrandExchangeOffer temporaryOffer;

    /**
     * The grand exchange offer history.
     */
    private GrandExchangeOffer[] history = new GrandExchangeOffer[5];

    public void save(ByteBuffer buffer) {
        for (GrandExchangeOffer offer : offers) {
            if (offer != null) {
                buffer.put((byte) offer.getIndex());
                buffer.putLong(offer.getUid());
            }
        }
        buffer.put((byte) -1);
        for (GrandExchangeOffer o : history) {
            if (o == null) {
                buffer.put((byte) -1);
                continue;
            }
            buffer.put((byte) (o.isSell() ? 1 : 0));
            buffer.putShort((short) o.getItemId());
            buffer.putInt(o.getTotalCoinExchange());
            buffer.putInt(o.getCompletedAmount());
        }
    }

    public void parse(ByteBuffer buffer) {
        int index = -1;
        GrandExchangeOffer o;
        while ((index = buffer.get()) != -1) {
            long key = buffer.getLong();
            o = offers[index] = GrandExchangeMap.forUID(key);
            if (o != null)
                o.setIndex(index);
            else {
                System.out.println("Could not locate G.E offer for key " +
                        key + "!");
            }
        }
        for (int i = 0; i < history.length; i++) {
            int s = buffer.get();
            if (s == -1)
                continue;
            o = history[i] = new GrandExchangeOffer(buffer.getShort(), s == 1);
            o.setTotalCoinExchange(buffer.getInt());
            o.setCompletedAmount(buffer.getInt());
        }
    }

    public int getOpenedIndex() {
        return openedIndex;
    }

    public void setOpenedIndex(int openedIndex) {
        this.openedIndex = openedIndex;
    }

    /**
     * The currently opened index.
     */
    private int openedIndex = -1;

    /**
     * Constructs a new {@code GrandExchange} {@code Object}.
     *
     * @param player The player.
     */
    public GrandExchange(Player player) {
        this.player = player;
    }

    /**
     * @param string
     */
    static void print(String string) {
        Logger.getInstance().info("[MyOffers] : Print / " + string);
    }

    public void openNewOffer(int index, boolean sell) {
        player.getAttributes().set("grandexchangemenu", 672);
        openedIndex = index;
        getTemporaryOffer().setPlayer(player);
        player.getPacketSender().displayInterface(BUY_INTERFACE);
        player.getPacketSender().sendInterfaceConfig(672, 2, sell); // Show Search

        player.getPacketSender().modifyText("Choose an item", 672, 14); // Item Name
        player.getPacketSender().modifyText("Click the icon to the left to search for items.", 672, 15); // Item Description

        if (!sell) {
            player.getPacketSender().modifyText("+1K", 672, 28); // Add 1,000 / All
        } else {
            player.getPacketSender().modifyText("All", 672, 28); // Add 1,000 / All
        }
        player.getPacketSender().modifyText("", 672, 17); // GE Price
        player.getPacketSender().modifyText("1", 672, 18); // Quantity
        player.getPacketSender().modifyText("1", 672, 19); // Price Per
        player.getPacketSender().modifyText("0 coins", 672, 20); // Total Price

        if (!sell) {
            requestItemByName();
        } else {
            player.getInventory().getInterfaceContainer().setInterfaceId(15);
            player.getInventory().getInterfaceContainer().setChild(0);
            player.getInventory().getInterfaceContainer().setType(93);
            player.getPacketSender().displayInventoryInterface(15);
            player.getInventory().refresh();
            player.getPacketSender().sendItems(149, 0, 93, player.getInventory().getItems());
        }
    }

    int temp_requested_amount = 1;

    public int requestPrice() {
        temp_requested_amount = 1;
        player.getPacketSender().requestStringInput(new InputAction<String>("Enter the price you would like to pay") {
            @Override
            public void handleInput(String input) {
                if (input == null || input.length() <= 0) {
                    System.out.println("Amount invalid");
                    return;
                }
                String kkkkk = input.replaceAll("k", "000").replaceAll("m", "000000");
                temp_requested_amount = Integer.parseInt(kkkkk);
                getTemporaryOffer().setOfferedValue(temp_requested_amount);
                GrandExchangeSetupTemporary.open(player, false);
            }
        });
        return temp_requested_amount;
    }

    public int requestAmount() {
        temp_requested_amount = 1;
        player.getPacketSender().requestStringInput(new InputAction<String>("Enter the amount you would like to purchase") {
            @Override
            public void handleInput(String input) {
                if (input == null || input.length() <= 0) {
                    System.out.println("Amount invalid");
                    return;
                }
                String kkkkk = input.replaceAll("k", "000").replaceAll("m", "000000");
                temp_requested_amount = Integer.parseInt(kkkkk);
                getTemporaryOffer().setAmount(temp_requested_amount);
                GrandExchangeSetupTemporary.open(player, false);
            }
        });
        return temp_requested_amount;
    }

    public void requestItemByName() {
        player.getPacketSender().requestStringInput(new InputAction<String>("Search an exact item by name") {
            @Override
            public void handleInput(String input) {
                if (input == null || input.length() <= 0) {
                    //endDialogue(player, -1);
                    return;
                }
                ItemDefinition item = ItemDefinition.forName(input);
                if (item == null) {
                    //System.out.println("Guessing");
                    item = ItemDefinition.guess(input);
                }
                if (item == null || !item.isTradeable()) {
                    return;
                }
                player.getGrandeExchange().setTemporaryOffer(new GrandExchangeOffer(item.getId(), false));
                player.getGrandeExchange().getTemporaryOffer().setAmount(1);
                player.getGrandeExchange().getTemporaryOffer().setOfferedValue(item.getGeneralPrice());
                player.getGrandeExchange().getTemporaryOffer().setTimeStamp(System.currentTimeMillis());
                player.getGrandeExchange().getTemporaryOffer().setState(OfferState.PENDING);
                player.getGrandeExchange().getTemporaryOffer().setTotalCoinExchange(0);
                player.getGrandeExchange().getTemporaryOffer().setPlayerUID(player.getUid());
                player.getGrandeExchange().getTemporaryOffer().setLimitation(true);
                GrandExchangeSetupTemporary.build(player);
            }
        });
    }

    /**
     * Constructs a new sale offer.
     *
     * @param item The item to sell.
     */
    public void constructSale(Item item) {
        if (openedIndex < 0 || offers[openedIndex] != null) {
            System.out.println("Offer is not null returning");
            return;
        }
        if (item.getId() == 995) {
            player.sendMessage("You can't offer money!");
            return;
        }
        int id = item.getId();
        if (item.getDefinition().isNoted())
            id = item.getDefinition().getNoteId() - 1;
        //   if (GrandExchangeDatabase.getDatabase().get(id) == null) {
        //  player.sendMessage("This item can't be sold on the Grand Exchange.");
        //  return;
        // }
        openNewOffer(openedIndex, true);

        System.out.println("Creating offer...");

        setTemporaryOffer(new GrandExchangeOffer(item.getId(), true));
        getTemporaryOffer().setDefault();
        getTemporaryOffer().setPlayer(player);
        getTemporaryOffer().setAmount(item.getCount());
        getTemporaryOffer().setOfferedValue(ItemDefinition.forId(item.getId()).getGeneralPrice());
        player.getPacketSender().sendItem(672, 38, 5, 0, item);
        GrandExchangeSetupTemporary.open(player, true);
    }

    public boolean handleMainInterface(int interface_id, int opcode, int button, int slot, int itemId) {
        if (interface_id != MAIN_INTERFACE) {
            return false;
        }

        switch (button) {
            case 98: // SELL 4
                player.getGrandeExchange().setTemporaryOffer(new GrandExchangeOffer(-1, true));
                player.getPacketSender().sendItem(672, 38, 5, 0, null);
                openNewOffer(3, true);
                return true;
            case 95: // BUY 4
                player.getGrandeExchange().setTemporaryOffer(new GrandExchangeOffer(-1, false));
                player.getPacketSender().sendItem(672, 38, 5, 0, null);
                openNewOffer(3, false);
                return true;
            case 96: // SELL 3
                player.getGrandeExchange().setTemporaryOffer(new GrandExchangeOffer(-1, true));
                player.getPacketSender().sendItem(672, 38, 5, 0, null);
                openNewOffer(2, true);
                return true;
            case 97: // BUY 3
                player.getGrandeExchange().setTemporaryOffer(new GrandExchangeOffer(-1, false));
                player.getPacketSender().sendItem(672, 38, 5, 0, null);
                openNewOffer(2, false);
                return true;
            case 94: // SELL 2
                player.getGrandeExchange().setTemporaryOffer(new GrandExchangeOffer(-1, true));
                player.getPacketSender().sendItem(672, 38, 5, 0, null);
                openNewOffer(1, true);
                return true;
            case 93: // BUY 2
                player.getGrandeExchange().setTemporaryOffer(new GrandExchangeOffer(-1, false));
                player.getPacketSender().sendItem(672, 38, 5, 0, null);
                openNewOffer(1, false);
                return true;
            case 92: // SELL 1
                player.getGrandeExchange().setTemporaryOffer(new GrandExchangeOffer(-1, true));
                player.getPacketSender().sendItem(672, 38, 5, 0, null);
                openNewOffer(0, true);
                return true;
            case 91: // BUY 1
                player.getGrandeExchange().setTemporaryOffer(new GrandExchangeOffer(-1, false));
                player.getPacketSender().sendItem(672, 38, 5, 0, null);
                openNewOffer(0, false);
                return true;
            default:
                System.out.println("GE MAIN INTERFACE BUTTON : [" + button + " : " + slot + "] item " + itemId);
                break;
        }
        return false;
    }

    public boolean handleOfferStatusInterface(int interface_id, int opcode, int button, int slot, int itemId) {
        if (interface_id != 674) {
            return false;
        }
        switch (button) {
            case 23: // Abort offer
                if (getOpenedOffer().getState() != OfferState.ABORTED) {
                    int amount_left = getOpenedOffer().getAmountLeft();
                    if (getOpenedOffer().getCompletedAmount() == getOpenedOffer().getAmount()) {
                        // TODO OFFER IS COMPLETE
                    } else {
                        if (getOpenedOffer().isSell()) {
                            Item slot_1 = null;
                            Item slot_2 = null;
                            int open_slot = -1;
                            if (getOpenedOffer().getWithdraw()[0] == null || getOpenedOffer().getWithdraw()[0].getId() == 7774) {
                                open_slot = 0;
                            } else if (getOpenedOffer().getWithdraw()[1] == null || getOpenedOffer().getWithdraw()[1].getId() == 7774) {
                                open_slot = 1;
                            }
                            if (open_slot == -1) {
                                System.out.println("Both slots are full!");
                                return true;
                            }
                            if (open_slot == 0) {
                                if (getOpenedOffer().getCompletedAmount() < getOpenedOffer().getAmount()) {
                                    slot_1 = new Item(getOpenedOffer().getItemId(), getOpenedOffer().getAmount() - getOpenedOffer().getCompletedAmount());
                                    if (getOpenedOffer().getTotalCoinExchange() < 0 && (getOpenedOffer().getWithdraw()[1] == null || getOpenedOffer().getWithdraw()[1].getId() == 7774)) {
                                        slot_2 = new Item(995, getOpenedOffer().getTotalCoinExchange());
                                    } else {
                                        slot_2 = null;
                                    }
                                    System.out.println("Completed amount " + getOpenedOffer().getCompletedAmount() + " / " + getOpenedOffer().getAmount());

                                    // System.out.println("Setting slot 1 " + slot_1.getId() + " " + slot_1.getCount());
                                    //  System.out.println("Setting slot 2 " + slot_2.getId() + " " + slot_2.getCount());

                                    getOpenedOffer().setWithdraw(new Item[]{slot_1, slot_2});
                                    getOpenedOffer().setCompletedAmount(getOpenedOffer().getAmount());
                                    getOpenedOffer().setState(OfferState.ABORTED);
                                   // GrandExchangeOpenOffer.open(player, true);
                                }
                            }
                        } else {
                            Item slot_1 = null;
                            Item slot_2 = null;
                            int open_slot = -1;
                            if (getOpenedOffer().getWithdraw()[0] == null) {
                                open_slot = 0;
                            } else if (getOpenedOffer().getWithdraw()[1] == null) {
                                open_slot = 1;
                            }
                            if (open_slot == -1) {
                                System.out.println("Both slots are full!");
                                return true;
                            }
                            if (open_slot == 0) {
                                if (getOpenedOffer().getCompletedAmount() < getOpenedOffer().getAmount()) {
                                    slot_1 = new Item(995, (getOpenedOffer().getAmount() * getOpenedOffer().getOfferedValue() - getOpenedOffer().getAmount() * getOpenedOffer().getCompletedAmount()));
                                    if (getOpenedOffer().getTotalCoinExchange() < 0 && getOpenedOffer().getWithdraw()[1] == null) {
                                        slot_2 = new Item(995, getOpenedOffer().getTotalCoinExchange());
                                    }
                                    System.out.println("Completed amount " + getOpenedOffer().getCompletedAmount() + " / " + getOpenedOffer().getAmount());

                                    //System.out.println("Setting slot 1 " + slot_1.getId() + " " + slot_1.getCount());
                                    //System.out.println("Setting slot 2 " + slot_2.getId() + " " + slot_2.getCount());

                                    getOpenedOffer().setWithdraw(new Item[]{slot_1, slot_2});
                                    getOpenedOffer().setCompletedAmount(getOpenedOffer().getAmount());
                                    getOpenedOffer().setState(OfferState.ABORTED);
                                  //  GrandExchangeOpenOffer.open(player, true);
                                }
                            }
                        }
                    }
                } else {
                    player.sendMessage("This offer is already closed.");
                }
                return true;
            case 12: // Back
                GrandExchangeMainInterface.open(player, false);
                player.getPacketSender().closeChatboxInterface();
                return true;
        }
        return false;
    }

    public boolean handleOfferInterface(int interface_id, int opcode, int button, int slot, int itemId) {
        if (interface_id != BUY_INTERFACE) {
            return false;
        }
        if (getTemporaryOffer() == null) {
            return true;
        }
        switch (button) {
            case 2: // Search
                requestItemByName();
                return true;
            case 5:
                int amount_quantity_add_10 = player.getGrandeExchange().getTemporaryOffer().getAmount();
                player.getGrandeExchange().getTemporaryOffer().setAmount(amount_quantity_add_10 + 10);
                // showTemporaryOffer(player.getGrandeExchange().getTemporaryOffer().isSell());
                return true;
            case 6:
                int amount_quantity_add_100 = player.getGrandeExchange().getTemporaryOffer().getAmount();
                player.getGrandeExchange().getTemporaryOffer().setAmount(amount_quantity_add_100 + 100);
                //showTemporaryOffer(player.getGrandeExchange().getTemporaryOffer().isSell());
                return true;
            case 7:
                int amount_quantity_add_1000 = player.getGrandeExchange().getTemporaryOffer().getAmount();
                player.getGrandeExchange().getTemporaryOffer().setAmount(amount_quantity_add_1000 + 1000);
                // showTemporaryOffer(player.getGrandeExchange().getTemporaryOffer().isSell());
                return true;
            case 8:
                requestAmount();
                return true;
            case 4:
            case 10: // Add Quantity
                int amount_quantity_add = player.getGrandeExchange().getTemporaryOffer().getAmount();
                player.getGrandeExchange().getTemporaryOffer().setAmount(amount_quantity_add + 1);
                //  showTemporaryOffer(player.getGrandeExchange().getTemporaryOffer().isSell());
                return true;
            case 13:
                int amount_quantity_sub = player.getGrandeExchange().getTemporaryOffer().getAmount();
                player.getGrandeExchange().getTemporaryOffer().setAmount(amount_quantity_sub - 1);
                // showTemporaryOffer(player.getGrandeExchange().getTemporaryOffer().isSell());
                return true;
            case 22:
                player.getGrandeExchange().getTemporaryOffer().setOfferedValue(ItemDefinition.forId(player.getGrandeExchange().getTemporaryOffer().getItemId()).getGeneralPrice());
                //  showTemporaryOffer(player.getGrandeExchange().getTemporaryOffer().isSell());
                return true;
            case 23: // Specific Price Amount
                requestPrice();
                return true;
            case 33: // Back
                player.getGrandeExchange().setTemporaryOffer(null);
                GrandExchangeMainInterface.open(player, false);
                player.getPacketSender().closeChatboxInterface();
                return true;
            case 36:
                System.out.println("Attempting to create offer... " + getTemporaryOffer().getPlayerUID() + " : " + getTemporaryOffer().getItemId());
                offers[openedIndex] = getTemporaryOffer();
                GrandExchangeMap.dispatch(player, player.getGrandeExchange().getTemporaryOffer());
                return true;
            default:
                System.out.println("GE BUY INTERFACE BUTTON : [" + button + " : " + slot + "] item " + itemId);
                break;
        }
        return false;
    }

    /**
     * Gets the temporaryOffer.
     *
     * @return The temporaryOffer.
     */
    public GrandExchangeOffer getTemporaryOffer() {
        return temporaryOffer;
    }

    /**
     * Sets the temporaryOffer.
     *
     * @param temporaryOffer The temporaryOffer to set.
     */
    public void setTemporaryOffer(GrandExchangeOffer temporaryOffer) {
        this.temporaryOffer = temporaryOffer;
    }

    /**
     * Gets the currently opened offer.
     *
     * @return The grand exchange offer currently opened.
     */
    public GrandExchangeOffer getOpenedOffer() {
        if (openedIndex < 0)
            return null;
        return offers[openedIndex];
    }

    /**
     * Gets the currently opened offer.
     *
     * @return The grand exchange offer currently opened.
     */
    public int getOpenedOfferIndex() {
        if (openedIndex < 0)
            return -1;
        return openedIndex;
    }

    /**
     * Checks if the player has an active offer.
     *
     * @return {@code True} if so.
     */
    public boolean hasActiveOffer() {
        for (GrandExchangeOffer offer : offers)
            if (offer != null)
                return true;
        return false;
    }

    /**
     * Gets the offers.
     *
     * @return The offers.
     */
    public GrandExchangeOffer[] getOffers() {
        return offers;
    }

    int MAIN_INTERFACE = 671;
    int BUY_INTERFACE = 672;
}
