package com.hyperion.game.content.grandexchange.inter;

import com.hyperion.game.content.grandexchange.inter.data.GEMainInterfaceData;
import com.hyperion.game.content.grandexchange.offer.GrandExchangeOffer;
import com.hyperion.game.world.entity.player.Player;

/**
 * Grand Exchange Main
 *
 * @author trees
 */
public class GrandExchangeButtons {

    // MAIN_BUTTONS #VIEW_OPEN_OFFER_INTERFACE
    public static boolean handleMainInterface(Player plr, int interface_id, int opcode, int button, int slot, int itemId) {
        if (interface_id != GrandExchangeMainInterface.MAIN_OFFERS_INTERFACE) {
            return false;
        }
        for (int i = 0; i < GEMainInterfaceData.values().length; i++) {
            GEMainInterfaceData o = GEMainInterfaceData.values()[i];
            if (o.getScon() == button) {
                System.out.println(button + " i " + i + " " + GEMainInterfaceData.values().length);

                plr.getGrandeExchange().setTemporaryOffer(new GrandExchangeOffer((-1), true));
                GrandExchangeSetupTemporary.open(plr, true);
                plr.getGrandeExchange().setOpenedIndex(i);
                return true;
            } else if (o.getBcon() == button) {
                System.out.println(button + " o " + i + " " + GEMainInterfaceData.values().length);

                plr.getGrandeExchange().setTemporaryOffer(new GrandExchangeOffer((-1), false));
                GrandExchangeSetupTemporary.open(plr, false);
                plr.getGrandeExchange().setOpenedIndex(i);
                return true;
            }
        }
        switch (button) {
            case 42: // OPENED OFFER SLOT 1
            case 43: // OPENED OFFER SLOT 1
            case 44: // OPENED OFFER SLOT 1
            case 45: // OPENED OFFER SLOT 1
                GrandExchangeOpenOffer.open(plr, false, 0);
                return true;
            case 49: // OPENED OFFER SLOT 2
            case 50: // OPENED OFFER SLOT 2
            case 51: // OPENED OFFER SLOT 2
            case 52: // OPENED OFFER SLOT 2
                GrandExchangeOpenOffer.open(plr, false, 1);
                return true;
            case 56: // OPENED OFFER SLOT 3
            case 57: // OPENED OFFER SLOT 3
            case 58: // OPENED OFFER SLOT 3
            case 59: // OPENED OFFER SLOT 3
                GrandExchangeOpenOffer.open(plr, false, 2);
                return true;
            case 63: // OPENED OFFER SLOT 4
            case 64: // OPENED OFFER SLOT 4
            case 65: // OPENED OFFER SLOT 4
            case 66: // OPENED OFFER SLOT 4
                GrandExchangeOpenOffer.open(plr, false, 3);
                return true;
            case 70: // OPENED OFFER SLOT 5
            case 71: // OPENED OFFER SLOT 5
            case 72: // OPENED OFFER SLOT 5
            case 73: // OPENED OFFER SLOT 5
                GrandExchangeOpenOffer.open(plr, false, 4);
                return true;
            case 77: // OPENED OFFER SLOT 6
            case 78: // OPENED OFFER SLOT 6
            case 79: // OPENED OFFER SLOT 6
            case 80: // OPENED OFFER SLOT 6
                GrandExchangeOpenOffer.open(plr, false, 5);
                return true;
            case 84: // OPENED OFFER SLOT 7
            case 85: // OPENED OFFER SLOT 7
            case 86: // OPENED OFFER SLOT 7
            case 87: // OPENED OFFER SLOT 7
                GrandExchangeOpenOffer.open(plr, false, 6);
                return true;
            case 90: // OPENED OFFER SLOT 8
            case 91: // OPENED OFFER SLOT 8
            case 92: // OPENED OFFER SLOT 8
            case 93: // OPENED OFFER SLOT 8
                GrandExchangeOpenOffer.open(plr, false, 7);
                return true;
            default:
                System.out.println("GE MAIN_BUTTONS #671 : [" + button + " : " + slot + "] item " + itemId);
                break;
        }
        return false;
    }
}
