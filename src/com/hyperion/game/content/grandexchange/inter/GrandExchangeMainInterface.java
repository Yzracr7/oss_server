package com.hyperion.game.content.grandexchange.inter;

import com.hyperion.game.content.grandexchange.inter.data.GEMainInterfaceData;
import com.hyperion.game.content.grandexchange.offer.GrandExchangeOffer;
import com.hyperion.game.content.grandexchange.offer.OfferState;
import com.hyperion.game.item.Item;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;

/**
 * Grand Exchange Main
 *
 * @author trees
 */
public class GrandExchangeMainInterface {

    final static int MAIN_OFFERS_INTERFACE = 671;

    // OPEN MAIN_INTERFACE #VIEW_OPEN_OFFER_INTERFACE
    public static void open(Player plr, boolean refreshing) {
        if (!refreshing) {
            clear(plr);
        }
        //Vars
        plr.getVariables().setUsing_grand_exchange(true);
        plr.getAttributes().set("grandexchangemenu", MAIN_OFFERS_INTERFACE);

        //Build
        build(plr);
        plr.getPacketSender().displayInterface(MAIN_OFFERS_INTERFACE);
    }

    // CLEAR MAIN_INTERFACE #VIEW_OPEN_OFFER_INTERFACE
    public static void clear(Player plr) {

        for (int i = 0; i < GEMainInterfaceData.values().length; i++) {
            plr.getPacketSender().sendInterfaceConfig(MAIN_OFFERS_INTERFACE, GEMainInterfaceData.values()[i].getEmpty(), false);
            plr.getPacketSender().sendInterfaceConfig(MAIN_OFFERS_INTERFACE, GEMainInterfaceData.values()[i].getYellow(), false);
            plr.getPacketSender().sendInterfaceConfig(MAIN_OFFERS_INTERFACE, GEMainInterfaceData.values()[i].getGreen(), false);
            plr.getPacketSender().sendInterfaceConfig(MAIN_OFFERS_INTERFACE, GEMainInterfaceData.values()[i].getRed(), false);
            plr.getPacketSender().modifyText("", MAIN_OFFERS_INTERFACE, GEMainInterfaceData.values()[i].getName());
            plr.getPacketSender().modifyText("", MAIN_OFFERS_INTERFACE, GEMainInterfaceData.values()[i].getPrice());
            plr.getPacketSender().modifyText("", MAIN_OFFERS_INTERFACE, GEMainInterfaceData.values()[i].getTitle());
            plr.getPacketSender().sendItem(MAIN_OFFERS_INTERFACE, GEMainInterfaceData.values()[i].getItem(), 82, 0, null);
            plr.getPacketSender().sendInterfaceConfig(MAIN_OFFERS_INTERFACE, GEMainInterfaceData.values()[i].getBcon(), true);
            plr.getPacketSender().sendInterfaceConfig(MAIN_OFFERS_INTERFACE, GEMainInterfaceData.values()[i].getScon(), true);
        }
    }

    // BUILD MAIN_INTERFACE #VIEW_OPEN_OFFER_INTERFACE
    public static void build(Player plr) {
        if (!plr.getAttributes().get("grandexchangemenu").equals(MAIN_OFFERS_INTERFACE)) {
            System.out.println("Player is not in grand exchange menu! abort drawing...");
            return;
        }
        for (int i = 0; i < plr.getGrandeExchange().getOffers().length; i++) {
            if (plr.getGrandeExchange().getOffers()[i] != null) {
                GrandExchangeOffer offer = plr.getGrandeExchange().getOffers()[i];
                OfferState state = plr.getGrandeExchange().getOffers()[i].getState();
                switch (state) {
                    case COMPLETED:
                        plr.getPacketSender().sendInterfaceConfig(MAIN_OFFERS_INTERFACE, GEMainInterfaceData.values()[i].getGreen(), true);
                        break;
                    case UPDATED:
                        plr.getPacketSender().sendInterfaceConfig(MAIN_OFFERS_INTERFACE, GEMainInterfaceData.values()[i].getYellow(), true);
                        break;
                    case REMOVED:
                    case REGISTERED:
                        plr.getPacketSender().sendInterfaceConfig(MAIN_OFFERS_INTERFACE, GEMainInterfaceData.values()[i].getEmpty(), true);
                        break;
                    case OUTDATED:
                    case ABORTED:
                        plr.getPacketSender().sendInterfaceConfig(MAIN_OFFERS_INTERFACE, GEMainInterfaceData.values()[i].getRed(), true);
                        break;
                    default:
                        break;
                }
                System.out.println("[BOX " + (i) + "] Offer is for " + offer.getItemId() + " with a state of [" + offer.getState() + "]");
                plr.getPacketSender().sendItem(MAIN_OFFERS_INTERFACE, GEMainInterfaceData.values()[i].getItem(), 82, 0, new Item(offer.getItemId()));
                plr.getPacketSender().modifyText(offer.isSell() ? "Sell" : "Buy", MAIN_OFFERS_INTERFACE, GEMainInterfaceData.values()[i].getTitle());
                plr.getPacketSender().modifyText(ItemDefinition.forId(offer.getItemId()).getName(), MAIN_OFFERS_INTERFACE, GEMainInterfaceData.values()[i].getName());
                plr.getPacketSender().modifyText(NumberUtils.format(offer.getOfferedValue()) + " coins", MAIN_OFFERS_INTERFACE, GEMainInterfaceData.values()[i].getPrice());
                plr.getPacketSender().sendInterfaceConfig(MAIN_OFFERS_INTERFACE, GEMainInterfaceData.values()[i].getBcon(), false);
                plr.getPacketSender().sendInterfaceConfig(MAIN_OFFERS_INTERFACE, GEMainInterfaceData.values()[i].getScon(), false);
            }
        }
    }
}
