package com.hyperion.game.content.grandexchange.inter;

import com.hyperion.game.content.grandexchange.offer.GrandExchangeOffer;
import com.hyperion.game.item.Item;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;

/**
 * Grand Exchange Offers
 *
 * @author trees
 */
public class GrandExchangeOpenOffer {

    final static int VIEW_OPEN_OFFER_INTERFACE = 674;

    // OPEN MAIN_INTERFACE #VIEW_OPEN_OFFER_INTERFACE
    public static void open(Player plr, boolean refreshing, int id) {
        plr.getGrandeExchange().setOpenedIndex(id);
        if (!refreshing) {
            clear(plr);
        }
        //Vars
        plr.getVariables().setUsing_grand_exchange(true);
        plr.getAttributes().set("grandexchangemenu", VIEW_OPEN_OFFER_INTERFACE);

        //Build
        build(plr, id);
        plr.getPacketSender().displayInterface(VIEW_OPEN_OFFER_INTERFACE);
    }

    // CLEAR MAIN_INTERFACE #VIEW_OPEN_OFFER_INTERFACE
    public static void clear(Player plr) {
    }

    // CLEAR MAIN_INTERFACE #VIEW_OPEN_OFFER_INTERFACE
    public static void build(Player plr, int id) {
        GrandExchangeOffer offer = plr.getGrandeExchange().getOffers()[id];
        if (offer == null) {
            System.out.println("offer is nullnullnull");
            return;
        }
        boolean sell = offer.isSell();
        if (!plr.getAttributes().get("grandexchangemenu").equals(VIEW_OPEN_OFFER_INTERFACE)) {
            System.out.println("Player is not in offers exchange menu! abort drawing...");
            return;
        }
        System.out.println(sell ? "is sell " : "is not sell");
        plr.getPacketSender().modifyText("" + ItemDefinition.forId(offer.getItemId()).getName(), 674, 4); // Item Name
        plr.getPacketSender().modifyText("" + ItemDefinition.forId(offer.getItemId()).getDescription(), 674, 5); // Item Description
        plr.getPacketSender().modifyText("" + NumberUtils.format(ItemDefinition.forId(offer.getItemId()).getGeneralPrice()), 674, 7); // GE Price
        plr.getPacketSender().modifyText("" + NumberUtils.format(offer.getAmount()), 674, 8); // Quantity
        plr.getPacketSender().modifyText("" + NumberUtils.format(offer.getOfferedValue()), 674, 9); // Price Per
        plr.getPacketSender().modifyText("" + NumberUtils.format(offer.getOfferedValue() * offer.getAmount()), 674, 10); // Total Price
        plr.getPacketSender().sendItem(674, 2, 5, 0, new Item(offer.getItemId()));
        if (offer.getWithdraw() != null) {
            plr.getPacketSender().sendItem(674, 13, 5, 0, offer.getWithdraw()[0]);
            plr.getPacketSender().sendItem(674, 14, 5, 0, offer.getWithdraw()[1]);

        } else {
            plr.getPacketSender().sendItem(674, 13, 5, 0, null);
            plr.getPacketSender().sendItem(674, 14, 5, 0, null);
        }
        plr.getPacketSender().modifyText((sell ? "Sell" : "Buy") + " Offer", 674, 17);
        plr.getPacketSender().sendInterfaceConfig(674, 19, !sell); // Show Search
        plr.getPacketSender().sendInterfaceConfig(674, 20, sell); // Show Search
        plr.getPacketSender().modifyText("You have " + (sell ? "sold" : "bought") + " the total of " + offer.getCompletedAmount(), 674, 15);
        plr.getPacketSender().modifyText("for a total price of " + NumberUtils.format(offer.getTotalCoinExchange()), 674, 16);
    }
}
