package com.hyperion.game.content.grandexchange.inter;

import com.hyperion.game.content.grandexchange.offer.GrandExchangeOffer;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;

/**
 * Grand Exchange Offers
 *
 * @author trees
 */
public class GrandExchangeSetupTemporary {

    final static int OFFER_SETUP_TEMPORARY_INTERFACE = 672;

    // OPEN MAIN_INTERFACE #VIEW_OPEN_OFFER_INTERFACE
    public static void open(Player plr, boolean refreshing) {
        if (!refreshing) {
            clear(plr);
        }
        //Vars
        plr.getVariables().setUsing_grand_exchange(true);
        plr.getAttributes().set("grandexchangemenu", OFFER_SETUP_TEMPORARY_INTERFACE);

        //Build
        build(plr);
        plr.getPacketSender().displayInterface(OFFER_SETUP_TEMPORARY_INTERFACE);
    }

    // CLEAR MAIN_INTERFACE #VIEW_OPEN_OFFER_INTERFACE
    public static void clear(Player plr) {
    }

    // CLEAR MAIN_INTERFACE #VIEW_OPEN_OFFER_INTERFACE
    public static void build(Player plr) {

        boolean sell = plr.getGrandeExchange().getTemporaryOffer().isSell();
        GrandExchangeOffer offer = plr.getGrandeExchange().getTemporaryOffer();
        if (offer == null) {
            System.out.println("offer is nullnullnull");
            return;
        } else {
            System.out.println("Building interface : output-'" + offer.isSell() + "'" + ItemDefinition.forId(offer.getItemId()).getName() + ",$" + offer.getOfferedValue());
        }
        if (!plr.getAttributes().get("grandexchangemenu").equals(OFFER_SETUP_TEMPORARY_INTERFACE)) {
            System.out.println("Player is not in offers exchange menu! abort drawing...");
            return;
        }

        plr.getPacketSender().modifyText("Choose an item", OFFER_SETUP_TEMPORARY_INTERFACE, 14); // Item Name
        plr.getPacketSender().modifyText("Click the icon to the left to search for items.", OFFER_SETUP_TEMPORARY_INTERFACE, 15); // Item Description

        plr.getPacketSender().modifyText((sell ? "Sell" : "Buy") + " Offer", OFFER_SETUP_TEMPORARY_INTERFACE, 41);
        if (!sell) {
            plr.getPacketSender().sendInterfaceConfig(OFFER_SETUP_TEMPORARY_INTERFACE, 44, false);
            plr.getPacketSender().modifyText("+1K", OFFER_SETUP_TEMPORARY_INTERFACE, 28); // Add 1,000 / All
        } else {
            plr.getPacketSender().sendInterfaceConfig(OFFER_SETUP_TEMPORARY_INTERFACE, 44, true);
            plr.getPacketSender().modifyText("All", OFFER_SETUP_TEMPORARY_INTERFACE, 28); // Add 1,000 / All
        }
        plr.getPacketSender().modifyText(NumberUtils.format(ItemDefinition.forId(offer.getItemId()).getGeneralPrice()), OFFER_SETUP_TEMPORARY_INTERFACE, 17); // GE Price
        plr.getPacketSender().modifyText(NumberUtils.format(offer.getAmount()), OFFER_SETUP_TEMPORARY_INTERFACE, 18); // Quantity
        plr.getPacketSender().modifyText(NumberUtils.format(offer.getOfferedValue()), OFFER_SETUP_TEMPORARY_INTERFACE, 19); // Price Per
        plr.getPacketSender().modifyText(NumberUtils.format(offer.getAmount() * offer.getOfferedValue()) + " coins", OFFER_SETUP_TEMPORARY_INTERFACE, 20); // Total Price

        if (!sell) {
            plr.getGrandeExchange().requestItemByName();
        } else {
            plr.getInventory().getInterfaceContainer().setInterfaceId(15);
            plr.getInventory().getInterfaceContainer().setChild(0);
            plr.getInventory().getInterfaceContainer().setType(93);
            plr.getPacketSender().displayInventoryInterface(15);
            plr.getInventory().refresh();
            plr.getPacketSender().sendItems(149, 0, 93, plr.getInventory().getItems());
        }
    }
}
