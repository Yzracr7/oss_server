package com.hyperion.game.content.grandexchange.inter.data;

public enum GEMainInterfaceData {
    BOX_1(42, 43, 44, 45, 46, 47, 48, 98, 108, 119, 120),
    BOX_2(49, 50, 51, 52, 53, 54, 55, 99, 109, 121, 122),
    BOX_3(56, 57, 58, 59, 60, 61, 62, 100, 110, 123, 124),
    BOX_4(63, 64, 65, 66, 67, 68, 69, 101, 111, 125, 126),
    BOX_5(70, 71, 72, 73, 74, 75, 76, 102, 112, 127, 128),
    BOX_6(77, 78, 79, 80, 81, 82, 83, 103, 113, 129, 130),
    BOX_7(84, 85, 86, 87, 88, 89, 90, 104, 114, 131, 132),
    BOX_8(91, 92, 93, 94, 95, 96, 97, 105, 115, 133, 134);

    int empty;
    int yellow;
    int green;
    int red;
    int model;
    int name;
    int price;
    int title;
    int item;
    int bcon;
    int scon;

    GEMainInterfaceData(int empty, int yellow, int green, int red, int model, int name, int price, int title, int item, int bcon, int scon) {
        this.empty = empty;
        this.yellow = yellow;
        this.green = green;
        this.red = red;
        this.model = model;
        this.name = name;
        this.price = price;
        this.title = title;
        this.item = item;
        this.bcon = bcon;
        this.scon = scon;
    }

    public int getBcon() { return bcon; }

    public void setBcon(int bcon) { this.bcon = bcon; }

    public int getScon() { return scon; }

    public void setScon(int scon) { this.scon = scon; }

    public int getItem() {
        return item;
    }

    public void setItem(int item) {
        this.item = item;
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public int getEmpty() {
        return empty;
    }

    public void setEmpty(int empty) {
        this.empty = empty;
    }

    public int getYellow() {
        return yellow;
    }

    public void setYellow(int yellow) {
        this.yellow = yellow;
    }

    public int getGreen() {
        return green;
    }

    public void setGreen(int green) {
        this.green = green;
    }

    public int getRed() {
        return red;
    }

    public void setRed(int red) {
        this.red = red;
    }

    public int getModel() {
        return model;
    }

    public void setModel(int model) {
        this.model = model;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

}
