package com.hyperion.game.content.grandexchange.offer;

import java.util.HashMap;
import java.util.Map;

/**
 * Handles the buying limitations.
 *
 * @author Emperor
 */
public final class BuyingLimitation {

    /**
     * Mapping holding buying amounts per player for every item id.
     */
    private static final Map<Integer, Map<Integer, Integer>> CACHE = new HashMap<>();

    /**
     * Constructs a new {@code BuyingLimitation} {@code Object}.
     */
    private BuyingLimitation() {
    }

    /**
     * Gets the maximum temp_requested_amount the player can buy of this item.
     *
     * @param itemId    The item id.
     * @param playerUID The player UID.
     * @return The maximum temp_requested_amount to buy.
     */
    public static int getMaximumBuy(int itemId, int playerUID) {
        Map<Integer, Integer> data = CACHE.get(itemId);
        Integer current = 0;
        if (data != null)
            current = data.get(playerUID);
        if (current == null)
            current = 0;
        return 2000; // TODO
    }

    /**
     * Updates the currently bought temp_requested_amount.
     *
     * @param itemId    The item id.
     * @param playerUID The player UID.
     * @param amount    The temp_requested_amount.
     */
    public static void updateBoughtAmount(int itemId, int playerUID, int amount) {
        Map<Integer, Integer> data = CACHE.get(itemId);
        if (data == null)
            CACHE.put(itemId, data = new HashMap<>());
        Integer current = data.get(playerUID);
        if (current == null)
            current = 0;
        current += amount;
        data.put(playerUID, current);
    }

    /**
     * Checks if the offer is limited.
     *
     * @return {@code True} if so.
     */
    public static boolean isLimited(int itemId, int playerUID) {
        Map<Integer, Integer> data = CACHE.get(itemId);
        if (data == null)
            return false;
        Integer current = data.get(playerUID);
        if (current == null)
            return false;
        int max = 2000; // TODO
        return current >= max;
    }

    /**
     * Clears the cached data.
     */
    public static void clear() {
        CACHE.clear();
    }
}