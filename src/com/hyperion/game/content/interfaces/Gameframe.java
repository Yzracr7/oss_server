package com.hyperion.game.content.interfaces;

import com.hyperion.game.world.entity.player.Player;

public class Gameframe {

    public static void setRedStone(Player player, int stone) {

        player.getPacketSender().sendInterfaceConfig(548, 114, stone == 1); // ATTACK STONE
        player.getPacketSender().sendInterfaceConfig(548, 115, stone == 2); // SKILLS STONE
        player.getPacketSender().sendInterfaceConfig(548, 116, stone == 3); // QUEST STONE
        player.getPacketSender().sendInterfaceConfig(548, 117, stone == 4); // INVENTORY STONE
        player.getPacketSender().sendInterfaceConfig(548, 118, stone == 5); // EQUIPMENT STONE
        player.getPacketSender().sendInterfaceConfig(548, 119, stone == 6); // PRAYER STONE
        player.getPacketSender().sendInterfaceConfig(548, 120, stone == 7); // MAGIC STONE

        player.getPacketSender().sendInterfaceConfig(548, 137, stone == 8); // CLAN STONE
        player.getPacketSender().sendInterfaceConfig(548, 138, stone == 9); // FRIENDS STONE
        player.getPacketSender().sendInterfaceConfig(548, 139, stone == 10); // IGNORE STONE
        player.getPacketSender().sendInterfaceConfig(548, 140, stone == 11); // LOGOUT STONE
        player.getPacketSender().sendInterfaceConfig(548, 141, stone == 12); // SETTINGS STONE
        player.getPacketSender().sendInterfaceConfig(548, 142, stone == 13); // EMOTES STONE

        player.getPacketSender().sendInterfaceConfig(548, 143, stone == 14); // MUSIC STONE
    }

    public static void hideStones(Player player) {
      //  player.getPacketSender().sendInterfaceConfig(548, 113, false); // TOP
       // player.getPacketSender().sendInterfaceConfig(548, 136, false); // BOTTOM
    }
    public static void enableStones(Player player){
        player.getPacketSender().sendInterfaceConfig(548, 113, true); // TOP
        player.getPacketSender().sendInterfaceConfig(548, 136, true); // BOTTOM
    }
}
