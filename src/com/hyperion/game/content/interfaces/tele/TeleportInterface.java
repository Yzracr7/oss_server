package com.hyperion.game.content.interfaces.tele;


import com.hyperion.Logger;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;

public class TeleportInterface {

    /**
     * @param string
     */
    static void print(String string) {
        Logger.getInstance().info("[TeleportInterface] : Print / " + string);
    }

    public static void open(Player player) {
        clear(player);
        sendTeleportTitles(0, player);
        player.getPacketSender().displayInterface(668);
        player.getAttributes().set("teleportInterface", 0);
    }

    public static void sendTeleportTitles(int tab, Player player) {
        TeleportInterfaceData data = TeleportInterfaceData.CITIES;
        player.getAttributes().set("teleportInterface", tab);
        switch (tab) {
            case 0:
                data = TeleportInterfaceData.CITIES;
                break;
            case 1:
                data = TeleportInterfaceData.MONSTERS;
                break;
            case 2:
                data = TeleportInterfaceData.BOSSES;
                break;
            case 3:
                data = TeleportInterfaceData.SKILLING;
                break;
            case 4:
                data = TeleportInterfaceData.DUNGEONS;
                break;
            case 5:
                data = TeleportInterfaceData.MINIGAMES;
                break;
            case 6:
                data = TeleportInterfaceData.WILDERNESS;
                break;
        }
        for (int i = 0; i < data.strings.length; i++) {
            player.getPacketSender().modifyText(data.getStrings()[i], data.getInterface_id(), data.getConfig_id()[i]);
        }
    }

    public static void clear(Player player) {
        for (int i = 0; i < 5; i++) {
        }
    }

    public static boolean handleTeleportInterfaceActions(Player player, int interfaceId, int buttonId) {
        switch (interfaceId) {
            case 668:
                clear(player);
                switch (buttonId) {
                    case 124://CITIES
                        sendTeleportTitles(0, player);
                        break;
                    case 125://MONSTERS
                        sendTeleportTitles(1, player);
                        break;
                    case 126://BOSSES
                        sendTeleportTitles(2, player);
                        break;
                    case 127://SKILLING
                        sendTeleportTitles(3, player);
                        break;
                    case 128://DUNGEONS
                        sendTeleportTitles(4, player);
                        break;
                    case 129://MINIGAMES
                        sendTeleportTitles(5, player);
                        break;
                    case 130://WILDERNESS
                        sendTeleportTitles(6, player);
                        break;
                    case 199:
                    case 202:
                    case 205:
                    case 208:
                    case 211:
                    case 214:
                        doTeleportAction(player, buttonId);
                        break;
                    case 410: // QP; Rock Crab
                    case 411: // QP; Fishing Guild
                    case 412: // QP; Mage Bank
                        doTeleportAction(player, buttonId);
                        break;
                    default:
                        System.out.println("Teleport Interface Button : " + buttonId);
                        break;
                }
                return true;
        }
        return false;
    }

    public static boolean hasAmountRequired(Player player, int tab) {
        int amount = -1;
        switch (tab) {
            case 0: // CITIES
                amount = 1000;
                break;
            case 1: // MONSTERS
                amount = 3000;
                break;
            case 2: // BOSSES
                amount = 3000;
                break;
        }
        if (amount != -1 && player.getInventory().deleteItem(995, amount)) {
            return true;
        }
        return false;
    }

    public static boolean doTeleportAction(Player player, int button) {
        System.out.println("Doing teleport action");
        if (player.getAttributes().isSet("interactingEntity") && player.getAttributes().isSet("teleportInterface")) {
            print("Entity:" + ((NPC) player.getAttributes().get("interactingEntity")).getDefinition().getName());
            print("Action: " + player.getAttributes().get("teleportInterface").toString());
            int tab = player.getAttributes().get("teleportInterface");
            if (!hasAmountRequired(player, tab) && button != 412 && button != 411 && button != 410) {
                player.sendMessage("You do not have the gold required.");
                return true;
            }
            Location destination = null;
            switch (tab) {
                case 0: // CITIES TAB
                    switch (button) {
                        case 199: // Lumbridge
                            destination = Location.create(3221 + NumberUtils.random(1), 3246 + NumberUtils.random(1), 0);
                            break;
                        case 202: // Draynor
                            destination = Location.create(3078, 3249, 0);
                            break;
                        case 205: // Falador
                            destination = Location.create(2967, 3410, 0);
                            break;
                        case 208: // Canafis
                            destination = Location.create(3490, 3484, 0);
                            break;
                        case 211: // Pollniveach
                            destination = Location.create(3351, 3001, 0);
                            break;
                        case 214: // Camelot
                            destination = Location.create(2748 + NumberUtils.random(2), 3471 + NumberUtils.random(2), 0);
                            break;
                        case 410: // Rock Crabs
                            destination = Location.create(2748 + NumberUtils.random(2), 3471 + NumberUtils.random(2), 0);
                            break;
                        default:
                            print("Unknown city teleport action button : " + button);
                            break;
                    }
                    break;

                case 1: // MONSTERS TAB
                    switch (button) {
                        case 199: // Neitzot
                            destination = player.getLocation();
                            break;
                        case 202: // Bandit Camp
                            destination = player.getLocation();
                            break;
                        case 205: // Ape Atoll
                            destination = player.getLocation();
                            break;
                        case 208: // Mos Le'Harmless
                            destination = player.getLocation();
                            break;
                        case 211: // Kalphite Lair
                            destination = player.getLocation();
                            break;
                        default:
                            print("Unknown monster teleport action button : " + button);
                            break;
                    }
                    break;
                case 2: // BOSSES
                    switch (button) {
                        case 199: // Giant Mole
                            destination = player.getLocation();
                            break;
                        case 202: // Zulruh
                            destination = Location.create(2200, 3055, 0);
                            break;
                        case 205: // Kraken Cave
                            destination = Location.create(2265, 3607, 0);
                        default:
                            print("Unknown monster teleport action button : " + button);
                            break;
                    }
                    break;
            }
            if (destination != null) {
                teleportPlayer(player, destination);
            }
            return true;
        } else {
            System.out.println("telelelelee");
        }

        return false;
    }

    public static void teleportPlayer(Player player, Location destination) {
        NPC npc = (NPC) player.getInteractingEntity();
        player.getPacketSender().sendCloseInterface();
        if (npc != null) {
            npc.playForcedChat("Belehehehahhahahaha!");
            npc.face(player.getLocation());
            npc.animate(1818);
            npc.playGraphic(343, 0, 100);
            player.getPacketSender().sendStillGraphics(player.getLocation(), new Graphic(342, 5, 0), 0);
            player.animate(1816);
            World.getWorld().submit(new Tickable(player, 5) {
                @Override
                public void execute() {
                    this.stop();
                    player.teleport(destination.getX() + NumberUtils.random(3), destination.getY(), destination.getZ());
                    player.getAttributes().remove("stopActions");
                    player.getAttributes().remove("interactingEntity");
                    player.getAttributes().remove("teleportInterface");
                    player.animate(65535);
                }
            });
        }
    }
}
