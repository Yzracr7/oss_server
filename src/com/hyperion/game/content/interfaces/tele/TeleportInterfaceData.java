package com.hyperion.game.content.interfaces.tele;

import com.hyperion.game.world.Location;

public enum TeleportInterfaceData {

    CITIES(new String[]{"    Lumbridge", "    Basic Training", "    Draynor Village", "    Skilling", "    Falador", "    Near Taverly", "    Canafis", "    Barrows Brothers", "    Pollnivech", "    Desert", "    Camelot", "    Resources"}, 668, new int[]{199, 200, 202, 203, 205, 206, 208, 209, 211, 212, 214, 215}, new Location[]{}),
    MONSTERS(new String[]{"    Neitzot Island", "    Yaks", "    Bandit Camp", "    Bandits", "    Ape Atoll", "    Monkeys", "    Mos Le'Harmless", "    Cave Horrors", "    Kalphite Lair", "    Kalphites", "    ", "    "}, 668, new int[]{199, 200, 202, 203, 205, 206, 208, 209, 211, 212, 214, 215}, new Location[]{}),
    BOSSES(new String[]{"    Giant Mole", "    Global", "    Zulruh", "    Instanced", "    Kraken Cave", "    Global / Instanced", "    ", "    ", "    ", "    ", "    ", "    "}, 668, new int[]{199, 200, 202, 203, 205, 206, 208, 209, 211, 212, 214, 215}, new Location[]{}),
    SKILLING(new String[]{"    Feldip Hills", "    Resources", "    Woodcutting Guild", "    Resources", "    ", "    ", "    ", "    ", "    ", "    ", "    ", "    "}, 668, new int[]{199, 200, 202, 203, 205, 206, 208, 209, 211, 212, 214, 215}, new Location[]{}),
    DUNGEONS(new String[]{"    Taverly Dungeon", "    Medium Level", "    Crash Island Dungeon", "    Medium level", "    Brimhaven Dungeon", "    Medium Level", "    God Wars Dungeon", "    High Level", "    ", "    ", "    ", "    "}, 668, new int[]{199, 200, 202, 203, 205, 206, 208, 209, 211, 212, 214, 215}, new Location[]{}),
    MINIGAMES(new String[]{"    Burgh de Rott", "    Barrows", "    Fight Caves", "    Fire Cape", "    ", "    ", "    ", "    ", "    ", "    ", "    ", "    "}, 668, new int[]{199, 200, 202, 203, 205, 206, 208, 209, 211, 212, 214, 215}, new Location[]{}),
    WILDERNESS(new String[]{"    ", "    ", "    ", "    ", "    ", "    ", "    ", "    ", "    ", "    ", "    ", "    "}, 668, new int[]{199, 200, 202, 203, 205, 206, 208, 209, 211, 212, 214, 215}, new Location[]{});

    String[] strings;
    int interface_id;
    int[] config_id;
    Location[] locs;

    TeleportInterfaceData(String[] strns, int inter, int[] id, Location[] locs) {
        this.strings = strns;
        this.interface_id = inter;
        this.config_id = id;
        this.locs = locs;
    }

    public String[] getStrings() {
        return strings;
    }

    public int getInterface_id() {
        return interface_id;
    }

    public int[] getConfig_id() {
        return config_id;
    }

}
