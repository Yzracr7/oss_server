package com.hyperion.game.content.minigames;

import java.util.ArrayList;
import java.util.List;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;

/**
 * @author Nando
 * creds to Scu11, looked at his to make this one
 */
public class AbstractGameSession implements GameSession {

	public Tickable session_timer = null;
	
	@Override
	public Player[] getTeam() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void start_game() {
		// TODO Auto-generated method stub
		if (getTeam() != null) {
			World.getWorld().submit(session_timer());
		}
	}

	@Override
	public void end_game() {
		// TODO Auto-generated method stub
		for(Player participant : getTeam()) {
			if (participant != null) {
				participant.teleport(end_area());
				participant.resetVariables();
				participant.getPacketSender().sendRemoveOverlay();
			}
		}
		if(session_timer() != null) {
			session_timer().stop();
		}
	}

	@Override
	public void insert_player(Player player) {
		// TODO Auto-generated method stub
		if(getTeam() != null) {
			if (!player_ingame(player)) {
				int slot_placement = free_slot();
				if (slot_placement == -1) {
					return;
				}
				getTeam()[slot_placement] = player;
				int x = NumberUtils.random(starting_placement().getHighestX() - starting_placement().getLowestX());
				int y = NumberUtils.random(starting_placement().getHighestY() - starting_placement().getLowestY());
				Location placement = Location.create(starting_placement().getLowestX() + x, starting_placement().getLowestY() + y, 0);
				player.teleport(placement);
				player.resetVariables();
			}
		}
	}

	@Override
	public void remove_player(Player player) {
		// TODO Auto-generated method stub
		player.getAttributes().remove("pestsession");
		player.getPacketSender().sendRemoveOverlay();
		player.resetVariables();
		if(getTeam() != null) {
			for (int i = 0; i < getTeam().length; i++) {
				if (getTeam()[i] != null) {
					if (getTeam()[i] == player) {
						getTeam()[i] = null;
					}
				}
			}
			if (members_active() < 1) {
				end_game();
			}
		}
	}

	@Override
	public void handle_death(Entity deadEntity, Entity killer) {
		// TODO Auto-generated method stub
		deadEntity.teleport(end_area());
		deadEntity.resetVariables();
		if (!items_are_safe()) {
			//TODO - drop loot or w.e we wanna do here.
		}
	}
	
	@Override
	public void handle_logout(Player player) {
		player.setLocation(end_area());
		remove_player(player);
	}

	@Override
	public Tickable session_timer() {
		// TODO Auto-generated method stub
		return session_timer;
	}

	@Override
	public GameBounds starting_placement() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Location death_placement() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Location end_area() {
		return null;
	}

	@Override
	public boolean items_are_safe() {
		// TODO Auto-generated method stub
		return true;
	}
	
	@Override
	public boolean player_ingame(Player player) {
		if (getTeam() != null) {
			for (Player team : getTeam()) {
				if (team != null) {
					if (team == player) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	@Override
	public int free_slot() {
		if (getTeam() != null) {
			for (int i = 0; i < getTeam().length; i++) {
				if (getTeam()[i] == null) {
					return i;
				}
			}
		}
		return -1;
	}
	
	@Override
	public int members_active() {
		int count = 0;
		if (getTeam() != null) {
			for (Player team : getTeam()) {
				if (team != null) {
					count++;
				}
			}
		}
		return count;
	}
	
	@Override
	public Player closest_randomized_player() {
		List<Player> chosen = new ArrayList<Player>();
		if (getTeam() != null) {
			for (Player players : getTeam()) {
				if (players != null) {
					chosen.add(players);
				}
			}
		}
		return chosen.get(NumberUtils.random(chosen.size() - 1));
	}
}
