package com.hyperion.game.content.minigames;

public class GameBounds {

	private int lowestX, lowestY;
	private int highestX, highestY;
	
	public GameBounds (int lowestX, int lowestY, int highestX, int highestY) {
		this.lowestX = lowestX;
		this.lowestY = lowestY;
		this.highestX = highestX;
		this.highestY = highestY;
	}
	
	public int getLowestX() {
		return lowestX;
	}

	public int getLowestY() {
		return lowestY;
	}
	
	public int getHighestX() {
		return highestX;
	}
	
	public int getHighestY() {
		return highestY;
	}
}
