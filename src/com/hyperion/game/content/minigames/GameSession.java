package com.hyperion.game.content.minigames;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.player.Player;

public interface GameSession {

	public Player[] getTeam();
	
	public void start_game();
	
	public void end_game();
	
	public void insert_player(Player player);
	
	public void remove_player(Player player);
	
	public void handle_death(Entity deadEntity, Entity killer);
	
	public void handle_logout(Player player);
	
	public Tickable session_timer();
	
	public GameBounds starting_placement();
	
	public Location death_placement();
	
	public Location end_area();
	
	public boolean items_are_safe();
	
	public boolean player_ingame(Player player);
	
	public int free_slot();
	
	public int members_active();
	
	public Player closest_randomized_player();
}
