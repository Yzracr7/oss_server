package com.hyperion.game.content.minigames;

import com.hyperion.game.content.TeleportLocations;
import com.hyperion.game.content.minigames.clanwars.FreeForAll;
import com.hyperion.game.content.minigames.clanwars.session.WarSession;
import com.hyperion.game.content.minigames.duelarena.DuelArenaData;
import com.hyperion.game.content.minigames.fightcaves.FightCaveData;
import com.hyperion.game.content.minigames.pestcontrol.PestSession;
import com.hyperion.game.content.minigames.pestcontrol.PestWaiting;
import com.hyperion.game.content.miniquests.RoundBasedMiniquest;
import com.hyperion.game.content.miniquests.impl.FightCaveSession;
import com.hyperion.game.content.miniquests.impl.MageArena;
import com.hyperion.game.content.miniquests.impl.MonkeyMadness;
import com.hyperion.game.world.TeleportAreaLocations;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

public class Settings {

	public static final Location MINIQUEST_AREA = Location.create(3090, 3474, 0);

	/**
	 * Incase server dc, we correct their positions.
	 * 
	 * @param player
	 */
	public static void correctPlayerPosition(Player player) {
		Location loc = player.getLocation();
		Location setting = null;
		if (player.getVariables().getSessionName() != null) {
			switch (player.getVariables().getSessionName().toLowerCase()) {
			case "magearena":
				setting = MageArena.MAGE_ARENA;
				break;
			case "lunardiplomacy":
			case "recipefordisaster":
				setting = MINIQUEST_AREA;
				break;
			case "fightcaves":
				if (player.getVariables().getFightCavesWave() > 1) {
					//TODO include xAdjust and yAdjust -- when player logs store those values?
					FightCaveSession game_instance = new FightCaveSession(player, true, player.getVariables().getFightCavesWave() - 1);
					player.getPacketSender().sendMessage("Welcome back to the fight caves! (You are on wave: " + (player.getVariables().getFightCavesWave() -1) + ")");
					player.getVariables().setCurrentRoundMiniquest(game_instance);
					player.getVariables().setSessionName("fightcaves");
					player.getAttributes().set("cavesession", true);
					player.resetVariables();
					player.setCaveSession(game_instance);
					game_instance.enter(player);
					return;
				}
				setting = FightCaveData.DEATH_POSITION;
				break;
			case "clanwars":
				setting = TeleportLocations.CLAN_WARS_LOBBY;
				break;
			}
			player.getVariables().setSessionName(null);
		}
		if (player.getVariables().isAtJungleDemon()) {
			setting = Location.create(2465, 3489, 0);
		} else if (TeleportAreaLocations.inDuelArenas(loc)) {
			setting = DuelArenaData.DUEL_LOBBY;
		} else if (TeleportAreaLocations.inPestControlWaitingBoat(loc)) {
			setting = Location.create(2657, 2639, 0);
		} else if (TeleportAreaLocations.atPestControlGameArea(loc)) {
			setting = Location.create(2657, 2649, 0);
		}
		if (setting != null) {
			player.setLocation(setting);
		}
	}

	/**
	 * Handles players dying inside a minigame.
	 * 
	 * @param killer
	 *            - killer
	 * @param dead
	 *            - the entity that died.
	 */
	public static boolean handleDeath(Entity dead, Entity killer) {
		if (dead.isPlayer()) {
			Player deadPlayer = (Player) dead;
			deadPlayer.getPacketSender().sendConfig(428, 0); //poison
			Location loc = deadPlayer.getCentreLocation();
			if (deadPlayer.getVariables().getCurrentRoundMiniquest() != null) {
				deadPlayer.getVariables().getCurrentRoundMiniquest().end(deadPlayer, false, true, false);
				return true;
			} else if (deadPlayer.getVariables().isAtJungleDemon()) {
				MonkeyMadness.end(deadPlayer, (NPC) killer, false, true);
				return true;
			} else if (deadPlayer.getDuelSession() != null) {
				deadPlayer.getDuelSession().finishDuel(true, false);
				return true;
			}else if (deadPlayer.getClanWarsSession() != null) {
				deadPlayer.getClanWarsSession().declineWar(false);
				return true;
			} else if (TeleportAreaLocations.inPestControlWaitingBoat(loc)) {
				deadPlayer.resetVariables();
				deadPlayer.teleport(2657, 2649, 0);
				PestWaiting.removePlayer(deadPlayer, false);
				return true;
			} else if (TeleportAreaLocations.isInFreeForAllClanWars(loc)) {
				FreeForAll.handleDeath(deadPlayer, killer);
				return true;
			} else if (deadPlayer.getAttributes().isSet("clansession")) {
				WarSession warSession = deadPlayer.getAttributes().get("clansession");
				if(warSession != null) {
					warSession.handle_death(deadPlayer, killer);
					return true;
				}
			} else if(deadPlayer.getVariables().getGameSession() != null 
					&& deadPlayer.getVariables().getGameSession() instanceof PestSession) {
				PestSession pest_control = (PestSession) deadPlayer.getVariables().getGameSession();
				if (pest_control != null) {
					pest_control.handle_death(deadPlayer, killer);
					return true;
				}
			}
		} else if (dead.isNPC()) {
			NPC deadNPC = (NPC) dead;
			if (deadNPC.getAttributes().isSet("pestzombie")) {
				PestSession pest_control = (PestSession) deadNPC.getAttributes().get("pestsession");
				pest_control.handle_death(deadNPC, killer);
				return true;
			}
		}
		return false;
	}

	/**
	 * If player logs during miniquest/minigames also handles logout for
	 * clanchat
	 * 
	 * @param player
	 *            The player
	 * @return
	 */
	public static boolean handle_logouts(Player player) {
		
		if (player.getVariables().getCurrentRoundMiniquest() != null) {
			RoundBasedMiniquest game = player.getVariables().getCurrentRoundMiniquest();
			game.end(player, false, false, true);
			return true;
		} else if (player.getDuelSession() != null) {
			player.getDuelSession().logout();
			return true;
		} else if (player.getVariables().getGameSession() != null) {
			player.getVariables().getGameSession().handle_logout(player);
			return true;
		}
		PestWaiting.removePlayer(player, true);
		return false;
	}
}
