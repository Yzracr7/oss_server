package com.hyperion.game.content.minigames.barrows;

import com.hyperion.game.content.QuestTab;
import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.game.world.grounditems.GroundItem;
import com.hyperion.game.world.grounditems.GroundItemManager;
import com.hyperion.utility.NumberUtils;

import java.util.List;

/**
 * @author Nando
 * @author trees
 */
public class Barrows extends BarrowsData {

    /**
     * Entering brothers area.
     *
     * @param player
     * @return
     */
    public static boolean canDig(final Player player) {
        player.animate(830);
        for (int i = 0; i < MOUND_COORDS.length; i++) {
            if (player.getLocation().inArea(MOUND_COORDS[i][0], MOUND_COORDS[i][1], MOUND_COORDS[i][2], MOUND_COORDS[i][3])) {
                if (player.getLocation().getZ() == 0) {
                    final int i2 = i;
                    World.getWorld().submit(new Tickable(1) {
                        @Override
                        public void execute() {
                            this.stop();
                            player.getPacketSender().setMinimapStatus(2);
                            player.teleport(Location.create(STAIR_COORDS[i2][0], STAIR_COORDS[i2][1], 3));
                        }
                    });
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Leaving brothers area.
     *
     * @param player
     * @param object
     * @return
     */
    public static boolean exitArea(Player player, GameObject object) {
        if (object.getName() != null) {
            if (!object.getName().equalsIgnoreCase("staircase")) {
                return false;
            }
        }
        Location moundLocation;
        final int cryptIndex = getCryptIndex(player, object);
        if (cryptIndex == -1) {
            return false;
        }
        moundLocation = Location.create(MOUND_COORDS[cryptIndex][0] + NumberUtils.random(3), MOUND_COORDS[cryptIndex][1] + NumberUtils.random(3), 0);
        final Location teleLoc = moundLocation;
        player.getPacketSender().setMinimapStatus(0);
        player.teleport(teleLoc);
        return true;
    }

    /**
     * Spawning a brother.
     *
     * @param player
     * @param object
     */
    public static boolean spawnBrother(Player player, GameObject object) {
        switch (object.getId()) {
            case 20770: // Ahrim
            case 20722: // Guthan
            case 20771: // Karil
            case 20721: // Torag
            case 20772: // Verac
            case 20720: // Dharock
                break;
            default:
                return false;
        }
        int cryptIndex = getCryptIndex(player, object);
        if (cryptIndex == -1) {
            return false;
        }
        if (getBarrowBrothersKilled(player, cryptIndex)) {
            player.getPacketSender().sendMessage("You don't find anything.");
            return false;
        }
        for (int regionId : player.getMapRegionsIds()) {
            List<Integer> npcsIndexes = World.getWorld().getRegion(regionId, false).getNPCsIndexes();
            if (npcsIndexes == null) {
                continue;
            }
            for (Integer npcIndex : npcsIndexes) {
                NPC npc = World.getWorld().getNPC(npcIndex);
                if (npc == null)
                    continue;
                if (player.getVariables().getOwnedNPCS().contains(npc)) {
                    player.getPacketSender().sendMessage("You don't find anything.");
                    return false;
                }
            }
        }
        NPC npc = new NPC(BROTHER_ID[cryptIndex]);
        Location loc = player.getLocation();
        npc.setLocation(loc);
        npc.setLastKnownRegion(loc);
        player.getVariables().addOwnedNPC(npc);
        npc.getCombatState().startAttack(player);
        World.getWorld().addNPC(npc);
        player.getPacketSender().sendHintArrow(npc);
        return true;
    }

    public static void handleBrothersDeath(Player killer, NPC victim) {
        killer.getVariables().increaseBrothersKilled(1);
        killer.getPacketSender().sendHintArrow(null);
        if (victim.getId() == 1672) {
            killer.getVariables().setAhrimsKilled(true);
        } else if (victim.getId() == 1673) {
            killer.getVariables().setDharoksKilled(true);
        } else if (victim.getId() == 1674) {
            killer.getVariables().setGuthansKilled(true);
        } else if (victim.getId() == 1675) {
            killer.getVariables().setKarilsKilled(true);
        } else if (victim.getId() == 1676) {
            killer.getVariables().setToragsKilled(true);
        } else if (victim.getId() == 1677) {
            killer.getVariables().setVeracsKilled(true);
        }
        if (killer.getVariables().getBrothersKilled() == 6) {
            int rewardedPoints = 0;
            for (int i = 0; i < 6; i++) {
                rewardedPoints += NumberUtils.random(50);
            }
            handleBarrowsLoot(killer, victim);
            killer.getVariables().increaseBarrowPoints(rewardedPoints);
            killer.getPacketSender().sendMessage("<col=437C17>You received " + rewardedPoints + " Barrow points.");
            killer.getVariables().resetBrothersKilled();
        }
        updateKills(killer);
        QuestTab.refresh(killer);
    }

    private static void handleBarrowsLoot(Player player, NPC npc) {
        Achievements.increase(player, 38);
        for (int i = 0; i < 3; i++) {
            int index = (int) (Math.random() * ITEM_REWARDS.length);
            Item reward = new Item(ITEM_REWARDS[index], ITEM_REWARDS_AMOUNT[index]);
            GroundItem groundItem = new GroundItem(reward, npc, player);
            GroundItemManager.create(groundItem, player);
        }
        int piece_chance = NumberUtils.random(12);
        if (piece_chance == 0) {
            int reward = BARROW_REWARDS[(int) (Math.random() * BARROW_REWARDS.length)];
            Item piece = new Item(reward, 1);
            GroundItem groundItem = new GroundItem(piece, npc, player);
            GroundItemManager.create(groundItem, player);
        }
    }

    private enum BARROWS_BROTHER {
        AHRIM,
        DHAROK,
        GUTHAN,
        KARIL,
        TORAG,
        VERAC;
    }

    public static void updateKills(Player player) {
        for (BARROWS_BROTHER b : BARROWS_BROTHER.values()) {
            String name = b.name().substring(0, 1).toUpperCase() + b.name().substring(1).toLowerCase();

            Boolean killed = name.equals("Ahrim") ? player.getVariables().isAhrimsKilled()
                    : name.equals("Dharok") ? player.getVariables().isDharoksKilled()
                    : name.equals("Guthan") ? player.getVariables().isGuthansKilled()
                    : name.equals("Karil") ? player.getVariables().isKarilsKilled()
                    : name.equals("Torag") ? player.getVariables().isToragsKilled()
                    : name.equals("Verac") ? player.getVariables().isVeracsKilled() : false;

            player.getPacketSender().modifyText((killed ? "<col=006600>" : "<col=ff0000>") + name, 651, b.ordinal() + 1);
        }
    }
}
