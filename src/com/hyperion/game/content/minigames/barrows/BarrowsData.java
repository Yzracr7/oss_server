package com.hyperion.game.content.minigames.barrows;

import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;

public class BarrowsData {

    /**
     * The array indexes of the relevant barrow brother/crypt.
     */
    private static final int VERAC = 0;
    private static final int DHAROK = 1;
    private static final int AHRIM = 2;
    private static final int GUTHAN = 3;
    private static final int KARIL = 4;
    private static final int TORAG = 5;

    /**
     * The ID of the Barrow brother, to thier relevant cryptIndex.
     */
    public static final int[] BROTHER_ID = {
            1677, 1673, 1672, 1674, 1675, 1676
    };

    protected static final int[][] STAIR_COORDS = {
            {3578, 9706}, // Verac.
            {3556, 9718}, // Dharok.
            {3557, 9703}, // Ahrim.
            {3534, 9704}, // Guthan.
            {3546, 9684}, // Karil.
            {3568, 9683}, // Torag.
    };

    /**
     * The 'inArea' Coords of the mounds, x (SW), y (SW), x2 (NE), y2 (NE) (to form a square).
     */
    protected static final int[][] MOUND_COORDS = {
            {3553, 3294, 3558, 3300}, // Verac.
            {3573, 3296, 3577, 3300}, // Dharok.
            {3563, 3288, 3567, 3291}, // Ahrim.
            {3576, 3280, 3579, 3284}, // Guthan.
            {3564, 3274, 3567, 3277}, // Karil.
            {3552, 3281, 3555, 3284}, // Torag.
    };

    protected static int getCryptIndex(Player p, GameObject object) {
        if (p.getLocation().inArea(3567, 9701, 3580, 9711)) {
            return VERAC;
        } else if (p.getLocation().inArea(3548, 9709, 3561, 9721)) {
            return DHAROK;
        } else if (p.getLocation().inArea(3549, 9691, 3562, 9706)) {
            return AHRIM;
        } else if (p.getLocation().inArea(3532, 9698, 3546, 9710)) {
            return GUTHAN;
        } else if (p.getLocation().inArea(3544, 9677, 3559, 9689)) {
            return KARIL;
        } else if (p.getLocation().inArea(3563, 9680, 3577, 9694)) {
            return TORAG;
        }
        return -1;
    }

    protected static boolean getBarrowBrothersKilled(Player player, int index) {
        switch (index) {
            case 0://Verac
                return player.getVariables().isVeracsKilled();
            case 1://Dharok
                return player.getVariables().isDharoksKilled();
            case 2://Ahrim
                return player.getVariables().isAhrimsKilled();
            case 3://Guthan
                return player.getVariables().isGuthansKilled();
            case 4://Karils
                return player.getVariables().isKarilsKilled();
            case 5://Torags
                return player.getVariables().isToragsKilled();
        }
        return false;
    }

    protected static int[] ITEM_REWARDS = {
            555,//Water runes
            560,//Death runes.
            565,//Blood runes.
            562,//Chaos runes.
            558,//Mind runes.
            561,//Nature runes.
            4740,//Bolt rack.
            1113,//Rune chainbody.
            1201,//rune kiteshield
            1249,//dragon spear
            1149,//dragon med
            1305,//dragon longsword
            1093,//rune plateskirt
            1127,//rune platebody
            1163,//rune fullhelm
            9144,//rune bolts
            9143,//addy bolts
    };

    protected static int[] ITEM_REWARDS_AMOUNT = {
            196,//Water runes.
            51,//Death runes.
            38,//Blood runes.
            84,//Chaos runes.
            139,//Mind runes.
            85,//Nature runes.
            245,//Bolt racks
            1,//rune chainbody
            1,//rune kiteshield
            1,//dragon spear
            1,//dragon med
            1,//dragon longsword
            1,//rune plateskirt
            1,//rune platebody
            1,//rune fullhelm
            95,//rune bolts
            145,//addy bolts
    };

    /**
     * Barrow rewards.
     */
    protected static final int[] BARROW_REWARDS = {
            4757, 4759, 4753, 4755, // Verac's
            4736, 4738, 4734, 4732, // Karil's
            4745, 4747, 4749, 4751, // Torag's
            4708, 4710, 4712, 4714, // Ahrim's
            4716, 4718, 4720, 4722, // Dharok's
            4724, 4726, 4728, 4730, // Guthan's
    };

}
