package com.hyperion.game.content.minigames.castlewars;
/*package com.hyperion.game.content.minigames.castlewars;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.hyperion.game.Constants;
import com.hyperion.game.item.Item;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
import com.hyperion.game.world.gameobjects.GameObject;


*//**
 * A basic Castle Wars framework.
 * @author Canownueasy
 *//*

public class CastleWars {
	
	public CastleWars() {
		
	}
	
	*//**
	 * Lists of players in game, and on seperate teams
	 * Big thanks to Harry (Im Da Baws) for teaching me how to use array lists
	 *//*
	public static List<String> cwPlayers = new ArrayList<String>();
	public static List<String> zPlayers = new ArrayList<String>();
	public static List<String> sPlayers = new ArrayList<String>();
	
	*//**
	 * Handles Castle War's objects
	 * @param player Handles object clicking for this player.
	 * @param loc Location of the object.
	 * @param option The option on the object
	 * @param object Defined object (usually o)
	 *//*
	public void handleObjectClicking(final Player player, Location loc, int objectId, int option, final GameObject o) {
		if(option == 1) {
			switch(objectId) {
				case 7272: //Castle Wars leave portal
					player.teleport(Constants.DEATH_AREA);
					break;
				case 1519: //Castle Wars lobby door
					player.getPacketSender().sendMessage("You cannot leave Castle Wars this way!");
					break;
				case 1516: //Castle Wars lobby door
					player.getPacketSender().sendMessage("You cannot leave Castle Wars this way!");
					break;
				case 4388: //Enter Zamorak portal
					joinTeam(1, player);
					break;
				case 4408: //Enter Guthix portal
					joinGuthixPortal(player);
					break;
				case 4387: //Enter Saradomin portal
					joinTeam(2, player);
					break;
				case 4390: //Leave Zamorak portal
					leaveTeam(1, player);
					break;
				case 4389: //Leave Saradomin portal
					leaveTeam(2, player);
					break;
				case 4406: //Leave Game Saradomin portal
					leaveTeam(2, player);
					break;
				case 4458: //Bandage table
					if(player.tableWait == 0) {
						player.animate(Animation.create(881));
						player.getInventory().add(new Item(4049, 1));
						player.tableWait += 2;
					}
					break;
				case 6280: //Ladder in Saradomin spawn room
					player.teleport(Location.create(2429, 3075, 2));
					break;
				case 4471: //Trapdoor in Saradomin
					player.teleport(Location.create(2429, 3075, 1));
					break;
				case 4469: //Saradomin barrier
					if(player.getLocation().getX() == 2426 && player.getLocation().getY() == 3080) {
						player.teleport(Location.create(2426, 3081, 1));
					}
					if(player.getLocation().getX() == 2426 && player.getLocation().getY() == 3081) {
						player.teleport(Location.create(2426, 3080, 1));
					}
					if(player.getLocation().getX() == 2423 && player.getLocation().getY() == 3076) {
						player.teleport(Location.create(2422, 3076, 1));
					}
					if(player.getLocation().getX() == 2422 && player.getLocation().getY() == 3076) {
						player.teleport(Location.create(2423, 3076, 1));
					}
					break;
				case 354: //Crate (random lol)
				case 366:
					player.getPacketSender().sendMessage("You search the create... but find nothing!");
					break;
				case 4417: //Saradomin stairs (up)
					if(player.getLocation().getX() == 2425 && player.getLocation().getY() == 3077) {
						player.teleport(Location.create(2425, 3074, 3));
					}
					if(player.getLocation().getX() == 2426 && player.getLocation().getY() == 3074) {
						player.teleport(Location.create(2425, 3077, 2));
					}
					break;
				case 4902: //Capture Saradomin Flag
					if(player.getEquipment().isSlotUsed(Equipment.SLOT_WEAPON)) {
						player.getPacketSender().sendMessage("Remove your weapon before taking the flag!");
						return;
					}
					if(player.getInventory().hasRoomFor(player.getEquipment().getById(Equipment.SLOT_WEAPON), Inventory.SIZE)) {
						player.getInventory().add(player.getEquipment().getById(Equipment.SLOT_WEAPON));
						player.getEquipment().add(new Item(4037));
						for(Player all : World.getWorld().getPlayers()) {
							if(cwPlayers.contains(all.getName())) {
								
							}
						}
					}
					break;
			}
		}
		if(option == 2) {
			switch(objectId) {
			
			}
		}
		if(option == 3) {
			switch(objectId) {
			
			}
		}
	}
	
	*//**
	 * The amount of players on a team.
	 * @param teamID The team's ID
	 * @return The amount of players on the team.
	 *//*
	public int getPlayers(int teamID) {
		if(teamID == 1) {
			return zPlayers.size();
		} else if(teamID == 2) {
			return sPlayers.size();
		} else {
			return 0;
		}
	}
	
	*//**
	 * Gets the amount of players in Castle Wars.
	 * @return The count of players in the cwPlayers list.
	 *//*
	public int getCWPlayers() {
		return cwPlayers.size();
	}
	
	*//**
	 * Gets the amount of players on the Zamorak team.
	 * @return The count of players in the zPlayers list.
	 *//*
	public int zPlayers() {
		return zPlayers.size();
	}
	
	*//**
	 * Gets the amount of players on the Saradomin team.
	 * @return The count of players in the sPlayers list.
	 *//*
	public int sPlayers() {
		return sPlayers.size();
	}
	
	*//**
	 * Gets the specified team's name.
	 * @param teamID The team's ID
	 * @return The team's name.
	 *//*
	public String getTeamName(int teamID) {
		if(teamID == 1) {
			return "Zamorak";
		}
		if(teamID == 2) {
			return "Saradomin";
		}
		return null;
	}
	
	*//**
	 * Checks if a player is on a team.
	 * @param teamID The team's ID
	 * @param player Who we're checking
	 *//*
	public boolean isOnTeam(int teamID, Player player) {
		if(teamID == 1) {
			if(zPlayers.contains(player.getName())) {
				return true;
			}
		}
		if(teamID == 2) {
			if(sPlayers.contains(player.getName())) {
				return true;
			}
		}
		return false;
	}
	
	*//**
	 * Checks if a player is in Castle Wars.
	 * @param player Who we're checking
	 *//*
	public static boolean isInCW(Player player) {
		if(cwPlayers.contains(player.getName())) {
			return true;
		} else {
			return false;
		}
	}
	
	*//**
	 * Does the team have the maximum amount of players?
	 * @param teamID The team's ID
	 * @return If the team has max players or not.
	 *//*
	public boolean hasMaxPlayers(int teamID) {
		if(teamID == 1) {
			if(getPlayers(1) >= 25) {
				return true;
			} else {
				return false;
			}
		}
		if(teamID == 2) {
			if(getPlayers(2) >= 25) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}
	
	*//**
	 * Tile of the waiting rooms
	 *//*
	public Location ZAMORAK_ROOM = Location.create(2421 + r.nextInt(3), 9524 + r.nextInt(3), 0);
	public Location SARADOMIN_ROOM = Location.create(2381 + r.nextInt(3), 9489 + r.nextInt(3), 0);
	
	*//**
	 * Location of the Clan Wars lobby
	 *//*
	public Location CASTLE_WARS_LOBBY = Location.create(2440 + r.nextInt(3), 3090 + r.nextInt(3), 0);
	
	*//**
	 * Joins a team.
	 * @param teamID The team's ID
	 * @param player The player who is joining
	 *//*
	public void joinTeam(final int teamID, final Player player) {
		if(player.getEquipment().isSlotUsed(Equipment.SLOT_HELM) && player.getEquipment().isSlotUsed(Equipment.SLOT_CAPE)) {
			player.getPacketSender().sendMessage("You cannot be wearing headgear or capes.");
			return;
		}
		if(player.getEquipment().isSlotUsed(Equipment.SLOT_HELM)) {
			player.getPacketSender().sendMessage("You cannot be wearing headgear.");
			return;
		}
		if(player.getEquipment().isSlotUsed(Equipment.SLOT_CAPE)) {
			player.getPacketSender().sendMessage("You cannot be wearing a cape.");
			return;
		}
		if(teamID == 1) {
			if(hasMaxPlayers(1)) {
				player.getPacketSender().sendMessage("The " + getTeamName(1) + " team already has enough players.");
				return;
			}
			player.teleport(ZAMORAK_ROOM);
			player.getEquipment().add(new Item(4515));
			player.getEquipment().add(new Item(4516));
			cwPlayers.add(player.getName().toString());
			zPlayers.add(player.getName().toString());
		}
		if(teamID == 2) {
			if(hasMaxPlayers(2)) {
				player.getPacketSender().sendMessage("The " + getTeamName(2) + " team already has enough players.");
				return;
			}
			player.teleport(SARADOMIN_ROOM);
			player.getEquipment().add(new Item(4513));
			player.getEquipment().add(new Item(4514));
			cwPlayers.add(player.getName().toString());
			sPlayers.add(player.getName().toString());
		}
	}
	
	*//**
	 * Joins the Guthix portal.
	 * @param player The player joining.
	 * @param teamID The team's ID
	 *//*
	public void joinGuthixPortal(final Player player) {
		if(hasMaxPlayers(1) && hasMaxPlayers(2)) {
			player.getPacketSender().sendMessage("Both teams already have enough players.");
			return;
		}
		if(getPlayers(1) == getPlayers(2)) {
			player.getPacketSender().sendMessage("Guthix denies access as both teams are balanced.");
			return;
		}
		if(getPlayers(1) < getPlayers(2)) {
			joinTeam(1, player);
		} else if(getPlayers(1) > getPlayers(2)) {
			joinTeam(2, player);
		}
	}
	
	*//**
	 * Removes a player from the waiting room.
	 * @param teamID The team's ID
	 * @param player The player who is being removed.
	 *//*
	public void leaveTeam(int teamID, Player player) {
		if(teamID == 1) {
			zPlayers.remove(player.getName());
		}
		if(teamID == 2) {
			sPlayers.remove(player.getName());
		}
		cwPlayers.remove(player.getName());
		player.teleport(CASTLE_WARS_LOBBY);
		Equipment.unequipItem(player, 4513, slot, false)
		player.getEquipment().remove(new Item(4513));
		player.getEquipment().remove(new Item(4514));
		player.getEquipment().remove(new Item(4515));
		player.getEquipment().remove(new Item(4516));
		player.getEquipment().swap(fromSlot, toSlot);
	}
	
	*//**
	 * Checks if the player is in the castle wars area upon login, if they are, teleport them to the Castle War's lobby.
	 * @param player The player who is being checked.
	 *//*
	public static void checkForCastleWarsLogin(final Player player) {
		if(player.getLocation().castleWars()) {
			player.setTeleportTarget(CASTLE_WARS_LOBBY);
		}
	}
	
	*//**
	 * Randoms
	 *//*
	public static final Random r = new Random();
	public static final int rInt(int value) {
		return Misc.random(value);
	}
}*/