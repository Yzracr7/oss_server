package com.hyperion.game.content.minigames.clanwars;

public class ClanWarsData {

	public final int OPTIONS_INTERFACE = 602;
	public final int ACCEPT = 223, CLOSE = 6;
	
	public final String WHITE = "<col=ffffff>";

	public enum Groups {
		GAME_END(21, 9, new Rules[]{
				Rules.LAST_TEAM_STANDING,
				Rules.KILLS_25,
				Rules.KILLS_50,
				Rules.KILLS_100,
				Rules.KILLS_200,
				Rules.KILLS_500,
				Rules.KILLS_1000,
				Rules.KILLS_5000,
				Rules.KILLS_10000}, 0),
		ARENA(66, 6, new Rules[] {Rules.PLATEAU,
				Rules.WASTELAND,
				Rules.SYLVAN_GLADE,
				Rules.FORESAKEN_QUARRY,
				Rules.TURRETS,
				Rules.GHASTLY_SWAMP}, 1),
		MISC(97, 4, new Rules[] {Rules.IGNORE_FREEZING,
				Rules.LOSE_ITEMS_ON_DEATH, //PJ_TIMER
				Rules.SINGLE_SPELLS,
				Rules.F2P_MODE}, //ALLOW_TRIDENT
				2),
		STRAGGLERS(137, 2, new Rules[] {Rules.KILL_EM_ALL,
				Rules.IGNORE_5}, 3),
		MELEE(149, 2, new Rules[] {Rules.MELEE_ON, 
				Rules.MELEE_OFF}, 4),
		RANGING(157, 2, new Rules[] {Rules.RANGING_ON,
				Rules.RANGING_OFF}, 5),
		MAGIC(165, 4, new Rules[] {Rules.MAGIC_ON,
				Rules.STANDARD_SPELLS,
				Rules.BINDING_ONLY,
				Rules.MAGIC_OFF}, 6),
		FOOD(181, 2, new Rules[] {Rules.FOOD_ON,
				Rules.FOOD_OFF}, 7),
		DRINKS(189, 2, new Rules[] {Rules.DRINKS_ON,
				Rules.DRINKS_OFF}, 8),
		SPECIALS(197, 3, new Rules[] {Rules.SPECIALS_ON,
				Rules.NO_STAFF_OF_THE_DEAD,
				Rules.SPECIALS_OFF}, 9),
		PRAYER(209, 3, new Rules[] {Rules.PRAYER_ON,
				Rules.NO_OVERHEADS,
				Rules.PRAYER_OFF}, 10);

		private int start, optionsAmount, index;
		private Rules[] rules;
		
		public int getStart() {
			return start;
		}
		
		public int getOptionsAmount() {
			return optionsAmount;
		}
		
		public Rules[] getRules() {
			return rules;
		}
		
		 public int getIndex() {
				return index;
		 }

		private Groups(int start, int optionsAmount, Rules[] rules, int index) {
			this.start = start;
			this.optionsAmount = optionsAmount;
			this.rules = rules;
			this.index = index;
		}
	}
	
	public Groups forIndex(int index) {
		for(Groups g : Groups.values()) {
			if(g.getIndex() == index)
				return g;
		}
		return null;
	}
	
	public static AreaType areaForRule(Rules rule) {
		switch(rule.name().toLowerCase()) {
		case "plateau":
			return AreaType.PLATEAU;
		}
		return AreaType.PLATEAU;
	}
	
	public enum Rules {
		LAST_TEAM_STANDING("Last team standing"),
		KILLS_25("25 kills"),
		KILLS_50("50 kills"),
		KILLS_100("100 kills"),
		KILLS_200("200 kills"),
		KILLS_500("500 kills"),
		KILLS_1000("1000 kills"),
		KILLS_5000("5000 kills"),
		KILLS_10000("10000 kills"),
		
		PLATEAU("Plateau"),
		WASTELAND("Wasteland"),
		SYLVAN_GLADE("Sylvan Glade"),
		FORESAKEN_QUARRY("Foresaken Quarry"),
		TURRETS("Turrets"),
		GHASTLY_SWAMP("Ghastly Swamp"),
		
		IGNORE_FREEZING("Ignore freezing"),
		LOSE_ITEMS_ON_DEATH("Lose items on death"),
		F2P_MODE("F2P mode"),
		//PJ_TIMER("PJ Timer"),
		SINGLE_SPELLS("Single spells"),
		//ALLOW_TRIDENT("Allow Trident in PvP"),
		
		MELEE_ON("Allowed"),
		MELEE_OFF("Disabled"),
		
		RANGING_ON("Allowed"),
		RANGING_OFF("Disabled"),
	
		MAGIC_ON("Allowed"),
		STANDARD_SPELLS("Standard spells"),
		BINDING_ONLY("Binding only"),
		MAGIC_OFF("Disabled"),
		
		FOOD_ON("Allowed"),
		FOOD_OFF("Disabled"),
		
		DRINKS_ON("Allowed"),
		DRINKS_OFF("Disabled"),
		
		SPECIALS_ON("Allowed"),
		NO_STAFF_OF_THE_DEAD("No Staff of the Dead"),
		SPECIALS_OFF("Disabled"),
		
		PRAYER_ON("Allowed"),
		NO_OVERHEADS("No overheads"),
		PRAYER_OFF("Disabled"),
		
		KILL_EM_ALL("Kill 'em all"),
		IGNORE_5("Ignore 5")
		;
		
		private String text;
		
		public String getText() {
			return text;
		}
		
		private Rules(String text) {
			this.text = text;
		}
	}
}
