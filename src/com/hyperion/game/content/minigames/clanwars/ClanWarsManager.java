package com.hyperion.game.content.minigames.clanwars;

import java.util.ArrayList;
import java.util.List;

import com.hyperion.cache.definitions.CachedItemDefinition;
import com.hyperion.game.Constants;
import com.hyperion.game.content.clanchat.ClanChat;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.content.minigames.clanwars.session.WarSession;
import com.hyperion.game.item.Item;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.player.Player;


public class ClanWarsManager {
	
	private static List<WarSession> activeWars = new ArrayList<WarSession>();

	public static void handleEnterPortal(Player player) {
		if(!player.getAttributes().isSet("clan_chat_id")) {
			player.getPacketSender().sendChatboxDialogue(true, "You are not currently in a clan.");
			return;
		}
		int currentClan = player.getAttributes().getInt("clan_chat_id");
		ClanChat clan = ClanChat.getClanById(currentClan);
		String leaderName = clan.getOwner();
		WarSession activeWar = getSessionByLeader(leaderName);
		if(activeWar != null) {
			int team = getTeamByLeader(activeWar, leaderName);
			if(team != -1) {
				if(activeWar.getMiscRules()[3]) {
					//f2p - check items
					if(hasMembersItem(player))
						return;
				}
				if(activeWar.isDangerous() && !player.getAttributes().isSet("warnedDangerous")) {
					player.getPacketSender().sendChatboxDialogue(true, "<col=E30000>WARNING:</col>", "This war is set to dangerous mode.", "Should you die, you will lose your items.", "You have been warned!");
					player.getPacketSender().sendMessage("<col=E30000>WARNING: This war is set to dangerous mode. You will lose your items should you die.");
					player.getPacketSender().sendMessage("<col=E30000>You have been warned!");
					player.getAttributes().set("warnedDangerous", true);
					return;
				}
				if(player.getAttributes().isSet("isMonkey")) {
					player.getPacketSender().sendChatboxDialogue(true, "Monkeys don't join wars.");
					return;
				}
				joinWar(activeWar, player, team);
			} else {
				player.getPacketSender().sendChatboxDialogue(true, "You can't do that right now. Try again in a bit.");
			}
		} else {
			player.getPacketSender().sendChatboxDialogue(true, "Your clan does not have an active war at the moment.", "Your leader can initiate one by challenging another clan leader.");
			return;
		}

	}
	
	public static boolean hasMembersItem(Player player) {
		for(Item i: player.getInventory().getItems()) {
			if(i == null)
				continue;
			CachedItemDefinition cacheDef = CachedItemDefinition.forId(i.getId());
			if(cacheDef.isMembers() || i.getId() >= 18600) {
				player.getPacketSender().sendChatboxDialogue(true, "<col=E30000>This item is not allowed in the F2P arena:</col>" , "" + i.getDefinition().getName());
				player.getPacketSender().sendMessage("This item is not allowed in the F2P Arena: " + i.getDefinition().getName());
				return true;
			}
		}
		for(Item i: player.getEquipment().getItems()) {
			if(i == null)
				continue;
			CachedItemDefinition cacheDef = CachedItemDefinition.forId(i.getId());
			if(cacheDef.isMembers() || i.getId() >= 18600) {
				player.getPacketSender().sendChatboxDialogue(true, "<col=E30000>This item is not allowed in the F2P arena:</col>" , "" + i.getDefinition().getName());
				player.getPacketSender().sendMessage("This item is not allowed in the F2P Arena: " + i.getDefinition().getName());
				return true;
			}
		}
		return false;
	}

	public static void handleLeavePortal(Player player, int objectId) {
		if(!player.getAttributes().isSet("clansession")) {
			player.teleport(Location.create(6471, 3675, 0));
		}
		WarSession war = player.getAttributes().get("clansession");
		if(war != null) {
			if(objectId == 38696) {
				war.leave_to_spectate(player);
			} else
				DialogueManager.handleMisc(player, -3, war.canRejoin());
				//war.leave_portal(player, objectId == 38696 ? false : true, false);
		} else {
			player.getAttributes().remove("clansession");
			player.teleport(Location.create(6471, 3675, 0));
		}
	}
	
	public static void handleViewingOrb(Player player, int buttonId) {
		if(!player.getAttributes().isSet("clansession")) {
			player.teleport(Location.create(6471, 3675, 0));
		}
		WarSession war = player.getAttributes().get("clansession");
		if(war != null) {
			war.viewing_orb(player, buttonId);
		} else {
			player.getAttributes().remove("clansession");
			player.teleport(Location.create(6471, 3675, 0));
		}
	}
	
	private static void joinWar(WarSession activeWar, Player player, int team) {
		if(activeWar != null) {
			//if(activeWar.hasWarStarted()) {
			//	player.getPacketSender().sendChatboxDialogue(true, "This war has already started.");
			//	return;
			//}
			boolean spectate = false;
			if(activeWar.hasWarStarted()) {
				spectate = true;
				player.getAttributes().set("spectator", true);
				if(!activeWar.canRejoin()) {
					player.getPacketSender().sendMessage("You can spectate or enterInstancedRoom the battle through the purple portal.");
				} else {
					player.getPacketSender().sendMessage("This war is set to end when one team is eliminated. You can only spectate.");
				}
			}
			if(!spectate) {
				if(team == 1)
					activeWar.getTeam1().add(player);
				else 
					activeWar.getTeam2().add(player);
			} else {
				if(team == 1)
					activeWar.getSpectators1().add(player);
				else 
					activeWar.getSpectators2().add(player);
			}
			

			activeWar.insert_player(player);
			player.getVariables().setGameSession(activeWar);
		} else {
			player.getPacketSender().sendChatboxDialogue(true, "You can't do that right now. Try again later.");
			System.out.println("[FATAL] Could not join war as it was null.");
		}
	}

	public static int getTeamByLeader(WarSession war, String leaderName) {
		if(war == null)
			return -1;
		return war.getLeader1().equalsIgnoreCase(leaderName) ? 1 : 2;
	}
	
	public static boolean addNewWar(WarSession war) {
		if (sessionExists(war)) {
			System.out.println("[FATAL] War already exists");
			return false;
		}
		activeWars.add(war);
		return true;
	}
	
	public static void removeWar(WarSession war) {
		activeWars.remove(war);
	}
	
	public static boolean sessionExists(WarSession war) {
		if(activeWars.contains(war)) {
			return true;
		}
		return false;
	}
	
	public static WarSession getSessionByLeader(String leader) {
		for (WarSession war : activeWars) {
			if(Constants.DEBUG_MODE)
				System.out.println("Checking warSession, leaders: " + war.getLeader1() + ", " + war.getLeader2());
			if(war.getLeader1().equalsIgnoreCase(leader)
					|| war.getLeader2().equalsIgnoreCase(leader)) {
				return war;
			}
		}
		return null;
	}

	public static void sendViewingOrb(Player player) {
		if(!player.getAttributes().isSet("clansession")) {
			player.teleport(Location.create(6471, 3675, 0));
		}
		WarSession war = player.getAttributes().get("clansession");
		if(war != null) {
			if(war.hasWarStarted())
				player.getPacketSender().displayInventoryInterface(374);
		} else {
			player.getAttributes().remove("clansession");
			player.teleport(Location.create(6471, 3675, 0));
		}
	}
}