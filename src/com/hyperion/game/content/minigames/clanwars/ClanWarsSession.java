package com.hyperion.game.content.minigames.clanwars;

import java.util.ArrayList;
import java.util.List;

import com.hyperion.game.Constants;
import com.hyperion.game.content.clanchat.ClanChat;
import com.hyperion.game.content.clanchat.ClanChat.Member;
import com.hyperion.game.content.minigames.clanwars.session.WarSession;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;

public class ClanWarsSession extends ClanWarsData {

	public enum WAR_STATE {
		UNACCEPTED, ACCEPTED
	}

	private Player player;
	private Player p2;
	private WAR_STATE state = WAR_STATE.UNACCEPTED;
	private Rules[] groupRules = new Rules[11]; //11 rules
	private boolean[] miscRules = new boolean[4];
	
	public ClanWarsSession(Player p, Player p2) {
		this.player = p;
		this.p2 = p2;
		openInterface();
	}
	
	public Rules[] getGroupRules() {
		return groupRules;
	}
	
	public boolean[] getMiscRules() {
		return miscRules;
	}

	private void openInterface() {
		player.getPacketSender().displayInterface(OPTIONS_INTERFACE);
		player.getPacketSender().modifyText("Clan Wars Setup: Challenging " + p2.getName(), OPTIONS_INTERFACE, 11);
		player.getPacketSender().closeChatboxInterface();
		player.getVariables().setCloseInterfacesEvent(new Runnable() {
			@Override
			public void run() {
				if(!(player.getVariables().getGameSession() instanceof WarSession))
					declinedWar();
			}
		});
	}

	
	private Rules getRule(Groups group, int button, int optionNumber) {
		if(optionNumber > group.getRules().length-1) {
			System.out.println("[FATAL] Out of bounds for clan wars rules - Group: " + group.name() + ", option: " + optionNumber + ", length: " + group.getRules().length);
			return null;
		}
		return group.getRules()[optionNumber];
	}
	/*
	 * Handles all inter buttons for the Clan Wars Options inter(602).
	 */
	public void handleInterface(Player player, int button) {
		if (button == CLOSE) {
			declineWar(true);
			return;
		}
		
		if(p2 == null || p2.getClanWarsSession() == null)
			declinedWar();
		
		final WAR_STATE other_state = p2.getClanWarsSession().getState();
		if (button == ACCEPT) {
			if(!ClanChat.isClanLeader(player)) {
				declinedWar();
				return;
			}
			if(other_state == WAR_STATE.ACCEPTED) {
				acceptWar();
			} else {
				setState(WAR_STATE.ACCEPTED);
				p2.getPacketSender().sendChatboxDialogue(false, "Other player has accepted the options.");
				player.getPacketSender().sendChatboxDialogue(false, "Waiting for other player to accept..");
			}
			return;
		} else {
			Groups group = null;
			int toggleStart = -1;
			
			boolean findAllChildren = false;

			for (Groups configGroup : Groups.values()) {
				
				findAllChildren = configGroup == Groups.MISC;
				
				if(button >= configGroup.getStart()+ configGroup.getOptionsAmount() && button <= configGroup.getStart()+ (configGroup.getOptionsAmount() * (findAllChildren ? 4 : 2)) - 1) {
					group = configGroup;
					toggleStart = configGroup.getStart()+ configGroup.getOptionsAmount();
				}
				
			}
			if (group == null)
				return;
			
			if(state == WAR_STATE.ACCEPTED || other_state == WAR_STATE.ACCEPTED) {
				setState(WAR_STATE.UNACCEPTED);
				p2.getClanWarsSession().setState(WAR_STATE.UNACCEPTED);
				player.getPacketSender().closeChatboxInterface();
				p2.getPacketSender().closeChatboxInterface();
			}
			
			
			int containerStart = toggleStart + group.getOptionsAmount();
			int optionNumber = -1;
			int buttonActivated = -1;
			int ruleGroup = group.ordinal();
			Rules rule = null;
			if(group != Groups.MISC) {
				optionNumber = button == toggleStart ? 0 : button - toggleStart;
				buttonActivated = containerStart + optionNumber;
				rule = getRule(group, button, optionNumber);
				
				for (int i = containerStart; i < containerStart + group.getOptionsAmount(); i++) {
					player.getPacketSender().sendInterfaceConfig(OPTIONS_INTERFACE, i, false);
					p2.getPacketSender().sendInterfaceConfig(OPTIONS_INTERFACE, i, false);
				}
				player.getPacketSender().sendInterfaceConfig(OPTIONS_INTERFACE, buttonActivated, true);
				p2.getPacketSender().sendInterfaceConfig(OPTIONS_INTERFACE, buttonActivated, true);
				groupRules[ruleGroup] = rule;
				p2.getClanWarsSession().getGroupRules()[ruleGroup] = rule;
				
				if(Constants.DEBUG_MODE)
					player.getPacketSender().sendMessage("Rule for group " + ruleGroup + " set to " + rule.toString());

			} else {
				/*
				 * Miscellaneous -- on or off seperately
				 */
				boolean isToggledConfig = button >= containerStart;
				if(!isToggledConfig) {
					//On
					optionNumber = button == toggleStart ? 0 : button - toggleStart;
					buttonActivated = containerStart + optionNumber;
				} else {
					//Off
					optionNumber = button == containerStart ? 0 : button - containerStart;
					buttonActivated = containerStart + optionNumber - group.getOptionsAmount();
				}
				
				player.getPacketSender().sendInterfaceConfig(OPTIONS_INTERFACE, buttonActivated, isToggledConfig ? false : true);
				p2.getPacketSender().sendInterfaceConfig(OPTIONS_INTERFACE, buttonActivated, isToggledConfig ? false : true);
				
				if(isToggledConfig)
					optionNumber = optionNumber - 4;
				if(Constants.DEBUG_MODE)
					System.out.println("OPTION: " + optionNumber);
				miscRules[optionNumber] = !miscRules[optionNumber];
				p2.getClanWarsSession().getMiscRules()[optionNumber] = miscRules[optionNumber];
				
				if(Constants.DEBUG_MODE)
					player.getPacketSender().sendMessage("Misc rule number " + optionNumber + " set to " + miscRules[optionNumber]);
				
				//groupRules[ruleGroup] = rule;
				//player.getPacketSender().sendMessage("Rule for group " + ruleGroup + " set to " + rule.toString());
			}
		}
	}

	public void acceptWar() {
		if(miscRules[3]) {
			boolean cancel = false;
			if(ClanWarsManager.hasMembersItem(player)) {
				cancel = true;
				p2.getPacketSender().sendMessage("Your opponent has items that are not allowed in the F2P arena.");
			} else if (ClanWarsManager.hasMembersItem(p2)) {
				cancel = true;
				player.getPacketSender().sendMessage("Your opponent has items that are not allowed in the F2P arena.");
			}
			if(player.getAttributes().isSet("isMonkey") || p2.getAttributes().isSet("isMonkey")) {
				player.getPacketSender().sendMessage("Monkeys cannot speak wars.");
				p2.getPacketSender().sendMessage("Monkeys cannot speak wars.");
				cancel = true;
			}
			if(cancel) {
				setState(WAR_STATE.UNACCEPTED);
				p2.getClanWarsSession().setState(WAR_STATE.UNACCEPTED);
				player.getPacketSender().closeChatboxInterface();
				p2.getPacketSender().closeChatboxInterface();
				return;
			}
		}
		int i = 0;
		for(Rules rule : groupRules) {
			if(rule == null) {
				Rules defaultRule = forIndex(i).getRules()[0];
				if(defaultRule != null) {
					groupRules[i] = defaultRule;
				} else {
					System.out.println("[FATAL] Could not find default clan wars rule " + i);
					declineWar(false);
					return;
				}
			}
			i++;
		}
		
		if(p2.getClanWarsSession() != null) {
			if(ClanWarsManager.getSessionByLeader(player.getName()) == null
					&& ClanWarsManager.getSessionByLeader(p2.getName()) == null) {
				int clanId1 = player.getAttributes().getInt("clan_chat_id");
				ClanChat clan1 = ClanChat.getClanById(clanId1);
				int clanId2 = p2.getAttributes().getInt("clan_chat_id");
				ClanChat clan2 = ClanChat.getClanById(clanId2);
				WarSession war_instance = new WarSession(player, p2, groupRules, miscRules, clan1.getName(), clan2.getName());
				war_instance.insert_player(player);
				war_instance.insert_player(p2);
				ClanWarsManager.addNewWar(war_instance);
				player.getVariables().setGameSession(war_instance);
				p2.getVariables().setGameSession(war_instance);
				war_instance.start_game();
				player.getPacketSender().sendMessage("<col=66008F>The battle begins in 2 minutes. Other can join through the purple portal!");
				p2.getPacketSender().sendMessage("<col=66008F>The battle begins in 2 minutes. Other can join through the purple portal!");
			
				List<Member> membersList1 = new ArrayList<Member>(clan1.getMembers());
				List<Member> membersList2 = new ArrayList<Member>(clan2.getMembers());
				for(int num = 0; num < 2; num++) {
					List<Member> membersList= num == 0 ? membersList1 : membersList2;
					String name = num == 0 ? clan1.getName() : clan2.getName();
					for (Member members : membersList) {
						Player players = World.getWorld().find_player_by_name(members.getName());
						if(players == null || (num == 0 && players == player) || (num == 1 && players == p2))
							continue;
						players.getPacketSender().sendChatboxDialogue(true, "Your clan leader has initiated a war against:", "<col=66008F>" + name + "</col>", "The battle will commence in two minutes,", "You can join the battle by entering the purple portal!");
						players.getPacketSender().sendMessage("<col=66008F>Your clan leader has initiated a war against: " + name);
						players.getPacketSender().sendMessage("<col=66008F>The battle will begin in 2 minutes. You can enterInstancedRoom through the purple portal!");
					}
				}
			} else {
				player.getPacketSender().sendMessage("You or your opponent are already in an active war.");
				p2.getPacketSender().sendMessage("You or your opponent are already in an active war.");
			}
		} else {
			player.getPacketSender().sendMessage("Your opponent has declined the Clan war options.");
		}
		player.setClanWarsSession(null);
		p2.setClanWarsSession(null);
		player.getPacketSender().softCloseInterfaces();
		p2.getPacketSender().softCloseInterfaces();
	}

	public void declineWar(boolean button_clicked) {
		if (button_clicked) {
			player.getVariables().setCloseInterfacesEvent(null);
			p2.getVariables().setCloseInterfacesEvent(null);
			declinedWar();
		}
		p2.getInterfaceSettings().closeInterfaces(false);
		player.getInterfaceSettings().closeInterfaces(false);
	}

	private void declinedWar() {
		player.getPacketSender().sendMessage("You decline the war options.");
		player.setClanWarsSession(null);
		if (p2.getClanWarsSession() != null) {
			p2.getPacketSender().sendMessage("Other player declined the war options.");
			p2.setClanWarsSession(null);
			p2.getInterfaceSettings().closeInterfaces(false);

		}
	}

	public void logout() {
		declinedWar();
	}

	public boolean hasAccepted() {
		return state.equals(WAR_STATE.ACCEPTED);
	}

	public Player getPlayer2() {
		return p2;
	}

	public WAR_STATE getState() {
		return state;
	}

	public void setState(WAR_STATE state) {
		this.state = state;
	}
}
