package com.hyperion.game.content.minigames.clanwars;

import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.player.Player;

public class FreeForAll {
	
	/**
	 * Handles the death of a player in the free for all miniigame
	 * 
	 * @param dead
	 *            The player that died
	 * @param killer
	 *            The player that killed the dead player.
	 */
	public static void handleDeath(Player dead, Entity killer) {
		dead.resetVariables();
		dead.teleport(2815, 5511, 0);
	}

}
