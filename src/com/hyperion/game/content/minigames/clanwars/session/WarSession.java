package com.hyperion.game.content.minigames.clanwars.session;

import java.util.ArrayList;
import java.util.List;

import com.hyperion.game.Constants;
import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.content.minigames.AbstractGameSession;
import com.hyperion.game.content.minigames.GameBounds;
import com.hyperion.game.content.minigames.clanwars.*;
import com.hyperion.game.content.minigames.clanwars.ClanWarsData.Rules;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.game.world.grounditems.GroundItem;
import com.hyperion.game.world.region.MapBuilder;
import com.hyperion.game.world.region.Region;

public class WarSession extends AbstractGameSession {

	private Location baseLocation = null;
	
	private String leader1, clanName1;
	private String leader2, clanName2;
	
	private List<Player> team1 = new ArrayList<Player>();
	private List<Player> team2 = new ArrayList<Player>();
	
	private List<Player> spectators1 = new ArrayList<Player>();
	private List<Player> spectators2 = new ArrayList<Player>();
	
	private List<String> defeatedPlayers1 = new ArrayList<String>();
	private List<String> defeatedPlayers2 = new ArrayList<String>();

	//private List<GameObject> walls = new ArrayList<GameObject>();
	
	private Rules[] warRules = new Rules[11]; //11 rules
	private boolean[] miscRules = new boolean[4];
	
	private boolean warActive = false;
	
	private int countdown, killLimit = -1, winningTeam = -1;
	
	private boolean dangerous = false;
	
	private AreaType area;
	
	private boolean messageSent = false;
	
	private boolean ignore5 = false;
	
	private boolean canRejoin = false;
	
	private int killcount1 = 0, killcount2 = 0;
	
	public WarSession(Player clanLeader1, Player clanLeader2, ClanWarsData.Rules[] rules, boolean[] miscRules, String clanName1, String clanName2) {
		this.team1.add(clanLeader1);
		this.team2.add(clanLeader2);
		this.leader1 = clanLeader1.getName();
		this.leader2 = clanLeader2.getName();
		this.warRules = rules;
		this.miscRules = miscRules;
		this.countdown = Constants.DEBUG_MODE ? 12 : 120; //120
		this.clanName1 = clanName1;
		this.clanName2 = clanName2;
		this.dangerous = miscRules[1];
		this.ignore5 = rules[3] == ClanWarsData.Rules.IGNORE_5;
		area = ClanWarsData.areaForRule(warRules[1]);
		int width = (area.getNorthEastTile().getX() - area.getSouthWestTile().getX()) / 8 + 2;
		int height = (area.getNorthEastTile().getY() - area.getSouthWestTile().getY()) / 8 + 1;
		int[] newCoords = MapBuilder.findEmptyChunkBound(width, height);
		MapBuilder.copyAllPlanesMap(area.getSouthWestTile().getChunkX(), area.getSouthWestTile().getChunkY(), newCoords[0], newCoords[1], width, height);
		baseLocation = Location.create(newCoords[0] << 3, newCoords[1] << 3, 0);
		baseLocation.setWidth(width);
		baseLocation.setHeight(height);

		final int regionId = baseLocation.getRegionId();
		
		World.getWorld().submit(new Tickable(1) {
			@Override
			public void execute() {
				if(World.getWorld().getRegionManager().getRegion(regionId, false).getLoadMapStage() == 2) {
					Location loc1 = Location.create(baseLocation.getX() + area.getFirstDeathOffsetX() - 1, baseLocation.getY() + area.getFirstDeathOffsetY() + 1, 0);
					Location loc2 = Location.create(baseLocation.getX() + area.getSecondDeathOffsetX() - 1, baseLocation.getY() + area.getSecondDeathOffsetY() - 1, 0);
					World.spawnObject(new GameObject(loc1, 27663, 10, 1));
					World.spawnObject(new GameObject(loc2, 27663, 10, 1));
					this.stop();
				}
			}
		});
		//createWalls();
		if (warRules[0].getText() != Rules.LAST_TEAM_STANDING.getText())
			this.killLimit = Integer.parseInt(warRules[0].getText().split(" ")[0]);
			if(this.killLimit > 0) {
				this.canRejoin = true;
			}
		if(Constants.DEBUG_MODE) {
			clanLeader1.getPacketSender().sendMessage("Kills set to: " + this.killLimit);
			clanLeader2.getPacketSender().sendMessage("Kills set to: " + this.killLimit);
		}
		
	}
	
	/*private void createWalls() {
		int start_x = baseLocation.getX() + 32;
		int start_y = baseLocation.getY() + 33; //or 40
		for(int i = 0; i < 52; i++) {
			//World.spawnObjectTemporary(new GameObject(Location.create(start_x + i, start_y, 0), 38687, 10, 0), timer);
			GameObject obj = new GameObject(Location.create(start_x + i, start_y, 0), 38687, 10, 0);
			World.spawnObject(obj);
			walls.add(obj);
		}
	}
	
	private void removeWalls() {
		for(GameObject wall : walls) {
			World.getWorld().sendObjectAnimation(wall, 10368);
		}
		World.getWorld().submit(new Tickable(4) {
			@Override
			public void execute() {
				for (GameObject wall : walls) {
					World.getWorld().removeObject(wall);
					walls.remove(wall);
				}
				this.stop();
			}
		});
	}
	*/
	
	@Override
	public Player[] getTeam() {
		Player[] playerList = new Player[team1.size() + team2.size()];
		int total = 0;
		for(int i = 0; i < team1.size(); i++) {
			playerList[total] = team1.get(i);
			total++;
		}
		for(int i = 0; i < team2.size(); i++) {
			playerList[total] = team2.get(i);
			total++;
		}
		return playerList;
	}

	public List<Player> getTeam1() {
		return team1;
	}
	
	public List<Player> getTeam2() {
		return team2;
	}
	
	public List<Player> getSpectators1() {
		return spectators1;
	}
	
	public List<Player> getSpectators2() {
		return spectators2;
	}
	
	public String getLeader1() {
		return leader1;
	}
	
	public String getLeader2() {
		return leader2;
	}

	@Override
	public void start_game() {
		session_timer = new Tickable(2) {
			@Override
			public void execute() {
				/*Region region = World.getWorld().getRegion(baseLocation.getRegionId(), false);
				
				if(region.getPlayerCount() == 0) {
					end_game();
					return;
				}*/
				if(winningTeam != -1) {
					end_game();
					return;
				}
				if (winningTeam == -1 && defeatedPlayers1.size() >= getTeam1().size()) {
					winningTeam = 2;
					end_game();
					return;
				} else if (winningTeam == -1 && defeatedPlayers2.size() >= getTeam2().size()) {
					winningTeam = 1;
					end_game();
					return;
				}
				if (!warActive) {
					if (countdown > 0) {
						countdown--;
						/*for(int i = 0; i < 2; i++) {
							List<Player> team = i == 0 ? team1 : team2;
							for(Player players : team) {
								players.getPacketSender().modifyText("Fight!", 652, 5);
							}
						}*/
					} else {
						warActive = true;
						for(int i = 0; i < 2; i++) {
							List<Player> team = i == 0 ? team1 : team2;
							for(Player players : team) {
								players.teleport(getStartLocation(players, i == 0 ? 1 : 2));
							}
						}
						//removeWalls();
					}
				} if (warActive) {
					if(killLimit != -1) {
						if(killcount1 >= killLimit) {
							winningTeam = 1;
							end_game();
							return;
						} else if (killcount2 >= killLimit) {
							winningTeam = 2;
							end_game();
							return;
						}
					} else {
						//No kill limit, keep going until no more players are in this instance
					}
				}
				final String time = "" + (countdown > 3 ? 
						(countdown >= 60 ? "1:" + (countdown - 60) : "0:" + (countdown < 10 ? "0" + countdown : countdown))
						: "<col=DB0000>GET READY");
				for(int i = 0; i < 2; i++) {
					List<Player> team = i == 0 ? team1 : team2;
					List<Player> spectators = i == 0 ? spectators1 : spectators2;
					int size1 = team1.size() - defeatedPlayers1.size();
					int size2 = team2.size() - defeatedPlayers2.size();
					for(Player players : team) {
						sendInterface(players, i, time, size1, size2);
					}
					for(Player players : spectators) {
						sendInterface(players, i, time, size1, size2);
					}
				}
				
			}
		};
		super.start_game();
	}
	
	public void sendInterface(Player player, int i, String time, int size1, int size2) {
		player.getPacketSender().modifyText((size1 > size2 ? "<col=00E300>" : size1 < size2 ? "<col=DB0000>" : "") + size1, 652, i == 0 ? 3 : 4);
		player.getPacketSender().modifyText((size2 > size1 ? "<col=00E300>" : size2 < size1 ? "<col=DB0000>" : "") + size2, 652, i == 0 ? 4 : 3);						
		
		if(warActive) {
			if(canRejoin) {
				player.getPacketSender().modifyText("Kills: " + (i == 0 ? killcount1 : killcount2) + " / " + killLimit, 652, 5);
			} else {
				player.getPacketSender().modifyText("", 652, 5);
			}
		} else {
			player.getPacketSender().modifyText("Countdown: " + time, 652, 5);
		}
	}
	
	public boolean isDangerous() {
		return dangerous;
	}
	
	public boolean[] getMiscRules() {
		return miscRules;
	}
	
	public boolean itemsStillOnGround(Region region) {
		
		return false;
	}

	@Override
	public void end_game() {
		if(dangerous) {
			boolean endGame = false;
			Region region = World.getWorld().getRegion(baseLocation.getRegionId(), false);
			
			if(region.getPlayerCount() == 0) {
				endGame = true;
			}
			
			if(!endGame && region != null && region.getGroundItems() != null) {
				//Check for vials and bones
				if (region.getGroundItems().size() > 0) {
					boolean hasValuable = false;
					for(GroundItem i : region.getGroundItems()) {
						if(i != null && i.getId() != 526 && i.getId() != 227) {
							//Not bones or vials
							hasValuable = true;
						}
					}
					endGame = !hasValuable;
				} else{
					endGame = true;
				}
			}
			
			if(!endGame) {
				if(!messageSent) {
					List<Player> playerList = winningTeam == 1 ? team1 : team2;
					List<Player> spectatorsList = winningTeam == 1 ? spectators1 : spectators2;
					for(Player player : playerList) {
						if(player == null)
							continue;
						player.getPacketSender().sendChatboxDialogue(true, "Your team has won, but there are still items on the ground.", "Make sure to pick up your loot before leaving the arena.");
					}
					for(Player player : spectatorsList) {
						if(player == null)
							continue;
						player.getPacketSender().sendChatboxDialogue(true, "Your team has won, but there are still items on the ground.", "Make sure to pick up your loot before leaving the arena.");
					}
					messageSent = true;
				}
				return;//Winning team is already logged by now, but wait until players pick up items then kick them out
			}
		}
		ClanWarsManager.removeWar(this);
		super.end_game();
		this.warActive = false;
		for(int i = 0; i < 2; i++) {
			List<Player> playerList = i == 0 ? team1 : team2;
			List<Player> spectatorsList = i == 0 ? spectators1 : spectators2;
			String message = winningTeam == i + 1 ? "Your clan was victorious in the battle!" : "Unfortunately, your clan was defeated in the battle.";
			for(Player player : playerList) {
				if(player == null)
					continue;
				Achievements.increase(player, 51);
				player.getPacketSender().sendChatboxDialogue(true, message);
				remove_player(player);
			}
			for(Player player : spectatorsList) {
				if(player == null)
					continue;
				Achievements.increase(player, 51);
				player.getPacketSender().sendChatboxDialogue(true, message);
				remove_player(player);
			}
		}
		World.getWorld().submit(new Tickable(2) {
			@Override
			public void execute() {
				MapBuilder.destroyMap(baseLocation.getChunkX(), baseLocation.getChunkY(), baseLocation.getWidth(), baseLocation.getHeight());
				this.stop();
			}
		});
	}
	
	@Override
	public void insert_player(Player player) {
		player.resetVariables();
		player.getAttributes().set("clansession", this);
		player.getVariables().setSessionName("clanwars");
		int team = team1.contains(player) ? 1 : 2;
		//Death loc temporarily until we get walls sorted out
		Location start = getDeathLocation(player, team);//getStartLocation(player, team);
		player.setLocation(start);
		player.face(start.getX(), start.getY() + (team == 1 ? 1 : -1), 0);
		
		int size1 = team1.size() - defeatedPlayers1.size();
		int size2 = team2.size() - defeatedPlayers2.size();
		player.getPacketSender().modifyText(clanName1 + " vs. " + clanName2, 652, 0);
		player.getPacketSender().modifyText((size1 > size2 ? "<col=00E300>" : size1 < size2 ? "<col=DB0000>" : "") + size1, 652, 3);
		player.getPacketSender().modifyText((size2 > size1 ? "<col=00E300>" : size2 < size1 ? "<col=DB0000>" : "") + size2, 652, 4);
		if(warActive) {
			if(canRejoin) {
				player.getPacketSender().modifyText("Kills: " + (team == 1 ? killcount1 : killcount2) + " / " + killLimit, 652, 5);
			} else {
				player.getPacketSender().modifyText("", 652, 5);
			}
		} else {
			String time = "" + (countdown > 3 ? 
					(countdown >= 60 ? "1:" + ((countdown - 60) < 10 ? "0" + (countdown - 60) : (countdown - 60)) : "0:" + (countdown < 10 ? "0" + countdown : countdown))
					: "<col=DB0000>GET READY");
			player.getPacketSender().modifyText(player.getAttributes().isSet("spectator") ? "" : "Countdown: " + time, 652, 5);
		}
		player.getPacketSender().modifyText(dangerous ? "<col=DB0000>Items Lost on Death" : "<col=00E300>Items Safe on Death", 652, 6);
		player.getPacketSender().sendOverlay(652);
	
		System.out.println("ok: " + World.getMask(player.getLocation().getX() - 1,player.getLocation().getY() + (team == 1 ? 1 : -1), 0));
	}

	public boolean isSpectator(Player player) {
		return spectators1.contains(player) || spectators2.contains(player);
	}
	
	public int removeSpectatorStatus(Player player) {
		if(spectators1.contains(player)) {
			spectators1.remove(player);
			return 1;
		} else if (spectators2.contains(player)) {
			spectators2.remove(player);
			return 2;
		}
		return -1;
	}
	
/*	public void leave_to_spectate(Player player) {
		if(!isSpectator(player)) {
			int team = 1;
			if(team1.contains(player)) {
				//team1.remove(player);
				spectators1.add(player);
			} else if (team2.contains(player)) {
				//team2.remove(player);
				spectators2.add(player);
				team = 2;
			}
			
			player.setLocation(getDeathLocation(player, team));
		}
	}
*/
	public void leave_to_spectate(Entity deadEntity) {
		Player player = (Player) deadEntity;
		int team = 1;
		if(team1.contains(player)) {
			defeatedPlayers1.add(player.getName());
			getSpectators1().add(player);
			killcount2++;
		}
		if(team2.contains(player)) {
			defeatedPlayers2.add(player.getName());
			getSpectators2().add(player);
			killcount1++;
			team = 2;
		}
		player.teleport(getDeathLocation(player, team));
		player.resetVariables();
	}

	public void leave_portal(Player player, boolean dead, boolean force) {
		if(!force && isSpectator(player)) {
			if(canRejoin) {
				if(Constants.DEBUG_MODE)
					player.getPacketSender().sendMessage("Send rejoin dialogue");
				//TODO: Dialogue ?
				int team = removeSpectatorStatus(player);
				if(team == -1) {
					player.getPacketSender().sendMessage("A fatal error has occured. Please contact a staff member.");
					System.out.println("[FATAL] Spectator has no team!");
					return;
				}if(team == 1)
					getTeam1().add(player);
				else 
					getTeam2().add(player);
				player.teleport(getStartLocation(player, team));
				return;
			} else {
				if(Constants.DEBUG_MODE)
					player.getPacketSender().sendMessage("war is set to kill limit, leave");
				leave_portal(player, dead, true);
				player.getPacketSender().sendMessage("This war has no kill limit set, which mean you can only spectate after death.");
				return;
			}
		}
		if(Constants.DEBUG_MODE)
			System.out.println("LEAVING");
		for(int i = 0; i < 2; i++) {
			List<Player> team = i == 0 ? team1 : team2;
			//List<String> defeated = i == 0 ? defeatedPlayers1 : defeatedPlayers2;
			if(team.contains(player)) {
				if(!dead && warActive) {
					if(Constants.DEBUG_MODE)
					System.out.println("NOT DEAD, WAR IS ACTIVE. ONLY ADD TO DEFEATEDPLAYERS");
					if(!isSpectator(player)) {
						if(i == 0) 
							defeatedPlayers1.add(player.getName());
						else
							defeatedPlayers2.add(player.getName());
					}
					break;
				} else if (!warActive && team.size() == 1) {
					if(Constants.DEBUG_MODE)
						System.out.println("LEAVING, war not active AND team only has this player.");
					end_game();
					return;
				} else if(!warActive && team.size() > 1){
					if(Constants.DEBUG_MODE)
						System.out.println("LEAVING, war not active but team has more players to hold it down.");
					if(i ==0) {
						team1.remove(player);
					} else {
						team2.remove(player);
					}
					break;
				}
				
				if(dead && i == 0)
					killcount2++;
				else if (dead && i == 1)
					killcount1++;
			}
		}
		removeSpectatorStatus(player);
		
		remove_player(player);
	}

	public void viewing_orb(Player player, int button) {
		if(!warActive) {
			player.getPacketSender().sendMessage("The battle has not yet begun.");
			return;
		}

		int start_x = -1;
		int start_y = -1;
				switch(button) {
				case 11: // Center
					start_x = baseLocation.getX() + 60;
					start_y = baseLocation.getY() + 35;
					break;
				case 12: // NW
					start_x = baseLocation.getX() + 45;
					start_y = baseLocation.getY() + 45;
					break;
				case 13: // NE
					start_x = baseLocation.getX() + 74;
					start_y = baseLocation.getY() + 49;
					break;
				case 14: // SE
					start_x = baseLocation.getX() + 72;
					start_y = baseLocation.getY() + 20;
					break;
				case 15: // SW
					start_x = baseLocation.getX() + 47;
					start_y = baseLocation.getY() + 19;
					break;
				case 5:
					if(player.getAttributes().isSet("viewingOrbLocation")) {
						unset_viewing_orb(player, true);
					}
					return;
				}
		if(start_x != -1) {
			if(!player.getAttributes().isSet("viewingOrbLocation")) {
				set_viewing_orb(player);
			}
			player.teleport(Location.create(start_x, start_y, 0));
		} else {
			if(Constants.DEBUG_MODE) {
				player.getPacketSender().sendMessage("No location for button: " + button);
			}
		}
	}
	
	private void set_viewing_orb(Player player) {
		player.getAttributes().set("viewingOrbLocation", player.getLocation());
		player.getAttributes().set("stopActions", true);
		//player.getPacketSender().sendPlayerOption("Null", 3, false);
		//player.getPacketSender().sendPlayerOption("Null", 4, false);
		player.setVisible(false);
		player.getAppearance().setInvisible(true);
		player.getPacketSender().transformToNpc(player, /*7901*/8260); //7901 green thing
		//7000 blank
	}
	
	private void unset_viewing_orb(Player player, boolean teleport) {
		player.getAttributes().remove("stopActions");
		if (teleport)
			player.teleport(player.getAttributes().get("viewingOrbLocation"));
		player.getAttributes().remove("viewingOrbLocation");
	//	player.getPacketSender().sendPlayerOption("Follow", 3, false);
	//	player.getPacketSender().sendPlayerOption("Trade with", 4, false);
		player.setVisible(true);
		player.getAppearance().setInvisible(false);
		player.getPacketSender().transformToNpc(player, -1);
		//player.getPacketSender().displayInventoryInterface(149);
		//player.getPacketSender().restoreTabs();
	}
	
	@Override
	public void remove_player(Player player) {
		player.getAttributes().remove("warnedDangerous");
		player.getAttributes().remove("spectator");
		player.getAttributes().remove("clansession");
		if(!player.getAttributes().isSet("loggingOut"))
			player.getVariables().setSessionName(null);
		if(player.getAttributes().isSet("stopActions"))
			player.getAttributes().remove("stopActions");
		if(player.getAttributes().isSet("viewingOrbLocation")) {
			unset_viewing_orb(player, false);
		}
		player.getPacketSender().sendRemoveOverlay();
		player.teleport(end_area());
		player.getVariables().setGameSession(null);
		//super.remove_player(player);
	}
	
	@Override
	public void handle_death(Entity deadEntity, Entity killer) {
		Player player = (Player) deadEntity;
		int team = 1;
		if(team1.contains(player)) {
			defeatedPlayers1.add(player.getName());
			getSpectators1().add(player);
			killcount2++;
		}
		if(team2.contains(player)) {
			defeatedPlayers2.add(player.getName());
			getSpectators2().add(player);
			killcount1++;
			team = 2;
		}
		//remove_player(player);
		//
		if (dangerous)
			player.dropLoot(killer);
		player.teleport(getDeathLocation(player, team));
		player.resetVariables();
	}

	@Override
	public void handle_logout(Player player) {
	/*	if(team1.contains(player)) {
			defeatedPlayers1.add(player.getName());
		}
		if(team2.contains(player)) {
			defeatedPlayers2.add(player.getName());
		}
		remove_player(player);*/
		player.getAttributes().set("loggingOut", true);
		leave_portal(player, false, true);
		//super.handle_logout(player);
	}

	@Override
	public Tickable session_timer() {
		return session_timer;
	}

	@Override
	public GameBounds starting_placement() {
		return null;
	}
	
	public Location getStartLocation(Player player, int team) {
		int start_x = -1;
		int start_y = -1;
		if(team == 1) {
			start_x = baseLocation.getX() + area.getFirstSpawnOffsetX();
			start_y = baseLocation.getY() + area.getFirstSpawnOffsetY();
		} else {
			start_x = baseLocation.getX() + area.getSecondSpawnOffsetX();
			start_y = baseLocation.getY() + area.getSecondSpawnOffsetY();
		}
		return Location.create(start_x, start_y, 0);
	}

	private Location getDeathLocation(Player player, int team) {
		Location loc = null;
		if(team == 1)
			loc = Location.create(baseLocation.getX() + area.getFirstDeathOffsetX(), baseLocation.getY() + area.getFirstDeathOffsetY(), 0);
		else
			loc = Location.create(baseLocation.getX() + area.getSecondDeathOffsetX(), baseLocation.getY() + area.getSecondDeathOffsetY(), 0);
		return loc;
	}
	
	@Override
	public Location death_placement() {
		return null;
	}

	@Override
	public Location end_area() {
		return Location.create(6471, 3675, 0);
	}

	@Override
	public boolean items_are_safe() {
		return true;
	}
	
	public boolean hasWarStarted() {
		return warActive;
	}

	public List<String> getDefeatedPlayers1() {
		return defeatedPlayers1;
	}
	
	public List<String> getDefeatedPlayers2() {
		return defeatedPlayers2;
	}
	
	public Rules[] getRules() {
		return warRules;
	}

	public int getKillLimit() {
		return killLimit;
	}

	public boolean canRejoin() {
		return canRejoin;
	}
}
