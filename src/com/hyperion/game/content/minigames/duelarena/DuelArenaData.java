package com.hyperion.game.content.minigames.duelarena;

import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.utility.NumberUtils;

public class DuelArenaData {

	public static final Location DUEL_LOBBY = Location.create(3367, 3276, 0);
	
	protected static final int[] RULE_CONFIGS = {
		1, 2, 16, 32, 64, 128,
		256, 512, 1024, 4096, 8192, 16384, 32768, 65536, 131072, 262144,
		524288, 2097152, 8388608, 16777216, 67108864, 134217728, 268435456
	};//rule 21 134217728

	
	public static final int[] DUEL_ARENA_GEAR_RULES = { 13, 14, 15, 16, 17, 18, 19, 20, 21, 11, 12 };
	public static final int[] DUEL_ARENA_GEAR_SLOTS = { 2, 3, 4, 5, 7, 9, 10, 12, 13, 0, 1 };

/*    protected static final int[] RULE_CONFIGS = {
        1, // no forfeit         0
        2, // no movement       1
        16, // no range         2
        32, // no melee         3
        64, // no magic            4
        128, // no drinks          5
        256, // no food           6
        512, // no prayer         7
        1024, // obstacles       8
        4096, // fun weapons    9
        8192, // special attacks    10
        65536, // amulet      11
        131072, // weapon 12
        262144, // body   13
        524288, // shield   14
        2097152, // legs    15
        8388608, // gloves   16
        16777216,// boots    17
        67108864, // ring     18
        134217728, // arrows     19
        268435456, // summoning    20
        16384, // Hat           21
        32768, // Cape         22
};
	*/
	protected static final int[] DUELING_BUTTON_IDS = { 124, 144, 132, 145, 126,
		148, 127, 149, 125, 150, 129, 151, 130, 152, 131, 153, 154, 155,
		156, 157, 159, 158, 200, 201, 202, 204, 205, 206, 207, 210, 209,
		208, 203 };
	
	protected static final int[] RULE_IDS = { 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5,
		6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
		20, 21 };
	
	protected static final int[] BEFORE_THE_DUEL_STARTS_CHILD_IDS = {138, 118, 119, 126, 127};
	protected static final int[] DURING_THE_DUEL_CHILD_IDS = {129, 130, 131, 132, 134, 135, 136, 137, 139, 140, 141};
	
	protected static final String[] BEFORE_THE_DUEL_STARTS = {"Some user items will be taken off", "Boosted stats will be restored", "Existing prayers will be stopped", "", ""};
	protected static final String[] RULE_STRINGS = {"You cannot forfeit the duel", "You cannot move", "You cannot use ranged attacks", "You cannot use melee attacks", "You cannot use magic attacks", "You cannot use drinks", "You cannot use food", "You cannot use prayer", "There will be obstacles in the arena.", "There will be fun weapons", "You cannot use special attacks."};
	
	 private static final Area[] OBSTACLE_AREA = new Area[]{new Area(Location.create(3367, 3246, 0), Location.create(3385, 3256, 0)), new Area(Location.create(3336, 3227, 0), Location.create(3355, 3237, 0)), new Area(Location.create(3367, 3208, 0), Location.create(3386, 3218, 0))};
	 private static final Area[] NORMAL_AREA = new Area[]{new Area(Location.create(3337, 3245, 0), Location.create(3355, 3256, 0)), new Area(Location.create(3366, 3227, 0), Location.create(3386, 3237, 0)), new Area(Location.create(3337, 3207, 0), Location.create(3354, 3218, 0))};
	 
	private static class Area {

			private Location southWestCorner;
			private Location northEastCorner;

			private Area(Location southWestCorner, Location northEastCorner) {
				this.southWestCorner = southWestCorner;
				this.northEastCorner = northEastCorner;
			}

			public Location getSouthWestCorner() {
				return southWestCorner;
			}

			public Location getNorthEastCorner() {
				return northEastCorner;
			}
	}
	 
	private static Location randomPosition(Area area) {
	 Location finalPosition = Location.create(area.getSouthWestCorner().getX() + NumberUtils.random(area.getNorthEastCorner().getX() - area.getSouthWestCorner().getX()), area.getSouthWestCorner().getY() + NumberUtils.random(area.getNorthEastCorner().getY() - area.getSouthWestCorner().getY()), area.getSouthWestCorner().getZ());
		while (World.getMask(finalPosition.getX(), finalPosition.getY(), finalPosition.getZ()) != 0)
			finalPosition = Location.create(area.getSouthWestCorner().getX() + NumberUtils.random(area.getNorthEastCorner().getX() - area.getSouthWestCorner().getX()), area.getSouthWestCorner().getY() + NumberUtils.random(area.getNorthEastCorner().getY() - area.getSouthWestCorner().getY()), area.getSouthWestCorner().getZ());
		return finalPosition;

	}
	
	public Location getNextToPlayerPosition(Location loc) {
		int x = loc.getX();
		int y = loc.getY();
		int z = 0;
		Location up = Location.create(x, y + 1, z);
		Location down = Location.create(x, y - 1, z);
		Location left = Location.create(x - 1, y, z);
		Location right = Location.create(x + 1, y, z);
		if (World.getMask(up.getX(), up.getY(), 0) == 0)
			return up;
		else if (World.getMask(down.getX(), down.getY(), 0) == 0)
			return down;
		else if (World.getMask(left.getX(), left.getY(), 0) == 0)
			return left;
		else
			return right;
	}
	
	public Location getRandomArenaPosition(boolean obstacles, int index) {
		return randomPosition(randomPositionSpawn(obstacles)[index]);
	}
	
	private Area[] randomPositionSpawn(boolean obstacles) {
		if (obstacles)
			return OBSTACLE_AREA;
		else
			return NORMAL_AREA;
	}
}
