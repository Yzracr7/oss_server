package com.hyperion.game.content.minigames.duelarena;

import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.item.Item;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
import com.hyperion.utility.NumberUtils;
import com.hyperion.utility.TextUtils;
import com.hyperion.utility.logger.Log;

/**
 * @author Nando
 */
public class DuelSession extends DuelArenaData {

	public enum DUEL_STATE {
		FIRST_SCREEN_UNACCEPTED, FIRST_SCREEN_ACCEPTED, SECOND_SCREEN_UNACCEPTED, SECOND_SCREEN_ACCEPTED, COUNT_DOWN, COMMENCE, ENDED
	}
	
	/*
	 * Rule 1 - 
	 * 
	 * 
	 */

	private Player player;
	private Player p2;
	private Player winner;
	private Item[] winnings;
	private DUEL_STATE state = DUEL_STATE.FIRST_SCREEN_UNACCEPTED;
	private boolean[] rules = new boolean[23];
	private int config;

	public DuelSession(Player p, Player p2) {
		this.player = p;
		this.p2 = p2;
		this.config = 0;
		openInterface();
	}

	private void openInterface() {
		player.getPacketSender().sendConfig(286, 0);
		player.getPacketSender().modifyText("", 107, 107);
		player.getPacketSender().modifyText("", 107, 113);
		player.getPacketSender().modifyText(p2.getDetails().getName(), 107, 248);
		player.getPacketSender().modifyText("" + p2.getSkills().getCombatLevel(), 107, 245);
		player.getPacketSender().displayInterface(107);
		player.getPacketSender().displayInventoryInterface(109);
		refreshDuel();
		player.getVariables().setCloseInterfacesEvent(new Runnable() {
			@Override
			public void run() {
				declinedDuel();
			}
		});
	}

	private void refreshDuel() {
		player.getPacketSender().closeChatboxInterface();
		player.getPacketSender().sendItems(109, 0, 93, player.getInventory().getItems());
		player.getInventory().refresh();
		player.getPacketSender().sendItems(107, 98, -1, player.getDuel().getItems());
		p2.getPacketSender().sendItems(107, 99, -1, player.getDuel().getItems());
	}

	public void toggleDuelRules(int buttonId) {
		if (buttonId == 103) {
			acceptDuel();
			return;
		}
		if (!allowedToWithdrawOrDeposit()) {
			return;
		}
		for (int index = 0; index < DUELING_BUTTON_IDS.length; index++) {
			if (DUELING_BUTTON_IDS[index] == buttonId) {
				buttonId = RULE_IDS[index];
				break;
			}
		}
		if (buttonId == -1) {
			return;
		}
		config = rules[buttonId] ? config - RULE_CONFIGS[buttonId] : config
				+ RULE_CONFIGS[buttonId];
		rules[buttonId] = rules[buttonId] ? false : true;
		System.out.println("Setting rule: " + buttonId + " to " + rules[buttonId] + " and config to " + config);
		if (buttonId == 16 && rules[16]) {
			player.getPacketSender().sendMessage("You will not be able to use two-handed weapons, such as bows.");
			p2.getPacketSender().sendMessage("You will not be able to use two-handed weapons, such as bows.");
		}
		if (buttonId == 21 && rules[21]) {
			player.getPacketSender().sendMessage("You will not be able to use any weapon which uses arrows.");
			p2.getPacketSender().sendMessage("You will not be able to use any weapon which uses arrows.");
		}
		p2.getDuelSession().setRule(buttonId, rules[buttonId]);
		if (config > 0) {
			System.out.println("operate config > 0");
			player.getPacketSender().sendConfig(286, 644);
			p2.getPacketSender().sendConfig(286, 644);
		} else {
			System.out.println("do not operate config < 0");
			player.getPacketSender().sendConfig(286, 0);
			p2.getPacketSender().sendConfig(286, 0);
		}
		p2.getDuelSession().setConfig(config);
		resetStatus();
	}

	public void acceptDuel() {
		if (state.equals(DUEL_STATE.FIRST_SCREEN_UNACCEPTED)) {
			if(player.getAttributes().isSet("isMonkey")) {
				player.getPacketSender().sendMessage("Monkeys cannot participate in duels.");
				return;
			}
			if (stakingOverMaxAmount()) {
				player.getPacketSender().sendMessage("You or your opponent do not have enough room for the stake.");
				return;
			}
			if (notEnoughRoom()) {
				player.getPacketSender().sendMessage("You don't have enough inventory space for the stake and/or duel options.");
				return;
			}
/*			if (p2.getDetails().isIronman() || player.getDetails().isIronman()) {
				if (!(p2.getDetails().isIronman() && player.getDetails().isIronman()) && p2.getDuel().getTotalItems() > 0 || player.getDuel().getTotalItems() > 0) {
					player.getPacketSender().sendChatboxDialogue(true, "Either you or your opponent is an ironman.", "Ironman accounts can only stake each other.");
					return;
				}
			}*/
			if (p2.getDuelSession().notEnoughRoom()) {
				player.getPacketSender().sendMessage("Other player does not have enough inventory space for the stake and/or duel options.");
				return;
			}
			if (rules[2] && rules[3] && rules[4]) {
				player.getPacketSender().modifyText("You cannot have no Ranged, no Melee and no Magic - how would you fight?", 107, 113);
				p2.getPacketSender().modifyText("You cannot have no Ranged, no Melee and no Magic - how would you fight?", 107, 113);
				return;
			}
			this.state = DUEL_STATE.FIRST_SCREEN_ACCEPTED;
		}
		if (state.equals(DUEL_STATE.FIRST_SCREEN_ACCEPTED)) {
			player.getPacketSender().modifyText("Waiting for other player...", 107, 113);
			p2.getPacketSender().modifyText("Other player has accepted...", 107, 113);
			if (p2.getDuelSession().getState().equals(DUEL_STATE.FIRST_SCREEN_ACCEPTED)) {
				displayConfirmation();
				p2.getDuelSession().displayConfirmation();
			}
			return;
		}
		if (state.equals(DUEL_STATE.SECOND_SCREEN_UNACCEPTED) && p2.getDuelSession().getState().equals(DUEL_STATE.SECOND_SCREEN_UNACCEPTED)) {
			player.getPacketSender().modifyText("Waiting for other player...", 106, 133);
			p2.getPacketSender().modifyText("Other player has accepted...", 106, 133);
			state = DUEL_STATE.SECOND_SCREEN_ACCEPTED;
			return;
		}
		if (state.equals(DUEL_STATE.SECOND_SCREEN_UNACCEPTED) && p2.getDuelSession().getState().equals(DUEL_STATE.SECOND_SCREEN_ACCEPTED)) {
			state = DUEL_STATE.COUNT_DOWN;
			p2.getDuelSession().setState(DUEL_STATE.COUNT_DOWN);
			player.getPacketSender().softCloseInterfaces();
			p2.getPacketSender().softCloseInterfaces();
			player.getPacketSender().sendMessage("Accepted stake and duel options.");
			p2.getPacketSender().sendMessage("Accepted stake and duel options.");
			Achievements.increase(player, 25);
			Achievements.increase(p2, 25);
			World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Staking " + TextUtils.getItemLogDisplay(player, player.getDuel().getItems()) + ", to player " + p2.getName() + " for " + TextUtils.getItemLogDisplay(p2, p2.getDuel().getItems())));
			World.getWorld().loggingThread.appendLog(new Log(p2.getName(), "Staking " + TextUtils.getItemLogDisplay(p2, p2.getDuel().getItems()) + ", to player " + player.getName() + " for " + TextUtils.getItemLogDisplay(player, player.getDuel().getItems())));
			if (rules[1]) {
				player.getAttributes().set("stopMovement", true);
				p2.getAttributes().set("stopMovement", true);
			}
			boolean obstacle = (ruleEnabled(8));
			Location arena = null;
			if (obstacle) {
				arena = getRandomArenaPosition(true, 0);
			} else {
				arena = getRandomArenaPosition(false, 0);
			}
			final Location playersPos = arena;
			player.teleport(arena);
			// refreshing areas
			if (obstacle) {
				arena = getRandomArenaPosition(true, 0);
			} else {
				arena = getRandomArenaPosition(false, 0);
			}
			boolean noMovement = (ruleEnabled(1));
			if (noMovement) {
				arena = getNextToPlayerPosition(playersPos);
			}
			p2.teleport(arena);
			MainCombat.endCombat(player, 1);
			MainCombat.endCombat(p2, 1);
			player.resetVariables();
			p2.resetVariables();
			removeWornItems();
			p2.getDuelSession().removeWornItems();
			player.getPacketSender().sendHintArrow(p2);
			p2.getPacketSender().sendHintArrow(player);
			player.getVariables().setCloseInterfacesEvent(null);
			p2.getVariables().setCloseInterfacesEvent(null);
			World.getWorld().setRandomIndex(player);
			World.getWorld().setRandomIndex(p2);
			World.getWorld().submit(new Tickable(2) {
				byte count = 3;

				@Override
				public void execute() {
					if (player.isDestroyed() || p2.isDestroyed()) {
						this.stop();
						return;
					}
					if (player.getDuelSession() == null || p2.getDuelSession() == null) {
						this.stop();
						return;
					}
					if (count > 0) {
						p2.playForcedChat("" + count);
						player.playForcedChat("" + count);
					} else {
						p2.playForcedChat("Fight!");
						player.playForcedChat("Fight!");
						state = DUEL_STATE.COMMENCE;
						p2.getDuelSession().setState(DUEL_STATE.COMMENCE);
						this.stop();
					}
					count--;
				}
			});
		}
	}

	public void declineDuel(boolean button_clicked) {
		if (button_clicked) {
			player.getVariables().setCloseInterfacesEvent(null);
			p2.getVariables().setCloseInterfacesEvent(null);
			declinedDuel();
		}
		p2.getInterfaceSettings().closeInterfaces(false);
		player.getInterfaceSettings().closeInterfaces(false);
	}

	private void declinedDuel() {
		player.getPacketSender().sendMessage("You decline the stake and duel options.");
		giveBack();
		player.setDuelSession(null);
		if (p2.getDuelSession() != null) {
			p2.getPacketSender().sendMessage("Other player declined the stake and duel options.");
			p2.getDuelSession().giveBack();
			p2.setDuelSession(null);
			p2.getInterfaceSettings().closeInterfaces(false);

		}
	}

	public void finishDuel(boolean lost, boolean forfeit) {
		if (state.equals(DUEL_STATE.ENDED)) {// in a tie, the 2nd player to hit
												// the floor gets this method
												// called..
			player.setDuelSession(null);
			return;
		}
		state = DUEL_STATE.ENDED;
		if (p2.getDuelSession() != null) {
			p2.getDuelSession().setState(DUEL_STATE.ENDED);
		}
		boolean tie = p2.getCombatState().isDead();
		if (tie) {
			// player 1 stuff
			player.teleport(3360 + NumberUtils.random(19), 3274 + NumberUtils.random(3), 0);
			player.getAttributes().remove("stopMovement");
			player.getPacketSender().sendHintArrow(null);
			player.resetVariables();
			player.getPacketSender().sendMessage("The duel ended in a tie.");
			player.getDuelSession().giveBack();
			player.setDuelSession(null);
			// player 2 stuff
			p2.teleport(3360 + NumberUtils.random(19), 3274 + NumberUtils.random(3), 0);
			p2.getAttributes().remove("stopMovement");
			p2.getPacketSender().sendHintArrow(null);
			p2.resetVariables();
			p2.getPacketSender().sendMessage("The duel ended in a tie.");
			p2.getDuelSession().giveBack();
			// both stuff
			World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Tied items " + TextUtils.getItemLogDisplay(player, player.getDuel().getItems()) + ", to player " + p2.getName() + " for " + TextUtils.getItemLogDisplay(p2, p2.getDuel().getItems())));
			World.getWorld().loggingThread.appendLog(new Log(p2.getName(), "Tied items " + TextUtils.getItemLogDisplay(p2, p2.getDuel().getItems()) + ", to player " + player.getName() + " for " + TextUtils.getItemLogDisplay(player, player.getDuel().getItems())));
			World.getWorld().getWorldLoader().savePlayer(player);
			World.getWorld().getWorldLoader().savePlayer(p2);
			return;
		}
		if (forfeit) {
			MainCombat.endCombat(p2, 1);
			p2.getPacketSender().sendMessage(player.getDetails().getName() + " has forfeited the duel!");
		} else {
			Achievements.increase(player, 26);
		}

		World.getWorld().loggingThread.appendLog(new Log(player.getName(), (lost ? "Lost" : "Won") + (forfeit ? "(by foreit) " : " ") + TextUtils.getItemLogDisplay(p2, p2.getDuel().getItems()) + ", from player " + p2.getName() + " for " + TextUtils.getItemLogDisplay(player, player.getDuel().getItems())));
		World.getWorld().loggingThread.appendLog(new Log(p2.getName(), (lost ? "Won" : "Lost") + (forfeit ? "(by foreit) " : " ") + TextUtils.getItemLogDisplay(player, player.getDuel().getItems()) + ", from player " + player.getName() + " for " + TextUtils.getItemLogDisplay(p2, p2.getDuel().getItems())));
		if (lost) {
			if (p2.getDuelSession() != null) {
				p2.getDuelSession().giveBack();
				p2.getDuelSession().setWinner(p2);
				p2.getDuelSession().setWinnings(player.getDuel().getItems());
				player.getDuel().clear();
			}
			p2.getPacketSender().sendMessage("Well done! You have defeated " + player.getDetails().getName() + "!");
			player.getPacketSender().sendMessage("You lost!");
		}
		// player stuff
		player.teleport(3360 + NumberUtils.random(19), 3274 + NumberUtils.random(3), 0);
		player.getPacketSender().sendHintArrow(null);
		player.getAttributes().remove("stopMovement");
		player.resetVariables();
		// player 2 stuff
		p2.teleport(3360 + NumberUtils.random(19), 3274 + NumberUtils.random(3), 0);
		p2.getPacketSender().sendHintArrow(null);
		p2.getAttributes().remove("stopMovement");
		p2.resetVariables();
		p2.getPacketSender().modifyText("" + player.getSkills().getCombatLevel(), 110, 104);
		p2.getPacketSender().modifyText(player.getDetails().getName(), 110, 105);
		p2.getPacketSender().sendItems(110, 88, 93, p2.getDuelSession().getWinnings());
		p2.getPacketSender().displayInterface(110);
		p2.getVariables().setCloseInterfacesEvent(new Runnable() {
			@Override
			public void run() {
				if (p2.getDuelSession() != null) {
					p2.getDuelSession().recieveWinnings(p2);
				}
			}
		});
		if (lost) {
			player.getInterfaceSettings().closeInterfaces(false);
		}
		player.setDuelSession(null);
		World.getWorld().getWorldLoader().savePlayer(player);
		World.getWorld().getWorldLoader().savePlayer(p2);
	}

	private void recieveWinnings(Player p) {
		if (!winner.equals(p)) {
			return;
		}
		if (!state.equals(DUEL_STATE.ENDED)) {
			return;
		}
		for (int i = 0; i < winnings.length; i++) {
			if (winnings[i] != null) {
				if (player.getInventory().addItem(winnings[i])) {
					winnings[i] = null;
				}
			}
		}
		player.getInventory().refresh();
		player.setDuelSession(null);
		player.getVariables().setCloseInterfacesEvent(null);
	}

	public void forfietDuel(int option) {
		if (rules[0]) {
			player.getPacketSender().sendMessage("You cannot forfeit this duel!");
			player.getInterfaceSettings().closeInterfaces(true);
			return;
		}
		if (option == 1) {// Yes
			finishDuel(true, true);
		}
		player.getInterfaceSettings().closeInterfaces(false);
	}

	private void giveBack() {
		for (int i = 0; i < player.getDuel().getItems().length; i++) {
			Item item = player.getDuel().getItems()[i];
			if (item != null) {
				if (player.getInventory().addItem(item)) {
					player.getDuel().deleteItem(item);
				}
			}
		}
		player.getInventory().refresh();
	}

	private void removeWornItems() {
		int weapon = player.getEquipment().getItemInSlot(3);
		ItemDefinition def = ItemDefinition.forId(weapon);
		boolean twoHanded = false;
		if (def != null) {
			twoHanded = def.isDoubleHanded();
		}
		for (int i = 0; i < DUEL_ARENA_GEAR_RULES.length; i++) {
			if (rules[DUEL_ARENA_GEAR_RULES[i]]) {
				if (i == 20) {
					continue;
				}
				if (ruleEnabled(14) && twoHanded) {
					removeEquip(player, 3);
					//Equipment.unequipItem(player, -1, 3, false);
				}
				removeEquip(player, DUEL_ARENA_GEAR_SLOTS[i]);
				//Equipment.unequipItem(player, -1, slot[j], false);
				
			}
		}
	}
	
	private void removeEquip(Player player, int slot) {
		if(player.getInventory().freeSlots() > 0) {
			Equipment.unequipItem(player, -1, slot, false);
		} else {
			player.getAttributes().set("currentTab", 0);
			Equipment.unequipToBank(player, player.getEquipment().getItemInSlot(slot), slot, false);
		}
	}

	private void displayConfirmation() {
		this.state = DUEL_STATE.SECOND_SCREEN_UNACCEPTED;
		boolean[] writeStrings = { true, true, true, true, true };
		/*
		 * Check the rules, and tell the array if we aren't going to write our
		 * strings on the inter.
		 */
		boolean drawWornCheck = false;
		for (int i = 11; i < 22; i++) {
			if (i != 20) {
				if (rules[i]) {
					drawWornCheck = true;
				}
			}
		}
		if (!drawWornCheck)// worn items r bein removed..
			writeStrings[0] = false;
		// if(!rules[6])//food
		// writeStrings[1] = false;
		if (!rules[7])// prayer
			writeStrings[2] = false;
		/*
		 * Makes sure the inter is clean before we write stuff on it (even
		 * if we don't).
		 */
		for (int i : BEFORE_THE_DUEL_STARTS_CHILD_IDS) {
			player.getPacketSender().modifyText("", 106, i);
		}
		int nextString = 0;
		int nextChild = 0;
		/*
		 * Write all the needed strings.
		 */
		for (boolean write : writeStrings) {
			if (write) {
				player.getPacketSender().modifyText(BEFORE_THE_DUEL_STARTS[nextString], 106, BEFORE_THE_DUEL_STARTS_CHILD_IDS[nextChild]);
				nextChild++;
			}
			nextString++;
		}
		/*
		 * Makes sure the inter is clean before we write stuff on it (even
		 * if we don't).
		 */
		for (int i : DURING_THE_DUEL_CHILD_IDS) {
			player.getPacketSender().modifyText("", 106, i);
		}
		/*
		 * This makes the correct rules(according to the rules array, go in the
		 * highest child available.
		 */
		nextString = 0;
		nextChild = 0;
		for (boolean rule : rules) {
			if (nextString == 11) {
				break;
			}
			if (rule) {
				player.getPacketSender().modifyText(RULE_STRINGS[nextString], 106, DURING_THE_DUEL_CHILD_IDS[nextChild]);
				nextChild++;
			}
			nextString++;
		}
		p2.getPacketSender().modifyText("", 106, 133);// Accepted text.
		player.getPacketSender().modifyText(buildStakedItemsString(), 106, 102);
		player.getPacketSender().modifyText(p2.getDuelSession().buildStakedItemsString(), 106, 103);
		player.getPacketSender().displayInterface(106);
		p2.getPacketSender().restoreTabs();
		player.getPacketSender().restoreTabs();
	}

	private void resetStatus() {
		if (state.equals(DUEL_STATE.FIRST_SCREEN_ACCEPTED) || p2.getDuelSession().getState().equals(DUEL_STATE.FIRST_SCREEN_ACCEPTED)) {
			this.state = DUEL_STATE.FIRST_SCREEN_UNACCEPTED;
			p2.getDuelSession().setState(DUEL_STATE.FIRST_SCREEN_UNACCEPTED);
			player.getPacketSender().modifyText("", 107, 113);
			p2.getPacketSender().modifyText("", 107, 113);
		}
	}

	private boolean stakingOverMaxAmount() {
		for (int i = 0; i < player.getDuel().getItems().length; i++) {
			Item item = player.getDuel().getItems()[i];
			if (item != null) {
				if (item.getId() == 995)
					continue;
				int id = item.getId();
				// if you're both staking the item
				long l1 = (long) p2.getDuel().getItemAmount(id) + (long) item.getCount();
				// you're staking the item, p2 has it in inventory
				long l2 = (long) p2.getInventory().getItemAmount(id) + (long) item.getCount();
				if (l1 > Integer.MAX_VALUE || l2 > Integer.MAX_VALUE) {
					return true;
				}
			}
		}
		for (int i = 0; i < p2.getDuel().getItems().length; i++) {
			Item item = p2.getDuel().getItems()[i];
			if (item != null) {
				if (item.getId() == 995)
					continue;
				// p2 is staking the item, you have it in inventory
				long l3 = (long) item.getCount() + (long) player.getInventory().getItemAmount(item.getId());
				if (l3 > Integer.MAX_VALUE) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean notEnoughRoom() {
		int j = 0;
		for (int i = 0; i < player.getDuel().getItems().length; i++) {
			Item item = player.getDuel().getItems()[i];
			if (item != null) {
				if (item.getDefinition().isStackable()) {
					if (player.getInventory().hasItem(item.getId())) {
						continue;
					}
				}
				j++;
			}
		}
		for (int i = 0; i < p2.getDuel().getItems().length; i++) {
			Item item = p2.getDuel().getItems()[i];
			if (item != null) {
				if (item.getDefinition().isStackable()) {
					if (player.getInventory().hasItem(item.getId()) || player.getDuel().hasItem(item.getId())) {
						continue;
					}
				}
				j++;
			}
		}
		j += getExtraSlots();
		return j > player.getInventory().freeSlots();
	}

	public int getExtraSlots() {
		int i = 0;
		int[] slot = { 2, 3, 4, 5, 7, 9, 10, 12, 13, 0, 1 };
		int[] rule = { 13, 14, 15, 16, 17, 18, 19, 20, 21, 11, 12 };
		for (int j = 0; j < rule.length; j++) {
			if (rules[rule[j]]) {
				Item item = player.getEquipment().get(slot[j]);
				if (item != null) {
					i += 1;
				}
			}
		}
		return i;
	}

	public boolean stakeItem(int itemId, int slot, int amount) {
		if (!allowedToWithdrawOrDeposit()) {
			return false;
		}
		Item invItem = player.getInventory().get(slot);
		if (invItem == null) {
			return false;
		}
		if (itemId != invItem.getId()) {
			return false;
		}
		if (invItem.getDefinition() == null) {
			return false;
		}
		if (invItem.getDefinition().isPlayerBound() || !invItem.getDefinition().isTradeable()) {
			player.getPacketSender().sendMessage("You cannot stake that item.");
			return false;
		}
		if ((player.getDetails().isIronman() && !p2.getDetails().isIronman())
				|| (!player.getDetails().isIronman() && p2.getDetails().isIronman())) {
			player.getPacketSender().sendChatboxDialogue(true, "You or your opponent are playing as an Ironman and cannot stake items.");
			return false;
		}
		boolean stackable = invItem.getDefinition().isStackable();
		Item itemToAdd = new Item(invItem.getId(), amount);
		int stakeSlot = player.getDuel().findItem(itemToAdd.getId());
		if (!stackable) {
			itemToAdd.setItemAmount(1);
			stakeSlot = player.getDuel().findFreeSlot();
			if (stakeSlot == -1) {
				return false;
			}
			for (int i = 0; i < amount; i++) {
				stakeSlot = player.getDuel().findFreeSlot();
				if (!player.getInventory().deleteItem(itemToAdd) || stakeSlot == -1) {
					break;
				}
				player.getDuel().addItem(itemToAdd);
			}
			resetStatus();
			refreshDuel();
			return true;
		} else if (stackable) {
			stakeSlot = player.getDuel().findItem(itemToAdd.getId());
			if (stakeSlot == -1) {
				stakeSlot = player.getDuel().findFreeSlot();
				if (stakeSlot == -1) {
					return false;
				}
			}
			if (amount > player.getInventory().getAmountInSlot(slot)) {
				itemToAdd.setItemAmount(player.getInventory().getAmountInSlot(slot));
			}
			if (player.getInventory().deleteItem(itemToAdd)) {
				player.getDuel().addItem(itemToAdd);
				resetStatus();
				refreshDuel();
				return true;
			}
		}
		return false;
	}

	public void removeItem(int itemId, int slot, int amount) {
		if (!allowedToWithdrawOrDeposit()) {
			return;
		}
		Item duelItem = player.getDuel().get(slot);
		if (duelItem == null) {
			return;
		}
		if (itemId != duelItem.getId()) {
			return;
		}
		if (duelItem.getDefinition() == null) {
			return;
		}
		boolean stackable = duelItem.getDefinition().isStackable();
		int duelSlot = player.getDuel().findItem(duelItem.getId());
		Item itemToRemove = new Item(duelItem.getId(), amount);
		if (duelSlot == -1) {
			return;
		}
		if (!stackable) {
			itemToRemove.setItemAmount(1);
			if (amount > player.getDuel().getItemAmount(itemToRemove.getId())) {
				amount = player.getDuel().getItemAmount(itemToRemove.getId());
			}
			for (int i = 0; i < amount; i++) {
				duelSlot = player.getDuel().findItem(itemToRemove.getId());
				if (player.getInventory().addItem(itemToRemove)) {
					player.getDuel().deleteItem(itemToRemove);
				}
			}
			resetStatus();
			refreshDuel();
		} else {
			duelSlot = player.getDuel().findItem(duelItem.getId());
			if (itemToRemove.getCount() > player.getDuel().getAmountInSlot(slot)) {
				itemToRemove.setItemAmount(player.getDuel().getAmountInSlot(slot));
			}
			if (player.getInventory().addItem(itemToRemove)) {
				player.getDuel().deleteItem(itemToRemove);
				p2.getPacketSender().tradeWarning(duelSlot);
			}
		}
		resetStatus();
		refreshDuel();
	}

	private String buildStakedItemsString() {
		if (player.getDuel().freeSlots() == player.getDuel().getSize()) {
			return "<col=FFFFFF>Absolutely nothing!";
		} else {
			StringBuilder bldr = new StringBuilder();
			for (int i = 0; i < player.getDuel().getSize(); i++) {
				Item item = player.getDuel().get(i);
				if (item != null) {
					bldr.append("<col=FF9040>" + item.getDefinition().getName());
					if (item.getCount() > 1) {
						bldr.append(" <col=FFFFFF> x <col=FFFFFF>" + item.getCount());
					}
					bldr.append("<br>");
				}
			}
			return bldr.toString();
		}
	}

	public void logout() {
		if (isStillInChallengeScreen()) {
			declinedDuel();
		} else if (isCommencing()) {
			finishDuel(true, true);
		} else {
			if (getWinner() != null) {
				if (getWinner().equals(player)) {
					recieveWinnings(player);
				}
			}
		}
	}

	private boolean allowedToWithdrawOrDeposit() {
		return state.equals(DUEL_STATE.FIRST_SCREEN_UNACCEPTED) || state.equals(DUEL_STATE.FIRST_SCREEN_ACCEPTED);
	}

	public boolean hasAccepted() {
		return state.equals(DUEL_STATE.SECOND_SCREEN_ACCEPTED) || state.equals(DUEL_STATE.FIRST_SCREEN_ACCEPTED);
	}

	public boolean isStillInChallengeScreen() {
		return state.equals(DUEL_STATE.FIRST_SCREEN_UNACCEPTED) || state.equals(DUEL_STATE.FIRST_SCREEN_ACCEPTED) || state.equals(DUEL_STATE.SECOND_SCREEN_UNACCEPTED) || state.equals(DUEL_STATE.SECOND_SCREEN_ACCEPTED);
	}

	public boolean isCommencing() {
		return state.equals(DUEL_STATE.COMMENCE);
	}

	public boolean isCountingDown() {
		return state.equals(DUEL_STATE.COUNT_DOWN);
	}

	public boolean ruleEnabled(int i) {
		return rules[i];
	}

	public Item[] getWinnings() {
		return winnings;
	}

	public void setWinnings(Item[] items) {
		Item[] winnings = new Item[28];
		int j = 0;
		for (int i = 0; i < items.length; i++) {
			if (items[i] != null) {
				if (items[i].getId() > 0) {
					winnings[j] = items[i];
					j++;
				}
			}
		}
		this.winnings = winnings;
	}

	public void setWinner(Player p) {
		this.winner = p;
	}

	public Player getWinner() {
		return winner;
	}

	public void setConfig(int c) {
		this.config = c;
	}

	private void setRule(int rule, boolean b) {
		this.rules[rule] = b;
	}

	public Player getPlayer2() {
		return p2;
	}

	public boolean isOpponent(Player other) {
		if (other.equals(p2)) {
			return true;
		}
		return false;
	}

	public DUEL_STATE getState() {
		return state;
	}

	public void setState(DUEL_STATE state) {
		this.state = state;
	}
}
