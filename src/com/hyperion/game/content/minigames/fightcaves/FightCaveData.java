package com.hyperion.game.content.minigames.fightcaves;

import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.player.Player;

public class FightCaveData {
	
	public static final Location START_POSITION = Location.create(2413, 5116, 0);

	public static final Location DEATH_POSITION = Location.create(2439, 5169, 0);
	
	private static final int TZ_KIH = 2189;

	public static final int TZ_KEK = 2191;

	private static final int TOX_XIL = 2193;

	private static final int YT_MEJKOT = 3123;

	private static final int KET_ZEK = 3125;

	public static final int TZ_TOK_JAD = 3127;

	private static int[][] WAVES = {
			{-1},//wave 0, ignore..
			{TZ_KIH},
			{TZ_KIH, TZ_KIH},
			{TZ_KEK},
			{TZ_KEK, TZ_KIH},
			{TZ_KEK, TZ_KIH, TZ_KIH},
			{TZ_KEK, TZ_KEK},
			{TOX_XIL},
			{TOX_XIL, TZ_KIH},
			{TOX_XIL, TZ_KIH, TZ_KIH},
			{TOX_XIL, TZ_KEK},
			{TOX_XIL, TZ_KEK, TZ_KIH},
			{TOX_XIL, TZ_KEK, TZ_KIH, TZ_KIH},
			{TOX_XIL, TZ_KEK, TZ_KEK},
			{TOX_XIL, TOX_XIL},
			{YT_MEJKOT},
			{YT_MEJKOT, TZ_KIH},
			{YT_MEJKOT, TZ_KIH, TZ_KIH},
			{YT_MEJKOT, TZ_KEK},
			{YT_MEJKOT, TZ_KEK, TZ_KIH},
			{YT_MEJKOT, TZ_KEK, TZ_KIH, TZ_KIH},
			{YT_MEJKOT, TZ_KEK, TZ_KEK},
			{YT_MEJKOT, TOX_XIL},
			{YT_MEJKOT, TOX_XIL, TZ_KIH},
			{YT_MEJKOT, TOX_XIL, TZ_KIH, TZ_KIH},
			{YT_MEJKOT, TOX_XIL, TZ_KEK},
			{YT_MEJKOT, TOX_XIL, TZ_KEK, TZ_KIH},
			{YT_MEJKOT, TOX_XIL, TZ_KEK, TZ_KIH, TZ_KIH},
			{YT_MEJKOT, TOX_XIL, TZ_KEK, TZ_KEK},
			{YT_MEJKOT, TOX_XIL, TOX_XIL},
			{YT_MEJKOT, YT_MEJKOT},
			{KET_ZEK},
			{KET_ZEK, TZ_KIH},
			{KET_ZEK, TZ_KIH, TZ_KIH},
			{KET_ZEK, TZ_KEK},
			{KET_ZEK, TZ_KEK, TZ_KIH},
			{KET_ZEK, TZ_KEK, TZ_KIH, TZ_KIH},
			{KET_ZEK, TZ_KEK, TZ_KEK},
			{KET_ZEK, TOX_XIL},
			{KET_ZEK, TOX_XIL, TZ_KIH},
			{KET_ZEK, TOX_XIL, TZ_KIH, TZ_KIH},
			{KET_ZEK, TOX_XIL, TZ_KEK},
			{KET_ZEK, TOX_XIL, TZ_KEK, TZ_KIH},
			{KET_ZEK, TOX_XIL, TZ_KEK, TZ_KIH, TZ_KIH},
			{KET_ZEK, TOX_XIL, TZ_KEK, TZ_KEK},
			{KET_ZEK, TOX_XIL, TOX_XIL},
			{KET_ZEK, YT_MEJKOT},
			{KET_ZEK, YT_MEJKOT, TZ_KIH},
			{KET_ZEK, YT_MEJKOT, TZ_KIH, TZ_KIH},
			{KET_ZEK, YT_MEJKOT, TZ_KEK},
			{KET_ZEK, YT_MEJKOT, TZ_KEK, TZ_KIH},
			{KET_ZEK, YT_MEJKOT, TZ_KEK, TZ_KIH, TZ_KIH},
			{KET_ZEK, YT_MEJKOT, TZ_KEK, TZ_KEK},
			{KET_ZEK, YT_MEJKOT, TOX_XIL},
			{KET_ZEK, YT_MEJKOT, TOX_XIL, TZ_KIH},
			{KET_ZEK, YT_MEJKOT, TOX_XIL, TZ_KIH, TZ_KIH},
			{KET_ZEK, YT_MEJKOT, TOX_XIL, TZ_KEK},
			{KET_ZEK, YT_MEJKOT, TOX_XIL, TZ_KEK, TZ_KIH},
			{KET_ZEK, YT_MEJKOT, TOX_XIL, TZ_KEK, TZ_KIH, TZ_KIH},
			{KET_ZEK, YT_MEJKOT, TOX_XIL, TZ_KEK, TZ_KEK},
			{KET_ZEK, YT_MEJKOT, TOX_XIL, TOX_XIL},
			{KET_ZEK, YT_MEJKOT, YT_MEJKOT},
			{KET_ZEK, KET_ZEK},
			{TZ_TOK_JAD}
		};
		
	public static int[] getNPCS(Player player, int wave) {
		int[] mobs = new int[7];
        int mobsAdded = 0;
        for (int i = 0; i < WAVES[wave].length; i++) {
        	mobs[mobsAdded++] = WAVES[wave][i];
        }
        return mobs;
	}
}
