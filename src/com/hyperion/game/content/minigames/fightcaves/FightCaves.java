package com.hyperion.game.content.minigames.fightcaves;

import com.hyperion.game.content.miniquests.impl.FightCaveSession;
import com.hyperion.game.world.entity.player.Player;

public class FightCaves extends FightCaveData {

	public static void enter(Player player) {
		FightCaveSession game_instance = new FightCaveSession(player, false, (player.getDetails().isDonator() ? 31 : 0));
		if(player.getDetails().isDonator()) {
			player.getPacketSender().sendMessage("As you are a donator, you have been placed on wave 31.");
		}
		game_instance.enter(player);
		player.getVariables().setCurrentRoundMiniquest(game_instance);
		player.getAttributes().set("cavesession", true);
		player.resetVariables();
		player.setCaveSession(game_instance);
	}
	
	public static void exit(Player player) {
		if (player.getVariables().getCurrentRoundMiniquest() != null) {
			player.getVariables().getCurrentRoundMiniquest().end(player, false, false, false);
		} else {
			player.teleport(DEATH_POSITION);
			player.resetVariables();
		}
	}
}
