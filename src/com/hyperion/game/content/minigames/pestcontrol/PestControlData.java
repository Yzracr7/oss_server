package com.hyperion.game.content.minigames.pestcontrol;

import com.hyperion.game.world.Location;

public class PestControlData {
	
	protected static final int MAX_PLAYERS = 10;
	
	protected static final Location PLAYERS_START_AREA = Location.create(3543, 3258, 0);
	
	protected static final Location[] ZOMBIE_SPAWNS = {
		Location.create(3536, 3259, 0),
		Location.create(3547, 3249, 0)
	};
}
