package com.hyperion.game.content.minigames.pestcontrol;

import java.util.ArrayList;
import java.util.List;

import com.hyperion.game.content.QuestTab;
import com.hyperion.game.content.achievements.*;
import com.hyperion.game.content.minigames.AbstractGameSession;
import com.hyperion.game.content.minigames.GameBounds;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.region.MapBuilder;
import com.hyperion.utility.NumberUtils;

/**
 * @author Nando
 */
public class PestSession extends AbstractGameSession {

	private int round = 1;
	private int currentRoundTime = 600, pausedTimer = 11;
	private List<NPC> zombiesAlive = new ArrayList<NPC>();
	private boolean paused = true, roundDefeated = false, zombieMarked = false;
	private Location baseLocation = null;
	
	private int barbariansKilled = 0;
	private NPC voidKnight;

	private int[] ZOMBIE_IDS = { 73, 73, 73, 73, 73, 73, 73, 73, 750, 749, };

	private Player[] team;

	public PestSession(int limit) {
		this.team = new Player[limit];
		Location north_east = Location.create(3584, 3263, 0);
		Location south_west = Location.create(3535, 3223, 0);
		int width = (north_east.getX() - south_west.getX()) / 8 + 2;
		int height = (north_east.getY() - south_west.getY()) / 8 + 1;
		int[] newCoords = MapBuilder.findEmptyChunkBound(width, height);
		MapBuilder.copyAllPlanesMap(south_west.getChunkX(), south_west.getChunkY(), newCoords[0], newCoords[1], width, height);
		baseLocation = Location.create(newCoords[0] << 3, newCoords[1] << 3, 0);
		baseLocation.setWidth(width);
		baseLocation.setHeight(height);
		voidKnight = new NPC(4657);
		voidKnight.setDirection(1);
		Location voidKnightLoc = Location.create(starting_placement().getLowestX() + 2, starting_placement().getLowestY(), 0);
		voidKnight.setLocation(voidKnightLoc);
		voidKnight.setLastKnownRegion(voidKnightLoc);
		voidKnight.getAttributes().set("pestsession", this);
		World.getWorld().addNPC(voidKnight);
	}

	private void spawn_next_wave() {
		int current_wave = round;
		int amountToLoop = 6;
		if (current_wave >= 8) {
			amountToLoop = 7;
		} else if (current_wave >= 10) {
			amountToLoop = 8;
		}
		if (current_wave >= 7) {
			amountToLoop = 10;
		}
		Location start = Location.create(baseLocation.getX() + 15, baseLocation.getY() + 41, 0);
		Location[] ZOMBIE_SPAWNS = { Location.create(start.getX() - 7, start.getY(), 0), Location.create(start.getX() + 4, start.getY() - 7, 0), Location.create(start.getX() + 9, start.getY() - 14, 0), Location.create(start.getX() + 25, start.getY() - 13, 0), Location.create(start.getX() + 5, start.getY() - 15, 0), Location.create(start.getX() + 16, start.getY() - 23, 0), Location.create(start.getX() + 18, start.getY() - 14, 0), Location.create(start.getX() + 19, start.getY() - 18, 0), Location.create(start.getX() - 1, start.getY() - 30, 0),// mage
																																																																																																																																									// spirit
				Location.create(start.getX() + 29, start.getY() - 2, 0),// melee
																		// spirit
		};
		for (int i = 0; i < amountToLoop; i++) {
			NPC zombie = new NPC(ZOMBIE_IDS[i]);
			addMonster(zombie);
			zombie.setLocation(ZOMBIE_SPAWNS[i]);
			zombie.setLastKnownRegion(ZOMBIE_SPAWNS[i]);
			zombie.getAttributes().set("pestsession", this);
			zombie.getAttributes().set("pestzombie", true);
			World.getWorld().addNPC(zombie);
		}

	}

	public int getPointsForRound() {
		/*
		 * int points = 0; for (int i = 1; i < round + 1; i++) { points += i; }
		 */
		/*
		 * if(round > 50) return round * 2; if(round > 100) return round * 3;
		 * //round 100 = 300 points return round;
		 */
		if(round <= 4)
			return 0; //to fix leave and enterInstancedRoom dupe
		return round * 10 + (barbariansKilled * 7);
	}

	@Override
	public Player[] getTeam() {
		return team;
	}

	@Override
	public void start_game() {
		session_timer = new Tickable(2) {
			@Override
			public void execute() {
				if (members_active() <= 0) {
					end_game();
					return;
				}
				if (!paused) {
					if (isRoundDefeated()) {
						set_round_defeated(false);
					}
					if (getZombiesAlive().size() <= 0) {
						increase_round();
						set_round_defeated(true);
						paused = true;
					} else {
						if (!isZombieMarked()) {
							if (getZombiesAlive().size() == 1) {
								setZombieMarked(true);
								for (Player players : getTeam()) {
									if (players != null) {
										for (NPC last : getZombiesAlive()) {
											if (last != null) {
												players.getPacketSender().sendHintArrow(last);
											}
										}
									}
								}
							}
						}
					}
				}
				if (paused) {
					if (pausedTimer > 0) {
						pausedTimer--;
					} else {
						spawn_next_wave();
						paused = false;
						pausedTimer = 4;
						reset_round_limit();
					}
				} else if (!paused) {
					if (getCurrentRoundTime() > 0) {
						deduct_round_timer();
					} else if (getCurrentRoundTime() <= 0) {
						end_game();
						return;
					}
				}
				for (Player players : getTeam()) {
					if (players != null) {
						players.getPacketSender().modifyText("<col=52D017>Round: " + getRound(), 133, 0);
						if (!paused)
							players.getPacketSender().modifyText("<col=52D017>Time left: " + NumberUtils.getMinutesDisplay(getCurrentRoundTime()), 133, 1);
						else
							players.getPacketSender().modifyText("<col=52D017>Starts in: " + getPausedTimer(), 133, 1);
						players.getPacketSender().modifyText("<col=52D017>Remaining Zombies: " + getZombiesAlive().size(), 133, 2);
					}
				}
			}
		};
		super.start_game();
	}

	@Override
	public void end_game() {
		super.end_game();
		for (NPC npcs : getZombiesAlive()) {
			if (npcs != null) {
				World.getWorld().removeNPC(npcs);
				if (voidKnight != null)
					World.getWorld().removeNPC(voidKnight);
			}
		}
		World.getWorld().submit(new Tickable(2) {
			@Override
			public void execute() {
				MapBuilder.destroyMap(baseLocation.getChunkX(), baseLocation.getChunkY(), baseLocation.getWidth(), baseLocation.getHeight());
				this.stop();
			}
		});
	}

	@Override
	public void insert_player(Player player) {
		super.insert_player(player);
		player.getAttributes().set("pestsession", this);
		player.getPacketSender().modifyText("", 133, 3);
		player.getPacketSender().modifyText("<col=52D017>Round: " + getRound(), 133, 0);
		player.getPacketSender().modifyText("<col=52D017>Starts in: " + getPausedTimer(), 133, 1);
		player.getPacketSender().modifyText("", 133, 4);
		player.getPacketSender().modifyText("<col=52D017>Remaining Zombies: " + getZombiesAlive().size(), 133, 2);
		player.getPacketSender().modifyText("", 133, 5);
		player.getPacketSender().sendOverlay(133);
		player.getPacketSender().sendMessage("Round starts in 10 seconds!");
	}

	@Override
	public void remove_player(Player player) {
		super.remove_player(player);
	}
	
	@Override
	public void handle_death(Entity deadEntity, Entity killer) {
		if (deadEntity.isNPC()) {
			removeMonster((NPC) deadEntity);
			if(((NPC)deadEntity).getId() >= 749) {
				barbariansKilled++;
				for(Player players : team) {
					if(players != null)
						players.getPacketSender().sendMessage("You receive 7 bonus points for killing a Barbarian.");
				}
			}
			World.getWorld().removeNPC((NPC) deadEntity);
			if (getZombiesAlive().size() <= 0) {
				setZombieMarked(false);
				for (Player players : getTeam()) {
					if (players != null) {
						players.getPacketSender().sendHintArrow(null);
					}
				}
			}
			return;
		}
		Player player = (Player) deadEntity;
		remove_player(player);
		player.getVariables().setGameSession(null);
		player.getVariables().increasePestPoints(getPointsForRound());
		Achievements.increase(player, 16);
		Achievements.increase(player, 32);
		if (getRound() >= 7)
			Achievements.increase(player, 24);
		player.teleport(death_placement());
	}

	@Override
	public void handle_logout(Player player) {
		super.handle_logout(player);
		player.getVariables().increasePestPoints(getPointsForRound());
	}

	@Override
	public Tickable session_timer() {
		return session_timer;
	}

	@Override
	public GameBounds starting_placement() {
		int start_x = baseLocation.getX() + 15;
		int start_y = baseLocation.getY() + 41;
		int lowestX = start_x - 1, lowestY = start_y - 1;
		int highestX = start_x + 1, highestY = start_y + 2;
		return new GameBounds(lowestX, lowestY, highestX, highestY);
	}

	@Override
	public Location death_placement() {
		return Location.create(2657, 2649, 0);
	}

	@Override
	public Location end_area() {
		return Location.create(2657, 2639, 0);
	}

	@Override
	public boolean items_are_safe() {
		return true;
	}

	public int getRound() {
		return round;
	}

	private void increase_round() {
		this.round++;
	}

	public void set_round(int round) {
		this.round = round;
	}

	public int getCurrentRoundTime() {
		return currentRoundTime;
	}

	private void deduct_round_timer() {
		this.currentRoundTime--;
	}

	private void reset_round_limit() {
		this.currentRoundTime = 600;
	}

	public void set_current_round_time(int currentRoundTime) {
		this.currentRoundTime = currentRoundTime;
	}

	public boolean isPaused() {
		return paused;
	}

	public void set_paused(boolean paused) {
		this.paused = paused;
	}

	public int getPausedTimer() {
		return pausedTimer;
	}

	public void set_paused_timer(int pausedTimer) {
		this.pausedTimer = pausedTimer;
	}

	public List<NPC> getZombiesAlive() {
		return zombiesAlive;
	}

	private void addMonster(NPC npc) {
		this.zombiesAlive.add(npc);
	}

	private void removeMonster(NPC npc) {
		this.zombiesAlive.remove(npc);
	}

	public boolean isRoundDefeated() {
		return roundDefeated;
	}

	private void set_round_defeated(boolean roundDefeated) {
		this.roundDefeated = roundDefeated;
		if (roundDefeated) {
			for (Player players : getTeam()) {
				if (players != null) {
					int current_best_wave = players.getVariables().getPestRound();
					int current_wave = getRound();
					if (current_wave > current_best_wave) {
						players.getVariables().setPestRound(current_wave);
						QuestTab.refresh(players);
						players.getPacketSender().sendMessage("Congratulations! You've reached a new personal highest round achieved!");
					}
				}
			}
		}
	}

	public boolean isZombieMarked() {
		return zombieMarked;
	}

	public void setZombieMarked(boolean zombieMarked) {
		this.zombieMarked = zombieMarked;
	}
}
