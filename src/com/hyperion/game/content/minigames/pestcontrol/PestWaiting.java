package com.hyperion.game.content.minigames.pestcontrol;

import java.util.ArrayList;
import java.util.List;

import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.player.Player;

public class PestWaiting {

	public static void displayBoatSettings() {
		boolean startNewGame = false;
		PestSession next_game = null;
		if (getNextDeparture() == -1) {
			if (waiters.size() > 0) {//ppl r waiting
				startNewGame = true;
			}
		}
		List<Player> waitersCopy = new ArrayList<Player>(waiters);
		if (startNewGame) {
			if (waitersCopy.size() >= 10) {
				next_game = new PestSession(10);
			} else if (waitersCopy.size() > 0 && waitersCopy.size() < 10) {
				next_game = new PestSession(waitersCopy.size());
			}
		}
		for (Player players : waitersCopy) {
			if (players != null) {
				if (!players.getCombatState().isDead()) {
					players.getPacketSender().modifyText("", 133, 3);
					players.getPacketSender().modifyText("", 133, 0);
					players.getPacketSender().modifyText("<col=52D017>Next Departure: " + getNextDeparture(), 133, 1);
					players.getPacketSender().modifyText("", 133, 4);
					players.getPacketSender().modifyText("<col=52D017>Points: " + players.getVariables().getPestPoints(), 133, 2);
					players.getPacketSender().modifyText("", 133, 5);
					if (startNewGame && next_game.members_active() < 10) {
						players.getPacketSender().sendRemoveOverlay();
						waiters.remove(players);
						next_game.insert_player(players);
						players.getVariables().setGameSession(next_game);
					}
				}
			}
		}
		if (startNewGame) {
			next_game.start_game();
		}
		if (getNextDeparture() > -1) {
			deductNextDeparture(1);
		} else {
			resetNextDeparture();
		}
	}
	
	public static void addPlayer(Player player) {
		if (!getWaiters().contains(player)) {
			getWaiters().add(player);
			player.getPacketSender().modifyText("", 133, 3);
			player.getPacketSender().modifyText("", 133, 0);
			player.getPacketSender().modifyText("<col=52D017>Next Departure: " + getNextDeparture(), 133, 1);
			player.getPacketSender().modifyText("", 133, 4);
			player.getPacketSender().modifyText("<col=52D017>Points: " + player.getVariables().getPestPoints(), 133, 2);
			player.getPacketSender().modifyText("", 133, 5);
			player.getPacketSender().sendOverlay(133);
			player.teleport(2661, 2639, 0);
			player.resetVariables();
		}
	}
	
	public static void removePlayer(Player player, boolean logged) {
		if (getWaiters().contains(player)) {
			getWaiters().remove(player);
			if (logged) {
				player.setLocation(Location.create(2657, 2639, 0));
			} else {
				player.teleport(2657, 2639, 0);
				player.getPacketSender().sendRemoveOverlay();
			}
		}
	}
	
	private static int nextDeparture = 30;
	private static ArrayList<Player> waiters = new ArrayList<Player>();

	private static ArrayList<Player> getWaiters() {
		return waiters;
	}

	private static int getNextDeparture() {
		return nextDeparture;
	}

	private static void deductNextDeparture(int amt) {
		nextDeparture -= amt;
	}
	
	private static void resetNextDeparture() {
		nextDeparture = 30;
	}
}
