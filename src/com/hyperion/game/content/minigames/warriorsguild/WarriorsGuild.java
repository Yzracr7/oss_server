package com.hyperion.game.content.minigames.warriorsguild;

import com.hyperion.game.Constants;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;

public class WarriorsGuild {

	/**
	 * The token id
	 */
	private static final int TOKENS = 8851;

	private static final int ANIMATOR = 15621;

	/**
	 * Location of the animated armour spawns
	 */
	private static final Location[] SPAWNS = { Location.create(2851, 3536, 0), Location.create(2857, 3536, 0) };

	/**
	 * The armour sets
	 */
	private static final int[][] ARMOURS = { { 1155, 1117, 1075 }, // Bronze 0
			{ 1153, 1115, 1067 }, // Iron 1
			{ 1157, 1119, 1069 }, // Steel 2
			{ 1165, 1125, 1077 }, // Black 3
			{ 1159, 1121, 1071 }, // Mithril 4
			{ 1161, 1123, 1073 }, // Adamant 5
			{ 1163, 1127, 1079 } // Rune 6
	};

	/**
	 * The list of animated armours that correspond with the armour-type used on
	 * the animator
	 */
	private static final int[] ANIMATED_ARMOURS = { 4278, // Animated Bronze
															// Armour
			4279, // Animated Iron Armour
			4280, // Animated Steel Armour
			4281, // Animated Black Armour
			4282, // Animated Mithril Armour
			4283, // Animated Adamant Armour
			4284, // Animated Rune Armour
	};

/*	private static final int[] TOKEN_AMOUNT = { 5, // bronze
			10, // iron
			15, // steel
			20, // black
			25, // mithril
			30, // adamant
			40, // rune
	};
*/
	/**
	 * An array for the defenders
	 */
	public static final int[] DEFENDERS = { 
			8850, // rune
			8849, // addy
			8848, // mithril
			8847, // black
			8846, // steel
			8845, // iron
			8844,// bronze
	};

	/**
	 * Corresponding to the rest of the arrays - armour names -
	 */
	private static final String[] ARMOUR_NAMES = { "Bronze", "Iron", "Steel", "Black", "Mithril", "Adamant", "Rune" };

	/**
	 * Gets the current defender
	 * 
	 * @param player
	 *            the player
	 * @return the defender
	 */
/*	private static int getCurrentDefender(final Player player) {
		for (int i = 0; i < DEFENDERS.length; i++) {
			if (player.getInventory().hasItem(DEFENDERS[i]) || player.getEquipment().hasItem(DEFENDERS[i])) {
				return DEFENDERS[i];
			}
		}
		return 8844;
	}*/

	/**
	 * Gets the next defender
	 * 
	 * @param player
	 *            the player
	 * @return the defender
	 */
/*	public static int getNextDefender(final Player player) {
		int current = getCurrentDefender(player);
		return  current == 8850 ? 18662 : DEFENDERS[8849 - current];
	}
*/
	/**
	 * Using the east animator object
	 * 
	 * @param player
	 * @return
	 */
	private static boolean isEastAnimator(GameObject object) {
		return object.getLocation().getX() == 2857 && object.getLocation().getY() == 3536 && object.getLocation().getZ() == 0;
	}

	/**
	 * Using the west animator object?
	 * 
	 * @param player
	 * @return
	 */
	public static boolean isWestAnimator(Player player) {
		return player.getLocation().getX() == 2851 && player.getLocation().getY() == 3536 && player.getLocation().getZ() == 0;
	}

	public static boolean canAnimate(Player player, GameObject object, int itemId) {
		if (World.getWorld().isUpdateInProgress()) {
			player.getPacketSender().sendMessage("To avoid loss of items, this is disabled while the world is updating.");
			return false;
		}
		if (!Constants.MINIGAME) {
			player.getPacketSender().sendMessage("Minigames are currently disabled!");
			return false;
		}
		int objectId = object.getId();
		if (objectId != ANIMATOR) {
			return false;
		}
		int index = hasSet(player, itemId);
		spawnNPC(player, index, object);
		return true;
	}

	private static void spawnNPC(final Player player, final int index, final GameObject object) {
		if (index == -1) {
			return;
		}
		player.getPacketSender().sendChatboxDialogue(false, "You place the armour on the animator and step back...");
		player.animate(827);
		player.getAttributes().set("stopMovement", true);
		for (int i = 0; i < ARMOURS[index].length; i++) {
			player.getInventory().deleteItem(ARMOURS[index][i]);
		}
		World.getWorld().submit(new Tickable(2) {
			int i = 0;
			NPC npc = null;

			@Override
			public void execute() {
				if (i == 0) {
					this.setTickDelay(1);
				} else if (i == 1) {
					final int x = player.getLocation().getX();
					final int y = player.getLocation().getY();
					player.getWalkingQueue().addStep(x, y + 3);
					player.getWalkingQueue().finish();
					player.face(object.getLocation().getX(), object.getLocation().getY(), 0);
					this.setTickDelay(3);
					player.getPacketSender().closeChatboxInterface();
				} else if (i == 2) {
					this.setTickDelay(1);
					Location getLoc = isEastAnimator(object) ? SPAWNS[1] : SPAWNS[0];
					npc = new NPC(ANIMATED_ARMOURS[index]);
					player.getVariables().addOwnedNPC(npc);
					npc.setLocation(getLoc);
					npc.playForcedChat("I'm ALIVE!");
					npc.animate(4166);
					World.getWorld().addNPC(npc);
					final int x = npc.getLocation().getX();
					final int y = npc.getLocation().getY();
					npc.getWalkingQueue().addStep(x, y + 2);
					npc.getWalkingQueue().finish();
					player.getPacketSender().sendHintArrow(npc);
				} else {
					this.stop();
					npc.getCombatState().startAttack(player);
					player.setInteractingEntity(npc);
					player.getPacketSender().softCloseInterfaces();
					player.getAttributes().remove("stopMovement");
				}
				i++;
			}
		});
	}

/*	public static void handleLoot(Player player, NPC npc) {
		int slot = -1;
		for (int i = 0; i < ANIMATED_ARMOURS.length; i++) {
			if (npc.getId() == ANIMATED_ARMOURS[i]) {
				slot = i;
			}
		}
		for (int j = 0; j < ARMOURS[slot].length; j++) {
			GroundItem pieces = new GroundItem(new Item(ARMOURS[slot][j], 1), npc, player);
			GroundItemManager.create(pieces, player);
		}
		GroundItem tokens = new GroundItem(new Item(TOKENS, TOKEN_AMOUNT[slot]), npc, player);
		GroundItemManager.create(tokens, player);
	}*/

	private static int hasSet(Player p, int itemId) {
		int itemIndex = -1;
		for (int i = 0; i < ARMOURS.length; i++) {
			for (int j = 0; j < ARMOURS[i].length; j++) {
				if (itemId == ARMOURS[i][j]) {
					itemIndex = i;
					for (int k = 0; k < ARMOURS[i].length; k++) {
						if (!p.getInventory().hasItem(ARMOURS[i][k])) {
							p.getPacketSender().sendMessage("You need a full set of " + ARMOUR_NAMES[i] + " armour to use the animator.");
							return -1;
						}
					}
					break;
				}
			}
		}
		return itemIndex;
	}
}
