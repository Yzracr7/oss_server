package com.hyperion.game.content.miniquests;

import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

public class AbstractRoundBasedMiniquest implements RoundBasedMiniquest {

	@Override
	public void enter(Player player) {
		
	}

	@Override
	public void nextRound(Player player, NPC npc) {
		
	}

	@Override
	public void end(Player player, boolean won_game, boolean died, boolean logged) {
		
	}
	
	@Override
	public String nameOfQuest() {
		return null;
	}

}
