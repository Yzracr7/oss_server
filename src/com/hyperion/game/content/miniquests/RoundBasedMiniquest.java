package com.hyperion.game.content.miniquests;

import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

public interface RoundBasedMiniquest {

	public void enter(final Player player);
	
	public void nextRound(Player player, NPC npc);
	
	public void end(final Player player, boolean won_game, boolean died, boolean logged);
	
	public String nameOfQuest();
}
