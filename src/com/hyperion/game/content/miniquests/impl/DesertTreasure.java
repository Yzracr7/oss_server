package com.hyperion.game.content.miniquests.impl;

import com.hyperion.game.content.QuestTab;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

public class DesertTreasure {
	
	public static void enterDessous(Player player) {
		player.resetVariables();
		Location instanceLocation = Location.create(3570, 3404, player.getLocation().getZ());
		NPC npc = new NPC(1915);
		npc.setLocation(instanceLocation);
		npc.setLastKnownRegion(instanceLocation);
		player.getVariables().addOwnedNPC(npc);
		World.getWorld().addNPC(npc);
		player.getPacketSender().sendHintArrow(npc);
		npc.getCombatState().startAttack(player);
	}
	
	public static void enterKamil(Player player) {
		player.resetVariables();
		Location instanceLocation = Location.create(2841, 3809, 2);
		NPC npc = new NPC(1913);
		npc.setLocation(instanceLocation);
		npc.setLastKnownRegion(instanceLocation);
		player.getVariables().addOwnedNPC(npc);
		World.getWorld().addNPC(npc);
		player.getPacketSender().sendHintArrow(npc);
		npc.getCombatState().startAttack(player);
	}
	
	public static void enterFareed(Player player) {
		player.resetVariables();
		Location instanceLocation = Location.create(3315, 9376, player.getLocation().getZ());
		NPC npc = new NPC(1977);
		npc.setLocation(instanceLocation);
		npc.setLastKnownRegion(instanceLocation);
		player.getVariables().addOwnedNPC(npc);
		World.getWorld().addNPC(npc);
		player.getPacketSender().sendHintArrow(npc);
		npc.getCombatState().startAttack(player);
	}
	
	public static void enterDamis(Player player) {
		player.resetVariables();
		Location instanceLocation = Location.create(2740, 5096, player.getLocation().getZ());
		NPC npc = new NPC(1974);
		npc.setLocation(instanceLocation);
		npc.setLastKnownRegion(instanceLocation);
		player.getVariables().addOwnedNPC(npc);
		World.getWorld().addNPC(npc);
		player.getPacketSender().sendHintArrow(npc);
		npc.getCombatState().startAttack(player);
	}
	
	private static void enterDamis2(Player player, Location loc) {//2nd spawns where 1st died,
		NPC npc = new NPC(1975);
		npc.setLocation(loc);
		npc.setLastKnownRegion(loc);
		player.getVariables().addOwnedNPC(npc);
		World.getWorld().addNPC(npc);
		player.getPacketSender().sendHintArrow(npc);
		npc.getCombatState().startAttack(player);
	}
	
	public static void handleDtNPCDeath(NPC npc, Player player) {
		player.getPacketSender().sendHintArrow(null);
		switch (npc.getId()) {
		case 1915://Dessous
			player.getVariables().setDessousKilled(true);
			break;
		case 1913://Kamil
			player.getVariables().setKamilKilled(true);
			break;
		case 1977://Fareed
			player.getVariables().setFareedKilled(true);
			break;
		case 1974://Damis v1
			enterDamis2(player, npc.getLocation());
			break;
		case 1975://Damis v2
			player.getVariables().setDamisKilled(true);
			break;
		}
		if(player.getVariables().isDtFinished())
			QuestTab.openQuestComplete(player, 4);
	}
	
	public static boolean isDtNPC(NPC npc) {
		switch (npc.getId()) {
		case 1915:
		case 1913:
		case 1977:
		case 1975:
		case 1974:
			return true;
		}
		return false;
	}
}
