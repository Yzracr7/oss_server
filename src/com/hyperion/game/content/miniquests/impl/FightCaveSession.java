package com.hyperion.game.content.miniquests.impl;

import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.content.dialogue.DialogueManager.FacialAnimation;
import com.hyperion.game.content.minigames.fightcaves.FightCaveData;
import com.hyperion.game.content.miniquests.AbstractRoundBasedMiniquest;
import com.hyperion.game.content.miniquests.RoundBasedMiniquest;
import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.grounditems.GroundItem;
import com.hyperion.game.world.grounditems.GroundItemManager;
import com.hyperion.game.world.pathfinders.DefaultPathFinder;
import com.hyperion.game.world.pathfinders.TileControl;
import com.hyperion.game.world.region.MapBuilder;
import com.hyperion.utility.NumberUtils;

import java.util.ArrayList;

/**
 * @author Nando
 */
public class FightCaveSession extends AbstractRoundBasedMiniquest {

    private Player player;
    private ArrayList<NPC> currentWaveNpcs = new ArrayList<NPC>();
    private byte currentWave;
    private boolean resumed, waveStarted, waveCompleted, gameStopped, completedGame, healersSpawned;
    private Location baseLocation = null, start = null;

    public FightCaveSession(Player player, boolean resumed, int wave) {
        this.player = player;
        this.currentWave = (byte) wave;
        this.waveCompleted = true;//starts next wave right away.
        this.resumed = resumed;

        //new additions
        enter(player);
    }

    @Override
    public void enter(final Player player) {
        Location north_east = Location.create(2422, 5117, 0);
        Location south_west = Location.create(2371, 5062, 0);
        int width = (north_east.getX() - south_west.getX()) / 8 + 5;
        int height = (north_east.getY() - south_west.getY()) / 8 + 2;
        int[] newCoords = MapBuilder.findEmptyChunkBound(width, height);
        MapBuilder.copyAllPlanesMap(south_west.getChunkX(), south_west.getChunkY(), newCoords[0], newCoords[1], width, height);
        baseLocation = Location.create(newCoords[0] << 3, newCoords[1] << 3, 0);
        baseLocation.setWidth(width);
        baseLocation.setHeight(height);
        start = Location.create(baseLocation.getX() + 45, baseLocation.getY() + 60, 0);
        player.getVariables().setSessionName("fightcaves");
        player.teleport(start);
        System.out.println("Region created on " + start.getX() + " " + start.getY());
		//player.getPacketSender().sendNPCHead(2617, 242, 0);
		player.getPacketSender().modifyText("TzHaar-Mej-Jal", 242, 1);
		player.getPacketSender().modifyText((resumed ? "Welcome back to the Fight caves, " :"You're on your own now, ") +player.getName()+".", 242, 2);
		player.getPacketSender().modifyText("Prepare to fight for your life!", 242, 3);
		//player.getPacketSender().animateInterface(FacialAnimation.DEFAULT.getAnimation().getId(), 242, 0);
		player.getPacketSender().sendChatboxInterface(242);
		player.face(player.getLocation().getX(), player.getLocation().getY() - 1, 0);
		player.execute_path(new DefaultPathFinder(), player.getLocation().getX(), player.getLocation().getY() - 10);
		player.getAttributes().set("nextDialogue", -1);
		World.getWorld().submit(new Tickable(2, false) {
			@Override
			public void execute() {
				if (player.isDestroyed() || !player.getAttributes().isSet("cavesession")) {
					remove_game_monsters();
					setGameStopped(true);
					this.stop();
					return;
				}
				if (waveCompleted) {
					increaseWave(1);
					player.getVariables().setFightCavesWave(getCurrentWave() > 0 ? getCurrentWave() : 1);
					player.getPacketSender().sendMessage("Wave "+getCurrentWave()+" will start soon.");
					return;
				} else if (completedGame) {
					end(player, true, false, false);
					this.stop();
					return;
				}
				if (!waveCompleted && !isGameStopped() && !isWaveStarted() && getCurrentWaveNpcs().size() <= 0) {
					startNextWave();
				}
			}
		});
    }

    @Override
    public void nextRound(Player player, NPC deadNPC) {
        int[] lvl45s = {2630, FightCaveData.TZ_KEK, 2737};
        for (int i = 0; i < lvl45s.length; i++) {
            if (deadNPC.getId() == lvl45s[i]) {//lvl 45s Spawn two little guys after.
                for (int j = 0; j < 2; j++) {
                    NPC nextForms = new NPC(2738);
                    getCurrentWaveNpcs().add(nextForms);
                    Location loc = deadNPC.getLocation();
                    nextForms.setLocation(loc);
                    nextForms.setLastKnownRegion(loc);
                    player.getVariables().addOwnedNPC(nextForms);
                    nextForms.getAttributes().set("cavesession", true);
                    World.getWorld().addNPC(nextForms);
                    nextForms.getCombatState().startAttack(player);
                }
                break;
            }
        }
        removeWaveNPC(deadNPC);
        if (getCurrentWave() >= 30)
            Achievements.set(player, 26, getCurrentWave());
        if (deadNPC.getId() == FightCaveData.TZ_TOK_JAD) {
            this.completedGame = true;
        } else {
            if (!this.waveCompleted) {
                if (getCurrentWaveNpcs().size() <= 0) {
                    this.waveCompleted = true;
                }
            }
        }
    }

    @Override
    public void end(Player player, boolean won_game, boolean died, boolean logged) {
        if (logged) {
            player.getVariables().setFightCavesWave(this.getCurrentWave() > 0 ? this.getCurrentWave() : 1);
        } else {
            player.getVariables().setSessionName(null);
            player.teleport(FightCaveData.DEATH_POSITION);
            player.getVariables().setFightCavesWave(-1);
            int wave = this.getCurrentWave();
            String lineOne = null;
            String lineTwo = null;
            int chatboxId = 241;
            if (won_game) {
                lineOne = "You have defeated TzTok-Jad, I am most impressed!";
                lineTwo = "Please accept this gift as a reward.";
                chatboxId = 242;
                if (!player.getInventory().addItem(new Item(6570, 1))) {
                    GroundItem groundItem = new GroundItem(new Item(6570, 1), player);
                    GroundItemManager.create(groundItem, player);
                } else {
                    player.getInventory().refresh();
                }
                if (NumberUtils.random(0, 155) >= 154 && player.getInventory().addItem(14422, 1)) {
                    player.getPacketSender().sendMessage("You have received TzTok-Jad boss pet for your efforts!");
                    player.getInventory().refresh();
                }
                Achievements.increase(player, 44);
            } else {
                if (wave > 1) {
                    lineOne = "Well done in the cave, here, take Tokkul as a reward.";
                } else {
                    lineOne = "Well I suppose you tried... better luck next time.";
                }
            }
            if (wave > 1) {
                int tokkul = wave > 50 ? wave * 175 : wave > 30 ? wave * (player.getDetails().isDonator() ? 5 : 150) : wave * 50;
                if (!player.getInventory().addItem(new Item(6529, tokkul))) {
                    GroundItem groundItem = new GroundItem(new Item(6529, tokkul), player);
                    GroundItemManager.create(groundItem, player);
                } else {
                    player.getInventory().refresh();
                }
                player.getPacketSender().sendMessage("You receive " + NumberUtils.format(tokkul) + " tokkul.");
            }
            player.getPacketSender().sendNPCHead(2617, chatboxId, 0);
            player.getPacketSender().modifyText("TzHaar-Mej-Jal", chatboxId, 1);
            player.getPacketSender().modifyText(lineOne, chatboxId, 2);
            if (chatboxId == 242) {
                player.getPacketSender().modifyText(lineTwo, chatboxId, 3);
            }
            player.getPacketSender().animateInterface(FacialAnimation.DEFAULT.getAnimation().getId(), chatboxId, 0);
            player.getPacketSender().sendChatboxInterface(chatboxId);
        }
        //int wave = this.getCurrentWave();
        //player.getVariables().setFightCavesWave(wave); //Is this necessary?
        player.setCaveSession(null);
        player.getAttributes().set("nextDialogue", -1);
        player.getAttributes().remove("cavesession");
        player.getVariables().setCurrentRoundMiniquest(null);
        player.resetVariables();
        World.getWorld().submit(new Tickable(2) {
            @Override
            public void execute() {
                MapBuilder.destroyMap(baseLocation.getChunkX(), baseLocation.getChunkY(), baseLocation.getWidth(), baseLocation.getHeight());
                this.stop();
            }
        });
    }

    @Override
    public String nameOfQuest() {
        return "Fight Caves";
    }

    private void startNextWave() {
        this.setWaveStarted(true);
        World.getWorld().submit(new Tickable(1, true) {
            int count = 15;

            @Override
            public void execute() {
                if (isGameStopped()) {
                    this.stop();
                    return;
                }
                if (count > 0) {
                    count--;
                    return;
                }
                spawnNextWave();
                this.stop();
            }
        });
    }

    private void spawnNextWave() {
        final int wave = this.getCurrentWave();
        if (wave == 63) {
           // player.getPacketSender().sendNPCHead(2617, 241, 0);
            player.getPacketSender().modifyText("TzHaar-Mej-Jal", 241, 1);
            player.getPacketSender().modifyText("Look out, here comes TzTok-Jad!", 241, 2);
           // player.getPacketSender().animateInterface(FacialAnimation.DEFAULT.getAnimation().getId(), 241, 0);
            player.getPacketSender().sendChatboxInterface(241);
            player.getAttributes().set("nextDialogue", -1);
        }
        int[] mobs = FightCaveData.getNPCS(player, wave);
        for (int i = 0; i < mobs.length; i++) {
            if (mobs[i] > 0) {
                NPC waveSpawn = new NPC(mobs[i]);
                player.getVariables().addOwnedNPC(waveSpawn);
                Location placement = Location.create((start.getX() - 14) - NumberUtils.random(8), (start.getY() - 31) - NumberUtils.random(8), 0);
                while (TileControl.getSingleton().locationOccupied(player, placement.getX(), placement.getY(), 0)) {
                    placement = Location.create((start.getX() - 14) - NumberUtils.random(8), (start.getY() - 31) - NumberUtils.random(8), 0);
                }
                waveSpawn.setLocation(placement);
                waveSpawn.setLastKnownRegion(placement);
                getCurrentWaveNpcs().add(waveSpawn);
                waveSpawn.getAttributes().set("cavesession", true);
                World.getWorld().addNPC(waveSpawn);
                waveSpawn.getCombatState().startAttack(player);
                System.out.println("CAVE SESSION "+waveSpawn);
            }
        }
    }

    public static void summonHealers(final Player player, final NPC jad) {
        FightCaveSession session = player.getFightCaveSession();
        if (session != null && !session.isHealersSpawned()) {
            session.setHealersSpawned(true);
            for (int i = 0; i < 4; i++) {
                final NPC healers = new NPC(2746);
                int random = NumberUtils.random(1);
                Location placement = Location.create(session.start.getX() - 19, session.start.getY() - 49, 0);
                Location loc = Location.create(random == 0 ? placement.getX() + NumberUtils.random(5) : placement.getX() - NumberUtils.random(5), placement.getY(), 0);
                healers.setLocation(loc);
                healers.setLastKnownRegion(loc);
                player.getVariables().addOwnedNPC(healers);
                healers.getAttributes().set("cavesession", true);
                World.getWorld().addNPC(healers);
                healers.setInteractingEntity(jad);
                healers.getFollowing().setFollowing(jad, true);
                World.getWorld().submit(new Tickable(1) {
                    @Override
                    public void execute() {
                        if (healers.isDestroyed() || healers.getCombatState().isDead()) {
                            this.stop();
                            return;
                        }
                        if (player.isDestroyed() || !player.getAttributes().isSet("cavesession")) {
                            World.getWorld().removeNPC(healers);
                            this.stop();
                            return;
                        }
                        if (healers.getCombatState().isOutOfCombat(null, false)) {
                            if (Location.isWithinDistance(healers, jad, 1)) {
                                if (NumberUtils.random(7) == 0) {
                                    jad.playGraphic(444, 0, (250 << 16));
                                    healers.animate(2639);
                                    int jadMaxHp = jad.getMaxHp();
                                    jad.heal((int) (jadMaxHp * 0.5));
                                }
                            }
                        }
                    }
                });
            }
        }
    }

    private void remove_game_monsters() {
        for (NPC npcs : getCurrentWaveNpcs()) {
            World.getWorld().removeNPC(npcs);
        }
        getCurrentWaveNpcs().clear();
    }

    public void sendAntiTeleportMessage() {
      //  player.getPacketSender().sendNPCHead(2617, 242, 0);
        player.getPacketSender().modifyText("TzHaar-Mej-Jal", 242, 1);
        player.getPacketSender().modifyText("I cannot allow you to teleport from the fight cave.", 242, 2);
        player.getPacketSender().modifyText("In Tzhaar, you either win, or die!", 242, 3);
       // player.getPacketSender().animateInterface(FacialAnimation.DEFAULT.getAnimation().getId(), 242, 0);
        player.getPacketSender().sendChatboxInterface(242);
        player.getAttributes().set("nextDialogue", -1);
        player.getPacketSender().sendMessage("You are unable to teleport from the fight cave.");
    }

    public static boolean atArea(Player player) {
        RoundBasedMiniquest miniquest = player.getVariables().getCurrentRoundMiniquest();
        return miniquest != null && miniquest.nameOfQuest().equalsIgnoreCase("fight caves");
    }

    public boolean isWaveStarted() {
        return waveStarted;
    }

    public void setWaveStarted(boolean waveStarted) {
        this.waveStarted = waveStarted;
    }

    public boolean isHealersSpawned() {
        return healersSpawned;
    }

    public void setHealersSpawned(boolean healersSpawned) {
        this.healersSpawned = healersSpawned;
    }

    public boolean isWaveCompleted() {
        return waveCompleted;
    }

    public byte getCurrentWave() {
        return currentWave;
    }

    public void setCurrentWave(byte currentWave) {
        this.currentWave = currentWave;
    }

    private void increaseWave(int amt) {
        this.currentWave += amt;
        this.waveCompleted = false;
        this.waveStarted = false;
    }

    public ArrayList<NPC> getCurrentWaveNpcs() {
        return currentWaveNpcs;
    }

    public void addWaveNPC(NPC npc) {
        this.currentWaveNpcs.add(npc);
    }

    private void removeWaveNPC(NPC npc) {
        this.currentWaveNpcs.remove(npc);
    }

    public boolean isCompletedGame() {
        return completedGame;
    }

    public boolean isGameStopped() {
        return gameStopped;
    }

    public void setGameStopped(boolean gameStopped) {
        this.gameStopped = gameStopped;
    }
}
