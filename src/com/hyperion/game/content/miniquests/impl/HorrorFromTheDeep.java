package com.hyperion.game.content.miniquests.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

public class HorrorFromTheDeep {
	
	public static void enterGame(final Player player) {
		player.teleport(2515, 4632, 0);
		final NPC npc = new NPC(3497);
		Location location = Location.create(2517, 4648, 0);
		npc.setLocation(location);
		npc.setLastKnownRegion(location);
		player.getVariables().addOwnedNPC(npc);
		World.getWorld().addNPC(npc);
		player.getPacketSender().sendHintArrow(npc);
		npc.getCombatState().startAttack(player);
		npc.getAttributes().set("hfdstage", 0);
		World.getWorld().submit(new Tickable(2, false) {
			int count = 0;
			@Override
			public void execute() {
				if (player.isDestroyed()) {
					World.getWorld().removeNPC(npc);
					this.stop();
					return;
				}
				if (npc.isDestroyed()) {
					this.stop();
					return;
				}
				if (!Location.isWithinDistance(npc, player, 17)) {
					player.getPacketSender().sendHintArrow(null);
					World.getWorld().removeNPC(npc);
					this.stop();
					return;
				}
				if (count == 15) {
					count = 0;
					npc.getAttributes().set("hfdstage", npc.getAttributes().getInt("hfdstage") + 1);
					if (npc.getAttributes().getInt("hfdstage") == 5) {
						npc.getAttributes().set("hfdstage", 0);
					}
					npc.setTransformationId(3497 + npc.getAttributes().getInt("hfdstage"));
				} else {
					count++;
				}
			}
		});
	}
}
