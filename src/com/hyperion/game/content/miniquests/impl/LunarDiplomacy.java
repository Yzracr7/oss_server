package com.hyperion.game.content.miniquests.impl;

import com.hyperion.game.content.QuestTab;
import com.hyperion.game.content.minigames.Settings;
import com.hyperion.game.content.miniquests.AbstractRoundBasedMiniquest;
import com.hyperion.game.content.miniquests.RoundBasedMiniquest;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.region.MapBuilder;
/**
 * @author Nando
 */
public class LunarDiplomacy extends AbstractRoundBasedMiniquest {

	private static final int[] stageNpcs = {
		5902, 5903, 5904, 5905
	};
	
	@Override
	public void enter(final Player player) {
		Location north_east = Location.create(1835, 5167, 2);
		Location south_west = Location.create(1813, 5141, 2);
		int width = (north_east.getX() - south_west.getX()) / 8 + 3;
	    int height = (north_east.getY() - south_west.getY()) / 8 + 2;
	    int[] newCoords = MapBuilder.findEmptyChunkBound(width, height);
		MapBuilder.copyAllPlanesMap(south_west.getChunkX(), south_west.getChunkY(), newCoords[0], newCoords[1], width, height);
		Location baseLocation = Location.create(newCoords[0] << 3, newCoords[1] << 3, 2);
		baseLocation.setWidth(width);
		baseLocation.setHeight(height);
		player.getAttributes().set("baselocation", baseLocation);
		Location player_start = Location.create(baseLocation.getX() + 17, baseLocation.getY() + 4, 2);
		player.teleport(player_start);
		player.getVariables().setLunarStage(0);
		final Location npc_start = Location.create(baseLocation.getX() + 16, baseLocation.getY() + 16, 2);
		World.getWorld().submit(new Tickable(4) {
			@Override
			public void execute() {
				this.stop();
				if (player.isDestroyed()) {
					return;
				}
				NPC npc = new NPC(stageNpcs[0]);
				npc.setLocation(npc_start);
				npc.setLastKnownRegion(npc_start);
				player.getVariables().addOwnedNPC(npc);
				World.getWorld().addNPC(npc);
				player.getPacketSender().sendHintArrow(npc);
				npc.getCombatState().startAttack(player);
			}
		});
	}
	
	@Override
	public void nextRound(Player player, NPC deadNPC) {
		player.getVariables().increase_lunar_stage(1);
		int stage = player.getVariables().getLunarStage();
		if (stage < 4) {
			final NPC npc = new NPC(stageNpcs[stage]);
			npc.setLocation(deadNPC.getLocation());
			npc.setLastKnownRegion(deadNPC.getLocation());
			player.getVariables().addOwnedNPC(npc);
			World.getWorld().addNPC(npc);
			player.getPacketSender().sendHintArrow(npc);
			npc.getCombatState().startAttack(player);
		} else {
			player.getPacketSender().sendHintArrow(null);
			player.teleport(Settings.MINIQUEST_AREA);
			player.getVariables().finishedLunars = true;
			QuestTab.openQuestComplete(player, 2);
			final Location baseLocation = player.getAttributes().get("baselocation");
			player.getAttributes().remove("baselocation");
			player.getVariables().setSessionName(null);
			player.getVariables().setCurrentRoundMiniquest(null);
			World.getWorld().submit(new Tickable(2) {
	    		@Override
	    		public void execute() {
	    			MapBuilder.destroyMap(baseLocation.getChunkX(), baseLocation.getChunkY(), baseLocation.getWidth(), baseLocation.getHeight());
	    			this.stop();
	    		}
	    	});
		}
	}
	
	@Override
	public void end(Player player, boolean won_game, boolean died, boolean logged) {
		World.getWorld().unregister_player_npcs(player);
		player.resetVariables();
		player.getVariables().finishedLunars = false;
		if (logged) {
			player.setLocation(Settings.MINIQUEST_AREA);
		} else {
			player.getVariables().setSessionName(null);
			
			player.getPacketSender().sendHintArrow(null);
			player.teleport(Settings.MINIQUEST_AREA);
		}
		final Location baseLocation = player.getAttributes().get("baselocation");
		player.getAttributes().remove("baselocation");
		player.getVariables().setCurrentRoundMiniquest(null);
		World.getWorld().submit(new Tickable(2) {
    		@Override
    		public void execute() {
    			MapBuilder.destroyMap(baseLocation.getChunkX(), baseLocation.getChunkY(), baseLocation.getWidth(), baseLocation.getHeight());
    			this.stop();
    		}
    	});
	}
	
	@Override
	public String nameOfQuest() {
		return "Lunar Diplomacy";
	}
	
	public static boolean atArea(Player player) {
		RoundBasedMiniquest miniquest = player.getVariables().getCurrentRoundMiniquest();
		return miniquest != null && miniquest.nameOfQuest().equalsIgnoreCase("Lunar Diplomacy");
	}
}
