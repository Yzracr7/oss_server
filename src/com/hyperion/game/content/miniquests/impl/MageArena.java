package com.hyperion.game.content.miniquests.impl;

import com.hyperion.game.content.QuestTab;
import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.content.miniquests.AbstractRoundBasedMiniquest;
import com.hyperion.game.content.miniquests.RoundBasedMiniquest;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.region.MapBuilder;
/**
 * @author Nando
 */
public class MageArena extends AbstractRoundBasedMiniquest {

	private static final int[] stageNpcs = {
		907, 908, 909, 910/*, 911*/, 997 //big spider
	};
	
	public static final Location MAGE_ARENA = Location.create(2539, 4719, 0);
	
	@Override
	public void enter(final Player player) {
		Location north_east = Location.create(3108, 3946, 0);
		Location south_west = Location.create(3090, 3921, 0);
		int width = (north_east.getX() - south_west.getX()) / 8 + 3;
	    int height = (north_east.getY() - south_west.getY()) / 8 + 1;
	    int[] newCoords = MapBuilder.findEmptyChunkBound(width, height);
		MapBuilder.copyAllPlanesMap(south_west.getChunkX(), south_west.getChunkY(), newCoords[0], newCoords[1], width, height);
		Location baseLocation = Location.create(newCoords[0] << 3, newCoords[1] << 3, 0);
		baseLocation.setWidth(width);
		baseLocation.setHeight(height);
		player.getAttributes().set("baselocation", baseLocation);
		Location player_start = Location.create(baseLocation.getX() + 11, baseLocation.getY() + 14, 0);
		player.teleport(player_start);
		player.getVariables().setMageArenaStage(0);
		final Location npc_start = Location.create(baseLocation.getX() + 18, baseLocation.getY() + 14, 0);
		World.getWorld().submit(new Tickable(4) {
			@Override
			public void execute() {
				this.stop();
				if (player.isDestroyed()) {
					return;
				}
				NPC npc = new NPC(stageNpcs[0]);
				npc.setLocation(npc_start);
				npc.setLastKnownRegion(npc_start);
				player.getVariables().addOwnedNPC(npc);
				World.getWorld().addNPC(npc);
				player.getPacketSender().sendHintArrow(npc);
				npc.playForcedChat("You must prove yourself... now!");
				npc.getCombatState().startAttack(player);
			}
		});
	}
	
	@Override
	public void nextRound(Player player, NPC deadNPC) {
		player.getVariables().increaseMageArenaStage(1);
		if (player.getVariables().getMageArenaStage() /*< 4*/ < 5) {
			NPC npc = new NPC(stageNpcs[player.getVariables().getMageArenaStage()]);
			npc.setLocation(deadNPC.getLocation());
			npc.setLastKnownRegion(deadNPC.getLocation());
			player.getVariables().addOwnedNPC(npc);
			World.getWorld().addNPC(npc);
			player.getPacketSender().sendHintArrow(npc);
			npc.getCombatState().startAttack(player);
			String text = null;
			switch (player.getVariables().getMageArenaStage()) {
			case 1:
				text = "This is only the begining; you can't beat me!";
				break;
			case 2:
				text = "Foolish mortal; I am unstoppable.";
				break;
			case 3:
				text = "Now you feel it... The dark energy.";
				break;
			case 4:
				text = "Aaaaaaaarrgghhhh! The power!";
				break;
			}
			npc.playForcedChat(text);
		} else {
			player.getPacketSender().sendHintArrow(null);
			player.teleport(MAGE_ARENA);
			player.getVariables().finishedMageArena = true;
			Achievements.increase(player, 17);
			QuestTab.openQuestComplete(player, 0);
			final Location baseLocation = player.getAttributes().get("baselocation");
			player.getAttributes().remove("baselocation");
			player.getVariables().setSessionName(null);
			player.getVariables().setCurrentRoundMiniquest(null);
			World.getWorld().submit(new Tickable(2) {
	    		@Override
	    		public void execute() {
	    			MapBuilder.destroyMap(baseLocation.getChunkX(), baseLocation.getChunkY(), baseLocation.getWidth(), baseLocation.getHeight());
	    			this.stop();
	    		}
	    	});
		}
	}
	
	@Override
	public void end(Player player, boolean won_game, boolean died, boolean logged) {
		final Location baseLocation = player.getAttributes().get("baselocation");
		if (logged) {
			player.setLocation(MAGE_ARENA);
		} else {
			player.teleport(MAGE_ARENA);
		}
		World.getWorld().unregister_player_npcs(player);
		player.getPacketSender().sendHintArrow(null);
		player.getVariables().finishedMageArena = false;
		player.getAttributes().remove("baselocation");
		player.resetVariables();
		player.getVariables().setSessionName(null);
		player.getVariables().setCurrentRoundMiniquest(null);
		World.getWorld().submit(new Tickable(2) {
    		@Override
    		public void execute() {
    			MapBuilder.destroyMap(baseLocation.getChunkX(), baseLocation.getChunkY(), baseLocation.getWidth(), baseLocation.getHeight());
    			this.stop();
    		}
    	});
	}
	
	@Override
	public String nameOfQuest() {
		return "Mage Arena";
	}
	
	public static boolean atArea(Player player) {
		RoundBasedMiniquest miniquest = player.getVariables().getCurrentRoundMiniquest();
		return miniquest != null && miniquest.nameOfQuest().equalsIgnoreCase("mage arena");
	}
}
