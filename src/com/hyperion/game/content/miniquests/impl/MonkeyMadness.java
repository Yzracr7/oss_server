package com.hyperion.game.content.miniquests.impl;

import com.hyperion.game.content.QuestTab;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.grounditems.GroundItem;
import com.hyperion.game.world.grounditems.GroundItemManager;
import com.hyperion.game.world.region.MapBuilder;

public class MonkeyMadness {

	public static void enterGame(final Player player) {
		Location north_east = Location.create(2735, 9206, 1);
		Location south_west = Location.create(2696, 9167, 1);
		final int width = (north_east.getX() - south_west.getX()) / 8 + 2;
		final int height = (north_east.getY() - south_west.getY()) / 8 + 2;
		int[] newCoords = MapBuilder.findEmptyChunkBound(width, height);
		MapBuilder.copyAllPlanesMap(south_west.getChunkX(), south_west.getChunkY(), newCoords[0], newCoords[1], width, height);
		final Location baseLocation = Location.create(newCoords[0] << 3, newCoords[1] << 3, 1);
		baseLocation.setWidth(width);
		baseLocation.setHeight(height);
		player.getAttributes().set("baselocation", baseLocation);
		Location start = Location.create(baseLocation.getX() + 19, baseLocation.getY() + 47, 1);
		player.getVariables().setAtJungleDemon(true);
		player.teleport(start);
		final NPC npc = new NPC(477);//1472
		Location npc_start = Location.create(baseLocation.getX() + 7, baseLocation.getY() + 38, 1);
		npc.setLocation(npc_start);
		npc.setLastKnownRegion(npc_start);
		player.getVariables().addOwnedNPC(npc);
		World.getWorld().addNPC(npc);
		player.getPacketSender().sendHintArrow(npc);
		npc.getCombatState().startAttack(player);
	}
	
	public static void end(Player player, NPC npc, boolean killedDemon, boolean died) {
		final Location baseLocation = player.getAttributes().get("baselocation");
		if (npc != null) {
			World.getWorld().removeNPC(npc);
			player.getPacketSender().sendHintArrow(null);
		}
		if (killedDemon) {
			player.getVariables().setDemon_killed(true);
			Item dragonScimmy = new Item(4587, 1);
			if (!player.getInventory().addItem(dragonScimmy)) {
				//player.getBank().addItem(dragonScimmy);
				player.getPacketSender().sendMessage("You don't have any inventory space. Your reward has been dropped on the ground.");
				GroundItem groundItem = new GroundItem(dragonScimmy, player.getLocation(), player);
				GroundItemManager.create(groundItem, player);
			} else {
				player.getInventory().refresh();
			}
			Item basicGreegree = new Item(4031, 1);
			if (!player.getInventory().addItem(basicGreegree)) {
				//player.getBank().addItem(basicGreegree);
				player.getPacketSender().sendMessage("You don't have any inventory space. Your reward has been dropped on the ground.");
				GroundItem groundItem = new GroundItem(basicGreegree, player.getLocation(), player);
			} else {
				player.getInventory().refresh();
			}
			QuestTab.openQuestComplete(player, 5);
			DialogueManager.handleDialogues(player, new NPC(670), 1);
		}
		player.teleport(Location.create(2465, 3489, 0));
		if (died) {
			player.resetVariables();
		}
		player.getVariables().setAtJungleDemon(false);
		player.getAttributes().remove("baselocation");
		World.getWorld().submit(new Tickable(2) {
    		@Override
    		public void execute() {
    			MapBuilder.destroyMap(baseLocation.getChunkX(), baseLocation.getChunkY(), baseLocation.getWidth(), baseLocation.getHeight());
    			this.stop();
    		}
    	});
	}
}
