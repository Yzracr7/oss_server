package com.hyperion.game.content.miniquests.impl;

import com.hyperion.game.content.QuestTab;
import com.hyperion.game.content.minigames.Settings;
import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.grounditems.GroundItem;
import com.hyperion.game.world.grounditems.GroundItemManager;

public class MountainDaughter {

	public static void enterGame(final Player player) {
		player.resetVariables();
		player.teleport(2789, 10083, 0);
		final NPC npc = new NPC(1813);
		Location location = Location.create(2787, 10073, 0);
		npc.setLocation(location);
		npc.setLastKnownRegion(location);
		player.getVariables().addOwnedNPC(npc);
		World.getWorld().addNPC(npc);
		player.getPacketSender().sendHintArrow(npc);
		npc.getCombatState().startAttack(player);
		World.getWorld().submit(new Tickable(2, false) {
			@Override
			public void execute() {
				if (player.isDestroyed()) {
					World.getWorld().removeNPC(npc);
					this.stop();
					return;
				}
				if (npc.isDestroyed()) {
					this.stop();
					return;
				}
				if (!Location.isWithinDistance(npc, player, 17)) {
					player.getPacketSender().sendHintArrow(null);
					World.getWorld().removeNPC(npc);
					this.stop();
					return;
				}
			}
		});
	}
	
	public static void handleFinished(Player player) {
		player.teleport(Settings.MINIQUEST_AREA);
		Item bearHead = new Item(4502, 1);
		if (!player.getInventory().addItem(bearHead)) {
			QuestTab.openQuestComplete(player, 3);
			player.getPacketSender().sendMessage("You don't have any inventory space. Your reward has been dropped on the ground.");
			GroundItem groundItem = new GroundItem(bearHead, player.getLocation(), player);
			GroundItemManager.create(groundItem, player);
			//player.getBank().addItem(bearHead);
		}
		player.getVariables().setKendal_killed(true);
		player.getInventory().refresh();
	}
}
