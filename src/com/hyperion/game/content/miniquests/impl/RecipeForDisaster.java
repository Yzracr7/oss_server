package com.hyperion.game.content.miniquests.impl;

import com.hyperion.game.content.QuestTab;
import com.hyperion.game.content.minigames.Settings;
import com.hyperion.game.content.miniquests.AbstractRoundBasedMiniquest;
import com.hyperion.game.content.miniquests.RoundBasedMiniquest;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.region.MapBuilder;

/**
 * @author Nando
 */
public class RecipeForDisaster extends AbstractRoundBasedMiniquest {

	private static final int[] stageNpcs = { 3493, 3494, 3495, 3496, 3497 };

	@Override
	public void enter(final Player player) {
		player.getPrayers().deactivateAllPrayers();
		Location north_east = Location.create(1910, 5366, 2);
		Location south_west = Location.create(1889, 5345, 2);
		final int width = (north_east.getX() - south_west.getX()) / 8;
		final int height = (north_east.getY() - south_west.getY()) / 8;
		int[] newCoords = MapBuilder.findEmptyChunkBound(width, height);
		MapBuilder.copyAllPlanesMap(south_west.getChunkX(), south_west.getChunkY(), newCoords[0], newCoords[1], width, height);
		final Location baseLocation = Location.create(newCoords[0] << 3, newCoords[1] << 3, 2);
		baseLocation.setWidth(width);
		baseLocation.setHeight(height);
		player.getAttributes().set("baselocation", baseLocation);
		Location player_start = Location.create(baseLocation.getX() + 9, baseLocation.getY() + 14, 2);
		player.teleport(player_start);
		final Location npc_start = Location.create(baseLocation.getX() + 8, baseLocation.getY() + 8, 2);
		World.getWorld().submit(new Tickable(4) {
			@Override
			public void execute() {
				this.stop();
				if (player.isDestroyed()) {
					return;
				}
				NPC npc = new NPC(stageNpcs[0]);
				npc.setLocation(npc_start);
				npc.setLastKnownRegion(npc_start);
				player.getVariables().addOwnedNPC(npc);
				World.getWorld().addNPC(npc);
				player.getPacketSender().sendHintArrow(npc);
				npc.getCombatState().startAttack(player);
			}
		});
	}

	@Override
	public void nextRound(Player player, NPC deadNPC) {
		player.getVariables().increaseRfdStage(1);
		int stage = player.getVariables().getRfdStage();
		if (stage < 5) {
			final NPC npc = new NPC(stageNpcs[stage]);
			npc.setLocation(deadNPC.getLocation());
			npc.setLastKnownRegion(deadNPC.getLocation());
			player.getVariables().addOwnedNPC(npc);
			World.getWorld().addNPC(npc);
			player.getPacketSender().sendHintArrow(npc);
			npc.getCombatState().startAttack(player);
			if (stage == 4) {// last - mother boss
				npc.getAttributes().set("hfdstage", 0);
				processMother(player, npc);
			}
		} else {
			player.getPacketSender().sendHintArrow(null);
			player.teleport(Settings.MINIQUEST_AREA);
			player.getVariables().finishedRfd = true;
			QuestTab.openQuestComplete(player, 1);
			final Location baseLocation = player.getAttributes().get("baselocation");
			player.getAttributes().remove("baselocation");
			player.getVariables().setSessionName(null);
			player.getVariables().setCurrentRoundMiniquest(null);
			World.getWorld().submit(new Tickable(2) {
				@Override
				public void execute() {
					MapBuilder.destroyMap(baseLocation.getChunkX(), baseLocation.getChunkY(), baseLocation.getWidth(), baseLocation.getHeight());
					this.stop();
				}
			});
		}
	}

	@Override
	public void end(Player player, boolean won_game, boolean died, boolean logged) {
		final Location baseLocation = player.getAttributes().get("baselocation");
		World.getWorld().unregister_player_npcs(player);
		player.getPacketSender().sendHintArrow(null);
		if (!logged) {
			player.teleport(Settings.MINIQUEST_AREA);
		} else {
			player.setLocation(Settings.MINIQUEST_AREA);
		}
		player.resetVariables();
		player.getVariables().finishedRfd = false;
		player.getAttributes().remove("baselocation");
		player.getVariables().setSessionName(null);
		player.getVariables().setCurrentRoundMiniquest(null);
		World.getWorld().submit(new Tickable(2) {
			@Override
			public void execute() {
				MapBuilder.destroyMap(baseLocation.getChunkX(), baseLocation.getChunkY(), baseLocation.getWidth(), baseLocation.getHeight());
				this.stop();
			}
		});
	}

	@Override
	public String nameOfQuest() {
		return "Recipe For Disaster";
	}

	public static boolean atArea(Player player) {
		RoundBasedMiniquest miniquest = player.getVariables().getCurrentRoundMiniquest();
		return miniquest != null && miniquest.nameOfQuest().equalsIgnoreCase("recipe for disaster");
	}

	private static void processMother(final Player player, final NPC npc) {
		World.getWorld().submit(new Tickable(2, false) {
			int count = 0;

			@Override
			public void execute() {
				if (player.isDestroyed()) {
					World.getWorld().removeNPC(npc);
					this.stop();
					return;
				}
				if (npc.isDestroyed()) {
					this.stop();
					return;
				}
				if (!Location.isWithinDistance(npc, player, 17)) {
					player.getPacketSender().sendHintArrow(null);
					World.getWorld().removeNPC(npc);
					this.stop();
					return;
				}
				if (count == 15) {
					count = 0;
					npc.getAttributes().set("hfdstage", npc.getAttributes().getInt("hfdstage") + 1);
					if (npc.getAttributes().getInt("hfdstage") == 5) {
						npc.getAttributes().set("hfdstage", 0);
					}
					npc.setTransformationId(3497 + npc.getAttributes().getInt("hfdstage"));
				} else {
					count++;
				}
			}
		});
	}
}
