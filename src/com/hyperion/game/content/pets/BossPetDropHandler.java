package com.hyperion.game.content.pets;

import java.util.Arrays;
import java.util.Optional;

import com.hyperion.game.Constants;
import com.hyperion.game.discord.Discord;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.Directions.NormalDirection;
import com.hyperion.utility.NumberUtils;
import com.hyperion.utility.game.Tile;

public enum BossPetDropHandler {

	// BOSS & SLAYER PETS
	DARK_CORE(318, 318, 12816, 123, 0), VENENATIS_SPIDERLING(495, 0, 13177, 126, 6610), CALLISTO_CUB(497, 0, 13178, 130,
			6503), HELLPUPPY(964, 0, 13247, 138, 0) {
				@Override
				public int getDialogue(Player player) {
					return NumberUtils.getRandomIndex(new int[] { 138, 143, 145, 150, 154 });
				}
			},
	CHAOS_ELEMENTAL_JR(2055, 0, 11995, 158, 2054), CHAOS_ELEMENTAL_JR2(2055, 0, 11995, 158, 6619), SNAKELING(2130, 2131, 12921, 162, 2042), MAGMA_SNAKELING(2131, 2132,
			12921, 169, 2042), TANZANITE_SNAKELING(2132, 2130, 12921, 176, 2042), VETION_JR(5536, 5537, 13179, 183,
					6611), VETION_JR_REBORN(5537, 5536, 13179, 189,
							6611), SCORPIAS_OFFSPRING(5561, 0, 13181, 195, 6615), ABYSSAL_ORPHAN(5884, 0, 13262, 202, 0) {
								@Override
								public int getDialogue(Player player) {
									// if (!player.getAppearance().isMale()) {
									return 206;
									// } else
									// return Misc.getRandomIndex(new int [] {202, 209});
								}
							},
	TZREK_JAD(5892, 0, 13225, 212, 0) {
		@Override
		public int getDialogue(Player player) {
			return NumberUtils.getRandomIndex(new int[] { 212, 217 });
		}
	},
	SUPREME_HATCHLING(6628, 0, 12643, 220, 0), PRIME_HATCHLING(6629, 0, 12644, 223, 0), REX_HATCHLING(6630, 0, 12645,
			231, 0), CHICK_ARRA(6631, 0, 12649, 239,
					0), GENERAL_AWWDOR(6632, 0, 12650, 247, 0), COMMANDER_MINIANA(6633, 0, 12651, 250, 0) {
						@Override
						public int getDialogue(Player player) {
							if (player.getEquipment().hasItem(11806)) {
								return 252;
							} else
								return 250;
						}
					},
	KRIL_TINYROTH(6634, 0, 12652, 254, 0), BABY_MOLE(6635, 0, 12646, 261, 0), PRINCE_BLACK_DRAGON(6636, 0, 12653, 267,
			239), KALPHITE_PRINCESS(6637, 6638, 12654, 271, 0), MORPHED_KALPHITE_PRINCESS(6638, 6637, 12654, 279,
					0), SMOKE_DEVIL(6639, 0, 12648, 288, 0), KRAKEN(6640, 0, 12655, 291,
							0), PENANCE_PRINCESS(6642, 0, 12703, 296, 0), OLMLET(7520, 0, 20851, 298, 0),

	// SKILL PETS
	HERON(6715, 0, 13320, -1, 0), BEAVER(6717, 0, 13322, -1, 0), RED_CHINCHOMPA(6718, 6719, 13323, -1,
			0), GREY_CHINCHOMPA(6719, 6720, 13324, -1, 0), BLACK_CHINCHOMPA(6720, 6718, 13325, -1,
					0), ROCK_GOLEM(6723, 0, 13321, -1, 0), GIANT_SQUIRREL(7334, 0, 20659, -1, 0),
	// TANGLEROOT(7335, 0, 0, -1, 0),
	// ROCKY(7336, 0, 0, -1, 0),

	// RIFT GUARDIANS (SKILL PETS)
	FIRE_RIFT_GAURDIAN(7337, 7338, 20665, -1, 0), AIR_RIFT_GUARDIAN(7338, 7339, 20667, -1, 0), MIND_RIFT_GUARDIAN(7339,
			7340, 20669, -1, 0), WATER_RIFT_GUARDIAN(7340, 7341, 20671, -1, 0), EARTH_RIFT_GUARDIAN(7341, 7342, 20673,
					-1, 0), BODY_RIFT_GUARDIAN(7342, 7343, 20675, -1, 0), COSMIC_RIFT_GUARDIAN(7343, 7344, 20677, -1,
							0), CHAOS_RIFT_GUARDIAN(7344, 7345, 20679, -1, 0), NATURE_RIFT_GUARDIAN(7345, 7346, 20681,
									-1, 0), LAW_RIFT_GUARDIAN(7346, 7347, 20683, -1, 0), DEATH_RIFT_GUARDIAN(7347, 7348,
											20685, -1,
											0), SOUL_RIFT_GUARDIAN(7348, 7349, 20687, -1, 0), ASTRAL_RIFT_GUARDIAN(7349,
													7350, 20689, -1, 0), BLOOD_RIFT_GUARDIAN(7350, 7337, 20691, -1, 0);

	private final int petId;
	private final int morphId;
	private final int itemId;
	private final int dialogue;

	public int getNpc_drop_pet() {
		return npc_drop_pet;
	}

	private final int npc_drop_pet;

	BossPetDropHandler(int petNpcId, int morphId, int itemId, int dialogue, int npc_that_drops) {
		this.petId = petNpcId;
		this.morphId = morphId;
		this.itemId = itemId;
		this.dialogue = dialogue;
		this.npc_drop_pet = npc_that_drops;
	}

	public int getId() {
		return petId;
	}

	public int getMorphId() {
		return morphId;
	}

	public boolean canMorph() {
		return (morphId != 0);
	}

	public int getItemId() {
		return itemId;
	}

	public int getDialogue(Player player) {
		return dialogue;
	}

	public static BossPetDropHandler roll(Player player, NPC entity) {
		for (int a = 0; a < BossPetDropHandler.values().length; a++) {
			if (BossPetDropHandler.values()[a].npc_drop_pet == entity.getId()) {
				int roll = NumberUtils.random(1000);
				if (roll == 4) {
					NPC pet = new NPC(BossPetDropHandler.values()[a].petId);
					if (player.getBank().containsItem(BossPetDropHandler.values()[a].getItemId(), 1) || player.getInventory().containsItem(BossPetDropHandler.values()[a].getItemId(), 1) || player.getVariables().getOwnedNPCS().contains(pet)) {
						player.getPacketSender().sendMessage("You have a funny feeling like you would have been followed...");
					} else if (player.getAttributes().isSet("petFollower")) {
						if (player.getInventory().freeSlots() > 0) {
							player.getPacketSender().sendMessage("You feel something weird sneaking into your backpack.");
							player.getInventory().addItem(BossPetDropHandler.values()[a].itemId);
						}
					} else {
						player.sendMessage("You have a funny feeling like you're being followed.");
						BossPetDropHandler.spawnPet(player, BossPetDropHandler.values()[a].getItemId(), false);
						int random_message = NumberUtils.random(5);
						String message = " has slain the mighty ";
						switch (random_message) {
						case 0:
							message = " has wrecked ";
							break;
						case 1:
							message = " swept the floor with ";
							break;
						case 2:
							message = " has dropped ";
							break;
						}
						Discord.speak("```" + player.getDetails().getName() + message + ""
								+ ((NPC) entity).getDefinition().getName() + " and received a pet!" + "```");
					}
				}
				return BossPetDropHandler.values()[a];
			}
		}
		return null;
	}

	public static BossPetDropHandler forNpc(int id) {
		for (BossPetDropHandler t : BossPetDropHandler.values()) {
			if (t.petId == id) {
				return t;
			}
		}
		return null;
	}

	public String getName() {
		String name = name().toLowerCase().replaceAll("_", " ");
		return name;// Misc.capitalizeWords(name);
	}

	public static Optional<BossPetDropHandler> getPet(int identifier) {
		return Arrays.stream(values()).filter(s -> s.petId == identifier).findFirst();
	}

	public static Optional<BossPetDropHandler> getPetForItem(int identifier) {
		return Arrays.stream(values()).filter(s -> s.itemId == identifier).findFirst();
	}

	public static Optional<BossPetDropHandler> getPetForNpcId(int identifier) {
		return Arrays.stream(values()).filter(s -> s.petId == identifier).findFirst();
	}

	public static boolean spawnPet(final Player player, int itemId, boolean deleteItem) {
		if (getPetForItem(itemId) == null) {
			return false;
		}
		BossPetDropHandler pet = getPetForItem(itemId).get();
		if (pet != null) {
			int npcId = pet.petId;
			if (player.getAttributes().isSet("petFollower")) {
				player.getPacketSender().sendMessage("You already have a follower!");
				return true;
			}

			NPC npc = new NPC(npcId);
			NormalDirection direction = Tile.hasLocationToMove(npc, player);

			boolean perform = true;

			if (direction != null) {
				Location loc = null;

				loc = Location.createSurroundingTile(npc, player, direction);
				if (loc == null) {
					if (Constants.DEBUG_MODE) {
						System.out.println("Direction given is not supported.");
					}
					perform = false;
				}
				/*
				 * if(perform && ProjectilePathFinder.hasLineOfSight(loc, player.getLocation(,
				 * 0), false)) { if(Constants.DEBUG_MODE)
				 * System.out.println("No line of sight."); perform = false; }
				 */

				if (perform && (!deleteItem || player.getInventory().deleteItem(itemId))) {
					if (Constants.DEBUG_MODE) {
						System.out.println("Dir: " + direction + ", loc: " + loc.toString());
					}
					npc.setLocation(loc);
					npc.setLastKnownRegion(loc);
					player.face(npc.getCentreLocation());
					npc.getAttributes().set("petOwner", player);
					player.getAttributes().set("petFollower", npc);

					player.getVariables().addOwnedNPC(npc);
					World.getWorld().addNPC(npc);
					npc.getFollowing().setFollowing(player, false);
					player.getVariables().setFamiliarId(npcId);
					npc.playGraphic(npc.getSize() > 1 ? 1315 : 1314);
					if (pet == BossPetDropHandler.REX_HATCHLING)
						player.getPacketSender().sendMessage("You have a funny feeling like you're being followed.");
				} else
					perform = false;
			} else
				perform = false;
			if (!perform) {
				npc = null;
				player.getPacketSender().sendMessage("There is not enough room to spawn your familiar here.");
			}

			return true;
		}
		return false;
	}

	public static boolean pickupPet(Player player, NPC npc) {
		int npcId = npc.getId();
		BossPetDropHandler pets = forNpc(npcId);
		if (pets != null) {
			if (npc.getAttributes().isSet("petOwner") && player.getAttributes().isSet("petFollower")
					&& npc.getAttributes().get("petOwner") == player
					&& player.getAttributes().get("petFollower") == npc) {
				// if (!player.getVariables().getOwnedNPCS().contains(npc)) {
				int itemId = pets.itemId;
				if (player.getInventory().freeSlots() > 0) {
					player.getAttributes().remove("petFollower");
					player.face(npc.getCentreLocation());
					player.getVariables().getOwnedNPCS().remove(npc);
					World.getWorld().removeNPC(npc);
					player.animate(827);
					player.getInventory().addItem(itemId, 1);

					player.getVariables().setFamiliarId(-1);

					player.getPacketSender().sendMessage("You pick up your pet.");
				} else {
					player.getPacketSender().sendMessage("You do not have enough inventory space to do this.");
				}
			} else {
				player.getPacketSender().sendMessage("This is not your pet.");
			}
			return true;
		} else {
			return false;
		}
	}
}