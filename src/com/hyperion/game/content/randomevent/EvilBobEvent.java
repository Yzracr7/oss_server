package com.hyperion.game.content.randomevent;

import com.hyperion.game.Constants;
import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.Utils;
import com.hyperion.utility.game.Tile;

public class EvilBobEvent {

	
	public static void start(final Player player) {
		if(player.getVariables().isNoTeleport())
			return;
		if(player.getAttributes().isSet("last_death")) {
			if((System.currentTimeMillis() - player.getAttributes().getLong("last_death")) < 300000) {
				return;
			}
		}
		final NPC npc = new NPC(2478); //2479 talkable
		Location loc = Tile.getSurroundingTile(player);
		npc.setLocation(loc);
		npc.setLastKnownRegion(loc);
		player.getVariables().addOwnedNPC(npc);
		World.getWorld().addNPC(npc);
		npc.face(player.getLocation());
		npc.setInteractingEntity(player);
		//npc.getFollowing().setFollowing(player, false);
		npc.playForcedChat(player.getName() + ", come with me!");
		player.getAttributes().set("preRandomLoc", player.getLocation());
		World.getWorld().submit(new Tickable(2) {
			@Override
			public void execute() {
				//tele player
				player.animate(8939);
				player.playForcedChat("ARGHHHHHHHHH!");
				player.getAttributes().set("stopActions", true);
				
				World.getWorld().submit(new Tickable(3) {
					@Override
					public void execute() {
						player.getVariables().setNoTeleport(true);
						player.animate(8941);
						player.setLocation(Location.create(2525, 4776, 0));
						player.getPacketSender().sendMessage("Welcome to ScapeRune.");
						World.getWorld().removeNPC(npc);
						player.getVariables().getOwnedNPCS().remove(npc);
						player.getAttributes().remove("stopActions");
						
						this.stop();
					}
				});
				this.stop();
			}
		});
	}
	
	public static void leave(final Player player) {
		if(player.getAttributes().isSet("canLeaveRandom")) {
			player.getAttributes().set("stopActions", true);
			player.face(Location.create(2525, 4775, 0));
			player.playForcedChat("Be seeing you!");
			player.animate(Animation.RASPBERRY);
			
			World.getWorld().submit(new Tickable(3) {
				@Override
				public void execute() {
					player.animate(8939);
					World.getWorld().submit(new Tickable(3) {
						@Override
						public void execute() {
							player.animate(8941);
							player.getAttributes().remove("stopActions");
							player.getAttributes().remove("canLeaveRandom");
							player.getVariables().setNoTeleport(false);
							player.getInventory().deleteItem(6202, player.getInventory().getCount(6202));
							player.getInventory().deleteItem(6200, player.getInventory().getCount(6200));
							giveReward(player);
							if(player.getAttributes().isSet("preRandomLoc")) {
								player.setLocation((Location)player.getAttributes().get("preRandomLoc"));
							} else {
								player.setLocation(Constants.RESPAWN_LOCATION);
							}
							this.stop();
						}
					});
					this.stop();
				}
			});
			
			
		} else {
			player.getPacketSender().sendMessage("You can't leave this area.");
		}
	}
	
	public static void giveReward(Player player) {
		Item item = new Item(Constants.RANDOM_EVENT_ITEMS[Utils.random(0, Constants.RANDOM_EVENT_ITEMS.length - 1)], 1);
		if(player.getInventory().addItem(item)) {
			player.getPacketSender().sendMessage("You receive 1 x " + item.getDefinition().getName() + "!");
			player.getInventory().refresh();
			return;
		}/* else if(player.getInventory().addItem(item)){
			player.getPacketSender().sendMessage("1 x " + item.getDefinition().getName() + " added to your inventory!");
		}*/ else {
			player.getPacketSender().sendMessage("You do not have enough room in your inventory for a reward.");
		}
	}
}
