package com.hyperion.game.content.skills;

import java.io.FileInputStream;
import java.util.List;

import com.hyperion.Logger;
import com.hyperion.game.content.skills.fishing.FishSpot;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.utility.XStreamUtil;

public class SkillLoaders {

    public static List<FishSpot> fish_spots;

    public static void load() throws Exception {
        loadFishing();
    }

    @SuppressWarnings("unchecked")
    private static void loadFishing() throws Exception {
        fish_spots = (List<FishSpot>) XStreamUtil.getXStream().fromXML(new FileInputStream("data/world/npcs/fishspots.xml"));
        //Logger.getInstance().info("Loaded " + fish_spots.size() + " fishing spots.");
        int index = -1;
        for (FishSpot spot : fish_spots) {
            spot.setIndex(index++);
            new NPC(spot.getNpc_id());
        }
    }
}
