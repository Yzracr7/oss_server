package com.hyperion.game.content.skills.agility;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.masks.ForceMovement;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.gameobjects.GameObject;

/**
 * Agility Movement actions
 *
 * @author Nando
 * @author trees
 */
public class AgilityMovement {

    public void teethGrip(Player player, GameObject object, Location destination, int delay) {
        boolean delayed = player.getLocation().getY() != 3546;
        if (delayed) {
            player.getAttributes().set("stopActions", true);
            player.getWalkingQueue().reset();
            player.getWalkingQueue().addStep(destination.getX(), destination.getY());
            player.getWalkingQueue().finish();
            World.getWorld().submit(new Tickable(2) {
                @Override
                public void execute() {
                    this.stop();
                    player.getPacketSender().sendMessage("You walk carefully across the " + object.getName() + "...");
                    execute_force_walking(player, 1602, destination.getX(), destination.getY(), 1, delay, true, 22, "... and make it safely to the other side.");
                }
            });
        } else {
            player.getPacketSender().sendMessage("You walk carefully across the " + object.getName() + "...");
            execute_force_walking(player, 1602, destination.getX(), destination.getY(), 1, delay, true, 22, "... and make it safely to the other side.");
        }
    }

    public void walkRope(Player player, GameObject object, Location destination, int delay) {
        boolean delayed = player.getLocation().getY() != 3546;
        if (delayed) {
            player.getAttributes().set("stopActions", true);
            player.getWalkingQueue().reset();
            player.getWalkingQueue().addStep(destination.getX(), destination.getY());
            player.getWalkingQueue().finish();
            World.getWorld().submit(new Tickable(2) {
                @Override
                public void execute() {
                    this.stop();
                    player.getPacketSender().sendMessage("You walk carefully across the " + object.getName() + "...");
                    execute_force_walking(player, 762, destination.getX(), destination.getY(), 1, delay, true, 22, "... and make it safely to the other side.");
                }
            });
        } else {
            player.getPacketSender().sendMessage("You walk carefully across the " + object.getName() + "...");
            execute_force_walking(player, 762, destination.getX(), destination.getY(), 1, delay, true, 22, "... and make it safely to the other side.");
        }
    }

    public static void jump(final Player player, final int x, final int y, final int z) {
        player.getAttributes().set("stopActions", true);
        player.animate(3067);
        World.getWorld().submit(new Tickable(player, 1) {
            @Override
            public void execute() {
                this.stop();
                player.teleport(x, y, z);
                player.getAttributes().remove("stopActions");
            }
        });
    }

    public static void climb(final Player player, boolean down, final int x, final int y, final int z) {
        player.getAttributes().set("stopActions", true);
        if (!down) {//up
            player.animate(828);
        } else {//down
            player.animate(827);
        }
        World.getWorld().submit(new Tickable(player, 1) {
            @Override
            public void execute() {
                this.stop();
                player.teleport(x, y, z);
                player.getAttributes().remove("stopActions");
            }
        });
    }

    public static void execute_force_walking(final Player player, final int animation, final int endX, final int endY, int delayTillStart, final int delayTillDone, final boolean removeAttribute, final double exp, final String endMessage) {
        player.getAttributes().set("stopActions", true);
        boolean running = player.getWalkingQueue().isRunning();
        player.getWalkingQueue().setRunningToggled(false);
        if (delayTillStart != -1) {
            World.getWorld().submit(new Tickable(delayTillStart) {
                @Override
                public void execute() {
                    this.stop();
                    if (animation != -1) {
                        player.getVariables().setWalkAnimation(animation);
                        player.getVariables().setRunAnimation(animation);
                        player.getVariables().setStandAnimation(animation);
                        player.getVariables().setTurn180Animation(animation);
                        player.getVariables().setTurn90Clockwise(animation);
                        player.getVariables().setTurn90CounterClockwise(animation);
                        player.getVariables().setTurnAnimation(animation);
                        player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
                    }
                    player.getWalkingQueue().reset();
                    player.getWalkingQueue().addStep(endX, endY);
                    player.getWalkingQueue().finish();
                    World.getWorld().submit(new Tickable(delayTillDone) {
                        @Override
                        public void execute() {
                            this.stop();
                            player.getVariables().setWalkAnimation(-1);
                            player.getVariables().setRunAnimation(-1);
                            player.getVariables().setStandAnimation(-1);
                            player.getVariables().setTurn180Animation(-1);
                            player.getVariables().setTurn90Clockwise(-1);
                            player.getVariables().setTurn90CounterClockwise(-1);
                            player.getVariables().setTurnAnimation(-1);
                            player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
                            if (removeAttribute) {
                                player.getAttributes().remove("stopActions");
                            }
                            if (endMessage != null) {
                                player.getPacketSender().sendMessage(endMessage);
                            }
                            if (exp != -1) {
                                player.getSkills().addExperience(Skills.AGILITY, exp);
                            }
                        }
                    });
                }
            });
        } else {
            if (animation != -1) {
                player.getVariables().setWalkAnimation(animation);
                player.getVariables().setRunAnimation(animation);
                player.getVariables().setStandAnimation(animation);
                player.getVariables().setTurn180Animation(animation);
                player.getVariables().setTurn90Clockwise(animation);
                player.getVariables().setTurn90CounterClockwise(animation);
                player.getVariables().setTurnAnimation(animation);
                player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
            }
            player.getWalkingQueue().reset();
            player.getWalkingQueue().addStep(endX, endY);
            player.getWalkingQueue().finish();
            World.getWorld().submit(new Tickable(delayTillDone) {
                @Override
                public void execute() {
                    this.stop();
                    player.getVariables().setWalkAnimation(-1);
                    player.getVariables().setRunAnimation(-1);
                    player.getVariables().setStandAnimation(-1);
                    player.getVariables().setTurn180Animation(-1);
                    player.getVariables().setTurn90Clockwise(-1);
                    player.getVariables().setTurn90CounterClockwise(-1);
                    player.getVariables().setTurnAnimation(-1);
                    player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
                    if (removeAttribute) {
                        player.getAttributes().remove("stopActions");
                    }
                    if (endMessage != null) {
                        player.getPacketSender().sendMessage(endMessage);
                    }
                    if (exp != -1) {
                        player.getSkills().addExperience(Skills.AGILITY, exp);
                    }
                }
            });
        }
        if (running) {
            player.getWalkingQueue().setRunningToggled(true);
        }
    }

    public static void execute_teleport(final Player player, final int animation, final Location target, int delayTillStart, final int delayTillDone, final double exp, final String endMessage) {
        player.getAttributes().set("stopActions", true);
        if (delayTillStart != -1) {
            World.getWorld().submit(new Tickable(delayTillStart) {
                @Override
                public void execute() {
                    this.stop();
                    player.animate(animation);
                    World.getWorld().submit(new Tickable(delayTillDone) {
                        @Override
                        public void execute() {
                            this.stop();
                            player.teleport(target);
                            player.getAttributes().remove("stopActions");
                            if (endMessage != null) {
                                player.getPacketSender().sendMessage(endMessage);
                            }
                            if (exp != -1) {
                                player.getSkills().addExperience(Skills.AGILITY, exp);
                            }
                        }
                    });
                }
            });
        } else {
            player.animate(animation);
            World.getWorld().submit(new Tickable(delayTillDone) {
                @Override
                public void execute() {
                    this.stop();
                    player.teleport(target);
                    player.getAttributes().remove("stopActions");
                    if (endMessage != null) {
                        player.getPacketSender().sendMessage(endMessage);
                    }
                    if (exp != -1) {
                        player.getSkills().addExperience(Skills.AGILITY, exp);
                    }
                }
            });
        }
    }

    public static void execute_force_movement(final Player player, final Location target, final int animation, int delayTillStart, final int delayTillDone, final int dir, final int first_speed, final int second_speed, final boolean removeAttribute, final double exp, final String endMessage) {
        player.getAttributes().set("stopActions", true);
        if (delayTillStart != -1) {
            World.getWorld().submit(new Tickable(delayTillStart) {
                @Override
                public void execute() {
                    this.stop();
                    player.animate(animation);
                    player.getVariables().setNextForceMovement(new ForceMovement(player.getLocation(), first_speed, target, second_speed, dir));
                    player.getUpdateFlags().flag(UpdateFlag.FORCE_MOVEMENT);
                    World.getWorld().submit(new Tickable(delayTillDone) {
                        @Override
                        public void execute() {
                            this.stop();
                            player.teleport(target);
                            if (animation == 9908) {
                                player.animate(-1);
                            }
                            if (removeAttribute) {
                                player.getAttributes().remove("stopActions");
                            }
                            if (endMessage != null) {
                                player.getPacketSender().sendMessage(endMessage);
                            }
                            if (exp != -1) {
                                player.getSkills().addExperience(Skills.AGILITY, exp);
                            }
                        }
                    });
                }
            });
        } else {
            player.animate(animation);
            player.getVariables().setNextForceMovement(new ForceMovement(player.getLocation(), first_speed, target, second_speed, dir));
            player.getUpdateFlags().flag(UpdateFlag.FORCE_MOVEMENT);
            World.getWorld().submit(new Tickable(delayTillDone) {
                @Override
                public void execute() {
                    this.stop();
                    player.teleport(target);
                    player.animate(-1);
                    if (removeAttribute) {
                        player.getAttributes().remove("stopActions");
                    }
                    if (endMessage != null) {
                        player.getPacketSender().sendMessage(endMessage);
                    }
                    if (exp != -1) {
                        player.getSkills().addExperience(Skills.AGILITY, exp);
                    }
                }
            });
        }
    }
}
