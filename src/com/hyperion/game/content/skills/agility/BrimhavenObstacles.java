package com.hyperion.game.content.skills.agility;

import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;

public class BrimhavenObstacles {

	public static void handleObjects(Player player, GameObject object) {
		if (object.getId() == 5110 || object.getId() == 5111) {
			handleSteppingStones(player, object);
		}
	}
	
	private static void handleSteppingStones(Player player, GameObject object) {
		if(player.getAttributes().isSet("stopActions"))
			return;
		final Location playerLoc = player.getLocation();
		final int playerX = playerLoc.getX();
		final int playerY = playerLoc.getY();
		if (playerX == 2649 && playerY == 9562) {
			player.teleport(2647, 9557, 0);
			/*
			int[] forceMovementVars = {0, 0, 0, -1, 5, 35, 4, 1};
			//stop all actions
			player.getAttributes().set("stopActions", true);
			//1st step
			Agility.forceMovement(player, 741, forceMovementVars, 1, false, false);
			//2nd step
			Agility.forceMovement(player, 741, forceMovementVars, 3, false, false);
			//3rd step
			int[] forceMovementVars3 = {0, 0, -1, 0, 5, 35, 3, 1};
			Agility.forceMovement(player, 741, forceMovementVars3, 5, false, false);
			//4th step
			Agility.forceMovement(player, 741, forceMovementVars3, 7, false, false);
			//5th step
			int[] forceMovementVars4 = {0, 0, 0, -1, 5, 35, 2, 1};
			Agility.forceMovement(player, 741, forceMovementVars4, 9, false, false);
			//6th step
			Agility.forceMovement(player, 741, forceMovementVars4, 11, false, false);
			//7th step
			Agility.forceMovement(player, 741, forceMovementVars4, 13, false, true);
			*/
		} else if (playerX == 2647 && playerY == 9557) {
			player.teleport(2649, 9562, 0);
			/*
			int[] forceMovementVars = {0, 0, 0, 1, 5, 35, 7, 1};
			//stop all actions
			player.getAttributes().set("stopActions", true);
			//1st step
			Agility.forceMovement(player, 741, forceMovementVars, 1, false, false);
			//2nd step
			Agility.forceMovement(player, 741, forceMovementVars, 3, false, false);
			//3rd step
			Agility.forceMovement(player, 741, forceMovementVars, 5, true, false);
			*/
		}
	}
}
