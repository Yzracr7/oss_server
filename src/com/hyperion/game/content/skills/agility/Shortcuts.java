package com.hyperion.game.content.skills.agility;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.masks.ForceMovement;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.gameobjects.GameObject;

/**
 * Shortcuts
 *
 * @author trees
 */
public class Shortcuts extends AgilityMovement {

    public static void crawlIn(final Player player, final GameObject object) {
        if (player.getAttributes().isSet("stopActions"))
            return;
        player.getAttributes().set("stopActions", true);

        if (!player.getAttributes().isSet("crawlingin")) {
            player.getAttributes().set("crawlingin", true);
            player.animate(2796);
            player.setTeleporting(true);
            World.getWorld().submit(new Tickable(2) {
                @Override
                public void execute() {
                    if (object.getLocation().getX() == 3016 && (object.getLocation().getX() < player.getLocation().getX())) {
                        player.teleport(new Location(3065, 10159, 3));
                        this.stop();
                    } else if (object.getId() == 678) { //
                        player.teleport(new Location(2965, 4382, 2));
                        this.stop();
                    } else if (object.getId() == 6481) {// DT Pyramid
                        player.teleport(new Location(3230, 9311, 0));
                        this.stop();
                    } else if (object.getId() == 30177) { // Kraken Cave Entrance
                        player.teleport(new Location(2276, 9988, 0));
                        this.stop();
                    } else if (object.getId() == 30178 && object.getLocation().isWithinDistance(Location.create(2276, 9987, 0))) {
                        player.teleport(new Location(2276, 9988, 0));
                        this.stop();
                    }
                }
            });
            player.getAttributes().

                    remove("stopActions");
            player.getAttributes().

                    remove("crawlingin");
            return;
        }
    }

    public static void edgeExchangePip(final Player player, final GameObject object) {
        if (player.getAttributes().isSet("stopActions"))
            return;
        if (21 > player.getSkills().getLevel(Skills.AGILITY)) {
            player.getPacketSender().sendMessage("You must have a Agility level of 21 to navigate this shortcut.");
            return;
        }
        player.getAttributes().set("stopActions", true);

        if (!player.getAttributes().isSet("edgeexchangepip")) {
            player.getAttributes().set("edgeexchangepip", true);
            player.animate(844);
            player.setTeleporting(true);
            World.getWorld().submit(new Tickable(3) {
                @Override
                public void execute() {
                    if (player.getLocation().equals(Location.create(3142, 3513, 0))) {
                        player.teleport(new Location(3137, 3516, 0));
                        this.stop();
                    } else {
                        player.teleport(new Location(3142, 3513, 0));
                        this.stop();
                    }
                }
            });
            player.getAttributes().remove("stopActions");
            player.getAttributes().remove("edgeexchangepip");
            return;
        }
    }

    public static void tavPipe(final Player player, final GameObject object) {
        if (player.getAttributes().isSet("stopActions"))
            return;
        player.getAttributes().set("stopActions", true);
        boolean headEast = false;
        if (object.getLocation().getX() <= 2888)
            headEast = true;
        final int oX = object.getLocation().getX();
        final int oY = object.getLocation().getY();
        final int dir = headEast ? -1 : +2;

        boolean correctTile = player.getLocation().equals(Location.create(oX + dir, oY, object.getLocation().getZ()));
        if (!correctTile && !player.getAttributes().isSet("tavpipewalk")) {
            player.getWalkingQueue().addStep(oX + dir, oY);
            player.getWalkingQueue().finish();
            player.getAttributes().set("tavpipewalk", true);
            World.getWorld().submit(new Tickable(3) {
                @Override
                public void execute() {
                    if (player.getLocation().equals(Location.create(oX + dir, oY, object.getLocation().getZ()))) {
                        tavPipe(player, object);

                        this.stop();
                    } else {
                        player.getWalkingQueue().addStep(oX + dir, oY);
                        player.getWalkingQueue().finish();
                        this.setTickDelay(1);
                    }
                }
            });
            return;
        }
        if (player.getSkills().getLevel(Skills.AGILITY) < 70) {
            player.getPacketSender().sendMessage("You must have an Agility level of 70 to use this shortcut.");
            player.getAttributes().remove("stopActions");
            return;
        }
        player.getPacketSender().sendMessage("You pull yourself through the pipes.");
        player.face(object.getLocation());
        int xToGo = object.getLocation().getX() + (headEast ? 2 : -2);
        int yToGo = object.getLocation().getY();
        execute_force_movement(player, Location.create(xToGo, yToGo, 0), 10580, 1, 2, (headEast ? ForceMovement.EAST : ForceMovement.WEST), 20, 80, false, -1, null);
        execute_force_walking(player, 844, xToGo + (headEast ? +2 : -1), yToGo, 4, 2, false, -1, null);
        execute_force_movement(player, Location.create(xToGo + (headEast ? +3 : -2), yToGo, 0), 10579, 6, 1, (headEast ? ForceMovement.EAST : ForceMovement.WEST), 10, 30, true, 25, null);
        //	player.face(headEast ? player.getLocation().getX() - 1 : player.getLocation().getX() + 1, player.getLocation().getY(), player.getLocation().getZ());
    }

    public static void jumpTavSpikes(final Player player, final GameObject ditch) {
        if (player.getSkills().getLevel(Skills.AGILITY) < 70) {
            player.getPacketSender().sendMessage("You must have an Agility level of 70 to use this shortcut.");
            return;
        }
        player.getAttributes().set("stopActions", true);
        final Location playerLoc = player.getLocation();
        player.animate(6132);
        Location before_ditch = Location.create(ditch.getLocation().getX() + 1, ditch.getLocation().getY(), 0);
        Location after_ditch = Location.create(ditch.getLocation().getX() - 1, ditch.getLocation().getY(), 0);
        final Location toTile = playerLoc.equals(before_ditch) ? after_ditch : before_ditch;
        final int dir = player.getLocation().equals(after_ditch) ? ForceMovement.EAST : ForceMovement.WEST;
        player.getVariables().setNextForceMovement(new ForceMovement(playerLoc, 33, toTile, 60, dir));
        player.getUpdateFlags().flag(UpdateFlag.FORCE_MOVEMENT);
        World.getWorld().submit(new Tickable(2) {
            @Override
            public void execute() {
                player.getAttributes().remove("stopActions");
                player.teleport(toTile);
                this.stop();
            }
        });
    }
}
