package com.hyperion.game.content.skills.agility.courses;

import java.util.ArrayList;
import java.util.List;

import com.hyperion.game.content.skills.agility.AgilityMovement;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.masks.ForceMovement;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.gameobjects.GameObject;

/**
 * @author Nando
 */
public class BarbarianCourse extends AgilityMovement {

	/**
	 * Represents an agility obstacle.
	 * @author Michael
	 */
	private enum BarbarianObstacle {
		
		/**
		 * Barbarian obstacle course
		 */
		
		BARBARIAN_COURSE_OBSTACLE_PIPE(2287, Location.create(2552, 3559, 0), 35, 0),
		
		BARBARIAN_COURSE_ROPE_SWING(43526, Location.create(2551, 3554, 0), Location.create(2551, 3550, 0), 35, 22),
		
		BARBARIAN_COURSE_LOG_BALANCE(2294, Location.create(2551, 3546, 0), Location.create(2550, 3546, 0), 35, 13.7),
		
		BARBARIAN_COURSE_OBSTACLE_NET(2284, Location.create(2539, 3545, 0), Location.create(2538, 3545, 0), 35, 8.2),
		
		BARBARIAN_COURSE_LEDGE(2302, Location.create(2535, 3547, 1), 35, 22),
		
		BARBARIAN_COURSE_CRUMBLING_WALL_1(1948, Location.create(2535, 3553, 0), Location.create(2536, 3553, 0), 35, 13.7),
		
		BARBARIAN_COURSE_CRUMBLING_WALL_2(1948, Location.create(2538, 3553, 0), Location.create(2539, 3553, 0), 35, 13.7),
		
		BARBARIAN_COURSE_CRUMBLING_WALL_3(1948, Location.create(2541, 3553, 0), Location.create(2542, 3553, 0), 35, 13.7),
		
		;
		
		/**
		 * The list of obstacles.
		 */
		private static List<BarbarianObstacle> obstacles = new ArrayList<BarbarianObstacle>();
		
		/**
		 * Populates the obstacle list
		 */
		static {
			for(BarbarianObstacle obstacle : BarbarianObstacle.values()) {
				obstacles.add(obstacle);
			}
		}
		
		public BarbarianObstacle forId(int id) {
			for(BarbarianObstacle obstacle : obstacles) {
				if(obstacle.getId() == id) {
					return obstacle;
				}
			}
			return null;
		}
		
		public static BarbarianObstacle forLocation(Location location) {
			for(BarbarianObstacle obstacle : obstacles) {
				if(obstacle.getLocation().equals(location)) {
					return obstacle;
				}
			}
			return null;
		}
		
		/**
		 * Object id.
		 */
		private int id;
		
		/**
		 * The location of this obstacle.
		 */
		private Location location;
		
		/**
		 * Where we should be at to speak.
		 */
		private Location standingLocation;
		
		/**
		 * The level required to use this obstacle.
		 */
		private int levelRequired;
		
		/**
		 * The experience granted for tackling this obstacle.
		 */
		private double experience;

		private BarbarianObstacle(int id, Location standingLocation, Location location, int levelRequired, double experience) {
			this.id = id;
			this.standingLocation = standingLocation;
			this.location = location;
			this.levelRequired = levelRequired;
			this.experience = experience;
		}
		
		private BarbarianObstacle(int id, Location location, int levelRequired, double experience) {
			this.id = id;
			this.standingLocation = location;
			this.location = location;
			this.levelRequired = levelRequired;
			this.experience = experience;
		}

		public int getId() {
			return id;
		}

		public Location getLocation() {
			return location;
		}

		public int getLevelRequired() {
			return levelRequired;
		}

		public double getExperience() {
			return experience;
		}

		public Location getStandingLocation() {
			return standingLocation;
		}
	}
	
	public static boolean isBarbarianCourse(Player player, GameObject object) {
		final BarbarianObstacle obstacle = BarbarianObstacle.forLocation(object.getLocation());
		if(obstacle != null) {
			execute_course(player, obstacle, object);
			return true;
		}
		return false;
	}
	
	private static void execute_course(final Player player, final BarbarianObstacle obstacle, final GameObject object) {
		if (player.getSkills().getLevel(Skills.AGILITY) < obstacle.getLevelRequired()) {
			player.getPacketSender().sendMessage("You need an Agility level of "+obstacle.getLevelRequired()+" to use this obstacle.");
			return;
		}
		if(player.getAttributes().isSet("stopActions"))
			return;
		//Location stand = obstacle.getStandingLocation();
		/*System.out.println(stand.toString());
		if (stand != null && !stand.equals(player.getLocation())) {
			player.execute_path(new DefaultPathFinder(), stand.getX(), stand.getY());
			World.getWorld().submit(new Tickable(player, 2) {
				@Override
				public void execute() {
					this.stop();
					execute_course(player, obstacle, object);
				}
			});
			return;
		}*/
		switch (obstacle) {
		case BARBARIAN_COURSE_OBSTACLE_PIPE:
			execute_force_movement(player, Location.create(obstacle.getLocation().getX(), player.getLocation().getY() >= 3561 ? 3558 : 3561, player.getLocation().getZ()), 10580, -1, 2, player.getLocation().getY() >= 3561 ? ForceMovement.SOUTH : ForceMovement.NORTH, 30, 60, true, -1, null);
			break;
		case BARBARIAN_COURSE_ROPE_SWING:
			if (player.getLocation().getY() != 3554) {
				player.getPacketSender().sendMessage("You'll need to get closer to make this jump.");
			} else {
				player.getPacketSender().sendMessage("You skillfully swing across.");
				execute_force_movement(player, Location.create(obstacle.getLocation().getX(), 3549, 0), 751, -1, 2, ForceMovement.SOUTH, 30, 60, true, obstacle.getExperience(), null);
				set_course_stage(player, 1);
				World.sendObjectAnimation(player, object, 497);
			}
			break;
		case BARBARIAN_COURSE_LOG_BALANCE:
			boolean delayed = player.getLocation().getY() != 3546;
			if (delayed) {
				player.getAttributes().set("stopActions", true);
				player.getWalkingQueue().reset();
				player.getWalkingQueue().addStep(2550, 3546);
				player.getWalkingQueue().finish();
				World.getWorld().submit(new Tickable(2) {
					@Override
					public void execute() {
						this.stop();
						player.getPacketSender().sendMessage("You walk carefully across the slippery log...");
						execute_force_walking(player, 762, 2541, object.getLocation().getY(), 1, 10, true, obstacle.getExperience(), "... and make it safely to the other side.");
					}
				});
			} else {
				player.getPacketSender().sendMessage("You walk carefully across the slippery log...");
				execute_force_walking(player, 762, 2541, object.getLocation().getY(), 1, 10, true, obstacle.getExperience(), "... and make it safely to the other side.");
			}
			if (getCourseStage(player) == 1)
				set_course_stage(player, 2);
			break;
		case BARBARIAN_COURSE_OBSTACLE_NET:
			if (player.getLocation().getX() == 2539) {
				player.getPacketSender().sendMessage("You climb the netting...");
				execute_teleport(player, 828, Location.create(object.getLocation().getX() - 1, player.getLocation().getY(), 1), -1, 2, obstacle.getExperience(), null);
				if (getCourseStage(player) == 2)
					set_course_stage(player, 3);
			}
			break;
		case BARBARIAN_COURSE_LEDGE:
			player.animate(753);
			execute_force_walking(player, 756, 2532, object.getLocation().getY(), 1, 4, false, obstacle.getExperience(), null);
			execute_force_walking(player, -1, 2532, 3546, 6, 4, false, -1, null);
			execute_teleport(player, 827, Location.create(2532, 3546, 0), 8, 2, -1, null);
			break;
		case BARBARIAN_COURSE_CRUMBLING_WALL_1:
		case BARBARIAN_COURSE_CRUMBLING_WALL_2:
		case BARBARIAN_COURSE_CRUMBLING_WALL_3:
			if (player.getLocation().getX() < object.getLocation().getX()) {
				Location target = Location.create(object.getLocation().getX() + 1, object.getLocation().getY(), object.getPlane());
				execute_force_movement(player, target, 4853, -1, 2, ForceMovement.EAST, 30, 60, true, obstacle.getExperience(), null);
				if (getCourseStage(player) == 3) {
					set_course_stage(player, 4);
				} else if (getCourseStage(player) == 4) {
					set_course_stage(player, 5);
				} else if (getCourseStage(player) == 5) {
					remove_course_stage(player);
					player.getSkills().addExperience(Skills.AGILITY, 46.2);
				}
			} else {
				player.getPacketSender().sendMessage("You cannot climb from this side.");
			}
			break;
		}
	}
	
	private static void set_course_stage(Player player, int stage) {
		player.getAttributes().set("barbariancourse", stage);
	}
	
	private static void remove_course_stage(Player player) {
		player.getAttributes().remove("barbariancourse");
	}
	
	private static int getCourseStage(Player player) {
		return player.getAttributes().getInt("barbariancourse");
	}
}
