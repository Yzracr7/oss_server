package com.hyperion.game.content.skills.agility.courses;

import java.util.ArrayList;
import java.util.List;

import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.content.skills.agility.AgilityMovement;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.masks.ForceMovement;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.game.world.pathfinders.DefaultPathFinder;

/**
 * @author Nando
 */
public class GnomeCourse extends AgilityMovement {

	/**
	 * Represents an agility obstacle.
	 * @author Michael
	 */
	private enum GnomeObstacle {
		
		/**
		 * Gnome obstacle course
		 */
		
		GNOME_COURSE_LOG_BALANCE(2295, Location.create(2474, 3435, 0), 1, 7),
		
		GNOME_COURSE_OBSTACLE_NET_1(2285, Location.create(2471, 3425, 0), 1, 8),
		
		GNOME_COURSE_OBSTACLE_NET_2(2285, Location.create(2473, 3425, 0), 1, 8),
		
		GNOME_COURSE_OBSTACLE_NET_3(2285, Location.create(2475, 3425, 0), 1, 8),
		
		GNOME_COURSE_TREE_BRANCH(2313, Location.create(2473, 3422, 1), 1, 5),
		
		GNOME_COURSE_BALANCE_ROPE(2312, Location.create(2478, 3420, 2), 1, 7),
		
		GNOME_COURSE_TREE_BRANCH_2(2314, Location.create(2486, 3419, 2), 1, 5),
		
		GNOME_COURSE_OBSTACLE_NET_4(2286, Location.create(2483, 3426, 0), 1, 8),
		
		GNOME_COURSE_OBSTACLE_NET_5(2286, Location.create(2485, 3426, 0), 1, 8),
		
		GNOME_COURSE_OBSTACLE_NET_6(2286, Location.create(2487, 3426, 0), 1, 8),
		
		GNOME_COURSE_OBSTACLE_PIPE_1(154, Location.create(2484, 3430, 0), Location.create(2484, 3431, 0), 1, 8),
		
		GNOME_COURSE_OBSTACLE_PIPE_2(43543, Location.create(2487, 3430, 0), Location.create(2487, 3431, 0), 1, 8),
		
		;
		
		/**
		 * The list of obstacles.
		 */
		private static List<GnomeObstacle> obstacles = new ArrayList<GnomeObstacle>();
		
		/**
		 * Populates the obstacle list
		 */
		static {
			for(GnomeObstacle obstacle : GnomeObstacle.values()) {
				obstacles.add(obstacle);
			}
		}
		
		public GnomeObstacle forId(int id) {
			for(GnomeObstacle obstacle : obstacles) {
				if(obstacle.getId() == id) {
					return obstacle;
				}
			}
			return null;
		}
		
		public static GnomeObstacle forLocation(Location location) {
			for(GnomeObstacle obstacle : obstacles) {
				if(obstacle.getLocation().equals(location)) {
					return obstacle;
				}
			}
			return null;
		}
		
		/**
		 * Object id.
		 */
		private int id;
		
		/**
		 * The location of this obstacle.
		 */
		private Location location;
		
		/**
		 * Where we should be at to speak.
		 */
		private Location standingLocation;
		
		/**
		 * The level required to use this obstacle.
		 */
		private int levelRequired;
		
		/**
		 * The experience granted for tackling this obstacle.
		 */
		private double experience;

		private GnomeObstacle(int id, Location standingLocation, Location location, int levelRequired, double experience) {
			this.id = id;
			this.standingLocation = standingLocation;
			this.location = location;
			this.levelRequired = levelRequired;
			this.experience = experience;
		}
		
		private GnomeObstacle(int id, Location location, int levelRequired, double experience) {
			this.id = id;
			this.standingLocation = location;
			this.location = location;
			this.levelRequired = levelRequired;
			this.experience = experience;
		}

		public int getId() {
			return id;
		}

		public Location getLocation() {
			return location;
		}

		public int getLevelRequired() {
			return levelRequired;
		}

		public double getExperience() {
			return experience;
		}

		public Location getStandingLocation() {
			return standingLocation;
		}
	}
	
	public static boolean isGnomeCourse(Player player, GameObject object) {
		final GnomeObstacle obstacle = GnomeObstacle.forLocation(object.getLocation());
		if(obstacle != null) {
			execute_course(player, obstacle);
			return true;
		}
		return false;
	}
	
	private static void execute_course(final Player player, final GnomeObstacle obstacle) {
		if(player.getAttributes().isSet("stopActions"))
			return;
		Location stand = obstacle.getStandingLocation();
		if (obstacle == GnomeObstacle.GNOME_COURSE_OBSTACLE_PIPE_1 || obstacle == GnomeObstacle.GNOME_COURSE_OBSTACLE_PIPE_2) {
			if (stand != null && !stand.equals(player.getLocation())) {
				player.execute_path(new DefaultPathFinder(), stand.getX(), stand.getY());
				World.getWorld().submit(new Tickable(player, 3) {
					@Override
					public void execute() {
						this.stop();
						execute_course(player, obstacle);
					}
				});
				return;
			}
		} else {
			if (obstacle == GnomeObstacle.GNOME_COURSE_OBSTACLE_NET_4 || obstacle == GnomeObstacle.GNOME_COURSE_OBSTACLE_NET_5 
					|| obstacle == GnomeObstacle.GNOME_COURSE_OBSTACLE_NET_6) {
				if (player.getLocation().getY() != 3425) {
					player.getPacketSender().sendMessage("You can't go over the net from here.");
					return;
				}
			}
		}
		switch (obstacle) {
		case GNOME_COURSE_LOG_BALANCE:
			player.getPacketSender().sendMessage("You walk carefully across the slippery log...");
			execute_force_walking(player, 762, 2474, 3429, 1, 7, true, obstacle.getExperience(), "... and make it safely to the other side.");
			set_course_stage(player, 1);
			break;
		case GNOME_COURSE_OBSTACLE_NET_1:
		case GNOME_COURSE_OBSTACLE_NET_2:
		case GNOME_COURSE_OBSTACLE_NET_3:
			player.getPacketSender().sendMessage("You climb the netting.");
			execute_teleport(player, 828, Location.create(player.getLocation().getX(), 3423, 1), 1, 3, obstacle.getExperience(), null);
			if (getCourseStage(player) == 1)
				set_course_stage(player, 2);
			break;
		case GNOME_COURSE_TREE_BRANCH:
			player.getPacketSender().sendMessage("You climb the tree...");
			execute_teleport(player, 828, Location.create(2473, 3420, 2), 1, 2, obstacle.getExperience(), "... to the platform above.");
			if (getCourseStage(player) == 2)
				set_course_stage(player, 3);
			break;
		case GNOME_COURSE_BALANCE_ROPE:
			execute_force_walking(player, 762, 2483, 3420, 1, 7, true, obstacle.getExperience(), "You passed the obstacle succesfully.");
			break;
		case GNOME_COURSE_TREE_BRANCH_2:
			execute_teleport(player, 828, Location.create(2487, 3421, 0), 1, 2, obstacle.getExperience(), "You climbed down the tree branch succesfully.");
			if (getCourseStage(player) == 3)
				set_course_stage(player, 4);
			break;
		case GNOME_COURSE_OBSTACLE_NET_4:
		case GNOME_COURSE_OBSTACLE_NET_5:
		case GNOME_COURSE_OBSTACLE_NET_6:
			player.getPacketSender().sendMessage("You climb the netting.");
			execute_teleport(player, 828, Location.create(player.getLocation().getX(), 3427, 0), 1, 2, obstacle.getExperience(), null);
			if (getCourseStage(player) == 4)
				set_course_stage(player, 5);
			break;
		case GNOME_COURSE_OBSTACLE_PIPE_1:
		case GNOME_COURSE_OBSTACLE_PIPE_2:
			int xToGo = obstacle.getLocation().getX();
			int yToGo = obstacle.getLocation().getY() + 2;
			player.getPacketSender().sendMessage("You pull yourself through the pipes.");
			if (getCourseStage(player) == 5) {
				remove_course_stage(player);
				player.getSkills().addExperience(Skills.AGILITY, 39.5);
				Achievements.increase(player, 18);
			}
			execute_force_movement(player, Location.create(xToGo, yToGo, 0), 10580, 1, 2, ForceMovement.NORTH, 20, 80, false, -1, null);
			execute_force_walking(player, 844, xToGo, yToGo + 2, 4, 2, false, -1, null);
			execute_force_movement(player, Location.create(xToGo, yToGo + 4, 0), 10579, 6, 1, ForceMovement.NORTH, 10, 30, true, obstacle.getExperience(), null);
			break;
		}
	}
	
	private static void set_course_stage(Player player, int stage) {
		player.getAttributes().set("gnomecourse", stage);
	}
	
	private static void remove_course_stage(Player player) {
		player.getAttributes().remove("gnomecourse");
	}
	
	private static int getCourseStage(Player player) {
		return player.getAttributes().getInt("gnomecourse");
	}
}
