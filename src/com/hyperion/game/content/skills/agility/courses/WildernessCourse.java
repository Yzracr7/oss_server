package com.hyperion.game.content.skills.agility.courses;

import java.util.ArrayList;
import java.util.List;

import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.content.skills.agility.AgilityMovement;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.masks.ForceMovement;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.game.world.pathfinders.DefaultPathFinder;

/**
 * @author Michael
 * @author Nando
 */
public class WildernessCourse extends AgilityMovement {
	
	//0 = 1st bottom gate at wildy course.
	//1 = double gates at top.
	private static boolean[] doorsUsed = new boolean[2];
		
	/**
	 * Represents an agility obstacle.
	 */
	private enum WildernessObstacle {
		
		/**
		 * Wilderness agility course
		 */
		WILDERNESS_COURSE_OBSTACLE_PIPE(2288, Location.create(3004, 3937, 0), Location.create(3004, 3938, 0), 52, 12.5),
		
		WILDERNESS_ROPE_SWING(2283, Location.create(3005, 3953, 0), Location.create(3005, 3952, 0), 52, 20),
		
		WILDERNESS_STEPPING_STONES(37704, Location.create(3002, 3960, 0), Location.create(3001, 3960, 0), 52, 20),
		
		WILDERNESS_LOG_BALANCE(2297, Location.create(3002, 3945, 0), Location.create(3001, 3945, 0), 52, 518),
		//WILDERNESS_LOG_BALANCE(2297, Location.create(3002, 3945, 0), Location.create(3001, 3945, 0), 52, 20),
		
		WILDERNESS_ROCKS(2328, Location.create(2995, 3937, 0), Location.create(2995, 3936, 0), 52, 0),
		
		WILDERNESS_ROCKS2(2328, Location.create(2994, 3937, 0), Location.create(2994, 3936, 0), 52, 0),
		
		WILDERNESS_ROCKS3(2328, Location.create(2993, 3937, 0), Location.create(2993, 3936, 0), 52, 0),
		
		WILDERNESS_GATES(2309, Location.create(2998, 3916, 0), Location.create(2998, 3917, 0), 52, 0),
		
		WILDERNESS_GATES2(2307, Location.create(2998, 3931, 0), Location.create(2998, 3931, 0), 1, 0),
		
		WILDERNESS_GATES3(2308, Location.create(2998, 3931, 0), Location.create(2997, 3931, 0), 1, 0),
		
		;
		
		/**
		 * The list of obstacles.
		 */
		private static List<WildernessObstacle> obstacles = new ArrayList<WildernessObstacle>();
		
		/**
		 * Populates the obstacle list
		 */
		static {
			for(WildernessObstacle obstacle : WildernessObstacle.values()) {
				obstacles.add(obstacle);
			}
		}
		
		public WildernessObstacle forId(int id) {
			for(WildernessObstacle obstacle : obstacles) {
				if(obstacle.getId() == id) {
					return obstacle;
				}
			}
			return null;
		}
		
		public static WildernessObstacle forLocation(Location location) {
			for(WildernessObstacle obstacle : obstacles) {
				if(obstacle.getLocation().equals(location)) {
					return obstacle;
				}
			}
			return null;
		}
		
		/**
		 * Object id.
		 */
		private int id;
		
		/**
		 * The location of this obstacle.
		 */
		private Location location;
		
		/**
		 * Where we should be at to speak.
		 */
		private Location standingLocation;
		
		/**
		 * The level required to use this obstacle.
		 */
		private int levelRequired;
		
		/**
		 * The experience granted for tackling this obstacle.
		 */
		private double experience;

		private WildernessObstacle(int id, Location standingLocation, Location location, int levelRequired, double experience) {
			this.id = id;
			this.standingLocation = standingLocation;
			this.location = location;
			this.levelRequired = levelRequired;
			this.experience = experience;
		}
		
		private WildernessObstacle(int id, Location location, int levelRequired, double experience) {
			this.id = id;
			this.standingLocation = location;
			this.location = location;
			this.levelRequired = levelRequired;
			this.experience = experience;
		}

		public int getId() {
			return id;
		}

		public Location getLocation() {
			return location;
		}

		public int getLevelRequired() {
			return levelRequired;
		}

		public double getExperience() {
			return experience;
		}

		public Location getStandingLocation() {
			return standingLocation;
		}
	}
	
	public static boolean isWildernessCourse(Player player, GameObject object) {
		final WildernessObstacle obstacle = WildernessObstacle.forLocation(object.getLocation());
		if(obstacle != null) {
			execute_course(player, obstacle, object);
			return true;
		}
		return false;
	}
	
	private static void execute_course(final Player player, final WildernessObstacle obstacle, final GameObject object) {
		if (player.getSkills().getLevel(Skills.AGILITY) < obstacle.getLevelRequired()) {
			player.getPacketSender().sendMessage("You need an Agility level of "+obstacle.getLevelRequired()+" to use this obstacle.");
			return;
		}
		if(player.getAttributes().isSet("stopActions"))
			return;
		Location stand = obstacle.getStandingLocation();
		if (stand != null && !stand.equals(player.getLocation())) {
			player.execute_path(new DefaultPathFinder(), stand.getX(), stand.getY());
			World.getWorld().submit(new Tickable(player, 2) {
				@Override
				public void execute() {
					this.stop();
					execute_course(player, obstacle, object);
				}
			});
			return;
		}
		switch (obstacle) {
		case WILDERNESS_COURSE_OBSTACLE_PIPE:
			Location loc = player.getLocation();
			player.getPacketSender().sendMessage("You squeeze into the pipe...");
			execute_force_movement(player, Location.create(loc.getX(), loc.getY() + 2, 0), 10580, -1, 2, ForceMovement.NORTH, 30, 60, false, -1, null);
			execute_force_walking(player, 844, loc.getX(), loc.getY() + 6, 3, 4, true, -1, null);
			execute_teleport(player, -1, Location.create(loc.getX(), loc.getY() + 10, 0), 5, 3, -1, null);
			execute_force_movement(player, Location.create(loc.getX(), loc.getY() + 13, 0), 10579, 10, 1, ForceMovement.NORTH, 10, 30, false, -1, null);
			set_course_stage(player, 1);
			break;
		case WILDERNESS_ROPE_SWING:
			World.sendObjectAnimation(player, object, 497);
			Location target = Location.create(object.getLocation().getX(), 3958, object.getPlane());
			execute_force_movement(player, target, 751, -1, 2, ForceMovement.NORTH, 30, 60, true, obstacle.getExperience(), null);
			if (getCourseStage(player) == 1) {
				set_course_stage(player, 2);
			} else {
				remove_course_stage(player);
			}
			break;
		case WILDERNESS_STEPPING_STONES://TODO - into a loop
			Location toTile = Location.create(3002 - 1, player.getLocation().getY(), player.getLocation().getZ());
			execute_force_movement(player, toTile, 741, -1, 1, ForceMovement.WEST, 5, 35, false, -1, null);
			//2nd
			toTile = Location.create(3002 - 2, player.getLocation().getY(), player.getLocation().getZ());
			execute_force_movement(player, toTile, 741, 2, 1, ForceMovement.WEST, 5, 35, false, -1, null);
			//3rd
			toTile = Location.create(3002 - 3, player.getLocation().getY(), player.getLocation().getZ());
			execute_force_movement(player, toTile, 741, 4, 1, ForceMovement.WEST, 5, 35, false, -1, null);
			//4th
			toTile = Location.create(3002 - 4, player.getLocation().getY(), player.getLocation().getZ());
			execute_force_movement(player, toTile, 741, 6, 1, ForceMovement.WEST, 5, 35, false, -1, null);
			//5th
			toTile = Location.create(3002 - 5, player.getLocation().getY(), player.getLocation().getZ());
			execute_force_movement(player, toTile, 741, 8, 1, ForceMovement.WEST, 5, 35, false, -1, null);
			//6th
			toTile = Location.create(3002 - 6, player.getLocation().getY(), player.getLocation().getZ());
			execute_force_movement(player, toTile, 741, 10, 1, ForceMovement.WEST, 5, 35, true, obstacle.getExperience(), null);
			if (getCourseStage(player) == 2) {
				set_course_stage(player, 3);
			} else {
				remove_course_stage(player);
			}
			break;
		case WILDERNESS_LOG_BALANCE:
			player.getPacketSender().sendMessage("You walk carefully across the slippery log...");
			execute_force_walking(player, 762, 2994, object.getLocation().getY(), 1, 8, true, obstacle.getExperience(), "... and make it safely to the other side.");
			if (getCourseStage(player) == 3) {
				set_course_stage(player, 4);
			} else {
				remove_course_stage(player);
			}
			break;
		case WILDERNESS_ROCKS:
		case WILDERNESS_ROCKS2:
		case WILDERNESS_ROCKS3:
			execute_force_movement(player, Location.create(object.getLocation().getX(), 3932, 0), 740, -1, 3, ForceMovement.SOUTH, 0, 90, true, -1, null);
			if (getCourseStage(player) != 4) {
				remove_course_stage(player);
			} else {
				player.getSkills().addExperience(Skills.AGILITY, 498.9);
				Achievements.increase(player, 35);
				set_course_stage(player, 0);
			}
			break;
		case WILDERNESS_GATES:
			if (!doorsUsed[0]) {
				toggleDoorStatus(0, true, -1);
				toggleDoorStatus(0, false, 4);
				World.spawnFakeObjectTemporary(player, new GameObject(object.getLocation(), object.getId(), object.getType(), 0), object, -1, 3);
				execute_force_walking(player, 762, 2998, 3931, -1, 17, true, -1, null);
				//top gates
				toggleDoorStatus(1, true, 15);
				toggleDoorStatus(1, false, 18);
				//EAST GATE ~~~~
				final Location east_gate_loc = Location.create(2998, 3931, 0);
				final GameObject east_gate = World.getObjectWithId(east_gate_loc, 2307);
				//remove east gate
				if(east_gate != null)
					World.spawnFakeObjectTemporary(player, new GameObject(east_gate.getLocation(), -1, east_gate.getType(), 0), east_gate, 15, 2);
				//spawn opened east gate
				Location east_gate_opened_loc = Location.create(2998, 3930, 0);
				World.spawnFakeObjectTemporary(player, new GameObject(east_gate_opened_loc, 2307, 0, 2), new GameObject(east_gate_opened_loc, -1, 0, 2), 15, 2);
				//WEST GATE ~~~
				final Location west_gate_loc = Location.create(2997, 3931, 0);
				final GameObject west_gate = World.getObjectWithId(west_gate_loc, 2308);
				//remove west gate
				if(west_gate != null)
					World.spawnFakeObjectTemporary(player, new GameObject(west_gate.getLocation(), -1, west_gate.getType(), 0), west_gate, 15, 2);
				//spawn opened west gate
				Location west_gate_opened_loc = Location.create(2997, 3930, 0);
				World.spawnFakeObjectTemporary(player, new GameObject(west_gate_opened_loc, 2308, 0, 4), new GameObject(west_gate_opened_loc, -1, 0, 4), 15, 2);
			}
			break;
		case WILDERNESS_GATES2:
		case WILDERNESS_GATES3:
			if (!doorsUsed[1]) {
				execute_force_walking(player, 762, 2998, 3916, 1, 16, true, -1, null);
				//TOP GATES
				toggleDoorStatus(1, true, -1);
				toggleDoorStatus(1, false, 4);
				//EAST GATE ~~~~
				final Location east_gate_loc = Location.create(2998, 3931, 0);
				final GameObject east_gate = World.getObjectWithId(east_gate_loc, 2307);
				//remove east gate
				World.spawnFakeObjectTemporary(player, new GameObject(east_gate.getLocation(), -1, east_gate.getType(), 0), east_gate, -1, 3);
				//spawn opened east gate
				Location east_gate_opened_loc = Location.create(2998, 3930, 0);
				World.spawnFakeObjectTemporary(player, new GameObject(east_gate_opened_loc, 2307, 0, 2), new GameObject(east_gate_opened_loc, -1, 0, 2), -1, 3);
				//WEST GATE ~~~
				final Location west_gate_loc = Location.create(2997, 3931, 0);
				final GameObject west_gate = World.getObjectWithId(west_gate_loc, 2308);
				//remove west gate
				World.spawnFakeObjectTemporary(player, new GameObject(west_gate.getLocation(), -1, west_gate.getType(), 0), west_gate, -1, 3);
				//spawn opened west gate
				Location west_gate_opened_loc = Location.create(2997, 3930, 0);
				World.spawnFakeObjectTemporary(player, new GameObject(west_gate_opened_loc, 2308, 0, 4), new GameObject(west_gate_opened_loc, -1, 0, 4), -1, 3);
				//BOTTOM GATE ~~~~
				final Location bottom_gate_loc = Location.create(2998, 3917, 0);
				final GameObject bottom_gate = World.getObjectWithId(bottom_gate_loc, 2309);
				//spawn/remove
				World.spawnFakeObjectTemporary(player, new GameObject(bottom_gate.getLocation(), bottom_gate.getId(), bottom_gate.getType(), 0), bottom_gate, 15, 2);
				//disable it temp
				toggleDoorStatus(0, true, 15);
				toggleDoorStatus(0, false, 18);
			}
			break;
		}
	}
	
	private static void toggleDoorStatus(final int doorIndex, final boolean bool, final int delay) {
		if (delay != -1) {
			World.getWorld().submit(new Tickable(delay) {
				@Override
				public void execute() {
					doorsUsed[doorIndex] = bool;
					this.stop();
				}
			});
		} else {
			doorsUsed[doorIndex] = bool;
		}
	}
	
	private static void set_course_stage(Player player, int stage) {
		player.getAttributes().set("wildernesscourse", stage);
	}
	
	private static void remove_course_stage(Player player) {
		player.getAttributes().remove("wildernesscourse");
	}
	
	private static int getCourseStage(Player player) {
		return player.getAttributes().getInt("wildernesscourse");
	}
}
