package com.hyperion.game.content.skills.agility.obstacles;

import com.hyperion.game.content.skills.agility.AgilityMovement;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.masks.ForceMovement;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.gameobjects.GameObject;

/**
 * Stile shortcut
 *
 * @author trees
 */
public class ClimbOver extends AgilityMovement {

    //Is Object Entrance
    public static boolean isClimbableObject(Player player, GameObject object) {
        if (object.getId() == 3730 && object.getLocation().equals(Location.create(2817, 3562, 0))) {
            if (player.getLocation().getY() < object.getLocation().getY()) {
                Location target = Location.create(object.getLocation().getX(), object.getLocation().getY() + 1, object.getPlane());
                execute_force_movement(player, target, 4853, -1, 2, ForceMovement.NORTH, 30, 60, true, 5, null);
                player.getSkills().addExperience(Skills.AGILITY, 46.2);
            } else {
                player.getPacketSender().sendMessage("You cannot climb from this side.");
            }
            return true;
        } else if (object.getId() == 3748) {
            if (player.getLocation().getY() < object.getLocation().getY()) {
                Location target = Location.create(object.getLocation().getX(), object.getLocation().getY() + 1, object.getPlane());
                execute_force_movement(player, target, 4853, -1, 2, ForceMovement.NORTH, 30, 60, true, 5, null);
                player.getSkills().addExperience(Skills.AGILITY, 6.2);
            } else if (player.getLocation().getY() > object.getLocation().getY()) {
                Location target = Location.create(object.getLocation().getX(), object.getLocation().getY() - 1, object.getPlane());
                execute_force_movement(player, target, 4853, -1, 2, ForceMovement.SOUTH, 30, 60, true, 5, null);
                player.getSkills().addExperience(Skills.AGILITY, 6.2);
            } else if (player.getLocation().getX() > object.getLocation().getX()) {
                Location target = Location.create(object.getLocation().getX() - 1, object.getLocation().getY(), object.getPlane());
                execute_force_movement(player, target, 4853, -1, 2, ForceMovement.WEST, 30, 60, true, 5, null);
                player.getSkills().addExperience(Skills.AGILITY, 6.2);
            } else if (player.getLocation().getX() < object.getLocation().getX()) {
                Location target = Location.create(object.getLocation().getX() + 1, object.getLocation().getY(), object.getPlane());
                execute_force_movement(player, target, 4853, -1, 2, ForceMovement.EAST, 30, 60, true, 5, null);
                player.getSkills().addExperience(Skills.AGILITY, 6.2);
            }
            return true;
        }
        return false;
    }
}
