package com.hyperion.game.content.skills.agility.rooftops;

import com.hyperion.game.content.Ladders;
import com.hyperion.game.content.skills.agility.AgilityMovement;
import com.hyperion.game.content.skills.agility.rooftops.grace.MarkOfGrace;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.masks.ForceMovement;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.gameobjects.GameObject;


/**
 * Draynor rooftop course
 *
 * @author trees
 */
public class AlKharidRooftop extends AgilityMovement {

    // Start the roof attributes and add prize
    public void start(Player player, GameObject object) {
        if (player.getSkills().getLevel(Skills.AGILITY) < 20) {
            player.sendMessage("You need a level of 10 agility to use this course.");
            return;
        }

        new MarkOfGrace(player).roll();

        player.getAttributes().set("alharid_rooftop", this);
        player.getAttributes().set("alharid_rooftop_stage", 1);
        player.getAttributes().set("stopActions", true);
        World.getWorld().submit(new Tickable(1) {
            @Override
            public void execute() {
                this.stop();
                AgilityMovement.climb(player, false, 3273, 3192, 3);
                player.getAttributes().remove("stopActions");
                player.getSkills().addExperience(Skills.AGILITY, 21);
            }
        });
    }

    // Handle the object
    public void handleObjectAction(Player player, GameObject object) {
        int stage = player.getAttributes().get("alharid_rooftop_stage") == null ? -1 : player.getAttributes().get("alharid_rooftop_stage");
        switch (object.getId()) {
            case 10284:
                if (stage == 1) {
                    walkRope(player, object, Location.create(3272, 3172, 3), 8);
                    player.getAttributes().set("alharid_rooftop_stage", 2);
                }
                break;
            case 10355:
                if (stage == 2) {
                    if (player.getLocation().getY() != 3166) {
                        player.getPacketSender().sendMessage("You'll need to get closer to make this jump.");
                    } else {
                        player.getPacketSender().sendMessage("You skillfully swing across.");
                        execute_force_movement(player, Location.create(3284, 3166, 3), 751, -1, 2, ForceMovement.EAST, 30, 60, true, 27, null);
                        World.sendObjectAnimation(player, object, 497);
                    }
                    player.getAttributes().set("alharid_rooftop_stage", 3);
                }
                break;
            case 10356:
                if (stage == 3) {
                    player.animate(2586);
                    player.teleport(Location.create(3303, 3163, 1));
                    teethGrip(player, object, Location.create(3315, 3163, 1), 12);
                    player.getAttributes().set("alharid_rooftop_stage", 4);
                }
                break;
            case 10357:
                if (stage == 4) {
                    //player.teleport(Location.create(3303, 3163, 1));
                    execute_force_movement(player, Location.create(3318, 3169, 1), 2586, -1, 3, ForceMovement.NORTH, 50, 80, true, 27, null);
                    player.animate(1122);
                    execute_force_movement(player, Location.create(3317, 3175, 2), 1122, -1, 3, ForceMovement.NORTH, 50, 80, true, 27, null);
                    player.getAttributes().set("alharid_rooftop_stage", 5);
                }
                break;
            case 10094:
                if (stage == 5) {
                    Ladders.executeLadder(player, false, 3316, 3180, 3);
                    player.getAttributes().set("alharid_rooftop_stage", 6);
                    player.getSkills().addExperience(Skills.AGILITY, 7);
                }
                break;
            case 10583:
                if (stage == 6) {
                    walkRope(player, object, Location.create(3302, 3186, 3), 10);
                    player.getAttributes().set("alharid_rooftop_stage", 7);
                    player.getSkills().addExperience(Skills.AGILITY, 19);
                }
                break;
            case 10352:
                if (stage == 7) {
                    player.animate(2586);
                    player.teleport(Location.create(3299, 3195, 0));
                    player.getAttributes().set("alharid_rooftop_stage", 7);
                    player.getSkills().addExperience(Skills.AGILITY, 25);
                    player.getAttributes().remove("alharid_rooftop_stage");
                    player.getAttributes().remove("alharid_rooftop");
                }
                break;
        }
    }

    //Is Object Entrance
    public static boolean isAlkharidRooftopObject(GameObject object) {
        if (object.getId() == 10093 && object.getLocation().equals(new Location(3273, 3195, 0))) {
            return true;
        } else if (object.getId() == 10284 && object.getLocation().equals(new Location(3272, 3181, 3))) {
            return true;
        } else if (object.getId() == 10355 && object.getLocation().equals(new Location(3269, 3166, 3))) {
            return true;
        } else if (object.getId() == 10356 && object.getLocation().equals(new Location(3302, 3163, 3))) {
            return true;
        } else if (object.getId() == 10357) {
            return true;
        } else if (object.getId() == 10094 && object.getLocation().equals(new Location(3316, 3179, 2))) {
            return true;
        } else if (object.getId() == 10583 && object.getLocation().equals(new Location(3313, 3186, 3))) {
            return true;
        } else if (object.getId() == 10352 && object.getLocation().equals(new Location(3300, 3193, 3))) {
            return true;
        }
        return false;
    }
}