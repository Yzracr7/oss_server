package com.hyperion.game.content.skills.agility.rooftops;

import com.hyperion.game.content.skills.agility.AgilityMovement;
import com.hyperion.game.content.skills.agility.rooftops.grace.MarkOfGrace;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.gameobjects.GameObject;

/**
 * Draynor rooftop course
 *
 * @author trees
 */
public class DraynorRooftop extends AgilityMovement {

    // Start the roof attributes and add prize
    public void start(Player player, GameObject object) {
        if (player.getSkills().getLevel(Skills.AGILITY) < 10) {
            player.sendMessage("You need a level of 10 agility to use this course.");
            return;
        }

        new MarkOfGrace(player).roll();

        player.getAttributes().set("draynor_rooftop", this);
        player.getAttributes().set("draynor_rooftop_stage", 1);
        player.getAttributes().set("stopActions", true);
        World.getWorld().submit(new Tickable(1) {
            @Override
            public void execute() {
                this.stop();
                AgilityMovement.climb(player, false, 3102, 3279, 3);
                player.getAttributes().remove("stopActions");
                player.getSkills().addExperience(Skills.AGILITY, 15);
            }
        });
    }

    // Handle the object
    public void handleObjectAction(Player player, GameObject object) {
        int stage = player.getAttributes().get("draynor_rooftop_stage") == null ? -1 : player.getAttributes().get("draynor_rooftop_stage");
        switch (object.getId()) {
            case 10074:
                if (stage == 1) {
                    player.getAttributes().set("draynor_rooftop_stage", 2);
                    walkRope(player, object, Location.create(3090, 3277, 3), 5);
                }
                break;
            case 10075:
                if (stage == 2) {
                    player.getAttributes().set("draynor_rooftop_stage", 3);
                    walkRope(player, object, Location.create(3092, 3267, 3), 5);
                }
                break;
            case 10077:
                if (stage == 3) {
                    player.getAttributes().set("draynor_rooftop_stage", 4);
                    player.animate(753);
                    execute_force_walking(player, 756, object.getLocation().getX(), 3261, 1, 4, true, 17, null);
                    execute_teleport(player, -1, Location.create(3088, 3261, 3), 2, 2, -1, null);
                }
                break;
            case 10084:
                if (stage == 4) {
                    player.getAttributes().set("draynor_rooftop_stage", 5);
                    AgilityMovement.climb(player, false, 3088, 3255, 3);
                    player.getSkills().addExperience(Skills.AGILITY, 5);
                }
                break;
            case 10085:
                if (stage == 5) {
                    player.getAttributes().set("draynor_rooftop_stage", 6);
                    AgilityMovement.jump(player, 3096, 3256, 3);
                    player.getSkills().addExperience(Skills.AGILITY, 5);
                }
                break;
            case 10086:
                if (stage == 6) {
                    player.animate(2586);
                    player.teleport(Location.create(3102, 3261, 1));
                    player.animate(2588);
                    World.getWorld().submit(new Tickable(player, 1) {
                        @Override
                        public void execute() {
                            if (player.getLocation().equals(Location.create(3102, 3261, 1))) {
                                this.stop();
                                //player.animate(2586);
                                player.teleport(Location.create(3103, 3261, 0));
                                player.getAttributes().remove("stopActions");
                            }
                        }
                    });
                    player.getSkills().addExperience(Skills.AGILITY, 50);
                    player.getAttributes().remove("draynor_rooftop");
                    player.getAttributes().remove("draynor_rooftop_stage");
                }
                break;
        }
    }

    //Is Object Entrance
    public static boolean isDraynorRooftopObject(GameObject object) {
        if (object.getId() == 10073 && object.getLocation().equals(new Location(3103, 3279, 0))) {
            return true;
        } else if (object.getId() == 10074 && object.getLocation().equals(new Location(3098, 3277, 3))) {
            return true;
        } else if (object.getId() == 10075 && object.getLocation().equals(new Location(3092, 3276, 3))) {
            return true;
        } else if (object.getId() == 10077 && object.getLocation().equals(new Location(3089, 3264, 3))) {
            return true;
        } else if (object.getId() == 10084 && object.getLocation().equals(new Location(3088, 3256, 3))) {
            return true;
        } else if (object.getId() == 10085 && object.getLocation().equals(new Location(3095, 3255, 3))) {
            return true;
        } else if (object.getId() == 10086 && object.getLocation().equals(new Location(3102, 3261, 3))) {
            return true;
        }
        return false;
    }

}
