package com.hyperion.game.content.skills.agility.rooftops.grace;

import com.hyperion.game.world.Location;

public enum MarkLocations {
    DRAYNOR(new Location[]{Location.create(3094, 3267, 3), Location.create(3098, 3257, 3)}), AL_KHARID(new Location[]{Location.create(3277, 3186, 3), Location.create(3297, 3186, 3), Location.create(3304, 3189, 3)});

    Location[] locs;

    MarkLocations(Location[] loc) {
        this.locs = loc;
    }

    public static MarkLocations getMarkForLocation(Location loc) {
        if (loc.isInArea(3065, 3233, 3122, 3288)) {
            return DRAYNOR;
        } else if (loc.isInArea(3260, 3150, 3325, 3200)) {
            return AL_KHARID;
        }
        return null;
    }
}
