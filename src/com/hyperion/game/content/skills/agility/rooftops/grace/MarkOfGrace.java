package com.hyperion.game.content.skills.agility.rooftops.grace;

import com.hyperion.game.item.Item;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.grounditems.GroundItem;
import com.hyperion.game.world.grounditems.GroundItemManager;
import com.hyperion.utility.NumberUtils;

/**
 * Rooftop agility reward
 *
 * @author trees
 */
public class MarkOfGrace {

    private Item MARK_OF_GRACE = new Item(11849, 1);
    private Player owner;

    public MarkOfGrace(Player player) {
        this.owner = player;
    }

    public void roll() {
        if (NumberUtils.random(50) > 37) {
            MarkLocations mark = MarkLocations.getMarkForLocation(owner.getLocation());
            if (mark != null) {
                Location loc = mark.locs[mark.locs.length - 1];
                if (loc != null) {
                    GroundItemManager.create(new GroundItem(MARK_OF_GRACE, loc, owner), owner);
                }
            }
        }
    }
}
