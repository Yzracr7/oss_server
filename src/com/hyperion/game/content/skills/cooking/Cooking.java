package com.hyperion.game.content.skills.cooking;

import java.security.SecureRandom;

import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.content.skills.SkillItem;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.utility.NumberUtils;
/**
 * @author Nando
 */
public class Cooking extends CookingData {
	
	public static boolean isCooking(final Player player, int item, GameObject object) {
		if (object.getName().endsWith("oven") || object.getName().equalsIgnoreCase("range") || object.getName().equalsIgnoreCase("stove")
				|| object.getName().equalsIgnoreCase("fire") || object.getId() == 114) {
			for (int i = 0; i < MEAT_RAW.length; i++) {
				if (item == MEAT_RAW[i]) {
					player.face(object.getLocation());
					if(object.getName().equalsIgnoreCase("fire")) {
						player.getAttributes().set("cookingOnFire", true);
					}
					setCookingItem(player, null);
					displayCookOption(player, i);
					return true;
				}
			}
		}
		return false;
	}
	
	public static void cookItem(final Player player, int amount, boolean newCook) {
		SkillItem item = null;
		if (newCook) {
			if (!player.getAttributes().isSet("meatItem")) {
				return;
			}
			int i = (Integer) player.getAttributes().get("meatItem");
			item = new SkillItem(MEAT_COOKED[i], MEAT_RAW[i], MEAT_BURNT[i], MEAT_LEVEL[i], COOKING, MEAT_XP[i], amount);
			setCookingItem(player, item);
		}
		item = (SkillItem) getCookingItem(player);
		if (item == null || player == null || !player.getInventory().hasItem(item.getItemOne())) {
			return;
		}
		if (player.getSkills().getLevel(COOKING) < item.getLevel()) {
			player.getInterfaceSettings().closeInterfaces(false);
			player.getPacketSender().sendMessage("You need a Cooking level of " + item.getLevel() + " to cook that.");
			return;
		}
		boolean burn = getFormula(player, item);
		int newFood = burn ? item.getItemTwo() : item.getFinishedItem();
		if(newFood == 3146)
			newFood = NumberUtils.random(0, 50) > 50 ? 3148: newFood;
			
		String message = burn ? "burn" : "successfully cook";
		double xp = burn ? 0 : item.getXp();
		player.getInterfaceSettings().closeInterfaces(false);
		if (player.getInventory().replaceItem(item.getItemOne(), newFood)) {
			player.getInventory().refresh();
			player.animate(player.getAttributes().isSet("cookingOnFire") ? 897 : 883);//fire ? 897 : 883
			player.getSkills().addExperience(COOKING, xp);
			Achievements.increase(player, 20);
			player.getPacketSender().sendMessage("You " + message + " the " + ItemDefinition.forId(item.getFinishedItem()).getName().toLowerCase() + ".");
			
			if(newFood == 3146)
				player.getPacketSender().sendMessage("The Karambwan appears to be poisonous!");
			else if (newFood == 385)
				Achievements.increase(player, 39); //Shark
			else if (newFood == 3144)
				Achievements.increase(player, 40); //Karambwan
			item.decreaseAmount();
		}
		if (item.getAmount() >= 1) {
			World.getWorld().submit(new Tickable(player, 3) {
				@Override
				public void execute() {
					cookItem(player, -1, false);
					this.stop();
				}
			});
		}
	}
	
	private static double getBurnStop(int id) {
		String name = ItemDefinition.forId(id).getName().toLowerCase();
		switch(name) {
		case "shrimp":
			return 33;
		case "sardine":
		case "anchovy":
			return 34;
		case "herring":
			return 40;
		case "mackarel":
			return 45;
		case "trout":
			return 50;
		case "cod":
		case "pike":
			return 53;
		case "salmon":
			return 58;
		case "slimy eel":
		case "slimy_eel":
			return 60;
		case "tuna":
			return 65;
		case "cave_eel":
		case "cave eel":
			return 70;
		case "lobster":
			return 74;
		case "bass":
			return 76;
		case "swordfish":
			return 86;
		case "lava eel":
		case "lava_eel":
			return 80;
		case "monkfish":
			 return 92;
		case "shark":
			return 94;
		case "rocktail":
			return 96;
		case "manta ray":
		case "manta_ray":
			return 110;
		}
		return -1;
	}
	
	private static boolean getFormula(Player p, SkillItem item) {

		SecureRandom cookingRandom = new SecureRandom();
		double burn_chance = (55.0);
		double cook_level = (double)p.getSkills().getLevel(COOKING);
		double lev_needed = (double)item.getLevel();
		double burn_stop = getBurnStop(item.getFinishedItem()) - (wearingCookingGauntlets(p) ? 2 : 0);
		double multi_a = (burn_stop - lev_needed);
		double burn_dec = (burn_chance / multi_a);
		double multi_b = (cook_level - lev_needed);
		burn_chance -= (multi_b * burn_dec);
		double randNum = cookingRandom.nextDouble() * 100.0;
		
		if(burn_stop == -1) {
			//Old formula, used for meat and such
			int foodLevel = item.getLevel();
			int cookLevel = p.getSkills().getLevel(COOKING);
			if (!wearingCookingGauntlets(p)) {
				return NumberUtils.random(cookLevel) <= NumberUtils.random((int) (foodLevel / 1.5));
			}
			return NumberUtils.random(cookLevel) <= NumberUtils.random(foodLevel / 3);
		} else if(burn_chance > randNum) {
			//burn
			return true;
		}
		return false;
		
		
	}
	
	private static boolean wearingCookingGauntlets(Player p) {
		return p.getEquipment().getItemInSlot(9) == COOKING_GAUNTLETS;
	}

	private static void displayCookOption(Player p, int index) {
		String s = "<br><br><br><br>";
		p.getPacketSender().sendChatboxInterface(307);
		p.getPacketSender().itemOnInterface(307, 2, 150, MEAT_COOKED[index]);
		p.getPacketSender().modifyText(s + ItemDefinition.forId(MEAT_COOKED[index]).getName(), 307, 6);
		p.getAttributes().set("meatItem", index);	
	}
	
	public static void setCookingItem(Player p, Object a) {
		if (a == null) {
			resetAllCookingVariables(p);
			p.getAttributes().remove("cookingItem");
			p.getAttributes().remove("cookingOnFire");
			return;
		}
		p.getAttributes().set("cookingItem", (Object) a);
	}
	
	private static void resetAllCookingVariables(Player p) {
		p.getAttributes().remove("meatItem");
		p.getAttributes().remove("cookingOnFire");
	}
	
	private static Object getCookingItem(Player p) {
		return (Object) p.getAttributes().get("cookingItem");
	}
}