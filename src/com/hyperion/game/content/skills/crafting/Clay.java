package com.hyperion.game.content.skills.crafting;

import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;

public class Clay extends CraftingData {

	public Clay() {
		
	}
	
	public static void displayClayOptions(final Player p, int craftType) {
		final String s = "<br><br><br><br>";
		int j = 2;
		int k = 10;
		for (int i = 0; i < CLAY_ITEMS.length; i++) {
			p.getPacketSender().itemOnInterface(306, j, 130, (Integer) CLAY_ITEMS[i][0]);
			p.getPacketSender().modifyText(s + CLAY_ITEMS[i][5], 306, k);
			j++;
			k += 4;
		}
		p.getPacketSender().sendChatboxInterface(306);
	}
	
	private static void craftClay(final Player p, int amount, int craftType, int craftItem, boolean newCraft) {
		if (newCraft) {
			if ((craftType != 1 && craftType != 2) || craftItem < 0 || craftItem > 4) {
				return;
			}
			int index = craftItem;
			int endItem = craftType == 1 ? 0 : 1;
			int xp = craftType == 1 ? 3 : 4;
			p.getAttributes().set("craftItem", new CraftItem(craftType, craftItem, amount, (Double) CLAY_ITEMS[index][xp], (Integer) CLAY_ITEMS[index][endItem], (String) CLAY_ITEMS[index][5], (Integer) CLAY_ITEMS[index][2]));
		}
		CraftItem item = (CraftItem) p.getAttributes().get("craftItem");
		if (item == null || p == null || item.getAmount() <= 0) {
			Crafting.resetCrafting(p);
			return;
		}
		int neededItem = item.getCraftType() == 1 ? CLAY : (Integer) CLAY_ITEMS[item.getCraftItem()][0];
		String s = item.getCraftType() == 1 ? "You mould the clay into a " + item.getMessage() : "You bake the " + item.getMessage() + " in the oven"; 
		String s1 = item.getCraftType() == 1 ? "You need some soft clay to mould a " + item.getMessage() : "You need a pre-made " + item.getMessage() + " to put in the oven";
		int animation = item.getCraftType() == 1 ? 883 : 899;
		if (p.getSkills().getLevel(CRAFTING) < item.getLevel()) {
			p.getPacketSender().sendMessage("You need a Crafting level of " + item.getLevel() + " to make a " + item.getMessage() + ".");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItem(neededItem)) {
			p.getPacketSender().sendMessage(s1 + ".");
			Crafting.resetCrafting(p);
			return;			
		}
		p.getInterfaceSettings().closeInterfaces(false);
		if (p.getInventory().deleteItem(neededItem)) {
			if (p.getInventory().addItem(new Item(item.getFinishedItem(), 1))) {
				p.getInventory().refresh();
				p.getSkills().addExperience(CRAFTING, item.getXp());
				p.getPacketSender().sendMessage(s + ".");
				p.animate(animation);
			}
		}
		item.decreaseAmount();
		if (item.getAmount() >= 1) {
			World.getWorld().submit(new Tickable(p, 3) {
				@Override
				public void execute() {
					craftClay(p, -1, -1, -1, false);
					this.stop();
				}
			});
		}
	}
}
