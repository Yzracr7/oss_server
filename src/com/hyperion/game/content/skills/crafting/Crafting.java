package com.hyperion.game.content.skills.crafting;

import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;

public class Crafting extends CraftingData {

    public Crafting() {

    }

    public static boolean wantsToCraft(Player player, int itemUsed, int usedWith) {
        int itemOne = itemUsed;
        int itemTwo = usedWith;
        for (int i = 0; i < 2; i++) {
            if (i == 1) {
                itemOne = usedWith;
                itemTwo = itemUsed;
            }
            if (itemOne == MOLTEN_GLASS && itemTwo == GLASSBLOWING_PIPE) {
                resetCrafting(player);
                Glass.displayGlassOption(player);
                return true;
            }
            for (int j = 0; j < TANNED_HIDE.length; j++) {
                if (itemOne == TANNED_HIDE[j] && itemTwo == NEEDLE) {
                    resetCrafting(player);
                    Leather.openLeatherInterface(player, j);
                    return true;
                }
            }
            for (int j = 0; j < GEMS.length; j++) {
                if (itemOne == CHISEL && itemTwo == (Integer) GEMS[j][0]) {
                    resetCrafting(player);
                    Jewellery.cutGem(player, j + 50, 1, true);
                    return true;
                }
            }
            for (int j = 0; j < AMULETS.length; j++) {
                if (itemOne == BALL_OF_WOOL && itemTwo == (Integer) AMULETS[j][0]) {
                    resetCrafting(player);
                    Jewellery.showStringAmulet(player, j);
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean wantsToCraftOnObject(Player player, int item, GameObject object) {
        if (object.getId() == CLAY_TABLE || object.getId() == 14887) {
            if (item == CLAY) {
                resetCrafting(player);
                Clay.displayClayOptions(player, 1);
                player.getAttributes().set("craftType", 1);
                return true;
            } else {
                for (int j = 0; j < CLAY_ITEMS.length; j++) {
                    if (item == (Integer) CLAY_ITEMS[j][0]) {
                        player.getPacketSender().sendMessage("This item must now be baked in a clay oven.");
                        return true;
                    }
                }
            }
        } else if (object.getId() == CLAY_OVEN) {
            if (item == -1) {
                player.getPacketSender().sendChatboxDialogue(true, "Use an item on the oven.");
            }
            if (item == CLAY) {
                player.sendMessage("This clay must be moulded into an item first.");
                return true;
            }
            for (int j = 0; j < CLAY_ITEMS.length; j++) {
                if (item == (Integer) CLAY_ITEMS[j][0]) {
                    resetCrafting(player);
                    Clay.displayClayOptions(player, 2);
                    player.getAttributes().set("craftType", 2);
                    return true;
                }
            }
        } else if (object.getId() == 2643) { // Furnace
            if (item == GOLD_BAR) {
                Jewellery.displayJewelleryInterface(player);
                return true;
            } else if (item == SILVER_BAR) {
                Silver.displaySilverOptions(player);
                return true;
            }
        }
        return false;
    }

    public static void resetCrafting(Player player) {
        player.getAttributes().remove("craftType");
        player.getAttributes().remove("craftItem");
        player.getAttributes().remove("leatherType");
        player.getAttributes().remove("leatherCraft");
    }
}
