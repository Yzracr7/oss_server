package com.hyperion.game.content.skills.crafting;

public class CraftingData {

	public CraftingData() {
		
	}
	
	protected static final int CRAFTING = 12;
	protected static final int CLAY = 1761;
	protected static final int CLAY_TABLE = 2642;
	protected static final int CLAY_OVEN = 11601;
	
	protected static final Object[][] CLAY_ITEMS = {
		// unfired, fired(finished) level, spin xp, fire xp, message/name
		{1787, 1931, 1, 6.3, 6.3, "Pot"},
		{1789, 2313, 7, 15.0, 10.0, "Pie dish"},
		{1791, 1923, 8, 18.0, 15.0, "Bowl"},
		{5352, 5350, 19, 20.0, 17.5, "Plant pot"},
		{4438, 4440, 25, 20.0, 20.0, "Pot lid"},
	};
	
	protected static final int MOLTEN_GLASS = 1775;
	protected static final int GLASSBLOWING_PIPE = 1785;
	
	protected static final Object[][] GLASS_ITEMS = {
		// finished item, level, xp, name
		{1919, 1, 17.5, "Beer glass"},
		{4529, 4, 19.0, "Candle lantern"},
		{4522, 12, 25.0, "Oil lamp"},
		{4537, 26, 50.0, "Oil lantern"},
		{229, 33, 35.0, "Vial"},
		{6667, 42, 42.5, "Fishbowl"},
		{567, 46, 52.5, "Orb"},
		{4542, 49, 55.0, "Lantern lens"},
		{10973, 87, 70, "Dorgeshuun light orb"}
	};
	
	protected static final int NEEDLE = 1733;
	protected static final int THREAD = 1734;
	protected static final int COWHIDE = 1739;
	
	protected static final int[] UNTANNED_HIDE = {
		1753, 1751, 1749, 1751, 1739
	};
	
	protected static final int[] TANNED_HIDE = {
		1745, 2505, 2507, 2509, 1741
	};
	
	protected static final Object[][] NORMAL_LEATHER = {
		// finished item, level, xp, name
		{1129, 14, 25.0, "Leather body"},
		{1059, 1, 13.8, "Leather gloves"},
		{1061, 7, 16.3, "Leather boots"},
		{1063, 11, 22.0, "Leather vambraces"},
		{1095, 18, 27.0, "Leather chaps"},
		{1169, 38, 37.0, "Coif"},
		{1167, 9, 18.5, "Leather cowl"},
	};

	
	protected static final Object[][]LEATHER_ITEMS = {
		//finished item, level, xp, # of hides, name
		{1135, 63, 186.0, 3, "Green body"},
		{2499, 71, 210.0, 3, "Blue body"},
		{2501, 77, 234.0, 3, "Red body"},
		{2503, 84, 258.0, 3, "Black body"},
		
		{1065, 57, 62.0, 1, "Green vambraces"},
		{2487, 66, 70.0, 1, "Blue vambraces"},
		{2489, 73, 78.0, 1, "Red vambraces"},
		{2491, 79, 86.0, 1, "Black vampbraces"},
		
		{1099, 60, 124.0, 2, "Green chaps"},
		{2493, 68, 140.0, 2, "Blue chaps"},
		{2495, 75, 156.0, 2, "Red chaps"},
		{2497, 82, 172.0, 2, "Black chaps"},
	};
	
	protected static final int CHISEL = 1755;
	
	protected static final Object[][] GEMS = {
		// uncut, cut, level, xp, name, cut emote
		{1625, 1609, 1, 15, "Opal", 886},
		{1627, 1611, 13, 20.0, "Jade", 886},
		{1629, 1613, 16, 25.0, "Red topaz", 887},
		{1623, 1607, 20, 50.0, "Sapphire", 888},
		{1621, 1605, 27, 67.5, "Emerald", 889},
		{1619, 1603, 34, 85.0, "Ruby", 887},
		{1617, 1601, 43, 107.5, "Diamond", 886},
		{1631, 1615, 55, 137.5, "Dragonstone", 885},
		{6571, 6573, 67, 167.5, "Onyx", 2717},
	};
	
	protected static final int[] CUT_GEMS = {
		1607, 1605, 1603, 1601, 1615, 6573
	};
	
	protected static final int NULL_RING = 1647;
	protected static final int GOLD_BAR = 2357;
	
	protected static final int[] NULL_JEWELLERY = {
		1647, 1666, 1685, 11067
	};
	
	protected static final int[][] JEWELLERY_INTERFACE_VARS = {
		// mould id, id to remove the text, index the images speak at
		{1592, 63, 66},
		{1597, 110, 113},
		{1595, 157, 160},
		{11065, 207, 210},
	};
	
	protected static final Object[][] RINGS = {
		{1635, 5, 15.0, "ring"},
		{1637, 30, 40.0, "ring"},
		{1639, 27, 55.0, "ring"},
		{1641, 34, 70.0, "ring"},
		{1643, 43, 85.0, "ring"},
		{1645, 55, 100.0, "ring"},
		{6575, 67, 115.0, "ring"},
	};
	
	protected static final Object[][] NECKLACES = {
		{1654, 6, 20.0, "necklace"},
		{1656, 22, 55.0, "necklace"},
		{1658, 29, 60.0, "necklace"},
		{1660, 40, 75.0, "necklace"},
		{1662, 56, 90.0, "necklace"},
		{1664, 72, 105.0, "necklace"},
		{6577, 82, 120.0, "necklace"},
	};
	
	protected static final Object[][] BRACELETS = {
		{11069, 7, 25.0, "bracelet"},
		{11072, 23, 60.0, "bracelet"},
		{11074, 30, 65.0, "bracelet"},
		{11076, 42, 80.0, "bracelet"},
		{11078, 58, 95.0, "bracelet"},
		{11080, 74, 110.0, "bracelet"},
		{11130, 84, 125.0, "bracelet"},
	};
	
	protected static final Object[][] AMULETS = {
		//finished id, level, xp, message
		{1673, 8, 30.0, "amulet"},
		{1675, 24, 65.0, "amulet"},
		{1677, 31, 70.0, "amulet"},
		{1679, 50, 85.0, "amulet"},
		{1681, 70, 100.0, "amulet"},
		{1683, 80, 150.0, "amulet"},
		{6579, 90, 165.0, "amulet"},
	};
	
	protected static final int STRINGING_XP = 4;
	
	protected static final int[][] STRUNG_AMULETS = {
		//finished id, not finished id
		{1692, 1673},
		{1694, 1675},
		{1696, 1677},
		{1698, 1679},
		{1700, 1681},
		{1702, 1683},
		{6581, 6579},
	};
	
	protected static final int SPINNING_WHEEL = 6;
	
	protected static final String[] SPIN_FINISH = {
		"Ball of Wool", "Bow String", "Crossbow String"
	};
	
	protected static final Object[][] SPINNING_ITEMS = {
		//finished id, needed id, level, xp, message
		{1759, 1737, 1, 2.5, "Wool"},
		{1777, 1779, 10, 15.0, "Flax"},
		{9438, 9436, 10, 15.0, "Sinew"}
	};
	
	protected static final int BALL_OF_WOOL = 1759;
	
	protected static final int SILVER_BAR = 2355;
	
	protected static final Object[][] SILVER_ITEMS = {
		// unfinished, mould Id, level, xp, message
		{1714, 1599, 16, 50.0, "Unholy symbol"},
		{5525, 5523, 23, 52.5, "Tiara"},
	};
	
	protected static int getIndexForButton(int button) {
		switch (button) {
		/*
		 * Leather
		 */
		case 125://armour
		case 124:
		case 115:
			return 0;
		case 127://gloves
		case 126:
		case 116:
			return 1;
		case 128://boots
		case 129://boots
		case 117://boots
			return 2;
		case 131://vambs
		case 130:
		case 118:
			return 3;
		case 133://chaps
		case 132:
		case 119:
			return 4;
		case 135://coif
		case 134:
		case 120:
			return 5;
		case 137://cowl
		case 136:
		case 121:
			return 6;
			/*
			 * End of leather
			 */
		}
		return -1;
	}
	
	protected static int getAmountForButton(int button) {
		switch (button) {
		case 125://leather body 1
		case 127://leather gloves 1
		case 129://leather boots 1
		case 131://leather vambs 1
		case 133://leather chaps 1
		case 135://coif 1
		case 137://cowl 1
			return 1;
		case 124://leather body 5
		case 126://leather gloves 5
		case 128://leather boots 5
		case 130://leather vambs 5
		case 132://leather chaps 5
		case 134://coif 5
		case 136://cowl 5
			return 5;
		case 115://leather body x
		case 116://leather gloves x
		case 117://leather boots x
		case 118://leather vambs x
		case 119://leather chaps x
		case 120://coif x
		case 121://cowl x
			return -1;
		}
		return 0;
	}
}
