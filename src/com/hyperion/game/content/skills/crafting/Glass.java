package com.hyperion.game.content.skills.crafting;

import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;

public class Glass extends CraftingData {

	public Glass() {
		
	}
	
	public static void displayGlassOption(Player p) {
		p.getPacketSender().displayInterface(542);
	}

	public static void craftGlass(final Player p, int amount, int index, boolean newCraft) {
		index++;
		if (newCraft) {
			p.getAttributes().set("craftItem", new CraftItem(3, index, amount, (Double) GLASS_ITEMS[index][2], (Integer) GLASS_ITEMS[index][0], (String) GLASS_ITEMS[index][3], (Integer) GLASS_ITEMS[index][1]));
		}
		CraftItem item = (CraftItem) p.getAttributes().get("craftItem");
		if (item == null || p == null || item.getAmount() <= 0 || item.getCraftType() != 3) {
			Crafting.resetCrafting(p);
			return;
		}
		p.getInterfaceSettings().closeInterfaces(false);
		if (!p.getInventory().hasItem(MOLTEN_GLASS)) {
			p.getPacketSender().sendMessage("You have no molten glass.");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItem(GLASSBLOWING_PIPE)) {
			p.getPacketSender().sendMessage("You need a glassblowing pipe if you wish to make a glass item.");
			Crafting.resetCrafting(p);
			return;
		}
		if (p.getSkills().getLevel(CRAFTING) < item.getLevel()) {
			p.getPacketSender().sendMessage("You need a Crafting level of " + item.getLevel() + " to craft that item.");
			Crafting.resetCrafting(p);
			return;
		}
		if (p.getInventory().deleteItem(MOLTEN_GLASS)) {
			p.getInventory().addItem(new Item(item.getFinishedItem(), 1));
			p.getSkills().addExperience(CRAFTING, item.getXp());
			p.animate(884);
			p.getInventory().refresh();
			//p.getPackets().sendMessage("You blow through the pipe, shaping the molten glass into a " + item.getMessage() + ".");
		}
		item.decreaseAmount();
		if (item.getAmount() >= 1) {
			World.getWorld().submit(new Tickable(p, 3) {
				@Override
				public void execute() {
					craftGlass(p, -1, -1, false);
					this.stop();
				}
			});
		}
	}
}
