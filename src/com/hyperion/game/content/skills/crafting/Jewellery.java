package com.hyperion.game.content.skills.crafting;

import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;

public class Jewellery extends CraftingData {

	public Jewellery() {
		
	}
	
	public static void showCutGemOption(Player p, int index) {
		int amount = p.getInventory().getItemAmount((Integer)GEMS[index][0]);
		if (amount > 1) {
			String s = "<br><br><br><br>";
			p.getPacketSender().itemOnInterface(309, 2, 190, (Integer) GEMS[index][1]);
			p.getPacketSender().modifyText(s + (String) GEMS[index][4], 309, 6);
			p.getPacketSender().sendChatboxInterface(309);
			p.getAttributes().set("craftType", index + 50);
			return;
		}
		cutGem(p, index + 50, 1, true);
	}

	public static void cutGem(final Player p, int index, int amount, boolean newCut) {
		index -= 50;
		if (newCut) {
			p.getAttributes().set("craftItem", new CraftItem(5, index, amount, (Double) GEMS[index][3], (Integer) GEMS[index][1], (String) GEMS[index][4], (Integer) GEMS[index][2]));
		}
		CraftItem item = (CraftItem) p.getAttributes().get("craftItem");
		if (item == null || p == null || item.getAmount() <= 0 || item.getCraftType() != 5) {
			Crafting.resetCrafting(p);
			return;
		}
		p.getInterfaceSettings().closeInterfaces(false);
		if (!p.getInventory().hasItem(CHISEL)) {
			p.getPacketSender().sendMessage("You cannot cut gems without a chisel.");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItem((Integer)GEMS[item.getCraftItem()][0])) {
			if (newCut) {
				p.getPacketSender().sendMessage("You have no " + item.getMessage() + " to cut.");
			} else {
				p.getPacketSender().sendMessage("You have no more " + item.getMessage() + "'s to cut.");
			}
			Crafting.resetCrafting(p);
			return;
		}
		if (p.getSkills().getLevel(CRAFTING) < item.getLevel()) {
			p.getPacketSender().sendMessage("You need a Crafting level of " + item.getLevel() + " to cut that gem.");
			Crafting.resetCrafting(p);
			return;
		}
		if (p.getInventory().deleteItem((Integer)GEMS[item.getCraftItem()][0])) {
			p.getInventory().addItem(new Item(item.getFinishedItem(), 1));
			p.getSkills().addExperience(CRAFTING, item.getXp());
			p.animate((Integer) GEMS[item.getCraftItem()][5]);
			p.getInventory().refresh();
			//p.getPackets().sendMessage("You cut the " + item.getMessage() + ".");
		}
		item.decreaseAmount();
		if (item.getAmount() >= 1) {
			World.getWorld().submit(new Tickable(p, 2) {
				@Override
				public void execute() {
					cutGem(p, -1, -1, false);
					this.stop();
				}
			});
		}
	}

	public static void displayJewelleryInterface(Player p) {
		for (int i = 0; i < JEWELLERY_INTERFACE_VARS.length; i++) {
			if (p.getInventory().hasItem(JEWELLERY_INTERFACE_VARS[i][0])) {
				p.getPacketSender().modifyText("", 446, JEWELLERY_INTERFACE_VARS[i][1]);
				displayJewellery(p, i);
			}
		}
		p.getPacketSender().displayInterface(446);
	}

	private static void displayJewellery(Player p, int index) {
		if (index == 3) {//Bracelets - TODO - fix wat items u get from craftin them
			return;
		}
		Object[][] items = getItemArray(p, index);
		if (items == null) {
			return;
		}
		int[] sizes = {100, 85, 65, 85};
		int SIZE = sizes[index];
		int interfaceSlot = JEWELLERY_INTERFACE_VARS[index][2];
		p.getPacketSender().itemOnInterface(446, interfaceSlot, SIZE, (Integer)items[0][0]); // display the gold item since we know we have the bar
		interfaceSlot += 6;
		if (index == 3) {
			interfaceSlot += 1;
		}
		for (int i = 1; i < items.length; i++) { // i is set to 1 to ignore the gold
			for (int j = 0; j < items[i].length; j++) {
				if (p.getInventory().hasItem((Integer) GEMS[i + 2][1]) && p.getSkills().getLevel(CRAFTING) >= (Integer)items[i][1]) { // lower it down 1 because of the gold..0 in jeweller is 0, but in GEMS is a sapphire
					p.getPacketSender().itemOnInterface(446, interfaceSlot, SIZE, (Integer)items[i][0]);
				} else {
					p.getPacketSender().itemOnInterface(446, interfaceSlot, SIZE, NULL_JEWELLERY[index]);
				}
			}
			interfaceSlot += 6;
		}
	}

	private static Object[][] getItemArray(Player p, int index) {
		switch(index) {
			case 0: return RINGS;
			case 1: return NECKLACES;
			case 2: return AMULETS;
			case 3: return BRACELETS;
		}
		return null;
	}
	
	public static void makeJewellery(final Player p, int buttonId, int amount, boolean newCraft) {
		int index = -1;
		if (newCraft) {
			int itemType = getIndex(buttonId, true);
			Object[][] items = getItemArray(p, itemType);
			index = getIndex(buttonId, false);
			amount = getAmountsForButton(buttonId);
			if (index == -1) {
				p.getInterfaceSettings().closeInterfaces(false);
				return;
			}
			p.getAttributes().set("craftItem", new CraftItem(itemType, index, amount, (Double)items[index][2], (Integer)items[index][0], (String)items[index][3], (Integer)items[index][1]));
		}
		CraftItem item = (CraftItem) p.getAttributes().get("craftItem");
		if (item == null || p == null || item.getAmount() <= 0) {
			Crafting.resetCrafting(p);
			p.getInterfaceSettings().closeInterfaces(false);
			return;
		}
		p.getInterfaceSettings().closeInterfaces(false);
		index = item.getCraftItem();
		int index2 = index;
		//String gemType = (String) GEMS[index2-1][4];
		String gemType = (String) GEMS[index2+2][4];
		String s = index == 3 ? "an" : "a";
		if (p.getSkills().getLevel(CRAFTING) < item.getLevel()) {
			p.getPacketSender().sendMessage("You need a Crafting level of " + item.getLevel() + " to craft a " + gemType + " " + item.getMessage() + ".");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItem(JEWELLERY_INTERFACE_VARS[item.getCraftType()][0])) {
			p.getPacketSender().sendMessage("You need " + s + " " + item.getMessage() + " mould to craft that.");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItem(GOLD_BAR)) {
			p.getPacketSender().sendMessage("You need a Gold bar to craft an item of jewellery.");
			Crafting.resetCrafting(p);
			return;
		}
		if (index2 > 0) { // We dont need gems for gold bars
			if (!p.getInventory().hasItem((Integer) GEMS[index2+2][1])) {
				p.getPacketSender().sendMessage("You don't have a cut " + (String) GEMS[index2+2][4] + ".");
				Crafting.resetCrafting(p);
				return;
			}
		}
		if (p.getInventory().deleteItem(GOLD_BAR)) {
			if (index2 > 0) {
				if (!p.getInventory().deleteItem((Integer) GEMS[index2+2][1])) {
					return;
				}
			}
			p.animate(3243);
			p.getInventory().addItem(new Item(item.getFinishedItem(), 1));
			p.getInventory().refresh();
			p.getSkills().addExperience(CRAFTING, item.getXp());
			//String message = index2 == 0 ? "You smelt the gold bar into a Gold " + item.getMessage() : "You fuse the Gold bar and " + (String) GEMS[index2+2][4] + " together, and create a " + (String) GEMS[index2+2][4] + " " + item.getMessage();
			//p.getPackets().sendMessage(message + ".");
		}
		item.decreaseAmount();
		if (item.getAmount() >= 1) {
			World.getWorld().submit(new Tickable(p, 3) {
				@Override
				public void execute() {
					makeJewellery(p, -1, -1, false);
					this.stop();
				}
			});
		}
	}

	private static int getIndex(int buttonId, boolean getItemType) {
		int[][] BUTTONS = {
			{71, 77, 83, 89, 95, 101, 107},
			{118, 124, 130, 136, 142, 148, 154},
			{165, 171, 177, 183, 189, 195, 201},
			{215, 221, 227, 233, 239, 245, 251},
		};
		for (int i = 0; i < BUTTONS.length; i++) {
			for (int j = 0; j < BUTTONS[i].length; j++) {
				boolean five = (BUTTONS[i][j] - 1) == buttonId;
				boolean ten = (BUTTONS[i][j] - 2) == buttonId;
				boolean x = (BUTTONS[i][j] - 3) == buttonId;
				boolean otherOption = (five || ten || x);
				if (buttonId == BUTTONS[i][j] || otherOption) {
					return getItemType ? i : j;
				}
			}			
		}
		return -1;
	}
	
	private static int getAmountsForButton(int buttonId) {
		int[] firstOptions = 
		{/*rings*/71, 77, 83, 89, 95, 101, 107,
		/*necklaces*/118, 124, 130, 136, 142, 148, 154,
		/*amulets*/165, 171, 177, 183, 189, 195, 201,};
		for (int i = 0; i < firstOptions.length; i++) {
			if (firstOptions[i] - 0 == buttonId) {
				return 1;
			} else if (firstOptions[i] - 1 == buttonId) {
				return 5;
			} else if (firstOptions[i] - 2 == buttonId) {
				return 10;
			} else if (firstOptions[i] - 3 == buttonId) {
				return 50;//will be X for now.
			}
		}
		return -1;
	}

	public static void showStringAmulet(Player p, int index) {
		//int amount = p.getInventory().getItemAmount(BALL_OF_WOOL);
		//int amount2 = p.getInventory().getItemAmount(STRUNG_AMULETS[index][1]);
		/*if (amount > 1 && amount2 > 1) {
			String s = "<br><br><br><br>";
			p.getPackets().itemOnInterface(309, 2, 120, STRUNG_AMULETS[index][0]);
			p.getPackets().modifyText(s + (String) GEMS[index - 1][4] + " Amulet", 309, 6);
			p.getPackets().sendChatboxInterface(309);
			p.setTemporaryAttribute("craftType", index + 100);
			return;
		}*/
		stringAmulet(p, index + 100, 1, true);
	}

	private static void stringAmulet(final Player p, int index, int amount, boolean newString) {
		index -= 100;
		if (newString) {
			p.getAttributes().set("craftItem", new CraftItem(4, index, amount, STRINGING_XP, STRUNG_AMULETS[index][0], (String) GEMS[index][4] + " amulet", 1));
		}
		CraftItem item = (CraftItem) p.getAttributes().get("craftItem");
		if (item == null || p == null || item.getAmount() <= 0 || item.getCraftType() != 4) {
			Crafting.resetCrafting(p);
			return;
		}
		p.getInterfaceSettings().closeInterfaces(false);
		if (!p.getInventory().hasItem(BALL_OF_WOOL)) {
			p.getPacketSender().sendMessage("You do not have a Ball of wool.");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItem(STRUNG_AMULETS[item.getCraftItem()][1])) {
			String s = item.getCraftItem() == 1 || item.getCraftItem() == 5 ? "an" : "a";
			p.getPacketSender().sendMessage("You don't have " + s + " " + GEMS[item.getCraftItem()][4] + " unstrung amulet to string.");
			Crafting.resetCrafting(p);
			return;
		}
		if (p.getInventory().deleteItem(STRUNG_AMULETS[item.getCraftItem()][1]) && p.getInventory().deleteItem(BALL_OF_WOOL)) {
			p.getInventory().addItem(new Item(item.getFinishedItem(), 1));
			p.getInventory().refresh();
			p.getSkills().addExperience(CRAFTING, STRINGING_XP);
			Achievements.increase(p, 34);
			//p.getPackets().sendMessage("You add a string to the amulet.");
		}
		item.decreaseAmount();
		if (item.getAmount() >= 1) {
			World.getWorld().submit(new Tickable(p, 2) {
				@Override
				public void execute() {
					stringAmulet(p, -1, -1, false);
					this.stop();
				}
			});
		}
	}
}
