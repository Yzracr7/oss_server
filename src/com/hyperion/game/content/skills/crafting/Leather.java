package com.hyperion.game.content.skills.crafting;

import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;

public class Leather extends CraftingData {

	public Leather() {
		
	}
	
	public static void openLeatherInterface(Player p, int type) {
		p.getAttributes().set("leatherCraft", type);
		if (type == 4) { // Cowhide
			p.getPacketSender().displayInterface(154);
			return;
		}
		int i = type;
		int k = 2;
		int l = 8;
		String s = "<br><br><br><br>";
		for (int j = 0; j < 3; j++) {
			p.getPacketSender().itemOnInterface(304, k, 180, (Integer) LEATHER_ITEMS[i][0]);
			p.getPacketSender().modifyText(s + (String) LEATHER_ITEMS[i][4], 304, l);
			l += 4;
			i += 4;
			k++;
		}
		p.getPacketSender().sendChatboxInterface(304);
	}
	
	public static void craftDragonHide(final Player p, int amount, int itemIndex, int leatherType, boolean newCraft) {
		if (newCraft) {
			itemIndex = leatherType != 0 ? itemIndex += leatherType : itemIndex;
			p.getAttributes().set("craftItem", new CraftItem(leatherType, itemIndex, amount, (Double) LEATHER_ITEMS[itemIndex][2], (Integer) LEATHER_ITEMS[itemIndex][0], (String) LEATHER_ITEMS[itemIndex][4], (Integer) LEATHER_ITEMS[itemIndex][1]));
		}
		CraftItem item = (CraftItem) p.getAttributes().get("craftItem");
		if (item == null || p == null || item.getAmount() <= 0) {
			Crafting.resetCrafting(p);
			return;
		}
		p.getInterfaceSettings().closeInterfaces(false);
		int index = item.getCraftItem();
		if (p.getSkills().getLevel(CRAFTING) < item.getLevel()) {
			p.getPacketSender().sendMessage("You need a Crafting level of " + item.getLevel() + " to craft that item.");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItemAmount(TANNED_HIDE[item.getCraftType()], (Integer) LEATHER_ITEMS[index][3])) {
			p.getPacketSender().sendMessage("You need " + (Integer) LEATHER_ITEMS[index][3] + " dragonhide to craft that.");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItem(NEEDLE)) {
			p.getPacketSender().sendMessage("You need a needle if you wish to craft leather.");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItemAmount(THREAD, (Integer)LEATHER_ITEMS[index][3])) {
			p.getPacketSender().sendMessage("You need " + (Integer)LEATHER_ITEMS[index][3] + " thread to craft that.");
			Crafting.resetCrafting(p);
			return;
		}
		//String s = index < 4 ? "a" : "a pair of";
		for (int j = 0; j < (Integer) LEATHER_ITEMS[index][3]; j++) {
			if (!p.getInventory().deleteItem(TANNED_HIDE[item.getCraftType()])) {
				return; 
			}
		}
		p.getInventory().deleteItem(THREAD, (Integer) LEATHER_ITEMS[index][3]);
		p.getInventory().addItem(new Item(item.getFinishedItem(), 1));
		p.getInventory().refresh();
		p.getSkills().addExperience(CRAFTING, item.getXp());
		p.animate(1249);
		//p.getFrames().sendMessage("You craft " + s + " " + item.getMessage() + ".");
		item.decreaseAmount();
		if (item.getAmount() >= 1) {
			World.getWorld().submit(new Tickable(p, 3) {
				@Override
				public void execute() {
					craftDragonHide(p, -1, -1, -1, false);
					this.stop();
				}
			});
		}
	}
	
	public static void init_crafting_leather(Player player, int index, int amount, boolean new_craft) {
		amount = getAmountForButton(index);
		index = getIndexForButton(index);
		if (index == -1 || amount <= 0) {
			return;
		}
		craftNormalLeather(player, index, amount, new_craft);
	}

	private static void craftNormalLeather(final Player p, int index, int amount, boolean newCraft) {
		if (newCraft) {
			p.getAttributes().set("craftItem", new CraftItem(4, index, amount, (double) NORMAL_LEATHER[index][2], (int) NORMAL_LEATHER[index][0], (String) NORMAL_LEATHER[index][3], (int) NORMAL_LEATHER[index][1]));
		}
		CraftItem item = (CraftItem) p.getAttributes().get("craftItem");
		if (item == null || p == null || item.getAmount() <= 0 || item.getCraftType() != 4 || item.getCraftItem() < 0) {
			Crafting.resetCrafting(p);
			return;
		}
		p.getInterfaceSettings().closeInterfaces(false);
		if (!p.getInventory().hasItem(TANNED_HIDE[4])) {
			p.getPacketSender().sendMessage("You have no Leather.");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItem(NEEDLE)) {
			p.getPacketSender().sendMessage("You need a needle if you wish to craft leather.");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItem(THREAD)) {
			p.getPacketSender().sendMessage("You have no thread.");
			Crafting.resetCrafting(p);
			return;
		}
		if (p.getSkills().getLevel(CRAFTING) < item.getLevel()) {
			p.getPacketSender().sendMessage("You need a Crafting level of " + item.getLevel() + " to craft that item.");
			Crafting.resetCrafting(p);
			return;
		}
		//int i = item.getCraftItem();
		//String s = i == 0 || i == 5 || i == 6 ? "a" : "a pair of";
		if (p.getInventory().deleteItem(THREAD) && p.getInventory().deleteItem(TANNED_HIDE[4])) {
			p.getInventory().addItem(new Item(item.getFinishedItem(), 1));
			p.getInventory().refresh();
			p.getSkills().addExperience(CRAFTING, item.getXp());
			p.animate(1249);
			//p.getFrames().sendMessage("You make " + s + " " + item.getMessage() + ".");
		}
		item.decreaseAmount();
		if (item.getAmount() >= 1) {
			World.getWorld().submit(new Tickable(p, 4) {
				@Override
				public void execute() {
					craftNormalLeather(p, -1, -1, false);
					this.stop();
				}
			});
		}
	}
}
