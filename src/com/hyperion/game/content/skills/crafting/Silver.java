package com.hyperion.game.content.skills.crafting;

import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;

public class Silver extends CraftingData {

	public Silver() {
		
	}

	public static void displaySilverOptions(Player p) {
		String s = "<br><br><br><br>";
		p.getPacketSender().sendChatboxInterface(303);
		p.getPacketSender().itemOnInterface(303, 2, 175, (Integer)SILVER_ITEMS[0][0]);
		p.getPacketSender().itemOnInterface(303, 3, 175, (Integer)SILVER_ITEMS[1][0]);
		p.getPacketSender().modifyText(s + (String)SILVER_ITEMS[0][4], 303, 7);
		p.getPacketSender().modifyText(s + (String)SILVER_ITEMS[1][4], 303, 11);
		p.getAttributes().set("craftType", 120);
	}
	
	private static void newSilverItem(final Player p, int amount, int index, boolean newCraft) {
		index -= 120;
		if (newCraft) {
			p.getAttributes().set("craftItem", new CraftItem(3, index, amount, (Double) SILVER_ITEMS[index][3], (Integer) SILVER_ITEMS[index][0], (String) SILVER_ITEMS[index][4], (Integer) SILVER_ITEMS[index][2]));
		}
		CraftItem item = (CraftItem) p.getAttributes().get("craftItem");
		if (item == null || p == null || item.getAmount() <= 0 || item.getCraftType() != 3) {
			Crafting.resetCrafting(p);
			return;
		}
		p.getInterfaceSettings().closeInterfaces(false);
		String s = item.getCraftItem() == 0 ? "an" : "a";
		if (!p.getInventory().hasItem((Integer)SILVER_ITEMS[item.getCraftItem()][1])) {
			p.getPacketSender().sendMessage("You need " + s + " " + item.getMessage() + " mould to make that.");
			Crafting.resetCrafting(p);
			return;
		}
		if (!p.getInventory().hasItem(SILVER_BAR)) {
			p.getPacketSender().sendMessage("You don't have a Silver bar.");
			Crafting.resetCrafting(p);
			return;
		}
		if (p.getSkills().getLevel(CRAFTING) < item.getLevel()) {
			p.getPacketSender().sendMessage("You need a Crafting level of " + item.getLevel() + " to smelt that.");
			Crafting.resetCrafting(p);
			return;
		}
		if (p.getInventory().deleteItem(SILVER_BAR)) {
			p.getInventory().addItem(new Item(item.getFinishedItem(), 1));
			p.getInventory().refresh();
			p.getSkills().addExperience(CRAFTING, item.getXp());
			p.animate(3243);
			p.getPacketSender().sendMessage("You smelt the Silver bar in to " + s + " " + item.getMessage() + ".");
		}
		item.decreaseAmount();
		if (item.getAmount() >= 1) {
			World.getWorld().submit(new Tickable(p, 3) {
				@Override
				public void execute() {
					newSilverItem(p, -1, -1, false);
					this.stop();
				}
			});
		}
	}
}
