package com.hyperion.game.content.skills.crafting;

import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;

public class Spinning extends CraftingData {

	public Spinning() {
		
	}
	
	public static void displaySpinningInterface(final Player p, GameObject object) {
		Crafting.resetCrafting(p);
		int k = 2;
		int l = 8;
		String s = "<br><br><br><br>";
		for (int j = 0; j < 3; j++) {
			p.getPacketSender().itemOnInterface(304, k, 180, (Integer) SPINNING_ITEMS[j][0]);
			p.getPacketSender().modifyText(s + (String) SPIN_FINISH[j], 304, l);
			l += 4;
			k++;
		}
		p.getAttributes().set("craftType", 6);
		p.getPacketSender().sendChatboxInterface(304);
	}
	
	public static void craftSpinning(final Player p, int amount, int index, boolean newCraft) {
		if (p == null) {
			return;
		}
		if (newCraft) {
			p.getAttributes().set("craftItem", new CraftItem(6, index, amount, (double) SPINNING_ITEMS[index][3], (Integer) SPINNING_ITEMS[index][0], (String) SPINNING_ITEMS[index][4], (Integer) SPINNING_ITEMS[index][2]));
		}
		CraftItem item = (CraftItem) p.getAttributes().get("craftItem");
		if (item == null || item.getAmount() <= 0 || item.getCraftType() != 6) {
			Crafting.resetCrafting(p);
			return;
		}
		p.getInterfaceSettings().closeInterfaces(true);
		int i = item.getCraftItem();
		if (!p.getInventory().hasItem((Integer) SPINNING_ITEMS[i][1])) {
			p.getPacketSender().sendMessage("You have no " + item.getMessage() + ".");
			Crafting.resetCrafting(p);
			return;
		}
		if (p.getSkills().getLevel(CRAFTING) < item.getLevel()) {
			p.getPacketSender().sendMessage("You need a Crafting level of " + item.getLevel() + " to spin that.");
			Crafting.resetCrafting(p);
			return;
		}
		if (p.getInventory().deleteItem((Integer) SPINNING_ITEMS[i][1])) {
			p.getInventory().addItem(new Item(item.getFinishedItem(), 1));
			p.getInventory().refresh();
			p.getSkills().addExperience(CRAFTING, item.getXp());
			p.animate(896);
			p.getPacketSender().sendMessage("You spin the " + item.getMessage().toLowerCase() + " into a " + SPIN_FINISH[i].toLowerCase() + ".");
		}
		item.decreaseAmount();
		if (item.getAmount() >= 1) {
			World.getWorld().submit(new Tickable(p, 2) {
				@Override
				public void execute() {
					craftSpinning(p, -1, -1, false);
					this.stop();
				}
			});
		}
	}
}
