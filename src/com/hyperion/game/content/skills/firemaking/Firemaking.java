package com.hyperion.game.content.skills.firemaking;

import java.util.Random;

import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.game.world.pathfinders.DumbPathFinder;
import com.hyperion.game.world.pathfinders.SizedPathFinder;
import com.hyperion.utility.NumberUtils;
import com.hyperion.utility.TextUtils;


/**
 * For the firemaking skill.
 *
 * @author Konceal
 * @author trees
 */
public class Firemaking {

    private Player player;

    /**
     * The player who is firemaking.
     *
     * @return player
     */
    public Player getPlayer() {
        return player;
    }

    public Firemaking(Player player) {
        this.player = player;
    }

    private enum Log {

        NORMAL(1511, 1, 30),
        OAK(1521, 15, 60),
        WILLOW(1519, 30, 90),
        TEAK(6333, 35, 105),
        MAPLE(1517, 45, 135),
        MAHOGANY(6332, 50, 157.5),
        YEW(1515, 60, 202.5),
        MAGIC(1513, 75, 303.8);

        private int id, reqLevel;
        private double exp;

        /**
         * The log item.
         *
         * @return new Item(id)
         */
        public Item getItem() {
            return new Item(id, 1);
        }

        /**
         * The required level to light the log.
         *
         * @return reqLevel
         */
        public int getRequiredLevel() {
            return reqLevel;
        }

        /**
         * The experience one gains from lighting the log.
         *
         * @return exp
         */
        public double getExperience() {
            return exp;
        }

        /**
         * Constructs a new log.
         *
         * @param id       The item id of the log.
         * @param reqLevel The required level to light the log.
         * @param exp      The experience one gains from lighting the log.
         */
        Log(int id, int reqLevel, double exp) {
            this.id = id;
            this.reqLevel = reqLevel;
            this.exp = exp;
        }

    }

    public static Log getLog(int id) {
        for (Log f : Log.values()) {
            if (f.id == id) {
                return f;
            }
        }
        return null;
    }

    /**
     * Lights a log.
     *
     * @param log The log to light.
     */
    public static void light(final Player player, final Log log, int slot) {
        Item item = log.getItem();
        if (log.getRequiredLevel() > player.getSkills().getLevel(Skills.FIREMAKING)) {

            String connect = "a";
            String itemName = item.getDefinition().getName().trim().replaceAll("_", " ");
            for (String vowel : TextUtils.vowels) {
                if (itemName.toLowerCase().startsWith(vowel)) {
                    connect = "an";
                    break;
                }
            }
            player.getPacketSender().sendMessage("You must have a Firemaking level of " + log.getRequiredLevel() + " to light " + connect + " " + itemName + ".");
            return;
        }
        GameObject tileObj = World.getWorld().getStandardObject(player.getLocation());
        if (tileObj != null && tileObj.getType() != 22) {
            player.getPacketSender().sendMessage("You cannot light a fire here.");
            return;
        }
        if (player.getLocation().isInArea(3089, 3473, 3098, 3484)) {
            player.getPacketSender().sendMessage("You cannot light a fire in the quest room.");
            return;
        }
        if (player.getLocation().isInArea(3090, 3487, 3099, 3500)) {
            player.getPacketSender().sendMessage("You cannot light a fire in the bank.");
            return;
        }
        if (player.getLocation().isInArea(3075, 3506, 3085, 3514)) {
            player.getPacketSender().sendMessage("You cannot light a fire in the shop room.");
            return;
        }
        player.getAttributes().set("stopActions", true);
        player.getInventory().deleteItem(item, slot);
        player.getInventory().refresh();
        player.animate(Animation.create(733));
        player.getPacketSender().sendMessage("You attempt to light the logs.");
        World.getWorld().submit(new Tickable(lightDelay(player, log)) {
            @Override
            public void execute() {

                player.animate(Animation.create(-1));
                final GameObject fire = new GameObject(player.getLocation(), 26185, 10, 0);
                World.spawnFireTemporary(fire, NumberUtils.random(80, 120), player);
                findPath(player);
                player.getAttributes().remove("stopActions");
                player.getPacketSender().sendMessage("The fire catches and the logs begin to burn.");
                addExperience(player, log);
                this.stop();
            }
        });


    }

    private static void findPath(Player player) {
        int dir = -1;
        if (!DumbPathFinder.blockedWest(player.getLocation(), player)) {
            dir = 12;
        } else if (!DumbPathFinder.blockedNorth(player.getLocation(), player)) {
            dir = 0;
        } else if (!DumbPathFinder.blockedSouth(player.getLocation(), player)) {
            dir = 4;
        } else if (!DumbPathFinder.blockedEast(player.getLocation(), player)) {
            dir = 8;
        }
        boolean foundPath = false;
        int targX = player.getLocation().getX();
        int targY = player.getLocation().getY();
        if (!foundPath) {
            if (dir == 0) {
                targY += 1;
            } else if (dir == 4) {
                targY -= 1;
            } else if (dir == 8) {
                targX += 1;
            } else if (dir == 12) {
                targX -= 1;
            }
        }
        player.execute_path(new SizedPathFinder(), targX, targY);
    }

    /**
     * Adds experience to the player for a specific log.
     *
     * @param log The log we will add experience for.
     * @return true if the player has gained a level.
     */
    private static boolean addExperience(Player player, Log log) {
        int beforeLevel = player.getSkills().getLevelForXp(Skills.FIREMAKING);
        player.getSkills().addExperience(Skills.FIREMAKING, log.getExperience());
        int afterLevel = player.getSkills().getLevelForXp(Skills.FIREMAKING);
        Achievements.increase(player, 2);
        //if(afterLevel > beforeLevel) {
        //	return true; //fm level up
        //}
        return false;
    }

    /**
     * Light delay for a specific log.
     *
     * @param log The log.
     * @return The light delay.
     */
    private static int lightDelay(Player player, Log log) {
        int lvl = player.getSkills().getLevel(Skills.FIREMAKING);

        if ((lvl + 19) > random(0, log.reqLevel + 10)) { //At level 80 you will stop failing
            return random(2, 4);
        } else {
            return 1;
        }
        //return random(1, 2);
    }

    /**
     * Finds the log for an item.
     *
     * @param item The log item.
     * @return The log for the item.
     */
    public Log findLog(Item item) {
        switch (item.getDefinition().getId()) {
            case 1511:
                return Log.NORMAL;
            case 1521:
                return Log.OAK;
            case 1519:
                return Log.WILLOW;
            case 6333:
                return Log.TEAK;
            case 1517:
                return Log.MAPLE;
            case 6332:
                return Log.MAHOGANY;
            case 1515:
                return Log.YEW;
            case 1513:
                return Log.MAGIC;
        }
        return null;
    }

    /**
     * Returns a random integer with min as the inclusive
     * lower data and max as the exclusive upper data.
     *
     * @param min The inclusive lower data.
     * @param max The exclusive upper data.
     * @return Random integer min <= n < max.
     */
    private static int random(int min, int max) {
        Random random = new Random();
        int n = Math.abs(max - min);
        return Math.min(min, max) + (n == 0 ? 0 : random.nextInt(n));
    }

}