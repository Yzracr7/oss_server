package com.hyperion.game.content.skills.fishing;

import com.hyperion.game.world.Location;

/**
 * @author Nando
 */
public class FishSpot {

	private Location location;
	private int npc_id;
	private Option[] options;
	private int index = -1;
	
	public static class Option {
		private int id;
		private int animation;
		private Fish[] fishes;
		private Requirement[] requirements;
		
		public int getId() {
			return id;
		}

		public int getAnimation() {
			return animation;
		}

		public Fish[] getFishes() {
			return fishes;
		}
		
		public Requirement[] getRequirements() {
			return requirements;
		}
	}
	
	public static class Fish {
		private int id;
		private int level;
		private double exp;
		
		public int getId() {
			return id;
		}
		
		public void setId(int id) {
			this.id = id;
		}
		
		public int getLevel() {
			return level;
		}
		
		public double getExp() {
			return exp;
		}
	}
	
	public static class Requirement {
		private int id;
		private int amount;
		private boolean delete;
		private boolean primary;
		private boolean secondary;
		
		public int getId() {
			return id;
		}
		
		public void setId(int id) {
			this.id = id;
		}
		
		public int getAmount() {
			return amount;
		}
		
		public boolean isDelete() {
			return delete;
		}

		public boolean isPrimary() {
			return primary;
		}

		public boolean isSecondary() {
			return secondary;
		}
	}
	
	public Location getSpot() {
		return location;
	}

	public int getNpc_id() {
		return npc_id;
	}

	public Option[] getOption() {
		return options;
	}
	
	public String toString() {
		return "fish npc: " + npc_id + ", loc: " + location;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
}
