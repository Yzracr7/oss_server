package com.hyperion.game.content.skills.fishing;

import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.content.skills.SkillLoaders;
import com.hyperion.game.content.skills.fishing.FishSpot.Fish;
import com.hyperion.game.content.skills.fishing.FishSpot.Option;
import com.hyperion.game.content.skills.fishing.FishSpot.Requirement;
import com.hyperion.game.item.Item;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.utility.NumberUtils;

/**
 * @author Nando
 */
public class FishingSession extends Tickable {

    private Player player;
    private FishSpot fish_spot;
    private int optionIndex = -1, fishIndex = -1, delay = 0;
    private boolean started = false;

    private FishingSession(Player player, NPC npc, FishSpot spot, int option) {
        super(1, true);
        this.player = player;
        this.fish_spot = spot;
        int len = fish_spot.getOption().length;
        this.optionIndex = len > 1 ? option - 1 : 0;
        player.getVariables().setFishing(this);
    }

    public static boolean canFish(Player player, NPC npc, int option) {
        if (SkillLoaders.fish_spots == null)
            return false;
        for (FishSpot spots : SkillLoaders.fish_spots) {
            if (spots.getNpc_id() == npc.getId()) {
                if (spots.getSpot().equals(npc.getLocation())) {
                    for (Option options : spots.getOption()) {
                        if (options.getId() == option) {
                            FishingSession session = new FishingSession(player, npc, spots, option);
                            World.getWorld().submit(session);
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private boolean sentReqMessage = false;

    private void startFishing(final boolean new_fish) {
        Fish[] fish = fish_spot.getOption()[optionIndex].getFishes();
        if (fish == null) {//shouldn't happen..
            return;
        }
        this.fishIndex = getFishToAdd();
        if (!hasRequirements()) {
            resetFishing();
            sentReqMessage = true;
            return;
        }
        player.animate(fish_spot.getOption()[optionIndex].getAnimation());
        final int delay = getCalculatedTicks();
        this.delay = delay;
    }

    private void finish_cycle() {
        if (!hasRequirements()) {
            resetFishing();
            sentReqMessage = true;
            return;
        }
        Fish fishToAdd = fish_spot.getOption()[optionIndex].getFishes()[fishIndex];
        Requirement[] req = fish_spot.getOption()[optionIndex].getRequirements();
        for (Requirement reqs : req) {
            if (reqs.isDelete()) {
                player.getInventory().deleteItem(reqs.getId(), 1);
            }
        }
        if (player.getDetails().isDonator()) {
            int noted_id = ItemDefinition.forId(fishToAdd.getId()).isNoteable() ? fishToAdd.getId() + 1 : -1;
            if (NumberUtils.random(3) == 1) {
                player.getInventory().addItem(noted_id, 1);
            }
        }
        player.getInventory().addItem(fishToAdd.getId(), 1);
        player.getInventory().refresh();
        Achievements.increase(player, 1);
        player.getSkills().addExperience(Skills.FISHING, fishToAdd.getExp());
        sentReqMessage = false;
    }

    private boolean hasRequirements() {
        if (player.getSkills().getLevel(Skills.FISHING) < fish_spot.getOption()[optionIndex].getFishes()[fishIndex].getLevel()) {
            player.getPacketSender().sendMessage("You need a Fishing level of " + fish_spot.getOption()[optionIndex].getFishes()[fishIndex].getLevel() + " to fish here.");
            return false;
        }
        Requirement[] req = fish_spot.getOption()[optionIndex].getRequirements();
        for (Requirement reqs : req) {
            if (!player.getInventory().hasItem(reqs.getId())) {
                String type = reqs.isPrimary() ? "a" : "some";
                ItemDefinition reqItem = ItemDefinition.forId(reqs.getId());
                player.getPacketSender().sendMessage("You need " + type + " " + reqItem.getName() + " to fish here.");
                return false;
            }
        }
        if (player.getInventory().findFreeSlot() == -1) {
            if (!sentReqMessage)
                player.getPacketSender().sendMessage("Your inventory is too full to catch any more fish.");
            return false;
        }
        if (player.getAttributes().isSet("teleporting")) {
            return false;
        }
        return true;
    }

    public int getFishToAdd() {
        int fishingLevel = player.getSkills().getLevel(Skills.FISHING);
        int[] canCatch = new int[fish_spot.getOption()[optionIndex].getFishes().length];
        int j = 0;
        for (Fish fish : fish_spot.getOption()[optionIndex].getFishes()) {
            if (fishingLevel >= fish.getLevel()) {
                canCatch[j] = fish.getId();
                j++;
            }
        }
        int[] canCatch2 = new int[j];
        for (int i = 0; i < canCatch.length; i++) {
            if (canCatch[i] > 0) {
                canCatch2[i] = canCatch[i];
            }
        }
        int fish = NumberUtils.random(0, canCatch2.length);
        for (int i = 0; i < fish_spot.getOption()[optionIndex].getFishes().length; i++) {
            if (fish == i) {
                return i;
            }
        }
        return NumberUtils.random(canCatch2.length - 1);
    }

    /**
     * Gets the amount of ticks for this session.
     *
     * @return The amount of ticks.
     */
    private int getCalculatedTicks() {
        int skill = player.getSkills().getLevel(Skills.FISHING);
        int level = fish_spot.getOption()[optionIndex].getFishes()[fishIndex].getLevel();
        int modifier = fish_spot.getOption()[optionIndex].getFishes()[fishIndex].getLevel();
        int randomAmt = NumberUtils.random(4);
        double cycleCount = Math.ceil((level * 50 - skill * 15) / modifier * 0.25 - randomAmt * 4);
        if (cycleCount < 1) {
            cycleCount = 1;
        }
        return (int) cycleCount + 1;
    }

    public static void depositFish(Player player) {
        if (player.getInventory().hasItem(15270)) {
            int noted = ItemDefinition.forId(15270).isNoteable() ? 15270 + 1 : -1;
            int amountInInv = player.getInventory().getItemAmount(15270);
            for (int i = 0; i < amountInInv; i++) {
                player.getInventory().deleteItem(15270, 1);
            }
            player.getInventory().addItem(new Item(noted, amountInInv));
            player.getInventory().refresh();
        } else {
            player.getPacketSender().sendMessage("You need raw Rocktail fish to use this.");
        }
    }

    public void resetFishing() {
        this.stop();
        player.getVariables().setFishing(null);
        player.animate(65535);
    }

    @Override
    public void execute() {
        // TODO Auto-generated method stub
        if (player.isDestroyed()) {
            this.stop();
            return;
        }
        if (delay > 0) {
            delay--;
        } else if (delay <= 0) {
            if (!started) {
                started = true;
                startFishing(true);
            } else {
                startFishing(false);
                finish_cycle();
            }
        }
    }
}
