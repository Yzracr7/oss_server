package com.hyperion.game.content.skills.fletching;

public class Ammo {

	private int finishedItem;
	private int itemOne;
	private int itemTwo;
	private double xp;
	private int level;
	private int itemType;
	private int amount;
	private boolean isBolt;
	
	public Ammo(int finished, int one, int two, double xp, int level, int itemType, boolean isBolt, int amount) {
		this.finishedItem = finished;
		this.itemOne = one;
		this.itemTwo = two;
		this.xp = xp;
		this.level = level;
		this.itemType = itemType;
		this.isBolt = isBolt;
		this.amount = amount;
	}
	
	public int getAmount() {
		return amount;
	}
	
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	public int getFinishedItem() {
		return finishedItem;
	}
	
	public int getItemOne() {
		return itemOne;
	}
	
	public int getItemTwo() {
		return itemTwo;
	}
	
	public double getXp() {
		return xp;
	}
	
	public int getLevel() {
		return level;
	}
	
	public int getItemType() {
		return itemType;
	}

	public void decreaseAmount() {
		this.amount = amount - 1;
	}

	public boolean isBolt() {
		return isBolt;
	}
}