package com.hyperion.game.content.skills.fletching;

import com.hyperion.game.world.entity.player.Player;

public class Fletching extends FletchData {

	public Fletching() {
		
	}
	
	/**
	 * Handles fletching buttons
	 * @param player
	 * @param child
	 */
	public static void handleFletching(Player player, int child) {
		boolean stringingBow = player.getAttributes().isSet("stringingBow");
		int boltTips = player.getAttributes().getInt("boltTips");
		int logType = player.getAttributes().getInt("fletchType");
		int ammoType = player.getAttributes().getInt("ammoType");
		int boltType = player.getAttributes().getInt("ammoType2");
		int xbowType = player.getAttributes().getInt("bowType2");
		int bowType = player.getAttributes().getInt("bowType");
		switch (child) {
		case 13:
			if (logType != -1) {
				MakeBows.cutLog(player, 1, logType, 0, stringingBow, true);
				break;
			}
			break;
		case 12:
			if (logType == 0) {
				MakeBows.cutLog(player, 5, logType, 0, stringingBow, true);
				break;
			} else if (logType > 0){
				MakeBows.cutLog(player, 1, logType, 1, stringingBow, true);
				break;
			} 
			break;
		case 11:
			if (logType == 0) {
				MakeBows.cutLog(player, 10, logType, 0, stringingBow, true);
				break;
			} else if (logType > 0){
				MakeBows.cutLog(player, 1, logType, 1, stringingBow, true);
				break;
			}
			break;
		case 10:
			if (logType == 0) {
				player.getInterfaceSettings().openEnterAmountInterface("Enter amount:", 305, 0);
				break;
			} else if (logType > 0){
				MakeBows.cutLog(player, 5, logType, 1, stringingBow, true);
				break;
			}
			break;
		case 17:
			if (logType != -1) {
				MakeBows.cutLog(player, 1, logType, 1, stringingBow, true);
				break;
			}
			break;
		case 16:
			if (logType != -1) {
				MakeBows.cutLog(player, 5, logType, 1, stringingBow, true);
				break;
			}
			break;
		case 15:
			if (logType != -1) {
				MakeBows.cutLog(player, 10, logType, 1, stringingBow, true);
				break;
			}
			break;
		case 14:
			if (logType == 0 && !stringingBow) {
				player.getInterfaceSettings().openEnterAmountInterface("Enter amount:", 305, 1);
				break;
			} else if (logType > 0){
				player.getInterfaceSettings().openEnterAmountInterface("Enter amount:", 305, 1);
				break;
			}
			break;
		case 9:
			if (logType == 0) {
				MakeBows.cutLog(player, 1, logType, 2, false, true);
				break;
			} else if (logType > 0){
				MakeBows.cutLog(player, 10, logType, 1, stringingBow, true);
				break;
			}
			break;
		case 8:
			if (logType == 0) {
				MakeBows.cutLog(player, 5, logType, 2, false, true);
				break;
			} else if (logType > 0){
				player.getInterfaceSettings().openEnterAmountInterface("Enter amount:", 305, 1);
				break;
			}
			break;
		case 7:
			if (logType == 0) {
				MakeBows.cutLog(player, 10, logType, 2, false, true);
				break;
			} else if (logType > 0){
				MakeBows.cutLog(player, 1, logType, 0, stringingBow, true);
				break;
			}
			break;
		case 6:
			if (logType == 0 && !stringingBow) {
				player.getInterfaceSettings().openEnterAmountInterface("Enter amount:", 305, 2);
				break;
			} else if (stringingBow && bowType != -1) {
				MakeBows.cutLog(player, 1, logType, bowType, true, true);
				break;
			} else if (boltTips > -1) {
				MakeAmmo.makeBoltTip(player, boltTips, 1, true);
				break;
			} else if (xbowType != -1) {
				MakeXbow.createXbow(player, 1, xbowType, stringingBow, true);
				break;
			} else if (logType > 0){
				MakeBows.cutLog(player, 5, logType, 0, false, true);
				break;
			}
			break;
		case 5:
			if (ammoType != -1) {
				MakeAmmo.createAmmo(player, 1, ammoType, false, true);
				break;
			} else if (stringingBow && bowType != -1) {
				MakeBows.cutLog(player, 5, logType, bowType, true, true);
				break;
			} else if (boltType != -1) {
				MakeAmmo.createAmmo(player, 1, boltType, true, true);
				break;
			} else if (xbowType != -1) {
				MakeXbow.createXbow(player, 5, xbowType, stringingBow, true);
				break;
			} else if (logType != -1) {
				MakeBows.cutLog(player, 10, logType, 0, false, true);
				break;
			} else if (boltTips > -1) {
				MakeAmmo.makeBoltTip(player, boltTips, 5, true);
				break;
			}
			break;
		case 4:
			if (ammoType != -1) {
				MakeAmmo.createAmmo(player, 5, ammoType, false, true);
				break;
			} else if (boltType != -1) {
				MakeAmmo.createAmmo(player, 5, boltType, true, true);
				break;
			} else if (xbowType != -1) {
				player.getInterfaceSettings().openEnterAmountInterface("Enter amount:", 309, 1);
				break;
			} else if (stringingBow && bowType != -1) {
				player.getInterfaceSettings().openEnterAmountInterface("Enter amount:", 309, 1);
				break;
			} else if (logType != -1){
				player.getInterfaceSettings().openEnterAmountInterface("Enter amount:", 305, 0);
				break;
			} else if (boltTips > -1) { // Cut bolt tips
				player.getInterfaceSettings().openEnterAmountInterface("Enter amount:", 309, 6);
				break;
			}
			break;
		case 3:
			if (ammoType != -1) {
				MakeAmmo.createAmmo(player, 10, ammoType, false, true);
				break;
			} else if (boltType != -1) {
				MakeAmmo.createAmmo(player, 10, boltType, true, true);
				break;
			} else if (stringingBow && bowType != -1) {
				MakeBows.cutLog(player, player.getInventory().getItemAmount(1777), logType, bowType, true, true);
				break;
			} else if (xbowType != -1) {
				MakeXbow.createXbow(player, player.getInventory().getItemAmount(9438), xbowType, stringingBow, true);
				break;
			} else if (boltTips > -1) {
				MakeAmmo.makeBoltTip(player, boltTips, player.getInventory().getItemAmount((Integer)FletchData.GEMS[boltTips][0]), true);
				break;
			}
			break;
		case 21:
			if (logType == 0) {
				MakeBows.cutLog(player, 1, logType, 3, false, true);
				break;
			}
			break;
		case 20:
			if (logType == 0) {
				MakeBows.cutLog(player, 5, logType, 3, false, true);
				break;
			}
			break;
		case 19:
			if (logType == 0) {
				MakeBows.cutLog(player, 10, logType, 3, false, true);
				break;
			}
			break;
		case 18:
			if (logType == 0) {
				player.getInterfaceSettings().openEnterAmountInterface("Enter amount:", 305, 3);
				break;
			}
			break;
		}
	}
	
	public static boolean isFletching(Player p, int itemUsed, int usedWith) {
		int itemOne = itemUsed;
		int itemTwo = usedWith;
		for (int i = 0; i < 2; i++) {
			if (i == 1) {
				itemOne = usedWith;
				itemTwo = itemUsed;
			}
			//Don't want anyone else getting loads of XP and then being forced to divide theirs by six.
			//p.getPacketSender().sendMessage("Fletching is currently disabled.");
			for (int j = 0; j < LOGS.length; j++) {
				if (itemOne == LOGS[j] && itemTwo == KNIFE) {
					MakeBows.displayBowOptions(p, j, false, -1);
					setFletchItem(p, null);
					p.getAttributes().set("fletchType", j);
					return true;
				}
			}
			for (int j = 0; j < UNSTRUNG_SHORTBOW.length; j++) {
				if (itemOne == UNSTRUNG_SHORTBOW[j] && itemTwo == BOWSTRING) {
					MakeBows.displayBowOptions(p, j, true, 0);
					setFletchItem(p, null);
					p.getAttributes().set("fletchType", j);
					p.getAttributes().set("bowType", 0);
					p.getAttributes().set("stringingBow", (Boolean) true);
					return true;
				}
			}
			for (int j = 0; j < UNSTRUNG_LONGBOW.length; j++) {
				if (itemOne == UNSTRUNG_LONGBOW[j] && itemTwo == BOWSTRING) {
					MakeBows.displayBowOptions(p, j, true, 1);
					setFletchItem(p, null);
					p.getAttributes().set("fletchType", j);
					p.getAttributes().set("bowType", 1);
					p.getAttributes().set("stringingBow", (Boolean) true);
					return true;
				}
			}
			for (int j = 0; j < ARROWHEAD.length; j++) {
				if (itemOne == ARROWHEAD[j] && itemTwo == HEADLESS_ARROW) {
					MakeAmmo.displayAmmoInterface(p, j, false, false);
					setFletchItem(p, null);
					p.getAttributes().set("ammoType", j);
					return true;
				}
			}
			for (int j = 0; j < FEATHERLESS_BOLT.length; j++) {
				if (itemOne == FEATHERLESS_BOLT[j] && itemTwo == FEATHER) {
					MakeAmmo.displayAmmoInterface(p, j, true, true);
					setFletchItem(p, null);
					p.getAttributes().set("ammoType2", j);
					return true;
				}
			}
			for (int k = 0; k < BOLT_TIPS.length; k++) {
				if (itemOne == HEADLESS_BOLT[k] && itemTwo == BOLT_TIPS[k]) {
					setFletchItem(p, null);
					p.getAttributes().set("ammoType2", k + 8);
					int boltType = p.getAttributes().getInt("ammoType2");
					MakeAmmo.createAmmo(p, 12, boltType, true, true);
					return true;
				}
			}
			for (int j = 0; j < FEATHERLESS_BOLT.length; j++) {
				for (int k = 0; k < BOLT_TIPS.length; k++) {
					if (itemOne == FEATHERLESS_BOLT[j] && itemTwo == BOLT_TIPS[k]) {
						setFletchItem(p, null);
						p.getPacketSender().sendMessage("You must add Feathers to a bolt before you can add a tip.");
						return true;
					}
				}
			}
			for (int j = 0; j < XBOW_LIMB.length; j++) {
				if (itemOne == XBOW_LIMB[j] && itemTwo == CROSSBOW_STOCK[0]) {
					MakeXbow.displayOptionInterface(p, j, false);
					setFletchItem(p, null);
					p.getAttributes().set("bowType2", j);
					return true;
				}
			}
			for (int j = 0; j < UNFINISHED_XBOW.length; j++) {
				if (itemOne == UNFINISHED_XBOW[j] && itemTwo == XBOW_STRING) {
					MakeXbow.displayOptionInterface(p, j, true);
					setFletchItem(p, null);
					p.getAttributes().set("bowType2", j);
					p.getAttributes().set("stringingBow", (Boolean) true);
					return true;
				}
			}
			for (int j = 0; j < GEMS.length; j++) {
				if (itemOne == (Integer)GEMS[j][0] && itemTwo == 1755) {
					MakeAmmo.displayGemOptions(p, j);
					setFletchItem(p, null);
					p.getAttributes().set("boltTips", j);
					return true;
				}
			}
			if (itemOne == ARROW_SHAFTS && itemTwo == FEATHER) {
				MakeAmmo.displayAmmoInterface(p, 0, true, false);
				setFletchItem(p, null);
				p.getAttributes().set("ammoType", 7);
				return true;
			}
		}
		return false;
	}
	
	public static void setFletchItem(Player p, Object a) {
		if (a == null) {
			resetAllFletchVariables(p);
			p.getAttributes().remove("fletchItem");
			return;
		}
		p.getAttributes().set("fletchItem", (Object) a);
	}

	private static void resetAllFletchVariables(Player p) {
		p.getAttributes().remove("fletchType");
		p.getAttributes().remove("ammoType");
		p.getAttributes().remove("bowType");
		p.getAttributes().remove("ammoType2");
		p.getAttributes().remove("bowType2");
		p.getAttributes().remove("stringingBow");
		p.getAttributes().remove("boltTips");
	}

	public static Object getFletchItem(Player p) {
		return (Object) p.getAttributes().get("fletchItem");
	}
}
