package com.hyperion.game.content.skills.fletching;

import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.content.skills.crafting.CraftItem;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;

public class MakeAmmo extends FletchData {

	public MakeAmmo() {
		
	}
	
	public static void displayAmmoInterface(Player p, int type, boolean featherless, boolean bolt) {
		String s = "<br><br><br><br>";
		String s1 = bolt && type == 0 ? "Bronze " : "";
		String text = "";
		int item = -1;
		if (!bolt) {
			if (featherless) {
				item = HEADLESS_ARROW;
				text = ARROW_AMOUNT + " Headless arrows";
			} else {
				item = ARROW[type];
				text = ItemDefinition.forId(item).getName() + "s";
			}
		} else {
			if (featherless) {
				item = FEATHERED_BOLT[type];
				text = ItemDefinition.forId(item).getName();
			} else {
				item = BOLT[type];
				text = ItemDefinition.forId(item).getName();
			}
		}
		p.getPacketSender().sendChatboxInterface(582);
		p.getPacketSender().itemOnInterface(582, 2, 150, item);
		p.getPacketSender().modifyText(s + s1 + text, 582, 5);
	}

	public static void createAmmo(final Player p, int sets, int type, boolean bolt, boolean newFletch) {
		Ammo item = null;
		if (newFletch || Fletching.getFletchItem(p) == null) {
			item = getAmmo(type, bolt, sets);
			Fletching.setFletchItem(p, item);
		}
		item = (Ammo) Fletching.getFletchItem(p);
		if (item == null || p == null) {
			return;
		}
		if (!canFletch(p, item)) {
			p.getInterfaceSettings().closeInterfaces(false);
			return;
		}
		int amt = getArrowAmount(p, item);
		if (amt <= 0) {
			return;
		}
		if (p.getInventory().deleteItem(item.getItemOne(), amt) && p.getInventory().deleteItem(item.getItemTwo(), amt)) {
			p.getInventory().addItem(item.getFinishedItem(), amt);
			p.getInventory().refresh();
			p.getSkills().addExperience(FLETCHING, item.getXp() * amt);
			Achievements.increase(p, 23);
			//p.getFrames().sendMessage(getMessage(item, amt));
			item.decreaseAmount();
			p.getInterfaceSettings().closeInterfaces(false);
		}
		/*if (item.getAmount() >= 1) {
			World.getWorld().submit(new Tickable(3) {
				@Override
				public void execute() {
					createAmmo(p, -1, -1, false, false);
					this.stop();
				}
			});
		}*/
	}

	/*private static String getMessage(Ammo item, int amount) {
		String s = amount > 1 ? "s" : "";
		String s1 = amount > 1 ? "some" : "a";
		String s2 = amount > 1 ? "some" : "an";
		if (!item.isBolt()) {
			s = amount > 1 ? "s" : "";
			s1 = amount > 1 ? "some" : "a";
			s2 = amount > 1 ? "some" : "an";
			if (item.getItemType() == 0) {
				return "You attach Feathers to " + s1 + " of your Arrow shafts, you make " + amount + " " + ItemDefinition.forId(item.getFinishedItem()).getName() + s + ".";
			} else {
				return "You attach " + s2 + " Arrowtip" + s + " to " + s1 + " Headless arrow" + s + ", you make " + amount + " " + ItemDefinition.forId(item.getFinishedItem()).getName() + s + ".";
			}
		} else {
			s = amount > 1 ? "s" : "";
			s1 = amount > 1 ? "some of your bolts" : "a bolt";
			s2 = amount > 1 ? "some" : "a";
			if (item.getItemType() < 8) {
				return "You attach Feathers to " + s1 + ", you make " + amount + " " + ItemDefinition.forId(item.getFinishedItem()).getName()+".";
			} else {
				s = amount > 1 ? "s" : "";
				s2 = amount > 1 ? "some" : "a";
				return "You attach " + s2 + " bolt tip" + s + " to " + s2 + " headless bolt" + s + ", you make " + amount + " " + ItemDefinition.forId(item.getFinishedItem()).getName() + ".";
			}
		}
	}*/

	private static Ammo getAmmo(int type, boolean bolt, int amount) {
		int i = type;
		if (!bolt) {
			if (i == 7) {
				return new Ammo(HEADLESS_ARROW, FEATHER, ARROW_SHAFTS, HEADLESS_ARROW_XP, HEADLESS_ARROW_LVL, 0, false, amount);
			}
			if (i >= 0 && i < 7) {
				return new Ammo(ARROW[i], HEADLESS_ARROW, ARROWHEAD[i], ARROW_XP[i], ARROW_LVL[i], i, false, amount);
			}
		} else {
			if (i < 8) {
				return new Ammo(FEATHERED_BOLT[i], FEATHER, FEATHERLESS_BOLT[i], FEATHERLESS_BOLT_XP[i], FEATHERLESS_BOLT_LVL[i], i, true, amount);
			}
			if (i >= 8) {
				i -= 8;
				return new Ammo(BOLT[i], HEADLESS_BOLT[i], BOLT_TIPS[i], HEADLESS_BOLT_XP[i], HEADLESS_BOLT_LVL[i], i, true, amount);
			}
		}
		return null;
	}

	private static int getArrowAmount(Player p, Ammo item) {
		if (item == null || p == null) {
			return 0;
		}
		int amt = item.isBolt() ? 12 : 15;
		int itemOneAmount = p.getInventory().getItemAmount(item.getItemOne());
		int itemTwoAmount = p.getInventory().getItemAmount(item.getItemTwo());
		int lowestStack = itemOneAmount > itemTwoAmount ? itemTwoAmount : itemOneAmount;
		int finalAmount = lowestStack > amt ? amt : lowestStack;
		return finalAmount;
	}

	private static boolean canFletch(Player p, Ammo item) {
		if (item == null || item.getAmount() <= 0) {
			return false;
		}
		String s = item.getItemOne() == HEADLESS_ARROW ? "s." : ".";
		String s1 = item.getItemTwo() == HEADLESS_ARROW ? "s." : ".";
		//TODO: if fletching, stop woodcutting
		if (p.getSkills().getLevel(FLETCHING) < item.getLevel()) {
			p.getPacketSender().sendMessage("You need a Fletching level of " + item.getLevel() + " to make that.");
			return false;
		}
		if (!p.getInventory().hasItem(item.getItemOne())) {
			p.getPacketSender().sendMessage("You don't have any " + ItemDefinition.forId(item.getItemOne()).getName() + s);
			return false;
		}
		if (!p.getInventory().hasItem(item.getItemTwo())) {
			p.getPacketSender().sendMessage("You don't have any " + ItemDefinition.forId(item.getItemTwo()).getName() + s1);
			return false;
		}
		return true;
	}

	public static void displayGemOptions(Player p, int index) {
		int amount = p.getInventory().getItemAmount((Integer)GEMS[index][0]);
		if (amount > 1) {
			String s = "<br><br><br><br>";
			p.getPacketSender().itemOnInterface(309, 2, 190, (Integer) GEMS[index][1]);
			p.getPacketSender().modifyText(s + (String) GEMS[index][4] + " bolt tips (x12)", 309, 6);
			p.getPacketSender().sendChatboxInterface(309);
			return;
		}
		makeBoltTip(p, index, 1, true);
	}

	public static void makeBoltTip(final Player p, int index, int amount, boolean newCut) {
		if (newCut) {
			p.getAttributes().set("fletchItem", new CraftItem(5, index, amount, (Double) GEMS[index][3], (Integer) GEMS[index][1], (String) GEMS[index][4], (Integer) GEMS[index][2]));
		}
		if(p.getAttributes().get("fletchItem") instanceof Ammo) {
			Fletching.setFletchItem(p, null);
			p.getPacketSender().sendMessage("Unable to fletch this item. If this is wrong contact a staff member.");
			return;
		}
		CraftItem item = (CraftItem) p.getAttributes().get("fletchItem");
		if (item == null || p == null || item.getAmount() <= 0) {
			Fletching.setFletchItem(p, null);
			return;
		}
		p.getInterfaceSettings().closeInterfaces(false);
		if (!p.getInventory().hasItem(1755)) {
			p.getPacketSender().sendMessage("You need a chisel to cut gems into bolt tips.");
			Fletching.setFletchItem(p, null);
			return;
		}
		if (!p.getInventory().hasItem((Integer)GEMS[item.getCraftItem()][0])) {
			if (newCut) {
				p.getPacketSender().sendMessage("You have no " + item.getMessage() + " to cut.");
			} else {
				p.getPacketSender().sendMessage("You have no more " + item.getMessage() + "s to cut.");
			}
			Fletching.setFletchItem(p, null);
			return;
		}
		if (p.getSkills().getLevel(FLETCHING) < item.getLevel()) {
			p.getPacketSender().sendMessage("You need a Fletching level of " + item.getLevel() + " to cut that gem.");
			Fletching.setFletchItem(p, null);
			return;
		}
		int amountToMake = item.getCraftItem() == 9 ? 24 : 12;
		if (p.getInventory().deleteItem((Integer)GEMS[item.getCraftItem()][0])) {
			p.getInventory().addItem(item.getFinishedItem(), amountToMake);
			p.getSkills().addExperience(FLETCHING, item.getXp());
			p.animate((Integer) GEMS[item.getCraftItem()][5]);
			p.getInventory().refresh();
			//p.getPackets().sendMessage("You cut the " + item.getMessage() + " into " + amountToMake + " " + item.getMessage() + " bolt tips.");
		}
		item.decreaseAmount();
		if (item.getAmount() >= 1) {
			World.getWorld().submit(new Tickable(2) {
				@Override
				public void execute() {
					makeBoltTip(p, -1, -1, false);
					p.getInventory().refresh();
					this.stop();
				}
			});
		}
	}
}
