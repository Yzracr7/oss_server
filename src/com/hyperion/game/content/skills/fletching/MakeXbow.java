package com.hyperion.game.content.skills.fletching;

import com.hyperion.game.content.skills.SkillItem;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;

public class MakeXbow extends FletchData {

	public static void displayOptionInterface(Player p, int type, boolean stringing) {
		String s = "<br><br><br><br>";
		p.getPacketSender().sendChatboxInterface(309);
		if (!stringing) {
			p.getPacketSender().itemOnInterface(309, 2, 150, UNFINISHED_XBOW[type]);
			p.getPacketSender().modifyText(s + ItemDefinition.forId(UNFINISHED_XBOW[type]).getName(), 309, 6);
			return;
		}
		p.getPacketSender().itemOnInterface(309, 2, 150, FINISHED_XBOW[type]);
		p.getPacketSender().modifyText(s + ItemDefinition.forId(FINISHED_XBOW[type]).getName(), 309, 6);
	}

	public static void createXbow(final Player p, int amount, int xbowType, boolean isStringing, boolean newFletch) {
		SkillItem item = null;
		if (newFletch || Fletching.getFletchItem(p) == null) {
			item = getXbow(xbowType, isStringing, amount);
			Fletching.setFletchItem(p, item);
		}
		item = (SkillItem) Fletching.getFletchItem(p);
		if (item == null || p == null) {
			return;
		}
		boolean stringing = item.getItemTwo() == XBOW_STRING ? true : false;
		if (!canFletch(p, item)) {
			p.getInterfaceSettings().closeInterfaces(false);
			return;
		}
		if (p.getInventory().deleteItem(item.getItemOne()) &&  p.getInventory().deleteItem(item.getItemTwo())) {
			p.getInventory().addItem(item.getFinishedItem());
			p.getSkills().addExperience(FLETCHING, item.getXp());
			item.decreaseAmount();
			p.getInventory().refresh();
			p.getInterfaceSettings().closeInterfaces(false);
			if (!stringing) {
				//p.getPackets().sendMessage("You attach some limbs to the Crossbow.");
			} else {
				p.animate(6677);
				//p.getPackets().sendMessage("You add a Crossbow String to the Crossbow, you have completed the " + ItemDefinition.forId(item.getFinishedItem()).getName() + ".");
			}
		}
		if (item.getAmount() >= 1) {
			World.getWorld().submit(new Tickable(3) {
				@Override
				public void execute() {
					createXbow(p, -1, -1, false, false);
					this.stop();
				}
			});
		}
	}

	private static boolean canFletch(Player p, SkillItem item) {
		if (item == null || item.getAmount() <= 0) {
			return false;
		}
		if (p.getSkills().getLevel(FLETCHING) < item.getLevel()) {
			p.getPacketSender().sendMessage("You need a Fletching level of " + item.getLevel() + " to make that.");
			return false;
		}
		if (!p.getInventory().hasItem(item.getItemOne())) {
			p.getPacketSender().sendMessage("You don't have a " + ItemDefinition.forId(item.getItemOne()).getName());
			return false;
		}
		if (!p.getInventory().hasItem(item.getItemTwo())) {
			p.getPacketSender().sendMessage("You don't have any " + ItemDefinition.forId(item.getItemTwo()).getName());
			return false;
		}
		return true;
	}

	private static SkillItem getXbow(int xbowType, boolean stringing, int amount) {
		int i = xbowType;
		if (!stringing) {
			return new SkillItem(UNFINISHED_XBOW[i], CROSSBOW_STOCK[0], XBOW_LIMB[i], XBOW_LVL[i], FLETCHING, XBOW_XP[i], amount);
		} else {
			return new SkillItem(FINISHED_XBOW[i], UNFINISHED_XBOW[i], XBOW_STRING, XBOW_LVL[i], FLETCHING, XBOW_XP[i], amount);
		}
	}		
}
