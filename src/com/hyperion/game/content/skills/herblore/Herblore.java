package com.hyperion.game.content.skills.herblore;

import com.hyperion.game.content.skills.SkillItem;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;

public class Herblore extends HerbloreData {

	public static boolean mixing_potions(Player player, int itemUsed, int usedWith) {
		int itemOne = itemUsed;
		int itemTwo = usedWith;
		for (int i = 0; i < 2; i++) {
			if (i == 1) {
				itemOne = usedWith;
				itemTwo = itemUsed;
			}
			for (int j = 0; j < SECONDARY.length; j++) {
				if (itemOne == SECONDARY[j] && itemTwo == UNFINISHED[j]) {
					setHerbloreItem(player, null);
					shopPotionOptions(player, j);
					return true;
				}
			}
			for (int j = 0; j < UNCRUSHED.length; j++) {
				if (itemOne == UNCRUSHED[j] && itemTwo == PESTLE_AND_MORTAR) {
					setHerbloreItem(player, null);
					showGrindOptions(player, j);
					return true;
				}
			}
			for (int j = 0; j < CLEAN_HERBS.length; j++) {
				if (itemOne == CLEAN_HERBS[j] && itemTwo == VIAL_OF_WATER) {
					setHerbloreItem(player, null);
					shopUnfinishedOptions(player, j);
					return true;
				}
			}
		}
		return false;
	}
	
	private static void shopPotionOptions(Player p, int index) {
		String s = "<br><br><br><br>";
		p.getPacketSender().sendChatboxInterface(309);
		p.getPacketSender().itemOnInterface(309, 2, 160, END_POTION[index]);
		p.getPacketSender().modifyText(s + ItemDefinition.forId(END_POTION[index]).getName(), 309, 6);
		p.getAttributes().set("completePotion", index);			
	}

	private static void shopUnfinishedOptions(Player p, int index) {
		String s = "<br><br><br><br>";
		String s1 = index == 6 ? "Spirit weed" : ItemDefinition.forId(CLEAN_HERBS[index]).getName();
		p.getPacketSender().sendChatboxInterface(309);
		p.getPacketSender().itemOnInterface(309, 2, 160, UNFINISHED_POTION[index]);
		p.getPacketSender().modifyText(s + " " + s1 + " potion (unf) ", 309, 6);
		p.getAttributes().set("unfinishedPotion", index);	
	}

	private static void showGrindOptions(Player p, int index) {
		String s = "<br><br><br><br>";
		p.getPacketSender().sendChatboxInterface(309);
		p.getPacketSender().itemOnInterface(309, 2, 150, CRUSHED[index]);
		p.getPacketSender().modifyText(s + ItemDefinition.forId(CRUSHED[index]).getName(), 309, 6);
		p.getAttributes().set("herbloreGrindItem", index);
	}
	
	public static void completePotion(final Player p, int amount, boolean newMix) {
		if (newMix && p.getAttributes().get("completePotion") == null) {
			return;
		}
		if (!newMix && p.getAttributes().get("herbloreItem") == null) {
			return;
		}
		if (newMix) {
			if (p.getAttributes().get("completePotion") == null) {
				return;
			}
			int index = (Integer) p.getAttributes().get("completePotion");
			p.getAttributes().set("herbloreItem", new Potion(END_POTION[index], UNFINISHED[index], SECONDARY[index], POTION_LEVEL[index], POTION_XP[index], amount));
		}
		final Potion item = (Potion) p.getAttributes().get("herbloreItem");
		if (item == null || p == null || item.getAmount() <= 0) {
			resetAllHerbloreVariables(p);
			return;
		}
		if (!p.getInventory().hasItem(item.getSecondary()) || !p.getInventory().hasItem(item.getUnfinished())) {
			resetAllHerbloreVariables(p);
			return;
		}
		if (p.getSkills().getLevel(Skills.HERBLORE) < item.getLevel()) {
			p.getPacketSender().sendMessage("You need a Herblore level of " + item.getLevel() + " to make this potion.");
			resetAllHerbloreVariables(p);
			return;
		}
		//String s = ItemDefinition.forId(item.getFinished()).getName().replaceAll("(3)", "");
		if (p.getInventory().deleteItem(item.getUnfinished()) && p.getInventory().deleteItem(item.getSecondary())) {
			if (p.getInventory().addItem(item.getFinished())) {
				item.decreaseAmount();
				p.animate(MIX_ANIMATION);
				p.getSkills().addExperience(Skills.HERBLORE, item.getXp());
				p.getPacketSender().sendMessage("You add the ingredient into the vial, you have completed the potion.");
				p.getInterfaceSettings().closeInterfaces(false);
			}
		}
		if (item.getAmount() >= 1) {
			World.getWorld().submit(new Tickable(p, 2) {
				@Override
				public void execute() {
					completePotion(p, item.getAmount(), false);
					this.stop();
				}
			});
		}	
	}
	
	public static void makeUnfinishedPotion(final Player p, int amount, boolean newMix) {
		if (newMix && p.getAttributes().get("unfinishedPotion") == null) {
			return;
		}
		if (!newMix && p.getAttributes().get("herbloreItem") == null) {
			return;
		}
		if (newMix) {
			if (p.getAttributes().get("unfinishedPotion") == null) {
				return;
			}
			int index = (Integer) p.getAttributes().get("unfinishedPotion");
			p.getAttributes().set("herbloreItem", new Potion(UNFINISHED_POTION[index], -1, CLEAN_HERBS[index], -1, -1, amount));
		}
		final Potion item = (Potion) p.getAttributes().get("herbloreItem");
		if (!p.getInventory().hasItem(item.getSecondary()) || !p.getInventory().hasItem(VIAL_OF_WATER)) {
			return;
		}
		if (p.getInventory().deleteItem(VIAL_OF_WATER) && p.getInventory().deleteItem(item.getSecondary())) {
			if (p.getInventory().addItem(item.getFinished())) {
				item.decreaseAmount();
				p.animate(MIX_ANIMATION);
				p.getPacketSender().sendMessage("You mix the " + ItemDefinition.forId(item.getSecondary()).getName() + " into a vial of water.");
				p.getInterfaceSettings().closeInterfaces(false);
			}
		}
		if (item.getAmount() >= 1) {
			World.getWorld().submit(new Tickable(p, 2) {
				@Override
				public void execute() {
					makeUnfinishedPotion(p, item.getAmount(), false);
					this.stop();
				}
			});
		}	
	}
	
	public static void grindIngredient(final Player p, int amount, boolean newGrind) {
		if (newGrind && p.getAttributes().get("herbloreGrindItem") == null) {
			return;
		}
		if (!newGrind && p.getAttributes().get("herbloreItem") == null) {
			return;
		}
		if (newGrind) {
			if (p.getAttributes().get("herbloreGrindItem") == null) {
				return;
			}
			int index = (Integer) p.getAttributes().get("herbloreGrindItem");
			p.getAttributes().set("herbloreItem", new SkillItem(CRUSHED[index], UNCRUSHED[index], -1, -1, -1, -1, amount));
		}
		final SkillItem item = (SkillItem) p.getAttributes().get("herbloreItem");
		if (!p.getInventory().hasItem(PESTLE_AND_MORTAR)) {
			p.getPacketSender().sendMessage("You do not have a Pestle and mortar.");
			return;
		}
		if (p.getInventory().replaceItem(item.getItemOne(), item.getFinishedItem())) {
			item.decreaseAmount();
			p.animate(GRIND_ANIMATION);
			p.getPacketSender().sendMessage("You grind the " + ItemDefinition.forId(item.getItemOne()).getName() + " into dust.");
			p.getInterfaceSettings().closeInterfaces(false);
		}
		if (item.getAmount() >= 1) {
			World.getWorld().submit(new Tickable(p, 2) {
				@Override
				public void execute() {
					grindIngredient(p, item.getAmount(), false);
					this.stop();
				}
			});
		}
	}

	private static void identifyHerb(Player p, int herb) {
		if (p.getAttributes().get("identifyHerbTimer") != null) {
			if (System.currentTimeMillis() - (Long) p.getAttributes().get("identifyHerbTimer") < 250) {
				return;
			}
		}
		if (p.getSkills().getLevel(Skills.HERBLORE) < HERB_LVL[herb]) {
			p.getPacketSender().sendMessage("You need a Herblore level of " + HERB_LVL[herb] + " to clean this herb.");
			return;
		}
		if (p.getInventory().replaceItem(GRIMY_HERBS[herb], CLEAN_HERBS[herb])) {
			p.getPacketSender().sendMessage("You cleaned the " + HERB_NAME[herb] + ".");
			p.getSkills().addExperience(Skills.HERBLORE, HERB_XP[herb]);
			p.getAttributes().set("identifyHerbTimer", (Long) System.currentTimeMillis());
		}	
	}
	
	public static boolean idHerb(Player p, int item) {
		for (int j = 0; j < GRIMY_HERBS.length; j++) {
			if (item == GRIMY_HERBS[j]) {
				identifyHerb(p, j);
				setHerbloreItem(p, null);
				return true;
			}
		}
		return false;
	}
	
	private static void resetAllHerbloreVariables(Player p) {
		p.getAttributes().remove("herbloreGrindItem");
		p.getAttributes().remove("fillingVials");
		p.getAttributes().remove("unfinishedPotion");
		p.getAttributes().remove("completePotion");
	}

	public static void setHerbloreItem(Player p, Object a) {
		if (a == null) {
			resetAllHerbloreVariables(p);
			p.getAttributes().remove("herbloreItem");
			return;
		}
		p.getAttributes().set("herbloreItem", (Object) a);
	}
	
	public static void clean_herb(Player player, int id) {
		
	}
	
	public static boolean is_cleaning(Player player, int id) {
		for (int i = 0; i < GRIMY_HERBS.length; i++) {
			if (id == GRIMY_HERBS[i]) {
				int required_lvl = HERB_LVL[i];
				int new_id = CLEAN_HERBS[i];
				if (player.getSkills().getLevelForXp(Skills.HERBLORE) >= required_lvl) {
					player.getPacketSender().sendMessage("You cleaned the " + ItemDefinition.forId(id).getName() + " into a " + ItemDefinition.forId(new_id).getName());
					player.getInventory().deleteItem(id);
					player.getInventory().addItem(new_id);
					player.getInventory().refresh();
					player.getSkills().addExperience(Skills.HERBLORE, HERB_XP[i]);
					return true;
				} else {
					player.getPacketSender().sendMessage("You need a Herblore level of " + required_lvl + " to clean this herb.");
					return false;
				}
			}
		}
		return false;
	}
	
	public static boolean mixDoses(Player p, int itemUsed, int usedWith, int usedSlot, int withSlot) {
		int ONE = 0, TWO = 1, THREE = 2, FOUR = 3;
		for (int i = 0; i < DOSES.length; i++) {
			for (int j = 0; j < DOSES[ONE].length; j++) {
				if (itemUsed == DOSES[ONE][j] && usedWith == DOSES[ONE][j]) {
					p.getInventory().replaceItem(DOSES[ONE][j], VIAL);
					p.getInventory().replaceItem(DOSES[ONE][j], DOSES[TWO][j]);
					return true;
				}
				if (itemUsed == DOSES[TWO][j] && usedWith == DOSES[TWO][j]) {
					p.getInventory().replaceItem(DOSES[TWO][j], VIAL);
					p.getInventory().replaceItem(DOSES[TWO][j], DOSES[FOUR][j]);
					return true;
				}
				if (itemUsed == DOSES[THREE][j] && usedWith == DOSES[THREE][j]) {
					p.getInventory().replaceItem(DOSES[THREE][j], DOSES[TWO][j]);
					p.getInventory().replaceItem(DOSES[THREE][j], DOSES[FOUR][j]);
					return true;
				}
				if (itemUsed == DOSES[ONE][j] && usedWith == DOSES[TWO][j]) {
					p.getInventory().replaceItem(DOSES[ONE][j], VIAL);
					p.getInventory().replaceItem(DOSES[TWO][j], DOSES[THREE][j]);
					return true;
				}
				if (itemUsed == DOSES[TWO][j] && usedWith == DOSES[ONE][j]) {
					p.getInventory().replaceItem(DOSES[TWO][j], VIAL);
					p.getInventory().replaceItem(DOSES[ONE][j], DOSES[THREE][j]);
					return true;
				}
				if (itemUsed == DOSES[ONE][j] && usedWith == DOSES[THREE][j]) {
					p.getInventory().replaceItem(DOSES[ONE][j], VIAL);
					p.getInventory().replaceItem(DOSES[THREE][j], DOSES[FOUR][j]);
					return true;
				}
				if (itemUsed == DOSES[THREE][j] && usedWith == DOSES[ONE][j]) {
					p.getInventory().replaceItem(DOSES[THREE][j], VIAL);
					p.getInventory().replaceItem(DOSES[ONE][j], DOSES[FOUR][j]);
					return true;
				}
				if (itemUsed == DOSES[TWO][j] && usedWith == DOSES[THREE][j]) {
					p.getInventory().replaceItem(DOSES[TWO][j], DOSES[ONE][j]);
					p.getInventory().replaceItem(DOSES[THREE][j], DOSES[FOUR][j]);
					return true;
				}
				if (itemUsed == DOSES[THREE][j] && usedWith == DOSES[TWO][j]) {
					p.getInventory().replaceItem(DOSES[THREE][j], DOSES[ONE][j]);
					p.getInventory().replaceItem(DOSES[TWO][j], DOSES[FOUR][j]);
					return true;
				}
			}
		}
		return false;
	}
}
