package com.hyperion.game.content.skills.magic;

import com.hyperion.game.content.skills.magic.data.BoltsEnchantData;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;

public class BoltsEnchanting extends BoltsEnchantData {

	public BoltsEnchanting() {
		
	}
	
	private static int getIndex(int button) {
		switch(button) {
		case 93://opals
			return 0;
		case 111://sapphires
			return 1;
		case 98://jades
			return 2;
		case 103://pearl
			return 3;
		case 115://emerald
			return 4;
		case 107://red topaz
			return 5;
		case 119://ruby
			return 6;
		case 123://diamond
			return 7;
		case 127://dragon
			return 8;
		case 131://onyx
			return 9;
		}
		return -1;
	}
	
	public static void handleBoltsEnchanting(Player player, int button) {
		int index = getIndex(button);
		if (index == -1) {
			return;
		}
		if (player.getSkills().getLevel(Skills.MAGIC) < makingData[index][2]) {
			player.getPacketSender().sendMessage("You need a Magic level of "+makingData[index][2]+" to enchant these bolts.");
			return;
		}
		if (!RuneReplacers.hasEnoughRunes(player, runeReqs[index], true)) {
			return;
		}
		int boltsToEnchant = makingData[index][0];
		int boltsToAdd = makingData[index][1];
		String toEnchantName = ItemDefinition.forId(boltsToEnchant).getName();
		String toAddName = ItemDefinition.forId(boltsToAdd).getName();
		int amount = player.getInventory().getItemAmount(boltsToEnchant);
		if (amount > 0) {//we have bolts to enchant.
			if (player.getInventory().addItem(boltsToAdd, 10)) {
				RuneReplacers.deleteRunes(player, runeReqs[index]);//delete req runes
				player.getInventory().deleteItem(boltsToEnchant, 10);
				player.getPacketSender().sendMessage("You enchant "+toEnchantName+" into some "+toAddName+".");
				player.getInventory().refresh();
			}
		} else {//we dont have the bolts
			ItemDefinition itemNeeded = ItemDefinition.forId(boltsToEnchant);
			player.getPacketSender().sendMessage("You don't have any "+itemNeeded.getName()+" to enchant.");
		}
	}
}
