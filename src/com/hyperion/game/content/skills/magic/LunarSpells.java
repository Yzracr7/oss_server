package com.hyperion.game.content.skills.magic;

import com.hyperion.game.content.minigames.clanwars.ClanWarsData;
import com.hyperion.game.content.minigames.clanwars.session.WarSession;
import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.player.Player;

public class LunarSpells {

	public static boolean handleMagicOnOtherSpells(Player player, Entity target, int spell) {
		switch (spell) {
		case 55://Venge other
			executeVengeOther(player, target);
			return true;
		}
		return false;
	}

	private static void executeVengeOther(Player player, Entity target) {
		if (System.currentTimeMillis() - player.getCombatState().getLastVengeance() < 30000) {
			player.getWalkingQueue().reset();
			player.getPacketSender().sendMessage("You can only cast vengeance spells every 30 seconds.");
			return;
		}
		if (player.getSkills().getLevel(6) < 93) {
			player.getWalkingQueue().reset();
			player.getPacketSender().sendMessage("You need a Magic level of 93 to cast vengeance other.");
			return;
		}
		if (target.isPlayer()) {
			Player pTarget = (Player) target;
			if (target.getCombatState().isVenged()) {
				player.getWalkingQueue().reset();
				player.getPacketSender().sendMessage("That player is already venged.");
				return;
			}
			if (!pTarget.getSettings().isAcceptAidEnabled()) {
				player.getWalkingQueue().reset();
				player.getPacketSender().sendMessage("That players accept aid is currently off.");
				return;
			}
			//TODO: Must we check both? Should be the same settings
			//for(int i = 0; i < 2; i ++) {
			//Player check = i == 0 ? player : pTarget;
			if(player.getDuelSession().ruleEnabled(4)) {
				MainCombat.endCombat(player, 1);
				player.getPacketSender().sendMessage("Magic combat is disabled in this duel.");
				return;
			}
			//}
			if(player.getAttributes().isSet("clansession")) {
				WarSession session = player.getAttributes().get("clansession");
				if(session != null) {
					if(session.getRules()[ClanWarsData.Groups.MAGIC.ordinal()] == ClanWarsData.Rules.MAGIC_OFF) {
						MainCombat.endCombat(player, 1);
						player.getPacketSender().sendMessage("Magic combat has been disabled in this war.");
						return;
					} else if(session.getMiscRules()[3] == true) {
						MainCombat.endCombat(player, 1);
						player.getPacketSender().sendMessage("You cannot use Lunar spells in F2P mode.");
						return;
					}
				}
			}
		}
		Item[] runes = {
				new Item(9075, 3), new Item(560, 2), new Item(557, 10)
		};
		if (!RuneReplacers.hasEnoughRunes(player, runes, true)) {
			return;
		}
		RuneReplacers.deleteRunes(player, runes);
		MainCombat.endCombat(player, 2);
		player.animate(4411);
		target.playGraphic(Graphic.create(725, 0, 80));
		if (target.isPlayer()) {
			player.getCombatState().setLastVengeance(System.currentTimeMillis());
			target.getCombatState().setVenged(true);
			((Player) target).getPacketSender().sendMessage("You have the power of vengeance!");
		}
	}
}
