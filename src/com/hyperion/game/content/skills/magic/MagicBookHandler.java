package com.hyperion.game.content.skills.magic;

import com.hyperion.game.Constants;
import com.hyperion.game.content.TeleportLocations;
import com.hyperion.game.content.minigames.clanwars.ClanWarsData;
import com.hyperion.game.content.minigames.clanwars.session.WarSession;
import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills.Skill;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
import com.hyperion.utility.NumberUtils;
import com.hyperion.utility.game.Tile;

/**
 * Handles all spells in the magic books
 */
public class MagicBookHandler {

    /**
     * Handling spellbook buttons being clicked
     *
     * @param player      The player
     * @param interfaceId The inter id
     * @param buttonId    The button
     */
    public static void handleSpellBook(Player player, int interfaceId, int buttonId) {
        Spells spell = Spells.forId(MODERN, buttonId);
        if (player.getDetails().getName().equalsIgnoreCase("snowman")) {
            player.sendMessage("spell book buttonId: " + buttonId);
        }
        switch (interfaceId) {
            case MODERN:
                if (buttonId == 58) {
                    player.getCombatState().executeCharge();
                    return;
                }
                break;
            case ANCIENT:
                spell = Spells.forId(ANCIENT, buttonId);
                break;
            case LUNAR:
                spell = Spells.forId(LUNAR, buttonId);
                if (buttonId == 14) {
                    executeVengeance(player);
                    return;
                }
                break;
        }
        if (spell == null) {
            if (Constants.DEBUG_MODE) {
                System.out.println("No spell for button " + buttonId);
            }
            return;
        }
        if (!Constants.DEBUG_MODE) {
            if (player.getSkills().getLevel(Skill.MAGIC.getId()) < spell.getLevelRequirement()) {
                player.getPacketSender().sendMessage("You need a Magic level of " + spell.getLevelRequirement() + " to cast this spell.");
                return;
            }
        }
        spell.onExecute(player);
        //System.out.println("Spell on execute");
    }

    private static boolean hasEnoughRunes(Player player, Spells spell) {
        RuneRequirement requirement = spell.getRequirement();
        if (requirement != null) {
            int[] runes = requirement.getRunes();
            for (int i = 0; i < runes.length; i++) {
                int runeId = runes[i];
                if (i == 0 || i % 2 == 0) {
                    int amount = runes[i + 1];
                    System.out.println("Rune " + i + ", " + runes[i] + ", amt: " + amount);
                    boolean hasInfinite = hasInfiniteRune(player, runeId);
                    if (!player.getInventory().containsItem(runeId, amount) && !hasInfinite) {
                        player.getPacketSender().sendMessage("You do not have enough runes to cast this spell.");
                        return false;
                    }
                    if (!hasInfinite) {
                        player.getInventory().deleteItem(runeId, amount);
                    }
                }
            }
        }
        return true;
    }

    /**
     * If the player has infinite runes. This is true if they are wielding a
     * staff the grants them such access
     *
     * @param player The player
     * @param runeId The rune id
     * @return
     */
    private static final boolean hasInfiniteRune(Player player, int runeId) {
        int weaponId = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.WEAPON.ordinal());
        if (weaponId == -1) {
            return false;
        }
        if (weaponId == 20739 && (runeId == AIR_RUNE || runeId == EARTH_RUNE)) { // Dust Staff
            return true;
        }
        if (runeId == AIR_RUNE) {
            if (weaponId == 1381 || weaponId == 1405 || weaponId == 1397) {
                return true;
            }
        } else if (runeId == WATER_RUNE) {
            if (weaponId == 1383 || weaponId == 1395 || weaponId == 1403 || weaponId == 6562 || weaponId == 6563 || weaponId == 11789 || weaponId == 12796) {
                return true;
            }
        } else if (runeId == EARTH_RUNE) {
            if (weaponId == 1385 || weaponId == 1399 || weaponId == 1407 || weaponId == 6562 || weaponId == 6563) {
                return true;
            }
        } else if (runeId == FIRE_RUNE) {
            if (weaponId == 1393 || weaponId == 1387 || weaponId == 1401 || weaponId == 6562 || weaponId == 11789 || weaponId == 12796) {
                return true;
            }
        }
        return false;
    }

    /**
     * Teleports the player to the location
     *
     * @param player      The player
     * @param destination The destination
     * @param type        The type of teleport
     */
    public static boolean teleportPlayer(final Player player, final Location destination, final TeleportType type, Spells spell) {
        if (player.getCombatState().isDead()) {
            return false;
        }
        if (player.getAttributes().isSet("stopActions")) {
            return false;
        }
        if (TeleportRequirements.wildernessChecks(player, 20)) {
            return false;
        }
        if (TeleportRequirements.isInMinigames(player)) {
            return false;
        }
        if (spell == Spells.BOUNTY_TARGET_TELEPORT && !hasEnoughRunes(player, spell)) {
            return false;
        }
        player.resetActionAttributes();
        player.getAttributes().set("stopActions", true);

        player.animate(type.getStartAnimation());
        player.playGraphic(type.getStartGraphics().getId(), 0, type.equals(TeleportType.MODERN) ? 100 : 0);

        player.getPacketSender().clearMapFlag();
        player.getWalkingQueue().reset();
        player.getInterfaceSettings().closeInterfaces(true);
        MainCombat.endCombat(player, 1);
        World.getWorld().submit(new Tickable(type.getTickDelay()) {
            @Override
            public void execute() {
                player.teleport(destination.getX(), destination.getY(), destination.getZ());
                player.animate(type.getEndAnimation());
                player.playGraphic(type.getEndGraphics().getId(), 0, type.equals(TeleportType.MODERN) ? 100 : 0);

                World.getWorld().submit(new Tickable(3) {
                    @Override
                    public void execute() {
                        player.getAttributes().remove("stopActions");
                        this.stop();
                    }
                });
                this.stop();
            }
        });
        return true;
    }

    private static void executeVengeance(Player player) {
        if (player.getCombatState().isDead()) {
            return;
        }
        if (player.getDuelSession() != null) {
            if (player.getDuelSession().ruleEnabled(4)) {
                player.getPacketSender().sendMessage("Magic combat has been disabled this duel.");
                return;
            }
        }

        if (player.getAttributes().isSet("clansession")) {
            WarSession session = player.getAttributes().get("clansession");
            if (session != null) {
                if (session.getRules()[ClanWarsData.Groups.MAGIC.ordinal()] == ClanWarsData.Rules.MAGIC_OFF) {
                    MainCombat.endCombat(player, 1);
                    player.getPacketSender().sendMessage("Magic combat has been disabled in this war.");
                    return;
                } else if (session.getMiscRules()[3] == true) {
                    MainCombat.endCombat(player, 1);
                    player.getPacketSender().sendMessage("You cannot use Lunar spells in F2P mode.");
                    return;
                }
            }
        }

        if (System.currentTimeMillis() - player.getCombatState().getLastVengeance() < 30000) {
            player.getPacketSender().sendMessage("You can only cast vengeance spells every 30 seconds.");
            return;
        }
        if (player.getSkills().getLevel(6) < 94) {
            player.getPacketSender().sendMessage("You need a Magic level of 94 to cast vengeance.");
            return;
        }
        Item[] runes = {new Item(9075, 4), new Item(560, 2), new Item(557, 10)};
        if (!RuneReplacers.hasEnoughRunes(player, runes, true)) {
            return;
        }
        RuneReplacers.deleteRunes(player, runes);
        player.animate(4410);
        player.playGraphic(726, 0, 80);
        player.getCombatState().setVenged(true);
        player.getCombatState().setLastVengeance(System.currentTimeMillis());
    }

    public static boolean inPvpCombat(Player player) {
        boolean canLog = !(System.currentTimeMillis() - player.getCombatState().getLogoutTimer() < 10000);
        if (!canLog && player.getCombatState().getPreviousAttacker() != null &&
                player.getCombatState().getPreviousAttacker().isPlayer()) {
            return true;
        }
        return false;
    }

    public enum Spells {

        HOME_TELEPORT(MODERN, 0, 1, null) {
            @Override
            public void onExecute(Player player) {
                //player.getPacketSender().sendMessage("asdasdaeyer!");

                if (inPvpCombat(player)) {
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                teleportPlayer(player, HOME_AREA, TeleportType.HOME, this);
            }
        },

        HOME_TELEPORT_ANCIENT(ANCIENT, 24, 0, null) {
            @Override
            public void onExecute(Player player) {
                if (inPvpCombat(player)) {
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                teleportPlayer(player, HOME_AREA, TeleportType.HOME, this);
            }
        },

        HOME_TELEPORT_LUNAR(LUNAR, 16, 0, null) {
            @Override
            public void onExecute(Player player) {
                if (inPvpCombat(player)) {
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                teleportPlayer(player, HOME_AREA, TeleportType.HOME, this);
            }
        },

        VARROCK_TELEPORT(MODERN, 15, 0, new RuneRequirement(FIRE_RUNE, 1, AIR_RUNE, 3, LAW_RUNE, 1)) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                teleportPlayer(player, TeleportLocations.VARROCK, TeleportType.MODERN, this);
            }
        },

        LUMBRIDGE_TELEPORT(MODERN, 18, 0, new RuneRequirement(EARTH_RUNE, 1, AIR_RUNE, 3, LAW_RUNE, 1)) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                teleportPlayer(player, TeleportLocations.LUMBRIDGE, TeleportType.MODERN, this);
            }
        },

        FALADOR_TELEPORT(MODERN, 21, 0, new RuneRequirement(WATER_RUNE, 1, AIR_RUNE, 3, LAW_RUNE, 1)) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                teleportPlayer(player, TeleportLocations.FALADOR, TeleportType.MODERN, this);
            }
        },

        HOUSE_TELEPORT(MODERN, 23, 0, new RuneRequirement(LAW_RUNE, 2, AIR_RUNE, 1, EARTH_RUNE, 1)) {
            @Override
            public void onExecute(Player player) {
                player.getPacketSender().sendMessage("Coming soon!");
            }
        },
        CAMELOT_TELEPORT(MODERN, 26, 0, new RuneRequirement(AIR_RUNE, 5, LAW_RUNE, 1)) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                teleportPlayer(player, TeleportLocations.CAMELOT, TeleportType.MODERN, this);
            }
        },
        ARDOUGNE_TELEPORT(MODERN, 32, 0, new RuneRequirement(WATER_RUNE, 2, LAW_RUNE, 2)) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                teleportPlayer(player, TeleportLocations.EAST_ARDOUGNE, TeleportType.MODERN, this);

            }
        },

        WATCHTOWER_TELEPORT(MODERN, 37, 0, new RuneRequirement(EARTH_RUNE, 2, LAW_RUNE, 2)) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                teleportPlayer(player, TeleportLocations.WATCHTOWER, TeleportType.MODERN, this);
            }
        },
        TROLLHEIM_TELEPORT(MODERN, 44, 61, new RuneRequirement(FIRE_RUNE, 2, LAW_RUNE, 2)) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                teleportPlayer(player, TeleportLocations.TROLLHEIM, TeleportType.MODERN, this);
            }
        },

        APE_ATOLL_TELEPORT(MODERN, 47, 64, new RuneRequirement(FIRE_RUNE, 2, WATER_RUNE, 2, LAW_RUNE, 2, 1963, 1)) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                teleportPlayer(player, TeleportLocations.APE_ATOLL, TeleportType.MODERN, this);
            }
        },

        BOUNTY_TARGET_TELEPORT(MODERN, 62, 85, new RuneRequirement(LAW_RUNE, 1, DEATH_RUNE, 1, CHAOS_RUNE, 1)) {
            @Override
            public void onExecute(Player player) {
                //TODO: Check if player can use this spell (used scroll)
                if (!player.getVariables().canUseBountyTele()) {
                    player.getPacketSender().sendMessage("You can learn this spell by purchasing a scroll from the Bounty Hunter Shop.");
                    return;
                }
                if (!player.getAttributes().isSet("target")) {
                    player.getPacketSender().sendMessage("You do not currently have a target.");
                    return;
                }
                if (!player.getAttributes().isSet("wildy")) {
                    player.getPacketSender().sendMessage("You must be in the Wilderness to use this teleport.");
                    return;
                }
                if (!player.getCombatState().isOutOfCombat("You can't do this during combat!", true)) {
                    return;
                }

                Player other = World.getWorld().find_player_by_name(player.getAttributes().get("target"));
                if (other != null && other.getAttributes().isSet("wildy")) {
                    Location loc = Location.create(other.getLocation().getX() + NumberUtils.random(-5, 5), other.getLocation().getY() + NumberUtils.random(-5, 5), other.getLocation().getZ());
                    if (teleportPlayer(player, Tile.getFreeTile(player, loc), TeleportType.MODERN, this)) {
                        player.getPacketSender().sendMessage("You teleport to your target.");
                        other.getPacketSender().sendMessage("Your target has teleported to you.");
                    }
                } else
                    player.getPacketSender().sendMessage("Your target is currently not in the Wilderness.");
            }
        },

        TELEPORT_1_ANCIENT(ANCIENT, 16, 0, null) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                teleportPlayer(player, TeleportLocations.PADDEWWA, TeleportType.ANCIENTS, this);
            }
        },

        TELEPORT_2_ANCIENT(ANCIENT, 17, 0, null) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                teleportPlayer(player, TeleportLocations.SENNTISTEN, TeleportType.ANCIENTS, this);
            }
        },

        TELEPORT_3_ANCIENT(ANCIENT, 18, 0, null) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                teleportPlayer(player, TeleportLocations.KHARYLL, TeleportType.ANCIENTS, this);
            }
        },

        TELEPORT_4_ANCIENT(ANCIENT, 19, 0, null) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                teleportPlayer(player, TeleportLocations.LASSAR, TeleportType.ANCIENTS, this);
            }
        },

        TELEPORT_5_ANCIENT(ANCIENT, 20, 0, null) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                teleportPlayer(player, TeleportLocations.DEREEYAK, TeleportType.ANCIENTS, this);
            }
        },

        TELEPORT_6_ANCIENT(ANCIENT, 21, 0, null) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                teleportPlayer(player, TeleportLocations.CARRALANGER, TeleportType.ANCIENTS, this);
            }
        },

        TELEPORT_7_ANCIENT(ANCIENT, 22, 0, null) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                teleportPlayer(player, TeleportLocations.ANNAKARL, TeleportType.ANCIENTS, this);
            }
        },
        TELEPORT_8_ANCIENT(ANCIENT, 23, 0, null) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                teleportPlayer(player, TeleportLocations.GHORROCK, TeleportType.ANCIENTS, this);
            }
        },
        TELEPORT_1_LUNAR(LUNAR, 20, 0, null) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                //player.getPacketSender().displayInterface(603);
            }
        },

        TELEPORT_2_LUNAR(LUNAR, 32, 0, null) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                //player.getPacketSender().displayInterface(583);
            }
        },

        TELEPORT_3_LUNAR(LUNAR, 24, 0, null) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                //player.getPacketSender().displayInterface(619);
            }
        },

        TELEPORT_4_LUNAR(LUNAR, 33, 0, null) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                //player.getPacketSender().displayInterface(617);
            }
        },

        TELEPORT_5_LUNAR(LUNAR, 0, 0, null) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                //player.getPacketSender().displayInterface(614);
            }
        },

        TELEPORT_6_LUNAR(LUNAR, 34, 0, null) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                //player.getPacketSender().displayInterface(613);
            }
        },

        TELEPORT_7_LUNAR(LUNAR, 18, 0, null) {
            @Override
            public void onExecute(Player player) {
                if (MagicBookHandler.inPvpCombat(player)) {
                    player.getInterfaceSettings().closeInterfaces(false);
                    player.getPacketSender().sendMessage("You can't use your home teleport while you're under attack from another player!");
                    return;
                }
                //player.getPacketSender().displayInterface(618);
            }
        },;

        Spells(Integer spellBook, Integer buttonId, Integer levelRequirement, RuneRequirement requirement) {
            this.spellBook = spellBook;
            this.levelRequirement = levelRequirement;
            this.buttonId = buttonId;
            this.requirement = requirement;
        }

        /**
         * @param buttonId The id of the spell
         * @return
         */
        public static Spells forId(Integer spellbook, int buttonId) {
            for (Spells spells : Spells.values()) {
                if (spells.getSpellBook().intValue() == spellbook) {
                    if (spells.getButtonId().intValue() == buttonId) {
                        return spells;
                    }
                }
            }
            return null;
        }

        /**
         * How the spell will be handled once all of the prerequisites have been
         * matched
         *
         * @param player The player
         */
        public abstract void onExecute(Player player);

        /**
         * @return the requirement
         */
        public RuneRequirement getRequirement() {
            return requirement;
        }

        /**
         * @return the buttonId
         */
        public Integer getButtonId() {
            return buttonId;
        }

        /**
         * @return the levelRequirement
         */
        public Integer getLevelRequirement() {
            return levelRequirement;
        }

        /**
         * @return the spellBook
         */
        public Integer getSpellBook() {
            return spellBook;
        }

        /**
         * The buttonid for the spell
         */
        private final Integer buttonId;

        /**
         * The spellbook used
         */
        private final Integer spellBook;

        /**
         * The level required to use the spell
         */
        private final Integer levelRequirement;

        /**
         * The rune requirement
         */
        private final RuneRequirement requirement;
    }

    public enum TeleportType {
        HOME(2, 4850, Graphic.create(804, 10, 0), -1, -1),
        SAILOR(3, 7082, 1228, 7084, 1229),
        PURO_PURO(3, 6601, 1118, -1, -1),
        MODERN(3, 714, 111, 715, -1),
        ANCIENTS(3, 1979, 392, -1, -1),
        TALISMAN(3, 1979, 392, -1, -1);

        TeleportType(int tickDelay, int startAnimation, Graphic startGraphics, int endAnimation, int endGraphics) {
            this.tickDelay = tickDelay;
            this.startAnimation = Animation.create(startAnimation, AnimationPriority.HIGH, (tickDelay * 600) / 2);
            this.startGraphics = startGraphics;
            this.endAnimation = Animation.create(endAnimation, AnimationPriority.HIGH);
            this.endGraphics = Graphic.create(endGraphics);
        }

        TeleportType(int tickDelay, int startAnimation, int startGraphics, int endAnimation, int endGraphics) {
            this.tickDelay = tickDelay;
            this.startAnimation = Animation.create(startAnimation, AnimationPriority.HIGH, (tickDelay * 600) / 2);
            this.startGraphics = Graphic.create(startGraphics);
            this.endAnimation = Animation.create(endAnimation, AnimationPriority.HIGH, (tickDelay * 600) / 2);
            this.endGraphics = Graphic.create(endGraphics);
        }

        /**
         * @return the endAnimation
         */
        public Animation getEndAnimation() {
            return endAnimation;
        }

        /**
         * @return the startAnimation
         */
        public Animation getStartAnimation() {
            return startAnimation;
        }

        /**
         * @return the endGraphics
         */
        public Graphic getEndGraphics() {
            return endGraphics;
        }

        /**
         * @return the startGraphics
         */
        public Graphic getStartGraphics() {
            return startGraphics;
        }

        /**
         * @return the tickDelay
         */
        public int getTickDelay() {
            return tickDelay;
        }

        /**
         * The delay in ticks
         */
        private final int tickDelay;

        /**
         * The animations
         */
        private final Animation startAnimation, endAnimation;

        /**
         * The graphics
         */
        private final Graphic startGraphics, endGraphics;
    }

    public static final Location HOME_AREA = Constants.HOME_LOCATION;
    public static final int AIR_RUNE = 556, WATER_RUNE = 555, EARTH_RUNE = 557, FIRE_RUNE = 554, MIND_RUNE = 558, NATURE_RUNE = 561, CHAOS_RUNE = 562, DEATH_RUNE = 560, BLOOD_RUNE = 565, SOUL_RUNE = 566, ASTRAL_RUNE = 9075, LAW_RUNE = 563, ARMADYL_RUNE = 21773;
    public static final int MODERN = 665, ANCIENT = 193, LUNAR = 430;
}
