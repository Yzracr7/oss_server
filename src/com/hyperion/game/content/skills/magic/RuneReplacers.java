package com.hyperion.game.content.skills.magic;

import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.player.Player;

public class RuneReplacers {

	private static final int[] COMBO_RUNES = { 4694, // Steam rune
			4695, // Mist rune
			4696, // Dust rune
			4697, // Smoke rune
			4698, // Mud rune
			4699, // Lava rune
	};

	private static final int[][] COMBO_REPLACERS = { { 554, 555 },// fires,
																	// waters
			{ 555, 556 },// waters, airs
			{ 557, 556 },// earths, airs
			{ 554, 556 },// fires, airs
			{ 557, 555 },// earths, waters
			{ 554, 557 },// fires, earths
	};

	private static final int[] RUNE_REPLACER = { 1387, // Staff of fire.
			1381, // Staff of air.
			1383, // Staff of water.
			1385, // Staff of earth.
			1393, // Fire battlestaff.
			1395, // Water battlestaff.
			1397, // Air battlestaff.
			1399, // Earth battlestaff.
			1401, // Mystic fire staff.
			1403, // Mystic water staff.
			1405, // Mystic air staff.
			1407, // Mystic earth staff.
			3053, // Lava battlestaff.
			3053, // Lava battlestaff.
			3054, // Mystic lava staff.
			3054, // Mystic lava staff.
			6562, // Mud battlestaff.
			6562, // Mud battlestaff.
			6563, // Mystic mud staff.
			6563, // Mystic mud staff.
			11736, // Steam battlstaff.
			11736, // Steam battlstaff.
			11738, // Mystic steam staff.
			11738, // Mystic steam staff.
			11998, // Smoke battlestaff
	};

	private static final int[] REPLACABLE_RUNES = { 554, // Fire rune.
			556, // Air rune.
			555, // Water rune.
			557, // Earth rune.
			554, // Fire rune.
			555, // Water rune.
			556, // Air rune.
			557, // Earth rune.
			554, // Fire rune.
			555, // Water rune.
			556, // Air rune.
			557, // Earth rune.
			557, // Earth rune.
			554, // Fire rune.
			557, // Earth rune.
			554, // Fire rune.
			555, // Water rune.
			557, // Earth rune.
			555, // Water rune.
			557, // Earth rune.
			554, // Fire rune.
			555, // Water rune.
			554, // Fire rune.
			555, // Water rune.
			556,
	};

	public static boolean hasEnoughRunes(Player player, Item[] runes, boolean sendMessage) {
		for (Item rune : runes) {
			if (checkForStaffs(player, rune.getId()) != -1) {
				int runeId = checkForComboRune(player, rune.getId());
				if (!player.getInventory().hasItemAmount(runeId, rune.getCount())) {
					if (sendMessage) {
						String runeName = rune.getDefinition().getName();
						player.getPacketSender().sendMessage("You do not have enough " + runeName + "s to cast this spell.");
					}
					return false;
				}
			}
		}
		return true;
	}

	private static int checkForStaffs(Player p, int rune) {
		for (int i = 0; i < RUNE_REPLACER.length; i++) {
			if (p.getEquipment().getItemInSlot(3) == RUNE_REPLACER[i]) {
				if (rune == REPLACABLE_RUNES[i]) {
					rune = -1;
					break;
				}
			}
		}
		return rune;
	}

	private static int checkForComboRune(Player player, int rune) {
		for (int i = 0; i < COMBO_RUNES.length; i++) {
			if (player.getInventory().hasItem(COMBO_RUNES[i])) {
				for (Integer runes : COMBO_REPLACERS[i]) {
					if (rune == runes.intValue()) {
						rune = COMBO_RUNES[i];
						break;
					}
				}
			}
		}
		return rune;
	}

	public static boolean deleteRunes(Player player, Item[] runes) {
		for (Item rune : runes) {
			Item runeToCheck = new Item(rune.getId(), rune.getCount());
			if (checkForStaffs(player, rune.getId()) != -1) {
				int runeId = checkForComboRune(player, runeToCheck.getId());
				runeToCheck.setItemAmount(runeId);
				if (!player.getInventory().deleteItem(rune)) {
					return false;
				}
			}
		}
		player.getInventory().refresh();
		return true;
	}
}