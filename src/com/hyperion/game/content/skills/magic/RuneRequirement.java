package com.hyperion.game.content.skills.magic;


public class RuneRequirement {

	public RuneRequirement(int... runes) {
		this.runes = runes;
	}

	/**
	 * @return the runes
	 */
	public int[] getRunes() {
		return runes;
	}

	/**
	 * The runes required for a spell
	 */
	private final int[] runes;

}
