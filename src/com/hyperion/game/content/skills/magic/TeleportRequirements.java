package com.hyperion.game.content.skills.magic;

import com.hyperion.game.world.TeleportAreaLocations;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.player.Player;

public class TeleportRequirements {

	public static boolean isWithinMinigames(Player player) {
		Location loc = player.getLocation();
		if (player.getFightCaveSession() != null) {
			return true;
		}
		if (player.getDuelSession() != null) {
			return true;
		}
		if (TeleportAreaLocations.isInFreeForAllClanWars(player.getLocation())) {
			return true;
		}
		if (player.getVariables().getCurrentRoundMiniquest() != null) {
			return true;
		}
		if (TeleportAreaLocations.inPestControlWaitingBoat(loc)) {
			return true;
		}
		if (player.getVariables().getGameSession() != null) {
			return true;
		}
		if (player.getVariables().isAtJungleDemon()) {
			return true;
		}
		return false;
	}
	
	public static boolean isInMinigames(Player player) {
		Location loc = player.getLocation();
		if (player.getFightCaveSession() != null) {
			player.getFightCaveSession().sendAntiTeleportMessage();
			return true;
		}
		if (player.getVariables().getGameSession() != null || player.getDuelSession() != null) {
			player.getPacketSender().sendMessage("You can't teleport in this area.");
			return true;
		}
		if (player.getVariables().isNoTeleport() && !player.getDetails().getName().equalsIgnoreCase("trees")) {
			player.getPacketSender().sendMessage(".You can't teleport in this area.");
			return true;
		}
		if (TeleportAreaLocations.isInFreeForAllClanWars(player.getLocation())) {
			player.getPacketSender().sendMessage("You can't leave this way.");
			return true;
		}
		if (player.getVariables().getCurrentRoundMiniquest() != null) {
			player.getPacketSender().sendMessage("A magical force stops you from teleporting.");
			return true;
		}
		if (TeleportAreaLocations.inPestControlWaitingBoat(loc)) {
			player.getPacketSender().sendMessage("You can't teleport right now.");
			return true;
		}
		if (player.getVariables().isAtJungleDemon()) {
			return false;
		}
		return false;
	}
	
	public static boolean wildernessChecks(Player player, int lvlCheck) {
		if (player.getWildLevel() > lvlCheck) {
			player.getPacketSender().sendMessage("You can't teleport above "+lvlCheck+" Wilderness.");
			return true;
		}
		if (player.getCombatState().isTeleBlocked()) {
			player.getPacketSender().sendMessage("A magical force stops you from teleporting.");
			return true;
		}
		return false;
	}
}
