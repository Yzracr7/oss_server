package com.hyperion.game.content.skills.magic;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.player.Player;

public class TeleportTablets {

    private enum Tabs {
        HOME_TAB(8013, MagicBookHandler.HOME_AREA),
        CAMELOT_TAB(8010, Location.create(2726, 3485, 0)),
        VARROCK_TAB(8007, Location.create(3212, 3423, 0)),
        FALADOR_TAB(8009, Location.create(2965, 3378, 0)),
        LUMBY_TAB(8008, Location.create(3221, 3219, 0)),
        ELF_HUT(6102, Location.create(2202, 3253, 0));
        private int id;
        private Location teleLocation;

        private Tabs(int id, Location teleLocation) {
            this.id = id;
            this.teleLocation = teleLocation;
        }

        public int getId() {
            return id;
        }

        public Location getTeleLocation() {
            return teleLocation;
        }
    }

    public static boolean executeTeleportTablet(final Player player, int item) {
        Tabs tab = null;
        for (Tabs tabs : Tabs.values()) {
            if (tabs.getId() == item) {
                tab = tabs;
            }
        }
        if (tab == null) {
            return false;
        }
        if (player.getCombatState().isDead()) {
            return false;
        }
        if (player.getAttributes().isSet("stopActions")) {
            return false;
        }
        if (TeleportRequirements.wildernessChecks(player, 20)) {
            return false;
        }
        if (TeleportRequirements.isInMinigames(player)) {
            return false;
        }
        if (player.getInventory().deleteItem(item, 1)) {
            player.getInventory().refresh();
            player.resetActionAttributes();
            player.getAttributes().set("stopActions", true);
            player.getInterfaceSettings().closeInterfaces(false);
            MainCombat.endCombat(player, 1);
            player.getPacketSender().clearMapFlag();
            player.animate(4731);
            player.playGraphic(678, 0, 0);
            final Tabs tablet = tab;
            World.getWorld().submit(new Tickable(2) {
                @Override
                public void execute() {
                    this.stop();
                    player.teleport(tablet.getTeleLocation());
                    player.animate(-1);
                    World.getWorld().submit(new Tickable(2) {
                        @Override
                        public void execute() {
                            this.stop();
                            player.getAttributes().remove("stopActions");
                        }
                    });
                }
            });
            return true;
        }
        return false;
    }
}
