package com.hyperion.game.content.skills.magic.data;

import com.hyperion.game.item.Item;

public class BoltsEnchantData {

	protected static final int[][] makingData = {
		//bolts id, enchanted bolts id, magic req, exp 
		{879, 9236, 4, 9},//opal
		{9337, 9240, 7, 17},//sapphire
		{9335, 9237, 14, 19},//jade
		{880, 9238, 24, 29},//pearl
		{9338, 9241, 27, 37},//emerald
		{9336, 9239, 29, 33},//topaz
		{9339, 9242, 49, 59},//ruby
		{9340, 9243, 57, 67},//diamond
		{9341, 9244, 68, 78},//dragon
		{9342, 9245, 87, 97},//onyx
	};
	
	protected static final Item[][] runeReqs = {
		new Item[]{new Item(564, 1), new Item(556, 2)},//opal
		new Item[]{new Item(564, 1), new Item(555, 1), new Item(558, 1)},//sapphire
		new Item[]{new Item(564, 1), new Item(557, 2)},//jade
		new Item[]{new Item(564, 1), new Item(555, 2)},//pearl
		new Item[]{new Item(564, 1), new Item(556, 3), new Item(561, 1)},//emerald
		new Item[]{new Item(564, 1), new Item(554, 2)},//topaz
		new Item[]{new Item(564, 1), new Item(554, 5), new Item(565, 1)},//ruby
		new Item[]{new Item(564, 1), new Item(557, 10), new Item(563, 2)},//diamond
		new Item[]{new Item(564, 1), new Item(557, 15), new Item(566, 1)},//dragonstone
		new Item[]{new Item(564, 1), new Item(554, 20), new Item(560, 1)},//onyx
	};
}