package com.hyperion.game.content.skills.magic.onitems;

import com.hyperion.game.content.skills.magic.RuneReplacers;
import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;

public abstract class Alchemy {

	/**
	 * req lvl
	 */
	public abstract int requiredLevel();
	
	/**
	 * Required runes
	 */
	public abstract Item[] runes();
	
	/**
	 * animation
	 */
	public abstract int startAnim();
	
	/**
	 * graphic
	 */
	public abstract int startGraphic();
	
	/**
	 * exp this spell gives
	 */
	public abstract double exp();
	
	/**
	 * gets price of alch
	 * @param item
	 * @return
	 */
	public abstract int alchPrice(Item item);
	
	/**
	 * Alchs the item
	 * @param player
	 * @param item
	 */
	public void execute(final Player player, Item item) {
		if (item.getDefinition().isPlayerBound() || !item.getDefinition().isTradeable()) {
			return;
		}
		if (System.currentTimeMillis() - player.getVariables().getLastAction() < 2000) {
			return;
		}
		player.getPacketSender().forceSendTab(6);
		if (player.getSkills().getLevel(6) < requiredLevel()) {
			player.getPacketSender().sendMessage("You need a Magic level of "+requiredLevel()+" to use this spell.");
			return;
		}
		if (item.getDefinition().getName().equalsIgnoreCase("coins")) {
			player.getPacketSender().sendMessage("You can't alchemy coins!");
			return;
		}
		if (player.getInventory().hasEnoughRoomFor(995)) {
			if (!RuneReplacers.hasEnoughRunes(player, runes(), true)) {
				return;
			}
			int price = alchPrice(item);
			String name = item.getDefinition().getName().toLowerCase();
			if (player.getDetails().isDonator()) {
				if (name.equals("magic longbow") || name.equals("yew longbow")) {
					price *= 2;
				}
			}
			player.getAttributes().set("stopActions", true);
			player.getVariables().setLastAction(System.currentTimeMillis());
			player.getSkills().addExperience(Skills.MAGIC, exp());
			player.getInventory().addItem(new Item(995, price));
			player.getInventory().deleteItem(item.getId(), 1);
			RuneReplacers.deleteRunes(player, runes());
			player.animate(startAnim());
			player.playGraphic(startGraphic(),0,100);
			player.getInventory().refresh();
			World.getWorld().submit(new Tickable(2) {
				@Override
				public void execute() {
					this.stop();
					player.getAttributes().remove("stopActions");
				}
			});
		}
	}
}