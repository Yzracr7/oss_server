package com.hyperion.game.content.skills.magic.onitems;

import com.hyperion.game.content.skills.magic.RuneReplacers;
import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.player.Player;

public abstract class EnchantingSpells {

	/**
	 * Spell name
	 */
	public abstract String name();
	
	/**
	 * req lvl
	 */
	public abstract int requiredLevel();
	
	/**
	 * Required runes
	 */
	public abstract Item[] runes();
	
	/**
	 * animation
	 */
	public abstract int startAnim();
	
	/**
	 * graphic
	 */
	public abstract int startGraphic();
	
	/**
	 * exp this spell gives
	 */
	public abstract double exp();
	
	/**
	 * Items this spell can use on
	 * @return
	 */
	public abstract int[][] items();
	
	/**
	 * Sends this if we don't use the right items for this spell.
	 * @return
	 */
	public abstract String sendReqMessage();
	
	
	/**
	 * Executes enchanting spells.
	 * @param player
	 */
	public void execute(Player player, Item item) {
		player.getPacketSender().forceSendTab(6);
		boolean canCast = false;
		if (player.getSkills().getLevel(6) < getRequiredLevel()) {
			player.getPacketSender().sendMessage("You need a Magic level of "+getRequiredLevel()+" to use this spell.");
			return;
		}
		if (!RuneReplacers.hasEnoughRunes(player, getRunes(), true)) {
			return;
		}
		int index = -1;
		for (int i = 0; i < items().length; i++) {
			if (item.getId() == items()[i][0]) {
				index = i;
				canCast = true;
				break;
			}
		}
		if (!canCast) {
			player.getPacketSender().sendMessage(sendReqMessage());
			return;
		}
		if (!player.getInventory().replaceItem(items()[index][0], items()[index][1])) {
			return;
		}
		player.getInventory().refresh();
		RuneReplacers.deleteRunes(player, getRunes());
		player.animate(startAnim());
		player.playGraphic(startGraphic(), 0, 100);
	}


	public String getName() {
		return name();
	}

	public int getRequiredLevel() {
		return requiredLevel();
	}

	public Item[] getRunes() {
		return runes();
	}

	public double getExp() {
		return exp();
	}
}