package com.hyperion.game.content.skills.magic.onitems;

import com.hyperion.game.content.skills.magic.onitems.impl.*;
import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.player.Player;

public class MagicOnItemSpells {

    public MagicOnItemSpells() {

    }

    private static final int getSpellIndex(int spell) {
        System.out.println("spell used : " + spell);
        switch (spell) {
            case 5: // Sapphire jewls enchanting
                return 1;
            case 13: // Low alch
                return 2;
            case 16: // Emerald jewls enchanting
                return 3;
            case 28: // Ruby jewls enchanting
                return 4;
            case 34: // High alch
                return 5;
            case 36: // Diamond jewls enchanting
                return 6;
            case 51: // Dragonstone jewls enchanting
                return 7;
            case 61: // Onyx jewls enchanting
                return 8;
            case 25: // Super Heat Ore
                return 9;
        }
        return -1;
    }

    public static void handleSpells(Player player, Item item, int spell) {
        int index = getSpellIndex(spell);
        if (index == -1) {
            return;
        }
        if (player.getVariables().isNoTeleport()) {
            player.getPacketSender().sendMessage("You cannot perform this spell right now.");
            return;
        }
        if (player.getSettings().getMagicType() == 1) {// modern spells
            switch (index) {
                case 1:// Sapphire jewls enchanting
                    new SapphireEnchanting().execute(player, item);
                    break;
                case 2:// Low alch
                    new LowAlchemy().execute(player, item);
                    break;
                case 3:// Emerald jewls enchanting
                    new EmeraldEnchanting().execute(player, item);
                    break;
                case 4:// Ruby jewls enchanting
                    new RubyEnchanting().execute(player, item);
                    break;
                case 5:// High alch
                    new HighAlchemy().execute(player, item);
                    break;
                case 6:// Diamond jewls enchanting
                    new DiamondEnchanting().execute(player, item);
                    break;
                case 7:// Dragonstone jewls enchanting
                    new DragonStoneEnchanting().execute(player, item);
                    break;
                case 8:// Onyx jewls enchanting
                    new OnyxEnchanting().execute(player, item);
                    break;
                case 9:// Super heat ore
                    new SuperheatItem().execute(player, item);
                    break;
            }
        }
    }
}
