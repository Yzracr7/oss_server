package com.hyperion.game.content.skills.magic.onitems;

import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.content.skills.magic.RuneReplacers;
import com.hyperion.game.content.skills.smithing.BarToSmelt;
import com.hyperion.game.content.skills.smithing.Smelting;
import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.utility.NumberUtils;

public abstract class Superheat {

    /**
     * req lvl
     */
    public abstract int requiredLevel();

    /**
     * Required runes
     */
    public abstract Item[] runes();

    /**
     * animation
     */
    public abstract int startAnim();

    /**
     * graphic
     */
    public abstract int startGraphic();

    /**
     * exp this spell gives
     */
    public abstract double exp();

    /**
     * Alchs the item
     *
     * @param player
     * @param item
     */
    public void execute(final Player player, Item item) {
        if (item.getDefinition().isPlayerBound() || !item.getDefinition().isTradeable()) {
            return;
        }
        if (System.currentTimeMillis() - player.getVariables().getLastAction() < 2000) {
            return;
        }
        player.getPacketSender().forceSendTab(6);
        if (player.getSkills().getLevel(6) < requiredLevel()) {
            player.getPacketSender().sendMessage("You need a Magic level of " + requiredLevel() + " to use this spell.");
            return;
        }
        String name = item.getDefinition().getName().toLowerCase();
        if (!name.endsWith(" ore")) {
            player.getPacketSender().sendMessage("You can't superheat that item.");
            return;
        }
        if (!RuneReplacers.hasEnoughRunes(player, runes(), true)) {
            return;
        }

        //Iron ore roll
        boolean iron_roll = NumberUtils.random(50) > 25;
        if (player.getDetails().isDonator()) {
            iron_roll = true;
        }

        player.getAttributes().set("stopActions", true);
        player.getVariables().setLastAction(System.currentTimeMillis());
        player.getSkills().addExperience(Skills.MAGIC, exp());
        RuneReplacers.deleteRunes(player, runes());

        //Get the bar index
        int index = 2;
        for (int i = 0; i < Smelting.BARS.length; i++) {
            player.getPacketSender().sendMessage("Bar index : " + i + " [bar=" + Smelting.BARS[i] + "]");
            if (Smelting.BARS[i] == item.getId()) {
                index = i;
                break;
            }
        }

        //Set the bar and attribute
        BarToSmelt bar = new BarToSmelt(index, Smelting.BARS[index], Smelting.SMELT_LEVELS[index], Smelting.SMELT_XP[index], 1, Smelting.SMELT_ORES[index], Smelting.SMELT_ORE_AMT[index], Smelting.BAR_NAMES[index]);
        player.getAttributes().set("smeltingBar", bar);

        //Smelt the ore
        smeltOre(player, -1, 1, false);
        player.getInventory().refresh();

        World.getWorld().submit(new Tickable(2) {
            @Override
            public void execute() {
                this.stop();
                player.getAttributes().remove("stopActions");
            }
        });
    }

    public void smeltOre(final Player player, int buttonId, int amount, boolean newSmelt) {
        player.getInterfaceSettings().closeInterfaces(true);
        int index = -1;
        if (!newSmelt && !player.getAttributes().isSet("smeltingBar")) {
            return;
        }
        final BarToSmelt bar = (BarToSmelt) player.getAttributes().get("smeltingBar");
        if (!Smelting.canSmelt(player, bar)) {
            Smelting.resetSmelting(player);
            return;
        }
        for (int i = 0; i < bar.getOre().length; i++) {
            if (bar.getOreAmount()[i] != 0) {
                for (int j = 0; j < bar.getOreAmount()[i]; j++) {
                    if (!player.getInventory().deleteItem(bar.getOre()[i], 1)) {
                        Smelting.resetSmelting(player);
                        return;
                    }
                }
            }
        }
        String s = bar.getOre()[1] == 0 ? "ore" : "ore and coal";
        player.getPacketSender().sendMessage("You smelt the " + bar.getName().toLowerCase() + " " + s + " with your superheat..");
        if (!player.getAttributes().isSet("smeltingBar")) {
            Smelting.resetSmelting(player);
            return;
        }
        final BarToSmelt myBar = (BarToSmelt) player.getAttributes().get("smeltingBar");
        if (!bar.equals(myBar)) {
            Smelting.resetSmelting(player);
            return;
        }
        boolean dropIron = false;
        /*if (bar.getIndex() == 2) {
            if (!wearingForgingRing(p)) {
				if (Misc.random(1) == 0) {
					p.getPackets().sendMessage("You accidentally drop the iron ore deep into the furnace..");
					dropIron = true;
				}
			} else {
				lowerForgingRing(p);
			}
		}*/
        player.animate(startAnim());
        player.playGraphic(startGraphic(), 0, 100);

        if (!dropIron) {
            String s1 = bar.getIndex() == 2 || bar.getIndex() == 8 ? "a" : "an";
            player.getPacketSender().sendMessage("You retrieve " + s1 + " " + bar.getName().toLowerCase() + " bar.");
            player.getInventory().addItem(new Item(bar.getBarId(), 1));
            player.getInventory().refresh();
            player.getSkills().addExperience(Skills.SMITHING, bar.getXp());
            Achievements.increase(player, 7);
        }
        bar.decreaseAmount();
        if (bar.getAmount() <= 0) {
            Smelting.resetSmelting(player);
        }
        World.getWorld().submit(new Tickable(player, 3) {
            @Override
            public void execute() {
                this.stop();
                if (!player.getAttributes().isSet("smeltingBar")) {
                    Smelting.resetSmelting(player);
                    return;
                }
                final BarToSmelt myBar = (BarToSmelt) player.getAttributes().get("smeltingBar");
                if (!bar.equals(myBar)) {
                    Smelting.resetSmelting(player);
                    return;
                }
                if (bar.getAmount() >= 1) {
                    smeltOre(player, -1, 1, false);
                }
            }
        });
    }

}