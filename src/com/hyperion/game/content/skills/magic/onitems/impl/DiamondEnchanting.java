package com.hyperion.game.content.skills.magic.onitems.impl;

import com.hyperion.game.content.skills.magic.onitems.EnchantingSpells;
import com.hyperion.game.item.Item;

public class DiamondEnchanting extends EnchantingSpells {

	@Override
	public String name() {
		// TODO Auto-generated method stub
		return "Lvl-4 Enchant";
	}

	@Override
	public int requiredLevel() {
		// TODO Auto-generated method stub
		return 57;
	}

	@Override
	public Item[] runes() {
		// TODO Auto-generated method stub
		return new Item[]{new Item(557, 10), new Item(564, 1)};
	}

	@Override
	public double exp() {
		// TODO Auto-generated method stub
		return 67;
	}

	@Override
	public int[][] items() {
		// TODO Auto-generated method stub
		return new int[][] {
				{1643, 2570},//ring
				{1662, 11090},//necklace
				{1700, 1731},//amulet
		};
	}

	@Override
	public String sendReqMessage() {
		// TODO Auto-generated method stub
		return "You need jewelery made from diamond to use this spell.";
	}

	@Override
	public int startGraphic() {
		// TODO Auto-generated method stub
		return 115;
	}

	@Override
	public int startAnim() {
		// TODO Auto-generated method stub
		return 720;
	}

}
