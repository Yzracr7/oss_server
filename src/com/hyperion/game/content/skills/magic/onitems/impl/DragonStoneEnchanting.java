package com.hyperion.game.content.skills.magic.onitems.impl;

import com.hyperion.game.content.skills.magic.onitems.EnchantingSpells;
import com.hyperion.game.item.Item;

public class DragonStoneEnchanting extends EnchantingSpells {

    @Override
    public String name() {
        // TODO Auto-generated method stub
        return "Lvl-5 Enchant";
    }

    @Override
    public int requiredLevel() {
        // TODO Auto-generated method stub
        return 68;
    }

    @Override
    public Item[] runes() {
        // TODO Auto-generated method stub
        return new Item[]{new Item(555, 15), new Item(557, 15), new Item(564, 1)};
    }

    @Override
    public double exp() {
        // TODO Auto-generated method stub
        return 78;
    }

    @Override
    public int[][] items() {
        // TODO Auto-generated method stub
        return new int[][]{
                {1645, 2572},//ring
                {1664, 11105},//necklace
                {1702, 1712},//amulet
        };
    }

    @Override
    public String sendReqMessage() {
        // TODO Auto-generated method stub
        return "You need jewelery made from dragonstone to use this spell.";
    }

    @Override
    public int startGraphic() {
        // TODO Auto-generated method stub
        return 116;
    }

    @Override
    public int startAnim() {
        // TODO Auto-generated method stub
        return 721;
    }

}
