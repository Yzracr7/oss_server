package com.hyperion.game.content.skills.magic.onitems.impl;

import com.hyperion.game.content.skills.magic.onitems.EnchantingSpells;
import com.hyperion.game.item.Item;

public class EmeraldEnchanting extends EnchantingSpells {

	@Override
	public String name() {
		return "Lvl-2 Enchant";
	}

	@Override
	public int requiredLevel() {
		return 27;
	}

	@Override
	public Item[] runes() {
		return new Item[] { new Item(556, 3), new Item(564, 1) };
	}

	@Override
	public double exp() {
		return 37;
	}

	@Override
	public int[][] items() {
		return new int[][] { { 1639, 2552 },// ring
				{ 1658, 5521 },// necklace
				{ 1696, 1729 },// amulet
		};
	}

	@Override
	public String sendReqMessage() {
		return "You need jewelery made from emerald to use this spell.";
	}

	@Override
	public int startGraphic() {
		return 114;
	}

	@Override
	public int startAnim() {
		return 719;
	}

}
