package com.hyperion.game.content.skills.magic.onitems.impl;

import com.hyperion.game.content.skills.magic.onitems.Alchemy;
import com.hyperion.game.item.Item;
import com.hyperion.utility.NumberUtils;

public class HighAlchemy extends Alchemy {

	@Override
	public int requiredLevel() {
		return 55;
	}

	@Override
	public Item[] runes() {
		return new Item[]{new Item(554, 5), new Item(561, 1)};
	}

	@Override
	public int startAnim() {
		return 713;
	}

	@Override
	public int startGraphic() {
		return 113;
	}

	@Override
	public double exp() {
		return 60;
	}

	@Override
	public int alchPrice(Item item) {
		if (item.getId() == 1123)
			return NumberUtils.random(6000, 8000);
		return item.getDefinition().getHighAlchValue();
	}

}
