package com.hyperion.game.content.skills.magic.onitems.impl;

import com.hyperion.game.content.skills.magic.onitems.Alchemy;
import com.hyperion.game.item.Item;
import com.hyperion.utility.NumberUtils;

public class LowAlchemy extends Alchemy {

	@Override
	public int requiredLevel() {
		return 21;
	}

	@Override
	public Item[] runes() {
		return new Item[]{new Item(554, 3), new Item(561, 1)};
	}

	@Override
	public int startAnim() {
		return 712;
	}

	@Override
	public int startGraphic() {
		return 112;
	}

	@Override
	public double exp() {
		return 31;
	}

	@Override
	public int alchPrice(Item item) {
		if (item.getId() == 1123)
			return NumberUtils.random(4000, 6000);
		return item.getDefinition().getLowAlchValue();
	}

}
