package com.hyperion.game.content.skills.magic.onitems.impl;

import com.hyperion.game.content.skills.magic.onitems.EnchantingSpells;
import com.hyperion.game.item.Item;

public class OnyxEnchanting extends EnchantingSpells {

	@Override
	public String name() {
		// TODO Auto-generated method stub
		return "Lvl-6 Enchant";
	}

	@Override
	public int requiredLevel() {
		// TODO Auto-generated method stub
		return 87;
	}

	@Override
	public Item[] runes() {
		// TODO Auto-generated method stub
		return new Item[]{new Item(557, 20), new Item(554, 20), new Item(564, 1)};
	}

	@Override
	public double exp() {
		// TODO Auto-generated method stub
		return 97;
	}

	@Override
	public int[][] items() {
		// TODO Auto-generated method stub
		return new int[][] {
				{6575, 6583},//ring
				{6577, 11128},//necklace
				{6581, 6585},//amulet
		};
	}

	@Override
	public String sendReqMessage() {
		// TODO Auto-generated method stub
		return "You need jewelery made from onyx to use this spell.";
	}

	@Override
	public int startGraphic() {
		// TODO Auto-generated method stub
		return 452;
	}

	@Override
	public int startAnim() {
		// TODO Auto-generated method stub
		return 721;
	}

}
