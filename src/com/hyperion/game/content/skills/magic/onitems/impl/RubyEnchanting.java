package com.hyperion.game.content.skills.magic.onitems.impl;

import com.hyperion.game.content.skills.magic.onitems.EnchantingSpells;
import com.hyperion.game.item.Item;

public class RubyEnchanting extends EnchantingSpells {

	@Override
	public String name() {
		// TODO Auto-generated method stub
		return "Lvl-3 Enchant";
	}

	@Override
	public int requiredLevel() {
		// TODO Auto-generated method stub
		return 47;
	}

	@Override
	public Item[] runes() {
		// TODO Auto-generated method stub
		return new Item[]{new Item(554, 5), new Item(564, 1)};
	}

	@Override
	public int startAnim() {
		// TODO Auto-generated method stub
		return 720;
	}

	@Override
	public int startGraphic() {
		// TODO Auto-generated method stub
		return 115;
	}

	@Override
	public double exp() {
		// TODO Auto-generated method stub
		return 59;
	}

	@Override
	public int[][] items() {
		// TODO Auto-generated method stub
		return new int[][] {
				{1641, 2568},//ring
				{1660, 11194},//necklace
				{1698, 1725},//amulet
		};
	}

	@Override
	public String sendReqMessage() {
		// TODO Auto-generated method stub
		return "You need jewelery made from ruby to use this spell.";
	}

}
