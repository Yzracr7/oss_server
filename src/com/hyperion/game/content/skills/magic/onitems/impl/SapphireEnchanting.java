package com.hyperion.game.content.skills.magic.onitems.impl;

import com.hyperion.game.content.skills.magic.onitems.EnchantingSpells;
import com.hyperion.game.item.Item;

public class SapphireEnchanting extends EnchantingSpells {

    @Override
    public String name() {
        // TODO Auto-generated method stub
        return "Lvl-1 Enchant";
    }

    @Override
    public int requiredLevel() {
        // TODO Auto-generated method stub
        return 7;
    }

    @Override
    public Item[] runes() {
        // TODO Auto-generated method stub
        return new Item[]{new Item(555, 1), new Item(564, 1)};
    }

    @Override
    public double exp() {
        // TODO Auto-generated method stub
        return 17.5;
    }

    @Override
    public int[][] items() {
        // TODO Auto-generated method stub
        return new int[][]{
                {1637, 2550},//ring
                {1656, 3853},//necklace
                {1694, 1727},//amulet
        };
    }

    @Override
    public String sendReqMessage() {
        // TODO Auto-generated method stub
        return "You need jewelery made from sapphire to use this spell.";
    }

    @Override
    public int startGraphic() {
        // TODO Auto-generated method stub
        return 114;
    }

    @Override
    public int startAnim() {
        // TODO Auto-generated method stub
        return 719;
    }

}
