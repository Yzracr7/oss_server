package com.hyperion.game.content.skills.magic.onitems.impl;

import com.hyperion.game.content.skills.magic.onitems.Superheat;
import com.hyperion.game.item.Item;

public class SuperheatItem extends Superheat {

    @Override
    public int requiredLevel() {
        return 43;
    }

    @Override
    public Item[] runes() {
        return new Item[]{new Item(554, 4), new Item(561, 1)};
    }

    @Override
    public int startAnim() {
        return 725;
    }

    @Override
    public int startGraphic() {
        return 148;
    }

    @Override
    public double exp() {
        return 54;
    }
}
