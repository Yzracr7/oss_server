package com.hyperion.game.content.skills.mining;

import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.utility.NumberUtils;

/**
 * @author Konceal
 */
public class MineSession extends Tickable {

    private Player player;
    private GameObject rock;
    private int index = -1, delay = 0;
    private int current_anim = -1, animationDelay = 0;
    private boolean started = false;

    private MineSession(Player player, GameObject rock) {
        super(1, true);
        this.player = player;
        this.rock = rock;
        player.getVariables().setMineSession(this);
    }

    public static boolean attempt_mine(Player player, GameObject rock) {
        final int i = MiningData.getRockIndex(rock.getId());
        if (i == -1) {
            return false;
        }
        MineSession session = new MineSession(player, rock);
        session.index = i;
        World.getWorld().submit(session);
        return true;
    }

    private void startMining(boolean re_clicked) {
        if (!re_clicked && rock.getHealth() <= 0) {
            rock.setHealth(10);
        }
        // player.sendMessage("Starting mine process");
        if (re_clicked) {
            Rock newRock = new Rock(index, rock.getId(), rock.getLocation(), MiningData.ORES[index], MiningData.ROCK_LEVEL[index], MiningData.NAME[index], MiningData.ROCK_XP[index]);
            player.getAttributes().set("miningRock", newRock);
            player.sendMessage("rock[" + MiningData.ORES[index] + "] [" + rock.getId() + "]");
            this.current_anim = getPickaxeAnimation(player);
        }

        final Rock rockToMine = (Rock) player.getAttributes().get("miningRock");
        if (stopMining(rockToMine)) {
            reset();
            return;
        }
        if (re_clicked) {
            player.getPacketSender().sendMessage("You begin to swing your pick..");
        }
        this.delay = getCycles(player, rockToMine);
        if (!re_clicked) {
            int index = rockToMine.getRockIndex();
            player.getInventory().refresh();
            player.getInterfaceSettings().closeInterfaces(false);

            boolean addGem = (index != 0 && NumberUtils.random(getGemChance(player)) == 0);
            if (addGem) {
                player.getPacketSender().sendMessage("You manage to mine a sparkling gem!");
            }
            rock.deductHealth(1);
            if (index != 14) {// && player.getInventory().addItem(addGem ? randomGem() : rockToMine.getOre())) {
                if (player.getDetails().isDonator()) {
                    int extra_ess = NumberUtils.random(3);
                    if (extra_ess == 1) {
                        //player.getInventory().addItem(rockToMine.getOre() + 1);
                    }
                }
                if (!addGem) {
                    //player.getPacketSender().sendMessage("You manage to mine some " + rock.getName() + ".");
                }
                player.getSkills().addExperience(Skills.MINING, rockToMine.getXp());
                Achievements.increase(player, 6);
            } else if (index == 14) {
                int xp = 0;
                Item reward = null;
                for (int x = 0; x < STAR_SIZE.length; x++) {
                    if (rock.getId() == STAR_SIZE[x]) {
                        int level = (x + 1) * 10;
                        xp = ((level) * 2 + 10) / 2;
                        reward = starReward(x);
                    }
                }
                if (reward != null)
                    player.getInventory().addItem(reward);
                player.getSkills().addExperience(Skills.MINING, xp);
                player.getPacketSender().sendMessage("You receive a reward from the star.");
                player.getInventory().addItem(rockToMine.getOre());
            }

            player.getInventory().refresh();
        }
    }

    private static final int[] STAR_SIZE = {38668, 38667, 38666, 38665, 38664, 38663, 38662, 38661, 38660};

    private int randomGem() {
        if (NumberUtils.random(5000) == 0) {
            return 1631; // Dragonstone
        }
        if (NumberUtils.random(15000) == 0) {
            return 6571; // Onyx
        }
        return MiningData.GEMS[(int) (Math.random() * MiningData.GEMS.length)];
    }

    private static int getGemChance(Player p) {
        for (int i = 0; i < MiningData.GLORY_AMULETS.length; i++) {
            if (p.getEquipment().getItemInSlot(2) == MiningData.GLORY_AMULETS[i]) {
                return 30;
            }
        }
        return 40;
    }

    private boolean stopMining(Rock rock) {
        if (player.getAttributes().isSet("stopActions")) {
            return true;
        }
        if (rock == null) {
            return true;
        }
        int level = rock.getLevel();
        if (rock.getRockIndex() == 14) {
            for (int x = 0; x < STAR_SIZE.length; x++) {
                if (rock.getRockId() == STAR_SIZE[x]) {
                    level = (x + 1) * 10;
                }
            }
        }
        if (player.getSkills().getLevel(Skills.MINING) < level) {
            player.getPacketSender().sendMessage("You need a Mining level of " + level + " to mine this rock.");
            return true;
        }
        if (getUsedPickaxe(player) == -1) {
            player.getPacketSender().sendMessage("You do not have a pickaxe that you can use.");
            return true;
        }
        if (player.getInventory().findFreeSlot() == -1) {
            player.getPacketSender().sendMessage("Your inventory is too full to carry any ores.");
            return true;
        }
        if (!this.rock.isAlive()) {
            player.getPacketSender().sendMessage("This rock has run out of ores.");
            return true;
        }
        //if (index == 14 && World.getObjectWithId(rock.getRockLocation(), rock.getRockId()) == null) {
        // player.getPacketSender().sendMessage("This rock has run out of ores.");
        // return true;
        // }
        return false;
    }

    public void reset() {
        player.getVariables().setMineSession(null);
        player.animate(65535);
        this.stop();
    }

    public int getObjectRespawnTimer() {
        int index = getRockIndex(rock.getId());
        if (index == -1)
            return -1; // seconds
        if (index > MiningData.RESPAWN_TIME.length)
            return 10;
        return MiningData.RESPAWN_TIME[index];
    }

    @Override
    public void execute() {
        // TODO Auto-generated method stub
        if (player.isDestroyed()) {
            this.stop();
            return;
        }

        if (player.getAttributes().isSet("stopActions")) {
            reset();
            return;
        }

        if (rock.getHealth() <= 0 && NumberUtils.random(10) == 5) {
            player.getPacketSender().sendMessage("This rock has run out of ores.");
            World.spawnObjectTemporary(new GameObject(rock.getLocation(), 7469, rock.getType(), rock.getRotation()), getObjectRespawnTimer());
            rock.setAlive(false);
            switch (index) {
                case 1:
                    rock.setHealth(NumberUtils.random(1));
                    break;
                case 6:
                    rock.setHealth(NumberUtils.random(2, 10));
                    break;
                case 8:
                    rock.setHealth(NumberUtils.random(1, 10));
                    break;
                case 9:
                    player.getInventory().addItem(453);
                    rock.setHealth(NumberUtils.random(4, 10));
                    break;
                case 11:
                    player.getInventory().addItem(447);
                    rock.setHealth(NumberUtils.random(2, 10));
                    break;
                case 12:
                    player.getInventory().addItem(449);
                    rock.setHealth(NumberUtils.random(2, 10));
                    break;
                case 13:
                    rock.setHealth(NumberUtils.random(2, 10));
                    break;
                case 2:
                    rock.setHealth(NumberUtils.random(1, 1));
                    break;
                case 3:
                    rock.setHealth(NumberUtils.random(1, 1));
                    break;
                case 10:
                    rock.setHealth(NumberUtils.random(2, 10));
                    break;
            }
            reset();
            this.stop();
            return;
        } else {
            rock.setAlive(true);
            rock.setHealth(NumberUtils.random(rock.getHealth()));
        }

        //   GameObject(Location location, int objId, int type, int rotation) {
        if (index != 0 && index != 14) {

        } else if (index == 14) {
            if (rock.getHealth() <= 0) {
                rock.setAlive(false);
                player.getPacketSender().sendChatboxDialogue(true, "The crashed star has degraded!");
                reset();
            }
        }

        if (delay > 0) {
            delay--;
        } else if (delay <= 0) {
            if (!started) {
                started = true;
                startMining(true);
            } else {
                startMining(false);
            }
        }
        if (animationDelay <= 0) {
            player.animate(current_anim);
            animationDelay = 7;
        } else {
            animationDelay--;
        }
    }

    private int getCycles(Player player, Rock rock) {
        int skill = player.getSkills().getLevel(Skills.MINING);
        int level = rock.getLevel();
        int modifier = rock.getLevel();
        if (index == 14) {
            for (int x = 0; x < STAR_SIZE.length; x++) {
                if (index == STAR_SIZE[x]) {
                    level = (x + 1) * 10;
                    modifier = level;
                    // modifier = (level) * 2 + 5;
                }
            }
        }
        int randomAmt = NumberUtils.random(3);
        double cycleCount = Math.ceil((level * 50 - skill * 10) / modifier * 0.25 - randomAmt * 4);
        if (cycleCount < 1) {
            cycleCount = 1;
        }
        return (int) cycleCount + 1;
    }

    private static int getRockIndex(int id) {
        return MiningData.getRockIndex(id);
    }

    private static boolean hasLevelRequiredAxe(Player p, int axeIndex) {
        if (axeIndex == -1) {
            return false;
        }
        if (p.getSkills().getLevel(Skills.MINING) >= MiningData.PICKAXE_LEVEL_REQS[axeIndex]) {
            return true;
        }
        return false;
    }

    private static int getUsedPickaxe(Player p) {
        int index = -1;
        for (int i = 0; i < MiningData.PICKAXES.length; i++) {
            if (p.getEquipment().getItemInSlot(3) == MiningData.PICKAXES[i] || p.getInventory().hasItem(MiningData.PICKAXES[i])) {
                if (hasLevelRequiredAxe(p, i)) {
                    index = i;
                }
            }
        }
        return index;
    }

    private static int getPickaxeAnimation(Player p) {
        int pickaxe = getUsedPickaxe(p);
        if (pickaxe == -1) {
            return 65535;
        }
        return MiningData.PICKAXE_ANIMS[pickaxe];
    }

    private static Item starReward(int index) {
        int chance = NumberUtils.random(0, 100) + (index); // index is star,
        // higher index =
        // bigger star
        int id = 995;
        if (chance > 99 && (NumberUtils.random(0, 300) + index) > 299) {
            id = extremeItems[NumberUtils.random(0, extremeItems.length - 1)];
            return new Item(id, id == 995 ? NumberUtils.random(40000, 80000) : 1);
        } else if (chance > 95) {
            id = highStacks[NumberUtils.random(0, highStacks.length - 1)];
            return new Item(id, id == 995 ? NumberUtils.random(35000, 45000) : NumberUtils.random(1, 3));
        } else if (chance > 80) {
            id = medStacks[NumberUtils.random(0, medStacks.length - 1)];
            return new Item(id, id == 995 ? NumberUtils.random(20000, 35000) : NumberUtils.random(1, 5));
        } else {
            id = regularStacks[NumberUtils.random(0, regularStacks.length - 1)];
            return new Item(id, id == 995 ? NumberUtils.random(5000, 15000) : NumberUtils.random(1, 10));
        }
    }

    private static int regularStacks[] = {995, 386, 392, 561, 892, 10476, 7947, 1618, 868

    };

    private static int medStacks[] = {995, 1124, 15273, 860, 1334, 9244, 1332

    };

    private static int highStacks[] = {995, 5699, 4588, 9245, 1632

    };

    private static int extremeItems[] = {4152, 6586, 2582, 2578,

    };
}
