package com.hyperion.game.content.skills.mining;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;

public class MiningData {

    public static final int MINING = 14;

    public static final int ORES[] = {
            7936, // Pure essence
            434, // Clay
            436, // Copper ore
            438, // Tin ore
            668, // Blurite ore
            3211, // Limestone
            440, //Iron ore
            2892, //Elemental ore
            442, // Silver ore
            453, // Coal
            444, // Gold ore
            447, // Mithril ore
            449, // Adamantite ore
            451, // Runite ore
            -1 //TODO shooting star tingz
    };

    public static final int ROCK_LEVEL[] = {
            1, // Pure essence
            1, // Clay
            1, // Copper ore
            1, // Tin ore
            10, // Blurite ore
            10, // Limestone
            15, //Iron ore
            20, //Elemental ore
            20, // Silver ore
            30, // Coal
            40, // Gold ore
            55, // Mithril ore
            70, // Adamantite ore
            85, // Runite ore
            10, // Shooting star

    };

    public static final int ROCK_HEALTH[] = {
            -1, // Pure essence
            4, // Clay
            5, // Copper ore
            5, // Tin ore
            5, // Blurite ore
            5, // Limestone
            5, //Iron ore
            8, //Elemental ore
            8, // Silver ore
            8, // Coal
            7, // Gold ore
            7, // Mithril ore
            6, // Adamantite ore
            5, // Runite ore
            -1, // Shooting star

    };

    public static final String NAME[] = {
            "essence", // Pure essence
            "clay", // Clay
            "copper", // Copper ore
            "tin", // Tin ore
            "blurite", // Blurite ore
            "limestone", // Limestone
            "iron", //Iron ore
            "elemental", //Elemental ore
            "silver", // Silver ore
            "coal", // Coal
            "gold", // Gold ore
            "mithril", // Mithril ore
            "adamantite", // Adamantite ore
            "runite", // Runite ore
            "star" // Shooting star
    };

    public static final double ROCK_XP[] = {
            5, // Pure essence
            5, // Clay
            17.5, // Copper ore
            17.5, // Tin ore
            17.5, // Blurite ore
            26.5, // Limestone
            35, //Iron ore
            0, //Elemental ore
            40, // Silver ore
            50, // Coal
            65, // Gold ore
            80, // Mithril ore
            95, // Adamantite ore
            125, // Runite ore
            -1 // Shooting star
    };

    public static final int[] PICKAXES = {
            1265, // Bronze
            1267, // Iron
            1269, // Steel
            1271, // Mithril
            1273, // Adamant
            1275,  // Rune
            13661, // Inferno Adze
            11920 // Dragon
    };

    public static final int[] PICKAXE_ANIMS = {
            625, // Bronze
            626, // Iron
            627, // Steel
            629, // Mithril
            628, // Adamant
            624,  // Rune
            10222, // Inferno Adze
            7139, // Dragon
    };

    public static final int[] PICKAXE_TIME = {
            2, // Bronze
            3, // Iron
            4, // Steel
            5, // Mithril
            6, // Adamant
            7,  // Rune
            7, // Inferno Adze
            8, // Dragon
    };

    public static final int[] PICKAXE_LEVEL_REQS = {
            1, // Bronze
            1, // Iron
            6, // Steel
            21, // Mithril
            31, // Adamant
            41,  // Rune
            41, // Inferno Adze
            61 // Dragon
    };

    public static final int[] RESPAWN_TIME = {
            -1, // Pure essence
            5, // Clay
            5, // Copper ore
            5, // Tin ore
            8, // Blurite ore
            8, // Limestone
            10, //Iron ore
            10, //Elemental ore
            10, // Silver ore
            10, // Coal
            20, // Gold ore
            30, // Mithril ore
            50, // Adamantite ore
            80, // Runite ore
            -1 //Shooting star
    };

    public static final int[] GEMS = {
            1623, // Sapphire
            1621, // Emerald
            1619, // Ruby
            1617, // Diamond
    };

    public static final int[] GLORY_AMULETS = {
            1704, // (0)
            1706, // (1)
            1708, // (2)
            1710, // (3)
            1712, // (4)
    };

    public static final int getRockIndex(int id) {
        switch (id) {
            case 7453: // Copper
            case 7484: // Copper
                return 2;
            case 7486: // Tin
            case 7485: // Tin
                return 3;
            case 7488: // Iron
            case 7455: // Iron
                return 6;
            case 7489: // Coal
            case 7456: // Coal
                return 9;
            case 7457: // Silver
            case 7490: // Silver
                return 8;
            case 7454: // Gold
            case 7487: // Gold
            case 7458: // Gold
            case 7491: // Gold
                return 10;
            case 7492: // Mithril
            case 7459: // Mithril
                return 11;
            case 7460: // Adamant
                return 12;
        }
        return -1;
    }

    private static final String getRockName(GameObject object) {
        int index = getRockIndex(object.getId());
        if (index == -1) {
            return "Nothing interesting happens.";
        }
        switch (index) {
            case 0://Ess
                return "Rune essence";
            case 1://Clay
                return "clay";
            case 2://Copper
                return "copper";
            case 3://Tin
                return "tin";
            case 6://Iron
                return "iron";
            case 8://Silver
                return "silver";
            case 9://Coal
                return "coal";
            case 10://Gold
                return "gold";
            case 11://Mith
                return "mithril";
            case 12://Addy
                return "adamantite";
            case 13://Rune
                return "runite";
            case 14://Shooting star
                return "stardust";
        }
        return "Nothing interesting happens.";
    }

    public static void prospectRocks(final Player player, GameObject object) {
        player.getPacketSender().sendMessage("You examine the rock for ores...");
        final String prospectMessage = getRockName(object);
        World.getWorld().submit(new Tickable(2) {
            @Override
            public void execute() {
                this.stop();
                player.getPacketSender().sendMessage("This rock contains " + prospectMessage + ".");
            }
        });
    }
}
