package com.hyperion.game.content.skills.mining;

import com.hyperion.game.world.Location;

public class Rock {

	private int rockIndex;
	private Location rockLocation;
	private int ore;
	private int level;
	private double xp;
	private int rockId;
	private boolean continueMine;
	private String name;
	
	public Rock(int index, int rockId, Location loc, int ore, int level, String name, double xp) {
		this.rockIndex = index;
		this.rockLocation = loc;
		this.ore = ore;
		this.rockId = rockId;
		this.level = level;
		this.xp = xp;
		this.name = name;
		this.continueMine = true;
	}

	public int getRockId() {
		return rockId;
	}

	public double getXp() {
		return xp;
	}

	public int getOre() {
		return ore;
	}

	public int getLevel() {
		return level;
	}

	public boolean canContinueMining() {
		return continueMine;
	}
	

	public void setContinueMining(boolean i) {
		continueMine = i;
	}


	public int getRockIndex() {
		return rockIndex;
	}

	public Location getRockLocation() {
		return rockLocation;
	}

	public String getName() {
		return name;
	}
}
