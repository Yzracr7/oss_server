package com.hyperion.game.content.skills.prayer;

import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;

public class BoneBurying extends PrayerData {

	public static boolean isItemBone(Player p, int item) {
		for (int i = 0; i < BONES.length; i++) {
			if (item == BONES[i]) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isBuryingBone(Player p, int item, int slot) {
		for (int i = 0; i < BONES.length; i++) {
			if (item == BONES[i]) {
				buryBone(p, i, slot);
				return true;
			}
		}
		return false;
	}
	
	public static boolean canCrushBone(Player p, int item) {
		for (int i = 0; i < BONES.length; i++) {
			if (item == BONES[i]) {
				crushBone(p, i);
				return true;
			}
		}
		return false;
	}
	
	private static void buryBone(final Player p, final int i, final int slot) {
		long lastBury = 0;
		if (p.getAttributes().isSet("lastBury")) {
			lastBury = p.getAttributes().getLong("lastBury");
		}
		if (System.currentTimeMillis() - lastBury < 1800) {
			return;
		}
		final String bonesName = ItemDefinition.forId(BONES[i]).getName();
		p.getAttributes().set("lastBury", System.currentTimeMillis());
		p.getWalkingQueue().reset();
		p.getPacketSender().clearMapFlag();
		p.getAttributes().set("stopActions", true);
		p.getPacketSender().sendMessage("You dig a hole in the ground...");
		p.animate(BURY_ANIMATION);
		World.getWorld().submit(new Tickable(2) {
			@Override
			public void execute() {
				this.stop();
				p.getInventory().deleteItem(BONES[i], 1);
				p.getSkills().addExperience(PRAYER, BURY_XP[i]);
				p.getAttributes().remove("stopActions");
				p.getPacketSender().sendMessage("You bury the "+bonesName+".");
			}
		});
	}
	
	private static void crushBone(final Player p, final int i) {
		final String bonesName = ItemDefinition.forId(BONES[i]).getName();
		p.getSkills().addExperience(PRAYER, BURY_XP[i]);
		//p.getFrames().sendMessage("Your bonecrusher buried the "+bonesName+".");
	}
	
	
}
