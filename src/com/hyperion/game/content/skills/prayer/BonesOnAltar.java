package com.hyperion.game.content.skills.prayer;

import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;

/**
 * @author Nando
 */
public class BonesOnAltar {

	public static int[][] BONES = { { 526, 5 }, // NPC BONES
			{ 528, 5 }, // BURNT BONES
			{ 530, 5 }, // BAT BONES
			{ 2859, 5 }, // WOLF BONES
			{ 3179, 5 }, // MONKEY BONES
			{ 3180, 5 }, // MONKEY BONES
			{ 3181, 5 }, // MONKEY BONES
			{ 3182, 5 }, // MONKEY BONES
			{ 3183, 5 }, // MONKEY BONES
			{ 3185, 5 }, // MONKEY BONES
			{ 3186, 5 }, // MONKEY BONES
			{ 3187, 5 }, // MONKEY BONES
			{ 532, 15 }, // BIG BONES
			{ 534, 30 }, // BABY DRAGON BONES
			{ 536, 72 }, // DRAGON BONES
			{ 2530, 5 }, // PLAYER BONES
			{ 3123, 25 }, // SHAIKAHAN BONES
			{ 3125, 23 }, // JOGRE BONES
			{ 3127, 25 }, // BURNT JOGRE BONES
			{ 4812, 82 }, // ZOGRE BONES
			{ 4830, 84 }, // FAYGR BONES
			{ 4832, 96 }, // RAURG BONES
			{ 4834, 140 }, // OURG BONES
			{ 6729, 125 }, // DAGANNOTH BONES
			{ 6812, 50 }, // WYVERN BONES
			{ 10976, 160 }, // LONG BONE
			{ 10977, 170 }, // CURVED BONE
			{ 11337, 180 }, // MANGLED BONE
			{ 11338, 200 }, // CHEWED BONE
			{ 11943, 140 }, // LAVA DRAGON BONES
	};

	public static boolean bones_on_altar_check(Player player, GameObject object, int item, int slot) {
		player.getAttributes().remove("bonesonaltar");
		for (int i = 0; i < BONES.length; i++) {
			if (item == BONES[i][0]) {
				if (player.getDetails().isDonator()) {
					show_options(player, item, i, object);
				} else {
					handleBones(player, object, item, slot, i);
				}
				return true;
			}
		}
		return false;
	}

	private static void show_options(Player p, int item, int index, GameObject object) {
		if (p.getVariables().getBonesOnAltarSession() != null)
			p.getVariables().getBonesOnAltarSession().reset();
		BonesOnAltarSession session = new BonesOnAltarSession(p, item, index, object);
		p.getAttributes().set("bonesonaltar", session);
		p.getPacketSender().itemOnInterface(309, 2, 135, item);
		p.getPacketSender().modifyText("Choose amount to use on altar", 309, 7);
		p.getPacketSender().modifyText("<br><br><br>Dragon bones", 309, 6);
		p.getPacketSender().sendChatboxInterface(309);
	}

	public static void handleBones(final Player player, final GameObject object, final int item, final int slot, int index) {
		boolean using_dialogue = player.getAttributes().isSet("bonesonaltar");
		if (using_dialogue) {
			BonesOnAltarSession session = player.getAttributes().get("bonesonaltar");
			player.getVariables().setBonesOnAltarSession(session);
			World.getWorld().submit(session);
			return;
		}
		if (player.getInventory().deleteItem(item, 1)) {
			player.getInventory().refresh();
			player.getSkills().addExperience(5, (BONES[index][1] * 2.5));
			player.animate(896);
			Graphic graphic = new Graphic(624, 0, 0);
			player.getPacketSender().sendStillGraphics(object.getLocation(), graphic, 0);
			player.getPacketSender().sendMessage("The Gods are pleased with your offerings..");
		}
	}
}
