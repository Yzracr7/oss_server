package com.hyperion.game.content.skills.prayer;

import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;

/**
 * @author Nando
 */
public class BonesOnAltarSession extends Tickable {

	private Player player;
	private int item, index, delay;
	private int animationDelay = 0;
	
	public BonesOnAltarSession(Player player, int item, int index, GameObject object) {
		super(1, true);
		this.player = player;
		this.item = item;
		this.index = index;
		player.getVariables().setBonesOnAltarSession(this);
	}
	
	public void reset() {
		this.stop();
		player.animate(-1);
		player.getAttributes().remove("bonesonaltar");
		player.getVariables().setBonesOnAltarSession(null);
	}

	@Override
	public void execute() {
		// TODO Auto-generated method stub
		if (player.isDestroyed()) {
			reset();
			return;
		}
		if (animationDelay <= 0) {
			player.animate(896);
			animationDelay = 5;
		} else if (animationDelay > 0) {
			animationDelay--;
		}
		if (delay > 0) {
			delay--;
			return;
		}
		if (!player.getInventory().deleteItem(item, 1)) {
			player.getPacketSender().sendMessage("You've ran out of "+ItemDefinition.forId(item).getName()+" to use.");
			reset();
			return;
		}
		delay = 2;
		player.getInterfaceSettings().closeInterfaces(false);
		player.getInventory().refresh();
		player.getSkills().addExperience(5, (BonesOnAltar.BONES[index][1] * 2.5));
		player.getPacketSender().sendMessage("The Gods are pleased with your offerings..");
	}
}
