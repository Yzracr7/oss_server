package com.hyperion.game.content.skills.prayer;

import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;

public class PrayerAltars {

    /**
     * Altar pray restoring
     *
     * @param player
     */
    public static boolean restorePrayer(Player player) {
        if (player.getSkills().getLevel(5) < player.getSkills().getLevelForXp(5)) {
            player.animate(645);
            player.getSkills().setLevel(5, player.getSkills().getLevelForXp(5));
            player.getPacketSender().sendSkillLevel(5);
            return true;
        } else {
            player.getPacketSender().sendMessage("You already have full prayer points.");
            return false;
        }
    }

    public static boolean isRestoringPrayerAtAltar(Player player, GameObject object) {
        int[] altars = {27501,4860,26366, 24343, 4859, 37990, 2640, 411, 409, 27661};
        for (int i = 0; i < altars.length; i++) {
            if (altars[i] == object.getId()) {
                restorePrayer(player);
                return true;
            }
        }
        return false;
    }
}
