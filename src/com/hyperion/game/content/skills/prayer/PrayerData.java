package com.hyperion.game.content.skills.prayer;

public class PrayerData {

    protected static final int PRAYER = 5;
    protected static final int BURY_ANIMATION = 827;

    public static final int[] BONES = {
            526, // Normal
            2859, // Wolf
            528, // Burnt
            3179, // Monkey
            530, // Bat
            532, // Big
            10977, // Curved
            10976, // Long
            3125, // Jogre
            4812, // Zogre
            3123, // Shaikahan
            534, // Baby dragon
            6812, // Wyvern
            536, // Dragon
            4830, // Fayrg
            4832, // Raurg
            6729, // Dagannoth
            4834, // Ourg
            18668, // Lava dragon bones
            18667, // Frost dragon bones
    };

    protected static final double[] BURY_XP = {
            4.5, // Normal
            4.5, // Wolf
            4.5, // Burnt
            5, // Monkey
            5.3, // Bat
            15, // Big
            15, // Curved
            15, // Long
            15, // Jogre
            15, // Zogre
            25, // Shaikahan
            30, // Baby dragon
            50, // Wyvern
            72, // Dragon
            84, // Fayrg
            96, // Raurg
            125, // Dagannoth
            140, // Ourg
            140, //Lava dragon
            180, //Frost dragon
    };

    public static double[] PRAYER_DRAIN = {
            12, // Thick Skin.
            12, // Burst of Strength.
            12, // Clarity of Thought.
            12, // Sharp Eye.
            12, // Mystic Will.
            6, // Rock Skin.
            6, // SuperHuman Strength.
            6, // Improved Reflexes.
            26, // Rapid restore
            18, // Rapid Heal.
            18, // Protect Items
            6, // Hawk eye.
            6, // Mystic Lore.
            3, // Steel Skin.
            3, // Ultimate Strength.
            3, // Incredible Reflexes.
            3, // Protect from Magic.
            3, // Protect from Missiles.
            3, // Protect from Melee.
            3, // Eagle Eye.
            3, // Mystic Might.
            12, // Retribution.
            6, // Redemption.
            2, // Smite
            1.5, // Chivalry.
            1.5, // Piety.
    };

    protected static final int[] PRAYER_LVL = {
            1, 4, 7, 8, 9, 10, 13, 16, 19, 22, 25, 26, 27, 28,
            31, 34, 37, 40, 43, 44, 45, 46, 49, 52, 60, 70
    };

    protected static final int[] PRAYER_CONFIG = {
            83, 84, 85, 862, 863, 86, 87, 88, 89, 90, 91, 864,
            865, 92, 93, 94, 95, 96, 97, 866, 867, 98, 99, 100,
            1052, 1053, -1, -1
    };

    protected static final String[] PRAYER_NAME = {
            "Thick Skin", "Burst of Strength", "Clarity of Thought", "Sharp Eye", "Mystic Will", "Rock Skin", "Superhuman Strength",
            "Improved Reflexes", "Rapid Restore", "Rapid Heal", "Protect Item", "Hawk Eye", "Mystic Lore", "Steel Skin",
            "Ultimate Strength", "Incredible Reflexes", "Protect from Magic", "Protect from Ranged",
            "Protect from Melee", "Eagle Eye", "Mystic Might", "Retribution", "Redemption", "Smite", "Preserve", "Chivalry", "Peity", "Rigour", "Augury"
    };

    protected static int getPrayerIndex(int button) {
        switch (button) {
            case 5://Thick skin
                return 0;
            case 7://Burst of skin
                return 1;
            case 9://Clarity of thought
                return 2;
            case 11://Sharp eye
                return 3;
            case 13://Mystic Will
                return 4;
            case 15://Rock Skin
                return 5;
            case 17://Superhuman strength
                return 6;
            case 19://Improved reflexes
                return 7;
            case 21://Rapid restore
                return 8;
            case 23://Rapid heal
                return 9;
            case 25://Protect 1 item
                return 10;
            case 27://Hawk eye
                return 11;
            case 29://Mystic Lore
                return 12;
            case 31://Steel skin
                return 13;
            case 33://Ultimate str
                return 14;
            case 35://Incredible Reflexes
                return 15;
            case 37://Protect from magic
                return 16;
            case 39://Protect from missles
                return 17;
            case 41://Protect from melee
                return 18;
            case 43://Eagle eye
                return 19;
            case 45://Mystic might
                return 20;
            case 47://Retribution
                return 21;
            case 49://Redemption
                return 22;
            case 51://Smite
                return 23;
            case 53://Chivarly
                return 24;
            case 55://Piety
                return 25;
        }
        return -1;
    }
}
