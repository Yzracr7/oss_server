package com.hyperion.game.content.skills.runecrafting;

import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;

public class RCEntry {

    public static boolean hasTiara(Player player, int objectId) {
        return false;
    }

    public static boolean enter_altar(Player player, GameObject objectId) {
        if (hasTiara(player, objectId.getId())) {
            player.sendMessage("You may enterInstancedRoom.");
            return true;
        }
        return false;
    }
}
