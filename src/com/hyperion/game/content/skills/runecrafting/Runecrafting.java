package com.hyperion.game.content.skills.runecrafting;

import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.content.randomevent.EvilBobEvent;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.utility.NumberUtils;
import com.hyperion.utility.Utils;

public class Runecrafting {

    public static boolean craft(Player player, GameObject objectId) {
        int[][] craft = null;
        double xp = 0;
        switch (objectId.getId()) {
            case 14897:
                craft = AIR;
                xp = AIR_XP;
                break;
            case 14898:
                craft = MIND;
                xp = MIND_XP;
                break;
            case 14899:
                craft = WATER;
                xp = WATER_XP;
                break;
            case 14900:
                craft = EARTH;
                xp = EARTH_XP;
                break;
            case 14901:
                craft = FIRE;
                xp = FIRE_XP;
                break;
            case 14902:
                craft = BODY;
                xp = BODY_XP;
                break;
            case 14903:
                craft = COSMIC;
                xp = COSMIC_XP;
                break;
            case 14906:
                craft = CHAOS;
                xp = CHAOS_XP;
                break;
            case 14911:
                craft = ASTRAL;
                xp = ASTRAL_XP;
                break;
            case 14905:
                craft = NATURE;
                xp = NATURE_XP;
                break;
            case 14904:
                craft = LAW;
                xp = LAW_XP;
                break;
            case 14907:
                craft = DEATH;
                xp = DEATH_XP;
                break;
            case 27978:
                craft = BLOOD;
                xp = BLOOD_XP;
                break;
        }
        System.out.println(craft + " " + xp);
        if (craft != null) {
            final int essenceAmount = player.getInventory().getCount(7936);
            if (essenceAmount <= 0) {
                player.getPacketSender().sendMessage("You don't have any Pure Essence to craft!");
                return true;
            }
            final int playerCraftLevel = player.getSkills().getLevel(Skills.RUNECRAFTING);
            final int craftLevel = craft[0][0];
            if (playerCraftLevel < craftLevel) {
                player.getPacketSender().sendMessage("You need a Runecrafting level of " + craftLevel + " to craft from here.");
                return true;
            }
            final int craftId = craft[0][1];
            int craftAmount = 1;
            if (craft.length > 1) {
                for (int i = 1; i < craft.length; i++) {
                    int[] craftMultiplier = craft[i];
                    if (playerCraftLevel >= craftMultiplier[0])
                        craftAmount = craftMultiplier[1];
                }
            }
            player.animate(791);
            player.playGraphic(186, 0, 100);
            player.getAttributes().set("stopActions", true);
            for (int i = 0; i < essenceAmount; i++) {
                if (!player.getInventory().deleteItem(7936)) {
                    break;
                }
            }
            player.getInventory().addItem(craftId, craftAmount * essenceAmount);
            player.getSkills().addExperience(Skills.RUNECRAFTING, (xp * 2) * essenceAmount);
            craft = null;

            World.getWorld().submit(new Tickable(3) {
                @Override
                public void execute() {
                    player.getInventory().refresh();
                    Achievements.increase(player, 21, essenceAmount);
                    int pointsToAdd = essenceAmount / 3;
                    if (pointsToAdd > 1)
                        player.getVariables().increase_rc_points(player.getDetails().isDonator() ? pointsToAdd * 2 : pointsToAdd);
                    player.getAttributes().remove("stopActions");
                    if (Utils.random(0, 300) > 298) {
                        EvilBobEvent.start(player);
                    }
                    this.stop();
                }
            });
            return true;
        }
        return false;
    }

    private static final int[][] AIR = new int[][]{
            {1, 556},
            {11, 2},
            {22, 3},
            {33, 4},
            {44, 5},
            {55, 6},
            {66, 7},
            {77, 8},
            {88, 9},
            {99, 10},
    };

    private static final int[][] MIND = new int[][]{
            {2, 558},
            {14, 2},
            {28, 3},
            {42, 4},
            {56, 5},
            {70, 6},
            {84, 7},
            {98, 8},
    };

    private static final int[][] WATER = new int[][]{
            {5, 555},
            {19, 2},
            {38, 3},
            {57, 4},
            {76, 5},
            {95, 6},
    };

    private static final int[][] EARTH = new int[][]{
            {9, 557},
            {26, 2},
            {52, 3},
            {78, 4},
    };

    private static final int[][] FIRE = new int[][]{
            {14, 554},
            {35, 2},
            {70, 3},
    };

    private static final int[][] BODY = new int[][]{
            {20, 559},
            {46, 2},
            {92, 3},
    };

    private static final int[][] COSMIC = new int[][]{
            {27, 564},
            {59, 2},
    };

    private static final int[][] CHAOS = new int[][]{
            {35, 562},
            {74, 2},
    };

    private static final int[][] ASTRAL = new int[][]{
            {40, 9075},
            {82, 2},
    };

    private static final int[][] NATURE = new int[][]{
            {44, 561},
            {91, 2},
    };

    private static final int[][] LAW = new int[][]{
            {54, 563},
    };

    private static final int[][] DEATH = new int[][]{
            {65, 560},
    };

    private static final int[][] BLOOD = new int[][]{
            {77, 565},
    };

    private static final double
            AIR_XP = 5,
            MIND_XP = 5.5,
            WATER_XP = 6,
            EARTH_XP = 6.5,
            FIRE_XP = 7,
            BODY_XP = 7.5,
            COSMIC_XP = 8,
            CHAOS_XP = 8.5,
            ASTRAL_XP = 8.75,
            NATURE_XP = 9,
            LAW_XP = 9.5,
            DEATH_XP = 10,
            BLOOD_XP = 10.5;

    public static boolean locate(final Player player, int itemId) {
        final int[] teleport = new int[3];
        switch (itemId) {
            case 1438: //air
                teleport[0] = 2841;
                teleport[1] = 4829;
                teleport[2] = 0;
                break;
            case 1440://earth
                teleport[0] = 2655;
                teleport[1] = 4830;
                teleport[2] = 0;
                break;
            case 1442: //fire
                teleport[0] = 2577;
                teleport[1] = 4846;
                teleport[2] = 0;
                break;
            case 1444://water
                teleport[0] = 3494;
                teleport[1] = 4832;
                teleport[2] = 0;
                break;
            case 1446://body
                teleport[0] = 2521;
                teleport[1] = 4834;
                teleport[2] = 0;
                break;
            case 1448://mind
                teleport[0] = 2793;
                teleport[1] = 4828;
                teleport[2] = 0;
                break;
            case 1450://blood
                teleport[0] = 2468;
                teleport[1] = 4889;
                teleport[2] = 1;
                break;
            case 1452://chaos
                teleport[0] = 2281;
                teleport[1] = 4837;
                teleport[2] = 0;
                break;
            case 1454://cosmic
                int x1 = 2142, y1 = 4853;
                int x2 = 2162, y2 = 4833;
                int x3 = 2142, y3 = 4813;
                int x4 = 2122, y4 = 4833;
                int random = NumberUtils.random(3);
                if (random == 0) {
                    teleport[0] = x1;
                    teleport[1] = y1;
                    teleport[2] = 0;
                } else if (random == 1) {
                    teleport[0] = x2;
                    teleport[1] = y2;
                    teleport[2] = 0;
                } else if (random == 2) {
                    teleport[0] = x3;
                    teleport[1] = y3;
                    teleport[2] = 0;
                } else if (random == 3) {
                    teleport[0] = x4;
                    teleport[1] = y4;
                    teleport[2] = 0;
                }
                break;
            case 1456://death
                teleport[0] = 2208;
                teleport[1] = 4830;
                teleport[2] = 0;
                break;
            case 1458://law
                teleport[0] = 2464;
                teleport[1] = 4818;
                teleport[2] = 0;
                break;
            case 1460://soul
                return true;
            case 1462://nature
                teleport[0] = 2400;
                teleport[1] = 4835;
                teleport[2] = 0;
                break;
            default:
                return false;
        }
        if (!(player.getLocation().getX() == 3129 && player.getLocation().getY() == 3496 && player.getLocation().getZ() == 0)) {
            player.getPacketSender().sendMessage("To do this, you must be in the center of a magical ring near Edgeville.");
            return true;
        }
        //MagicBookHandler.talismanTeleport(teleport[0], teleport[1], teleport[2]);
        return true;
    }
}
