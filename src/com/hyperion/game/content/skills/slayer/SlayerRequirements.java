package com.hyperion.game.content.skills.slayer;


public class SlayerRequirements {
	
	public static int getRequiredSlayerLevel(String name) {
		if (name.equalsIgnoreCase("crawling hand"))
			return 5;
		if (name.equalsIgnoreCase("cave crawler"))
			return 10;
		if (name.equalsIgnoreCase("banshee"))
			return 15;
		if (name.equalsIgnoreCase("pyrefiend"))
			return 30;
		if (name.equalsIgnoreCase("basilisk"))
			return 40;
		if (name.equalsIgnoreCase("terror dog"))
			return 40;
		if (name.equalsIgnoreCase("infernal mage"))
			return 45;
		if (name.equalsIgnoreCase("brine rat"))
			return 47;
		if (name.equalsIgnoreCase("bloodveld"))
			return 50;
		if (name.equalsIgnoreCase("jelly"))
			return 52;
		if (name.equalsIgnoreCase("turoth"))
			return 55;
		if (name.equalsIgnoreCase("cave horror"))
			return 58;
		if (name.equalsIgnoreCase("aberrant spectre"))
			return 60;
		if (name.equalsIgnoreCase("spiritual ranger"))
			return 63;
		if (name.equalsIgnoreCase("dust devil"))
			return 65;
		if (name.equalsIgnoreCase("kurask"))
			return 70;
		if (name.equalsIgnoreCase("skeletal wyvern"))
			return 72;
		if (name.equalsIgnoreCase("gargoyle"))
			return 75;
		if(name.equalsIgnoreCase("nechryael"))
			return 80;
		if (name.equalsIgnoreCase("abyssal demon"))
			return 85;
		if (name.equalsIgnoreCase("dark beast"))
			return 90;
		return 1;
	}
}
