package com.hyperion.game.content.skills.slayer;

import com.hyperion.game.content.QuestTab;
import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.utility.NumberUtils;
import com.hyperion.utility.TextUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * @author Nando
 * @author Tyrone
 */
public class SlayerTasks {

    private static class Task {
        private String name;
        private int low; // least amount of task assigned
        private int max; // max amount of task assigned

        private Task(String name, int low, int max) {
            this.name = name;
            this.low = low;
            this.max = max;
        }
    }

    private enum Task_Type {
        EASY(new Task[]{new Task("COW", 5, 35), new Task("YAK", 5, 35), new Task("ROCK_CRAB", 5, 35), new Task("GHOST", 5, 35), new Task("BANSHEE", 5, 35), new Task("HILL_GIANT", 5, 35), new Task("SKELETON", 5, 35), new Task("GUARD", 5, 35), new Task("CRAWLING_HAND", 5, 35), new Task("MEN", 5, 35), new Task("ZOMBIE", 5, 35)}), MEDIUM(new Task[]{new Task("MOSS_GIANT", 35, 100),
                new Task("LESSER_DEMON", 25, 65),
                new Task("ICE_WARRIOR", 25, 75), new Task("ICE_GIANT", 25, 75), new Task("HILL_GIANT", 30, 100), new Task("DAGANNOTH", 25, 70), new Task("WILD_DOG", 25, 70), new Task("FIRE_GIANT", 30, 100), new Task("TZHAAR", 30, 100), new Task("KALPHITE", 60, 130), new Task("EXPERIMENT", 40, 130), new Task("CHAOS_DRUID", 55, 140),
                // new Task("CYCLOPS", 55, 140),
                new Task("BLOODVELD", 55, 140), new Task("EARTH_WARRIOR", 55, 140), /*new Task("JELLY", 30, 80),*/ new Task("ABERRANT_SPECTRE", 30, 80), new Task("ELF_WARRIOR", 30, 80), new Task("INFERNAL_MAGE", 30, 80)

        }), HARD(new Task[]{
                // new Task("DUST_DEVIL", 55, 140),
                new Task("GARGOYLE", 55, 140), new Task("INFERNAL_MAGE", 55, 140), new Task("BLOODVELD", 55, 140), //new Task("JELLY", 55, 140),
                // new Task("GIANT_ROCK_CRAB", 55, 140),
                // new Task("OGRE", 55, 140),
                // new Task("SHADOW_WARRIOR", 55, 140),
                new Task("DAGANNOTH", 55, 140), new Task("MONKEY_GUARD", 50, 130), new Task("ABERRANT_SPECTRE", 5, 45), new Task("GREEN_DRAGON", 45, 100), new Task("BLUE_DRAGON", 45, 100), new Task("BLACK_DRAGON", 35, 80), new Task("RED_DRAGON", 30, 90), new Task("BRONZE_DRAGON", 50, 130), new Task("IRON_DRAGON", 50, 130), new Task("STEEL_DRAGON", 50, 130),
                // new Task("BLACK_DEMON", 50, 130),
                // new Task("GREATER_DEMON", 60, 130),
                new Task("TZHAAR", 60, 130), new Task("KALPHITE", 60, 130), new Task("DARK_BEAST", 5, 100), new Task("HELLHOUND", 55, 120), new Task("WATERFIEND", 55, 140), new Task("ABYSSAL_DEMON", 35, 100)}),
        EXTREMELY_HARD(new Task[]{new Task("DAGANNOTH_KINGS", 5, 30), new Task("KALPHITE_QUEEN", 5, 10), new Task("MITHRIL_DRAGON", 5, 30), new Task("TZTOK JAD", 1, 1), new Task("KING_BLACK_DRAGON", 5, 15), new Task("GENERAL_GRAARDOR", 5, 15),
                new Task("KRAKEN", 5, 30), new Task("CALLISTO", 5, 15), new Task("VENENATIS", 5, 15), new Task("VET'ION", 5, 15), new Task("SCORPIA", 5, 15)});

        Task[] tasks;

        Task_Type(Task[] tasks) {
            this.tasks = tasks;
        }
    }

    private static Task find_task(Player player, int diff) {
        Task_Type slayer_task_type = diff == 1 ? Task_Type.EASY : diff == 2 ? Task_Type.MEDIUM : diff == 3 ? Task_Type.HARD : Task_Type.EXTREMELY_HARD;
        List<Task> possibleTasks = new LinkedList<Task>();
        for (Task tasks : slayer_task_type.tasks) {
            if (tasks == null)
                continue;
            int required = SlayerRequirements.getRequiredSlayerLevel(tasks.name.replace("_", " "));
            if (player.getSkills().getLevelForXp(Skills.SLAYER) >= required) {
                possibleTasks.add(tasks);
            }
        }
        Random r = new Random();
        return possibleTasks.get(r.nextInt(possibleTasks.size()));
    }

    public static void assign(Player player, int diff) {
        if (!player.getVariables().getTaskName().equalsIgnoreCase("")) {
            player.getPacketSender().sendMessage("You need to finish your current task! " + player.getVariables().getTaskAmount() + " " + player.getVariables().getTaskName() + "s.");
            return;
        }
        Task currentTask = find_task(player, diff);
        if (currentTask == null) {
            return;
        }
        String name = TextUtils.fixName(currentTask.name.toLowerCase()).replace("_", " ");
        if ((name.startsWith("Tzhaar")) || (name.startsWith("Tztok"))) {
            name = name.replace(" ", "-");
        }
        int amount = NumberUtils.random(currentTask.low, currentTask.max);
        player.getVariables().setTaskName(name);
        player.getVariables().setTaskAmount(amount);
        player.getPacketSender().sendMessage("You're new slayer assignment is to kill " + amount + " " + name + "'s.");
    }

    private static boolean deduction_allowed(String task, String name) {
        if (task.equals("ghost")) {
            if ((name.contains("ghost"))) {
                return true;
            }
        } else if (task.startsWith("tzhaar")) {
            if ((name.startsWith("tzhaar")) || (name.startsWith("tztok"))) {
                return true;
            }
        } else if (task.equals("dagannoth kings")) {
            if (name.contains("supreme") || name.contains("rex") || name.contains("prime")) {
                return true;
            }
        } else if (task.equals("blue dragon")) {
            if (name.contains("blue dragon") || name.contains("baby blue dragon")) {
                return true;
            }
        } else if (task.equals("red dragon")) {
            if (name.contains("red dragon") || name.contains("baby red dragon")) {
                return true;
            }
        } else if (task.equals("black dragon")) {
            if (name.contains("black dragon") || name.contains("baby black dragon") || name.contains("king black dragon")) {
                return true;
            }
        } else if (task.equals("men")) {
            if ((name.contains("man")) || (name.contains("woman"))) {
                return true;
            }
        } else if (task.equals("guard")) {
            if (name.contains("guard")) {
                return true;
            }
        } else if (task.equals("kalphite")) {
            if (name.contains("kalphite")) {
                return true;
            }
        } else {
            if (name.equalsIgnoreCase(task)) {
                return true;
            }
        }
        return false;
    }

    public static void deduct_task(Player player, NPC npc) {
        String name = player.getVariables().getTaskName();
        if (name.equalsIgnoreCase("")) {
            return;
        }
        String original_name = npc.getDefinition().getName();
        if (!deduction_allowed(name.toLowerCase(), original_name.toLowerCase())) {
            return;
        }
        player.getVariables().deduct_task(1);
        int points = 1;
        for (Task t : Task_Type.EXTREMELY_HARD.tasks) {
            if (t.name.replace("_", " ").equalsIgnoreCase(player.getVariables().getTaskName())) {
                points = 5;
                player.getPacketSender().sendMessage("You receive 5 slayer points for killing an Extreme Task assignment.");
                break;
            }
        }
        if (player.getVariables().getTaskAmount() == 0) {
            points += 10;
            player.getPacketSender().sendMessage("You receive 10 extra Slayer Points for completing your Slayer assignment.");
        }
        player.getVariables().increase_slayer_points(points);
        player.getSkills().addExperience(Skills.SLAYER, npc.getDefinition().getHitpoints() / 4);
        if (player.getVariables().getTaskAmount() <= 0) {
            player.getVariables().setTaskName(null);
            player.getVariables().setTaskAmount(0);
            Achievements.increase(player, 22);
            Achievements.increase(player, 33);
            player.getPacketSender().sendMessage("You've finished your task! Visit a slayer master for a new one.");
        }
        QuestTab.refresh(player);
    }

    public static void current_task(Player player) {
        String name = player.getVariables().getTaskName();
        int amount = player.getVariables().getTaskAmount();
        if (name.equalsIgnoreCase("")) {
            player.getPacketSender().sendMessage("You currently have no Slayer task.");
            return;
        }
        player.getPacketSender().sendMessage("You're current slayer assignment is to kill " + amount + " " + name + "'s.");
    }

    public static int resetAmount = 50000;

    public static void reset_task(Player player) {
        String name = player.getVariables().getTaskName();
        if (name.equalsIgnoreCase("")) {
            player.getPacketSender().sendMessage("You currently have no Slayer task.");
            return;
        }
        if (!player.getDetails().isDonator()) {
            if (!player.getInventory().hasItemAmount(995, resetAmount)) {
                player.getPacketSender().sendMessage("You don't have enough coins to reset your task.");
                return;
            } else {
                player.getInventory().deleteItem(new Item(995, resetAmount));
                player.getInventory().refresh();
            }
        } else {
            if (!player.getInventory().hasItemAmount(995, resetAmount / 2)) {
                player.getPacketSender().sendMessage("You don't have enough coins to reset your task.");
                return;
            } else {
                player.getInventory().deleteItem(new Item(995, resetAmount / 2));
                player.getInventory().refresh();
            }
        }
        player.getVariables().setTaskName(null);
        player.getVariables().setTaskAmount(0);
        QuestTab.refresh(player);
    }
}
