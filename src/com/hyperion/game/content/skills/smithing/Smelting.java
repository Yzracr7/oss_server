package com.hyperion.game.content.skills.smithing;

import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.item.Item;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;


public class Smelting extends SmithingData {
	
	public Smelting() {
		
	}
	
	public static void displaySmeltOptions(final Player p, GameObject object) {
		int gfxChild = 4;
		int textChild = 16;
		String s = "<br><br><br><br>";
		for (int i = 0 ; i < BARS.length; i++) {
			boolean enabled = p.getSkills().getLevel(SMITHING) >= SMELT_LEVELS[i];
			String colour = enabled ? "<col=000000>" : "<col=A00000>";
			p.getPacketSender().itemOnInterface(311, gfxChild, 130, BARS[i]);
			p.getPacketSender().modifyText(s + colour + BAR_NAMES[i], 311, textChild);
			gfxChild++;
			textChild += 4;
		}
		p.getPacketSender().sendChatboxInterface(311);
	}
	
	public static void smeltOre(final Player p, int buttonId, int amount, boolean newSmelt) {
		p.getInterfaceSettings().closeInterfaces(true);
		int index = -1;
		if (!newSmelt && !p.getAttributes().isSet("smeltingBar")) {
			return;
		}
		if (newSmelt) {
			resetSmelting(p);
			index = getBarToMake(buttonId);
			if (index == -1) {
				return;
			}
			if (amount == -1) {
				amount =  getAmount(buttonId);
			}
			if (amount == -2) {
				p.getInterfaceSettings().openEnterAmountInterface("Enter amount to smelt:", 311, buttonId);
				return;
			}
			BarToSmelt bar = new BarToSmelt(index, BARS[index], SMELT_LEVELS[index], SMELT_XP[index], amount, SMELT_ORES[index], SMELT_ORE_AMT[index], BAR_NAMES[index]);
			p.getAttributes().set("smeltingBar", bar);
		}
		final BarToSmelt bar = (BarToSmelt) p.getAttributes().get("smeltingBar");
		if (!canSmelt(p, bar)) {
			resetSmelting(p);
			return;
		}
		for (int i = 0; i < bar.getOre().length; i++) {
			if (bar.getOreAmount()[i] != 0) {
				for (int j = 0; j < bar.getOreAmount()[i]; j++) {
					if (!p.getInventory().deleteItem(bar.getOre()[i], 1)) {
						resetSmelting(p);
						return;
					}
				}
			}
		}
		String s = bar.getOre()[1] == 0 ? "ore" : "ore and coal";
		p.getPacketSender().sendMessage("You place the " + bar.getName().toLowerCase()+ " " + s + " into the furnace..");
		p.animate(3243);
		if (!p.getAttributes().isSet("smeltingBar")) {
			resetSmelting(p);
			return;
		}
		final BarToSmelt myBar = (BarToSmelt) p.getAttributes().get("smeltingBar");
		if (!bar.equals(myBar)) {
			resetSmelting(p);
			return;
		}
		boolean dropIron = false;
		/*if (bar.getIndex() == 2) {
			if (!wearingForgingRing(p)) {
				if (Misc.random(1) == 0) {
					p.getPackets().sendMessage("You accidentally drop the iron ore deep into the furnace..");
					dropIron = true;
				}
			} else {
				lowerForgingRing(p);
			}
		}*/
		if (!dropIron) {
			String s1 = bar.getIndex() == 2 || bar.getIndex() == 8 ? "a" : "an";
			p.getPacketSender().sendMessage("You retrieve " + s1 + " " + bar.getName().toLowerCase() + " bar.");
			p.getInventory().addItem(new Item(bar.getBarId(), 1));
			p.getInventory().refresh();
			p.getSkills().addExperience(SMITHING, bar.getXp());
			Achievements.increase(p,  7);
		}
		bar.decreaseAmount();
		if (bar.getAmount() <= 0) {
			resetSmelting(p);
		}
		World.getWorld().submit(new Tickable(p, 3) {
			@Override
			public void execute() {
				this.stop();
				if (!p.getAttributes().isSet("smeltingBar")) {
					resetSmelting(p);
					return;
				}
				final BarToSmelt myBar = (BarToSmelt) p.getAttributes().get("smeltingBar");
				if (!bar.equals(myBar)) {
					resetSmelting(p);
					return;
				}
				if (bar.getAmount() >= 1) {
					smeltOre(p, -1, 1, false);
				}
			}
		});
	}
	
	public static boolean canSmelt(Player p, BarToSmelt bar) {
		if (bar == null || bar.getAmount() <= 0) {
			return false;
		}
		if (p.getSkills().getLevel(SMITHING) < bar.getLevel()) {
			p.getPacketSender().sendMessage("You need a Smithing level of " + bar.getLevel() + " to make that bar.");
			return false;
		}
		for (int i = 0; i < bar.getOre().length; i++) {
			if (bar.getOreAmount()[i] != 0) {
				String itemName = ItemDefinition.forId(bar.getOre()[i]).getName();
				if (!p.getInventory().hasItemAmount(bar.getOre()[i], bar.getOreAmount()[i])) {
					p.getPacketSender().sendMessage("You don't have enough "+itemName+" to make that bar.");
					return false;
				}
			}
		}
		return true;
	}
	
	public static int getAmount(int buttonId) {
		for (int i = 13; i <= 48; i++) {
			if (buttonId == i) {
				return SMELT_BUTTON_AMOUNT[i - 13];
			}
		}
		return 0;
	}
	
	public static int getBarToMake(int buttonId) {
		int counter = 0, barIndex = 0;
		for (int i = 13; i <= 48; i++) {
			if (counter == 4) {
				barIndex++;
				counter = 0;
			}
			if (buttonId == i) {
				return barIndex;
			}
			counter++;
		}
		return -1;
	}
	
	public static void resetSmelting(Player p) {
		p.getAttributes().set("smeltingBar", null);
	}
}