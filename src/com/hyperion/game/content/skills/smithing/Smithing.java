package com.hyperion.game.content.skills.smithing;

import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.utility.game.InputAction;

public class Smithing extends SmithingData {

	public Smithing() {}
	
	public static boolean wantsToSmith(Player player, GameObject object, Item item) {
		player.face(object.getLocation());
		if (object.getName().equalsIgnoreCase("anvil")) {
			for (int i = 0; i < BARS.length; i++) {
				if (item.getId() == BARS[i]) {
					final int j = i;
					displaySmithOptions(player, j);
					return true;
				}
			}
		}
		return false;
	}
	
	private static void displaySmithOptions(Player p, int index) {
		Object[][] variables = getInterfaceVariables(p, index);
		if (variables == null) {
			return;
		}
		if (index == -1){
			for (int i = 0; i < Smelting.BARS.length; i++) {
				if (p.getInventory().hasItem(Smelting.BARS[0])){
					index = i;
					break;
				}
			}
		}
		//Check our bar type -- and if there exists inter placement for it, sendTeleportTitles config to unlock.
	//	p.getPacketSender().sendInterfaceConfig(312, 65, variables[6][5] != null);	//Darts - inter placement(child)
	//	p.getPacketSender().sendInterfaceConfig(312, 81, variables[8][5] != null); //bronze wire
	//	p.getPacketSender().sendInterfaceConfig(312, 89, variables[9][5] != null); //null
	//	p.getPacketSender().sendInterfaceConfig(312, 97, variables[10][5] != null); //null
	//	p.getPacketSender().sendInterfaceConfig(312, 161, variables[18][5] != null); //null
	//	p.getPacketSender().sendInterfaceConfig(312, 169, variables[19][5] != null); //null
	//	p.getPacketSender().sendInterfaceConfig(312, 209, variables[24][5] != null);//claws
	//	p.getPacketSender().sendInterfaceConfig(312, 266, variables[29][5] != null); //pickaxe
		
		//Loop through all of our smithing types
		Item[] firstColumn = new Item[5];
		Item[] secondColumn = new Item[5];
		Item[] thirdColumn = new Item[5];
		Item[] fourthColumn = new Item[5];
		Item[] fifthColumn = new Item[5];
		Item[] sixthColumn = new Item[3];
		
		for (int j = 0; j < variables.length; j++) {
			boolean canSmith = p.getSkills().getLevel(SMITHING) >= (Integer)variables[j][1];
			String barColour = p.getInventory().hasItemAmount(BARS[index], (Integer)variables[j][3]) ? "<col=00FF00>" : "<col=DB8C02>";
			String amount = barColour + variables[j][3];
			String name = (String)variables[j][5];
			String s = (Integer)variables[j][3] > 1 ? "s" : "";
			int child = (Integer)variables[j][6];
			//p.getPacketSender().itemOnInterface(312, child, (Integer)variables[j][2], (Integer)variables[j][0]);
			if(child <= 104) {
				firstColumn[child - 100] = new Item((Integer)variables[j][0], (Integer)variables[j][2]);
			} else if (child <= 109) {
				secondColumn[child - 105] = new Item((Integer)variables[j][0], (Integer)variables[j][2]);
			} else if (child <= 114) {
				thirdColumn[child - 110] = new Item((Integer)variables[j][0], (Integer)variables[j][2]);
			} else if (child <= 119) {
				fourthColumn[child - 115] = new Item((Integer)variables[j][0], (Integer)variables[j][2]);
			} else if (child <= 124) {
				fifthColumn[child - 120] = new Item((Integer)variables[j][0], (Integer)variables[j][2]);
			} else if (child <= 129) {
				sixthColumn[child - 125] = new Item((Integer)variables[j][0], (Integer)variables[j][2]);
			}
			p.getPacketSender().modifyText(canSmith ? "<col=FFFFFF>" + name : name, 312, (Integer)variables[j][7]);
			p.getPacketSender().modifyText(((Integer)variables[j][3]) == 0 ? "" : (barColour + amount + " bar" + s), 312, (Integer)variables[j][8]);
		}
		
		p.getPacketSender().sendItems(312, 146, 2, firstColumn);
		p.getPacketSender().sendItems(312, 147, 2, secondColumn);
		p.getPacketSender().sendItems(312, 148, 2, thirdColumn);
		p.getPacketSender().sendItems(312, 149, 2, fourthColumn);
		p.getPacketSender().sendItems(312, 150, 2, fifthColumn);
		p.getPacketSender().sendItems(312, 151, 2, sixthColumn);
		
		p.getPacketSender().displayInterface(312);
		p.getAttributes().set("smithingBarType", index);
	}
	
	public static void smithItem(final Player p, int interfaceShit, int slot, int offset, boolean newSmith) {
		int index = -1;
		if (!newSmith && p.getAttributes().get("smithingItem") == null) {
			return;
		}
		if (newSmith) {
			if (p.getAttributes().get("smithingBarType") == null) {
				return;
			}
			index = (Integer) p.getAttributes().get("smithingBarType");
			if (index == -1) {
				return;
			}
			Object[][] variables = getInterfaceVariables(p, index);
			int amountToMake = -1;
			int item = -1;
			
			int column = interfaceShit - 20447378; //Bound to change?
			
			int columnStart = 100 + (column * 5);

			int button = columnStart + slot;
			
			for(int i = 0; i < variables.length; i++) {
				if((Integer)variables[i][6] == button) {
					item = i;
				}
			}
			if (offset == 4) {
				p.getPacketSender().requestIntegerInput(new InputAction<Integer>("Enter amount to smith:") {
					@Override
					public void handleInput(Integer amount) {
						if (!p.getAttributes().isSet("smithingItem")) {
							return;
						}
						SmithBar item = (SmithBar) p.getAttributes().get("smithingItem");
						if (item != null) {
							item.setAmount(amount);
						}
						Smithing.smithItem(p, -1, button, amount, false);
					}
					});
				p.getInterfaceSettings().openEnterAmountInterface("Enter amount to smith:", 312, slot);
			} else
				if (offset == 3) {
				amountToMake = 28;
			} else
				if (offset == 5) {
				amountToMake = 5;
			} else
				if (offset == 6) {
				amountToMake = 1;
			}
			if (offset >= 400) {
				amountToMake = offset - 400;	
			}
			if(item == -1) {
				p.getPacketSender().sendMessage("Bar invalid -- Please contact a staff member.");
				return;
			}
			//bar, amt, level, xp, itemId(again?), amt each smith
			if(variables[item][4] instanceof Double) {
				p.getAttributes().set("smithingItem", new SmithBar(BARS[index], (Integer)variables[item][3], (Integer)variables[item][1], (Double)variables[item][4], (Integer)variables[item][0], (Integer)variables[item][2], amountToMake));
			} else {
				p.getAttributes().set("smithingItem", new SmithBar(BARS[index], (Integer)variables[item][3], (Integer)variables[item][1], (Integer)variables[item][4], (Integer)variables[item][0], (Integer)variables[item][2], amountToMake));
			}
			
		}
		SmithBar item = (SmithBar) p.getAttributes().get("smithingItem");
		if (!canSmith(p, item)) {
			return;
		}
		p.getInterfaceSettings().closeInterfaces(false);
		p.animate(898);
		for (int i = 0; i < item.getBarAmount(); i++) {
			if (!p.getInventory().deleteItem(item.getBarType())) {
				return;
			}
		}
		if (p.getInventory().addItem(item.getFinishedItem(), item.getFinishedItemAmount())) {
			p.getSkills().addExperience(SMITHING, item.getXp());
		}
		p.getInventory().refresh();
		item.decreaseAmount();
		if (item.getAmount() >= 1) {
			World.getWorld().submit(new Tickable(p, 3) {
				@Override
				public void execute() {
					this.stop();
					smithItem(p, -1, -1, 1, false);
				}
			});
		}
	}
	
	private static boolean canSmith(Player p, SmithBar item) {
		if (p == null || item == null) {
			return false;
		}
		if (item.getAmount() <= 0) {
			return false;
		}
		if (!p.getInventory().hasItem(HAMMER)) {
			p.getPacketSender().sendMessage("You need a hammer if you wish to smith.");
			return false;
		}
		if (p.getSkills().getLevel(SMITHING) < item.getLevel()) {
			p.getPacketSender().sendMessage("You need a Smithing level of " + item.getLevel() + " to make that item.");
			return false;
		}
		if (!p.getInventory().hasItemAmount(item.getBarType(), item.getBarAmount())) {
			p.getPacketSender().sendMessage("You don't have enough bars to make that item.");
			return false;
		}
		return true;
	}
	
	private static Object[][] getInterfaceVariables(Player p, int i) {
		switch(i) {
			case 0:
				return BRONZE;
				// 1 is blurite
			case 2:
				return IRON;
				// 3 is silver
			case 4:
				return STEEL;
				// 5 is gold
			case 6:
				return MITHRIL;
				
			case 7:
				return ADAMANT;
				
			case 8:
				return RUNE;
		}
		return null;
	}
	
	public static void resetSmithing(Player p) {
		p.getAttributes().remove("smithingItem");
		p.getAttributes().remove("smithingBarType");
	}
}