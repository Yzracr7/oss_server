package com.hyperion.game.content.skills.thieving;

import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;

/**
 * Generates pickpocketing loot
 *
 * @author trees
 */
public class PickpocketLoot {

    /**
     * On success of pick-pocketing this will generate a loot based on npc ip
     *
     * @param player
     * @param npc
     * @return
     */
    public static Item generate(final Player player, final NPC npc) {
        if (npc != null) {
            switch (npc.getId()) {
                case 3078: // MAN
                case 3079: // MAN
                case 3080: // MAN
                case 3084: // WOMEN
                case 3083: // WOMEN
                case 3015: // WOMEN
                    int random_ordinal0 = NumberUtils.random(0, 50);
                    player.getInventory().addItem(random_ordinal0 < 47 ? 995 : 5318, random_ordinal0 < 45 ? 170 : 1);
                    break;
                case 3094: // EDGEVILLE GUARD
                case 3010: // VARROCK GUARD (LVL-21)
                case 3271: // FALADOR GUARD AXE (LVL-19)
                case 3269: // FALADOR GUARD SWORD (LVL-21)
                case 3272: // FALADOR GUARD ARCHER (LVL-22)
                case 3270: // FALADOR GUARD CROSSBOW (LVL-22)
                case 3245: // ARDOUGNE GUARD (LVL-20)
                    player.getInventory().addItem(995, 341);
                    break;
                case 3108: // ARDOUGNE KNIGHT (LVL-46)
                case 3106: // ARDOUGNE HERO (LVL-69)
                    player.getInventory().addItem(995, npc.getId() == 3106 ? (1000 + NumberUtils.random(1000)) : NumberUtils.random(1000));
                    break;
                case 3100: // WARRIOR WOMEN (LVL-24)
                case 3105: // PALADIN
                    int random_ordinal1 = NumberUtils.random(0, 50);
                    player.getInventory().addItem(random_ordinal1 < 45 ? 995 : 6430, random_ordinal1 < 45 ? 750 : 2);
                    break;
                case 3086: // NORMAL FARMER
                case 3087: // NORMAL FARMER
                case 3088: // NORMAL FARMER
                    player.getInventory().addItem(995, NumberUtils.random(422, 534));
                    break;
                case 3258: // MASTER FARMER
                case 5832: // MARTIN MASTER FARMER
                case 3257: // MASTER FARMER
                    int random_ordinal = NumberUtils.random(MasterFarmerSeeds.values().length);
                    if (random_ordinal > NumberUtils.random(MasterFarmerSeeds.values().length)) {
                        random_ordinal -= 1;
                    }
                    player.getInventory().addItem(MasterFarmerSeeds.values()[random_ordinal].id, NumberUtils.random(MasterFarmerSeeds.values()[random_ordinal].min, MasterFarmerSeeds.values()[random_ordinal].max));
                    break;
                default:
                    player.sendMessage("This npc does not have loot.");
                    //player.getInventory().addItem(995, 100);
                    break;
            }
        }
        return null;
    }
}
