package com.hyperion.game.content.skills.thieving;

/**
 * Pickpocket data
 *
 * @author trees
 */
public enum PickpocketableNPC {
    MAN(3078, 1, 8, 50, 1), MAN2(3079, 1, 8, 200, 1), MAN3(3080, 1, 8, 200, 1), WOMEN3(3015, 1, 8, 200, 1),WOMEN(3084, 1, 8, 200, 1), WOMEN2(3083, 1, 8, 200, 1), FARMER(3086, 10, 11, 137, 1), FARMER2(3087, 10, 11, 137, 1), MARTIN_MASTER_FARMER(5832, 38, 17, -1, 2), MASTER_FARMER(3257, 38, 17, -1, 2), MASTER_FARMER3(3258, 38, 17, -1, 2), ARDYGUARD(3245, 40, 19, 189, 6), PALADIN(3105, 70, 42, -1, 6), FALADOR_GUARD_ARCHER(3272, 40, 19, 189, 6), WARRIOR_WOMEN(3100, 25, 19, 189, 6), FALADOR_GUARD(3269, 40, 19, 189, 6), FALADOR3_GUARD(3270, 40, 19, 189, 6), FALADOR2_GUARD(3271, 40, 19, 189, 6),EDGE_GUARD(3094, 40, 19, 189, 6), VARROCK_GUARD(3010, 40, 19, 189, 6), ARDYKNIGHT_GUARD(3108, 55, 27, -1, 6), ARDY_HERO(3106, 80, 53, -1, 6);

    private int id;
    private int levelReq, exp, reward, damage;

    private PickpocketableNPC(int id, int levelReq, int exp, int reward, int damage) {
        this.id = id;
        this.levelReq = levelReq;
        this.exp = exp;
        this.reward = reward;
        this.damage = damage;
    }

    public int getId() {
        return id;
    }

    public int getReq() {
        return levelReq;
    }

    public int getExp() {
        return exp;
    }

    public int getReward() {
        return reward;
    }

    public int getDamage() {
        return damage;
    }
}
