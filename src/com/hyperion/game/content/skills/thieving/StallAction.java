package com.hyperion.game.content.skills.thieving;

import com.hyperion.game.item.Item;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.utility.NumberUtils;

/**
 * Stall action handling
 *
 * @author trees
 */
public class StallAction {

    public static boolean handle(Player player, GameObject object) {
        Stalls stall = Stalls.forId(object.getId());
        if (stall == null) {
            return false;
        }
        if (stall.level > player.getSkills().getLevel(Skills.THIEVING)) {
            player.getPacketSender().sendMessage("You must have a Thieving level of " + stall.level + " to steal from this stall.");
            return true;
        }
        if (player.getInventory().freeSlots() <= 0) {
            player.getPacketSender().sendMessage("You don't have enough inventory space to steal from this stall.");
            return true;
        }
        if (player.getAttributes().isSet("stopActions"))
            return true;
        player.getAttributes().set("stopActions", true);

        player.animate(Animation.create(881));
        player.getInventory().refresh();
        player.getPacketSender().sendMessage("You steal some loot from the stall.");

        GameObject new_stall;
        if (World.getObjectWithId(object.getLocation(), stall.empty_id) == null) {
            new_stall = new GameObject(object.getLocation(), stall.empty_id, object.getType(), object.getRotation());
            World.spawnObjectTemporary(new_stall, stall.respawn);
        }
        Thieving.addExperience(player, stall.exp, true);
        int random_item_length = NumberUtils.random(stall.items.length);
        player.getInventory().addItem(new Item(stall.items[random_item_length >= stall.items.length ? random_item_length - 1 : random_item_length], 1));
        player.getAttributes().remove("stopActions");
        return false;
    }
}
