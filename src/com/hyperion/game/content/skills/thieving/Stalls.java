package com.hyperion.game.content.skills.thieving;

/**
 * Stall data
 *
 * @author trees
 */
public enum Stalls {

    VEGETABLE_STALL(4708, 4276, 2, 10, 2, new int[]{1942, 1982, 1965, 1550, 1957}),
    BAKERS_STALL(11730, 634, 5, 16, 3, new int[]{1891, 1897}),
    CRAFTING_STALL(4874, 4797, 5, 16, 7, new int[]{1755, 1592, 1597}),
    GENERAL_STALL(4876, 4797, 5, 16, 7, new int[]{1931, 2347, 590}),
    FOOD_STALL(4875, 4797, 5, 16, 7, new int[]{1963}),
    TEA_STALL(635, 634, 5, 16, 5, new int[]{712}),
    SILK_STALL(11729, 634, 20, 17, 40, new int[]{950}),
    WINE_STALL(14011, 634, 22, 27, 10, new int[]{1937, 1993, 1987, 1935, 7919}),
    FRUIT_STALL(28823, 27537, 25, 29, 2, new int[]{1955, 1963, 5504, 1951, 247, 464, 2120, 2102, 2114, 5972}),
    SEED_STALL(7053, 634, 27, 10, 10, new int[]{5318, 5319, 5324, 5322, 5320, 5323, 5321, 5305, 5306, 5097, 5096, 5307, 5308, 5309, 5310, 5311}),
    FUR_STALL(4278, 4276, 35, 36, 10, new int[]{958}),
    FUR_STALL2(11732, 634, 35, 36, 10, new int[]{958}),
    FISH_STALL(4277, 4276, 42, 42, 10, new int[]{331, 359, 377}),
    //CROSSBOW_STALL(, , 49, 52, 9, new int[]{}),
    SILVER_STALL(11734, 634, 50, 54, 16, new int[]{1714, 442}),
    SPICE_STALL(11733, 634, 65, 34, 40, new int[]{2007, 1550, 946}),
    MAGIC_STALL(4877, 4797, 65, 100, 40, new int[]{556, 557, 554, 555, 563}),
    SCIMITAR_STALL(4878, 4797, 65, 160, 40, new int[]{1323, 1325}),
    GEM_STALL(11731, 634, 75, 42, 80, new int[]{1623, 1631, 1607, 1621, 1601, 1605, 1619, 1617});
    //ORE_STALL(, , 82, 180, 60, new int[]{});

    int id;
    int empty_id;
    int level;
    int exp;
    int respawn;
    int[] items;

    Stalls(int object_id, int empty_object_id, int level_required, int xp_received, int respawn_timer, int[] possible_items) {
        this.id = object_id;
        this.empty_id = empty_object_id;
        this.level = level_required;
        this.exp = xp_received;
        this.respawn = respawn_timer;
        this.items = possible_items;
    }

    public static Stalls forId(int id) {
        for (Stalls data : Stalls.values())
            if (data.id == id)
                return data;
        return null;
    }
    }
