package com.hyperion.game.content.skills.thieving;

import com.hyperion.game.Constants;
import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.content.randomevent.EvilBobEvent;
import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.utility.NumberUtils;
import com.hyperion.utility.Utils;

/**
 * For the thieving skill
 *
 * @author Konceal
 */
public class Thieving {

    private Player player;

    public Player getPlayer() {
        return player;
    }

    public Thieving(Player player) {
        this.player = player;
    }

    private enum Stall {

        BAKERS(11730, 1, 1897, 30), BAKERS2(11729, 1, 1897, 30), GEM(11731, 20, 1635, 60), FUR(2563, 40, 950, 90), SPICE(2564, 60, 7650, 105), SILVER(2565, 80, 1613, 135);

        private final int id, reqLevel, rewardId;
        private final double exp;

        public int getId() {
            return id;
        }

        public int getRequiredLevel() {
            return reqLevel;
        }

        public double getExperience() {
            return exp;
        }

        Stall(int id, int reqLevel, int rewardId, double exp) {
            this.id = id;
            this.reqLevel = reqLevel;
            this.rewardId = rewardId;
            this.exp = exp;
        }

        /**
         * @return the rewardId
         */
        public int getRewardId() {
            return rewardId;
        }

    }

    private static Stall getStall(int id) {
        for (Stall stall : Stall.values()) {
            if (stall.getId() == id) {
                return stall;
            }
        }
        return null;
    }

    public static boolean thieveStall(final Player player, final GameObject o) {
        final Stall stall = getStall(o.getId());
        if (stall == null) // not a stall
            return false;
        if (!o.isAlive())
            return false;
        if (stall.getRequiredLevel() > player.getSkills().getLevel(Skills.THIEVING)) {
            player.getPacketSender().sendMessage("You must have a Thieving level of " + stall.getRequiredLevel() + " to steal from this stall.");
            return true; // handled
        }
        if (player.getAttributes().isSet("stopActions"))
            return true;

        if (player.getAttributes().isSet("lastAnimation") && player.timeSinceLastAnimation() < 3600 && player.timeSinceLastAnimation() != 0) {
            // too soon
            return true;
        }
        if (player.getInventory().freeSlots() <= 0) {
            player.getPacketSender().sendMessage("You don't have enough inventory space to steal from this stall.");
            return true;
        }
        player.getAttributes().set("stopActions", true);
        player.animate(Animation.create(881));
        World.getWorld().submit(new Tickable(3) {
            @Override
            public void execute() {
                World.replaceResourceObject(o, NumberUtils.random(5, 9), player, 634);
                if (stall != null && stall.getRewardId() != -1) {
                    player.getInventory().addItem(stall.getRewardId(), 1);
                    player.getInventory().refresh();
                    player.getPacketSender().sendMessage("You steal some loot from the stall.");
                    addExperience(player, stall.getExperience(), true);
                }
                player.getAttributes().remove("stopActions");
                if (Utils.random(0, 300) > 298) {
                    EvilBobEvent.start(player);
                }
                this.stop();
            }
        });
        return true;
    }

    public static boolean thieveSafe(final Player player, final GameObject o) {
        if (o == null) // not a stall
            return false;
        if (50 > player.getSkills().getLevel(Skills.THIEVING)) {
            player.getPacketSender().sendMessage("You must have a Thieving level of 50 to crack this safe.");
            return true; // handled
        }
        if (player.getAttributes().isSet("stopActions"))
            return true;

        if (player.getAttributes().isSet("lastAnimation") && player.timeSinceLastAnimation() < 3600 && player.timeSinceLastAnimation() != 0) {
            return true;
        }
        if (player.getInventory().freeSlots() <= 0) {
            player.getPacketSender().sendMessage("You don't have enough inventory space to steal from this safe.");
            return true;
        }
        int capeId = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.CAPE.ordinal());
        if (Utils.random(0, (capeId >= 15345 && capeId <= 15349) ? 15 : 12) == 1) {
            player.getPacketSender().sendMessage("You fail to pick the lock!");
            player.animate(3170);
            if (player.getSkills().getLevel(Skills.HITPOINTS) <= 30) {
                player.inflictDamage(new Hit(Utils.random(1, 4), -1), false);
            } else {
                player.inflictDamage(new Hit(Utils.random(1, 10), -1), false);
                return true;
            }
            player.getAttributes().set("stopActions", true);
            World.getWorld().submit(new Tickable(3) {
                @Override
                public void execute() {
                    player.getAttributes().remove("stopActions");
                    this.stop();
                }
            });
        } else {
            player.getAttributes().set("stopActions", true);
            final int amt = Utils.random(1000, 20000);
            player.animate(Animation.create(2247));
            World.getWorld().submit(new Tickable(3) {
                @Override
                public void execute() {
                    player.getInventory().addItem(995, amt);

                    player.getPacketSender().sendMessage("You steal " + amt + " coins from the safe");
                    addExperience(player, 70, true);
                    player.getAttributes().remove("stopActions");

                    if (Utils.random(0, 12) == 1) {
                        Item item = new Item(Constants.RANDOM_GEM[Utils.random(0, Constants.RANDOM_GEM.length - 1)], 1);
                        player.getPacketSender().sendMessage("You find a gem inside the safe!");
                        player.getInventory().addItem(item);
                    }

                    player.getInventory().refresh();
                    if (Utils.random(0, 300) > 298) {
                        EvilBobEvent.start(player);
                    }
                    this.stop();
                }
            });
        }
        return true;

    }

    private static PickpocketableNPC getNpc(int id) {
        for (PickpocketableNPC npc : PickpocketableNPC.values()) {
            if (npc.getId() == id) {
                return npc;
            }
        }
        return null;
    }

    private static boolean randomFail(final Player player, final int levelReq) {
        int skill = player.getSkills().getLevel(Skills.THIEVING);
        int bonus;
        if (skill < 10) {
            bonus = 15;
        } else {
            bonus = 1;
        }
        return (player.getSkills().getLevel(Skills.THIEVING) + bonus) - levelReq + getEquipmentBonus(player) < NumberUtils.random(levelReq) + 15;
    }

    private static int getEquipmentBonus(final Player player) {
        int capeId = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.CAPE.ordinal());
        return (player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.GLOVES.ordinal()) == 10075 ? 5 : 0)
                + (((capeId >= 9777 && capeId <= 9778) ? 5 : 0) + (player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.AMULET.ordinal()) == 21143 ? 7 : 0));
    }

    public static boolean pickpocketNpc(final Player player, final NPC npc) {
        if (npc == null || npc.isDestroyed() || npc.getCombatState().isDead())
            return false;
        final PickpocketableNPC pNpc = getNpc(npc.getId());
        if (pNpc == null) // not a pickpocketable npc
            return false;
        if (!player.getCombatState().isOutOfCombat("You can't do this during combat!", true)) {
            return false;
        }
        if (pNpc.getReq() > player.getSkills().getLevel(Skills.THIEVING)) {
            player.getPacketSender().sendMessage("You must have a Thieving level of " + pNpc.getReq() + " to pickpocket " + (pNpc == PickpocketableNPC.MAN ? "Men." : npc.getDefinition().getName().replace("_", " ") + "s."));
            return true; // handled
        }
        if (player.getAttributes().isSet("stopActions"))
            return true;
        if (player.getInventory().freeSlots() <= 0) {
            player.getPacketSender().sendMessage("You don't have enough inventory space to steal from this stall.");
            return true;
        }
        player.getPacketSender().sendMessage("You attempt to picketpocket the " + npc.getDefinition().getName().replace("_", " ") + "...");
        player.getAttributes().set("stopActions", true);
        player.animate(Animation.create(881));
        World.getWorld().submit(new Tickable(3) {
            @Override
            public void execute() {
                if (!randomFail(player, pNpc.getReq())) {
                    final int amt = pNpc.getReward();
                    PickpocketLoot.generate(player, npc);
                    player.getInventory().refresh();
                    if (getEquipmentBonus(player) > 0) {
                        player.getPacketSender().sendMessage("Your equipment helped your pickpocket success.");
                    }
                    player.getPacketSender().sendMessage("You successfully pickpocket the " + npc.getDefinition().getName().replace("_", " ") + ".");
                    addExperience(player, pNpc.getExp(), false);

                    player.getAttributes().remove("stopActions");
                } else {
                    npc.face(player.getLocation());
                    npc.animate(Animation.create(npc.getDefinition().getAttackAnimation(), AnimationPriority.HIGH));
                    npc.playForcedChat("What do you think you're doing in my pockets?!");
                    player.playGraphic(80);
                    // player.inflictDamage(new Hit(pNpc.getDamage(), 0), false);
                    player.getPacketSender().sendMessage("You failed to pickpocket the " + npc.getDefinition().getName().replace("_", " ") + ".");
                    World.getWorld().submit(new Tickable(4) {
                        @Override
                        public void execute() {
                            player.playGraphic(-1);
                            player.getAttributes().remove("stopActions");
                            this.stop();
                        }
                    });
                }
                this.stop();
            }
        });
        return true;

    }

    public static boolean addExperience(Player player, double xp, boolean stall) {
        player.getSkills().getLevelForXp(Skills.THIEVING);
        player.getSkills().addExperience(Skills.THIEVING, xp);
        if (stall)
            Achievements.increase(player, 19);
        else
            Achievements.increase(player, 8);
        return false;
    }
}