package com.hyperion.game.content.skills.woodcutting;

import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.item.Item;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.game.world.grounditems.GroundItem;
import com.hyperion.game.world.grounditems.GroundItemManager;
import com.hyperion.utility.NumberUtils;

/**
 * Woodcutting cut action
 *
 * @author Nando / Konceal
 * @author trees
 */
public class CutSession extends Tickable {

    // TODO: Bird's nests

    private Player player;
    private GameObject tree;
    private int index = -1, delay = 0;
    private int current_anim = -1, animationDelay = 0;
    private boolean started = false;

    private CutSession(Player player, GameObject tree) {
        super(1, true);
        this.player = player;
        this.tree = tree;
        player.getVariables().setCutSession(this);
    }

    public static boolean attempt_woodcut(Player player, GameObject tree) {
        int index = WoodcuttingData.getIndex(tree);
        if (index == -1) {
            return false;
        }
        if (!hasAxe(player)) {
            player.sendMessage("You do not have an axe you can use.");
            return false;
        } else if (getUsedAxe(player) == 8 && player.getSkills().getLevel(Skills.FIREMAKING) <= 85) {
            player.sendMessage("You do not have the required firemaking to use this axe.");
        }
        CutSession session = new CutSession(player, tree);
        session.index = index;
        World.getWorld().submit(session);
        return true;
    }

    private void startCutting(boolean re_clicked) {
        if (re_clicked) {
            Tree newTree = new Tree(index, tree.getId(), tree.getLocation(), WoodcuttingData.LOGS[index], WoodcuttingData.LEVEL[index], WoodcuttingData.TREE_NAME[index], WoodcuttingData.XP[index]);
            player.getAttributes().set("cuttingTree", newTree);
        }
        final Tree treeToCut = (Tree) player.getAttributes().get("cuttingTree");
        if (stopCutting(treeToCut)) {
            reset();
            return;
        }
        if (re_clicked) {
            this.current_anim = getAxeAnimation(player);
        }
        if (re_clicked) {
            player.getPacketSender().sendMessage("You begin to swing your axe at the tree..");
        }
        this.delay = getCycles(player, treeToCut);
        if (!re_clicked) {
            if (player.getInventory().addItem(treeToCut.getLog())) {
                if (player.getDetails().isDonator()) {
                    int notedChance = NumberUtils.random(3);
                    if (notedChance == 1) {
                        int noted_id = ItemDefinition.forId(treeToCut.getLog()) != null ? treeToCut.getLog() + 1 : -1;
                        if (noted_id > -1)
                            player.getInventory().addItem(new Item(noted_id, 1));
                    }
                }

                if (player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.WEAPON.ordinal()) == 13241
                        || player.getInventory().containsItem(13241, 1)) {
                    if (NumberUtils.random(3) == 0) {
                        player.getSkills().addExperience(Skills.FIREMAKING, treeToCut.getXp());
                        player.getInventory().deleteItem(treeToCut.getLog(), 1);
                        player.getPacketSender().sendMessage("Your axe burned the " + tree.getName() + " logs.");
                        player.playGraphic(new Graphic(1776, 0, 100));
                    }
                }
                player.getInventory().refresh();
                player.getInterfaceSettings().closeInterfaces(false);
                int index = treeToCut.getTreeIndex();
                player.getSkills().addExperience(Skills.WOODCUTTING, treeToCut.getXp());
                if (index == 6) {
                    player.getPacketSender().sendMessage("You retrieve some Hollow bark from the tree.");
                } else {
                    if (!tree.getName().equalsIgnoreCase("tree")) {
                        player.getPacketSender().sendMessage("You cut down some " + tree.getName() + " logs.");
                    } else {
                        player.getPacketSender().sendMessage("You cut down some logs.");
                        Achievements.increase(player, 0);
                        Achievements.increase(player, 30);
                    }
                }
            }
        }
    }

    private boolean stopCutting(Tree tree) {
        if (player.getAttributes().isSet("stopActions")) {
            return true;
        }
        if (player.getSkills().getLevel(Skills.WOODCUTTING) < tree.getLevel()) {
            player.getPacketSender().sendMessage("You need a Woodcutting level of " + tree.getLevel() + " to cut that tree.");
            return true;
        }
        if (getUsedAxe(player) == -1) {
            player.getPacketSender().sendMessage("You do not have an axe that you can use.");
            return true;
        }
        if (player.getInventory().findFreeSlot() < 0) {
            player.getPacketSender().sendMessage("Your inventory is too full to carry any logs.");
            return true;
        }
        if (!this.tree.isAlive()) {
            player.getPacketSender().sendMessage("The tree has run out of logs.");
            return true;
        }
        return false;
    }

    public void reset() {
        player.getVariables().setCutSession(null);
        player.animate(-1);
        this.stop();
    }

    public int getObjectRespawnTimer() {
        int index = WoodcuttingData.getIndex(tree);
        if (index == -1)
            return 15; // ticks
        return WoodcuttingData.RESPAWN_TIME[index];
    }

    @Override
    public void execute() {
        int index = WoodcuttingData.LOGS[WoodcuttingData.getIndex(tree)];
        if (player.isDestroyed()) {
            this.stop();
            return;
        }

        if (player.getAttributes().isSet("stopActions")) {
            reset();
            return;
        }

        System.out.println("tree health " + tree.getHealth());
        if (tree.getHealth() == -1) {
            // System.out.println("healthsfdfsdfsdf" + tree.getHealth());
            World.replaceResourceObject(tree, getObjectRespawnTimer(), player, 1342);
            if (player.getInventory().freeSlots() > 0) {
                player.getInventory().addItem(index);
            } else if (player.getInventory().findFreeSlot() == -1) {
                GroundItemManager.create(new GroundItem(new Item(WoodcuttingData.LOGS[WoodcuttingData.getIndex(tree)], 1), player).setSpawnDelay(-1).setGlobal(true).setRemovalDelay(-1), player);
                player.getPacketSender().sendMessage("Your inventory is too full to carry any logs.");
            }
            tree.setAlive(false);
            reset();
            this.stop();
            return;
        } else

        {
            tree.setAlive(true);
            tree.deductHealth(1);
            if (tree.getHealth() <= 0) {
                tree.setHealth(-1);
            }
        }

        if (delay > 0)

        {
            delay--;
        } else if (delay <= 0)

        {
            if (!started) {
                started = true;
                startCutting(true);
            } else {
                startCutting(false);
            }
        }
        if (animationDelay <= 0)

        {
            player.animate(current_anim);
            animationDelay = 3;
        } else

        {
            animationDelay--;
        }

    }

    private static int getCycles(Player player, Tree tree) {
        int skill = player.getSkills().getLevel(Skills.WOODCUTTING);
        int level = tree.getLevel();
        int modifier = tree.getLevel();
        int randomAmt = NumberUtils.random(3);
        double cycleCount = Math.ceil((level * 50 - skill * 10) / modifier * 0.25 - randomAmt * 4);
        if (cycleCount < 1) {
            cycleCount = 1;
        }
        return (int) cycleCount + 1;
    }

    private static int getAxeAnimation(Player p) {
        int axe = getUsedAxe(p);
        if (axe == -1) {
            return 65535;
        }
        return WoodcuttingData.AXE_ANIMATION[axe];
    }

    /**
     * Does player have an axe
     *
     * @param player
     * @return
     */
    private static boolean hasAxe(Player player) {
        return getUsedAxe(player) != -1;
    }

    /**
     * Get current axe to use
     *
     * @param p
     * @return
     */
    private static int getUsedAxe(Player p) {
        int index = -1;
        for (int i = 0; i < WoodcuttingData.AXES.length; i++) {
            if (p.getEquipment().getItemInSlot(3) == WoodcuttingData.AXES[i] || p.getInventory().hasItem(WoodcuttingData.AXES[i])) {
                if (p.getSkills().getLevel(Skills.WOODCUTTING) >= WoodcuttingData.AXE_LVL[i]) {
                    index = i;
                }
            }
        }
        return index;
    }
}
