package com.hyperion.game.content.skills.woodcutting;

import com.hyperion.game.world.Location;

public class Tree {

	private int treeIndex;
	private Location treeLocation;
	private int log;
	private int level;
	private double xp;
	private int treeId;
	private String name;
	
	public Tree(int index, int treeId, Location loc, int log, int level, String name, double xp) {
		this.treeIndex = index;
		this.treeLocation = loc;
		this.log = log;
		this.treeId = treeId;
		this.level = level;
		this.xp = xp;
		this.name = name;
	}

	public int getTreeIndex() {
		return treeIndex;
	}

	public Location getTreeLocation() {
		return treeLocation;
	}

	public int getLog() {
		return log;
	}

	public int getLevel() {
		return level;
	}

	public double getXp() {
		return xp;
	}

	public int getTreeId() {
		return treeId;
	}

	public String getName() {
		return name;
	}
}
