package com.hyperion.game.content.skills.woodcutting;

import com.hyperion.game.world.gameobjects.GameObject;

public class WoodcuttingData {

    public WoodcuttingData() {

    }

    protected static final int WOODCUTTING = 8;

    protected static final int[] AXES = {
            1351, // Bronze
            1349, // Iron
            1353, // Steel
            1361, // Black
            1355, // Mithril
            1357, // Adamant
            1359, // Rune
            6739, // Dragon
            13241, // Infernal
    };

    protected static final int[] AXE_LVL = {
            1, // Bronze
            1, // Iron
            6, // Steel
            6, // Black
            21, // Mithril
            31, // Adamant
            41, // Rune
            61, // Dragon
            61, // Inferno adze
    };

    protected static final int[] AXE_ANIMATION = {
            879, // Bronze
            877, // Iron
            875, // Steel
            873, // Black
            871, // Mithril
            869, // Adamant
            867, // Rune
            2846, // Dragon
            2117, // Infernal
    };

    public static final int[] AXE_DELAY = {
            1, // Bronze
            2, // Iron
            3, // Steel
            4, // Black
            5, // Mithril
            6, // Adamant
            7, // Rune
            8, // Dragon
    };

    protected static final double[] XP = {
            25, // Normal logs
            37.5, // Oak logs
            67.5, // Willow logs
            25, // Achey logs
            85, // Teak logs
            100, // Maple logs
            82.5, // Hollow bark
            125, // Mahogany logs
            140.2, // Arctic pine logs
            165, // Eucalyptus logs
            175, // Yew logs
            250, // Magic logs
    };

    protected static final int[] RESPAWN_TIME = {
            30, // Normal logs
            50, // Oak logs
            60, // Willow logs
            60, // Achey logs
            55, // Teak logs
            85, // Maple logs
            60, // Hollow bark
            60, // Mahogany logs
            90, // Arctic pine logs
            90, // Eucalyptus logs
            100, // Yew logs
            130, // Magic logs
    };

    protected static final int[] LEVEL = {
            1, // Normal logs
            15, // Oak logs
            30, // Willow logs
            1, // Achey logs
            35, // Teak logs
            45, // Maple logs
            45, // Hollow bark
            50, // Mahogany logs
            54, // Arctic pine logs
            58, // Eucalyptus logs
            60, // Yew logs
            75, // Magic logs
    };

    protected static final String[] TREE_NAME = {
            "Normal", // Normal logs
            "Evergreen", // normal logs
            "Oak", // Oak logs
            "Willow", // Willow logs
            "Achey", // Achey logs
            "Teak", // Teak logs
            "Maple", // Maple logs
            "Bark", // Hollow bark
            "Mahogany", // Mahogany logs
            "Arctic pine", // Arctic pine logs
            "Eucalyptus", // Eucalyptus logs
            "Yew", // Yew logs
            "Magic", // Magic logs
    };

    protected static final int[] LOGS = {
            1511, // Normal logs
            1521, // Oak logs
            1519, // Willow logs
            2862, // Achey logs
            6333, // Teak logs
            1517, // Maple logs
            3239, // Hollow bark
            6332, // Mahogany logs
            10810, // Arctic pine logs
            12581, // Eucalyptus logs
            1515, // Yew logs
            1513, // Magic logs
    };

    protected static final int[] TREES = {
            1276, // Normal tree
            1278, // Normal tree
            2409, // Normal tree
            1277, // Normal tree with but different coloured stump
            3034, // Normal tree with dark stump -- TODO correct stump
            3033, // Normal tree with dark stump -- TODO correct stump
            10041, // Normal tree
            1282, // Dead tree
            1283, // Dead tree
            1284, // Dead tree
            1285, // Dead tree
            1286, // Dead tree
            1289, // Dead tree
            1290, // Dead tree
            1365, // Dead tree
            1383, // Dead tree
            1384, // Dead tree
            1291, // Dead tree
            3035, // Dead tree
            3036, // Dead tree
            1315, // Evergreen
            1316, // Evergreen
            1318, // Snowy Evergreen
            1319, // Snowy Evergreen
            1330, // Snow covered tree
            1331, // Snow covered tree
            1332, // Snow covered tree
            3879, // Evergreen from elf land
            3881, // Evergreen from elf land (slightly bigger than one above)
            3882, // Evergreen from elf land (slightly bigger than one above)
            3883, // Small Evergreen from elf land
            1281, // Normal Oak tree
            9036, //Teak - cwars
            3037, // Oak tree dark stump
            1308, // Normal Willow tree
            5551, // Normal Willow tree
            5552, // Normal Willow tree
            5553, // Normal Willow tree
            2023, // Normal Achey tree
            9036, // Normal Teak tree
            15062, // Normal Teak tree (same as above?)
            1307, // Normal Maple tree
            4674, // Exactly same as above
            2289, // Normal Hollow tree
            4060, // Normal Hollow tree (bigger than above)
            9034, // Normal Mahogany tree
            4674, // Exactly same as above
            1280, // Normal tree
            24168, // Dying tree in Varrock
            21273, // Arctic pine
            28951, // Normal Eucalyptus tree
            28952, // Normal Eucalyptus tree (smaller)
            28953, // Normal Eucalyptus tree (smallest)
            1309, // Normal Yew tree
            1306, // Normal Magic tree
            14309,
            1761, //PC game island tree - TODO stump
    };

    public static int getIndex(GameObject object) {
        int object_id = object.getId();
        if (object_id == 1276 || object_id == 1278 || object_id == 2091 || object_id == 2092) { //Trees - Evergreens
            return 0;
        } else if (object_id == 1751) { // Oak
            return 1;
        } else if (object_id == 1750 || object_id == 1756 || object_id == 1758 || object_id == 1760) { // Willow
            return 2;
        } else if (object_id == 1759) { // Maple
            return 5;
        } else if (object_id == 1753) { // Yew
            return 10;
        } else if (object_id == 1761) { // Magic
            return 11;
        }
        return -1;
    }

    public static int getHealth(GameObject object) {
        int object_id = object.getId();
        if (object_id == 1276 || object_id == 1278 || object_id == 2091 || object_id == 2092) { //Trees - Evergreens
            return 15;
        } else if (object_id == 1751) { // Oak
            return 30;
        } else if (object_id == 1750 || object_id == 1756 || object_id == 1758 || object_id == 1760) { // Willow
            return 45;
        } else if (object_id == 1759) { // Maple
            return 50;
        } else if (object_id == 1753) { // Yew
            return 130;
        } else if (object_id == 1761) { // Magic
            return 150;
        }
        return -1;
    }

}
