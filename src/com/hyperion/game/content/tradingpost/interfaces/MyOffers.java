package com.hyperion.game.content.tradingpost.interfaces;

import com.hyperion.Logger;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;

public class MyOffers {

    /**
     * @param string
     */
    static void print(String string) {
        Logger.getInstance().info("[MyOffers] : Print / " + string);
    }

    public static void open(Player player) {
        empty(player);
        buildFakeSales(player);
        player.getPacketSender().displayInterface(671);
        player.getAttributes().set("grandexchangemenu", 671);
        player.sendMessage("One of your Grand Exchange offers has updated!");
    }

    static boolean show_box_1 = true;
    static boolean show_box_2 = true;
    static boolean show_box_3 = true;
    static boolean show_box_4 = true;

    static boolean show_box_5 = true;
    static boolean show_box_6 = true;
    static boolean show_box_7 = true;
    static boolean show_box_8 = true;


    public static void buildFakeSales(Player player) {
        if (show_box_1) {
            //TODO
        }
        if (show_box_2) {
            // TODO
        }
        if (show_box_3) {
        }
        if (show_box_4) {
            //OFFER SELL STATE 4
            player.getPacketSender().sendInterfaceConfig(671, 49, true); // BOX 4 GREEN BAR
            player.getPacketSender().modifyText(ItemDefinition.forId(20997).getName(), 671, 51); // BOX 4 Name
            player.getPacketSender().modifyText(NumberUtils.format(ItemDefinition.forId(20997).getGeneralPrice()) + " coins", 671, 52); // BOX 4 Price
            player.getPacketSender().itemOnInterface(671, 50, 100, 20997); // BOX 4 ITEM
            player.getPacketSender().modifyText("Buy", 671, 80); // BOX 4 B/S TITLE

        }
        if (show_box_5) {
            // TODO
        }
        if (show_box_6) {
            // TODO
        }
        if (show_box_7) {
            // TODO
        }
        if (show_box_8) {
            // TODO
        }
    }

    public static void empty(Player player) {

        //BOX 1
        player.getPacketSender().sendInterfaceConfig(671, 25, false); // BOX 17 EMPTY BAR
        player.getPacketSender().sendInterfaceConfig(671, 26, false); // BOX 17 YELLOW BAR
        player.getPacketSender().sendInterfaceConfig(671, 27, false); // BOX 17 GREEN BAR
        player.getPacketSender().modifyText("", 671, 29); // BOX 17 Name
        player.getPacketSender().modifyText("", 671, 30); // BOX 17 Price

        //BOX 2
        player.getPacketSender().sendInterfaceConfig(671, 31, false); // BOX 18 EMPTY BAR
        player.getPacketSender().sendInterfaceConfig(671, 32, false); // BOX 18 YELLOW BAR
        player.getPacketSender().sendInterfaceConfig(671, 33, false); // BOX 18 GREEN BAR
        player.getPacketSender().modifyText("", 671, 35); // BOX 18 Name
        player.getPacketSender().modifyText("", 671, 36); // BOX 18 Price

        // BOX 3
        player.getPacketSender().sendInterfaceConfig(671, 37, false); // BOX 19 EMPTY BAR
        player.getPacketSender().sendInterfaceConfig(671, 38, false); // BOX 19 YELLOW BAR
        player.getPacketSender().sendInterfaceConfig(671, 39, false); // BOX 19 GREEN BAR
        player.getPacketSender().modifyText("", 671, 41); // BOX 19 NAME
        player.getPacketSender().modifyText("", 671, 42); // BOX 19 PRICE

        //BOX 4
        player.getPacketSender().sendInterfaceConfig(671, 47, false); // BOX 20 EMPTY BAR
        player.getPacketSender().sendInterfaceConfig(671, 48, false); // BOX 20 YELLOW BAR
        player.getPacketSender().sendInterfaceConfig(671, 49, false); // BOX 20 GREEN BAR
        player.getPacketSender().modifyText("", 671, 51); // BOX 20 Name
        player.getPacketSender().modifyText("", 671, 52); // BOX 20 Price

        //BOX 5
        player.getPacketSender().sendInterfaceConfig(671, 53, false); // BOX 21 EMPTY BAR
        player.getPacketSender().sendInterfaceConfig(671, 54, false); // BOX 21 YELLOW BAR
        player.getPacketSender().sendInterfaceConfig(671, 55, false); // BOX 21 GREEN BAR
        player.getPacketSender().modifyText("", 671, 57); // BOX 21 Name
        player.getPacketSender().modifyText("", 671, 58); // BOX 21 Price

        //BOX 6
        player.getPacketSender().sendInterfaceConfig(671, 59, false); // BOX 22 EMPTY BAR
        player.getPacketSender().sendInterfaceConfig(671, 60, false); // BOX 22 YELLOW BAR
        player.getPacketSender().sendInterfaceConfig(671, 61, false); // BOX 22 GREEN BAR
        player.getPacketSender().modifyText("", 671, 63); // BOX 22 Name
        player.getPacketSender().modifyText("", 671, 64); // BOX 22 Price

        //BOX 7
        player.getPacketSender().sendInterfaceConfig(671, 65, false); // BOX 23 EMPTY BAR
        player.getPacketSender().sendInterfaceConfig(671, 66, false); // BOX 23 YELLOW BAR
        player.getPacketSender().sendInterfaceConfig(671, 67, false); // BOX 23 GREEN BAR
        player.getPacketSender().modifyText("", 671, 69); // Name
        player.getPacketSender().modifyText("", 671, 70); // Price

        //BOX 8
        player.getPacketSender().sendInterfaceConfig(671, 71, false); // BOX 24 EMPTY BAR
        player.getPacketSender().sendInterfaceConfig(671, 72, false); // BOX 24 YELLOW BAR
        player.getPacketSender().sendInterfaceConfig(671, 73, false); // BOX 24 GREEN BAR
        player.getPacketSender().modifyText("", 671, 75); // BOX 24 Name
        player.getPacketSender().modifyText("", 671, 76); // BOX 24 Price

        // 1-4 BUY SELL TITLES
        player.getPacketSender().modifyText("", 671, 77); // BOX 1 B/S TITLE
        player.getPacketSender().modifyText("", 671, 78); // BOX 2 B/S TITLE
        player.getPacketSender().modifyText("", 671, 79); // BOX 3 B/S TITLE
        player.getPacketSender().modifyText("", 671, 80); // BOX 4 B/S TITLE

        // 5-8 BUY SELL TITLES
        player.getPacketSender().modifyText("", 671, 81); // BOX 5 B/S TITLE
        player.getPacketSender().modifyText("", 671, 82); // BOX 6 B/S TITLE
        player.getPacketSender().modifyText("", 671, 83); // BOX 7 B/S TITLE
        player.getPacketSender().modifyText("", 671, 84); // BOX 8 B/S TITLE

        //BANNED NOTIFICATION
        player.getPacketSender().sendInterfaceConfig(671, 85, true); // RED BOX
        player.getPacketSender().sendInterfaceConfig(671, 86, true); // TEXT

    }
}
