package com.hyperion.game.content.wilderness;

import java.util.Collections;

import com.hyperion.game.item.Item;
import com.hyperion.game.world.TeleportAreaLocations;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.Container;
import com.hyperion.game.world.grounditems.GroundItem;
import com.hyperion.game.world.grounditems.GroundItemManager;
import com.hyperion.utility.NumberUtils;

public class BountyHunter {

    public static final int TARGET_NAME_CHILD = 4, LEVEL_CHILDS_CHILD = 5,
            WEALTH_CHILD = 6;

    private static final int[] BOUNTY_INTERFACE_CONTAINERS = new int[]{10, 11, 13, 15, 17, 19};


    public static void sendInterfaceDetails(Player player, Player other) {
        int difference = Math.abs(player.getWildLevel() - other.getWildLevel());
        player.getPacketSender()
                .modifyText(
                        "<col="
                                + (difference > 10 ? "8C0404"
                                : difference > 5 ? "8C3204" : "3D8205")
                                + ">"
                                + (other.getAttributes().isSet("wildyTimer") ? "Safezone"
                                : getLevels(other)) + ", Cmb "
                                + other.getSkills().getCombatLevel(), 650,
                        LEVEL_CHILDS_CHILD);
        player.getPacketSender().modifyText("" + other.getName(), 650,
                TARGET_NAME_CHILD);
        modifyBountyIcon(player, other);
    }

    private static void modifyBountyIcon(Player player, Player other) {
        int wealth = getPlayerWealth(other);

        player.getPacketSender().modifyText(
                "Wealth: " + (wealth > 1500000 ? "V. High" : wealth > 1000000 ? "High"
                        : wealth > 250000 ? "Medium" : wealth > 100000 ? "Low" : "V. Low"), 650, WEALTH_CHILD);

        int showChild = wealth > 1500000 ? 19 : wealth > 1000000 ? 17
                : wealth > 250000 ? 15 : wealth > 100000 ? 13 : 11;
        player.getPacketSender().sendInterfaceConfig(650, showChild, true);
        for (int i : BOUNTY_INTERFACE_CONTAINERS) {
            if (i != showChild)
                player.getPacketSender().sendInterfaceConfig(650, i, false);
        }
        //1221-1225
    }

    /*
     * Gets the combined value of the other players equipment and inventory(alch
     * values)
     */
    public static int getPlayerWealth(Player player) {
        if (player == null)
            return 0;
        Container itemsLost = player.getItemsKeptOnDeath()[1];

        int value = 0;

        for (Item item : itemsLost.getItems()) {
            if (item != null)
                if (BountyHunterEmblem.isEmblem(item.getId())) {
                    int index = BountyHunterEmblem.getEmblemForId(item.getId()).getIndex();
                    value = value + (index == 1 ? 100_000 : index > 1 ? 100_000 * index : 0);
                } else
                    value = value + (item.getDefinition().getGeneralPrice() * item.getCount());
        }
        return value;
    }

    /*
     * Our target has left the Wilderness(this is called after 2 minute timer),
     * so we add ourselves back to the target list and reset variables.
     */
    public static void targetLeft(Player player, boolean killed) {
        World.getWorld().getPossibleTargets().add(player.getName());
        player.getAttributes().remove("target");
        player.getPacketSender().sendMessage(
                killed ? "You have defeated your target. You will be assigned a new one shortly."
                        : "Your target has left the Wilderness. You will be assigned a new one shortly.");
        player.getPacketSender().sendHintArrow(null);
        sendBlankInterface(player);
    }

    /*
     * Gets the Wilderness levels our target is between.
     */

    public static String getLevels(Player other) {
        // TODO: Color
        int otherWildy = other.getWildLevel();
        int low = otherWildy - 2;
        int high = otherWildy + 1;
        return "Lvl: "
                + (otherWildy <= 4 ? "1-4" : ((low) + "-" + (high > 56 ? 56
                : high)));
    }

    /*
     * Finds us a target from the list of possible targets, whilst removing
     * non-existent players and those outside of the wilderness.
     */

    public static void findTarget(Player player) {
        Collections.shuffle(World.getWorld().getPossibleTargets());
        int cb = player.getSkills().getCombatLevel();
        for (String name : World.getWorld().getPossibleTargets()) {
            if (name == null) {
                continue;
            }
            if (name.equalsIgnoreCase(player.getName()))
                continue;

            Player other = World.getWorld().find_player_by_name(name);

            if (other == null) {
                World.getWorld().getPossibleTargets().remove(name);
                World.getWorld().getPlayersInWilderness().remove(name);
                continue;
            }

            // Target not in Wilderness anymore
            if (!TeleportAreaLocations.isInPVP(other)) {
                other.getAttributes().remove("target");
                World.getWorld().getPossibleTargets().remove(name);
                World.getWorld().getPlayersInWilderness().remove(name);
                continue;
            }
            // Target already has target
            if (other.getAttributes().isSet("target")) {
                World.getWorld().getPossibleTargets().remove(name);
                continue;
            }

            int otherCb = other.getSkills().getCombatLevel();
            if (otherCb >= (cb - 4) && otherCb <= (cb + 4)) {
                player.getAttributes().set("target", other.getName());
                player.getPacketSender().sendMessage("You've been assigned a target!");
                sendInterfaceDetails(player, other);
                player.getPacketSender().sendHintArrow(other);

                other.getAttributes().set("target", player.getName());
                other.getPacketSender().sendMessage("You've been assigned a target!");
                other.getPacketSender().modifyText("" + player.getName(), 650, 4);
                other.getPacketSender().sendHintArrow(player);
                sendInterfaceDetails(other, player);
                return;
            } else {
                continue;
            }

        }
    }

    public static Container handleDrop(Player player, Player pKiller, Container itemsToDrop) {
        boolean isOurTarget = ((String) pKiller.getAttributes().get("target")).equalsIgnoreCase(player.getName());
        boolean isLastTarget = pKiller.getVariables().getLastTarget().equalsIgnoreCase(player.getName());
        boolean sameIP = pKiller.getDetails().getIP().equalsIgnoreCase(player.getDetails().getIP());
        boolean sameMAC = pKiller.getDetails().getMacAddress().length() > 0
                && pKiller.getDetails().getMacAddress().equalsIgnoreCase(player.getDetails().getMacAddress());

        if (isOurTarget && !isLastTarget && !(sameMAC || sameIP)) {
            boolean canReceiveEmblem = true;
            for (Item item : itemsToDrop.getItems()) {
                if (item != null && BountyHunterEmblem.isEmblem(item.getId())) {
                    BountyHunterEmblem emblem = BountyHunterEmblem.getEmblemForId(item.getId());
                    if (emblem != null)
                        item.setItemId(emblem.getPreviousOrFirst().getItemId());
                }
            }

            BountyHunterEmblem bestEmblem = BountyHunterEmblem.getBest(pKiller, true);//.get();
            if (bestEmblem != null) {
                for (Item item : pKiller.getInventory().getItems()) {
                    if (item != null && BountyHunterEmblem.isEmblem(item.getId())) {
                        BountyHunterEmblem emblem = BountyHunterEmblem.getEmblemForId(item.getId());
                        if (emblem != null && item.getId() == bestEmblem.getItemId()) {
                            pKiller.getPacketSender().sendMessage("Your " + item.getDefinition().getName() + " has been upgraded.");
                            item.setItemId(emblem.getNextOrLast().getItemId());
                            canReceiveEmblem = false;
                            pKiller.getInventory().refresh();
                            break;
                        }
                    }
                }
            }

            int upperEnd = 300 + player.getSkills().getCombatLevel() + (getPlayerWealth(player) / 10)
                    + (pKiller.getVariables().getKillStreak() * 10);

            if (canReceiveEmblem && NumberUtils.random(0, upperEnd) > 250) {
                if ((player.getDetails().isIronman() && pKiller.getDetails().isIronman()) || !player.getDetails().isIronman()) {
                    GroundItemManager.create(new GroundItem(new Item(BountyHunterEmblem.TIER_1.getItemId(), 1), player, pKiller), pKiller);
                    pKiller.getPacketSender().sendMessage("A Mysterious emblem has appeared on the ground.");
                }
            }
        } else {
            pKiller.getPacketSender().sendMessage("You have killed this player too recently to receive an emblem.");
        }
        return itemsToDrop;
    }

    public static void sendBlankInterface(Player player) {
        player.getPacketSender().modifyText("None", 650, TARGET_NAME_CHILD);
        player.getPacketSender().modifyText("---", 650, WEALTH_CHILD);
        player.getPacketSender().modifyText("Level: -----", 650, LEVEL_CHILDS_CHILD);
        player.getPacketSender().sendInterfaceConfig(650, 10, true);
        for (int i : BOUNTY_INTERFACE_CONTAINERS) {
            if (i != 10)
                player.getPacketSender().sendInterfaceConfig(650, i, false);
        }
    }
}
