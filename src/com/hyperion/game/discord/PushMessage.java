package com.hyperion.game.discord;

import com.google.common.util.concurrent.FutureCallback;
import de.btobastian.javacord.DiscordAPI;
import de.btobastian.javacord.Javacord;
import de.btobastian.javacord.entities.message.Message;
import de.btobastian.javacord.listener.message.MessageCreateListener;

/**
 * Pushes messages via my server slave
 *
 * @author trees
 */
public class PushMessage {

    public String message = "null";

    public PushMessage(String mess, String email, String password) {
        this.message = mess;
        DiscordAPI api = Javacord.getApi(email, password);

        // connect
        api.connect(new FutureCallback<DiscordAPI>() {
            @Override
            public void onSuccess(DiscordAPI api) {

                //Send the message
                api.getChannelById("412443048371290120").sendMessage(message);

                // register listener
                api.registerListener(new MessageCreateListener() {
                    @Override
                    public void onMessageCreate(DiscordAPI api, Message message) {
                        // check the content of the message
                        if (message.getContent().equalsIgnoreCase("ping")) {
                            // reply to the message
                            message.reply("pong");
                        }
                    }
                });
            }

            @Override
            public void onFailure(Throwable t) {
                System.out.println("discord ping failure");
                t.printStackTrace();
            }
        });
    }
}