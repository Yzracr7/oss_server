package com.hyperion.game.event;

import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.utility.game.ClickOption;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Apr 3, 2015
 */
public abstract class AbstractEvent {

	/**
	 * The array of keys that are binded to this event
	 * 
	 * @return A {@code Integer} {@code Array}
	 */
	public abstract int[] getKeyBinds();

	/**
	 * The type of event this is
	 * 
	 * @return
	 */
	public abstract EventType getEventType();

	/**
	 * Handles the way a player interacts with an npc
	 * 
	 * @param player
	 *            The player
	 * @param npc
	 *            The npc
	 * @param option
	 *            The option
	 * @return
	 */
	public abstract boolean handleNPCInteraction(Player player, NPC npc, ClickOption option);

	/**
	 * Handles the way a player interacts with an object
	 * 
	 * @param player
	 *            The player
	 * @param object
	 *            The object
	 * @param option
	 *            The option
	 * @return
	 */
	public abstract boolean handleObjectInteraction(Player player, GameObject object, ClickOption option);

	/**
	 * Handles the way a player interacts on an inter
	 * 
	 * @param player
	 *            The player
	 * @param interfaceId
	 *            The inter id
	 * @param buttonId
	 *            The button id
	 * @param slotId
	 *            The slot id
	 * @param packetId
	 *            The packet id
	 * @return
	 */
	public abstract boolean handleInterfaceInteraction(Player player, int interfaceId, int buttonId, int slotId, int packetId);

	public enum EventType {
		NPC, INTERFACE, OBJECT
	}
}
