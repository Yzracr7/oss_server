package com.hyperion.game.event;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.hyperion.game.event.AbstractEvent.EventType;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.utility.Utils;
import com.hyperion.utility.game.ClickOption;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Apr 3, 2015
 */
public class EventHandler {

	/**
	 * This method initializes all game events into the {@link #EVENTS} list.
	 */
	public static void initialize() {
		for (String directory : Utils.getSubDirectories(EventHandler.class)) {
			if (directory.equals("type")) {
				continue;
			}
			for (Object clazz : Utils.getClassesInDirectory(EventHandler.class.getPackage().getName() + "." + directory)) {
				getEvents().add((AbstractEvent) clazz);
			}
		}
	}

	/**
	 * If there is a npc event to be handled, this is returned true
	 * 
	 * @param player
	 *            The player
	 * @param npc
	 *            The npc
	 * @param option
	 *            The option
	 * @return
	 */
	public static boolean handleNPCInteraction(Player player, NPC npc, ClickOption option) {
		List<AbstractEvent> events = getEvents(npc.getId(), EventType.NPC);
		for (AbstractEvent event : events) {
			if (event.handleNPCInteraction(player, npc, option)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * If there is an object event to be handled, this is returned true and it
	 * is handled
	 * 
	 * @param player
	 *            The player
	 * @param object
	 *            The object
	 * @param option
	 *            The option
	 * @return
	 */
	public static boolean handleObjectInteraction(Player player, GameObject object, ClickOption option) {
		List<AbstractEvent> events = getEvents(object.getId(), EventType.OBJECT);
		for (AbstractEvent event : events) {
			if (event.handleObjectInteraction(player, object, option)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * If the is an inter event to be handled, this is returned true and it
	 * is handled
	 * 
	 * @param player
	 *            The player
	 * @param interfaceId
	 *            The inter id
	 * @param buttonId
	 *            The button id
	 * @param slotId
	 *            The slot id
	 * @param packetId
	 *            The packet id
	 * @return
	 */
	public static boolean handleInterfaceInteraction(Player player, int interfaceId, int buttonId, int slotId, int packetId) {
		List<AbstractEvent> events = getEvents(interfaceId, EventType.OBJECT);
		for (AbstractEvent event : events) {
			if (event.handleInterfaceInteraction(player, interfaceId, buttonId, slotId, packetId)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets all of the events of a certain type
	 * 
	 * @param type
	 *            The type of events we want
	 * @return
	 */
	private static final List<AbstractEvent> getEvents(int key, EventType type) {
		List<AbstractEvent> events = EVENTS.stream().filter(p -> p.getEventType().equals(type)).filter(p -> {
			for (int keyId : p.getKeyBinds()) {
				if (key == keyId) {
					return true;
				}
			}
			return false;
		}).collect(Collectors.toList());
		return events;
	}

	/**
	 * @return the events
	 */
	public static List<AbstractEvent> getEvents() {
		return EVENTS;
	}

	/**
	 * The list of events
	 */
	private static final List<AbstractEvent> EVENTS = new ArrayList<>();
}
