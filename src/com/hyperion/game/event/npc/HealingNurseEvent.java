package com.hyperion.game.event.npc;

import com.hyperion.game.event.type.NPCInteractionEvent;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.game.ClickOption;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Apr 3, 2015
 */
public class HealingNurseEvent extends NPCInteractionEvent {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.event.NPCInteractionEvent#getKeyBinds()
	 */
	@Override
	public int[] getKeyBinds() {
		return new int[] { 961 };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.event.NPCInteractionEvent#handleNPCInteraction(com.
	 * hyperion.game.world.entity.player.Player,
	 * com.hyperion.game.world.entity.npc.NPC,
	 * com.hyperion.utility.game.ClickOption)
	 */
	@Override
	public boolean handleNPCInteraction(Player player, NPC npc, ClickOption option) {
		npc.animate(12575);
		player.playGraphic(1314);
		
		//player.getSettings().resetSpecial();
		player.heal(990);
		player.getSkills().setSkillsToNormal();
		if (player.getCombatState().isPoisoned()) {
			player.getCombatState().setPoisonAmount(0);
			player.getPacketSender().sendConfig(428, 0);
		}
		return true;
	}

}
