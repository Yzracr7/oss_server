package com.hyperion.game.event.npc;

import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.event.type.NPCInteractionEvent;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.game.ClickOption;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Apr 3, 2015
 */
public class MeleeTutorEvent extends NPCInteractionEvent{

	/* (non-Javadoc)
	 * @see com.hyperion.game.event.type.NPCInteractionEvent#getKeyBinds()
	 */
	@Override
	public int[] getKeyBinds() {
		return new int[] { 705 };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.event.type.NPCInteractionEvent#handleNPCInteraction(com.hyperion.game.world.entity.player.Player, com.hyperion.game.world.entity.npc.NPC, com.hyperion.utility.game.ClickOption)
	 */
	@Override
	public boolean handleNPCInteraction(Player player, NPC npc, ClickOption option) {
		DialogueManager.handleDialogues(player, npc, 1);
		return true;
	}

}
