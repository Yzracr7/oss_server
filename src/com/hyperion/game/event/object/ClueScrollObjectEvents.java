package com.hyperion.game.event.object;

import com.hyperion.game.event.type.ObjectInteractionEvent;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.utility.game.ClickOption;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Apr 3, 2015
 */
public class ClueScrollObjectEvents extends ObjectInteractionEvent {

	/* (non-Javadoc)
	 * @see com.hyperion.game.event.type.ObjectInteractionEvent#getKeyBinds()
	 */
	@Override
	public int[] getKeyBinds() {
		return new int[] { 2618 };
	}

	/* (non-Javadoc)
	 * @see com.hyperion.game.event.type.ObjectInteractionEvent#handleObjectInteraction(com.hyperion.game.world.entity.player.Player, com.hyperion.game.world.gameobjects.GameObject, com.hyperion.utility.game.ClickOption)
	 */
	@Override
	public boolean handleObjectInteraction(Player player, GameObject object, ClickOption option) {
		switch(object.getId()) {
		case 2618:
			player.teleport(Location.create(player.getLocation().getX(), player.getLocation().getY() >= 3493 ? 3492 : 3493));
			break;
		}
		return true;
	}

}
