package com.hyperion.game.event.object;

import com.hyperion.game.Constants;
import com.hyperion.game.content.godwars.Godwars;
import com.hyperion.game.content.godwars.Godwars.ChamberTypes;
import com.hyperion.game.content.skills.magic.MagicBookHandler;
import com.hyperion.game.content.skills.magic.MagicBookHandler.TeleportType;
import com.hyperion.game.content.skills.prayer.PrayerAltars;
import com.hyperion.game.event.type.ObjectInteractionEvent;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.utility.game.ClickOption;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Apr 4, 2015
 */
public class GodwarsObjectEvents extends ObjectInteractionEvent {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.event.type.ObjectInteractionEvent#getKeyBinds()
	 */
	@Override
	public int[] getKeyBinds() {
		return new int[] { 26384, 26425, 26444, 26445, 26427, 26287, 26289, 26303, 26426 };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.event.type.ObjectInteractionEvent#handleObjectInteraction
	 * (com.hyperion.game.world.entity.player.Player,
	 * com.hyperion.game.world.gameobjects.GameObject,
	 * com.hyperion.utility.game.ClickOption)
	 */
	@Override
	public boolean handleObjectInteraction(Player player, GameObject object, ClickOption option) {
		switch (object.getId()) {
		case 26425: // Godwars bandos door
			if (player.getVariables().getGwdKillcount()[ChamberTypes.BANDOS.ordinal()] >= ChamberTypes.KILLCOUNT_REQUIREMENT * (player.getDetails().isDonator() ? 0.5 : 1)) {
				player.teleport(player.getLocation().getX() >= 2864 ? 2863 : 2864, player.getLocation().getY(), player.getLocation().getZ());

				player.getVariables().getGwdKillcount()[ChamberTypes.BANDOS.ordinal()] -= ChamberTypes.KILLCOUNT_REQUIREMENT * (player.getDetails().isDonator() ? 0.5 : 1);
				player.getPacketSender().sendMessage("You feel your killcount drain from your body...");
				player.getVariables().setUsedGodwarsAltar(false);
				Godwars.updateKills(player);
			} else {
				player.getPacketSender().sendMessage("You need " + (ChamberTypes.KILLCOUNT_REQUIREMENT / (player.getDetails().isDonator() ? 2 : 1)) + " Bandos kills to enterInstancedRoom this room.");
			}
			break;
		case 26384: // Godwars bandos room door
			player.teleport(player.getLocation().getX() >= 2851 ? 2850 : 2851, player.getLocation().getY(), player.getLocation().getZ());
			break;
		case 26444: // saradomin godwars rock
			player.teleport(2914, 5300, 1);
			break;
		case 26445: // saradomin godwars 2nd rock
			player.teleport(2920, 5274, 0);
			break;
		case 26427: // saradomin godwars door
			if (player.getVariables().getGwdKillcount()[ChamberTypes.SARADOMIN.ordinal()] >= ChamberTypes.KILLCOUNT_REQUIREMENT * (player.getDetails().isDonator() ? 0.5 : 1)) {
				player.teleport(player.getLocation().getX() >= 2908 ? 2907 : 2908, 5265, 0);

				player.getVariables().getGwdKillcount()[ChamberTypes.SARADOMIN.ordinal()] -= ChamberTypes.KILLCOUNT_REQUIREMENT * (player.getDetails().isDonator() ? 0.5 : 1);
				player.getPacketSender().sendMessage("You feel your killcount drain from your body...");
				player.getVariables().setUsedGodwarsAltar(false);
				Godwars.updateKills(player);
			} else {
				player.getPacketSender().sendMessage("You need " + (ChamberTypes.KILLCOUNT_REQUIREMENT / (player.getDetails().isDonator() ? 2 : 1)) + " Saradomin kills to enterInstancedRoom this room.");
			}
			break;
		case 26289: // bandos altar
		case 26287: // saradomin altar
			if (option == ClickOption.FIRST) {
				if (!player.getVariables().isUsedGodwarsAltar()) {
					if (PrayerAltars.restorePrayer(player)) {
						player.getVariables().setUsedGodwarsAltar(true);
					}
				} else {
					player.getPacketSender().sendMessage("You have already used the altar on this trip.");
				}
			} else if (option == ClickOption.SECOND) {
				MagicBookHandler.teleportPlayer(player, Constants.HOME_LOCATION, TeleportType.MODERN, null);
			}
			break;
		case 26303: // pillar armadyl
			player.teleport(2872, player.getLocation().getY() >= 5279 ? 5269 : 5279, 2);
			break;
		case 26426: // armadyl chamber door
			if (player.getVariables().getGwdKillcount()[ChamberTypes.ARMADYL.ordinal()] >= ChamberTypes.KILLCOUNT_REQUIREMENT * (player.getDetails().isDonator() ? 0.5 : 1)) {
				player.teleport(2839, player.getLocation().getY() <= 5295 ? 5296 : 5295, 2);
					
				player.getVariables().getGwdKillcount()[ChamberTypes.ARMADYL.ordinal()] -= ChamberTypes.KILLCOUNT_REQUIREMENT * (player.getDetails().isDonator() ? 0.5 : 1);
				player.getPacketSender().sendMessage("You feel your killcount drain from your body...");
				player.getVariables().setUsedGodwarsAltar(false);
				Godwars.updateKills(player);
			} else {
				player.getPacketSender().sendMessage("You need " + (ChamberTypes.KILLCOUNT_REQUIREMENT / (player.getDetails().isDonator() ? 2 : 1)) + " Armadyl kills to enterInstancedRoom this room.");
			}
			break;
		}
		return true;
	}
}
