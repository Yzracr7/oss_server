package com.hyperion.game.event.type;

import com.hyperion.game.event.AbstractEvent;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.utility.game.ClickOption;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Apr 3, 2015
 */
public abstract class ObjectInteractionEvent extends AbstractEvent {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.event.AbstractEvent#getKeyBinds()
	 */
	@Override
	public abstract int[] getKeyBinds();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.event.AbstractEvent#handleObjectInteraction(com.hyperion
	 * .game.world.entity.player.Player,
	 * com.hyperion.game.world.gameobjects.GameObject,
	 * com.hyperion.utility.game.ClickOption)
	 */
	@Override
	public abstract boolean handleObjectInteraction(Player player, GameObject object, ClickOption option);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.event.AbstractEvent#getEventType()
	 */
	@Override
	public EventType getEventType() {
		return EventType.OBJECT;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.event.AbstractEvent#handleNPCInteraction(com.hyperion
	 * .game.world.entity.player.Player, com.hyperion.game.world.entity.npc.NPC,
	 * com.hyperion.utility.game.ClickOption)
	 */
	@Override
	public boolean handleNPCInteraction(Player player, NPC npc, ClickOption option) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.hyperion.game.event.AbstractEvent#handleInterfaceInteraction(com.
	 * hyperion.game.world.entity.player.Player, int, int, int, int)
	 */
	@Override
	public boolean handleInterfaceInteraction(Player player, int interfaceId, int buttonId, int slotId, int packetId) {
		return false;
	}

}
