package com.hyperion.game.item;

import java.io.*;

public class BonusPacker {

    public static final void dumpIntoTxt() {
        for (int itemId = 0; itemId < 21863; itemId++) {
            double[] bonuses = ItemDefinition.forId(itemId).getBonus();
            if (bonuses == null) {
                System.out.println("nulled bonuses: " + itemId);
                continue;
            }
            File file = new File("data/world/items/bonuses/" + itemId + ".txt");
            if (file.exists())
                continue;
            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(file));
                writer.write("Attack bonus");
                writer.newLine();
                writer.flush();
                for (int index = 0; index < 5; index++) {
                    writer.write("" + bonuses[index]);
                    writer.newLine();
                    writer.flush();
                }
                writer.write("Defence bonus");
                writer.newLine();
                writer.flush();
                for (int index = 5; index < 11; index++) {
                    if (index == 10)//summoning
                        continue;
                    writer.write("" + bonuses[index]);
                    writer.newLine();
                    writer.flush();
                }
                writer.write("Other bonuses");
                writer.newLine();
                writer.flush();
                for (int index = 14; index < 18; index++) {
                    if (index == 15) {//ranged str
                        continue;
                    } else if (index == 17) {//dunno
                        continue;
                    }
                    writer.write("" + bonuses[index]);
                    writer.newLine();
                    writer.flush();
                    System.out.println("Exists ? : " + file.exists());
                }
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static final void main(String[] args) throws IOException {
        DataOutputStream out = new DataOutputStream(new FileOutputStream(
                "data/world/items/bonuses.ib"));
        for (int itemId = 0; itemId < 21863; itemId++) {
            File file = new File("data/world/items/bonuses/" + itemId + ".txt");
            if (file.exists()) {
                BufferedReader reader = new BufferedReader(new FileReader(file));
                out.writeShort(itemId);
                reader.readLine();
                // att bonuses
                out.writeShort(Integer.valueOf(reader.readLine()));
                out.writeShort(Integer.valueOf(reader.readLine()));
                out.writeShort(Integer.valueOf(reader.readLine()));
                out.writeShort(Integer.valueOf(reader.readLine()));
                out.writeShort(Integer.valueOf(reader.readLine()));
                reader.readLine();
                // def bonuses
                out.writeShort(Integer.valueOf(reader.readLine()));
                out.writeShort(Integer.valueOf(reader.readLine()));
                out.writeShort(Integer.valueOf(reader.readLine()));
                out.writeShort(Integer.valueOf(reader.readLine()));
                out.writeShort(Integer.valueOf(reader.readLine()));
                reader.readLine();
                // Other bonuses
                out.writeShort(Integer.valueOf(reader.readLine()));
                out.writeShort(Integer.valueOf(reader.readLine()));
                if (reader.readLine() != null)
                    throw new RuntimeException("Should be null line" + itemId);
                reader.close();
            }
        }
        out.flush();
        out.close();
        System.out.println("Bonuses packed.");
    }
}
