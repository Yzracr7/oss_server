package com.hyperion.game.item;


/**
 * Represents a single item.
 * <p>
 * Immutable.
 *
 * @author Graham
 */
public class Item {

    private int id;
    private int count;
    private ItemDefinition itemDefinition;

    public Item(int id, int amount) {
        this.id = id;
        this.count = amount;
        this.itemDefinition = ItemDefinition.forId(id);
    }

    public Item() {
        // TODO WEWOO
    }

    /**
     * @param
     */
    public Item(int id) {
        this.id = id;
        this.count = 1;
        this.itemDefinition = ItemDefinition.forId(id);
    }

    public ItemDefinition getDefinition() {
        if (itemDefinition == null) {
            return ItemDefinition.forId(this.id);
        }
        return itemDefinition;
    }

    public Object readResolve() {
        this.itemDefinition = ItemDefinition.forId(id);
        return this;
    }

    public int getId() {
        return this.id;
    }

    public void setItemId(int itemId) {
        this.id = itemId;
        this.itemDefinition = ItemDefinition.forId(id);
    }

    public int getCount() {
        return count;
    }

    public void setItemAmount(int amount) {
        this.count = amount;
    }

    @Override
    public String toString() {
        return "[id: " + id + ", amount: " + count + "]";
    }

    public int fetch_render() {
        //return Constants.USE_DEV_CACHE ? id : Loaders.renderIds[id];
        //return Loaders.renderIds[id];
        //if (id == 13263 || id == 14636 || id == 14637)
        // return 15492; //Fix for slayer helm on 464
        return id;
    }
}
