package com.hyperion.game.item;

public class ItemData {

    public static int getColoredItem(String original_item_name, String color) {
        switch (original_item_name) {
            case "abyssal whip":
                if (color.equalsIgnoreCase("yellow")) {
                    return 15441;
                } else if (color.equalsIgnoreCase("blue")) {
                    return 15442;
                } else if (color.equalsIgnoreCase("silver")) {
                    return 15443;
                } else if (color.equalsIgnoreCase("green")) {
                    return 15444;
                }
                return 4151;
            case "dark bow":
                if (color.equalsIgnoreCase("yellow")) {
                    return 15701;
                } else if (color.equalsIgnoreCase("blue")) {
                    return 15702;
                } else if (color.equalsIgnoreCase("silver")) {
                    return 15703;
                } else if (color.equalsIgnoreCase("green")) {
                    return 15704;
                }
                return 11235;
        }
        return -1;
    }

    public static int getNotedItem(int item) {
        ItemDefinition itemDef = ItemDefinition.forId(item);
        if (itemDef == null) {
            return -1;
        }
        return itemDef.isNoteable() ? item + 1 : -1;
    }

    public static int getUnNotedItem(int item) {
        ItemDefinition itemDef = ItemDefinition.forId(item - 1);
        if (itemDef != null) {
            return itemDef.getId();
        }
        return -1;
    }

    public static boolean isPlayerBound(int item) {
        for (int i = 0; i < PLAYER_BOUND_ITEMS.length; i++) {
            if (item == PLAYER_BOUND_ITEMS[i]) {
                return true;
            }
        }
        return false;
    }

    /*
     * Unused
     */
    public static boolean isNote(int item) {
        ItemDefinition itemDef = ItemDefinition.forId(item);
        return itemDef.isNoted();
    }

    public static boolean isFullBody(ItemDefinition def) {
        if (def == null || def.getName() == null) {
            return false;
        }
        String weapon = def.getName().toLowerCase();
        int wearId = def.getId();
        if (wearId == 7592)
            return false;
        for (int i = 0; i < FULL_BODY_IDS.length; i++) {
            if (wearId == FULL_BODY_IDS[i]) {
                return true;
            }
        }
        for (int i = 0; i < FULL_BODY.length; i++) {
            if (weapon.contains(FULL_BODY[i])) {
                return true;
            }
        }
        if (wearId >= 4892 && wearId <= 4895) {
            return true;
        }
        return false;
    }

    public static boolean isFullHat(ItemDefinition def) {
        if (def == null || def.getName() == null) {
            return false;
        }
        String name = def.getName().toLowerCase();
        int wearId = def.getId();
        for (int i = 0; i < FULL_HAT_IDS.length; i++) {
            if (wearId == FULL_HAT_IDS[i]) {
                return true;
            }
        }
        for (int i = 0; i < FULL_HAT.length; i++) {
            if (name.endsWith(FULL_HAT[i]) || name.startsWith(FULL_HAT[i])) {
                return true;
            }
        }
        return false;
    }

    public static boolean isFullMask(ItemDefinition def) {
        if (def == null || def.getName() == null) {
            return false;
        }
        String name = def.getName().toLowerCase();
        int wearId = def.getId();
        for (int i = 0; i < FULL_MASK.length; i++) {
            if (name.equalsIgnoreCase("Highwayman mask"))
                return false;
            if (name.endsWith(FULL_MASK[i]) || name.equalsIgnoreCase(FULL_MASK[i]) || name.startsWith(FULL_MASK[i])) {
                return true;
            }
        }
        for (int i = 0; i < FULL_MASK_IDS.length; i++) {
            if (wearId == FULL_MASK_IDS[i]) {
                return true;
            }
        }
        return false;
    }

    /* Fullbody is an item that covers your arms. */
    private static final int[] FULL_BODY_IDS = {7392, 20794, 20840, 20421, 544, 13552, 20184, 13557, 2623, 20415, 20169, 13547, 2653, 4967, 12460, 13366, 13542, 13562, 13361, 12470, 4966, 2661, 13371, 12480, 13376, 12814, 6617, 13381, 4965, 4964, 4968, 10348, 12225, 12277, 2669, 3481, 12287, 12215, 12205, 10691, 10798, 10800, 10782, 10776, 10780, 10698, 10697, 10690, 10618, 4720, 4728, 4749, 4892, 4893, 4894, 4895, 4896, 4916, 4917, 4918, 4919, 4920, 577, 2607, 2615, 1035, 544, 546, 636, 638, 640, 642, 1115, 1117, 1119, 1121, 1123, 2599, 1125, 1127, 2583, 2591, 426, 644, 540, 2896, 2906, 2916, 2926, 2936, 4091, 4101, 4111, 7390, 20131, 20199, 10370, 20425, 20576, 21021, 10378, 10460, 10462, 10784, 10786, 12265, 10788, 12253, 12193, 10386, 10458, 6129, 6186, 6184, 6322, 10330, 10338, 10348, 18698};
    /* Fullhat covers your head but not your beard. */
    private static final int[] FULL_HAT_IDS = {21264, 10828, 13364, 10342, 4164, 10548, 12677, 10392, 6326, 11663, 11664, 11665, 12681, 9629, 4502, 7112, 3749, 3751, 3753, 3755, 4716, 10604, 10350, 10342, 4513, 4515};
    /* Fullmask covers your entire head. */
    private static final int[] FULL_MASK_IDS = {};
    /* Fullbody is an item that covers your arms. */
    private static final String[] FULL_BODY = {"ele' blouse", "morrigan's leather body", "hauberk", "ghostly robe", "monk's robe", "granite", "vesta", "runecrafter robe", "top", "shirt", "platebody", "ahrims robetop", "karils leathertop", "brassard", "robe top", "robetop", "platebody (t)", "platebody (g)", "chestplate", "torso", "dragon chainbody", "pernix body", "naval shirt", "moonclan armour", "ham shirt"};
    /* Fullhat covers your head but not your beard. */
    private static final String[] FULL_HAT = {"helm", "dharok's helm", "mitre", "bandana", "cowl", "berserker", "med helm", "coif", "dharoks helm", "hood", "initiate helm", "coif", "helm of neitiznot"};
    /* Fullmask covers your entire head. */
    private static final String[] FULL_MASK = {"slayer helmet", "spiny helmet", "sallet", "full helm(t)", "full helm(g)", "full helm", "mask", "verac's helm", "guthan's helm", "torag's helm", "karil's coif", "full helm (t)", "full helm (g)", "mask", "pernix cowl", "slayer helm", "full slayer helm", "armadyl helmet", "serpentine", "ironman helm"};

    /**
     * An array of non-tradable items. First block of items don't get destroyed
     * when dropped, the second block do.
     */
    private static final int[] PLAYER_BOUND_ITEMS = {
            2996, // Agility ticket.
            10548, // Fighter hat
            10551, // Fighter torso.
            6570, //Fire cape
            20195, // Inferno cape
            12954, //Dragon defender
            8850, // Rune defender.
            8849, // Adamant defender
            8848, // Mith def
            8847, // Black def
            8846, // Steel def
            8845, // Iron def
            8844, // Bronze def
            8840, // Void skirt.
            8839, // Void top.
            11665, // Melee void helm.
            11663, // Range void helm.
            11664, // Mage void helm.
            8842, // Void gloves.
            8851, // Warrior guild token.
            2412, // Sara cape
            2413, // Guthix cape
            2414, // Zam cape
            3842, // unholy book
            3844, // balance
            3840, // holy book
            10499, // Ava
            10498, // Ava
            7462, // Barrow gloves
            7461, // Dragon gloves
            7460, // Rune gloves
            7459, // Addy gloves
            7458, // Mith gloves
            7457, // Black gloves
            7456, // Steel gloves
            7455, // Iron gloves
            7454, // Bronze gloves
            4502, // Bearhead
            7534, // Fishbowl helmet
            7535, // Diving apparatus
            7053, // lit bug latern
            9747, // Skillcapes speak here
            9748, 9753, 9754, 9750, 9751, 9768, 9769, 9756, 9757, 9759, 9760, 9762, 9763, 9801, 9802, 9807, 9808, 9783, 9784, 9798, 9799, 9804, 9805, 9780, 9781, 9795, 9796, 9792, 9793, 9774, 9775, 9771, 9772, 9777, 9778, 9786, 9787, 9810, 9811, 9765, 9766, 9948, 9949, 9789, 9790, 12169, 12170, 9813, // Skillcapes
            // end
            // here
            9749, // Skill hoods speak here
            9752, 9755, 9758, 9761, 9764, 9767, 9770, 9773, 9776, 9779, 9782, 9785, 9788, 9791, 9794, 9797, 9800, 9803, 9806, 9809, 9812, 9814, 9950,// Skill
            // hoods
            // end
            // here
    };
}
