package com.hyperion.game.item;

import com.hyperion.game.world.entity.combat.WeaponInterfaces.WeaponInterface;
import com.hyperion.game.world.entity.player.container.impl.Equipment;

/**
 * The container that represents an item definition.
 *
 * @author lare96 <http://github.com/lare96>
 */
public final class ItemDefinition {

    /**
     * The array that contains all of the item definitions.
     */
    public static final ItemDefinition[] DEFINITIONS = new ItemDefinition[25000];

    /**
     * The description of the item.
     */
    private final String description;

    /**
     * The equipment slot of this item.
     */
    private final int equipmentSlot;

    /**
     * The flag that determines if the item is noteable.
     */
    private final boolean noteable;

    /**
     * The special store price of this item.
     */
    private final int specialPrice;

    /**
     * The general store price of this item.
     */
    private final int generalPrice;

    /**
     * The low alch value of this item.
     */
    private final int lowAlchValue;

    /**
     * The high alch value of this item.
     */
    private final int highAlchValue;

    /**
     * The array of bonuses for this item.
     */
    private final double[] bonus;

    /**
     * The flag that determines if this item is two-handed.
     */
    private final boolean doubleHanded;

    /**
     * The flag that determines if this item is a full helmet.
     */
    private final boolean fullHelm;

    /**
     * The flag that determines if this item is a platebody.
     */
    private final boolean platebody;

    /**
     * The flag that determines if this item is tradeable.
     */
    private int id;
    private String name = "";
    private String examine = "";
    private WeaponInterface weaponInterface;
    private boolean stackable;
    private boolean tradeable;
    private boolean dropable;
    private boolean sellable;
    private boolean noted;
    private int value;
    private int highAlch;
    private int lowAlch;
    private int dropValue;
    private int noteId = -1;
    private int blockAnim = 424;
    private int standAnim = 808;
    private int walkAnim = 819;
    private int runAnim = 824;
    private int standTurnAnim = 823;
    private int turn180Anim = 820;
    private int turn90CWAnim = 821;
    private int turn90CCWAnim = 821;
    private double weight;
    private double[] bonuses;
    private int[] requirements;
    private int attackAnim;

    public int getAttackAnim() {
        return attackAnim;
    }

    public void setAttackAnim(int attackAnim) {
        this.attackAnim = attackAnim;
    }

    /**
     * Creates a new {@link ItemDefinition}.
     *
     * @param id            the identifier for the item.
     * @param name          the proper name of the item.
     * @param description   the description of the item.
     * @param equipmentSlot the equipment slot of this item.
     * @param noteable      the flag that determines if the item is noteable.
     * @param stackable     the flag that determines if the item is stackable.
     * @param specialPrice  the special store price of this item.
     * @param generalPrice  the general store price of this item.
     * @param lowAlchValue  the low alch value of this item.
     * @param highAlchValue the high alch value of this item.
     * @param weight        the weight value of this item.
     * @param bonus         the array of bonuses for this item.
     * @param twoHanded     the flag that determines if this item is two-handed.
     * @param fullHelm      the flag that determines if this item is a full helmet.
     * @param platebody     the flag that determines if this item is a platebody.
     * @param tradeable     the flag that determines if this item is tradeable.
     */
    public ItemDefinition(int id, String name, String description, int equipmentSlot, boolean noteable,int note_id, boolean stackable,
                          int specialPrice, int generalPrice, int lowAlchValue, int highAlchValue, double weight, double[] bonus, boolean twoHanded,
                          boolean fullHelm, boolean platebody, boolean tradeable, int standAnim, int runAnim, int walkAnim, int blockAnim, int standTurnAnim, int turn180Anim, int turn90CWAnim, int turn90CCWAnim, int attackAnim) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.equipmentSlot = equipmentSlot;
        this.noteable = noteable;
        this.stackable = stackable;
        this.specialPrice = specialPrice;
        this.generalPrice = generalPrice;
        this.lowAlchValue = lowAlchValue;
        this.highAlchValue = highAlchValue;
        this.weight = weight;
        this.noteId = note_id;
        this.bonus = bonus;
        this.doubleHanded = twoHanded;
        this.fullHelm = fullHelm;
        this.platebody = platebody;
        this.tradeable = tradeable;
        this.standAnim = standAnim;
        this.runAnim = runAnim;
        this.walkAnim = walkAnim;
        this.blockAnim = blockAnim;
        this.standTurnAnim = standTurnAnim;
        this.turn180Anim = turn180Anim;
        this.turn90CWAnim = turn90CWAnim;
        this.turn90CCWAnim = turn90CCWAnim;
        this.attackAnim = attackAnim;
        boolean full_body = ItemData.isFullBody(this);
        if (full_body) {
            this.setEquipmentType(EquipmentType.PLATEBODY);
        }
        boolean full_mask = ItemData.isFullMask(this);
        if (full_mask) {
            this.setEquipmentType(EquipmentType.FULL_MASK);
        }
        boolean full_helm = ItemData.isFullHat(this);
        if (full_helm) {
            this.setEquipmentType(EquipmentType.FULL_HELM);
        }
        setWeaponDefinition(new WeaponDefinition());
        //System.out.println("Loaded Item : " + id + " " + name + " " + description + " " + equipmentSlot);
        // prayerBonus();
    }

    /**
     * Gets definition by name
     *
     * @param name the name
     * @return the definition
     */
    public static ItemDefinition forName(final String name) {
        for (final ItemDefinition def : DEFINITIONS) {
            if (def != null) {
                if (def.getName().equalsIgnoreCase(name)) {
                    return def;
                }
            }
        }
        return null;
    }

    /**
     * Gets definition by name
     *
     * @param name the name
     * @return the definition
     */
    public static ItemDefinition guess(final String name) {
        for (final ItemDefinition def : DEFINITIONS) {
            if (def != null) {
                if (def.getName().contains(name)) {
                    return def;
                }
            }
        }
        return null;
    }

    public boolean matchesEquipmentType(EquipmentType equipmentType) {
        if (this.equipmentType == equipmentType) {
            return true;
        }
        return false;
    }

    /**
     * Returns a definition for a specific id
     *
     * @param id
     * @return
     */
    public static ItemDefinition forId(int id) {
        if (id == -1) {
            return DEFINITIONS[22328];
        }
        if (id > DEFINITIONS.length) {
            return null;
        }
        if (DEFINITIONS[id] == null) {
            return DEFINITIONS[22328];
        }
        return DEFINITIONS[id];
    }

    /**
     * Equipment type definitions
     * <p>
     * These are in order and used by ordinal, dont change the structure
     */
    public enum EquipmentType {
        HELMET("Helmet", Equipment.EQUIPMENT.HELMET.ordinal()),
        CAPE("Cape", Equipment.EQUIPMENT.CAPE.ordinal()),
        AMULET("Amulet", Equipment.EQUIPMENT.AMULET.ordinal()),
        WEAPON("Weapon", Equipment.EQUIPMENT.WEAPON.ordinal()),
        PLATEBODY("Platebody", Equipment.EQUIPMENT.PLATE.ordinal()),
        SHIELD("Shield", Equipment.EQUIPMENT.SHIELD.ordinal()),
        UNKNOWN_ONE("Unknown", Equipment.EQUIPMENT.SHIELD.ordinal() + 1),
        LEGS("Legs", Equipment.EQUIPMENT.LEGS.ordinal()),
        FULL_MASK("Full mask", Equipment.EQUIPMENT.HELMET.ordinal()),
        GLOVES("gloves", Equipment.EQUIPMENT.GLOVES.ordinal()),
        BOOTS("boots", Equipment.EQUIPMENT.BOOTS.ordinal()),
        UNKNOWN_3("Full helm", Equipment.EQUIPMENT.BOOTS.ordinal()),
        RING("Ring", Equipment.EQUIPMENT.RING.ordinal()),
        ARROWS("Arrow", Equipment.EQUIPMENT.ARROW.ordinal()),
        FULL_HELM("Full helm", Equipment.EQUIPMENT.HELMET.ordinal());
        /**
         * The description.
         */
        private String description;

        /**
         * The slot.
         */
        private int slot;

        /**
         * Creates the equipment type.
         *
         * @param description The description.
         * @param slot        The slot.
         */
        private EquipmentType(String description, int slot) {
            this.description = description;
            this.slot = slot;
        }

        /**
         * Gets the description.
         *
         * @return The description.
         */
        public String getDescription() {
            return description;
        }

        /**
         * Gets the slot.
         *
         * @return The slot.
         */
        public int getSlot() {
            return slot;
        }
    }

    public enum CombatTypes {
        MELEE, RANGED
    }

    /**
     * The method that erases the prayer bonus from ranged weapons.
     */
    private void prayerBonus() {
        //  if (equipmentSlot == Equipment.ARROWS_SLOT || name.contains("knife") || name.contains("dart") || name.contains("thrownaxe") || name
        //        .contains("javelin")) {
        bonus[11] = 0;
        //  }
    }

    /**
     * Gets the identifier for the item.
     *
     * @return the identifier.
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the proper name of the item.
     *
     * @return the proper name.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the description of the item.
     *
     * @return the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the equipment slot of this item.
     *
     * @return the equipment slot.
     */
    public int getEquipmentSlot() {
        return equipmentSlot;
    }

    /**
     * Determines if the item is noted or not.
     *
     * @return {@code true} if the item is noted, {@code false} otherwise.
     */
    public boolean isNoted() {
        return description.equals("Swap this note at any bank for the " + "equivalent item.");
    }

    /**
     * Determines if the item is noteable or not.
     *
     * @return {@code true} if the item is noteable, {@code false} otherwise.
     */
    public boolean isNoteable() {
        return noteable;
    }

    /**
     * Determines if the item is stackable or not.
     *
     * @return {@code true} if the item is stackable, {@code false} otherwise.
     */
    public boolean isStackable() {
        return stackable;
    }

    /**
     * Gets the special store price of this item.
     *
     * @return the special price.
     */
    public int getSpecialPrice() {
        return specialPrice;
    }

    /**
     * Gets the general store price of this item.
     *
     * @return the general price.
     */
    public int getGeneralPrice() {
        return generalPrice;
    }

    /**
     * Gets the low alch value of this item.
     *
     * @return the low alch value.
     */
    public int getLowAlchValue() {
        return lowAlchValue;
    }

    /**
     * Gets the high alch value of this item.
     *
     * @return the high alch value.
     */
    public int getHighAlchValue() {
        return highAlchValue;
    }

    /**
     * Gets the weight value of this item.
     *
     * @return the weight value.
     */
    public double getWeight() {
        return weight;
    }

    /**
     * Gets the array of bonuses for this item.
     *
     * @return the array of bonuses.
     */
    public double[] getBonus() {
        return bonus;
    }

    /**
     * Determines if this item is two-handed or not.
     *
     * @return {@code true} if this item is two-handed, {@code false} otherwise.
     */
    public boolean isDoubleHanded() {
        return doubleHanded;
    }

    /**
     * Determines if this item is a full helmet or not.
     *
     * @return {@code true} if this item is a full helmet, {@code false}
     * otherwise.
     */
    public boolean isFullHelm() {
        return fullHelm;
    }

    /**
     * Determines if this item is a platebody or not.
     *
     * @return {@code true} if this item is a platebody, {@code false}
     * otherwise.
     */
    public boolean isPlatebody() {
        return platebody;
    }

    /**
     * Determines if this item is tradeable.
     *
     * @return {@code true} if this item is tradeable, {@code false} otherwise.
     */
    public boolean isTradeable() {
        return tradeable;
    }

    public int getBlockAnim() {
        return blockAnim;
    }

    public void setBlockAnim(int blockAnim) {
        this.blockAnim = blockAnim;
    }

    public int getStandAnim() {
        return standAnim;
    }

    public void setStandAnim(int standAnim) {
        this.standAnim = standAnim;
    }

    public int getWalkAnim() {
        return walkAnim;
    }

    public void setWalkAnim(int walkAnim) {
        this.walkAnim = walkAnim;
    }

    public int getRunAnim() {
        return runAnim;
    }

    public void setRunAnim(int runAnim) {
        this.runAnim = runAnim;
    }

    public int getStandTurnAnim() {
        return standTurnAnim;
    }

    public void setStandTurnAnim(int standTurnAnim) {
        this.standTurnAnim = standTurnAnim;
    }

    public int getTurn180Anim() {
        return turn180Anim;
    }

    public void setTurn180Anim(int turn180Anim) {
        this.turn180Anim = turn180Anim;
    }

    public int getTurn90CWAnim() {
        return turn90CWAnim;
    }

    public void setTurn90CWAnim(int turn90CWAnim) {
        this.turn90CWAnim = turn90CWAnim;
    }

    public int getTurn90CCWAnim() {
        return turn90CCWAnim;
    }

    public void setTurn90CCWAnim(int turn90CCWAnim) {
        this.turn90CCWAnim = turn90CCWAnim;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double[] getBonuses() {
        return bonuses;
    }

    public void setBonuses(double[] bonuses) {
        this.bonuses = bonuses;
    }

    public int[] getRequirements() {
        return requirements;
    }

    public void setRequirements(int[] requirements) {
        this.requirements = requirements;
    }

    public void setEquipmentDefinition(EquipmentDefinition equipmentDefinition) {
        this.equipmentDefinition = equipmentDefinition;
    }

    public void setWeaponDefinition(WeaponDefinition weaponDefinition) {
        this.weaponDefinition = weaponDefinition;
    }

    public DegradableType getDegradableType() {
        return degradableType;
    }

    public void setDegradableType(DegradableType degradableType) {
        this.degradableType = degradableType;
    }

    public WeaponInterface getWeaponInterface() {
        return weaponInterface;
    }

    /**
     * WEAPON AND EQUIPMENT HANDLING
     */
    private EquipmentDefinition equipmentDefinition;
    private WeaponDefinition weaponDefinition;
    private EquipmentType equipmentType = null;
    private DegradableType degradableType = null;

    public class WeaponDefinition {

        private boolean isTwoHanded;
        private CombatTypes combatType;
        private AttackStyles attackStyles;
        private final RangedWeaponDefinition rangedWeaponDefinition = null;
        private RangedCombatData rangedCombatData = null;
        private int blockAnimation;
        private int standAnimation;
        private int walkAnimation;
        private int runAnimation;
        private int interfaceId;
        private int child;
        private boolean hasSpecialAttack;
        private final SpecialAttackData specialAttackData = null;
        private int soundId;

        public boolean isTwoHanded() {
            return isTwoHanded;
        }

        public CombatTypes getCombatType() {
            return combatType;
        }

        public AttackStyles getAttackStyles() {
            return attackStyles;
        }

        public RangedWeaponDefinition getRangedWeaponDefinition() {
            return rangedWeaponDefinition;
        }

        public int getBlockAnimation() {
            return blockAnimation;
        }

        public int getStandAnimation() {
            return standAnimation;
        }

        public int getWalkAnimation() {
            return walkAnimation;
        }

        public int getRunAnimation() {
            return runAnimation;
        }

        public int getInterfaceId() {
            return interfaceId;
        }

        public int getChildId() {
            return child;
        }

        public boolean hasSpecialAttack() {
            return hasSpecialAttack;
        }

        public SpecialAttackData getSpecialAttackData() {
            return specialAttackData;
        }

        public int getSoundId() {
            return soundId;
        }

        public RangedCombatData getRangedCombatData() {
            return rangedCombatData;
        }

        public void setRangedCombatData(boolean set) {
            this.rangedCombatData = set ? new RangedCombatData() : null;
        }

        public class SpecialAttackData {

            private int animationId;
            private int animationDelay;
            private int graphicId;
            private int graphicDelay;
            private boolean isHighGraphic;
            private final int[] specialBarData = null;
            private double specialAmount;
            private int specialBarButtonId;

            public int getAnimationId() {
                return animationId;
            }

            public int getAnimationDelay() {
                return animationDelay;
            }

            public int getGraphicId() {
                return graphicId;
            }

            public int getGraphicDelay() {
                return graphicDelay;
            }

            public boolean isHighGraphic() {
                return isHighGraphic;
            }

            public int[] getSpecialBarData() {
                return specialBarData;
            }

            public double getSpecialAmount() {
                return specialAmount;
            }

            public int getSpecialBarButtonId() {
                return specialBarButtonId;
            }
        }

        public class AttackStyles {

            private int accurateAnimation;
            private int aggressiveAnimation;
            private int controlledAnimation;
            private int defensiveAnimation;
            private int accurateSpeed;
            private int aggressiveSpeed;
            private int controlledSpeed;
            private int defensiveSpeed;

            public int getAccurateAnimation() {
                return accurateAnimation;
            }

            public int getAggressiveAnimation() {
                return aggressiveAnimation;
            }

            public int getControlledAnimation() {
                return controlledAnimation;
            }

            public int getDefensiveAnimation() {
                return defensiveAnimation;
            }

            public int getAccurateSpeed() {
                return accurateSpeed;
            }

            public int getAggressiveSpeed() {
                return aggressiveSpeed;
            }

            public int getControlledSpeed() {
                return controlledSpeed;
            }

            public int getDefensiveSpeed() {
                return defensiveSpeed;
            }
        }

        public class RangedWeaponDefinition {

            private int[] arrowsAllowed;

            public int[] getArrowsAllowed() {
                return arrowsAllowed;
            }
        }

        public class RangedCombatData {

            private int drawback = -1, projectile, rangedStr;
            private boolean usesAmmoSlot = true;

            public RangedCombatData() {

            }

            public int getDrawback() {
                return drawback;
            }

            public void setDrawback(int drawback) {
                this.drawback = drawback;
            }

            public int getProjectile() {
                return projectile;
            }

            public void setProjectile(int projectile) {
                this.projectile = projectile;
            }

            public boolean usesAmmoSlot() {
                return usesAmmoSlot;
            }

            public void setUsesAmmoSlot(boolean usesAmmoSlot) {
                this.usesAmmoSlot = usesAmmoSlot;
            }

            public int getRangedStr() {
                return rangedStr;
            }

            public void setRangedStr(int rangedStr) {
                this.rangedStr = rangedStr;
            }
        }
    }

    public enum DegradableType {
        recoil(), dharoks_helmet(), dharoks_plate(), dharoks_legs(), dharoks_axe(), torags_helm(), torags_plate(), torags_legs(), torags_hammers(), ahrims_hood(), ahrims_top(), ahrims_bottom(), ahrims_staff(), karils_coif(), karils_top(), karils_bottom(), karils_bow(), veracs_helm(), veracs_plate(), veracs_skirt(), veracs_flail(), guthans_helm(), guthans_plate(), guthans_legs(), guthans_spear(), chaotic_maul(), chaotic_crossbow(), chaotic_longsword(), chaotic_rapier(), crystal_bow(), vesta_spear(), vesta_longsword(), vesta_plateskirt(), vesta_chainbody(), statius_platebody(), statius_platelegs(), statius_helm(), statius_hammer();

        private DegradableType() {
        }

        public static DegradableType getType(String set) {
            for (DegradableType type : DegradableType.values()) {
                String name = type.name().replace("_", " ");
                if (set.equalsIgnoreCase(name)) {
                    return type;
                }
            }
            return null;
        }
    }

    public class EquipmentDefinition {

        private int equipmentSlot = -1;
        private int requirements[];
        private ShieldDefinition shieldDefinition;

        public int getEquipmentSlot() {
            return equipmentSlot;
        }

        public void setEquipmentSlot(int s) {
            equipmentSlot = s;
        }

        public int[] getRequirements() {
            return requirements;
        }

        public ShieldDefinition getShieldDefinition() {
            return shieldDefinition;
        }

        public class ShieldDefinition {

            private int animationId;

            public int getAnimationId() {
                return animationId;
            }
        }
    }

    public WeaponDefinition getWeaponDefinition() {
        return weaponDefinition;
    }

    public EquipmentType getEquipmentType() {
        return equipmentType;
    }

    public EquipmentDefinition getEquipmentDefinition() {
        return equipmentDefinition;
    }

    public void setEquipmentType(EquipmentType equipmentType) {
        this.equipmentType = equipmentType;
    }

    public int getNoteId() {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

    /**
     * Item Bound To Player
     */
    private boolean isPlayerBound;

    public boolean isPlayerBound() {
        return isPlayerBound;
    }

    public void setPlayerBound(boolean isPlayerBound) {
        this.isPlayerBound = isPlayerBound;
    }

}