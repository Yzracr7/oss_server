package com.hyperion.game.item;

public class RequirementHandler {
 
	private static Requirement[][] itemRequirements = new Requirement[25000][];

	public static void addRequirement(final int itemId, final int id,
			final int level) {
		if (itemRequirements[itemId] == null) {
			itemRequirements[itemId] = new Requirement[] { new Requirement(id, level) };
		} else {
			final Requirement[] current = itemRequirements[itemId];
			itemRequirements[itemId] = new Requirement[itemRequirements[itemId].length + 1];
			for (int i = 0; i < current.length; i++) {
				itemRequirements[itemId][i] = current[i];
			}
			itemRequirements[itemId][itemRequirements[itemId].length - 1] = new Requirement(id, level);
		}
	}

	public static Requirement[] getItemRequirements(final int itemId) {
		return itemRequirements[itemId];
	}

	public static class Requirement {

		private final int id;
		private final int level;

		private Requirement(final int id, final int level) {
			this.id = id;
			this.level = level;
		}

		public int getId() {
			return id;
		}

		public int getLevel() {
			return level;
		}

	}
}