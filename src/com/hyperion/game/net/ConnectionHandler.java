package com.hyperion.game.net;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;

import com.hyperion.game.net.protocol.RS2CodecFactory;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.sql.login.LoginManager;

/**
 * The <code>ConnectionHandler</code> processes incoming events from MINA,
 * submitting appropriate tasks to the <code>GameEngine</code>.
 * @author Graham Edgecombe
 *
 */
public class ConnectionHandler extends IoHandlerAdapter {
	
	/**
	 * The <code>GameEngine</code> instance.
	 */
	//private final GameEngine engine = World.getWorld().getEngine();

	@Override
	public void exceptionCaught(IoSession session, Throwable throwable) throws Exception {
		session.close(false);
	}

	@Override
	public void messageReceived(IoSession session, Object message) throws Exception {
		if(session.getAttribute("player") != null) {
			Player p = (Player) session.getAttribute("player");
			Packet packet = (Packet) message;
			PacketManager.getPacketManager().handle(p, packet);
		}
	}

	@Override
	public void sessionClosed(IoSession session) throws Exception {
		
		//TODO: Add a timer for the ping packet.
		//If the ping packet(202) doesn't arrive for 10 game ticks then you can xlog?
		if(session.getAttribute("player") != null) {
			final Player p = (Player) session.getAttribute("player");
			if (p != null) {
				boolean canLog = !(System.currentTimeMillis() - p.getCombatState().getLogoutTimer() < 10000);
				if (!canLog && !p.getAttributes().isSet("forcelogout")) {
					System.out.println(p.getName() + " X-logged in combat, saving player in 10 ticks.");
					World.getWorld().submit(new Tickable(17) { //16.66 ticks = 10 seconds
						@Override
						public void execute() {
							LoginManager.unregister_player(p);
							this.stop();
						}
					});
				} else {
					System.out.println(p.getName() + " X-logged, saving player immediately.");
					LoginManager.unregister_player(p);
				}
			} else {
				System.out.println("[FATAL]Player already null, could not save x-logged player.");
			}
		}
	}

	@Override
	public void sessionIdle(IoSession session, IdleStatus status) throws Exception {
		if(session.getAttribute("player") != null) {
			Player p = (Player) session.getAttribute("player");
			if (p != null) {
				System.out.println("Wanted to unregister player " + p.getName() + " for idle session. Idle status: " + status.toString()
						 + ", both idle count: " + session.getBothIdleCount() + ", status count: " + session.getIdleCount(status));
			//	LoginManager.unregister_player(p);
			}
		}
		//session.close(false);
	}

	@Override
	public void sessionOpened(IoSession session) throws Exception {
		session.setAttribute("remote", session.getRemoteAddress());
		session.getFilterChain().addFirst("protocol", new ProtocolCodecFilter(RS2CodecFactory.LOGIN));
	}

}
