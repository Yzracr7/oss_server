package com.hyperion.game.net;

import com.hyperion.Logger;
import com.hyperion.utility.json.JsonManager;
import com.hyperion.utility.json.impl.PunishmentLoader;
import com.hyperion.utility.json.sub.Punishment;
import org.apache.mina.core.session.IoSession;

import com.hyperion.game.Constants;
import com.hyperion.game.packet.PacketHandler;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.logger.Log;

import java.util.concurrent.TimeUnit;

/**
 * Managers <code>PacketHandler</code>s.
 *
 * @author Graham Edgecombe
 */
public class PacketManager {

    /**
     * The instance.
     */
    private static final PacketManager INSTANCE = new PacketManager();

    /**
     * Gets the packet manager instance.
     *
     * @return The packet manager instance.
     */
    public static PacketManager getPacketManager() {
        return INSTANCE;
    }

    /**
     * The packet handler array.
     */
    private PacketHandler[] packetHandlers = new PacketHandler[256];

    /**
     * Creates the packet manager.
     */
    public PacketManager() {
        /*
         * Set default handlers.
		 */
        /*
         * final PacketHandler defaultHandler = new DefaultPacketHandler();
		 * for(int i = 0; i < packetHandlers.length; i++) { if(packetHandlers[i]
		 * == null) { packetHandlers[i] = defaultHandler; } }
		 */
    }

    /**
     * Binds an opcode to a handler.
     *
     * @param id      The opcode.
     * @param handler The handler.
     */
    public void bind(int id, PacketHandler handler) {
        packetHandlers[id] = handler;
    }

    /**
     * Handles a packet.
     *
     * @param player The player.
     * @param packet The packet.
     */
    public void handle(Player player, Packet packet) {
        if (player == null || packet == null)
            return;
        World.getWorld().addPing(player.getName());

        if (player.getVariables().getPacketsExecuted() >= Constants.MAX_PACKETS_PER_CYCLE) {
            if (Constants.DEBUG_MODE) {
                player.getPacketSender().sendMessage("exceeding max packets per cycle");
            }
            return;
        } else if (Constants.DEBUG_MODE && packet.getOpcode() != 8 && packet.getOpcode() != 202) {
            System.out.println("packet: " + packet.getOpcode());
        }
        if (packet.getOpcode() == 99) {

            int current_click_time = (int) (System.currentTimeMillis() - player.lastClickTime2);
            int lat1 = current_click_time;
            int lat2 = player.last_click_time;
            int dif = lat1 - lat2;

            // MACRO DETECTION
            if (current_click_time == player.last_click_time || dif < 13 && dif > -13) {
                player.macro_detection_flags += 1;
                if (player.macro_detection_flags > 75) {
                    Logger.getInstance().warning("[Macro Detection][" + dif + "][" + player.macro_detection_flags + "][" + player.getDetails().getName() + "] was detected for botting;");
                }
                if (player.macro_detection_flags > 100) {
                    player.getPacketSender().forceLogout();
                    JsonManager.getSingleton().getLoader(PunishmentLoader.class).addPunishment(player.getName(), Punishment.PunishmentType.BAN, TimeUnit.DAYS.toMillis(3));
                    World.getWorld().sendGlobalMessage("[<col=911f1f>Bot</col>] - Macro detector has just banned " + player.getName() + " for " + 3 + " days", false);
                }
            }

            // RAPID CLICKING DETECTION
            if (player.clickWarnings < 3) {
                player.lastClickTime1 = System.currentTimeMillis();
            }
            player.lastClickTime2 = System.currentTimeMillis();
            player.last_click_time = current_click_time;
            if (current_click_time < 132 && current_click_time > 0) {
                player.clickWarnings += 1;
                Logger.getInstance().warning("[Packet Abuse][" + current_click_time + "][" + player.getDetails().getName() + "] was detected for packet abuse;");
                if (player.getCombatState().getPreviousAttacker() != null && player.getCombatState().getLastAttacked() > (System.currentTimeMillis() - 4000) && (player.clickWarnings >= 7 && (int) (System.currentTimeMillis() - player.lastClickTime1) < 200)) {
                    player.getPacketSender().forceLogout();
                    World.getWorld().sendGlobalMessage("[<col=911f1f>Staff Alert</col>] - Anti spam click detector has kicked player: " + player.getName() + " from server.", true);
                } else {
                    player.lastClickTime1 = System.currentTimeMillis();
                }
            }
        }

        IoSession session = player.getSession();
        if (packetHandlers[packet.getOpcode()] == null) {
            World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Sent invalid packet=" + packet.getOpcode() + "."));
            return;
        }
        try {
            packetHandlers[packet.getOpcode()].handle((Player) session.getAttribute("player"), packet);
            player.getVariables().increasePacketsExecuted();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
