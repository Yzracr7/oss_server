package com.hyperion.game.net;

import org.apache.mina.core.future.IoFuture;
import org.apache.mina.core.future.IoFutureListener;

import com.hyperion.cache.definitions.CachedNpcDefinition;
import com.hyperion.game.Constants;
import com.hyperion.game.content.BeginnerTutorial;
import com.hyperion.game.content.QuestTab;
import com.hyperion.game.content.Skillcape;
import com.hyperion.game.content.clanchat.ClanChat;
import com.hyperion.game.content.grandexchange.offer.GrandExchangeOffer;
import com.hyperion.game.content.interfaces.Gameframe;
import com.hyperion.game.content.minigames.Settings;
import com.hyperion.game.content.pets.BossPetDropHandler;
import com.hyperion.game.item.Item;
import com.hyperion.game.net.Packet.Type;
import com.hyperion.game.tickable.impl.GrandExchangeMap;
import com.hyperion.game.tickable.impl.PoisonTask;
import com.hyperion.game.world.Loaders;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.TeleportAreaLocations;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.npc.NPCDefinition;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.entity.player.container.impl.Bank;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.game.world.grounditems.GroundItem;
import com.hyperion.game.world.grounditems.GroundItemManager;
import com.hyperion.game.world.region.DynamicRegion;
import com.hyperion.game.world.region.Region;
import com.hyperion.utility.TextUtils;
import com.hyperion.utility.game.InputAction;
import com.hyperion.utility.logger.Log;

/**
 * A utility class for sending packets.
 *
 * @author Graham Edgecombe
 */
public class PacketSender {

    /**
     * The player.
     */
    private Player player;

    /**
     * Creates an action sender for the specified player.
     *
     * @param player The player to create the action sender for.
     */
    public PacketSender(Player player) {
        this.player = player;
    }

    public PacketSender sendConstructedRegion() {
        player.setLastKnownRegion(player.getLocation());
        PacketBuilder bldr = new PacketBuilder(222, Type.VARIABLE_SHORT);
        int middleChunkX = player.getLocation().getChunkX();
        int middleChunkY = player.getLocation().getChunkY();
        int localX = player.getLocation().getLocalX();
        int localY = player.getLocation().getLocalY();
        int z = player.getLocation().getZ();
        bldr.putLEShortA((short) middleChunkY);
        bldr.putByteS((byte) z);
        bldr.putLEShortA((short) middleChunkX);
        bldr.putLEShortA((short) localY);
        bldr.putShortA((short) localX);
        int sceneLength = 104 >> 4;
        int[] regionIds = new int[4 * sceneLength * sceneLength];
        int newRegionIdsCount = 0;
        bldr.startBitAccess();
        for (int plane = 0; plane < 4; plane++) {
            for (int realChunkX = (middleChunkX - sceneLength); realChunkX <= ((middleChunkX + sceneLength)); realChunkX++) {
                int regionX = realChunkX / 8;
                y:
                for (int realChunkY = (middleChunkY - sceneLength); realChunkY <= ((middleChunkY + sceneLength)); realChunkY++) {
                    int regionY = realChunkY / 8;
                    // rcx / 8 = rx, rcy / 8 = ry, regionid is encoded region x
                    // and y
                    int regionId = (regionX << 8) + regionY;
                    Region region = World.getWorld().getRegionManager().getRegions().get(regionId);
                    int newChunkX;
                    int newChunkY;
                    int newPlane;
                    int rotation;
                    if (region instanceof DynamicRegion) { // generated map
                        DynamicRegion dynamicRegion = (DynamicRegion) region;
                        int[] pallete = dynamicRegion.getRegionCoords()[plane][realChunkX - (regionX * 8)][realChunkY - (regionY * 8)];
                        newChunkX = pallete[0];
                        newChunkY = pallete[1];
                        newPlane = pallete[2];
                        rotation = pallete[3];
                    } else { // real map
                        newChunkX = realChunkX;
                        newChunkY = realChunkY;
                        newPlane = plane;
                        rotation = 0;// no rotation
                    }
                    // invalid chunk, not built chunk
                    if (newChunkX == 0 || newChunkY == 0)
                        bldr.putBits(1, 0);
                    else {
                        bldr.putBits(1, 1);
                        // chunk encoding = (x << 14) | (y << 3) | (plane <<
                        // 24), theres addition of two more bits for rotation
                        bldr.putBits(26, (rotation << 1) | (newPlane << 24) | (newChunkX << 14) | (newChunkY << 3));
                        int newRegionId = (((newChunkX / 8) << 8) + (newChunkY / 8));
                        for (int index = 0; index < newRegionIdsCount; index++)
                            if (regionIds[index] == newRegionId)
                                continue y;
                        regionIds[newRegionIdsCount++] = newRegionId;
                    }
                }
            }
        }
        bldr.finishBitAccess();
        for (int index = 0; index < newRegionIdsCount; index++) {
            if (Constants.USE_DEV_CACHE) {
                bldr.putInt(0);
                bldr.putInt(0);
                bldr.putInt(0);
                bldr.putInt(0);
            } else {
                int[] data = World.getWorld().mapdata.getData(regionIds[index]);
                bldr.putInt(data[0]);
                bldr.putInt(data[1]);
                bldr.putInt(data[2]);
                bldr.putInt(data[3]);
            }
        }
        player.getSession().write(bldr.toPacket());
        return this;
    }

    private void sendWelcomeScreen() {
        // This could also include donator status WITH img=xx drawn.
        sendWindowPane(549);
        sendInterface(549, 2, 378, true);
        sendInterface(549, 3, /* 405 */Constants.WELCOME_BOX_ID, true);
        displayInterface(549);
        Gameframe.setRedStone(player, 4);
        modifyText("Welcome to " + Constants.SERVER_NAME, 378, 12);
        int messages = player.getDetails().getUnreadMessages();
        String unread_text = messages > 0 ? "<col=FFFFFF>" + messages + "</col>" : "0";
        modifyText("<br><br><br>" + "You have <col=00FF00>" + unread_text + " unread messages</col> in your message centre.", 378, 15);
        modifyText((player.getDetails().getCurrentEmail() == null ? "You have not yet set a recovery email.<br> It is <col=FFAC30>strongly</col> recommended that you do so.<br>If you don't you will be <col=FFAC30>unable to recover your</col><br><col=FFAC30>password</col> if you forget it, or it is stolen." : "Recovery Email:<br><col=00FF00>" + player.getDetails().getCurrentEmail()), 378, 14);
        if (player.getVariables().getBankPin() == null) {
            modifyText("You do not have a Bank PIN. Please visit a bank if you would like one.", 378, 17);
        } else {
            modifyText("You have a Bank PIN.", 378, 17);
        }
        modifyText("You have <col=00FF00> 0 credits</col>.<br>Visit <col=00FF00>" + Constants.webStoreURL + "</col> to buy credits.", 378, 19);
        boolean bowlean = player.getSession() != null && player.getSession().getRemoteAddress() != null;
        String address = (bowlean ? player.getSession().getRemoteAddress().toString().substring(1).split(":")[0] : "none");
        modifyText("<col=000000>You last logged in <col=FF0000>earlier today<col=000000>.", 378, 13);
        modifyText("Welcome to " + Constants.SERVER_NAME + "!<br>View the latest updates at www.runespark.com", Constants.WELCOME_BOX_ID, 1);
        String mac = player.getDetails().getMacAddress() != null ? player.getDetails().getMacAddress() : "none";
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Logged in from " + address + " and mac " + mac + "."));
    }

    public PacketSender sendChildColor(int inter, int child, int red, int green, int blue) {
        // player.getSession().write(new PacketBuilder(244).putInt(inter << 16 |
        // child).putShortA((short) red << 198761418 | green << -1750503739 |
        // blue).toPacket());
        return this;
    }

    public PacketSender sendComponentPosition(int interfaceId, int childId, int x, int y) {
        player.getSession().write(new PacketBuilder(201).putLEInt(interfaceId << 16 | childId).putShortA(y).putShort(x).toPacket());
        return this;
    }

    /*
     * public PacketSender sendHintArrowFollowing(Entity entity) { PacketBuilder
     * bldr = new PacketBuilder(217); bldr.put((byte) 2); bldr.put((byte) 1);
     * bldr.putShort(entity.getLocation().getX());
     * bldr.putShort(entity.getLocation().getY()); bldr.put((byte) (100 * 4 >>
     * 2));// Distance to show on map bldr.putShort(65535);
     * player.getSession().write(bldr.toPacket()); return this; }
     */

    /*
     * public PacketSender sendHintArrow(Entity entity) { PacketBuilder bldr =
     * new PacketBuilder(150); if (entity != null) { bldr.put((byte)
     * (entity.isNPC() ? 1 : 10)); bldr.putShort(entity.getIndex());
     * bldr.put((byte) 0); bldr.put((byte) 0); bldr.put((byte) 0); } else {
     * bldr.put((byte) (10)); bldr.putShort(-1); bldr.put((byte) 0);
     * bldr.put((byte) 0); bldr.put((byte) 0); }
     * player.getSession().write(bldr.toPacket()); return this; }
     *
     * public PacketSender sendRemoveHint(int index) { PacketBuilder bldr = new
     * PacketBuilder(217); bldr.put((byte) (index << 5)); //
     * ////player.getSession().write(bldr.toPacket());//TODO - fix return this;
     * }
     */

    public PacketSender sendHintArrow(Entity entity) {
        PacketBuilder bldr = new PacketBuilder(156);
        boolean remove = false;
        boolean flash = true;
        if (entity == null) {
            remove = true;
        }
        /*
         * if(entity.isNPC()) { return this; } else {
         */
        bldr.put((byte) (remove ? 69 : entity.isNPC() ? 1 : 10));
        bldr.put((byte) (remove ? 0 : entity.getClientIndex())); // client-sided
        // index?
        bldr.put((byte) (flash ? 1 : 0));
        player.getSession().write(bldr.toPacket());
        // }
        return this;
    }

    public void setMinimapStatus(int setting) {
        // //player.getSession().write(new PacketBuilder(192).put((byte)
        // setting).toPacket());
    }

    public void sendClueScroll(Item[] items) {
        this.displayInterface(364);
        this.sendAccessMask(1278, 364, 4, 0, 12);
        // this.sendAccessMask(1278, 23855108, 0, 12);
        this.sendClientScript(150, new Object[]{" ", " ", " ", " ", " ", " ", " ", " ", " ", -1, 0, 4, 3, 32858, 23855108}, "IviiiIsssssssss");
        this.sendItems(364, 1, 0, items);
    }

    public void sendSystemUpdate(int time) {
        player.getSession().write(new PacketBuilder(30).putShort(time).toPacket());
    }

    public void softCloseInterfaces() {
        sendCloseInterface();
        restoreTabs();
        closeChatboxInterface();
        player.setInteractingEntity(null);
    }

    public void sendObjectAnimation(int anim, GameObject object) {
        Location loc = object.getLocation();
        sendArea(Location.create(loc.getX(), loc.getY(), player.getLocation().getZ()));
        PacketBuilder spb = new PacketBuilder(140);
        //140? 55 on 474
        int ot = ((object.getType() << 2) + (object.getRotation() & 0x3));
        spb.putLEShortA(anim);
        spb.put((byte) 0);
        spb.put((byte) ot);
        //	player.getSession().write(spb.toPacket());
    }
	
	
	/*
	 *  474
	 * 	public ActionSender animateObject(GameObject obj, int animationId) {
		if(player.getLocation().getZ() != obj.getLocation().getZ()) {
			return this;
		}
		sendArea(obj.getLocation(), 0, 0);
		PacketBuilder spb = new PacketBuilder(55);
		int ot = ((obj.getType() << 2) + (obj.getDirection() & 3));
		spb.putByteA((byte) ot);
		spb.put((byte) 0);
		spb.putShort(animationId);
		player.write(spb.toPacket());
		return this;
	}
	
	 */


    public void setFriendsListStatus() {
        PacketBuilder spb = new PacketBuilder(152);
        spb.put((byte) 2);
        player.getSession().write(spb.toPacket());
    }

    public void setPrivacyOptions() {
        /** DO NOT USE **/
        // player.getSession().write(new PacketBuilder(156).put((byte)
        // player.getFriends().getPrivacyOption(0)).put((byte)
        // player.getFriends().getPrivacyOption(1)).put((byte)
        // player.getFriends().getPrivacyOption(2)).toPacket());
    }

    public void sendSentPrivateMessage(String name, byte[] message) {
        player.getSession().write(new PacketBuilder(23, Type.VARIABLE).putRS2String(name).put(message).toPacket());
    }

    public void sendReceivedPrivateMessage(String name, int rights, byte[] packed) {
        int messageCounter = player.getFriends().getNextUniqueId();
        player.getSession().write(new PacketBuilder(50, Type.VARIABLE).putRS2String(name).putShort(1).put(new byte[]{(byte) ((messageCounter << 16) & 0xFF), (byte) ((messageCounter << 8) & 0xFF), (byte) (messageCounter & 0xFF)}).put((byte) rights).put(packed).toPacket());
    }

    public void sendFriend(String name, int world) {
        PacketBuilder bldr = new PacketBuilder(100, Type.VARIABLE);
        bldr.putRS2String(name);
        bldr.putShort(world);
        bldr.put((byte) 0);
        player.getSession().write(bldr.toPacket());
    }

    public void sendClanTest() {
        PacketBuilder bldr = new PacketBuilder(55, Type.VARIABLE_SHORT);
        bldr.putLong(TextUtils.stringToLong("test1"));
        bldr.putLong(TextUtils.stringToLong("test2"));
        bldr.put((byte) 1);
        bldr.put((byte) 1);
        for (int i = 0; i < 1; i++) {
            bldr.putLong(TextUtils.stringToLong("test3"));
            bldr.putShort(1);
            bldr.put((byte) 3);
            bldr.putRS2String("test");
        }
        player.getSession().write(bldr.toPacket());
    }

    public void sendIgnores(String[] names) {
        PacketBuilder spb = new PacketBuilder(75, Type.VARIABLE);
        for (String name : names) {
            spb.putRS2String(name);
        }
        player.getSession().write(spb.toPacket());
    }

    public void clearGroundItem(GroundItem item) {
        if (item != null) {
            sendArea(item.getLocation());
            PacketBuilder bldr = new PacketBuilder(39);
            bldr.putShortA(item.getItem().fetch_render());
            bldr.putByteS((byte) 0);
            player.getSession().write(bldr.toPacket());
        }
    }

    public void sendGroundItem(GroundItem item) {
        if (item != null) {
            sendArea(item.getLocation());
            PacketBuilder bldr = new PacketBuilder(112);
            bldr.putShort(item.getItem().fetch_render());
            bldr.putLEShort(item.getItem().getCount());
            bldr.putByteS((byte) 0);
            player.getSession().write(bldr.toPacket());
        }
    }

    public void createObject(int objectId, Location loc, int face, int type) {
        sendArea(Location.create(loc.getX(), loc.getY(), player.getLocation().getZ()));
        PacketBuilder spb = new PacketBuilder(17);
        int ot = ((type << 2) + (face & 3));
        spb.putByteA((byte) 0);
        // spb.putLEShort(Constants.USE_DEV_CACHE ? objectId :
        // CachedObjectDefinition.renderIds[objectId]);
        spb.putLEShort(objectId);
        spb.putByteA((byte) ot);
        player.getSession().write(spb.toPacket());
    }

    public void removeObject(int type, int face, Location loc) {
        sendArea(Location.create(loc.getX(), loc.getY(), player.getLocation().getZ()));
        PacketBuilder spb = new PacketBuilder(16);
        int ot = ((type << 2) + (face & 3));
        spb.putByteA((byte) ot);
        spb.putByteA((byte) 0);
        player.getSession().write(spb.toPacket());
    }

    public void createObject(GameObject object) {
        sendArea(object.getLocation());
        PacketBuilder spb = new PacketBuilder(17);
        spb.putByteA((byte) 0);
        // spb.putLEShort(Constants.USE_DEV_CACHE ? object.getId() :
        // CachedObjectDefinition.renderIds[object.getId()]);
        spb.putLEShort(object.getId());
        spb.putByteA((byte) ((object.getType() << 2) + (object.getRotation() & 3)));
        player.getSession().write(spb.toPacket());
    }

    public void removeObject(GameObject object) {
        sendArea(object.getLocation());
        PacketBuilder spb = new PacketBuilder(16);
        int ot = ((object.getType() << 2) + (object.getRotation() & 3));
        spb.putByteA((byte) ot);
        spb.putByteA((byte) 0);
        player.getSession().write(spb.toPacket());
    }

    public void sendStillGraphics(Location loc, Graphic graphics, int tilesAway) {
        sendArea(loc);
        player.getSession().write(new PacketBuilder(186).put((byte) tilesAway).putShort(graphics.getId()).put((byte) graphics.getHeight()).putShort(graphics.getDelay()).toPacket());
    }
    
    public void sendStillGraphicsWithDelay(Location loc, Graphic graphics, int tilesAway, int delay) {
        sendArea(loc);
        player.getSession().write(new PacketBuilder(186).put((byte) tilesAway).putShort(graphics.getId()).put((byte) graphics.getHeight()).putShort(delay).toPacket());
    }

    private void sendArea(Location location) {
        PacketBuilder bldr = new PacketBuilder(132);
        int regionX = player.getLastKnownRegion().getChunkX();
        int regionY = player.getLastKnownRegion().getChunkY();
        bldr.put((byte) ((location.getY() - ((regionY - 6) * 8))));
        bldr.put((byte) ((location.getX() - ((regionX - 6) * 8))));
        player.getSession().write(bldr.toPacket());
    }

    public void closeChatboxInterface() {
        player.getSession().write(new PacketBuilder(137).putInt(548 << 16 | 79).toPacket());
    }


    public void itemOnInterface(int inter, int child, int size, int item) {
        if (size == 0)
            size = 1;
        player.getSession().write(new PacketBuilder(114).putLEInt(size).putShort(item).putLEInt(inter << 16 | child).toPacket());
    }

    public void sendChatboxInterface(int childId) {
        sendInterface(548, 79, childId, false);
    }

    public void sendItemDialogue(Player player, int itemId, int zoom, String... text) {
        if (text.length > 4 || text.length < 1) {
            return;
        }
        int interfaceId = 63 + text.length;
        if (interfaceId <= 63) {
            interfaceId = 64;
        }
        for (int i = 0; i < text.length; i++) {
            modifyText(text[i], interfaceId, 2 + i);
        }
        itemOnInterface(interfaceId, 0, zoom, itemId); // zoom = 200
        modifyText(/* player.getDetails().getName() */"", interfaceId, 1);
        sendChatboxInterface(interfaceId);
    }

    public void sendChatboxDialogue(boolean continueButton, String... text) {
        if (text.length > 4 || text.length < 1)
            return;
        int interfaceId = continueButton ? (209 + text.length) : (214 + text.length);
        interfaceId = (continueButton && interfaceId <= 209) ? 210 : (!continueButton && interfaceId <= 214) ? 215 : interfaceId;
        for (int i = 0; i < text.length; i++)
            modifyText(text[i], interfaceId, 0 + i);
        sendChatboxInterface(interfaceId);
    }

    public void sendEnergy() {
        player.getSession().write(new PacketBuilder(163).put((byte) player.getVariables().getRunningEnergy()).toPacket());
    }

    public void sendOverlay(int i) {
        player.getInterfaceSettings().setOverlayOpened(i);
        sendInterface(548, 64, i, true);
    }

    public void sendRemoveOverlay() {
        player.getInterfaceSettings().setOverlayOpened(-1);
        player.getSession().write(new PacketBuilder(137).putInt(548 << 16 | 64).toPacket());// closes
        // main
        // screen
    }



    /*
    OSRSfinal class AnimateComponentEncoder extends EventEncoder[AnimateComponent] {
  override def encode(buffer: ByteBuffer, replication: AnimateComponent) = {
    buffer.put(61.toByte)
    buffer.putShort(replication.motionId.toShort)
    buffer.putInt(replication.interfaceId << 16 | replication.component)

  }final class PlaceNpcHeadComponentEncoder extends EventEncoder[PlaceNpcHeadComponent] {
  override def encode(buffer: ByteBuffer, replication: PlaceNpcHeadComponent) = {
    buffer.put(9.toByte)
    buffer.putShort(replication.npcId.toShort)
    buffer.putInt(replication.interfaceId << 16 | replication.component)
  }
}
}
     */

    public void sendPlayerHead(int interfaceID, int childID) {
        player.getSession().write(new PacketBuilder(8).putLEInt(interfaceID << 16 | childID).toPacket());
    }

    public void sendNPCHead(int npcID, int interfaceId, int childId) {
        System.out.println("Sending Interface : " + interfaceId + " " + childId);
        player.getSession().write(new PacketBuilder(207).putLEShortA(Constants.USE_DEV_CACHE ? npcID : NPCDefinition.renderIds[npcID]).putInt(interfaceId << 16 | childId).toPacket());

        //player.getSession().write(new PacketBuilder(207).putLEShortA(npcID).putInt((interfaceId << 16 | childId)));
        //player.getSession().write(new PacketBuilder(207).putLEShortA(Constants.USE_DEV_CACHE ? npcID : NPCDefinition.forId(npcID).putInt(interfaceId << 16 | childId).toPacket());
    }

    public void animateInterface(int animID, int interfaceId, int childId) {
        player.getSession().write(new PacketBuilder(63).putInt2(interfaceId << 16 | childId).putLEShort(Constants.USE_DEV_CACHE ? animID : Loaders.animIds[animID]).toPacket());
    }

    public void transformToNpc(Player player, int npc) {
        player.getAppearance().setNpcId(npc);
        if (npc != -1) {
            player.getVariables().setWalkAnimation(CachedNpcDefinition.getNPCDefinitions(npc).walkAnimation);
            player.getVariables().setStandAnimation(CachedNpcDefinition.getNPCDefinitions(npc).idleAnimation);
            player.getVariables().setTurn180Animation(CachedNpcDefinition.getNPCDefinitions(npc).turn180Animation);
            player.getVariables().setTurn90Clockwise(CachedNpcDefinition.getNPCDefinitions(npc).turn90CWAnimation);
            player.getVariables().setTurn90CounterClockwise(CachedNpcDefinition.getNPCDefinitions(npc).turn90CCAnimation);

        } else {
            player.getVariables().setWalkAnimation(-1);
            player.getVariables().setStandAnimation(-1);
            player.getVariables().setTurn180Animation(-1);
            player.getVariables().setTurn90Clockwise(-1);
            player.getVariables().setTurn90CounterClockwise(-1);
        }
        player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
    }

    public void sendPlayerOption(String option, int slot, boolean top) {
        PacketBuilder bldr = new PacketBuilder(72, Type.VARIABLE);
        bldr.putRS2String(option);
        bldr.putByteS((byte) slot);
        bldr.putByteC(top ? (byte) 1 : (byte) 0);
        player.getSession().write(bldr.toPacket());
    }

    public void tradeWarning(int slot) {
        Object[] opt = new Object[]{slot, 7, 4, 335 << 16 | 51};
        sendClientScript(143, opt, "iiii");
    }

    public void tradeWarning2(int slot) {
        Object[] opt = new Object[]{slot, 7, 4, 335 << 16 | 49};
        sendClientScript(143, opt, "iiii");
    }

    public void restoreTabs() {
        player.getSession().write(new PacketBuilder(137).putInt(548 << 16 | 84).toPacket());
    }

    public void sendCloseInterface() {
        player.getInterfaceSettings().setInterfaceOpened(-1);
        player.getSession().write(new PacketBuilder(137).putInt(548 << 16 | 62).toPacket());
    }

    public void requestIntegerInput(InputAction<?> action) {
        sendClientScript(108, new Object[]{action.getText()}, "s");
        player.getAttributes().set("input_action", action);
    }

    public void requestStringInput(InputAction<?> action) {
        sendClientScript(110, new Object[]{action.getText()}, "s");
        player.getAttributes().set("input_action", action);
    }

    public void displayEnterAmount(String amount) {
        Object[] o = {amount};
        sendClientScript(108, o, "s");
    }

    public void displayEnterText(String text) {
        Object[] o = {text};
        sendClientScript(110, o, "s");
    }

    public void clearMapFlag() {
        player.getSession().write(new PacketBuilder(68).toPacket());
    }

    public void sendAccessMask(int set, int interfaceId, int window, int start, int end) {
        PacketBuilder bldr = new PacketBuilder(254);
        bldr.putLEShort(end);
        bldr.putInt(interfaceId << 16 | window);
        bldr.putShortA(start);
        bldr.putInt1(set);
        player.getSession().write(bldr.toPacket());
    }

    public void displayInventoryInterface(int childId) {
        sendInterface(548, 84, childId, false);
    }

    public void sendSkillLevels() {
        for (int i = 0; i < Skills.SKILL_COUNT; i++) {
            sendSkillLevel(i);
        }
    }

    public void sendSkillLevel(int skill) {
        PacketBuilder spb = new PacketBuilder(190);
        spb.putByteS((byte) skill);
        spb.putLEInt((int) player.getSkills().getXp(skill));
        if (skill == 5) {
            spb.put((byte) Math.ceil(player.getSettings().getPrayerPoints()));
        } else {
            spb.put((byte) player.getSkills().getLevel(skill));
        }
        player.getSession().write(spb.toPacket());
    }

    public void sendMessage(String message) {
        player.getSession().write(new PacketBuilder(108, Type.VARIABLE).putRS2String(message).toPacket());
    }

    public void sendNewsMessage(String string) {
        for (Player p : World.getWorld().getPlayers()) {
            if (p != null) {
                p.getPacketSender().sendMessage("<col=556B2F>" + string);
            }
        }
    }

    public void sendItems(int interfaceId, int childId, int type, Item[] items) {
        PacketBuilder bldr = new PacketBuilder(92, Type.VARIABLE_SHORT);
        bldr.putInt(interfaceId << 16 | childId);
        bldr.putShort(type);
        bldr.putShort(items.length);
        for (Item item : items) {
            if (item != null) {
                int count = item.getCount();
                if (count > 254) {
                    bldr.putByteC((byte) 255);
                    bldr.putInt(count);
                } else {
                    bldr.putByteC((byte) count);
                }
                bldr.putLEShort(item.fetch_render() + 1);
            } else {
                bldr.putByteC((byte) 0);
                bldr.putLEShort(0);
            }
        }
        player.getSession().write(bldr.toPacket());
    }

    public void sendItem(int interfaceId, int childId, int type, int slot, Item item) {
        PacketBuilder bldr = new PacketBuilder(120, Type.VARIABLE_SHORT);
        bldr.putInt(interfaceId << 16 | childId);
        bldr.putShort(type);
        bldr.putSmart(slot);
        if (item != null) {
            bldr.putShort(item.fetch_render() + 1);
            int count = item.getCount();
            if (count > 254) {
                bldr.put((byte) 255);
                bldr.putInt(count);
            } else {
                bldr.put((byte) count);
            }
        } else {
            bldr.putShort(0);
            bldr.put((byte) -1);
        }
        player.getSession().write(bldr.toPacket());
    }

    public void sendBlankClientScript(int id, String s) {
        PacketBuilder packet = new PacketBuilder(69, Type.VARIABLE_SHORT).putRS2String(s);
        packet.putInt(id);
        player.getSession().write(packet.toPacket());
    }

    public void sendClientScript(int id, Object[] params, String types) {
        PacketBuilder bldr = new PacketBuilder(69, Type.VARIABLE_SHORT);
        bldr.putRS2String(types);
        if (params.length > 0) {
            int j = 0;
            for (int i = types.length() - 1; i >= 0; i--, j++) {
                if (types.charAt(i) == 115) {
                    bldr.putRS2String((String) params[j]);
                } else {
                    bldr.putInt((Integer) params[j]);
                }
            }
        }
        bldr.putInt(id);
        player.getSession().write(bldr.toPacket());
    }

    /**
     * 115: 35913759, 35913757,0 142 doe: var20: 1, var21:35913842 142 doe:
     * var20: 1, var21:35913843 142 doe: var20: 1, var21:35913844 142 doe:
     * var20: 1, var21:35913845 142 doe: var20: 1, var21:35913846 142 doe:
     * var20: 1, var21:35913847 142 doe: var20: 1, var21:35913848 142 doe:
     * var20: 0, var21:35913842
     * <p>
     * //842 - combat options //0 - on 1 - off
     * <p>
     * <p>
     * When I do test118: (without clicking): OFF: 142 doe: var20: 1,
     * var21:35913846 ON: 142 doe: var20: 0, var21:35913846
     *
     * @param interfaceId
     * @param childId
     * @param show
     */

    public void sendInterfaceConfig(int interfaceId, int childId, boolean show) {
        PacketBuilder bldr = new PacketBuilder(142);
        bldr.putByteS((byte) (!show ? 1 : 0));
        bldr.putShort(childId);
        bldr.putShort(interfaceId);
        player.getSession().write(bldr.toPacket());
    }

    public void sendInterfaceScript(int id, int value) {
        PacketBuilder packet = new PacketBuilder(9);
        packet.putInt1(id);
        player.getSession().write(packet.toPacket());
    }

    public void forceSendTab(int tab) {
        sendClientScript(115, new Object[]{tab}, "i");
    }

    public void sendBlankClientScript(int id) {
        PacketBuilder packet = new PacketBuilder(69, Type.VARIABLE_SHORT).putRS2String("");
        packet.putInt(id);
        player.getSession().write(packet.toPacket());
    }

    public void displayInterface(int id) {
        player.getInterfaceSettings().setInterfaceOpened(id);
        sendInterface(548, 62, id, false);
    }

    public void sendConfig(int id, int value) {
        if (value < 128 && value > -128) {
            PacketBuilder bldr = new PacketBuilder(245);
            bldr.putShortA(id);
            bldr.put((byte) value);
            player.getSession().write(bldr.toPacket());
        } else {
            PacketBuilder bldr = new PacketBuilder(37);
            bldr.putShortA(id);
            bldr.putLEInt(value);
            player.getSession().write(bldr.toPacket());
        }
    }

    public void modifyText(String text, int interfaceId, int childId) {
        if (!player.getVariables().interfaceRequiresUpdate(interfaceId, childId, text)) {
            return;
        }
        player.getVariables().registerInterfaceText(interfaceId, childId, text);
        PacketBuilder bldr = new PacketBuilder(47, Type.VARIABLE_SHORT);
        bldr.putInt1(interfaceId << 16 | childId);
        bldr.putRS2String(text);
        player.getSession().write(bldr.toPacket());
    }

    public void sendWindowPane(int pane) {
        player.getSession().write(new PacketBuilder(77).putLEShortA(pane).toPacket());
    }

    public void sendTab(int tabId, int childId) {
        sendInterface(548, tabId, childId, true);
    }

    public void sendInterface(int interfaceId, boolean interfaceClosed) {
        sendInterface(548, 64, interfaceId, false);
    }

    private void sendInterface(int windowId, int position, int interfaceId, boolean walkable) {
        PacketBuilder pb = new PacketBuilder(238);
        pb.putInt1((windowId << 16) | position);
        pb.putShort(interfaceId);
        pb.putByteC(walkable ? 1 : 0);
        player.getSession().write(pb.toPacket());
    }

    public void sendSideBarInterfaces() {
        sendTab(77, 137);// chatbox
        sendTab(86, 92); // attack style
        sendTab(87, 320);
        sendTab(88, 274);
        sendTab(89, 149);
        sendTab(90, 387);//387 // 657
        sendTab(91, 271);
        sendTab(92, player.getSettings().getMagicType() == 1 ? 665 : player.getSettings().getMagicType() == 2 ? 193 : 430);
        sendTab(93, 320);

        sendTab(94, 550);
        sendTab(95, 551);
        player.getPacketSender().modifyText("When you have finished playing " + Constants.SERVER_NAME + ", always use the button below to logout safely.", 182, 0);
        sendTab(96, 182);
        sendTab(97, 655);
        sendTab(98, 464);
        sendTab(99, 612); //Clan Chat
        
        sendTab(14, 264);

        QuestTab.refresh(player);
    }

    /**
     * Sends all the login packets.
     *
     * @return The action sender instance, for chaining.
     */
    public PacketSender sendLogin() {
        player.loadMapRegions();
        sendGameframe(player.getDetails().getGameframe(), false);
        if (player.getVariables().getMode() == 1)
            player.getDetails().setIronman(true);
        else
            player.getDetails().setIronman(false);
        for (int i = 0; i < 5; i++) {
            sendMessage("");
        }
        sendMessage("Welcome to " + Constants.SERVER_NAME + ".");
        //sendQuests(player);
        //Temp fix for <im=> in title
        if (player.getVariables().getTitle().contains("<img") || player.getVariables().getTitle().contains("<str")) {
            player.getVariables().setTitle("");
            player.getPacketSender().sendMessage("Your title has been reset.");
            player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
        }

        Settings.correctPlayerPosition(player);

        long doubleXpTimer = player.getVariables().getExtraExpTimer();
        if (doubleXpTimer > 0) {
            int minutes = (int) ((player.getVariables().getExtraExpTimer()) * 0.6 / 60);
            player.getPacketSender().sendMessage("You have " + minutes + " minutes of Double XP left.");
        }
        sendWelcomeScreen();
        sendLoginSettings();
        return this;
    }

    public void sendQuests(Player player) {
        player.getPacketSender().modifyText("Quest Points: 20", 274, 9);
        player.getPacketSender().modifyText("<col=1dd845>Black Knight's Fortress", 274, 12);
        player.getPacketSender().modifyText("<col=1dd845>Cook's Assistant", 274, 13);
        player.getPacketSender().modifyText("<col=1dd845>The Corsair Curse", 274, 14);
        player.getPacketSender().modifyText("<col=1dd845>Demon Slayer", 274, 15);
        player.getPacketSender().modifyText("<col=1dd845>Doric's Quest", 274, 16);
        player.getPacketSender().modifyText("<col=1dd845>Dragon Slayer", 274, 17);
        player.getPacketSender().modifyText("<col=1dd845>Ernest the Chicken", 274, 18);
        player.getPacketSender().modifyText("<col=1dd845>Goblin Diplomacy", 274, 19);
        player.getPacketSender().modifyText("<col=1dd845>Imp Catcher", 274, 20);
        player.getPacketSender().modifyText("<col=1dd845>The Knight's Sword", 274, 21);
        player.getPacketSender().modifyText("<col=1dd845>Misthalin Mystery", 274, 22);
        player.getPacketSender().modifyText("<col=1dd845>Pirate's Treasure", 274, 23);
        player.getPacketSender().modifyText("<col=1dd845>Prince Ali Rescue", 274, 24);
        player.getPacketSender().modifyText("<col=1dd845>The Restless Ghost", 274, 25);
        player.getPacketSender().modifyText("<col=1dd845>Romeo & Juliet", 274, 26);
        player.getPacketSender().modifyText("<col=1dd845>Rune Mysteries", 274, 27);
        player.getPacketSender().modifyText("<col=1dd845>Sheep Shearer", 274, 28);
        player.getPacketSender().modifyText("<col=1dd845>Shield of Arrav", 274, 29);
        player.getPacketSender().modifyText("<col=1dd845>Vampire Slayer", 274, 30);
        player.getPacketSender().modifyText("<col=1dd845>Witch's Potion", 274, 31);


    }

    public void sendGameframe(int version, boolean reverse) {
        if (player.getDetails().getClientSize() > 0 && !reverse)
            return;
        if (version == 464) {
            sendInterfaceConfig(548, 0, reverse ? false : true); // chat options
            sendInterfaceConfig(548, 44, reverse ? false : true); // bar on top
            // of
            // chatbox
            sendInterfaceConfig(548, 46, reverse ? false : true); // bar on left
            // of
            // gameframe
            sendInterfaceConfig(548, 48, reverse ? false : true); // Container
            // of sprite
            // left of
            // chatbox
            sendInterfaceConfig(548, 50, reverse ? false : true); // right left?
            // of
            // minimap
            sendInterfaceConfig(548, 52, reverse ? false : true); // Right
            // pillar
            sendInterfaceConfig(548, 54, reverse ? false : true); // Bar on top
            // of
            // gameframe
            sendInterfaceConfig(548, 56, reverse ? false : true); // under xp
            // icon
            sendInterfaceConfig(548, 58, reverse ? false : true); // Top pillar
            sendInterfaceConfig(548, 60, reverse ? false : true); // Bottom
            // pillar
            sendInterfaceConfig(548, 72, reverse ? false : true); // minimap
            // client
            // code 1338

            sendInterfaceConfig(548, 221, reverse ? true : false); // new xpc
            sendInterfaceConfig(548, 227, reverse ? true : false); // new run
            sendInterfaceConfig(548, 229, reverse ? true : false); // new pray
            sendInterfaceConfig(655, 50, true);
            sendInterfaceConfig(655, 51, true);
            sendInterfaceConfig(655, 52, true);
            sendInterfaceConfig(655, 53, true);
            sendInterfaceConfig(655, 54, true);
            sendInterfaceConfig(655, 55, true);

        }
        if (version >= 474 && version <= 530) {

            sendInterfaceConfig(548, 108, reverse ? false : true); // Container
            // for all
            // new
            // gameframes
            sendInterfaceConfig(548, version == 474 ? 111 : 109, reverse ? false : true); // Gameframe
            // sprite
            // container

            sendInterfaceConfig(548, 170, reverse ? true : false); // Hide 562
            // tabs top
            sendInterfaceConfig(548, 192, reverse ? true : false); // Hide 562
            // taps low
            if (version == 474) { // Minimap things - same as 464
                sendInterfaceConfig(548, 50, reverse ? false : true); // minimap
                // surround
                sendInterfaceConfig(548, 54, reverse ? false : true); // bar on
                // top
                // of gf
                sendInterfaceConfig(548, 56, reverse ? false : true); // Minimap
                // surround
                sendInterfaceConfig(548, 72, reverse ? false : true); // minimap
                // clickarea
                // client
                // code
                // 1338
                // sendInterfaceConfig(548, 168, reverse ? true : false);
                // //DISABLE minimap clickarea 6969(only for 498+ gameframes)

                sendInterfaceConfig(548, 221, reverse ? true : false); // new
                // xpc
                sendInterfaceConfig(548, 227, reverse ? true : false); // new
                // run
                sendInterfaceConfig(548, 229, reverse ? true : false); // new
                // pray

            } else {
                sendInterfaceConfig(548, 223, reverse ? true : false); // Run
                // orb
                // 464
                sendInterfaceConfig(548, 225, reverse ? true : false); // Prayer
                // orb
                // 464
            }

        } else if (version >= 562) {
            // We must disable tabs because clickareas are diffo.

            sendInterfaceConfig(548, 108, reverse ? false : true); // Container
            // for all
            // new
            // gameframes
            sendInterfaceConfig(548, 158, reverse ? false : true); // 562
            // Gameframe
            sendInterfaceConfig(548, 113, reverse ? true : false); // Top row
            // only
            // 474-525
            sendInterfaceConfig(548, 136, reverse ? true : false); // Lower row
            // only
            // 474-525

            sendInterfaceConfig(548, 170, reverse ? false : true); // Show 562
            // tabs top
            sendInterfaceConfig(548, 192, reverse ? false : true); // Show 562
            // taps low

            sendInterfaceConfig(548, 223, reverse ? true : false); // Run orb
            // 464
            sendInterfaceConfig(548, 225, reverse ? true : false); // Prayer orb
            // 464

        }
    }

    public void changeGameFrame(int currentFrame, int newFrame) {
        if (player.getDetails().getClientSize() > 0)
            return;
        if (currentFrame == 562 || currentFrame == 525) {
            // Going to oldschool, disable newschool things
            sendInterfaceConfig(548, 221, false); // new xpc
            sendInterfaceConfig(548, 227, false); // new run
            sendInterfaceConfig(548, 229, false); // new pray

            sendInterfaceConfig(548, currentFrame == 562 ? 158 : 109, false); // Gameframe

            // sendInterfaceConfig(548, 168, false); //DISABLE minimap clickarea
            // 6969(only for 498+ gameframes)

            // if(newFrame == 474)
            sendInterfaceConfig(548, 170, false); // Hide 562 tabs top
            sendInterfaceConfig(548, 192, false); // Hide 562 taps low

            sendInterfaceConfig(548, 113, true); // Top row only 474-525
            sendInterfaceConfig(548, 136, true); // Lower row only 474-525

        } else {
            // Going newschool, disable oldschool things

            sendInterfaceConfig(548, 111, false); // Gameframe
            sendInterfaceConfig(548, newFrame == 562 ? 158 : 109, true); // Gameframe

            // sendInterfaceConfig(548, 168, true); //ENABLE minimap clickarea
            // 6969(only for 498+ gameframes)

            sendInterfaceConfig(548, 223, false); // Run orb 464
            sendInterfaceConfig(548, 225, false); // Prayer orb 464
            if (newFrame == 562) {
                sendInterfaceConfig(548, 113, false); // Hide Top row only
                // 474-525

                // sendInterfaceConfig(548, 169, true); //Show 562 tabs top
                // sendInterfaceConfig(548, 191, true); //Show 562 taps low
            } else {
                sendInterfaceConfig(548, 170, false); // Hide 562 tabs top
                sendInterfaceConfig(548, 192, false); // Hide 562 taps low

                sendInterfaceConfig(548, 113, true); // Top row only 474-525
                sendInterfaceConfig(548, 136, true); // Lower row only 474-525
            }

            sendInterfaceConfig(548, 0, false); // chat options
            sendInterfaceConfig(548, 44, false); // bar on top of chatbox
            sendInterfaceConfig(548, 46, false); // bar on left of gameframe
            sendInterfaceConfig(548, 48, false); // Container of sprite left of
            // chatbox
            sendInterfaceConfig(548, 50, false); // right left? of minimap
            sendInterfaceConfig(548, 52, false); // Right pillar
            sendInterfaceConfig(548, 54, false); // Bar on top of gameframe
            sendInterfaceConfig(548, 56, false); // under xp icon
            sendInterfaceConfig(548, 58, false); // Top pillar
            sendInterfaceConfig(548, 60, false); // Bottom pillar
            sendInterfaceConfig(548, 72, false); // minimap client code 1338
        }

        sendGameframe(newFrame, false);

        if (newFrame <= 474) {
            sendInterfaceConfig(548, 223, true); // Run orb 464
            sendInterfaceConfig(548, 225, true); // Prayer orb 464
        } else {
            sendInterfaceConfig(548, 221, true); // new xpc
            sendInterfaceConfig(548, 227, true); // new run
            sendInterfaceConfig(548, 229, true); // new pray
        }

    }

    private void sendLoginSettings() {
        TeleportAreaLocations.sendLoginInterfaces(player);
        refreshContainerInterfaces();
        sendEnergy();
        sendSkillLevels();
        sendConfig(172, player.getSettings().isAutoRetaliate() ? 1 : 0);// Auto retal
        sendConfig(173, player.getWalkingQueue().isRunningToggled() ? 1 : 0);// Run
        if (player.getCombatState().isPoisoned()) {
            World.getWorld().submit(new PoisonTask(player, player.getCombatState().getPoisonAmount(), false));
        } else {
            sendConfig(428, 0);
        }
        //Below are opposite -- 0 is slider position 4
        sendConfig(166, 3); //brightness
        sendConfig(168, 0); //music vol
        sendConfig(169, 0); //sound effect
        sendConfig(872, 2); //camera zoom
        sendConfig(555, 0);// exp counter
        sendInterfaceConfig(548, 100, false);// copyright
        sendPlayerOption("Follow", 3, false);
        sendPlayerOption("Trade with", 4, false);
        player.getFriends().refresh();
        player.getFriends().login();
        player.getSettings().refresh();
        ClanChat.cleanInterface(player, true, false, true, false);
        if (player.getAttributes().isSet("last_clan") && player.getAttributes().get("last_clan") != "") {
            ClanChat.join(player, (String) player.getAttributes().get("last_clan"));
        } else {
            ClanChat.join(player, "Help"); // community
        }
        for (int ordinal = 0; ordinal < player.getBound_offer_uids().length; ordinal++) {
            if (player.getBound_offer_uids()[ordinal] != -1) {
                GrandExchangeOffer offer = GrandExchangeMap.forUID(player.getBound_offer_uids()[ordinal]);
                player.getGrandeExchange().getOffers()[ordinal] = offer;
            }
        }
        sendConfig(313, (Skillcape.wearingCape(player) ? 511 : 255));
        sendConfig(465, 511);
        sendConfig(802, 511);
        if (player.getVariables().getFamiliarId() > 0) {
            int id = BossPetDropHandler.forNpc(player.getVariables().getFamiliarId()).getId();
            if (id != -1) {
                if (BossPetDropHandler.spawnPet(player, BossPetDropHandler.forNpc(player.getVariables().getFamiliarId()).getItemId(), false)) {
                    player.getPacketSender().sendMessage("Your familiar is now following you.");
                } else {
                    boolean added = false;
                    if (player.getInventory().addItem(new Item(id, 1))) {
                        player.getPacketSender().sendMessage("Your familiar cannot spawn here, so it has been added to your inventory.");
                        player.getVariables().setFamiliarId(-1);
                        player.getInventory().refresh();
                        added = true;
                    } else if (!added && player.getBank().freeSlots() >= 1 || player.getBank().hasItem(id)) {
                        player.getAttributes().set("currentTab", 0);
                        Bank.depositItem(player, id, 1, false, false, -1);
                        added = true;
                        player.getPacketSender().sendMessage("Your familiar cannot spawn here, so it has been added to your bank.");
                    } else if (!added) {
                        GroundItem groundItem = new GroundItem(new Item(id, 1), player.getLocation(), player);
                        GroundItemManager.create(groundItem, player);
                        player.getPacketSender().sendMessage("Your familiar cannot spawn here, so it has been dropped on the ground.");
                    }
                }
            }
        }
    }

    public void refreshContainerInterfaces() {
        sendSideBarInterfaces();
        player.getEquipment().refresh();
        Equipment.setWeapon(player, true);
        player.getInventory().refresh();
        player.getBonuses().refresh();
    }


    /**
     * Sends the map region load command.
     *
     * @return The action sender instance, for chaining.
     */
    public PacketSender sendMapRegion() {
        player.setLastKnownRegion(player.getLocation());
        PacketBuilder pb = new PacketBuilder(221, Type.VARIABLE_SHORT);
        pb.putShortA(player.getLocation().getChunkY());
        for (int xCalc = (player.getLocation().getChunkX() - 6) / 8; xCalc <= (player.getLocation().getChunkX() + 6) / 8; xCalc++) {
            for (int yCalc = (player.getLocation().getChunkY() - 6) / 8; yCalc <= (player.getLocation().getChunkY() + 6) / 8; yCalc++) {
                int region = yCalc + (xCalc << 8);
                if (Constants.USE_DEV_CACHE) {
                    pb.putInt1(0);
                    pb.putInt1(0);
                    pb.putInt1(0);
                    pb.putInt1(0);
                } else {
                    int[] data = World.getWorld().mapdata.getData(region);
                    pb.putInt1(data[0]);
                    pb.putInt1(data[1]);
                    pb.putInt1(data[2]);
                    pb.putInt1(data[3]);
                }
            }
        }
        int z = player.getLocation().getZ();
        pb.putLEShort(player.getLocation().getChunkX());
        pb.putShort(player.getLocation().getLocalX());
        pb.put((byte) z);
        pb.putShort(player.getLocation().getLocalY());
        player.getSession().write(pb.toPacket());
        return this;
    }

    /**
     * Sends the logout packet.
     *
     * @return The action sender instance, for chaining.
     */
    @SuppressWarnings("rawtypes")
    public PacketSender sendLogout() {
        player.getSession().write(new PacketBuilder(167).toPacket()).addListener(new IoFutureListener() {
            @Override
            public void operationComplete(IoFuture i) {
                i.getSession().close(false);
            }
        });
        return this;
    }

    public void forceLogout() {
        if (!player.getAttributes().isSet("forcelogout")) {
            player.getAttributes().set("forcelogout", true);
        }
        sendLogout();
    }

    /**
     * Sends a projectile to a location.
     *
     * @param start       The starting location.
     * @param finish      The finishing location.
     * @param id          The graphic id.
     * @param delay       The delay before showing the projectile.
     * @param angle       The angle the projectile is coming from.
     * @param speed       The speed the projectile travels at.
     * @param startHeight The starting height of the projectile.
     * @param endHeight   The ending height of the projectile.
     * @param lockon      The lockon index of the projectile, so it follows them if they
     *                    move.
     * @param slope       The slope at which the projectile moves.
     * @param radius      The radius from the centre of the tile to display the
     *                    projectile from.
     * @return The action sender instance, for chaining.
     */
    public PacketSender sendProjectile(Location start, Location finish, int id, int delay, int angle, int speed, int startHeight, int endHeight, Entity lockon, int slope, int radius) {
        int offsetX = (start.getX() - finish.getX()) * -1;
        int offsetY = (start.getY() - finish.getY()) * -1;
        sendProjectileCoords(start);
        PacketBuilder bldr = new PacketBuilder(218);
        bldr.put((byte) angle);
        bldr.put((byte) offsetX);
        bldr.put((byte) offsetY);
        bldr.putShort(lockon == null ? -1 : lockon.isPlayer() ? -(lockon.getIndex() + 1) : lockon.getIndex() + 1);
        bldr.putShort(id);
        bldr.put((byte) startHeight);
        bldr.put((byte) endHeight);
        bldr.putShort(delay);
        bldr.putShort(speed);
        bldr.put((byte) slope);
        bldr.put((byte) radius);
        player.getSession().write(bldr.toPacket());
        return this;
    }

    private void sendProjectileCoords(Location location) {
        PacketBuilder spb = new PacketBuilder(132);
        int regionX = player.getLastKnownRegion().getChunkX();
        int regionY = player.getLastKnownRegion().getChunkY();
        spb.put((byte) (location.getY() - ((regionY - 6) * 8) - 2));
        spb.put((byte) (location.getX() - ((regionX - 6) * 8) - 3));
        player.getSession().write(spb.toPacket());
    }

    public void playMusic(int id) {
        PacketBuilder spb = new PacketBuilder(4);
        spb.putInt(id);
        player.getSession().write(spb.toPacket());
    }

    public void sendSoundEffect(int id1, int id2, int id3) {
        PacketBuilder bldr = new PacketBuilder(172);
        bldr.putShort(id1);// id
        bldr.put((byte) id2);// volume
        bldr.putShort(id3);// delay
        player.getSession().write(bldr.toPacket());
    }

    public void sendMusicEffect(int id) {
        PacketBuilder bldr = new PacketBuilder(208);
        bldr.putTriByte(255);
        bldr.putLEShort(id);
        player.getSession().write(bldr.toPacket());
    }

    /**
     *
     */
    public void sendRunInformation() {
        sendConfig(173, player.getWalkingQueue().isRunningToggled() ? 1 : 0);
        sendInterfaceConfig(261, 51, player.getWalkingQueue().isRunningToggled());
        sendInterfaceConfig(261, 52, !player.getWalkingQueue().isRunningToggled());
    }

    public void closeWelcome() {
        boolean opened = !player.getAttributes().isSet("closed_screen");
        if (opened) {
            player.getAttributes().set("closed_screen", true);
            player.getPacketSender().sendWindowPane(548);
            player.getPacketSender().sendRunInformation();
            player.getPacketSender().sendSideBarInterfaces();

            Equipment.setWeapon(player, true);
            player.getInterfaceSettings().closeInterfaces(false);
        }

        if (TeleportAreaLocations.inWilderness(player.getLocation())) {
            player.playGraphic(1844);
        }

        if (!player.getVariables().hasReceivedStarter() && !player.getAttributes().isSet("inStartTut")) {
            player.getAttributes().set("inStartTut", true);
            player.getAttributes().set("stopMovement", true);
            player.getAttributes().set("stopActions", true);
            player.getAttributes().set("inTutorial", true);
            BeginnerTutorial.start(player);
            //player.getPacketSender().sendInterface(649, false);
            //DialogueManager.handleMisc(player, -2, true);
        }
    }
}
