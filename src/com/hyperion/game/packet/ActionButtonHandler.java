package com.hyperion.game.packet;

import com.hyperion.Logger;
import com.hyperion.game.Constants;
import com.hyperion.game.content.*;
import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.content.clanchat.ClanChat;
import com.hyperion.game.content.clanchat.loot.LootshareStatus;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.content.dialogue.impl.jewelery.impl.AmuletOfGlory;
import com.hyperion.game.content.dialogue.impl.jewelery.impl.GamesNecklace;
import com.hyperion.game.content.dialogue.impl.jewelery.impl.RingOfDueling;
import com.hyperion.game.content.dialogue.impl.jewelery.impl.RingOfSlaying;
import com.hyperion.game.content.dialogue.impl.ladders.LadderOption;
import com.hyperion.game.content.grandexchange.inter.GrandExchangeButtons;
import com.hyperion.game.content.interfaces.Gameframe;
import com.hyperion.game.content.interfaces.tele.TeleportInterface;
import com.hyperion.game.content.minigames.clanwars.ClanWarsManager;
import com.hyperion.game.content.skills.cooking.Cooking;
import com.hyperion.game.content.skills.crafting.Glass;
import com.hyperion.game.content.skills.crafting.Jewellery;
import com.hyperion.game.content.skills.crafting.Leather;
import com.hyperion.game.content.skills.crafting.Spinning;
import com.hyperion.game.content.skills.fletching.Fletching;
import com.hyperion.game.content.skills.herblore.Herblore;
import com.hyperion.game.content.skills.magic.MagicBookHandler;
import com.hyperion.game.content.skills.magic.MagicBookHandler.TeleportType;
import com.hyperion.game.content.skills.prayer.BonesOnAltar;
import com.hyperion.game.content.skills.smithing.Smelting;
import com.hyperion.game.event.EventHandler;
import com.hyperion.game.item.Item;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.net.Packet;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.tickable.impl.RestingTask;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.combat.data.magic.StaffsInterface;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.AttackStyles;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.impl.Bank;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
import com.hyperion.utility.game.InputAction;
import com.hyperion.utility.logger.Log;

public class ActionButtonHandler implements PacketHandler {

    private static final int CLOSE = 71; // d
    private static final int ACTIONBUTTON = 153; // d
    private static final int EXAMINING_ITEM = 205; // d
    private static final int ACTIONBUTTON2 = 113; // d
    private static final int ACTIONBUTTON3 = 240; // d

    @Override
    public void handle(Player player, Packet packet) {
        switch (packet.getOpcode()) {
            case CLOSE:
                handleCloseButton(player, packet);
                break;
            case ACTIONBUTTON:
            case ACTIONBUTTON2:
            case 37:// Trade option 5
            case 134:// Trade option 10
            case 137:// Trade all
            case 140:// Trade x
                handleActionButton(player, packet);
                break;
            case ACTIONBUTTON3:
                handleActionButton3(player, packet);
                break;
            case EXAMINING_ITEM:
                handleExamineItemInterface(player, packet);
                break;
        }
    }

    private void handleActionButton(final Player player, Packet packet) {
        int interfaceId = packet.getShort() & 0xFFFF;
        int buttonId = packet.getShort() & 0xFFFF;
        int buttonId2 = 0;
        if (packet.getLength() >= 6) {
            buttonId2 = packet.getShort() & 0xFFFF;
        }
        if (buttonId2 == 65535) {
            buttonId2 = 0;
        }
        // if (Constants.DEBUG_MODE) {
        // Logger.getInstance().info("inter: " + interfaceId + " button: " + buttonId + ", packet: " + packet.getOpcode());
        // }
        if (AttackStyles.handleAttackStyles(player, interfaceId, buttonId)) {
            return;
        }
        if (TeleportInterface.handleTeleportInterfaceActions(player, interfaceId, buttonId)) {
            return;
        }
        if (EventHandler.handleInterfaceInteraction(player, interfaceId, buttonId, buttonId2, packet.getOpcode())) {
            return;
        }
        if (GrandExchangeButtons.handleMainInterface(player, interfaceId, -1, buttonId, buttonId2, -1) || player.getGrandeExchange().handleOfferInterface(interfaceId, -1, buttonId, buttonId2, -1) || player.getGrandeExchange().handleOfferStatusInterface(interfaceId, -1, buttonId, buttonId2, -1)) {
            return;
        }
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Pressed " + interfaceId + ", button: " + buttonId + ", button2: " + buttonId2));

        Logger.getInstance().warning("Unhandled button inter: " + interfaceId + ", button: " + buttonId + ", button2: " + buttonId2 + ".");
        switch (interfaceId) {
            case CharacterDesign.ID:
                CharacterDesign.handleButtons(player, buttonId);
                break;
            case 668:
                TeleportInterface.doTeleportAction(player, buttonId);
                break;
            case 320:
                int skillMenu = -1;
                switch (buttonId) {
                    case 123:
                        skillMenu = 0;
                        break;
                    case 126:
                        skillMenu = 1;
                        break;
                    case 129:
                        skillMenu = 2;
                        break;
                    case 132:
                        skillMenu = 3;
                        break;
                    case 135:
                        skillMenu = 4;
                        break;
                    case 142:
                        skillMenu = 5;
                        break;
                    case 145:
                        skillMenu = 18;
                        break;
                    case 148:
                        skillMenu = 21;
                        break;
                    case 124:
                        skillMenu = 6;
                        break;
                    case 127:
                        skillMenu = 7;
                        break;
                    case 130:
                        skillMenu = 8;
                        break;
                    case 133:
                        skillMenu = 9;
                        break;
                    case 136:
                        skillMenu = 10;
                        break;
                    case 143:
                        skillMenu = 11;
                        break;
                    case 146:
                        skillMenu = 19;
                        break;
                    case 149:
                        skillMenu = 22;
                        break;
                    case 125:
                        skillMenu = 12;
                        break;
                    case 128:
                        skillMenu = 13;
                        break;
                    case 131:
                        skillMenu = 14;
                        break;
                    case 134:
                        skillMenu = 15;
                        break;
                    case 137:
                        skillMenu = 16;
                        break;
                    case 144:
                        skillMenu = 17;
                        break;
                    case 147:
                        skillMenu = 20;
                        break;
                }
                if (skillMenu != -1) {
                    player.getPacketSender().sendConfig(965, skillMenu);
                    player.getPacketSender().displayInterface(499);
                    player.getAttributes().set("skillMenu", skillMenu);
                }
                break;
            case 499:
                skillMenu = player.getAttributes().get("skillMenu");
                if (skillMenu == -1) {
                    return;
                }
                switch (buttonId) {
                    case 10:
                    case 11:
                    case 12:
                    case 13:
                    case 14:
                    case 15:
                    case 16:
                    case 17:
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                        player.getPacketSender().sendConfig(965, (1024 * (buttonId - 10)) + skillMenu);
                    default:
                        // System.out.println("Unhandled skill tab action button : " +
                        // interfaceId + " - " + buttonId + " - " + childButtonId);
                }
                break;
            case 602:
                if (player.getClanWarsSession() != null) {
                    player.getClanWarsSession().handleInterface(player, buttonId);
                }
                break;
            case 374:
                ClanWarsManager.handleViewingOrb(player, buttonId);
                break;
            case 655:
                System.out.println("button " + buttonId);
                switch (buttonId) {
                    case 4:
                        player.getSettings().setAcceptAidEnabled(!player.getSettings().isAcceptAidEnabled());
                        player.getPacketSender().sendConfig(427, player.getSettings().isAcceptAidEnabled() ? 1 : 0);
                        player.getPacketSender().sendInterfaceConfig(261, 59, player.getSettings().isAcceptAidEnabled());
                        player.getPacketSender().sendInterfaceConfig(261, 60, !player.getSettings().isAcceptAidEnabled());
                        break;
                    case 7: //Brightness
                    case 8:
                    case 9:
                    case 10:
                        //	player.getSettings().setBrightness(10 - buttonId);
                        break;
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                        //player.getSettings().setCameraZoom(33 - buttonId);
                        break;
                }
                if (buttonId == 52) {
                    player.getPacketSender().sendInterfaceConfig(655, 52, true);
                    player.getPacketSender().sendInterfaceConfig(655, 53, false);
                }
                if (buttonId == 53) {
                    player.getPacketSender().sendInterfaceConfig(655, 54, true);
                    player.getPacketSender().sendInterfaceConfig(655, 55, true);
                }
                if (buttonId == 25) {
                    player.getPacketSender().sendInterfaceConfig(655, 0, false);
                    player.getPacketSender().sendInterfaceConfig(655, 1, true);
                    player.getPacketSender().sendInterfaceConfig(655, 2, false);
                    player.getPacketSender().sendInterfaceConfig(655, 3, false);

                    //for (int i = 0; i < 75; i++) {
                    // player.getPacketSender().sendInterfaceConfig(655, i, true);
                    // }
                }
                if (buttonId == 24) {
                    player.getPacketSender().sendInterfaceConfig(655, 0, true);
                    player.getPacketSender().sendInterfaceConfig(655, 1, false);
                    player.getPacketSender().sendInterfaceConfig(655, 2, false);
                    player.getPacketSender().sendInterfaceConfig(655, 3, false);
                    player.getPacketSender().sendInterfaceConfig(655, 24, true);
                    player.getPacketSender().sendInterfaceConfig(655, 25, false);
                    player.getPacketSender().sendInterfaceConfig(655, 26, false);
                    player.getPacketSender().sendInterfaceConfig(655, 27, false);

            		/*for (int i = 30; i < 75; i++)
                    {
            			player.getPacketSender().sendInterfaceConfig(655, i, false);
            		}*/
                }
                if (buttonId == 26) {
                    player.getPacketSender().sendInterfaceConfig(655, 0, false);
                    player.getPacketSender().sendInterfaceConfig(655, 1, false);
                    player.getPacketSender().sendInterfaceConfig(655, 2, true);
                    player.getPacketSender().sendInterfaceConfig(655, 3, false);
                    /*for (int i = 30; i < 75; i++)
                    {
            			player.getPacketSender().sendInterfaceConfig(655, i, false);
            		}*/
                }
                if (buttonId == 27) {
                    player.getPacketSender().sendInterfaceConfig(655, 0, false);
                    player.getPacketSender().sendInterfaceConfig(655, 1, false);
                    player.getPacketSender().sendInterfaceConfig(655, 2, false);
                    player.getPacketSender().sendInterfaceConfig(655, 3, true);
                }
                break;
            case 648: // Titles inter
                if (buttonId >= 123 && buttonId <= 143) {
                    for (int i = 0; i <= 20; i++) {
                        player.getPacketSender().sendInterfaceConfig(648, 165 + i, false);
                    }
                    player.getPacketSender().sendInterfaceConfig(648, buttonId + 42, true);
                    player.getAttributes().set("titleToSet", buttonId == 123 ? -1 : (buttonId - 124));
                    int title = player.getAttributes().get("titleToSet");
                    String titleString = title == -1 ? "None" : Constants.TITLES[title];
                    player.getPacketSender().modifyText(titleString, 648, 116);
                    player.getPacketSender().modifyText("Cost: " + (titleString == "None" ? "None" : player.getVariables().getUnlockedTitles()[title] == true ? "Already unlocked" : Constants.TITLE_COSTS[title] + " Loyalty Points"), 648, 118);
                } else if (buttonId == 121) {
                    if (player.getAttributes().isSet("titleToSet")) {
                        int title = player.getAttributes().get("titleToSet");
                        if (title != -1 && !player.getVariables().getUnlockedTitles()[title]) {
                            if (player.getVariables().getLoyaltyPoints() >= Constants.TITLE_COSTS[title]) {
                                player.getVariables().setLoyaltyPoints(player.getVariables().getLoyaltyPoints() - Constants.TITLE_COSTS[title]);
                                player.getPacketSender().sendMessage(Constants.TITLE_COSTS[title] + " Loyalty Points have been deducted. You have " + player.getVariables().getLoyaltyPoints() + " points remaining.");
                            } else {
                                player.getPacketSender().sendChatboxDialogue(true, "You don't have enough loyalty points!");
                                player.getPacketSender().sendMessage("You don't have enough loyalty points!");
                                return;
                            }
                        }
                        String titleString = title == -1 ? "" : Constants.TITLES[title];
                        player.getPacketSender().sendChatboxDialogue(false, "Updating forum title - please wait..");
                        int beginIndex = titleString.contains("<shad=") ? 20 : 12;

                        player.getVariables().setTitle(titleString);
                        player.getInterfaceSettings().closeInterfaces(false);
                        player.getPacketSender().sendChatboxDialogue(true, "Your title has been set to: ", (titleString == "" ? "None" : titleString));
                        player.getPacketSender().sendMessage("Your title has been set to: " + (titleString == "" ? "None" : titleString));
                        player.getAttributes().remove("titleToSet");
                        player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
                    }
                } else {
                    player.getPacketSender().sendChatboxDialogue(true, "You have not selected a title!");
                    player.getPacketSender().sendMessage("You have not selected a title!");
                }

                break;
            case 598: // rcing
                Location loc = null;
                switch (buttonId) {
                    case 17:// air
                        loc = Location.create(2841, 4829, 0);
                        break;
                    case 18:// water
                        loc = Location.create(2727, 4833, 0);
                        break;
                    case 19:// earth
                        loc = Location.create(2655, 4830, 0);
                        break;
                    case 20:// fire
                        loc = Location.create(2574, 4849, 0);
                        break;
                    case 21:// mind
                        loc = Location.create(2792, 4827, 0);
                        break;
                    case 22:// cosmic
                        loc = Location.create(2142, 4853, 0);
                        break;
                    case 23:// law
                        loc = Location.create(2464, 4818, 0);
                        break;
                    case 24:// nat
                        loc = Location.create(2400, 4835, 0);
                        break;
                    case 25:// chaos
                        loc = Location.create(2281, 4837, 0);
                        break;
                    case 26:// death
                        loc = Location.create(2208, 4830, 0);
                        break;
                    case 27:// blood
                        loc = Location.create(2467, 4888, 1);
                        break;
                    case 28:// astral
                        loc = Location.create(2155, 3857, 0);
                        break;
                }
                if (loc != null)
                    MagicBookHandler.teleportPlayer(player, loc, TeleportType.MODERN, null);
                break;
            case 378:// Welcome screen
                switch (buttonId) {
                    case 6:// play
                        player.getPacketSender().closeWelcome();
                        break;
                }
                break;
            case 649: // Pick game mode inter
                System.out.println("Button " + buttonId);

                if (buttonId == 2 || buttonId == 11) {
                    player.getPacketSender().sendInterfaceConfig(649, 9, true);
                    player.getPacketSender().sendInterfaceConfig(649, 8, false);
                    player.getPacketSender().sendInterfaceConfig(649, 56, false);
                    player.getPacketSender().sendInterfaceConfig(649, 57, false);
                    player.getAttributes().set("selectedIronMan", 2);
                } else if (buttonId == 10 || buttonId == 1) {
                    player.getAttributes().set("selectedIronMan", 1);
                    player.getPacketSender().sendInterfaceConfig(649, 9, false);
                    player.getPacketSender().sendInterfaceConfig(649, 8, true);
                    player.getPacketSender().sendInterfaceConfig(649, 56, false);
                    player.getPacketSender().sendInterfaceConfig(649, 57, false);
                } else if (buttonId == 56) {
                    player.getAttributes().set("selectedIronMan", 3);
                    player.getPacketSender().sendInterfaceConfig(649, 9, false);
                    player.getPacketSender().sendInterfaceConfig(649, 8, false);
                    player.getPacketSender().sendInterfaceConfig(649, 56, true);
                    player.getPacketSender().sendInterfaceConfig(649, 57, false);
                } else if (buttonId == 57) {
                    player.getAttributes().set("selectedIronMan", 4);
                    player.getPacketSender().sendInterfaceConfig(649, 9, false);
                    player.getPacketSender().sendInterfaceConfig(649, 8, false);
                    player.getPacketSender().sendInterfaceConfig(649, 56, false);
                    player.getPacketSender().sendInterfaceConfig(649, 57, true);
                } else if (buttonId == 4) {
                    if (!player.getAttributes().isSet("inTutorial"))
                        return;
                    if (!player.getAttributes().isSet("selectedIronMan")) {
                        player.getAttributes().set("selectedIronMan", 0);
                    }
                    int mode = player.getAttributes().getInt("selectedIronMan");
                    player.getVariables().setMode(mode);

                    player.getDetails().setIronman(true);

                    player.getPacketSender().sendMessage("You have selected the " + (player.getVariables().getMode() == 1 ? "Iron Man" : "Normal") + " game mode.");
                    if (player.getDetails().isNewRegistration() && (player.getDetails().getCurrentEmail() == "none" || player.getDetails().getCurrentEmail() == null || player.getDetails().getCurrentEmail() == "")) {
                        player.getAttributes().set("enterEmail", true);
                        player.getInterfaceSettings().openEnterTextInterface(612, 0, "A forum account has been created for you! Enter your email address:");
                    } else {
                        player.getAttributes().remove("stopMovement");
                        player.getAttributes().remove("stopActions");
                        player.getAttributes().remove("inTutorial");
                        player.getAttributes().remove("tutorialStage");
                        player.teleport(3233, 3229, 0);
                        BeginnerTutorial.giveStarter(player);
                        player.getPacketSender().sendChatboxDialogue(false, "Select your character looks and clothing above");
                        player.getPacketSender().modifyText("" + Constants.SERVER_NAME + " Character Design", 269, 97);
                        player.getPacketSender().displayInterface(269);
                    }
                }

                break;

            case 14:// Bank pin settings.
                switch (buttonId) {
                    case 133:// Delete pin
                        Bank.deletePin(player);
                        break;
                    case 132:// Change pin
                        if (player.getVariables().getBankPin() == null)
                            Bank.displayFirstConfirmation(player);
                        else
                            Bank.changePin(player);
                        break;
                    case 135:// cancel pending pin
                        Bank.openPinSettings(player, 4);
                        break;
                    case 130:// set pin
                        Bank.displayFirstConfirmation(player);
                        break;
                    case 131:// Change pin delay
                    case 134:// Change pin delay
                        Bank.changePinDelay(player);
                        break;
                    case 225:// Yes
                        if (Bank.isPinPending(player)) {
                            Bank.verifyPin(player, true);
                            break;
                        }
                        Bank.openEnterPin(player);
                        break;
                    case 230:// No
                        if (Bank.isPinPending(player)) {
                            Bank.cancelPendingPin(player);
                            break;
                        }
                        Bank.openPinSettings(player, 2);
                        break;
                }
                break;
            case 446:// Jewl making
                Jewellery.makeJewellery(player, buttonId, 1, true);
                break;
            case 107:// Stake duel first inter.
                if (player.getDuelSession() != null) {
                    player.getDuelSession().toggleDuelRules(buttonId);
                }
                break;
            case 106:// Stake 2nd duel inter
                if (player.getDuelSession() != null) {
                    switch (buttonId) {
                        case 143:// Accept
                            player.getDuelSession().acceptDuel();
                            break;
                    }
                }
                break;
            case 432:// Enchanting bolts
                // BoltsEnchanting.handleBoltsEnchanting(player, buttonId);
                break;
            case 542: // Craft glass.
                switch (buttonId) {
                    case 40: // Make 1 beer glass.
                        Glass.craftGlass(player, 1, 0, true);
                        break;

                    case 41: // Make 1 candle lantern.
                        Glass.craftGlass(player, 1, 1, true);
                        break;

                    case 42: // Make 1 oil lamplayer.
                        Glass.craftGlass(player, 1, 2, true);
                        break;

                    case 38: // Make 1 vial.
                        Glass.craftGlass(player, 1, 3, true);
                        break;

                    case 44: // Make 1 Fishbowl
                        Glass.craftGlass(player, 1, 4, true);
                        break;

                    case 39: // Make 1 orb.
                        Glass.craftGlass(player, 1, 5, true);
                        break;

                    case 43: // Make 1 lantern lens
                        Glass.craftGlass(player, 1, 6, true);
                        break;

                    case 45: // Make 1 dorgeshuun light orb.
                        Glass.craftGlass(player, 1, 7, true);
                        break;
                }
                break;
            case 675:// Jewl crafting
                Jewellery.makeJewellery(player, buttonId, 1, true);
                break;
            case 154: // Craft normal leather
                Leather.init_crafting_leather(player, buttonId, -1, true);
                break;
            case 311: // Smelt inter.
                Smelting.smeltOre(player, buttonId, -1, true);
                break;
            case 669:// modern
            case 388:// ancients
            case 797:// ancients with miasmic
                StaffsInterface.setAutoCastingSpell(player, buttonId);
                break;
            case 464: // Emote tab.
                Emotes.emote(player, buttonId);
                break;
            case 271:// Prayer tab
            case 597:// Curses tab
                player.getPrayers().activatePrayers(buttonId);
                break;
            case 612: // Clan chat
                switch (buttonId) {
                    case 136: // Join chat / Leave chat
                        if (player.getAttributes().isSet("clan_chat_id")) {
                            ClanChat.leave(player, false);
                        } else {
                            player.getPacketSender().requestStringInput(new InputAction<String>("Enter name of Channel Owner:") {
                                @Override
                                public void handleInput(String input) {
                                    ClanChat.join(player, input);
                                }
                            });
                            player.getInterfaceSettings().openEnterTextInterface(612, 0, "Enter name of channel owner:");
                        }
                        break;
                    case 138:// Clan setup button
                        if (player.getAttributes().isSet("clan_chat_id")) {
                            int clanId = player.getAttributes().getInt("clan_chat_id");
                            if (ClanChat.isOwner(player, clanId)) {
                                DialogueManager.handleMisc(player, -1, true);
                            } else {
                                // normal player shit, we're in A clan though. not ours
                                player.getPacketSender().sendMessage("You do not have permission to moderate this channel.");
                            }
                        } else {
                            // not in a clan yet
                            player.getAttributes().set("enterNewName", true);
                            player.getInterfaceSettings().openEnterTextInterface(612, 0, "Enter a name for your channel:");
                        }
                        break;
                    case 132:
                        if (player.getVariables().isEnabled()) {
                            player.getVariables().getLootshare().setStatus(LootshareStatus.DISABLED);
                            ClanChat.disableLootshare(player);
                            player.getPacketSender().sendInterfaceConfig(612, 141, false);
                            System.out.println("disabled");
                        } else {
                            player.getVariables().getLootshare().setStatus(LootshareStatus.ENABLED);
                            ClanChat.enableLootshare(player);
                            player.getPacketSender().sendInterfaceConfig(612, 141, true);
                            System.out.println("eman;ed");

                        }
                        break;
                }
                break;
            case 763:// Bank screen - inventory
                if (player.getVariables().isBanking()) {
                    Bank.deposit(player, buttonId2, 1, true);
                }
                break;
            case 12:// Bank screen
                switch (buttonId) {
                    case 37:
                        int previousTab = player.getAttributes().get("currentTab") != null ? player.getAttributes().getInt("currentTab") : 0;
                        player.getAttributes().set("currentTab", 0);
                        if (Constants.DEBUG_MODE)
                            player.getPacketSender().sendMessage("View tab: 0");
                        Bank.refreshBank(player, previousTab == 0 ? false : true);
                        break;
                    case 16:// Insert - Swap toggle
                    case 17:// Insert - Swap toggle
                        if (!player.getBank().isInserting()) {
                            player.getBank().setInserting(true);
                        } else {
                            player.getBank().setInserting(false);
                        }
                        break;
                    case 10:// Note
                    case 11:// Item
                        player.getSettings().setWithdrawAsNotes(
                                buttonId == 10);
                        break;
                    case 117:
                        player.sendMessage("This feature has not been added yet.");
                        break;
                    case 118:
                        player.sendMessage("This feature has not been added yet.");
                        break;
                    case 119: // Deposit Inventory
                        if (player.getInventory().freeSlots() == 28) {
                            player.getPacketSender().sendMessage("Your inventory is clearAllBoxes!");
                        } else {
                            for (int i = 0; i < player.getInventory().getItems().length; i++) {
                                Item invyItem = player.getInventory().get(i);
                                if (invyItem != null)
                                    Bank.deposit(player, i, invyItem.getCount(), false);
                            }
                            //player.getPacketSender().sendMessage("Inventory successfully deposited.");
                        }
                        Bank.refreshBank(player, false); //no need to refresh, all in same tab
                        break;
                    case 120: // Deposit Equipment
                        if (player.getEquipment().getTotalItems() == 0) {
                            player.getPacketSender().sendMessage("You aren't wearing any equipment!");
                        } else {
                            for (int i = 0; i < player.getEquipment().getItems().length; i++) {
                                Item equipI = player.getEquipment().get(i);
                                if (equipI != null) {
                                    // player.getEquipment().deleteItem(equipI);
                                    // Equipment.unequipItem(player, equipI.getId(),
                                    // player.getEquipment().getSlotById(equipI.getId()),
                                    // false);
                                    Equipment.unequipToBank(player, equipI.getId(), player.getEquipment().getSlotById(equipI.getId()), false);
                                }
                            }
                            //  player.getPacketSender().sendMessage("Equipment successfully deposited.");
                        }
                        Bank.refreshBank(player, false); //no need to refresh tabs
                        break;
                }
                break;
            case 336:// Trade screen - inventory || equip - inv
                if (player.getTradingSession() != null) {
                    switch (packet.getOpcode()) {
                        case 113:// Offer 1
                            player.getTradingSession().tradeItem(player.getInventory().getItemInSlot(buttonId2), buttonId2, 1);
                            break;
                        case 37:// Offer 5
                            player.getTradingSession().tradeItem(player.getInventory().getItemInSlot(buttonId2), buttonId2, 5);
                            break;
                        case 134:// Offer 10
                            player.getTradingSession().tradeItem(player.getInventory().getItemInSlot(buttonId2), buttonId2, 10);
                            break;
                        case 137:// Offer all
                            Item item = player.getInventory().getSlot(buttonId2);
                            if (item == null) {
                                break;
                            }
                            int amount = player.getInventory().getItemAmount(item.getId());
                            if (item.getDefinition().isStackable()) {
                                amount = player.getInventory().getAmountInSlot(buttonId2);
                            }
                            player.getTradingSession().tradeItem(player.getInventory().getItemInSlot(buttonId2), buttonId2, amount);
                            break;
                        case 140:// Offer x
                            player.getInterfaceSettings().openEnterAmountInterface(interfaceId, buttonId2, player.getInventory().getItemInSlot(buttonId2), "Enter amount:");
                            break;
                    }
                } else if (player.getDuelSession() != null) {
                    player.getDuelSession().stakeItem(player.getInventory().getItemInSlot(buttonId2), buttonId2, 1);
                } else {
                    Equipment.equipItem(player, player.getInventory().getItemInSlot(buttonId2), buttonId2, true);
                }
                break;
            case 335:// Trade screen
                if (player.getTradingSession() != null) {
                    switch (packet.getOpcode()) {
                        case 113:// Remove 1
                            switch (buttonId) {
                                case 48:// Item area
                                    player.getTradingSession().removeItem(player.getTrade().getItemInSlot(buttonId2), buttonId2, 1);
                                    break;
                                case 17:// Accept
                                    player.getTradingSession().accept();
                                    break;
                                case 18:// Decline
                                    player.getTradingSession().decline(true);
                                    break;
                            }
                            break;
                        case 37:// Remove 5
                            player.getTradingSession().removeItem(player.getTrade().getItemInSlot(buttonId2), buttonId2, 5);
                            break;
                        case 134:// Remove 10
                            player.getTradingSession().removeItem(player.getTrade().getItemInSlot(buttonId2), buttonId2, 10);
                            break;
                        case 137:// Remove all
                            Item item = player.getTrade().getSlot(buttonId2);
                            if (item == null) {
                                break;
                            }
                            int amount = player.getTrade().getItemAmount(item.getId());
                            if (item.getDefinition().isStackable()) {
                                amount = player.getTrade().getAmountInSlot(buttonId2);
                            }
                            player.getTradingSession().removeItem(player.getTrade().getItemInSlot(buttonId2), buttonId2, amount);
                            break;
                        case 140:// Remove x
                            player.getInterfaceSettings().openEnterAmountInterface(interfaceId, buttonId2, player.getTrade().getItemInSlot(buttonId2), "Enter amount:");
                            break;
                    }
                }
                break;
            case 334:// 2nd Trade screen
                if (player.getTradingSession() != null) {
                    switch (buttonId) {
                        case 20:// Accept
                            player.getTradingSession().accept();
                            break;
                        case 21:// Decline
                            player.getTradingSession().decline(true);
                            break;
                    }
                }
                break;
            case 665:
            case 193:
            case 430:
                MagicBookHandler.handleSpellBook(player, interfaceId, buttonId);
                break;
            case 548:// Game Window
                switch (buttonId) {
                    case 30:
                        Gameframe.setRedStone(player, 1);
                        break;
                    case 31:
                        Gameframe.setRedStone(player, 2);
                        break;
                    case 32:// quest tab
                        Gameframe.setRedStone(player, 3);
                        break;
                    case 33:
                        Gameframe.setRedStone(player, 4);
                        break;
                    case 34:
                        Gameframe.setRedStone(player, 5);
                        break;
                    case 35:
                        Gameframe.setRedStone(player, 6);
                        break;
                    case 36:
                        Gameframe.setRedStone(player, 7);
                        break;
                    case 19:
                        Gameframe.setRedStone(player, 8);
                        break;
                    case 14:
                        Gameframe.setRedStone(player, 9);
                        break;
                    case 15:
                        Gameframe.setRedStone(player, 10);
                        break;
                    case 16:
                        Gameframe.setRedStone(player, 11);
                        break;
                    case 17:
                        Gameframe.setRedStone(player, 12);
                        break;
                    case 18:
                        Gameframe.setRedStone(player, 13);
                        break;
                    case 13:
                        Gameframe.setRedStone(player, 14);
                        break;
                    case 251: // fullscreen quest
                        QuestTab.refresh(player);
                        break;
                    case 101:
                    case 222:
                        player.getVariables().handle_counter(packet.getOpcode() == 37); // opcode
                        // 37
                        // is
                        // click
                        // 2
                        break;
                    case 226: // run <=474
                    case 228: // run >=525

                        if (packet.getOpcode() == 37) { // click 2
                            if (player.getAttributes().isSet("isResting") || player.getAttributes().isSet("stopActions")) {
                                player.getAttributes().remove("isResting");
                                player.animate(11788);
                                return;
                            } else {
                                World.getWorld().submit(new RestingTask(player));
                            }
                        } else {
                            if (player.getAttributes().isSet("isResting")) {
                                player.getAttributes().remove("isResting");
                                player.animate(11788);
                            } else {
                                player.getWalkingQueue().setRunningToggled(!player.getWalkingQueue().isRunningToggled());
                                player.getPacketSender().sendRunInformation();
                            }
                        }
                        break;
                    case 8:// Report abuse
                    case 317:
                    case 168:
                /*if (!player.getAttributes().isSet("reportabuse")) {
                    resetActions(player);
					player.getAttributes().set("reportabuse", true);
					player.getPacketSender().displayInterface(553);
					player.getPacketSender().sendInterfaceConfig(553, 16, true);
					//135 close
					//134 mod option(mute)
					//133 operate '|' yellow instead ofs tar
					player.getPacketSender().sendClientScript(136, new Object[] {}, "");
				}*/
                        break;
                }
                break;
            case 670:
                if (buttonId == 137) { // Set quest tab {
                    player.getAttributes().set("questTab", 1);
                    player.getPacketSender().sendTab(88, 274);

                }
                break;
            case 274: // Quest Tab
                if (buttonId == 10) { // Set achievements page
                    player.getAttributes().set("questTab", 2);
                } else if (buttonId == 137) { // Set quests page
                    player.getAttributes().set("questTab", 1);
                } else if (buttonId == 138) { // Set player tab
                    player.getAttributes().set("questTab", 0);
                } else if (buttonId == 141) { // Set player tab {
                    player.getAttributes().set("questTab", 2);
                    player.getPacketSender().sendTab(88, 670);
                }
                QuestTab.refresh(player);

                // handle buttons in tab
                int tab = player.getAttributes().getInt("questTab");
                if (tab == -1)
                    tab = 0; // Player tab
                switch (tab) {
                    case 0: // Player tab
                        break;
                    case 1: // Quest tab
                        int quest = buttonId - 11;
                        QuestTab.openQuestInterface(player, quest);
                        break;
                    case 2: // Achievements tab
                        resetActions(player);
                        if (buttonId == 11) {
                            player.getAttributes().set("achievementType", 0);
                            Achievements.openJournal(player, 0);
                        } else if (buttonId == 12) {
                            player.getAttributes().set("achievementType", 1);
                            Achievements.openJournal(player, 0);
                        } else if (buttonId == 13)
                            player.getPacketSender().sendMessage("Coming soon!");
                        // Achievements.openHardJournal();
                        // else if (buttonId == 14)
                        // player.getPacketSender().sendMessage("Coming soon!");
                        // Achievements.openExtremeJournal();*/
                        break;
                }
                break;
            case 609: // Achievements inter
            case 610:
            case 611:
                int page = player.getAttributes().getInt("achievementPage");
                if (page == -1)
                    page = 0;
                //	switch (type) {
                //	case 0: // Easy
                if (buttonId == 48) // Next page
                    if (page == 0) {
                        Achievements.openJournal(player, 1);
                        return;
                    } else if (page == 1) {
                        Achievements.openJournal(player, 2);
                        return;

                    }
                if (buttonId == 46) // Previous page
                    if (page == 0)
                        return;
                    else if (page == 1) {
                        Achievements.openJournal(player, 0);
                        return;
                    } else if (page == 2) {
                        Achievements.openJournal(player, 1);
                        return;
                    }
                break;
            //	case 1: // Medium
            //		break;
            //	case 2: // Hard
            //		break;
            //	case 3: // Extreme
            //		break;
            //	}
            //	break;
            case 584:// Togglables
                player.getSettings().handleTogglableSettings(buttonId);
                break;
            case 261:// Settings tab.
                switch (buttonId) {
                    case 0:
                        player.getWalkingQueue().setRunningToggled(!player.getWalkingQueue().isRunningToggled());
                        player.getPacketSender().sendConfig(173, player.getWalkingQueue().isRunningToggled() ? 1 : 0);
                        player.getPacketSender().sendInterfaceConfig(261, 51, player.getWalkingQueue().isRunningToggled());
                        player.getPacketSender().sendInterfaceConfig(261, 52, !player.getWalkingQueue().isRunningToggled());
                        break;
                    case 1:
                        if (!player.getSettings().isChatEffectsEnabled()) {
                            player.getSettings().setChatEffectsEnabled(true);
                            player.getPacketSender().sendConfig(171, 0);
                        } else {
                            player.getSettings().setChatEffectsEnabled(false);
                            player.getPacketSender().sendConfig(171, 1);
                        }
                        break;
                    case 2:
                        player.getSettings().setPrivateChatSplit(!player.getSettings().isPrivateChatSplit(), true);
                        player.getPacketSender().sendConfig(287, player.getSettings().isPrivateChatSplit() ? 1 : 0);
                        player.getPacketSender().sendInterfaceConfig(261, 55, player.getSettings().isPrivateChatSplit());
                        player.getPacketSender().sendInterfaceConfig(261, 56, !player.getSettings().isPrivateChatSplit());
                        player.getPacketSender().refreshContainerInterfaces(); // update
                        break;
                    case 3:
                        if (!player.getSettings().isMouseTwoButtons()) {
                            player.getSettings().setMouseTwoButtons(true);
                            player.getPacketSender().sendConfig(170, 0);
                        } else {
                            player.getSettings().setMouseTwoButtons(false);
                            player.getPacketSender().sendConfig(170, 1);
                        }
                        break;

                    case 5:// Togglable options
                        player.getSettings().sendTogglables(true);
                        break;
                }
                break;
            case 182:// logout
                //for(int i = 0; i < Integer.MAX_VALUE; i++) {
                player.getPacketSender().modifyText("Log out of " + Constants.SERVER_NAME + "", 182, 0); //269
                //player.getPacketSender().modifyText("Log out of R N", 2451, 3); //269
                //}
                boolean canLogout = !(System.currentTimeMillis() - player.getCombatState().getLogoutTimer() < 10000);
                if (!canLogout) {
                    resetActions(player);
                    player.getPacketSender().sendMessage("You can't logout until 10 seconds after the end of combat.");
                    break;
                }
                player.getPacketSender().sendLogout();
                break;
            case 657:
                switch (buttonId) {
                    case 17:// Equipment tab
                        resetActions(player);
                        Equipment.displayEquipmentScreen(player);
                        break;
                    case 21:// Items kept on death
                        resetActions(player);
                        ItemsKeptOnDeath.display(player);
                        break;
                }
                break;
            case 102:// Items on death inter
                if (buttonId == 18) {
                    player.getPacketSender().sendMessage("You will keep this item if you should die.");
                } else {
                    player.getPacketSender().sendMessage("You will lose this item if you should die.");
                }
                break;
        }

        if (interfaceId >= 622 && interfaceId <= 647 && buttonId == 2)
            player.getInterfaceSettings().closeInterfaces(false);

    }

    /*
     * Called when a non-walkable inter is opened
     */
    private void resetActions(Player player) {
        if (!player.getAttributes().isSet("stopActions")) {
            player.getWalkingQueue().reset();
            player.resetActionAttributes();
            MainCombat.endCombat(player, 1);
        }
    }

    private void handleActionButton3(Player player, Packet packet) {
        int interfaceShit = packet.getInt();
        int interfaceId = interfaceShit >> 16;
        int id = interfaceShit & 0xffff;
        boolean bones_on_altar = player.getAttributes().isSet("bonesonaltar");
        int craftType = player.getAttributes().getInt("craftType");
        int cookItem = player.getAttributes().getInt("meatItem");
        int leatherCraft = player.getAttributes().getInt("leatherCraft");
        int unfinishedPotion = player.getAttributes().getInt("unfinishedPotion");
        int completePotion = player.getAttributes().getInt("completePotion");
        int grindItem = player.getAttributes().getInt("herbloreGrindItem");

        if (interfaceId >= 157 && interfaceId <= 177) {
            player.getPacketSender().closeChatboxInterface();
            return;
        }
        if (interfaceId >= 210 && interfaceId <= 212) {
            player.getPacketSender().closeChatboxInterface();
            return;
        }
        System.out.println("Pressed2 " + interfaceId + ", button: " + id);
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Pressed2 " + interfaceId + ", button: " + id));
        switch (interfaceId) {
            case 303:// fletching?
            case 305:// fletching
            case 582:// fletching
                Fletching.handleFletching(player, id);
                break;
            case 309:// Crafting - gems
                if (craftType != -1) {
                    if (craftType >= 50 && craftType <= 60) { // Cut gem
                        switch (id) {
                            case 6:// Make 1
                                Jewellery.cutGem(player, craftType, 1, true);
                                break;
                            case 5:// Make 5
                                Jewellery.cutGem(player, craftType, 5, true);
                                break;
                            case 4:// Make x
                                player.getInterfaceSettings().openEnterAmountInterface("Enter amount to cut:", interfaceId, id);
                                break;
                            case 3:// Make All
                                Jewellery.cutGem(player, craftType, 28, true);
                                break;
                        }
                    }
                } else if (grindItem != -1) {
                    switch (id) {
                        case 6:// Make 1
                            Herblore.grindIngredient(player, 1, true);
                            break;
                        case 5:// Make 5
                            Herblore.grindIngredient(player, 5, true);
                            break;
                        case 4:// Make x
                            player.getInterfaceSettings().openEnterAmountInterface("Enter amount to cut:", interfaceId, id);
                            break;
                        case 3:// Make All
                            Herblore.grindIngredient(player, 28, true);
                            break;
                    }
                } else if (unfinishedPotion != -1) {
                    switch (id) {
                        case 6:// Make 1
                            Herblore.makeUnfinishedPotion(player, 1, true);
                            break;
                        case 5:// Make 5
                            Herblore.makeUnfinishedPotion(player, 5, true);
                            break;
                        case 4:// Make x
                            player.getInterfaceSettings().openEnterAmountInterface("Enter amount to cut:", interfaceId, id);
                            break;
                        case 3:// Make All
                            Herblore.makeUnfinishedPotion(player, 28, true);
                            break;
                    }
                } else if (completePotion != -1) {
                    switch (id) {
                        case 6:// Make 1
                            Herblore.completePotion(player, 1, true);
                            break;
                        case 5:// Make 5
                            Herblore.completePotion(player, 5, true);
                            break;
                        case 4:// Make x
                            player.getInterfaceSettings().openEnterAmountInterface("Enter amount to cut:", interfaceId, id);
                            break;
                        case 3:// Make All
                            Herblore.completePotion(player, 28, true);
                            break;
                    }
                } else if (bones_on_altar) {
                    BonesOnAltar.handleBones(player, null, -1, -1, -1);
                } else {
                    Fletching.handleFletching(player, id);
                }
                break;
            case 304:// Crafting - spinning
                if (leatherCraft != -1) {
                    switch (id) {
                        case 12:// Make 1 - Vambraces
                            Leather.craftDragonHide(player, 1, 4, leatherCraft, true);
                            break;
                        case 11:// Make 5 - Vambraces
                            Leather.craftDragonHide(player, 5, 4, leatherCraft, true);
                            break;
                        case 10:// Make 10 - Vambraces
                            Leather.craftDragonHide(player, 10, 4, leatherCraft, true);
                            break;
                        case 9:// Make x - vambraces
                        case 13:// Make x - chaps
                        case 5:// Make x - body
                            player.getInterfaceSettings().openEnterAmountInterface("Enter amount to make:", interfaceId, id);
                            break;
                        case 16:// Make 1 - chaps
                            Leather.craftDragonHide(player, 1, 8, leatherCraft, true);
                            break;
                        case 15:// Make 5 - chaps
                            Leather.craftDragonHide(player, 5, 8, leatherCraft, true);
                            break;
                        case 14:// Make 10 - chaps
                            Leather.craftDragonHide(player, 10, 8, leatherCraft, true);
                            break;
                        case 8:// Make 1 - body
                            Leather.craftDragonHide(player, 1, 0, leatherCraft, true);
                            break;
                        case 7:// Make 5 - body
                            Leather.craftDragonHide(player, 5, 0, leatherCraft, true);
                            break;
                        case 6:// Make 10 - body
                            Leather.craftDragonHide(player, 10, 0, leatherCraft, true);
                            break;
                    }
                    return;
                } else if (craftType != -1) {
                    if (craftType == 6) {// Crossbow string
                        switch (id) {
                            case 8:// Make 1 - Ball of wool
                                Spinning.craftSpinning(player, 1, 0, true);
                                break;
                            case 7:// Make 5 - Ball of wool
                                Spinning.craftSpinning(player, 5, 0, true);
                                break;
                            case 6:// Make 10 - Ball of wool
                                Spinning.craftSpinning(player, 10, 0, true);
                                break;
                            case 5:// Make X - Ball of wool
                                player.getInterfaceSettings().openEnterAmountInterface("Enter amount to make:", interfaceId, id);
                                break;
                            case 12:// Make 1 - Bow string
                                Spinning.craftSpinning(player, 1, 1, true);
                                break;
                            case 11:// Make 5 - Bow string
                                Spinning.craftSpinning(player, 5, 1, true);
                                break;
                            case 10:// Make 10 - Bow string
                                Spinning.craftSpinning(player, 10, 1, true);
                                break;
                            case 9:// Make x - Bow string
                                player.getInterfaceSettings().openEnterAmountInterface("Enter amount to make:", interfaceId, id);
                                break;
                            case 16:// Make 1 - crossbow string
                                Spinning.craftSpinning(player, 1, 2, true);
                                break;
                            case 15:// Make 5 - crossbow string
                                Spinning.craftSpinning(player, 5, 2, true);
                                break;
                            case 14:// Make 10 - crossbow string
                                Spinning.craftSpinning(player, 10, 2, true);
                                break;
                            case 13:// Make x - crossbow string
                                player.getInterfaceSettings().openEnterAmountInterface("Enter amount to make:", interfaceId, id);
                                break;
                        }
                    }
                }
                break;
            case 307:// Used for skills?
                if (cookItem != -1) {
                    switch (id) {
                        case 6:// Cook 1
                            Cooking.cookItem(player, 1, true);
                            break;
                        case 5:// Cook 5
                            Cooking.cookItem(player, 5, true);
                            break;
                        case 4:// Cook X
                            player.getInterfaceSettings().openEnterAmountInterface("Enter amount to cook:", interfaceId, id);
                            break;
                        case 3:// Cook All
                            Cooking.cookItem(player, 28, true);
                            break;
                    }
                }
                break;
            case 228:// two options inter
            case 230:// three options inter
            case 232:// four options inter
            case 234:// five options inter

                // Climb Ladder Up Down Option
                if (player.getAttributes().get("grand_tree_dialogue") != null) {
                    if (interfaceId == 230 && id == 1) {
                        player.animate(714);
                        player.playGraphic(1039, 0, 0);
                        World.getWorld().submit(new Tickable(player, 4) {
                            @Override
                            public void execute() {
                                this.stop();
                                player.teleport(Location.create(2463, 3444, 0));
                                player.animate(65535);
                                World.getWorld().submit(new Tickable(player, 2) {
                                    @Override
                                    public void execute() {
                                        this.stop();
                                        player.getAttributes().remove("stopActions");
                                    }
                                });
                            }
                        });
                        player.getAttributes().remove("grand_tree_dialogue");
                        player.getAttributes().remove("teleporting");
                    }
                }

                // Banker Dialogues
                if (player.getAttributes().get("banker_dialogue") != null) {
                    if (interfaceId == 232 && id == 1) {
                        player.getAttributes().remove("banker_dialogue");
                        player.getPacketSender().closeChatboxInterface();
                        Bank.open(player);
                    } else if (interfaceId == 232 && id == 2) {
                        player.getAttributes().remove("banker_dialogue");
                        player.getPacketSender().closeChatboxInterface();
                        player.sendMessage("Sorry bank pins are currently disabled.");
                        //Bank.openPinSettings(player,1);                    }
                    }
                }

                // Climb Ladder Up Down Option
                if (player.getAttributes().get("ladder_up_down") != null) {
                    if (interfaceId == 228 && id == 1) {
                        player.getAttributes().set("ladder_up_down", 1);
                    } else if (interfaceId == 228 && id == 2) {
                        player.getAttributes().set("ladder_up_down", 2);
                    }
                    LadderOption.handleOptionSelection(player);
                }

                // Games Necklace
                if (player.getAttributes().get("games_dialogue") != null) {
                    if (interfaceId == 230 && id == 1) {
                        player.getAttributes().set("games_dialogue", 1);
                    } else if (interfaceId == 230 && id == 2) {
                        player.getAttributes().set("games_dialogue", 2);
                    } else if (interfaceId == 230 && id == 3) {
                        player.getAttributes().set("games_dialogue", 3);
                    }
                    GamesNecklace.operateJewelery(player);
                }

                // Dueling Ring
                if (player.getAttributes().get("dueling_ring_dialogue") != null) {
                    if (interfaceId == 228 && id == 1) {
                        player.getAttributes().set("dueling_ring_dialogue", 1);
                    } else if (interfaceId == 228 && id == 2) {
                        player.getAttributes().set("dueling_ring_dialogue", 2);
                    }
                    RingOfDueling.operateJewelery(player);
                }

                // Ring Of Slaying
                if (player.getAttributes().get("slaying_ring_dialogue") != null) {
                    if (interfaceId == 228 && id == 1) {
                        player.getAttributes().set("slaying_ring_dialogue", 1);
                    } else if (interfaceId == 228 && id == 2) {
                        player.getAttributes().set("slaying_ring_dialogue", 2);
                    } else if (interfaceId == 228 && id == 3) {
                        player.getAttributes().set("slaying_ring_dialogue", 3);
                    } else if (interfaceId == 228 && id == 4) {
                        player.getAttributes().set("slaying_ring_dialogue", 4);
                    }
                    RingOfSlaying.operateJewelery(player);
                }

                // Amulet Of Glory
                if (player.getAttributes().get("glory_dialogue") != null) {
                    if (interfaceId == 232 && id == 1) {
                        player.getAttributes().set("glory_dialogue", 1);
                    } else if (interfaceId == 232 && id == 2) {
                        player.getAttributes().set("glory_dialogue", 2);
                    } else if (interfaceId == 232 && id == 3) {
                        player.getAttributes().set("glory_dialogue", 3);
                    } else if (interfaceId == 232 && id == 4) {
                        player.getAttributes().set("glory_dialogue", 4);
                    }
                    AmuletOfGlory.operateJewelery(player);
                }
                if (player.getAttributes().get("teleport_wizard_option") != null) {
                    if (interfaceId == 228 && id == 1) {
                        player.getAttributes().set("teleport_wizard_option", true);
                    } else if (interfaceId == 228 && id == 2) {
                        player.getAttributes().set("teleport_wizard_option", false);
                    }
                }
                if (player.getAttributes().get("demonic_gnome_dialogue") != null) {
                    if (interfaceId == 228 && id == 1) {
                        player.getAttributes().set("demonic_gnome_dialogue", 1);
                    } else if (interfaceId == 228 && id == 2) {
                        player.getAttributes().set("demonic_gnome_dialogue", 2);
                    }
                }
                if (player.getAttributes().get("captain_barnaby_dialogue") != null) {
                    if (interfaceId == 228 && id == 1) {
                        player.getAttributes().set("captain_barnaby_dialogue", 1);
                    } else if (interfaceId == 228 && id == 2) {
                        player.getAttributes().set("captain_barnaby_dialogue", 2);
                    }
                }
                if (player.getAttributes().get("boarder_guard_dialogue") != null) {
                    if (interfaceId == 230 && id == 3) {
                        player.getAttributes().set("boarder_guard_dialogue", 3);
                    } else if (interfaceId == 230 && id == 2) {
                        player.getAttributes().set("boarder_guard_dialogue", 2);
                    } else if (interfaceId == 230 && id == 1) {
                        player.getAttributes().set("boarder_guard_dialogue", 1);
                    }
                }
                if (player.getAttributes().isSet("godbookcasket")) {
                    GodBooksCasket.handleOption(player, id);
                } else if (player.getAttributes().isSet("forfietDuel")) {
                    if (player.getDuelSession() != null) {
                        player.getDuelSession().forfietDuel(id);
                    }
                } else if (player.getAttributes().isSet("nextDialogue")) {
                    DialogueManager.handleOptions(player, (NPC) player.getAttributes().get("dialogueNPC"), id);
                } else {
                    player.getPacketSender().closeChatboxInterface();
                }
                break;
            case 240:// Dunno
            case 241:// One line dialogue inter - npc
            case 242:// Two lined dialogue inter - npc
            case 243:// Three lined dialogue inter - npc
            case 244:// Four lined dialogue inter - npc
            case 64:// One line dialogue inter - player
            case 65:// Two lined dialogue inter - player
            case 66:// Three lined dialogue inter - player
            case 67:// Four lined dialogue inter - player

                if (player.getAttributes().isSet("nextDialogue")) {
                    if (player.getAttributes().getInt("nextDialogue") == -1) {
                        player.getPacketSender().closeChatboxInterface();
                        break;
                    }
                    DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
                } else {
                    player.getPacketSender().closeChatboxInterface();
                }
                break;
            case 210: // chatbox dialogues(also item dialogues)
            case 211:
            case 213:
            case 214:
            case 215:
            case 216:
            case 217:
            case 218:
                if (player.getAttributes().isSet("nextDialogue")) {
                    if (player.getAttributes().getInt("nextDialogue") == -1) {
                        player.getPacketSender().closeChatboxInterface();
                        break;
                    }
                    DialogueManager.handleDialogues(player, (NPC) player.getAttributes().get("dialogueNPC"), player.getAttributes().getInt("npcOption"));
                } else {
                    player.getPacketSender().closeChatboxInterface();
                }
        }
        if (Constants.DEBUG_MODE) {
            player.getPacketSender().sendMessage("Interface: " + interfaceId + " button: " + id);
        }
    }

    private void handleCloseButton(final Player player, Packet packet) {
        if (player.getAttributes().isSet("reportabuse")) {
            player.getAttributes().remove("reportabuse");
            player.getPacketSender().sendBlankClientScript(80);
        }
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Closed Interface: " + player.getInterfaceSettings().getCurrentInterface()));
        player.getInterfaceSettings().closeInterfaces(false);
    }

    private void handleExamineItemInterface(Player player, Packet packet) {
        if (packet.getLength() < 1)
            return;
        int id = packet.getShort() & 0xFFFF;
        // if (id < 0 || id > CachedItemDefinition.definitions_666.length) {
        // return;
        // }
        ItemDefinition def = ItemDefinition.forId(id);
        if (def != null) {
            player.getPacketSender().sendMessage(def.getDescription());
        }
    }

    @Override
    public boolean canExecute(Player player, Packet packet) {
        // TODO Auto-generated method stub
        return true;
    }
}