package com.hyperion.game.packet;

import com.hyperion.game.net.Packet;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.player.Player;

public class AppearanceHandler implements PacketHandler {

	@Override
	public void handle(Player player, Packet packet) {
		// TODO Auto-generated method stub
		final int gender = packet.get();
		final int head = packet.get();
		int beard = packet.get();
		final int chest = packet.get();
		final int arms = packet.get();
		final int hands = packet.get();
		final int legs = packet.get();
		final int feet = packet.get();
		final int hairColour = packet.get();
		final int torsoColour = packet.get();
		final int legColour = packet.get();
		final int feetColour = packet.get();
		final int skinColour = packet.get();
		boolean female = gender == 1;
		if (gender < 0 || gender > 1) {
			return;
		}
		if (female) {
			beard = 1000;
		}
		player.getAppearance().setGender(gender);
		player.getAppearance().setColour(0, hairColour);
		player.getAppearance().setColour(1, torsoColour);
		player.getAppearance().setColour(2, legColour);
		player.getAppearance().setColour(3, feetColour);
		player.getAppearance().setColour(4, skinColour);
		player.getAppearance().setLook(0, head);
		player.getAppearance().setLook(1, beard);
		player.getAppearance().setLook(2, chest);
		player.getAppearance().setLook(3, arms);
		player.getAppearance().setLook(4, hands);
		player.getAppearance().setLook(5, legs);
		player.getAppearance().setLook(6, feet);
		player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
		player.getInterfaceSettings().closeInterfaces(true);
	}

	@Override
	public boolean canExecute(Player player, Packet packet) {
		return true;
	}

}
