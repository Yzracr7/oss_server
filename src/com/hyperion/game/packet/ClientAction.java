package com.hyperion.game.packet;

import com.hyperion.game.Constants;
import com.hyperion.game.content.randomevent.EvilBobEvent;
import com.hyperion.game.net.Packet;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;

/**
 * Packets relating to client operation.
 */
public class ClientAction implements PacketHandler {

	private static final int IDLE = 230; // d
	private static final int MOVE_CAMERA = 21; // d
	private static final int PING = 202; // 93
	private static final int FOCUS = 22; // d
	private static final int CLICK_MOUSE = 99; // d
	private static final int WINDOW_TYPE = 243; // d
	private static final int SOUND_SETTINGS = 98; // d
	private static final int MAP_REGION = 174;

	@Override
	public void handle(Player player, Packet packet) {
		switch (packet.getOpcode()) {
		case MAP_REGION:
			handleChangingMapRegion(player, packet);
			break;
		case CLICK_MOUSE:
			handleMouseClicking(player, packet);
			break;
		case IDLE:
			// System.out.println("Player AFK, logged out " + player.getName());
			// player.getPacketSender().forceLogout();
			break;
		case PING:
			handlePing(player, packet);
			break;
		case MOVE_CAMERA:
		case FOCUS:
		case SOUND_SETTINGS:
			break;

		case WINDOW_TYPE:
			handleScreenSettings(player, packet);
			break;
		}
	}

	@SuppressWarnings("unused")
	private void handleScreenSettings(Player player, Packet packet) {
		int windowType = packet.getByte() & 0xff;
		int windowWidth = packet.getShort();
		int windowHeight = packet.getShort();
		int junk = packet.getByte() & 0xff;
	}

	private void handleMouseClicking(Player player, Packet packet) {
		// TODO: minimize packet - if minimized kick player if doing actions
		int hash = packet.getInt();
		int clicks = 0;
		if (player.getAttributes().isSet("lastClickHash") && (int) player.getAttributes().get("lastClickHash") == hash) {
			if (player.getAttributes().isSet("identicalHashClicks")) {
				clicks = (int) player.getAttributes().get("identicalHashClicks");
				player.getAttributes().set("identicalHashClicks", clicks + 1);
			} else {
				player.getAttributes().set("identicalHashClicks", (int) 1);
			}
		} else if (player.getAttributes().isSet("lastClickHash") && (int) player.getAttributes().get("lastClickHash") != hash) {
			player.getAttributes().set("identicalHashClicks", 0);
		}

		if (clicks >= 30) {
			player.getAttributes().set("identicalHashClicks", (int) 0);
			if (Constants.DEBUG_MODE) {
				//player.getPacketSender().sendMessage("ten identical clicks");
			}
			if(NumberUtils.random(0, 100) > 90)
				EvilBobEvent.start(player);
		}

		player.getAttributes().set("lastClickHash", (int) hash);
		player.getAttributes().set("last_click_received", System.currentTimeMillis());
		// int y = packet.getInt();
		// packet.getInt();
		// System.out.println("Dunno: " + dunno);
		// int hash = (int)packet.getInt();
		// int time = hash >>> 20;
		// int button = (hash >>> 19) & 1;
		// int coords = hash & 524287;
		// int y = coords / 765;
		// int x = coords - (y * 765);
		// return new MouseClickEvent(time, button, x, y);
	}

	private void handleChangingMapRegion(Player player, Packet packet) {

	}

	private void handlePing(Player player, Packet packet) {
		//World.getWorld().addPing(player.getName());
	}

	@Override
	public boolean canExecute(Player player, Packet packet) {
		return true;
	}
}