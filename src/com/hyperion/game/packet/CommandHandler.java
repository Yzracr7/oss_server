package com.hyperion.game.packet;

import com.hyperion.game.Constants;
import com.hyperion.game.net.Packet;
import com.hyperion.game.world.entity.player.Player;

/**
 * @author Nando
 */
public class CommandHandler implements PacketHandler {

    @Override
    public void handle(final Player player, Packet packet) {
        String command = packet.getRS2String();
        if (command == null) {
            return;
        }
        if (!Constants.SERVER_HOSTED) {
            com.hyperion.game.content.commands.CommandHandler.handleCommand(player, command);
        }
    }

    @Override
    public boolean canExecute(Player player, Packet packet) {
        return true;
    }
}