package com.hyperion.game.packet;

import java.util.concurrent.TimeUnit;

import com.hyperion.Server;
import com.hyperion.game.Constants;
import com.hyperion.game.content.clanchat.ClanChat;
import com.hyperion.game.net.Packet;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.masks.ChatMessage;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.ProtocolUtils;
import com.hyperion.utility.TextUtils;
import com.hyperion.utility.json.JsonManager;
import com.hyperion.utility.json.impl.PunishmentLoader;
import com.hyperion.utility.json.sub.Punishment.PunishmentType;
import com.hyperion.utility.logger.Log;

public class Communication implements PacketHandler {

	private static final int PUBLIC = 115; // d
	private static final int ADD_FRIEND = 197; // d
	private static final int DELETE_FRIEND = 133; // d
	private static final int ADD_IGNORE = 102; // d
	private static final int DELETE_IGNORE = 214; // d
	private static final int SEND_PM = 238; // d
	private static final int PRIVACY_SETTINGS = 157; // d 11
	private static final int REPORT_ABUSE = 99;

	@Override
	public void handle(Player player, Packet packet) {
		switch (packet.getOpcode()) {
		case PUBLIC:
			handlePublicChat(player, packet);
			break;
		case ADD_FRIEND:
			handleAddFriend(player, packet);
			break;
		case DELETE_FRIEND:
			handleDeleteFriend(player, packet);
			break;
		case ADD_IGNORE:
			handleAddIgnore(player, packet);
			break;
		case DELETE_IGNORE:
			handleDeleteIgnore(player, packet);
			break;
		case SEND_PM:
			handleSendPm(player, packet);
			break;
		case PRIVACY_SETTINGS:
			handlePrivacySettings(player, packet);
			break;
		case REPORT_ABUSE:
			handleReportAbuse(player, packet);
			break;
		}
	}

	private void handlePublicChat(Player player, Packet packet) {
		if (player.getChatMessageQueue().size() >= 4) {
			return;
		}
		if(packet.getLength() <= 3)
		    return;
		int effects = packet.getByte() & 0xFF;
		int colour = packet.getByte() & 0xFF;
		int size = packet.get() & 0xFF;
		String text = ProtocolUtils.decryptPlayerChat(packet, size);
		if (text.startsWith(";;") || text.startsWith(">>")) {
			com.hyperion.game.content.commands.CommandHandler.handleCommand(player, text.substring(2).trim());
			World.getWorld().loggingThread.appendLog(new Log(player.getName(), "typed command " + text + "."));
			return;
		}
		if (JsonManager.getSingleton().getLoader(PunishmentLoader.class).isPunished(player.getDetails().getIP(), PunishmentType.IPMUTE) || JsonManager.getSingleton().getLoader(PunishmentLoader.class).isPunished(player.getName(), PunishmentType.MUTE)) {
			long duration = JsonManager.getSingleton().getLoader(PunishmentLoader.class).getPunishmentTime(player.getDetails().getIP(), PunishmentType.IPMUTE);
			if (duration == -1)
				duration = JsonManager.getSingleton().getLoader(PunishmentLoader.class).getPunishmentTime(player.getName(), PunishmentType.MUTE);
			player.getPacketSender().sendMessage("You are muted for " + (TimeUnit.MILLISECONDS.toHours(duration - System.currentTimeMillis())) + " more hours.");
			return;
		}
		if (text.startsWith("/")) {
			if (text.length() > 1) {
				if (player != null && player.getVariables().isNoTeleport()) {
					if (player.getLocation().distanceToPoint(Location.create(3013, 3189, 0)) < 15) {
						return;
					}
				}
				ClanChat.speak(player, text.substring(1, 2).toUpperCase() + text.substring(2));
				World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Said[CC] " + text + "."));
			}
			return;
		}
		World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Said " + text + "."));
		player.getChatMessageQueue().add(new ChatMessage(player, effects, size, text, colour, null));
	}

	private void handleAddFriend(Player player, Packet packet) {
		String name = packet.getRS2String();
		if (name != null) {
			if (Constants.ENABLE_LOGIN_SERVER)
				Server.loginServer.engine.getPacketSender().friendAdded(player.getName(), name);
			else
				player.getFriends().addFriend(name);
		}
	}

	private void handleDeleteFriend(Player player, Packet packet) {
		String name = packet.getRS2String();
		if (name != null) {
			player.getFriends().removeFriend(name);
			if (Constants.ENABLE_LOGIN_SERVER) {
				Server.loginServer.engine.getPacketSender().friendDeleted(player.getName(), name);
			}
		}
	}

	private void handleAddIgnore(Player player, Packet packet) {
		String name = packet.getRS2String();
		if (name != null) {
			player.getFriends().addIgnore(name);
		}
	}

	private void handleDeleteIgnore(Player player, Packet packet) {
		String name = packet.getRS2String();
		if (name != null) {
			player.getFriends().removeIgnore(name);
		}
	}

	private void handleSendPm(Player player, Packet packet) {
		if (JsonManager.getSingleton().getLoader(PunishmentLoader.class).isPunished(player.getDetails().getIP(), PunishmentType.IPMUTE) || JsonManager.getSingleton().getLoader(PunishmentLoader.class).isPunished(player.getName(), PunishmentType.MUTE)) {
			long duration = JsonManager.getSingleton().getLoader(PunishmentLoader.class).getPunishmentTime(player.getDetails().getIP(), PunishmentType.IPMUTE);
			if (duration == -1)
				duration = JsonManager.getSingleton().getLoader(PunishmentLoader.class).getPunishmentTime(player.getName(), PunishmentType.MUTE);
			player.getPacketSender().sendMessage("You are muted for " + (TimeUnit.MILLISECONDS.toHours(duration - System.currentTimeMillis())) + " more hours.");
			return;
		}
		long nameAsLong = packet.getLong();
		String name = TextUtils.longToName(nameAsLong);
		byte[] lol = new byte[packet.getLength() - 8];
		packet.get(lol);
		int size = lol.length;
		String text = ProtocolUtils.textUnpack(lol, size);
		World.getWorld().loggingThread.appendLog(new Log(player.getName(), "PM to " + name + ": " + text + "."));
		byte[] packed = new byte[size];
		ProtocolUtils.textPack(packed, text);
		if (text != null && name != null) {
			if (Constants.ENABLE_LOGIN_SERVER) {
				player.getPacketSender().sendSentPrivateMessage(name, packed);
				Server.loginServer.engine.getPacketSender().sendPm(name, text, player.getDetails().getRights(), player.getName());
			} else {
				player.getFriends().sendMessage(name, packed);
			}
		}
	}

	private void handlePrivacySettings(Player player, Packet packet) {
		int publicStatus = packet.getByte();
		int privateStatus = packet.getByte();
		int tradeStatus = packet.getByte();
		player.getFriends().setPrivacyOption(publicStatus, privateStatus, tradeStatus);
	}

	private void handleReportAbuse(Player player, Packet packet) {
		long textAsLong = packet.getLong();
		int something = packet.getByte();
		String text = TextUtils.longToName(textAsLong);
		System.out.println("text: " + text + " something: " + something);
		// player.getFrames().sendBlankClientScript(1362);
		// 131 script
	}

	@Override
	public boolean canExecute(Player player, Packet packet) {
		return true;
	}
}