package com.hyperion.game.packet;

import com.hyperion.game.net.Packet;
import com.hyperion.game.world.entity.player.Player;

/**
 * Reports information about unhandled packets.
 * @author Graham Edgecombe
 *
 */
public class DefaultPacketHandler implements PacketHandler {
	
	@Override
	public void handle(Player player, Packet packet) {
		System.out.println("Unhandled Packet : [opcode=" + packet.getOpcode() + " length=" + packet.getLength() + " payload=" + packet.getPayload() + "]");
	}

	@Override
	public boolean canExecute(Player player, Packet packet) {
		// TODO Auto-generated method stub
		return true;
	}

}
