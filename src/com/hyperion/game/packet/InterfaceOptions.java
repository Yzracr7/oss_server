package com.hyperion.game.packet;

import com.hyperion.Logger;
import com.hyperion.game.Constants;
import com.hyperion.game.content.Skillcape;
import com.hyperion.game.content.clanchat.ClanChat;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.content.dialogue.impl.jewelery.HandleJewlery;
import com.hyperion.game.content.grandexchange.inter.GrandExchangeMainInterface;
import com.hyperion.game.content.grandexchange.inter.GrandExchangeOpenOffer;
import com.hyperion.game.content.grandexchange.offer.OfferState;
import com.hyperion.game.content.skills.crafting.Glass;
import com.hyperion.game.content.skills.crafting.Leather;
import com.hyperion.game.content.skills.smithing.Smithing;
import com.hyperion.game.item.Item;
import com.hyperion.game.net.Packet;
import com.hyperion.game.tickable.impl.GrandExchangeMap;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.impl.Bank;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
import com.hyperion.game.world.shopping.Shop;
import com.hyperion.game.world.shopping.ShopManager;
import com.hyperion.utility.Debugging;
import com.hyperion.utility.NumberUtils;
import com.hyperion.utility.game.InputAction;
import com.hyperion.utility.logger.Log;

public class InterfaceOptions implements PacketHandler {

    private static final int ENTER_AMOUNT = 78; // d
    private static final int ENTER_TEXT = 65; // 244
    //private static final int REPORT_ABUSE = 73;
    private static final int CLICK_1 = 177; // d
    private static final int CLICK_2 = 88; // d
    private static final int CLICK_3 = 159; // d
    private static final int CLICK_4 = 86; // d
    private static final int CLICK_5 = 220; // d
    private static final int CLICK_6 = 168; // d
    private static final int CLICK_7 = 166; // d
    private static final int CLICK_8 = 64; // d
    private static final int CLICK_9 = 53; // d

    @Override
    public void handle(Player player, Packet packet) {
        if (Constants.DEBUG_MODE)
            player.getPacketSender().sendMessage("Interface packet: " + packet.getOpcode());
        switch (packet.getOpcode()) {
            case CLICK_1:
                handleClickOne(player, packet);
                break;
            case CLICK_2:
                handleClickTwo(player, packet);
                break;
            case CLICK_3:
                handleClickThree(player, packet);
                break;
            case CLICK_4:
                handleClickFour(player, packet);
                break;
            case CLICK_5:
                handleClickFive(player, packet);
                break;
            case CLICK_6:
                handleClickSix(player, packet);
                break;
            case CLICK_7:
                handleClickSeven(player, packet);
                break;
            case CLICK_8:
                handleClickEight(player, packet);
                break;
            case CLICK_9:
                handleClickNine(player, packet);
                break;
            case ENTER_AMOUNT:
                handleEnterAmount(player, packet);
                break;
            case ENTER_TEXT:
                handleEnterText(player, packet);
                break;
            //case REPORT_ABUSE:
            //	handleReportAbuse(player, packet);
            //	break;
            default:
                break;

        }
    }

    private void handleEnterAmount(Player player, Packet packet) {
        int amount = packet.getInt();
        if (amount <= 0 || amount > Constants.MAX_ITEMS) {
            return;
        }
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Entered amount on inter " + amount + "."));
        if (player.getAttributes().get("input_action") != null) {
            InputAction<Integer> action = player.getAttributes().get("input_action");
            player.getAttributes().remove("input_action");
            action.handleInput(amount);
            return;
        }
        if (player.getInterfaceSettings().isEnterAmountInterfaceOpen()) {
            player.getInterfaceSettings().closeEnterAmountInterface(amount);
        }
    }

    private void handleEnterText(Player player, Packet packet) {
        String text = packet.getRS2String();
        text = text.trim();
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Entered Text(join clan) on inter " + text + "."));
        if (player.getAttributes().get("input_action") != null) {
            InputAction<String> action = player.getAttributes().get("input_action");
            player.getAttributes().remove("input_action");
            action.handleInput(text);
            return;
        }
        if (player.getAttributes().isSet("enterNewName")) {
            if (ClanChat.clanExists(player.getName())) {
                ClanChat.changeClanName(player, text);
            } else {
                ClanChat.create(player, text);
            }
            player.getAttributes().remove("enterNewName");
        } else if (player.getAttributes().isSet("kickPlayer")) {
            if (ClanChat.clanExists(player.getName())) {
                ClanChat.kick(player, World.getWorld().find_player_by_name(text));
            }
            player.getAttributes().remove("kickPlayer");
        } /*else if (player.getAttributes().isSet("enterEmail")) {
            player.getPacketSender().sendChatboxDialogue(false, "Updating email address - please wait..");
			if (text.length() > 1 && text.contains("@") && text.contains(".")) {
				player.getAttributes().remove("enterEmail");
				if (World.getWorld().forumIntegration.setEmail(player.getName(), text) == 1) {
					player.getPacketSender().sendMessage("Your email address has been set to: " + text);
					player.getAttributes().remove("stopMovement");
					player.getAttributes().remove("stopActions");
					player.getAttributes().remove("inTutorial");
					player.getAttributes().remove("tutorialStage");
					BeginnerTutorial.giveStarter(player);
					player.getPacketSender().sendChatboxDialogue(false, "<col=0000FF>Email address set!</col>", "Select your character looks and clothing above");
					player.getPacketSender().displayInterface(269);
				} else {
					player.getAttributes().set("enterEmail", true);
					player.getInterfaceSettings().openEnterTextInterface(612, 0, "Error setting email, try entering it again:");
				}
			} else {
				player.getAttributes().set("enterEmail", true);
				player.getInterfaceSettings().openEnterTextInterface(612, 0, "Email invalid, try entering it again:");
			}
		}*/
    }

	/*private void handleReportAbuse(Player player, Packet packet) {
		int int1 = packet.getInt1();
		short short1 = packet.getShort();
		short short2 = packet.getLEShortA();
		short short3 = packet.getLEShortA();
		short short4 = packet.getLEShort();
		System.out.println("Report: " + int1 + ", " + short1 + ", " + short2 + ", " + short3 + ", " + short4);
	}*/

    private void handleClickOne(Player player, Packet packet) {
        int itemId = packet.getShort();
        int interfaceShit = packet.getInt1();
        int slot = packet.getShort();
        int interfaceId = interfaceShit >> 16;
        int child = interfaceShit & 0xffff;
        if (player.getAttributes().isSet("stopActions")) {
            return;
        }
        Item item = null;
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Clicked item1 " + itemId + "," + slot + "," + interfaceId + "," + child + "."));
        if (player.getDetails().getName().equals("woo")) {
            player.sendMessage("handleClickOne ~ inter :" + interfaceId + " - child :" + child);
        }
        switch (interfaceId) {
            case 300:// Shop screen
                if (player.getAttributes().isSet("shopping")) {
                    switch (child) {
                        case 75:// Value
                            Shop shop = (Shop) player.getAttributes().get("shop");
                            item = shop.getStockItem(slot);
                            if (item == null) {
                                break;
                            }
                            ShopManager.getShopManager().valueItem(player, item, 0);
                            break;
                    }
                }
                break;
            case 301:// Shop inventory
                if (player.getAttributes().isSet("shopping")) {
                    switch (child) {
                        case 0:// Value
                            item = player.getInventory().getSlot(slot);
                            if (item == null) {
                                break;
                            }
                            ShopManager.getShopManager().valueItem(player, item, 1);
                            break;
                    }
                }
                break;
            case 312: //Smithing
                Smithing.smithItem(player, interfaceShit, slot, 6, true);
                break;
            case 109:// Duel screen - inventory deposit 1
                if (slot < 0 || slot > 27) {
                    break;
                }
                if (player.getDuelSession() != null) {
                    player.getDuelSession().stakeItem(itemId, slot, 1);
                }
                break;
            case 107:// Duel screen - withdraw 1
                if (slot < 0 || slot > 27) {
                    break;
                }
                if (player.getDuelSession() != null) {
                    player.getDuelSession().removeItem(itemId, slot, 1);
                }
                break;
            case 12:// Banking - withdraw 1
                if (slot < 0 || slot > 400) {
                    break;
                }
                if (!player.getVariables().isBanking() || !canBank(player))
                    return;
                if (interfaceShit != Constants.BANK_TAB_HEADER) {
                    Bank.withdraw(player, slot, 1, false);
                } else {
                    //player.getBank().switchTab(slot + 1);
                    int size = Bank.getTabSize(player, slot + 1);
                    if (size > 0) {
                        int previousTab = player.getAttributes().get("currentTab") != null ? player.getAttributes().getInt("currentTab") : 0;
                        if (Constants.DEBUG_MODE)
                            player.getPacketSender().sendMessage("View tab: " + (slot + 1));
                        player.getAttributes().set("currentTab", slot + 1);
                        Bank.refreshBank(player, previousTab == (slot + 1) ? false : true);
                    } else {
                        player.getPacketSender().sendMessage("You have nothing in this tab yet, drag an item to it to create a new tab!");
                    }
                }
                break;
            case 15:// Banking - inventory deposit 1
                if (slot < 0 || slot > 27) {
                    break;
                }

                if (player.getVariables().isBanking() && canBank(player)) {
                    Bank.deposit(player, slot, 1, true);
                } else if (player.getVariables().isUsing_grand_exchange()) {
                    player.getGrandeExchange().constructSale(new Item(itemId, 1));
                    return;
                }
                break;
            case 387:// Unequip inv
            case 465:// Equip inter - unequip
                if (slot < 0 || slot > 13) {
                    break;
                }
                Equipment.unequipItem(player, itemId, slot, interfaceId == 465);
                break;
            case 674:
                System.out.println("Child " + child);
                if (child == 13) {
                    if (player.getGrandeExchange().getOpenedOffer() == null) {
                    }
                    if (player.getGrandeExchange().getOpenedOffer().isSell()) {
                        OfferState state = player.getGrandeExchange().getOpenedOffer().getState();
                        if (state == OfferState.COMPLETED || state == OfferState.ABORTED) {
                            if (player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].getWithdraw()[0] == null && player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].getWithdraw()[0] == null) {
                                player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()] = null;
                                player.getBound_offer_uids()[player.getGrandeExchange().getOpenedOfferIndex()] = -1;
                                GrandExchangeMainInterface.open(player, false);
                                return;
                            }
                            if (player.getInventory().hasEnoughRoomFor(player.getGrandeExchange().getOpenedOffer().getWithdraw()[0].getId())) {
                                if (player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].getWithdraw()[0].getId() != 7774) {
                                    player.getInventory().addItem(player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].getWithdraw()[0].getId(), player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].getWithdraw()[0].getCount());
                                    player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].setWithdraw(new Item[]{new Item(player.getGrandeExchange().getOpenedOffer().getWithdraw()[0].getId(), player.getGrandeExchange().getOpenedOffer().getWithdraw()[0].getCount() - 1), null});
                                    if (player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].getWithdraw()[0].getCount() == 0) {
                                        player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].setWithdraw(new Item[]{null, null});
                                    }
                                }
                                GrandExchangeMap.updateOffer(player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()]);
                                if (player.getGrandeExchange().getOpenedOffer().getCompletedAmount() == player.getGrandeExchange().getOpenedOffer().getAmount()) {
                                    player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()] = null;
                                    player.getBound_offer_uids()[player.getGrandeExchange().getOpenedOfferIndex()] = -1;
                                    GrandExchangeMainInterface.open(player, false);
                                }
                            }
                        } else {
                            System.out.println("atm i am here");

                            if (player.getInventory().hasEnoughRoomFor(player.getGrandeExchange().getOpenedOffer().getItemId())) {
                                System.out.println("atm i am here 2");

                                if (player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].getWithdraw()[0].getId() != 7774) {
                                    System.out.println("atm i am here 3");

                                    player.getInventory().addItem(player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].getWithdraw()[0].getId(), player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].getWithdraw()[0].getCount());
                                    player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].setWithdraw(new Item[]{new Item(player.getGrandeExchange().getOpenedOffer().getWithdraw()[0].getId(), player.getGrandeExchange().getOpenedOffer().getWithdraw()[0].getCount() - 1), null});
                                    if (player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].getWithdraw()[0].getCount() == 0) {
                                        player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].setWithdraw(new Item[]{null, null});
                                    }
                                }
                                //GrandExchangeOpenOffer.open(player, true);
                                GrandExchangeMap.updateOffer(player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()]);
                            }
                        }
                    } else {
                        OfferState state = player.getGrandeExchange().getOpenedOffer().getState();
                        if (state == OfferState.COMPLETED || state == OfferState.ABORTED) {
                            if (player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].getWithdraw()[0] == null && player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].getWithdraw()[0] == null) {
                                player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()] = null;
                                player.getBound_offer_uids()[player.getGrandeExchange().getOpenedOfferIndex()] = -1;
                                GrandExchangeMainInterface.open(player, false);
                                return;
                            }
                            if (player.getInventory().hasEnoughRoomFor(995)) {
                                if (player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].getWithdraw()[0].getId() != 7774) {
                                    player.getInventory().addItem(player.getGrandeExchange().getOpenedOffer().getWithdraw()[0].getId(), player.getGrandeExchange().getOpenedOffer().getWithdraw()[0].getCount());
                                    player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].setWithdraw(new Item[]{new Item(player.getGrandeExchange().getOpenedOffer().getWithdraw()[0].getId(), player.getGrandeExchange().getOpenedOffer().getWithdraw()[0].getCount() - 1), null});
                                    if (player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].getWithdraw()[0].getCount() == 0) {
                                        player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].setWithdraw(new Item[]{null, null});
                                    }
                                }
                                GrandExchangeMap.updateOffer(player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()]);
                                if (player.getGrandeExchange().getOpenedOffer().getCompletedAmount() == player.getGrandeExchange().getOpenedOffer().getAmount()) {
                                    if (player.getGrandeExchange().getOpenedOffer().getWithdraw()[0] != null) {
                                        player.getInventory().addItem(player.getGrandeExchange().getOpenedOffer().getWithdraw()[0].getId(), player.getGrandeExchange().getOpenedOffer().getWithdraw()[0].getCount());
                                    }
                                    player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()] = null;
                                    player.getBound_offer_uids()[player.getGrandeExchange().getOpenedOfferIndex()] = -1;
                                    GrandExchangeMainInterface.open(player, false);
                                }
                            }
                        } else {
                            System.out.println("currently here buying offer");

                            if (player.getInventory().hasEnoughRoomFor(995)) {
                                System.out.println("currently here buying offer 2");

                                if (player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].getWithdraw()[0].getId() != 7774) {
                                    System.out.println("currently here buying offer 3");

                                    player.getInventory().addItem(player.getGrandeExchange().getOpenedOffer().getWithdraw()[0].getId(), player.getGrandeExchange().getOpenedOffer().getWithdraw()[0].getCount());
                                    player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].setWithdraw(new Item[]{new Item(player.getGrandeExchange().getOpenedOffer().getWithdraw()[0].getId(), player.getGrandeExchange().getOpenedOffer().getWithdraw()[0].getCount() - 1), null});
                                    if (player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].getWithdraw()[0].getCount() == 0) {
                                        player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()].setWithdraw(new Item[]{null, null});
                                    }
                                }
                               // GrandExchangeOpenOffer.open(player, true);
                                GrandExchangeMap.updateOffer(player.getGrandeExchange().getOffers()[player.getGrandeExchange().getOpenedOfferIndex()]);
                            }
                        }
                    }
                }
                break;
            default:
                Logger.getInstance().info("InterfaceOption 1: interfaceId: " + interfaceId);
                break;
        }

    }

    private void handleClickTwo(Player player, Packet packet) {
        int interfaceShit = packet.getLEInt();
        int interfaceId = interfaceShit >> 16;
        int child = interfaceShit & 0xffff;
        int itemId = packet.getLEShort() & 0xffff;
        int slot = packet.getLEShortA() & 0xffff;
        Shop shop = (Shop) player.getAttributes().get("shop");
        Item item = null;
        if (player.getAttributes().isSet("stopActions")) {
            return;
        }
        if (player.getDetails().getName().equals("woo")) {
            player.sendMessage("handleClickOne ~ inter :" + interfaceId + " - child :" + child);
        }
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Clicked item2 " + itemId + "," + slot + "," + interfaceId + "," + child + "."));
        if (HandleJewlery.operate(player, itemId, slot, interfaceId)) {
            return;
        }
        switch (interfaceId) {
            case 312: //Smithing
                Smithing.smithItem(player, interfaceShit, slot, 5, true);
                break;
            case 387:// Equipment
                if (slot < 0 || slot > 13) {
                    break;
                }
                item = player.getEquipment().get(slot);
                if (item == null || item.getId() != itemId) {
                    break;
                }
                if (DialogueManager.handleMisc(player, item.getId(), true)) {
                    break;
                }
                switch (item.getId()) {
                    case 2550:
                        if (slot == Equipment.EQUIPMENT.RING.ordinal())
                            player.getPacketSender().sendMessage("Your recoil has " + (40 - player.getVariables().getRecoilUses()) + " charges left.");
                        break;
                    default:
                        if (slot == Equipment.EQUIPMENT.CAPE.ordinal())
                            Skillcape.emote(player);
                        break;
                }
                break;
            case 109:// Duel screen - inventory deposit 1
                if (slot < 0 || slot > 27) {
                    break;
                }
                if (player.getDuelSession() != null) {
                    player.getDuelSession().stakeItem(itemId, slot, 5);
                }
                break;
            case 107:// Duel screen - withdraw 1
                if (slot < 0 || slot > 27) {
                    break;
                }
                if (player.getDuelSession() != null) {
                    player.getDuelSession().removeItem(itemId, slot, 5);
                }
                break;
            case 12:// Bank inv - 10
                if (!player.getVariables().isBanking() || !canBank(player))
                    return;
                if (interfaceShit != Constants.BANK_TAB_HEADER) {
                    Bank.withdraw(player, slot, 5, false);
                } else {
                    int collapseTab = slot + 1;
                    player.getAttributes().set("currentTab", collapseTab);
                    Item[] tabItems = Bank.getTabItems(player, collapseTab);
                    if (tabItems == null) {
                        player.getPacketSender().sendMessage("You have found a bug: Null items when collapsing bank. Please report to a staff member.");
                        return;
                    }
                    player.getAttributes().set("dontRefreshTabs", true);
                    Item[] storedItems = new Item[tabItems.length];
                    int index = 0;
                    //player.getPacketSender().sendMessage("Collapse tab: " + collapseTab);
                    for (Item items : tabItems) {
                        if (items == null)
                            continue;
                        storedItems[index] = items;
                        if (Constants.DEBUG_MODE)
                            System.out.println("Withdrawing " + items.getId() + ", " + items.getCount() + " from tab slot: " + (+index));
                        Bank.withdraw(player, 0, items.getCount(), true);
                        index++;
                    }
                    player.getAttributes().set("currentTab", 0);
                    for (Item items : storedItems) {
                        if (items == null)
                            continue;
                        if (Constants.DEBUG_MODE)
                            System.out.println("Depositing " + items.getId());
                        Bank.depositItem(player, items.getId(), items.getCount(), false, false, -1);
                    }
				/*				for(Item items : Bank.getTabItems(player, collapseTab)) {
					storeItems[index] = items;
					if(items == null) {
						System.out.println("null items when collapsing? collapseTab " + collapseTab);
						return;
					}
					player.getBank().set(null, player.getBank().getSlotById(items.getId()), false);
					player.getBank().arrange();
					Bank.withdrawTabEffect(player);
					if ((Bank.getTabSize(player, collapseTab) == 0 && collapseTab != 0)
							|| Bank.getTabSize(player, collapseTab) < 0) {
						//When our bank tab is clearAllBoxes of these items
						if (Constants.DEBUG_MODE)
							System.out.println("Gonna be clearAllBoxes");
						if (Bank.getTabSize(player, collapseTab + 1) >= 1)
							collapseTab = collapseTab + 1;
						else
							collapseTab = collapseTab - 1;
						player.getAttributes().set("currentTab", collapseTab);
					} else {
						if (Constants.DEBUG_MODE)
							System.out.println("Size of current tab " + collapseTab
									+ " = " + Bank.getTabSize(player, collapseTab));
					}
					//int actualOldSlot = collapseTab == 0 ? slot : player.getVariables().getTabStarts()[collapseTab] + slot;

					index++;
				}*/
                    //	player.getPacketSender().sendMessage("Found " + storeItems.length + " items in tab");
                    //player.getAttributes().set("currentTab", 0);
                    //	int currentTab = player.getAttributes().getInt("currentTab");
				/*	for(Item items : storeItems) {
					if(items == null) {
						player.getPacketSender().sendMessage("ITEMS NULL FATAL");
						continue;
					}
						Bank.depositTabEffect(player);
						Item toAdd = new Item(items.getId(), items.getCount());
					player.getBank().addItem(toAdd);
					int fromSlot = player.getBank().getSlotById(toAdd.getId());
					int toSlot = player.getVariables().getTabStarts()[currentTab]
							+ Bank.getTabSize(player, currentTab) - 1;
					player.getPacketSender().sendMessage("Deposit item " + toAdd.getId() + " to slot " + toSlot);
						player.getBank().insert(
								fromSlot,
								toSlot);
						//TODO: we gotta insert the item properly. Also the wrong tab is getting closed or some shit?
				}*/
                    player.getAttributes().remove("dontRefreshTabs");
                    Bank.refreshBank(player, true); //also refresh tabs
                }
                break;
            case 15:// Bank inv deposit 10
                if (player.getVariables().isBanking() && canBank(player)) {
                    Bank.deposit(player, slot, 5, true);
                }
                break;
            case 631: // Duel inter - remove 5
                if (player.getDuelSession() != null) {
                    player.getDuelSession().removeItem(itemId, slot, 5);
                }
                break;
            case 300:// Shopping inter
                if (shop == null) {
                    return;
                }
                ShopManager.getShopManager().purchaseItem(player, slot, 1);
                break;
            case 301:// Shopping - inventory
                if (shop == null) {
                    return;
                }
                ShopManager.getShopManager().sellItem(player, slot, 1);
                break;
            case 154: // Craft normal leather.
                Leather.init_crafting_leather(player, child, 5, true);
                break;
            case 542:// Glass crafting
                switch (child) {
                    case 44:// Fish bowl
                        Glass.craftGlass(player, 1, 4, true);
                        break;
                }
                break;
            case 336:// Trade/stake inventory - trade 5.
                if (player.getTradingSession() != null) {
                    player.getTradingSession().tradeItem(itemId, slot, 5);
                } else if (player.getDuelSession() != null) {
                    player.getDuelSession().stakeItem(itemId, slot, 5);
                }
                break;
            case 335:// Trade inter - remove 5.
                if (player.getTradingSession() != null) {
                    player.getTradingSession().removeItem(itemId, slot, 5);
                }
                break;
            default:
                Debugging.print("Click 2: " + interfaceId + ", " + child + ", " + slot);
                break;
        }
    }

    private void handleClickThree(Player player, Packet packet) {
        int interfaceShit = packet.getLEInt();
        int interfaceId = interfaceShit >> 16;
        int child = interfaceShit & 0xffff;
        int slot = packet.getLEShort() & 0xffff;
        int itemId = packet.getLEShort() & 0xffff;

        Shop shop = (Shop) player.getAttributes().get("shop");
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Clicked item3 " + itemId + "," + slot + "," + interfaceId + "," + child + "."));
        switch (interfaceId) {
            case 312: //Smithing
                Smithing.smithItem(player, interfaceShit, slot, itemId == 6969 ? 4 : 3, true); //6969 means withdraw-x
                break;
            case 109:// Duel screen - inventory deposit 1
                if (slot < 0 || slot > 27) {
                    break;
                }
                if (player.getDuelSession() != null) {
                    player.getDuelSession().stakeItem(itemId, slot, 10);
                }
                break;
            case 107:// Duel screen - withdraw 1
                if (slot < 0 || slot > 27) {
                    break;
                }
                if (player.getDuelSession() != null) {
                    player.getDuelSession().removeItem(itemId, slot, 10);
                }
                break;
            case 12:// Bank inv - 10
                if (!player.getVariables().isBanking() || !canBank(player))
                    return;
                if (interfaceShit != Constants.BANK_TAB_HEADER) {
                    if (player.getVariables().isBanking()) {
                        Bank.withdraw(player, slot, 10, false);
                    }
                } else {

                    int value = 0;
                    for (Item items : Bank.getTabItems(player, slot + 1)) {
                        if (items != null)
                            value = value + (items.getDefinition().getGeneralPrice() * items.getCount());
                    }
                    player.getPacketSender().sendMessage("Tab " + (slot + 1) + " is valued at <col=3E7A02>" + NumberUtils.format(value) + " GP</col>.");
                }
                break;
            case 15:// Bank inv deposit 10
                if (player.getVariables().isBanking() && canBank(player)) {
                    Bank.deposit(player, slot, 10, true);
                }
                break;
            case 631: // Duel inter - remove 5
                if (player.getDuelSession() != null) {
                    player.getDuelSession().removeItem(itemId, slot, 10);
                }
                break;
            case 300:// Shopping inter
                if (shop == null) {
                    return;
                }
                ShopManager.getShopManager().purchaseItem(player, slot, 5);
                break;
            case 301:// Shopping - inventory
                if (shop == null) {
                    return;
                }
                ShopManager.getShopManager().sellItem(player, slot, 5);
                break;
            case 154: // Craft normal leather
                Leather.init_crafting_leather(player, child, player.getInventory().getItemAmount(1741), true);
                break;
            case 542:// Glass crafting
                switch (child) {
                    case 44:// Fish bowl
                        Glass.craftGlass(player, 1, 4, true);
                        break;
                }
                break;
            case 336:// Trade/stake inventory - trade 5.
                if (player.getTradingSession() != null) {
                    player.getTradingSession().tradeItem(itemId, slot, 10);
                } else if (player.getDuelSession() != null) {
                    player.getDuelSession().stakeItem(itemId, slot, 10);
                }
                break;
            case 335:// Trade inter - remove 5.
                if (player.getTradingSession() != null) {
                    player.getTradingSession().removeItem(itemId, slot, 10);
                }
                break;
            default:
                Debugging.print("Click 3: " + interfaceId + ", " + child + ", " + slot);
                break;
        }
    }

    private boolean canBank(Player player) {
        if (player.getAttributes().isSet("dontRefreshTabs")) {
            //TODO: Stop withdrawals/changing tabs/anything to do with banking
            return false;
        }
        return true;
    }

    private void handleClickFour(Player player, Packet packet) {
        int slot = packet.getLEShort() & 0xFFFF;
        int itemId = packet.getShort() & 0xFFFF;
        int interfaceSet = packet.getInt2();
        int interfaceId = interfaceSet >> 16;
        int child = interfaceSet & 0xffff;
        Shop shop = (Shop) player.getAttributes().get("shop");
        Item item = null;
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Clicked item4 " + itemId + "," + slot + "," + interfaceId + "," + child + "."));
        switch (interfaceId) {
            case 12:// Bank inv - 10
                if (player.getVariables().isBanking() && canBank(player)) {
                    int currentTab = player.getAttributes().get("currentTab") != null ? player.getAttributes().getInt("currentTab") : 0;
                    int actualSlot = currentTab == 0 ? slot : player.getVariables().getTabStarts()[currentTab] + slot;
                    Bank.withdraw(player, slot, player.getBank().getAmountInSlot(actualSlot), false);
                }
                break;
            case 15:// Bank inv deposit 10
                if (player.getVariables().isBanking() && canBank(player)) {
                    Bank.deposit(player, slot, player.getInventory().getItemAmount(itemId), true);
                }
                break;
            case 300:// Shopping inter
                if (shop == null) {
                    return;
                }
                ShopManager.getShopManager().purchaseItem(player, slot, 10);
                break;
            case 301:// Shopping - inventory
                if (shop == null) {
                    return;
                }
                ShopManager.getShopManager().sellItem(player, slot, 10);
                break;
            case 154: // Craft normal leather.
                player.getInterfaceSettings().openEnterAmountInterface("Enter amount to craft:", interfaceId, child);
                break;
            case 542:// Glass crafting
                switch (child) {
                    case 44:// Fish bowl
                        player.getInterfaceSettings().openEnterAmountInterface("Enter amount to make:", interfaceId, child);
                        break;
                }
                break;
            case 109:// stake inventory - all
                if (player.getDuelSession() != null) {
                    item = player.getInventory().getSlot(slot);
                    if (item == null) {
                        break;
                    }
                    int amount = player.getInventory().getItemAmount(item.getId());
                    if (item.getDefinition().isStackable()) {
                        amount = player.getInventory().getAmountInSlot(slot);
                    }
                    player.getDuelSession().stakeItem(itemId, slot, amount);
                }
                break;
            case 107:// Duel inter - remove all
                if (player.getDuelSession() != null) {
                    item = player.getDuel().getSlot(slot);
                    if (item == null) {
                        break;
                    }
                    int amount = player.getDuel().getItemAmount(item.getId());
                    if (item.getDefinition().isStackable()) {
                        amount = player.getDuel().getAmountInSlot(slot);
                    }
                    player.getDuelSession().removeItem(itemId, slot, amount);
                }
                break;
            case 336:// Trade inventory - trade all.
                if (player.getTradingSession() != null) {
                    item = player.getInventory().getSlot(slot);
                    if (item == null) {
                        break;
                    }
                    int amount = player.getInventory().getItemAmount(item.getId());
                    if (item.getDefinition().isStackable()) {
                        amount = player.getInventory().getAmountInSlot(slot);
                    }
                    player.getTradingSession().tradeItem(itemId, slot, amount);
                }
                break;
            case 335:// Trade inter - remove all.
                if (player.getTradingSession() != null) {
                    item = player.getTrade().getSlot(slot);
                    if (item == null) {
                        break;
                    }
                    // int amount = item.getCount();
                    int amount = player.getTrade().getItemAmount(item.getId());
                    if (item.getDefinition().isStackable()) {
                        amount = player.getTrade().getAmountInSlot(slot);
                    }
                    player.getTradingSession().removeItem(itemId, slot, amount);
                }
                break;
            default:
                Debugging.print("Click 4: " + interfaceId + ", " + child + ", " + slot);
                break;
        }
    }

    private void handleClickFive(Player player, Packet packet) {
        boolean allButOne = false, buy250 = false, buy100 = false, sell250 = false, sell100 = false;
        int interfaceCrap = packet.getInt(); //interfaceSet
        if (interfaceCrap == 6969) //allbutone
            allButOne = true;
        else if (interfaceCrap == 69691)
            buy100 = true;
        else if (interfaceCrap == 69692)
            buy250 = true;
        else if (interfaceCrap == 69693)
            sell100 = true;
        else if (interfaceCrap == 69694)
            sell250 = true;
        int interfaceId = allButOne ? 12 : (buy250 || buy100) ? 300 : (sell250 || sell100) ? 301 : interfaceCrap >> 16;
        int child = (allButOne ? 786439 : (buy250 || buy100) ? 19660875 : (sell250 || sell100) ? 19726336 : interfaceCrap) & 0xffff; //index on client
        int itemId = packet.getShortA() & 0xffff;
        int slot = packet.getLEShort() & 0xffff;
        Shop shop = (Shop) player.getAttributes().get("shop");
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Clicked item5 " + itemId + "," + slot + "," + interfaceId + "," + child + "."));
        switch (interfaceId) {
            case 107:// Duel screen - remove x
                if (player.getDuelSession() != null) {
                    player.getInterfaceSettings().openEnterAmountInterface(interfaceId, slot, itemId, "Enter amount to remove:");
                }
                break;
            case 109:// Duel inv - offer x
                if (player.getDuelSession() != null) {
                    player.getInterfaceSettings().openEnterAmountInterface(interfaceId, slot, itemId, "Enter amount to offer:");
                }
                break;
            case 301:// Shopping - inventory
                if (shop == null) {
                    break;
                }
                ShopManager.getShopManager().sellItem(player, slot, sell250 ? 250 : sell100 ? 100 : 50);
                break;
            case 300:// Shopping inter
                if (shop == null) {
                    break;
                }
                ShopManager.getShopManager().purchaseItem(player, slot, buy250 ? 250 : buy100 ? 100 : 50);
                break;
            case 335: // Trade inter - remove X.
                if (player.getTrade() != null) {
                    player.getInterfaceSettings().openEnterAmountInterface(interfaceId, slot, player.getTrade().getItemInSlot(slot), "Enter amount:");
                    break;
                }
                break;
            case 336: // Trade inventory - trade X.
                player.getInterfaceSettings().openEnterAmountInterface(interfaceId, slot, player.getInventory().getItemInSlot(slot), "Enter amount:");
                break;
            case 15: // Bank inventory - X.
                player.getInterfaceSettings().openEnterAmountInterface(interfaceId, slot, player.getInventory().getItemInSlot(slot), "Enter amount:");
                break;
            case 12:// Bank X
                if (!player.getVariables().isBanking() || !canBank(player))
                    return;
                int currentTab = player.getAttributes().get("currentTab") != null ? player.getAttributes().getInt("currentTab") : 0;
                int actualSlot = currentTab == 0 ? slot : player.getVariables().getTabStarts()[currentTab] + slot;
                if (allButOne) {
                    int amount = player.getBank().getAmountInSlot(actualSlot);
                    Bank.withdraw(player, slot, amount - 1 >= 1 ? amount - 1 : 1, false);
                } else
                    player.getInterfaceSettings().openEnterAmountInterface(interfaceId, slot, player.getBank().getItemInSlot(slot), "Enter amount:");
                break;
            default:
                Debugging.print("Click 5: " + interfaceId + ", " + child + ", " + slot);
                break;
        }
    }

    private void handleClickSix(Player player, Packet packet) {
        int interfaceId = packet.getShort();
        int child = packet.getShort();
        int slot = packet.getShort();
        Shop shop = (Shop) player.getAttributes().get("shop");
        Item item = null;
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Clicked item6 " + slot + "," + interfaceId + "," + child + "."));
        switch (interfaceId) {
            case 301:// Shopping - inventory
                if (shop == null) {
                    break;
                }
                ShopManager.getShopManager().sellItem(player, slot, 100);
                break;
            case 300:// Shopping inter
                if (shop == null) {
                    break;
                }
                ShopManager.getShopManager().purchaseItem(player, slot, 100);
                break;

            case 15:// Bank screen - inventory
                if (player.getVariables().isBanking() && canBank(player)) {
                    item = player.getInventory().getSlot(slot);
                    if (item == null) {
                        break;
                    }
                    int amount = player.getInventory().getItemAmount(item.getId());
                    if (item.getDefinition().isStackable()) {
                        amount = player.getInventory().getAmountInSlot(slot);
                    }
                    Bank.deposit(player, slot, amount, true);
                }
                break;
            case 12:// Bank screen
                if (player.getVariables().isBanking() && canBank(player)) {
                    //Withdraw All-but-1
                    item = player.getBank().getSlot(slot);
                    if (item == null) {
                        break;
                    }
                    int amount = player.getBank().getItemAmount(item.getId());
                    if (item.getDefinition().isStackable()) {
                        amount = player.getBank().getAmountInSlot(slot);
                    }
                    Bank.withdraw(player, slot, amount > 1 ? amount - 1 : 1, false);
                }
                break;
            default:
                Debugging.print("Click 6: " + interfaceId + ", " + child + ", " + slot);
                break;
        }
    }

    private void handleClickSeven(Player player, Packet packet) {
        int interfaceId = packet.getShort();
        int child = packet.getShort();
        int slot = packet.getShort();
        Shop shop = (Shop) player.getAttributes().get("shop");
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Clicked item7 " + slot + "," + interfaceId + "," + child + "."));
        switch (interfaceId) {
            case 12:// Bank screen
                if (player.getVariables().isBanking() && canBank(player)) {
                    switch (child) {
                        case 73:// withdraw all but one
                            int amount = player.getBank().getAmountInSlot(slot) - 1;
                            Bank.withdraw(player, slot, amount > 0 ? amount : 1, false);
                            break;
                    }
                }
                break;
            case 301:// Shopping - inventory
                if (shop == null) {
                    break;
                }
                ShopManager.getShopManager().sellItem(player, slot, 250);
                break;
            case 300:// Shopping inter
                if (shop == null) {
                    break;
                }
                ShopManager.getShopManager().purchaseItem(player, slot, 250);
                break;
            default:
                Debugging.print("Click 7: " + interfaceId + ", " + child + ", " + slot);
                break;
        }
    }

    @SuppressWarnings("unused")
    private void handleClickEight(Player player, Packet packet) {
        int interfaceId = packet.getShort();
        int child = packet.getShort();
        int slot = packet.getShort();
        switch (interfaceId) {

        }
    }

    @SuppressWarnings("unused")
    private void handleClickNine(Player player, Packet packet) {
        int interfaceId = packet.getShort();
        int child = packet.getShort();
        int slot = packet.getShort();
        switch (interfaceId) {

        }
    }

    @Override
    public boolean canExecute(Player player, Packet packet) {
        // TODO Auto-generated method stub
        return true;
    }
}