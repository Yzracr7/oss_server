package com.hyperion.game.packet;

import com.hyperion.Logger;
import com.hyperion.game.Constants;
import com.hyperion.game.content.*;
import com.hyperion.game.content.clues.ClueScrollManagement;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.content.dialogue.impl.jewelery.HandleJewlery;
import com.hyperion.game.content.minigames.barrows.Barrows;
import com.hyperion.game.content.pets.BossPetDropHandler;
import com.hyperion.game.content.skills.crafting.Crafting;
import com.hyperion.game.content.skills.firemaking.Firemaking;
import com.hyperion.game.content.skills.fletching.Fletching;
import com.hyperion.game.content.skills.herblore.Herblore;
import com.hyperion.game.content.skills.magic.TeleportTablets;
import com.hyperion.game.content.skills.magic.onitems.MagicOnItemSpells;
import com.hyperion.game.content.skills.prayer.BoneBurying;
import com.hyperion.game.content.skills.slayer.SlayerTasks;
import com.hyperion.game.item.Item;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.net.Packet;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Loaders;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.entity.player.container.impl.Bank;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
import com.hyperion.game.world.entity.player.container.impl.Inventory;
import com.hyperion.game.world.entity.player.packets.ItemPackets;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.game.world.grounditems.GroundItem;
import com.hyperion.game.world.grounditems.GroundItemManager;
import com.hyperion.game.world.pathfinders.DefaultPathFinder;
import com.hyperion.game.world.pathfinders.ObjectPathFinder;
import com.hyperion.game.world.pathfinders.PathState;
import com.hyperion.utility.NumberUtils;
import com.hyperion.utility.logger.Log;

public class ItemInteract implements PacketHandler {

    private static final int EQUIP = 215;
    private static final int ITEM_ON_ITEM = 166;
    private static final int INVEN_CLICK = 101;
    private static final int ITEM_ON_OBJECT = 103;
    private static final int OPERATE = 212;
    private static final int DROP = 247;
    private static final int PICKUP = 216;
    private static final int SWAP_SLOT = 121;
    private static final int RIGHT_CLICK_OPTION2 = 153;
    private static final int RIGHT_CLICK_OPTION3 = 161;
    private static final int EXAMINE_ITEM = 28;
    private static final int EXAMINE_ITEM2 = 92;
    private static final int MAGIC_ON_ITEM = 163;

    @Override
    public void handle(Player player, Packet packet) {
        if (Constants.DEBUG_MODE) {
            System.out.println("item opcode: " + packet.getOpcode());
        }
        switch (packet.getOpcode()) {
            case RIGHT_CLICK_OPTION2:
                handleRightClickTwo(player, packet);
                break;
            case RIGHT_CLICK_OPTION3:
                handleRightClickThree(player, packet);
                break;
            case ITEM_ON_ITEM:
                handleItemOnItem(player, packet);
                break;
            case MAGIC_ON_ITEM:
                handleMagicOnItems(player, packet);
                break;
            case ITEM_ON_OBJECT:
                handleItemOnObject(player, packet);
                break;
            case OPERATE:
                handleOperateItem(player, packet);
                break;
            case PICKUP:
                MainCombat.endCombat(player, 1);
                handlePickupItem(player, packet);
                break;
            case INVEN_CLICK:
                handleInvenClickItem(player, packet);
                break;
            case DROP:
                handleDropItem(player, packet);
                break;
            case EQUIP:
                handleEquipItem(player, packet);
                break;
            case SWAP_SLOT:
                handleSwapSlot(player, packet);
                break;
            case EXAMINE_ITEM: // ?
                handleExamineItem(player, packet);
                break;
            case EXAMINE_ITEM2:
                handleExamineItem2(player, packet);
                break;
        }
    }

    private void handleRightClickTwo(Player player, Packet packet) {
        int childId = packet.getLEShort();
        int interfaceId = packet.getLEShort();
        int slot = packet.getLEShort();
        int item = !Constants.USE_DEV_CACHE ? Loaders.renderIdsViseVersa[packet.getLEShort()] : packet.getLEShort();
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Clicked 2nd item option " + item + "," + slot + "," + interfaceId + "," + childId + "."));
        if (slot < 0 || slot > 28 || player.getCombatState().isDead()) {
            return;
        }
        if (player.getInventory().getItemInSlot(slot) == item) {
            player.getInterfaceSettings().closeInterfaces(false);

            switch (player.getInventory().getItemInSlot(slot)) {
                case 4155:// Enchanted gem
                    SlayerTasks.current_task(player);
                    break;
                case 5509: // Small pouch.
                    player.getPacketSender().sendMessage("There is " + player.getVariables().getSmallPouch() + " Pure essence in your small pouch. (holds 3).");
                    break;

                case 5510: // Medium pouch.
                    player.getPacketSender().sendMessage("There is " + player.getVariables().getMediumPouch() + " Pure essence in your medium pouch. (holds 6).");
                    break;

                case 5512: // Large pouch.
                    player.getPacketSender().sendMessage("There is " + player.getVariables().getLargePouch() + " Pure essence in your large pouch. (holds 9).");
                    break;

                case 5514: // Giant pouch.
                    player.getPacketSender().sendMessage("There is " + player.getVariables().getGiantPouch() + " Pure essence in your giant pouch. (holds 12).");
                    break;
            }
        }
    }

    private void handleRightClickThree(Player player, Packet packet) {
        int interfaceHash = packet.getLEInt();
        int itemId = !Constants.USE_DEV_CACHE ? Loaders.renderIdsViseVersa[packet.getLEShortA()] : packet.getLEShortA();
        int slot = packet.getLEShortA();
        int interfaceId = interfaceHash >> 16;
        //World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Clicked 3rd item option " + itemId + "," + slot + "," + interfaceId + "."));
        if (slot < 0 || slot > 28 || player.getCombatState().isDead()) {
            return;
        }
        if (HandleJewlery.operate(player, itemId, slot, interfaceId)) {
            return;
        }
        if (player.getInventory().getItemInSlot(slot) == itemId) {
            player.getInterfaceSettings().closeInterfaces(false);
            switch (player.getInventory().getItemInSlot(slot)) {
                case 12006: //Abyssal tentacle
                    if (player.getInventory().freeSlots() >= 1) {
                        if (player.getInventory().deleteItem(12006, 1)) {
                            player.getInventory().addItem(new Item(4151, 1));
                            player.getInventory().addItem(new Item(12004, 1));
                            player.sendMessage("Your have reverted your Abyssal tentacle back to its original form.");
                            player.getInventory().refresh();
                        }
                    } else {
                        player.sendMessage("You need a free inventory slot to do that.");
                    }
                    break;
            }
        }
    }

    private void handleItemOnItem(Player player, Packet packet) {
        int itemUsedSlot = packet.getLEShort() & 0xffff;
        int usedWithSlot = packet.getShort() & 0xffff;
        int usedWith = !Constants.USE_DEV_CACHE ? Loaders.renderIdsViseVersa[packet.getLEShortA() & 0xffff] : packet.getLEShortA() & 0xffff;
        int hash1 = packet.getInt2();
        int hash2 = packet.getLEInt();
        int itemUsed = !Constants.USE_DEV_CACHE ? Loaders.renderIdsViseVersa[packet.getShortA() & 0xffff] : packet.getShortA() & 0xffff;
        if (hash1 != hash2)
            return;
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Clicked item on item " + itemUsed + " on " + usedWith + " " + itemUsedSlot + "," + usedWithSlot + ", inter=" + (hash1 >> 16) + "."));
        if (itemUsedSlot > 28 || itemUsedSlot < 0 || usedWithSlot > 28 || usedWithSlot < 0 || player.getCombatState().isDead()) {
            return;
        }
        Item usedItem = player.getInventory().getSlot(itemUsedSlot);
        Item usedWithItem = player.getInventory().getSlot(usedWithSlot);
        if (usedWithItem == null || usedItem == null || usedItem.getId() != itemUsed || usedWithItem.getId() != usedWith) {
            return;
        }
        player.getInterfaceSettings().closeInterfaces(false);
        if (Crafting.wantsToCraft(player, usedWith, itemUsed)) {
            return;
        }
        if (Fletching.isFletching(player, usedWith, itemUsed)) {
            return;
        }
        WeaponPoison.execute(player, itemUsed, usedWith);
        if (usedWith == 590 || itemUsed == 590) {
            if (Firemaking.getLog(usedWith) != null) {
                Firemaking.light(player, Firemaking.getLog(usedWith), itemUsedSlot);
                return;
            } else if (Firemaking.getLog(itemUsed) != null) {
                Firemaking.light(player, Firemaking.getLog(itemUsed), itemUsedSlot);
                return;
            }
        }
        if (Herblore.mixing_potions(player, itemUsed, usedWith)) {
            return;
        } else if (Herblore.mixDoses(player, usedItem.getId(), usedWithItem.getId(), itemUsedSlot, usedWithSlot)) {
            return;
        }
        ItemPackets.handleItemOnItem(player, usedWith, itemUsed);
    }

    @SuppressWarnings("unused")
    private void handleMagicOnItems(Player player, Packet packet) {
        int itemId = !Constants.USE_DEV_CACHE ? Loaders.renderIdsViseVersa[packet.getLEShortA()] : packet.getLEShortA();
        int itemSlot = packet.getLEShort();
        int junk = packet.getLEShortA();
        int hash1 = packet.getInt2();
        int hash2 = packet.getLEInt();
        int inventoryInterface = hash1 >> 16;
        int magicInterface = hash2 >> 16;
        int childId = hash2 & 0xff;
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Magic on item, inter: " + magicInterface + ", child: " + childId + ", on item: " + itemId + " in slot " + itemSlot + "."));
        if (magicInterface == 192) {
            int book = player.getSettings().getMagicType();
            if (book == 1) {
                if (itemSlot < 0 || itemSlot > 28 || !canExecute(player, packet)) {
                    return;
                }
                if (inventoryInterface != 149) {
                    return;
                }
                Item item = player.getInventory().getSlot(itemSlot);
                if (item == null) {
                    return;
                }
                if (item.getId() != itemId) {
                    return;
                }
                MagicOnItemSpells.handleSpells(player, item, childId);
            }
        }
    }

    @SuppressWarnings("unused")
    private void handleItemOnObject(Player player, Packet packet) {
        int objectX = -1;
        int slot = -1;
        int objectY = -1;
        int interfaceHash = -1;
        int itemId = -1;
        int objectId = -1;
        // int interfaceId = interfaceHash >> 16;
        // int child = interfaceHash & 0xffff;
        try {
            objectX = packet.getShortA() & 0xffff;
            slot = packet.getShort() & 0xffff;
            objectY = packet.getLEShortA() & 0xffff;
            interfaceHash = packet.getInt2();
            itemId = packet.getShortA() & 0xffff;
            objectId = packet.getLEShort();
            // int interfaceId = interfaceHash >> 16;
            // int child = interfaceHash & 0xffff;
        } catch (Exception e) {
            System.out.println("ItemOnObject exception: " + e);
        }
        MainCombat.endCombat(player, 1);
        Location loc = Location.create(objectX, objectY, player.getLocation().getZ());
        if (slot > 27 || slot < 0 || !canExecute(player, packet)) {
            return;
        }
        final int regionId = loc.getRegionId();
        if (!player.getMapRegionsIds().contains(regionId)) {
            return;
        }
        GameObject mapObject = World.getObjectWithId(loc, objectId);
        if (mapObject == null || mapObject.getId() != objectId) {
            return;
        }
        final GameObject object = mapObject;
        if (!player.getLocation().withinActualDistance(object.getLocation(), 16)) {
            return;
        }
        Item item = player.getInventory().getSlot(slot);
        if (item == null || item.getId() != itemId) {
            return;
        }
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Item on object: " + objectId + ", at loc:" + loc.toString() + ", with item: " + itemId + "."));
        PathState state = ObjectPathFinder.executePath(player, object);
        if (!state.isRouteFound()) {
            player.face(loc);
            player.getPacketSender().sendMessage("I can't reach that!");
            player.getWalkingQueue().reset();
            player.getAttributes().remove("itemOnObject");
            player.getAttributes().remove("itemOnObjectItem");
            return;
        }
        player.getInterfaceSettings().closeInterfaces(false);
        player.getAttributes().set("itemOnObject", object);
        player.getAttributes().set("itemOnObjectItem", item);
        /*
         * if(object.getId() == 7) { if(player.getAttributes().isSet("cannon"))
         * { Cannon cannon = player.getAttributes().get("cannon");
         * cannon.addPart(item); } } else if (object.getId() == 6) {
         * if(player.getAttributes().isSet("cannon")) { Cannon cannon =
         * player.getAttributes().get("cannon"); cannon.handleCannon(item); } }
         */

    }

    private void handlePickupItem(final Player player, Packet packet) {
        int id = !Constants.USE_DEV_CACHE ? Loaders.renderIdsViseVersa[packet.getShort() & 0xffff] : packet.getShort() & 0xffff;
        int y = packet.getShortA() & 0xffff;
        int x = packet.getLEShortA() & 0xffff;
        if (!canExecute(player, packet)) {
            return;
        }
        Location playerLocation = player.getLocation();
        Location itemLocation = Location.create(x, y, playerLocation.getZ());
        final int regionId = itemLocation.getRegionId();
        if (!player.getMapRegionsIds().contains(regionId)) {
            return;
        }
        if (id == 15492)
            id = 13263;
        final GroundItem item = World.getWorld().getRegion(regionId, false).getGroundItem(id, itemLocation, player);
        if (item == null) {
            return;
        }
        if (item.getId() != id && !(id == 13263 && item.getId() == 15492)) {
            return;
        }
        if (player.getDetails().isIronman()) {
            if (!item.getOwner().equalsIgnoreCase(player.getName())) {
                player.getPacketSender().sendChatboxDialogue(true, "As an ironman, you cannot do this.");
                return;
            }
        }
        PathState pathState = player.execute_path(new DefaultPathFinder(), x, y);
        if (!pathState.isRouteFound()) {
            player.getWalkingQueue().reset();
            player.getPacketSender().sendMessage("I can't reach that!");
            player.resetActionAttributes();
            return;
        }
        boolean frozen = player.getCombatState().isFrozen();
        if (frozen) {
            player.getWalkingQueue().reset();
        }
        if (player.getLocation().equals(itemLocation)) {
            ItemPackets.handleItemPickup(player, item);
            return;
        }
        boolean north = playerLocation.equals(itemLocation.getNorth());
        boolean east = playerLocation.equals(itemLocation.getEast());
        boolean south = playerLocation.equals(itemLocation.getSouth());
        boolean west = playerLocation.equals(itemLocation.getWest());
        boolean around = (north || east || south || west);
        if (around && frozen) {
            player.face(itemLocation);
            player.animate(832);
            World.getWorld().submit(new Tickable(1) {
                @Override
                public void execute() {
                    this.stop();
                    ItemPackets.handleItemPickup(player, item);
                }
            });
            return;
        }
        if (frozen) {
            player.resetActionAttributes();
            return;
        }
        player.getAttributes().set("pickupId", id);
        player.getAttributes().set("pickupitem", item);
    }

    private void handleInvenClickItem(final Player player, Packet packet) {
        int interfaceId = packet.getInt1() >> 16;
        int slot = packet.getLEShort();
        int itemId = !Constants.USE_DEV_CACHE ? Loaders.renderIdsViseVersa[packet.getLEShort()] : packet.getLEShort();
        System.out.println("Item Inventory click " + interfaceId + " " + itemId);
        if (slot > 28 || slot < 0 || !canExecute(player, packet)) {
            return;
        }
        Item item = player.getInventory().getSlot(slot);
        if (item == null) {
            return;
        }
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Clicked item: " + itemId + ", on inter: " + interfaceId + ", in slot: " + slot + "."));
        player.getPacketSender().softCloseInterfaces();
        if (item.getId() != itemId) {
            return;
        }
        if (ClueScrollManagement.readScroll(player, itemId)) {
            return;
        }
        if (Foods.eat(player, item, slot)) {
            return;
        }
        if (Potions.canDrink(player, item, slot)) {
            return;
        }
        if (BoneBurying.isBuryingBone(player, item.getId(), slot)) {
            return;
        }
        if (TeleportTablets.executeTeleportTablet(player, item.getId())) {
            return;
        }
        if (Herblore.is_cleaning(player, item.getId())) {
            return;
        }

        if (itemId == 5509 || itemId == 5510 || itemId == 5512 || itemId == 5514) {
            int pureEss = 7936;
            int maxAmount = itemId == 5509 ? 3 : itemId == 5510 ? 6 : itemId == 5512 ? 9 : 12;
            int amountToAdd = 0;
            int currentAmount = itemId == 5509 ? player.getVariables().getSmallPouch() : itemId == 5510 ? player.getVariables().getMediumPouch() : itemId == 5512 ? player.getVariables().getLargePouch() : player.getVariables().getGiantPouch();
            String pouchName = itemId == 5509 ? "small" : itemId == 5510 ? "medium" : itemId == 5512 ? "large" : "giant";
            maxAmount = maxAmount - currentAmount;
            if (maxAmount == 0) {
                player.getPacketSender().sendMessage("Your " + pouchName + " pouch is full.");
            } else if (player.getInventory().containsItem(pureEss, 1)) {
                for (int i = 0; i < maxAmount; i++) {
                    if (!player.getInventory().deleteItem(7936)) {
                        break;
                    } else {
                        amountToAdd++;
                    }
                }
                switch (itemId) {
                    case 5509:
                        player.getVariables().setSmallPouch(player.getVariables().getSmallPouch() + amountToAdd);
                        break;
                    case 5510:
                        player.getVariables().setMediumPouch(player.getVariables().getMediumPouch() + amountToAdd);
                        break;
                    case 5512:
                        player.getVariables().setLargePouch(player.getVariables().getLargePouch() + amountToAdd);
                        break;
                    case 5514:
                        player.getVariables().setGiantPouch(player.getVariables().getGiantPouch() + amountToAdd);
                        break;
                }
                player.getPacketSender().sendMessage(amountToAdd > 0 ? ("You fill your " + pouchName + " pouch with " + amountToAdd + " pure essence.") : ("Your " + pouchName + " is already full."));
            } else {
                player.getPacketSender().sendMessage("You do not have any essence with which you may fill this pouch.");
            }
        }
        if (itemId == 7509) {
            if (player.getInventory().deleteItem(item, slot)) {
                player.getInventory().refresh();
                player.animate(829);
                player.playForcedChat("Ow! Ow! That's hot!");
                if (player.getSkills().getLevel(Skills.HITPOINTS) > 1)
                    player.inflictDamage(new Hit(1, -1), false);
                player.getPacketSender().sendMessage("You burn your mouth and hands on the hot rock cake and then drop it.");
                return;
            }
        }
        if (itemId == 6199) {
            player.getPacketSender().sendMessage("You open the mystery box...");
            World.getWorld().submit(new Tickable(1) {
                @Override
                public void execute() {
                    player.getInventory().deleteItem(6199, 1);
                    int random = NumberUtils.random(100);
                    int[] set = random == 1 && NumberUtils.random(100) >= 75 ? Constants.SUPER_RARE_ITEMS : random < 5 ? Constants.RARE_ITEMS : random < 30 ? Constants.BARROWS_ITEMS : Constants.REGULAR_ITEMS;
                    int reward = set[NumberUtils.random(set.length - 1)];
                    int amount = 1;
                    switch (reward) {
                        case 868:
                            amount = NumberUtils.random(4000, 6000);
                            break;
                        case 11212:
                            amount = NumberUtils.random(5, 310);
                            break;
                        case 2362:
                        case 2364:
                            amount = NumberUtils.random(30, 3000);
                            break;
                        default:
                            amount = 1;
                            break;
                    }
                    Item rewardItem = new Item(reward, amount);
                    player.getInventory().addItem(rewardItem.getId(), rewardItem.getCount());
                    player.getPacketSender().sendMessage("You find ... <col=0081C2>" + amount + " x " + rewardItem.getDefinition().getName() + "!");
                    this.stop();
                }
            });

            return;
        }
        if (ClueScrollManagement.openCasket(player, item.getId())) {
            return;
        }
        switch (item.getId()) {
            case 4155:// Enchanted gem
                SlayerTasks.current_task(player);
                break;
            case 2714:// Casket
                CasketRewards.open_casket(player);
                break;
            case 2717:// Money casket
                if (player.getInventory().deleteItem(item)) {
                    int money_casket_amount = player.getDetails().isDonator() ? 100000 : 50000;
                    player.getInventory().addItem(new Item(995, money_casket_amount));
                    player.getInventory().refresh();
                }
                break;
            case 2720:// Rare money casket
                if (player.getInventory().deleteItem(item)) {
                    int rare_money_casket_amount = player.getDetails().isDonator() ? 200000 : 100000;
                    player.getInventory().addItem(new Item(995, rare_money_casket_amount));
                    player.getInventory().refresh();
                }
                break;
            case 405:// God book casket
                GodBooksCasket.openCasket(player, item.getId());
                break;
            case 10952:
                player.animate(Animation.create(6083));
                break;
            case 952:// Spade
                if (Barrows.canDig(player)) {
                } else if (ClueScrollManagement.confirmPerformance(player, -1)) {
                    return;
                }
                break;
            case 9668:// Initiate set
                if (player.getInventory().deleteItem(item)) {
                    player.getInventory().addItem(new Item(5574, 1));
                    player.getInventory().addItem(new Item(5575, 1));
                    player.getInventory().addItem(new Item(5576, 1));
                    player.getInventory().refresh();
                }
                break;
            case 9666:// Proselyte set
                if (player.getInventory().deleteItem(item)) {
                    player.getInventory().addItem(new Item(9672, 1));
                    player.getInventory().addItem(new Item(9674, 1));
                    player.getInventory().addItem(new Item(9676, 1));
                    player.getInventory().refresh();
                }
                break;
            case 9670:// Proselyte set - female
                if (player.getInventory().deleteItem(item)) {
                    player.getInventory().addItem(new Item(9672, 1));
                    player.getInventory().addItem(new Item(9674, 1));
                    player.getInventory().addItem(new Item(9678, 1));
                    player.getInventory().refresh();
                }
                break;
        }
    }

    @SuppressWarnings("unused")
    private void handleDropItem(Player player, Packet packet) {
        int interfaceId = packet.getInt2() >> 16;
        int itemId = !Constants.USE_DEV_CACHE ? Loaders.renderIdsViseVersa[packet.getShortA()] : packet.getShortA();
        int slot = packet.getLEShort();
        if (slot > 28 || slot < 0 || !canExecute(player, packet)) {
            return;
        }
        Item item = player.getInventory().getSlot(slot);
        if (item == null) {
            return;
        }

        if (item.getId() != itemId && !(item.getId() == 13263 && itemId == 15492)) {
            return;
        }
        if (BossPetDropHandler.getPetForItem(itemId).isPresent()) {
            BossPetDropHandler.spawnPet(player, itemId, true);
            BossPetDropHandler pet = BossPetDropHandler.getPetForItem(itemId).get();
            player.animate(Animation.create(827));
            return;
        }
        player.getPacketSender().softCloseInterfaces();
        player.resetActionAttributes();
        if (itemId >= 2412 && itemId <= 2414) {
            String type = itemId == 2412 ? "Saradomin" : itemId == 2413 ? "Guthix" : "Zamorak";
            if (player.getInventory().deleteItem(item, slot)) {
                player.getInventory().refresh();
                player.getPacketSender().sendMessage("" + type + " reclaims the cape as it touches the ground.");
                return;
            }
        }
        if (itemId == 703 || itemId == 702) {
            boolean identified = itemId == 703;
            if (player.getInventory().deleteItem(item, slot)) {
                player.getInventory().refresh();
                player.playForcedChat(identified ? "Ow! The nitroglycerin exploded!" : "Ow!");
                if (player.getSkills().getLevel(Skills.HITPOINTS) > (identified ? 35 : 25))
                    player.inflictDamage(new Hit(identified ? 35 : 25, -1), false);
                return;
            }
        }
        switch (player.getInventory().getItemInSlot(slot)) {
            case 12006: //Abyssal tentacle
                if (player.getInventory().freeSlots() >= 1) {
                    if (player.getInventory().deleteItem(12006, 1)) {
                        player.getInventory().addItem(new Item(4151, 1));
                        player.getInventory().addItem(new Item(12004, 1));
                        player.sendMessage("Your have reverted your Abyssal tentacle back to its original form.");
                        player.getInventory().refresh();
                    }
                } else {
                    player.sendMessage("You need a free inventory slot to do that.");
                }
                break;
        }
        int itemToDrop = item.getId();
        if (player.getInventory().deleteItem(item, slot)) {
            player.getInventory().refresh();
            GroundItem groundItem = new GroundItem(new Item(itemToDrop, item.getCount()), player);
            player.getInterfaceSettings().closeInterfaces(false);
            GroundItemManager.create(groundItem, player);
        } else {
            player.getInventory().set(null, slot, true);
        }

        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Dropped " + item.getCount() + " x " + item.getDefinition().getName() + " from slot: " + slot + " at " + player.getLocation().toString()));

    }

    private void handleEquipItem(Player player, Packet packet) {
        int slot = packet.getLEShort();
        int interfaceId = packet.getLEInt() >> 16;
        int itemId = !Constants.USE_DEV_CACHE ? Loaders.renderIdsViseVersa[packet.getLEShort()] : packet.getLEShort();
        System.out.println("Item Inventory click " + interfaceId + " " + itemId);

        if (slot > 28 || slot < 0 || !canExecute(player, packet)) {
            return;
        }

        Item item = player.getInventory().getSlot(slot);
        if (item == null) {
            return;
        }
        if (item.getId() != itemId && !(item.getId() == 13263 && itemId == 15492)) {
            return;
        }
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Equipped item: " + itemId + ", into slot: " + slot + ", on inter: " + interfaceId));
        player.getInterfaceSettings().closeInterfaces(true);
        switch (interfaceId) {
            case 149:// inv
                if (itemId == 15073) {
                    // ClanOrb.clickOrb(player, 1);
                    break;
                }
                if (itemId == 5509 || itemId == 5510 || itemId == 5512 || itemId == 5514) {
                    int amountToWithdraw = itemId == 5509 ? player.getVariables().getSmallPouch() : itemId == 5510 ? player.getVariables().getMediumPouch() : itemId == 5512 ? player.getVariables().getLargePouch() : player.getVariables().getGiantPouch();

                    String pouchName = itemId == 5509 ? "small" : itemId == 5510 ? "medium" : itemId == 5512 ? "large" : "giant";

                    if (amountToWithdraw <= 0) {
                        player.getPacketSender().sendMessage("Your " + pouchName + " pouch is clearAllBoxes.");
                        break;
                    }
                    if (player.getInventory().freeSlots() == 0) {
                        player.getPacketSender().sendMessage("You don't have any inventory space to withdraw essence.");
                        break;
                    }
                    if (player.getInventory().freeSlots() >= amountToWithdraw) {
                        player.getPacketSender().sendMessage("You clearAllBoxes your " + pouchName + " pouch.");
                    } else {
                        amountToWithdraw = player.getInventory().freeSlots();
                        player.getPacketSender().sendMessage("You don't have enough inventory space to withdraw all of your essence.");
                        player.getPacketSender().sendMessage("You withdraw " + amountToWithdraw + " essence from your " + pouchName + " pouch.");
                    }

                    switch (itemId) {
                        case 5509:
                            player.getVariables().setSmallPouch(player.getVariables().getSmallPouch() - amountToWithdraw);
                            break;
                        case 5510:
                            player.getVariables().setMediumPouch(player.getVariables().getMediumPouch() - amountToWithdraw);
                            break;
                        case 5512:
                            player.getVariables().setLargePouch(player.getVariables().getLargePouch() - amountToWithdraw);
                            break;
                        case 5514:
                            player.getVariables().setGiantPouch(player.getVariables().getGiantPouch() - amountToWithdraw);
                            break;
                    }

                    player.getInventory().addItem(new Item(7936, amountToWithdraw));
                    player.getInventory().refresh();
                    break;
                }
                Equipment.equipItem(player, itemId, slot, false);
                break;
        }
    }

    private void handleSwapSlot(Player player, Packet packet) {
        int newSlot = packet.getShortA();
        int junk = packet.getByteA();
        int interfaceShit = packet.getInt1();
        int interfaceId = interfaceShit >> 16;
        int oldSlot = packet.getLEShort();
        //if (Constants.DEBUG_MODE) {
        //	System.out.println("Swap slot: newSlot " + newSlot + ", byte: " + junk + ", id: " + interfaceId + ", oldslot: " + oldSlot);
        //}
        if (!canExecute(player, packet)) {
            return;
        }
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "swapped from " + oldSlot + " to " + newSlot + " inter " + interfaceId));
        Item itemToSwap = null;
        switch (interfaceId) {
            case 12:// Bank swapping
                if (junk == 57) {
                    //TODO: PROBLEM DRAGGING TO ZERO
                    //TODO: ALSO DRAGGING TO A NEW TAB THAT SKIPS 1 TAB IS FUCKED (Eg. drag from 0 to 3), just the order is wrong
                    //TODO: ALSO withdrawing with nothing left in the tab might fuck shit up. check for collpase.


//Drag to tab
                    int oldTab = player.getAttributes().get("currentTab") != null ? player.getAttributes().getInt("currentTab") : 0;
                    int newTab = newSlot - 6969;
                    if (oldTab == newTab) {
                        player.getPacketSender().sendMessage("You can't drag an item into your current tab!");
                        return;
                    }

                    int bankSize = player.getBank().getSize() - player.getBank().freeSlots();

                    if (Bank.getTabSize(player, oldTab) <= 1) {
                        player.getPacketSender().sendMessage("You can't move to a new tab when you only have one item in the current tab!");
                        return;
                    }


                    //TO COLLAPSE:
                    //Collapse should just be getVariables().getTabStarts()[currentTab] = getVariables().getTabStarts()[currentTab]  + 1; and so on until getVariables().getTabStarts()[currentTab + 1] is -1 or bank size
                    if (Constants.DEBUG_MODE)
                        System.out.println("BEFORE DEBUG: Tab: " + newTab + " speak slot " + player.getVariables().getTabStarts()[newTab] + " next speak slot " + player.getVariables().getTabStarts()[newTab + 1]);
                    if (player.getVariables().getTabStarts()[newTab] == bankSize || player.getVariables().getTabStarts()[newTab] == -1 || (player.getVariables().getTabStarts()[newTab] == 0 && newTab != 0)) {//New tab doesn't yet exist
                        /*(Bank.getTabSize(player, newTab - 1) == 0 || */
                        if (((player.getVariables().getTabStarts()[newTab - 1] == 0 || player.getVariables().getTabStarts()[newTab - 1] == -1) && (newTab - 1) != 0) || player.getVariables().getTabStarts()[newTab - 1] == bankSize
                                ) {
                            player.getPacketSender().sendMessage("This tab is too far to create!");
                            if (player.getVariables().getTabStarts()[newTab] > bankSize) {
                                System.out.println("Tab " + newTab + " was at a higher speak point than bank size: " + player.getVariables().getTabStarts()[newTab]);
                                player.getVariables().getTabStarts()[newTab] = 0;
                            }
                            return;
                        } else {
                            //player.getPacketSender().sendMessage("Apparently the tab right before the tab to create has a size of " + Bank.getTabSize(player, newTab - 1));
                        }

                        if (newTab > oldTab) {
                            //toSlot = toSlot - 1;
                            if (Constants.DEBUG_MODE)
                                System.out.println("NEWTABMETHOD:New tab > oldTab. Decrease all between");
                            //TODO: something like player.getVariables().getTabStarts()[oldTab]--;
                            for (int i = oldTab + 1; i <= newTab; i++) { //example from tab 3 to 7. All tabs in between must decrease by 1, even tab 7 since it isn't new
                                if (player.getVariables().getTabStarts()[i] != -1) {
                                    if (Constants.DEBUG_MODE)
                                        System.out.println("Decreasing getVariables().getTabStarts() " + i);
                                    player.getVariables().getTabStarts()[i]--;
                                }
                            }
                        }

                        player.getVariables().getTabStarts()[newTab] = bankSize - 1; // size - 1 because we just removed an item from the last tab
                        player.getVariables().getTabStarts()[newTab + 1] = bankSize;
                        int toSlot = player.getVariables().getTabStarts()[newTab]; // + 1 ?
                        int overallOldSlot = oldTab == 0 ? oldSlot : player.getVariables().getTabStarts()[oldTab] + oldSlot;
                        if (Constants.DEBUG_MODE)
                            System.out.println("New tab doesn't exist. Creating tab at bankarray slot " + toSlot + " and moving item in arrayslot " + overallOldSlot + " there.");
                        if (overallOldSlot != newSlot)
                            player.getBank().insert(overallOldSlot, toSlot);
                        Bank.refreshBank(player, true);
                        //TODO: Lets say we drag from tab 3 to newly created tab 8. for tabs 4 to 8(7?) we must decrease the speak index
                    } else { //Tab already exists
                        if (/*New bug fix April 13 for depositing into 8 when 7 doesnt exist*/
                                (player.getVariables().getTabStarts()[newTab] > bankSize)) {
                            player.getPacketSender().sendMessage("This tab is too far to create!");
                            if (player.getVariables().getTabStarts()[newTab] > bankSize) {
                                System.out.println("Tab " + newTab + " was at a higher speak point than bank size: " + player.getVariables().getTabStarts()[newTab]);
                                //player.getVariables().getTabStarts()[newTab] = 0;
                            }
                            return;
                        }
                        int tabSize = Bank.getTabSize(player, newTab);
                        if (Constants.DEBUG_MODE)
                            System.out.println("DEBUG: startSlot for new tab: " + player.getVariables().getTabStarts()[newTab] + ", size of new tab: " + tabSize);
                        int toSlot = player.getVariables().getTabStarts()[newTab] + tabSize;
                        int overallOldSlot = oldTab == 0 ? oldSlot : player.getVariables().getTabStarts()[oldTab] + oldSlot;
                        if (Constants.DEBUG_MODE)
                            System.out.println("New tab exists. Adding item from arrayslot " + overallOldSlot + " at end of tab " + newTab + " at slot " + toSlot + " there.");
                        if (newTab > oldTab) {
                            toSlot = toSlot - 1;
                            if (Constants.DEBUG_MODE)
                                System.out.println("New tab > oldTab. Decrease all between");
                            //TODO: something like player.getVariables().getTabStarts()[oldTab]--;
                            for (int i = oldTab + 1; i <= newTab; i++) { //example from tab 3 to 7. All tabs in between must decrease by 1, even tab 7 since it isn't new
                                if (player.getVariables().getTabStarts()[i] != -1) {
                                    if (Constants.DEBUG_MODE)
                                        System.out.println("Decreasing getVariables().getTabStarts() " + i);
                                    player.getVariables().getTabStarts()[i]--;
                                }
                            }
                        }
                        if (newTab < oldTab) {
                            if (Constants.DEBUG_MODE)
                                System.out.println("oldTab > newTab. Increase all between");
                            //player.getVariables().getTabStarts()[oldTab]++;
                            for (int i = oldTab; i > newTab; i--) { //example from tab 7 to 3. All tabs in between must increase by 1, except tab 3(speak)
                                if (player.getVariables().getTabStarts()[i] != -1) {
                                    player.getVariables().getTabStarts()[i]++;
                                }
                            }
                        }

                        if (overallOldSlot != newSlot && overallOldSlot >= 0 && toSlot > 0)
                            player.getBank().insert(overallOldSlot, toSlot); //SWAP ?
                    }
                    player.getBank().refresh();
                } else {
                    //System.out.println("Swapping oldslot " + oldSlot + " and to newSlot " + newSlot);
                    if (oldSlot < 0 || oldSlot >= Bank.SIZE || newSlot < 0 || newSlot >= Bank.SIZE) {
                        break;
                    }
                    itemToSwap = player.getBank().get(oldSlot);
                    if (itemToSwap == null) {
                        break;
                    }
                    int oldTab = player.getAttributes().get("currentTab") != null ? player.getAttributes().getInt("currentTab") : 0;
                    int overallOldSlot = oldTab == 0 ? oldSlot : player.getVariables().getTabStarts()[oldTab] + oldSlot;
                    int overallNewSlot = oldTab == 0 ? newSlot : player.getVariables().getTabStarts()[oldTab] + newSlot;
                    if (player.getBank().isInserting()) {
                        if (player.getBank().get(overallNewSlot) != null
                                && Bank.getTabByItemSlot(player, overallNewSlot) == Bank.getTabByItemSlot(player, overallOldSlot))
                            player.getBank().insert(overallOldSlot, overallNewSlot);
                    } else {
                        if (player.getBank().get(overallNewSlot) != null
                                && Bank.getTabByItemSlot(player, overallNewSlot) == Bank.getTabByItemSlot(player, overallOldSlot))
                            player.getBank().swap(overallOldSlot, overallNewSlot);
                    }

                    boolean refreshTabs = overallOldSlot == player.getVariables().getTabStarts()[oldTab] ? true : false;
                    //only refresh tab config if ur swapping slot 0 in CURRENT tab to update tab item at top
                    Bank.refreshBank(player, refreshTabs);
                }
                break;
            case 149:// inventorys
            case 15:
                if (oldSlot < 0 || oldSlot >= Inventory.SIZE || newSlot < 0 || newSlot >= Inventory.SIZE) {
                    break;
                }
                itemToSwap = player.getInventory().get(oldSlot);
                if (itemToSwap == null) {
                    break;
                }
                player.getInventory().swap(oldSlot, newSlot);
                break;
            default:
                Logger.getInstance().info("UNHANDLED ITEM SWAP 1 : inter = " + interfaceId);
                break;
        }
    }

    private void handleOperateItem(Player player, Packet packet) {
        int interfaceHash = packet.getInt1();
        int slot = packet.getLEShortA();
        int itemId = !Constants.USE_DEV_CACHE ? Loaders.renderIdsViseVersa[packet.getLEShort()] : packet.getLEShort();
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "operated item " + itemId + "," + slot + "," + (interfaceHash >> 16)));
        if (slot < 0 || slot > 28 || !canExecute(player, packet)) {
            return;
        }
        Item item = player.getInventory().getSlot(slot);
        if (item == null) {
            return;
        }
        if (item.getId() != itemId) {
            return;
        }
        player.getInterfaceSettings().closeInterfaces(false);
        if (DialogueManager.handleMisc(player, itemId, false)) {
            return;
        }
    }

    @SuppressWarnings("unused")
    private void handleExamineItem(Player player, Packet packet) {
        int interfaceHash = packet.getInt();
        int slot = packet.getShort();
        Item item = null;
        int interfaceId = interfaceHash >> 16;
        switch (interfaceId) {
            case 336:// equip screen - inv
                item = player.getInventory().get(slot);
                break;
            case 335:// Trade screen
                if (player.getTradingSession() != null) {
                    Player other = player.getTradingSession().getPlayer2();
                    if (other != null) {
                        item = other.getTrade().get(slot);
                    }
                }
                break;
            default:
                player.getPacketSender().sendMessage("Unhandled inter item examine: " + interfaceId);
                break;
        }
        /*
         * if (item != null) { ItemDefinition def =
         * ItemDefinition.forId(item.getId()); if (def == null) { return; }
         * String examine = def.getExamine();
         * player.getFrames().sendMessage(examine); }
         */
    }

    private void handleExamineItem2(Player player, Packet packet) {
        if (packet.getLength() < 1)
            return;
        int itemId = packet.getLEShortA();
        ItemDefinition def = ItemDefinition.forId(itemId);
        if (def == null)
            return;
        player.getPacketSender().sendMessage(def.getDescription() + ((player.getDetails().isAdmin() || player.getDetails().isOwner() || Constants.DEBUG_MODE) ? (" (" + itemId + ")") : ""));
        /*boolean isVowel = false;
        for (String vowel : TextUtils.vowels) {
			if(def.getName()..toLowerCase().startsWith(vowel))
				isVowel = true;
		}
		player.getPacketSender().sendMessage("It's " + (isVowel ? "an " : "a ") + def.getName() + "." + ((player.getDetails().isAdmin() || player.getDetails().isOwner() || Constants.DEBUG_MODE) ? ("(" + itemId + ")") : ""));
	*/
    }

    @Override
    public boolean canExecute(Player player, Packet packet) {
        // TODO Auto-generated method stub
        if (player.getCombatState().isDead()) {
            return false;
        }
        if (player.getAttributes().isSet("stopActions")) {
            return false;
        }
        return true;
    }
}
