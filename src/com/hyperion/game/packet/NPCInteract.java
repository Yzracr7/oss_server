package com.hyperion.game.packet;

import com.hyperion.cache.definitions.CachedNpcDefinition;
import com.hyperion.game.Constants;
import com.hyperion.game.item.Item;
import com.hyperion.game.net.Packet;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.combat.CombatAction;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.NPCDefinition;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.DumbPathFinder;
import com.hyperion.game.world.pathfinders.PathState;
import com.hyperion.game.world.pathfinders.VariablePathFinder;
import com.hyperion.utility.logger.Log;

/**
 * NPC clicking packets.
 *
 * @author Nando
 */
public class NPCInteract implements PacketHandler {

    private static final int ATTACK = 129; // d
    private static final int FIRST_CLICK = 156; // d
    private static final int SECOND_CLICK = 19; // d
    private static final int FOURTH_CLICK = 51;
    private static final int FIFTH_CLICK = 218;
    private static final int MAGIC_ON_NPC = 69; // d
    private static final int ITEM_ON_NPC = 187; // d
    private static final int EXAMINE_NPC = 72;

    @Override
    public void handle(Player player, Packet packet) {
        if (player.getAttributes().isSet("stopActions")
                || player.getCombatState().isDead()) {
            return;
        }
        if (packet.getOpcode() != EXAMINE_NPC) {
            MainCombat.endCombat(player, 1);
            player.resetActionAttributes();
        }
        switch (packet.getOpcode()) {
            case ATTACK:
                CombatType type = player.getCombatState().getCombatType();
                handleAttackNPC(player, packet, type);
                break;
            case MAGIC_ON_NPC:
                handleMagicOnNPC(player, packet);
                break;
            case FIRST_CLICK:
                handleFirstClickNPC(player, packet);
                break;
            case SECOND_CLICK:
                handleSecondClickNPC(player, packet);
                break;
            case FOURTH_CLICK:
                handleFourthClickNPC(player, packet);
                return;
            case FIFTH_CLICK:
                handleFifthClickNPC(player, packet);
                break;
            case ITEM_ON_NPC:
                handleItemOnNPC(player, packet);
                break;
            case EXAMINE_NPC:
                handleNPCExamining(player, packet);
                break;
        }
    }

    private void handleAttackNPC(Player player, Packet packet, CombatType type) {
        int npcIndex = packet.getLEShort() & 0xFFFF;
        if (npcIndex < 0 || npcIndex > Constants.MAX_NPCS) {
            return;
        }
        final NPC npc = (NPC) World.getWorld().getNPC(npcIndex);
        if (npc == null || npc.isDestroyed()) {
            System.out.println("NPC null on attack: " + npcIndex);
            return;
        }

        if (!npc.isAttackable()) {
            if (player.getDetails().isStaff())
                player.getPacketSender().sendMessage("NPC " + npc.getId() + " could not be attacked because of lack of definitions.");
            return;
        }
        for (String owner : Constants.OWNERS) {
            if (Constants.DEBUG_MODE || player.getName().equalsIgnoreCase(owner)) {
                player.getPacketSender().sendMessage("Attacking npc: " + npc.getId());
            }
        }
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Attacked npc: " + npc.getId() + ", at location: " + npc.getLocation().toString()));
        player.setInteractingEntity(npc);
        if (player.getAttributes().isSet("isMonkey")) {
            player.getPacketSender().sendMessage("You cannot fight with your Monkey greegree equipped!");
            return;
        }
        if (npc.getId() == 493) {
            if (type.equals(CombatType.MELEE)) {
                player.sendMessage("I don't think melee will work on these...");
                return;
            } else {
                if (npc.getTransformationId() != 492) {
                    player.sendMessage("A cave kraken is appearing!");
                    npc.animate(7135);
                    npc.setTransformationId(492);
                }
            }
        }
        CombatAction action = MainCombat.getAction(player);
        if (!action.canAttack(player, npc)) {
            return;
        }
        player.getCombatState().setTarget(npc);
        MainCombat.getAction(player); //set combat type
        //TODO setting action is applied twice, once in startAttack
        if (player.getLocation().distance(npc.getLocation()) > 1
                && player.getCombatState().getCombatType() == CombatType.MAGE)
            MainCombat.checkPending(player, false);
        //MainCombat.handleSwitches(player);
        player.getCombatState().startAttack(npc);
        if (player.getAttributes().isSet("regularCast")) {
            player.getAttributes().remove("regularCast");
        }
    }

    private void handleMagicOnNPC(Player player, Packet packet) {
        packet.getShort();
        int interfaceHash = packet.getInt();
        int childId = interfaceHash & 0xFFFF;
        int npcIndex = packet.getShort();
        if (npcIndex < 0 || npcIndex > Constants.MAX_NPCS) {
            return;
        }
        final NPC npc = (NPC) World.getWorld().getNPC(npcIndex);
        if (npc == null || npc.isDestroyed()) {
            return;
        }
        if (!npc.isAttackable()) {
            return;
        }
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Magic on npc: " + npc.getId() + ", at loc: " + npc.getLocation().toString() + ", inter: " + (interfaceHash >> 16) + ", child: " + childId + "."));
        player.getAttributes().set("regularCast", childId);
        MainCombat.handleSwitches(player);
        player.getCombatState().startAttack(npc);
    }

    private void handleFirstClickNPC(final Player player, Packet packet) {
        int npcIndex = packet.getShortA() & 0xFFFF;
        if (npcIndex < 0 || npcIndex > Constants.MAX_NPCS) {
            return;
        }
        final NPC npc = (NPC) World.getWorld().getNPC(npcIndex);
        if (npc == null || npc.isDestroyed()) {
            return;
        }
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Click1 npc: " + npc.getId() + ", at location: " + npc.getLocation().toString()));
        player.setInteractingEntity(npc);
        int x = npc.getLocation().getX();
        int y = npc.getLocation().getY();
        if (player.getLocation().equals(npc.getLocation())) {
            DumbPathFinder.generateMovement(player);
            player.getAttributes().set("type", 1);
            player.getAttributes().set("npcPacket", npc);
            return;
        }
        PathState pathState = player.execute_path(new VariablePathFinder(-1, 0, 0, npc.getSize(), npc.getSize()), x, y);
        if (!canReach(player, npc, pathState, x, y))
            return;

        System.out.println("Handling entity click {type}" + 1 + " " + npc.getId());
        player.getAttributes().set("type", 1);
        player.getAttributes().set("npcPacket", npc);
    }

    private void handleSecondClickNPC(final Player player, Packet packet) {
        int npcIndex = packet.getLEShortA() & 0xFFFF;
        if (npcIndex < 0 || npcIndex > Constants.MAX_NPCS) {
            return;
        }
        final NPC npc = (NPC) World.getWorld().getNPC(npcIndex);
        if (npc == null || npc.isDestroyed()) {
            return;
        }
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Click2 npc: " + npc.getId() + ", at location: " + npc.getLocation().toString()));
        player.setInteractingEntity(npc);
        int x = npc.getLocation().getX();
        int y = npc.getLocation().getY();
        if (player.getLocation().equals(npc.getLocation())) {
            DumbPathFinder.generateMovement(player);
            player.getAttributes().set("type", 2);
            player.getAttributes().set("npcPacket", npc);
            return;
        }
        PathState pathState = player.execute_path(new VariablePathFinder(-1, 0, 0, npc.getSize(), npc.getSize()), x, y);
        if (!pathState.isRouteFound()) {
            if (!canReach(player, npc, pathState, x, y))
                return;
        }

        player.getAttributes().set("type", 2);
        player.getAttributes().set("npcPacket", npc);
    }

    private void handleFourthClickNPC(Player player, Packet packet) {
        int npcIndex = packet.getShortA() & 0xFFFF;
        if (npcIndex < 0 || npcIndex > Constants.MAX_NPCS) {
            System.out.println("a");
            return;
        }
        final NPC npc = (NPC) World.getWorld().getNPC(npcIndex);
        if (npc == null || npc.isDestroyed()) {
            System.out.println("a2");
            return;
        }
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Click4 npc: " + npc.getId() + ", at location: " + npc.getLocation().toString()));
        player.setInteractingEntity(npc);
        int x = npc.getLocation().getX();
        int y = npc.getLocation().getY();
        if (player.getLocation().equals(npc.getLocation())) {
            DumbPathFinder.generateMovement(player);
            player.getAttributes().set("type", 3);
            player.getAttributes().set("npcPacket", npc);
            //Debugging.print("NPC OPTION 31: " + npc.getId());
            return;
        }
        PathState pathState = player.execute_path(new VariablePathFinder(-1, 0, 0, npc.getSize(), npc.getSize()), x, y);
        if (!pathState.isRouteFound()) {
            if (!canReach(player, npc, pathState, x, y))
                return;
        }
        //Debugging.print("NPC OPTION 3: " + npc.getId());
        player.getAttributes().set("type", 3);
        player.getAttributes().set("npcPacket", npc);
    }

    private void handleFifthClickNPC(Player player, Packet packet) {
        int npcIndex = packet.getLEShort();
        if (npcIndex < 0 || npcIndex > Constants.MAX_NPCS) {
            return;
        }
        final NPC npc = (NPC) World.getWorld().getNPC(npcIndex);
        if (npc == null || npc.isDestroyed()) {
            return;
        }
        player.setInteractingEntity(npc);
        //System.out.println("Fifth click NPC " + npc.getId());
    }

    private void handleItemOnNPC(Player player, Packet packet) {
        int interfaceHash = packet.getInt();
        int npcIndex = packet.getShortA() & 0xffff;
        int itemId = packet.getShort() & 0xffff;
        int slot = packet.getShort() & 0xffff;
        int interfaceId = interfaceHash >> 16;
        if (interfaceId != 149) {//inv
            return;
        }
        if (npcIndex < 0 || npcIndex > Constants.MAX_NPCS) {
            return;
        }
        final NPC npc = (NPC) World.getWorld().getNPC(npcIndex);
        if (npc == null || npc.isDestroyed()) {
            return;
        }
        MainCombat.endCombat(player, 1);
        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Item on npc: " + npc.getId() + ", at loc: " + npc.getLocation().toString() + ", with item: " + itemId + ", from slot: " + slot + "."));
        Item item = player.getInventory().getSlot(slot);
        if (item == null) {
            return;
        }
        if (itemId != item.getId()) {
            return;
        }
        if (!player.getInventory().hasItem(itemId)) {
            return;
        }
        player.setInteractingEntity(npc);
        int x = npc.getLocation().getX();
        int y = npc.getLocation().getY();
        if (player.getLocation().equals(npc.getLocation())) {
            DumbPathFinder.generateMovement(player);
            player.getAttributes().set("itemOnNPCItem", item);
            player.getAttributes().set("type", -1);
            player.getAttributes().set("npcPacket", npc);
            return;
        }
        PathState pathState = player.execute_path(new VariablePathFinder(-1, 0, 0, npc.getSize(), npc.getSize()), x, y);
        if (!pathState.isRouteFound()) {
            if (!canReach(player, npc, pathState, x, y))
                return;
        }
        player.getAttributes().set("itemOnNPCItem", item);
        player.getAttributes().set("type", -1);
        player.getAttributes().set("npcPacket", npc);
    }

    private void handleNPCExamining(Player player, Packet packet) {
        if (packet.getLength() <= 0)
            return;
        int id = packet.getShort();
        if (id <= 0)
            return;
        String examine = NPCDefinition.forId(id).getDescription();

        if (examine != null)
            player.getPacketSender().sendMessage("It's a " + CachedNpcDefinition.getNPCDefinitions(id).getName() + "." + (player.getDetails().isOwner() || Constants.DEBUG_MODE ? ("(" + id + ")") : ""));
        else if (player.getName().equalsIgnoreCase("nju") && player.getName().equalsIgnoreCase("snowman"))
            player.getPacketSender().sendMessage("No def for npc examine: " + id);
        //System.out.println(player.getName() + " examined " + CachedNpcDefinition.getNPCDefinitions(id).getName() + " with id " + id + " " + player.getLocation());
    }

    @Override
    public boolean canExecute(Player player, Packet packet) {
        return true;
    }


    private boolean canReach(final Player player, final NPC npc, PathState pathState, final int x, final int y) {
        int finalX = player.getLocation().getX();
        int finalY = player.getLocation().getY();
        if (pathState.getPoints().peekLast() != null) {
            finalX = pathState.getPoints().getLast().getX();
            finalY = pathState.getPoints().getLast().getY();
        }
        if (!pathState.isRouteFound()) {
            if (npc.getId() == 7663 || npc.getId() == 394 || npc.getId() == 395 || npc.getId() == 5453 || npc.getId() == 5455 || npc.getId() == 5456) {
                final double dir = npc.getLocation().distance(player.getLocation());
                //west = 3, north = 1(should be 6, 1 is actually south but paragon devs fuked it up)
                pathState = player.execute_path(new VariablePathFinder(-1, 0, 0, npc.getSize(), npc.getSize()), x + (dir == 3 ? -1 : 0), y + (dir == 1 ? +1 : 0));
                return true;
            } else {
                cantReach(player);
                return false;
            }
        }
        if (player.getCombatState().isFrozen()) {
            final boolean on = Location.create(finalX, finalY, player.getLocation().getZ()).equals(player.getLocation());
            final boolean reached = pathState.hasReached() && on;
            if (!reached && !on) {
                cantReach(player);
                return false;
            }
        }
        return true;
    }

    private void cantReach(Player player) {
        player.getPacketSender().sendMessage("I can't reach that! 0");
        player.getWalkingQueue().reset();
        player.resetActionAttributes();
    }

}