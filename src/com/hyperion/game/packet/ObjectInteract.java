package com.hyperion.game.packet;

import com.hyperion.cache.definitions.CachedObjectDefinition;
import com.hyperion.game.Constants;
import com.hyperion.game.content.skills.agility.rooftops.AlKharidRooftop;
import com.hyperion.game.content.skills.agility.rooftops.DraynorRooftop;
import com.hyperion.game.net.Packet;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.game.world.pathfinders.ObjectPathFinder;
import com.hyperion.game.world.pathfinders.PathState;
import com.hyperion.utility.TextUtils;

/**
 * Object clicking packets.
 *
 * @author Nando
 * @author trees
 */
public class ObjectInteract implements PacketHandler {

    private static final int FIRST_CLICK = 44; // d
    private static final int SECOND_CLICK = 119; // d
    private static final int THIRD_CLICK = 120; // d
    private static final int FOURTH_CLICK = 247;
    private static final int EXAMINE_OBJECT = 94;

    @Override
    public void handle(Player player, Packet packet) {
        player.getAttributes().remove("npcPacket");
        if (player.getAttributes().isSet("stopActions")
                || player.getCombatState().isDead()) {
            return;
        }
        if (packet.getOpcode() != EXAMINE_OBJECT) {
            MainCombat.endCombat(player, 1);
            player.resetActionAttributes();
        }
        switch (packet.getOpcode()) {
            case FIRST_CLICK:
                handleFirstClickObject(player, packet);
                break;
            case SECOND_CLICK:
                handleSecondClickObject(player, packet);
                break;
            case THIRD_CLICK:
                handleThirdClickObject(player, packet);
                break;
            case FOURTH_CLICK:
                handleFourthClickObject(player, packet);
                break;
            case EXAMINE_OBJECT:
                handleObjectExamining(player, packet);
                break;
            default:
                System.out.println("unhandled object packet opcode: " + packet.getOpcode());
                break;
        }
    }

    private void handleFirstClickObject(final Player player, Packet packet) {
        //final int objectId = packet.getLEShort() & 0xffff;
        int objectId = Constants.USE_DEV_CACHE ? packet.getLEShort() & 0xffff
                : CachedObjectDefinition.renderIdsViseVersa[packet.getLEShort() &
                0xffff];
        final int objectX = packet.getShort();
        final int objectY = packet.getLEShort();
        int z = player.getLocation().getZ();
        int playerX = player.getLocation().getX();
        int playerY = player.getLocation().getY();
        int finalX = -1;
        int finalY = -1;
        Location tile = Location.create(objectX, objectY, z);
        final int regionId = tile.getRegionId();
        if (!player.getMapRegionsIds().contains(regionId)) {
            return;
        }
        GameObject mapObject = World.getObjectWithId(tile, objectId);
        if (mapObject == null || mapObject.getId() != objectId) {
            return;
        }
        final GameObject object = mapObject;
        if (!player.getLocation().withinActualDistance(object.getLocation(), 16)) {
            return;
        }
        if (!canExecute(player, packet)) {
            return;
        }
        player.face(tile);
        PathState state = ObjectPathFinder.executePath(player, object);
        if (state.getPoints().peekLast() != null) {
            finalX = state.getPoints().getLast().getX();
            finalY = state.getPoints().getLast().getY();
        } else {
            finalX = playerX;
            finalY = playerY;
        }
        if (!state.isRouteFound() && !DraynorRooftop.isDraynorRooftopObject(object) && !AlKharidRooftop.isAlkharidRooftopObject(object)) {
            player.getWalkingQueue().reset();
            player.getPacketSender().sendMessage("I can't reach that! 1");
            player.resetActionAttributes();
            return;
        }
        Location endOfPath = Location.create(finalX, finalY, z);
        if (player.getCombatState().isFrozen()) {
            player.getWalkingQueue().reset();
            boolean on = endOfPath.equals(player.getLocation());
            boolean reached = state.hasReached() && on;
            if (!reached && !on) {
                player.getPacketSender().sendMessage("I can't reach that! 2");
                player.resetActionAttributes();
                return;
            }
        }
        player.getInterfaceSettings().closeInterfaces(false);
        player.getAttributes().set("type", 1);
        player.getAttributes().set("objectPacket", object);
    }

    private void handleSecondClickObject(final Player player, Packet packet) {
        final int objectX = packet.getShort() & 0xFFFF;
        final int objectId = packet.getLEShortA() & 0xffff;
        final int objectY = packet.getLEShortA() & 0xFFFF;
        int z = player.getLocation().getZ();
        int playerX = player.getLocation().getX();
        int playerY = player.getLocation().getY();
        int finalX = -1;
        int finalY = -1;
        Location tile = Location.create(objectX, objectY, z);
        final int regionId = tile.getRegionId();
        if (!player.getMapRegionsIds().contains(regionId))
            return;
        GameObject mapObject = World.getObjectWithId(tile, objectId);
        if (mapObject == null || mapObject.getId() != objectId)
            return;
        final GameObject object = mapObject;
        if (!player.getLocation().withinActualDistance(object.getLocation(), 16)) {
            return;
        }
        if (!canExecute(player, packet)) {
            return;
        }
        player.face(tile);
        PathState state = ObjectPathFinder.executePath(player, object);
        int real_id = CachedObjectDefinition.renderIdsViseVersa[objectId];
        if (!state.isRouteFound()/* && real_id != 12309 */) {
            player.getPacketSender().sendMessage("I can't reach that! 3");
            player.getWalkingQueue().reset();
            player.resetActionAttributes();
            return;
        }
        if (state.getPoints().peekLast() != null) {
            finalX = state.getPoints().getLast().getX();
            finalY = state.getPoints().getLast().getY();
        } else {
            finalX = playerX;
            finalY = playerY;
        }
        Location endOfPath = Location.create(finalX, finalY, z);
        if (player.getCombatState().isFrozen()) {
            player.getWalkingQueue().reset();
            boolean on = endOfPath.equals(player.getLocation());
            boolean reached = state.hasReached() && on;
            if (!reached && !on) {
                player.getPacketSender().sendMessage("I can't reach that! 4");
                player.resetActionAttributes();
                return;
            }
        }
        player.getInterfaceSettings().closeInterfaces(false);
        player.getAttributes().set("type", 2);
        player.getAttributes().set("objectPacket", object);
    }

    private void handleThirdClickObject(Player player, Packet packet) {
        int objectY = packet.getShort();
        int objectId = packet.getShortA() & 0xffff;
        // int objectId = Constants.USE_DEV_CACHE ? packet.getShortA() :
        // CachedObjectDefinition.renderIdsViseVersa[packet.getShortA()];
        int objectX = packet.getShortA();
        int z = player.getLocation().getZ();
        int playerX = player.getLocation().getX();
        int playerY = player.getLocation().getY();
        int finalX = -1;
        int finalY = -1;
        Location tile = Location.create(objectX, objectY, z);
        final int regionId = tile.getRegionId();
        if (!player.getMapRegionsIds().contains(regionId))
            return;
        GameObject mapObject = World.getObjectWithId(tile, objectId);
        if (mapObject == null || mapObject.getId() != objectId)
            return;
        final GameObject object = mapObject;
        if (!player.getLocation().withinActualDistance(object.getLocation(), 16)) {
            return;
        }
        if (!canExecute(player, packet)) {
            return;
        }
        player.face(tile);
        PathState state = ObjectPathFinder.executePath(player, object);
        int real_id = CachedObjectDefinition.renderIdsViseVersa[objectId];
        if (!state.isRouteFound()/* && real_id != 12309 */) {
            player.getPacketSender().sendMessage("I can't reach that! 5");
            player.getWalkingQueue().reset();
            player.resetActionAttributes();
            return;
        }
        if (state.getPoints().peekLast() != null) {
            finalX = state.getPoints().getLast().getX();
            finalY = state.getPoints().getLast().getY();
        } else {
            finalX = playerX;
            finalY = playerY;
        }
        Location endOfPath = Location.create(finalX, finalY, z);
        if (player.getCombatState().isFrozen()) {
            player.getWalkingQueue().reset();
            boolean on = endOfPath.equals(player.getLocation());
            boolean reached = state.hasReached() && on;
            if (!reached && !on) {
                player.getPacketSender().sendMessage("I can't reach that! 6");
                player.resetActionAttributes();
                return;
            }
        }
        player.getInterfaceSettings().closeInterfaces(false);
        player.getAttributes().set("type", 3);
        player.getAttributes().set("objectPacket", object);
    }

    private void handleFourthClickObject(Player player, Packet packet) {
        int objectY = packet.getShort();
        int objectId = packet.getShortA() & 0xffff;
        // int objectId = Constants.USE_DEV_CACHE ? packet.getShortA() :
        // CachedObjectDefinition.renderIdsViseVersa[packet.getShortA()];
        int objectX = packet.getShortA();
        int z = player.getLocation().getZ();
        int playerX = player.getLocation().getX();
        int playerY = player.getLocation().getY();
        int finalX = -1;
        int finalY = -1;
        Location tile = Location.create(objectX, objectY, z);
        final int regionId = tile.getRegionId();
        if (!player.getMapRegionsIds().contains(regionId))
            return;
        GameObject mapObject = World.getObjectWithId(tile, objectId);
        if (mapObject == null || mapObject.getId() != objectId)
            return;
        final GameObject object = mapObject;
        if (!player.getLocation().withinActualDistance(object.getLocation(), 16)) {
            return;
        }
        if (!canExecute(player, packet)) {
            return;
        }
        player.face(tile);
        PathState state = ObjectPathFinder.executePath(player, object);
        int real_id = CachedObjectDefinition.renderIdsViseVersa[objectId];
        if (!state.isRouteFound()/* && real_id != 12309 */) {
            player.getPacketSender().sendMessage("I can't reach that! 7");
            player.getWalkingQueue().reset();
            player.resetActionAttributes();
            return;
        }
        if (state.getPoints().peekLast() != null) {
            finalX = state.getPoints().getLast().getX();
            finalY = state.getPoints().getLast().getY();
        } else {
            finalX = playerX;
            finalY = playerY;
        }
        Location endOfPath = Location.create(finalX, finalY, z);
        if (player.getCombatState().isFrozen()) {
            player.getWalkingQueue().reset();
            boolean on = endOfPath.equals(player.getLocation());
            boolean reached = state.hasReached() && on;
            if (!reached && !on) {
                player.getPacketSender().sendMessage("I can't reach that! 8");
                player.resetActionAttributes();
                return;
            }
        }
        player.getInterfaceSettings().closeInterfaces(false);
        player.getAttributes().set("type", 4);
        player.getAttributes().set("objectPacket", object);
    }

    @SuppressWarnings("unused")
    private void handleObjectExamining(Player player, Packet packet) {
        if (packet.getLength() < 1)
            return;
        int objectId = packet.getLEShortA();
        CachedObjectDefinition def = CachedObjectDefinition.forId(objectId);
        if (def == null)
            return;
        boolean isVowel = false;
        for (String vowel : TextUtils.vowels) {
            if (def.name != null && def.name.toLowerCase().startsWith(vowel))
                isVowel = true;
        }
        player.getPacketSender().sendMessage("It's " + (isVowel ? "an " : "a ") + def.name + "." + ((player.getDetails().isAdmin() || player.getDetails().isOwner() || Constants.DEBUG_MODE) ? ("(" + objectId + ")") : ""));
    }

    @Override
    public boolean canExecute(Player player, Packet packet) {
        // TODO Auto-generated method stub
        if (player.getCombatState().isDead()) {
            return false;
        }
        if (player.getAttributes().isSet("stopMovement")) {
            return false;
        }
        if (player.getAttributes().isSet("stopActions")) {
            return false;
        }
        return true;
    }

}