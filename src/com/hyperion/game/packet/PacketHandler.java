package com.hyperion.game.packet;

import com.hyperion.game.net.Packet;
import com.hyperion.game.world.entity.player.Player;

/**
 * An inter which describes a class that handles packets.
 * 
 * @author Graham Edgecombe
 *
 */
public interface PacketHandler {

	/**
	 * Handles a single packet.
	 * 
	 * @param player
	 *            The player.
	 * @param packet
	 *            The packet.
	 */
	public void handle(Player player, Packet packet);

	/**
	 * Can this packet continue
	 * 
	 * @param player
	 * @return
	 */
	public boolean canExecute(Player player, Packet packet);

}
