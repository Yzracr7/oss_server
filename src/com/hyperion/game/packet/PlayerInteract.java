package com.hyperion.game.packet;

import com.hyperion.game.Constants;
import com.hyperion.game.content.minigames.clanwars.session.WarSession;
import com.hyperion.game.net.Packet;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.TeleportAreaLocations;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.combat.CombatAction;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.RequestManager.RequestType;
import com.hyperion.game.world.pathfinders.DumbPathFinder;
import com.hyperion.game.world.pathfinders.PathState;
import com.hyperion.game.world.pathfinders.VariablePathFinder;
import com.hyperion.utility.game.Tile;
import com.hyperion.utility.logger.Log;

public class PlayerInteract implements PacketHandler {

	private static final int ATTACK = 84;
	private static final int FOLLOW = 180;
	private static final int TRADE = 183;
	private static final int MAGIC_ON_PLAYER = 123;
	private static final int HANDLE_REQUEST = 68;

	@Override
	public void handle(Player player, Packet packet) {
		if (!canExecute(player, packet)) {
			return;
		}
		player.getWalkingQueue().reset();
		player.resetActionAttributes();
		switch (packet.getOpcode()) {
		case ATTACK:
			handleAttackPlayer(player, packet);
			break;
		case FOLLOW:
			handleFollowPlayer(player, packet);
			break;
		case TRADE:
			handleTradePlayer(player, packet);
			break;
		case MAGIC_ON_PLAYER:
			handleMagicOnPlayer(player, packet);
			break;
		case HANDLE_REQUEST:
			handleRequest(player, packet);
		}
	}

	private void handleRequest(Player player, Packet packet) {
		if(packet.getLength() < 3) {
			System.out.println("Handle request packet invalid, length: " + packet.getLength());
			return;
		}
		int type = packet.getLEShortA();
		int index = packet.getLEShortA();
		if(Constants.DEBUG_MODE) {
			System.out.println("Request: type: " + type + ", player index: " + index + ", packet length: " + packet.getLength());
		}
		Player other = World.getWorld().getPlayer(index);
		if(other == null)
			return;
		final RequestType requestType = type == 4 ? RequestType.TRADE : TeleportAreaLocations.atClanWarsLobby(player.getLocation()) ? RequestType.CLANWAR : RequestType.DUEL;
		handleRequest(player, other, requestType);

	}

	@SuppressWarnings("unused")
	private void handleAttackPlayer(final Player player, Packet packet) {
		int index = packet.getShort();
		if (index < 0 || index >= Constants.MAX_PLAYERS) {
			player.getWalkingQueue().reset();
			return;
		}
		final Player p2 = (Player) World.getWorld().getPlayer(index);
		if (p2 == null) {
			return;
		}
		World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Attacked " + p2.getName() + " at " + p2.getLocation().toString()));
		int x = p2.getLocation().getX();
		int y = p2.getLocation().getY();
		player.getInterfaceSettings().closeInterfaces(false);
		player.setInteractingEntity(p2);
		if (player.getCombatState().isFrozen()) {
			player.getWalkingQueue().reset();
		}
		boolean inWar = player.getAttributes().isSet("clansession");
		if (player.getAttributes().isSet("duelArea") || TeleportAreaLocations.atClanWarsLobby(player.getLocation())
				|| inWar) {

			if (!TeleportAreaLocations.inDuelArenas(player.getLocation()) && !inWar) {
				final RequestType requestType = TeleportAreaLocations.atClanWarsLobby(player.getLocation()) ? RequestType.CLANWAR : RequestType.DUEL;
				handleRequest(player, p2, requestType);
				return;
			} else {
				if(inWar && !p2.getAttributes().isSet("clansession")) {
					player.getPacketSender().sendMessage("You can not attack this player.");
					return;
				}
				if(player.getAttributes().isSet("clansession")) {
					WarSession war_session = player.getAttributes().get("clansession");
					if(!war_session.hasWarStarted()) {
						player.getPacketSender().sendMessage("The war has not started yet.");
						return;
					} else if(war_session.getTeam1().contains(player)) {
						if(war_session.getTeam1().contains(p2)) {
							player.getPacketSender().sendMessage("You can not attack this player.");
							return;
						}
					} else {
						if(war_session.getTeam2().contains(p2)) {
							player.getPacketSender().sendMessage("You can not attack this player.");
							return;
						}
					}
				}
				player.getCombatState().setTarget(p2);
				MainCombat.getAction(player); //set combat type
				//TODO setting action is applied twice, once in startAttack
				if(player.getLocation().distance(p2.getLocation()) > 1
						&& player.getCombatState().getCombatType() == CombatType.MAGE)
					MainCombat.checkPending(player, false);
				//MainCombat.handleSwitches(player);
				player.getCombatState().startAttack(p2);
				return;
			}
		}
		CombatAction action = MainCombat.getAction(player);
		if (!action.canAttack(player, p2)) {
			return;
		}
		player.getCombatState().startAttack(p2);
	}

	private void handleFollowPlayer(Player player, Packet packet) {
		int index = packet.getShort() & 0xFFFF;
		if(Constants.DEBUG_MODE) {
			System.out.println("Follow player index: " + index);
		}
		//if (index > 2000) { // weird packet issue when clicking purple trade
		//	}

		if (index < 0 || index >= Constants.MAX_PLAYERS) {
			return;
		}
		final Player p2 = (Player) World.getWorld().getPlayer(index);
		if (p2 == null) {
			return;
		}
		World.getWorld().loggingThread.appendLog(new Log(player.getName(), "followed " + p2.getName() + " at " + p2.getLocation().toString()));
		player.setInteractingEntity(p2);
		player.getFollowing().setFollowing(p2, false);
	}

	private void handleTradePlayer(final Player player, Packet packet) {
		int index = packet.getLEShortA() & 0xFFFF;
		if (index < 0 || index >= Constants.MAX_PLAYERS) {
			return;
		}
		final Player p2 = (Player) World.getWorld().getPlayer(index);
		if (p2 == null) {
			return;
		}
		World.getWorld().loggingThread.appendLog(new Log(player.getName(), "traded " + p2.getName() + " at " + p2.getLocation().toString()));
		player.setInteractingEntity(p2);
		if(player.getLocation().equals(p2.getLocation())) {
			player.getFollowing().setFollowing(p2, false);
			Location tile = Tile.getSurroundingTile(player, p2);
			player.getWalkingQueue().addStep(tile);
		} else {
			PathState state = player.execute_path(new VariablePathFinder(9, 0, 0, 1, 1), p2.getLocation().getX(), p2.getLocation().getY());
			if (!state.isRouteFound()) {
				player.getPacketSender().sendMessage("I can't reach that! 3");
				player.getWalkingQueue().reset();
				return;
			}
		}

		player.getInterfaceSettings().closeInterfaces(false);
		player.getRequestManager().sendRequest(p2, RequestType.TRADE);
	}

	@SuppressWarnings("unused")
	private void handleMagicOnPlayer(Player player, Packet packet) {
		int junk = packet.getLEShort();
		int index = packet.getLEShortA();
		int interfaceHash = packet.getLEInt();
		int spellBook = interfaceHash >> 16;
		int spell = interfaceHash & 0xffff;
		if (spell < 0) {
			return;
		}
		if (index < 0 || index >= Constants.MAX_PLAYERS) {
			return;
		}
		final Player p2 = (Player) World.getWorld().getPlayer(index);
		if (p2 == null) {
			return;
		}
		World.getWorld().loggingThread.appendLog(new Log(player.getName(), "cast spell on " + p2.getName() + " at " + p2.getLocation().toString() + ", " + spell + ":" + spellBook));
		player.getInterfaceSettings().closeInterfaces(false);
		player.getCombatState().setCombatType(CombatType.MAGE);
		player.getAttributes().set("regularCast", spell);
		MainCombat.handleSwitches(player);
		player.getCombatState().startAttack(p2);
	}

	public static void handleRequest(Player player, Player other, RequestType requestType) {
		player.setInteractingEntity(other);
		final int x = other.getLocation().getX();
		final int y = other.getLocation().getY();
		if (player.getLocation().equals(other.getLocation())) {
			DumbPathFinder.generateMovement(player);
		} else {
			PathState pathState = player.execute_path(new VariablePathFinder(-1, 0, 0, other.getSize(), other.getSize()), x, y);
			if (!pathState.isRouteFound()) {
				player.getPacketSender().sendMessage("I can't reach that! 2");
				player.getWalkingQueue().reset();
				player.resetActionAttributes();
				return;
			}
		}
		if (player.getLocation().distance(other.getLocation()) == 1) {
			player.getRequestManager().sendRequest(other, requestType);
		} else {
			World.getWorld().submit(new Tickable(1) {
				@Override
				public void execute() {
					if(player == null || player.isDestroyed() || player.getCombatState().isDead() || !player.getWalkingQueue().isMoving() || player.getInteractingEntity() != other) {
						this.stop();
					} else {
						if(player.getLocation().distance(other.getLocation()) <= 2) {
							player.getRequestManager().sendRequest(other, requestType);
							this.stop();
						}
					}
				}
			});
		}
	}

	@Override
	public boolean canExecute(Player player, Packet packet) {
		if (player.getAttributes().isSet("stopActions")) {
			return false;
		}
		if (player.getCombatState().isDead()) {
			return false;
		}
		return true;
	}

}