package com.hyperion.game.packet;

import com.hyperion.game.net.Packet;
import com.hyperion.game.world.entity.player.Player;

/**
 * A packet handler which takes no action i.e. it ignores the packet.
 * 
 * @author Graham Edgecombe
 *
 */
public class QuietPacketHandler implements PacketHandler {

	@Override
	public void handle(Player player, Packet packet) {
		switch (packet.getOpcode()) {
		case FRAME_SWITCH_PACKET:
			handleFrameSwitching(player, packet);
			break;
		}
		/*
		 * System.out.println("Unhandled Packet : [opcode=" + packet.getOpcode()
		 * + " length=" + packet.getLength() + " payload=" + packet.getPayload()
		 * + "]");
		 */
	}

	@Override
	public boolean canExecute(Player player, Packet packet) {
		return true;
	}

	/**
	 * @param player
	 *            The player
	 * @param packet
	 *            The packet
	 */
	private void handleFrameSwitching(Player player, Packet packet) {
		int focusedState = packet.get();
		boolean focused = (focusedState == 1);
		player.getInterfaceSettings().setClientFocused(focused);
	}

	/**
	 * The opcode of the packet that is received when the player turns the
	 * screen on or off
	 */
	private static final int FRAME_SWITCH_PACKET = 7;

}
