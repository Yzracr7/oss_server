package com.hyperion.game.packet;

import com.hyperion.game.net.Packet;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.DefaultPathFinder;
import com.hyperion.game.world.pathfinders.SizedPathFinder;
import com.hyperion.utility.logger.Log;

/**
 * Handles walking packets.
 *
 * @author Graham
 * @author Nando
 */
public class Walk implements PacketHandler {

    @Override
    public void handle(final Player player, final Packet packet) {
        player.getVariables().setUsing_grand_exchange(false);
        if (player.getAttributes().isSet("grandexchangemenu")) {
            player.getAttributes().remove("grandexchangemenu");
        }
        //if(packet.getOpcode() == 36) //When following/trading/challenging a player?
        //	return;
        /*
         * Packet 50 - walk
         * Packet 143 - walk(minimap)
         */
        boolean noclip = false;

        if (player.getAttributes().isSet("isResting")) {
            player.animate(11788);
            World.getWorld().submit(new Tickable(2) {
                @Override
                public void execute() {

                    player.getAttributes().remove("isResting");
                    player.getAttributes().remove("stopActions");
                    handle(player, packet);
                    this.stop();
                }
            });

        }
        if (!canExecute(player, packet)) {
            if (!player.getAttributes().isSet("isResting") /* && !emoteDelay */)
                player.getPacketSender().clearMapFlag();
            return;
        }
        //Cast spell before running/rushing

        if (player.getAttributes().isSet("regularCast") || player.getAttributes().isSet("autocastspell"))
            MainCombat.checkPending(player, true);
        //	else if (System.currentTimeMillis() - player.getCombatState().getLastMeleed() < 2800)
        // player.getCombatState().deductCombatCycles(1);
        resetWalkingActions(player, packet);
        int size = packet.getLength();
        if (packet.getOpcode() == 143) {
            size -= 14;
        }
        final int steps = (size - 5) / 2;
        if (steps < 0)
            return;
        final int[][] path = new int[steps][2];
        final boolean runSteps = packet.getByteS() == 1;
        int endY = packet.getLEShort();
        int endX = packet.getShortA();
        for (int i = 0; i < steps; i++) {
            path[i][0] = packet.getByteS();
            path[i][1] = packet.getByteS();
        }
        player.getWalkingQueue().setRunningQueue(runSteps);
        if (steps > 0) {
            endX += path[steps - 1][0];
            endY += path[steps - 1][1];
        }
        if (endX < 0 || endY < 0) {
            return;
        }
        // System.out.println(player.getLocation()+" @ "+endX+" ~ "+endY);
        if (player.getAppearance().isNpc()) {
            player.execute_path(new SizedPathFinder(true), endX, endY);
        } else {
            if (!noclip) {
                player.execute_path(new DefaultPathFinder(), endX, endY);
            } else {
                if (player.getLocation().getX() == endX && player.getLocation().getY() == endY)
                    World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Walked from " + player.getLocation().getX() + "," + player.getLocation().getY() + " to " + endX + "," + endY));
                player.getWalkingQueue().addStep(endX, endY);
                player.getWalkingQueue().finish();
            }
        }
        player.getInterfaceSettings().closeInterfaces(false);
    }

    private void resetWalkingActions(Player player, Packet packet) {
        MainCombat.endCombat(player, 1);
        if (packet.getOpcode() != 36) {// force walkin
            player.resetActionAttributes();
        }
    }

    @Override
    public boolean canExecute(Player player, Packet packet) {
        if (player.getCombatState().isDead()) {
            return false;
        }
        if (player.getAttributes().isSet("isResting")) {
            return false;
        }
        if (packet.getOpcode() != 36) {
            if (player.getCombatState().isFrozen()) {
                player.getPacketSender().sendMessage("A magical force stops you from moving.");
                MainCombat.endCombat(player, 1);
                return false;
            }
        }
        if (player.getAttributes().isSet("stopMovement")) {
            return false;
        }
        if (player.getAttributes().isSet("stopActions")) {
            return false;
        }
        return true;
    }

}
