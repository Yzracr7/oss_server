package com.hyperion.game.task.impl;

import com.hyperion.game.Constants;
import com.hyperion.game.GameEngine;
import com.hyperion.game.task.Task;
import com.hyperion.game.world.World;
import com.hyperion.game.world.region.MapBuilder;
import com.hyperion.game.world.region.Region;

/**
 * Performs garbage collection and finalization.
 * @author Graham Edgecombe
 *
 */
public class CleanupTask implements Task {

	@Override
	public void execute(GameEngine context) {
		boolean force = (Runtime.getRuntime().freeMemory() < Constants.MIN_FREE_MEM_ALLOWED);
		if (force) {
			skip: for (Region region : World.getWorld().getRegionManager()
					.getRegions().values()) {
				for (int regionId : MapBuilder.FORCE_LOAD_REGIONS)
					if (regionId == region.getId())
						continue skip;
				region.unloadMap();
			}
		}
		System.gc();
		System.runFinalization();
	}

}
