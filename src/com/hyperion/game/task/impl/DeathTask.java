package com.hyperion.game.task.impl;

import com.hyperion.game.GameEngine;
import com.hyperion.game.task.Task;

/**
 * A task which stops the game engine.
 * @author Graham Edgecombe
 *
 */
public class DeathTask implements Task {

	@Override
	public void execute(GameEngine context) {
		if(context.isRunning()) {
			context.stop();
		}
	}

}
