package com.hyperion.game.task.impl;
/*package com.hyperion.game.task.impl;

import com.hyperion.game.GameEngine;
import com.hyperion.game.task.Task;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;

public class MinuteTask implements Task {

	
	//Unused -- See MinuteTick
	@Override
	public void execute(GameEngine context) {
		for (final Player player : World.getWorld().getPlayers()) {
			if (player == null)
				continue;
			player.heal(1);
			for (int i = 0; i < Skills.SKILL_COUNT; i++) {
				if (player.getSkills().getLevel(i) > player.getSkills().getLevelForXp(i)) {
					player.getSkills().decrementLevel(i);
				}
			}
			if (player.getSettings().isSkulled() && !player.getCombatState().isDead()) {
				player.getSettings().setSkullCycles(player.getSettings().getSkullCycles() - 1);
			}
			if (player.getSettings().getSpecialAmount() < 100) {
				player.getSettings().increaseSpecialAmount(20);
				if (player.getSettings().getSpecialAmount() > 100) {
					player.getSettings().setSpecialAmount(100, false);
				}
				player.getSettings().setSpecialAmount(player.getSettings().getSpecialAmount(), true);
			}
			if (player.getAttributes().isSet("cyclopsroom")) {
				if (player.getInventory().hasItemAmount(8851, 20)) {
					player.getInventory().deleteItem(8851, 20);
					player.getPacketSender().sendMessage("Your tokens have been reduced.");
				} else {
					player.getAttributes().remove("cyclopsroom");
					MainCombat.endCombat(player, 1);
					Entity other = player.getCombatState().getCurrentAttacker();
					if (other != null) {
						MainCombat.endCombat(other, 1);
					}
					player.teleport(2845, 3540, 2);
					player.getPacketSender().sendMessage("You have run out of tokens.");
					player.getAttributes().set("nextDialogue", -1);
				}
			}
		}
	}
}*/