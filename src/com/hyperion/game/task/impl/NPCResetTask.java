package com.hyperion.game.task.impl;

import com.hyperion.game.world.entity.npc.NPC;

/**
 * A task which resets an NPC after an update cycle.
 * @author Graham Edgecombe
 *
 */
public class NPCResetTask {
	
	public static void execute(NPC npc) {
		npc.resetHits();
		npc.getUpdateFlags().reset();
		npc.setTeleporting(false);
		npc.reset();
	}

}
