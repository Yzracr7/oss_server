package com.hyperion.game.task.impl;

import java.util.List;

import com.hyperion.game.content.pets.BossPetDropHandler;
import com.hyperion.game.world.AreaHandler;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.TeleportAreaLocations;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatAction;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.Directions;
import com.hyperion.game.world.pathfinders.Directions.NormalDirection;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.game.world.pathfinders.SizedPathFinder;
import com.hyperion.game.world.pathfinders.TileControl;
import com.hyperion.utility.NumberUtils;

/**
 * A task which performs pre-update tasks for an NPC.
 *
 * @author Graham Edgecombe
 */
public class NPCTickTask {

	public static void execute(NPC npc) {
		/*
		 * If the map region changed set the last known region.
		 */
		if (npc.isMapRegionChanging()) {
			npc.setLastKnownRegion(npc.getLocation());
			npc.loadMapRegions();
		}
		handleOverheadChat(npc);
		handleNPCActions(npc);
		if (npc.getCombatState().getTarget() != null) {
			MainCombat.executeCombatCycles(npc, npc.getCombatState().getTarget());
		}
		if (npc.getCombatState().getCombatCycles() > 0) {
			npc.getCombatState().deductCombatCycles(1);
		}
		if (npc.getCombatState().getTarget() != null) {
			npc.getFollowing().setFollowing(npc.getCombatState().getTarget(), true);
		}
		if (npc.getFollowing() != null) {
			if (npc.getAttributes().isSet("petOwner") && npc.getFollowing().getOther() == null) {
				Player owner = (Player) npc.getAttributes().get("petOwner");
				if (owner != null) {
					npc.getFollowing().setFollowing(owner, false);
					// System.out.println("Set owner for " + owner.getName());
				} else
					World.getWorld().removeNPC(npc);
			}
			if (npc.getFollowing().getOther() != null) {
				npc.getFollowing().executeFollowing();
			}
		}

		npc.processQueuedHits();
		npc.getWalkingQueue().processMovement();
		AreaHandler.handleAreas(npc);
		npc.updateCoverage(npc.getLocation());
		TileControl.getSingleton().setOccupiedLocation(npc, TileControl.getHoveringTiles(npc));
	}

	private static boolean handleNPCAggression(NPC npc) {
		if (!npc.getDefinition().isAttackable()) {
			return false;
		}
		boolean aggressive = false;
		if (npc.getId() == 1603) {
			return false;
		}
		if (TeleportAreaLocations.inWilderness(npc.getLocation())
				&& BossPetDropHandler.getPetForNpcId(npc.getId()) == null) {
			switch (npc.getId()) {
			case 2580:
			case 2204:
			case 694:
			case 693:
				return false;
			default:
				return true;
			}
		} else if (npc.getDefinition().isAggressive()) {
			aggressive = true;
		}
		if (npc.getAttributes().isSet("owner")) {
			Player player = null;
			NPC owner = null;
			if (npc.getId() == 6617) {
				owner = npc.getAttributes().get("owner");
			} else {
				player = npc.getAttributes().get("owner");
			}
			if (player != null) {
				if (player.getLocation().distanceToPoint(npc.getLocation()) < 20) {
					attemptAttack(npc, player);
				}
			} else {
				if (owner.getLocation().distanceToPoint(npc.getLocation()) < 20) {
					// attemptAttack(npc, owner);
				}
			}
			return false;
		}
		for (int regionId : npc.getMapRegionsIds()) {

			List<Integer> playerIndexes = World.getWorld().getRegion(regionId, false).getPlayerIndexes();
			if (playerIndexes == null) {
				continue;
			}

			for (Integer playerIndex : playerIndexes) {
				Player player = World.getWorld().getPlayer(playerIndex);

				if (player == null)
					continue;

				if (npc.getLast_shout() >= 0) {
					npc.setLast_shout(System.currentTimeMillis());
				} else {
					npc.last_shout--;
				}
				if (npc.getDefinition().getName().toLowerCase().contains("bandit")) {
					if (npc.getId() == 694 || npc.getId() == 693) {
						return false;
					}
					// System.out.println(npc.getDefinition().getName() + " good " + npc.getId());
					if (ProjectilePathFinder.hasLineOfSight(npc.getLocation(), player.getLocation(), false)) {
						attemptAttack(npc, player);
						if (System.currentTimeMillis() - npc.getLast_shout() < 1000) {
							if (player.getEquipment().containsName("zamorak")) {
								npc.playForcedChat("Prepare to die, Zamorakian scum!!");
							} else if (player.getEquipment().containsName("saradomin")) {
								npc.playForcedChat("Prepare to die, Saradominist filth!");
							} else if (NumberUtils.random(0, 10) > 8) {
								npc.playForcedChat("You picked the wrong place to speak trouble!");
							}
						}
						return true;
					}
				} else {
					if (!aggressive) {
						return false;
					}
				}
				if (attemptAttack(npc, player)) {

					// Aggression count
					int aggr_hits = player.getAttributes().get("aggressive_hits") != null
							? player.getAttributes().getInt("aggressive_hits")
							: 0;
					player.getAttributes().set("aggressive_hits", aggr_hits + 1);
					break;
				}
			}

		}
		return false;
	}

	private static boolean attemptAttack(NPC npc, Player player) {
		if (player.getAttributes().isSet("aggressive_timer")) {
			return false;
		}
		if (player.getLocation().getZ() != npc.getLocation().getZ()) {
			return false;
		}
		if (!TeleportAreaLocations.isInMultiZone(player, player.getLocation())) {
			if (System.currentTimeMillis() - player.getCombatState().getPreviousHit() < 6000) {
				return false;
			}
		}
		if (npc.armadyl_armour_checker()) {
			if (player.getEquipment().containsName("armadyl"))
				return false;
		} else if (npc.bandos_armour_checker()) {
			if (player.getEquipment().containsName("bandos"))
				return false;
		} else if (npc.zamorak_armour_checker()) {
			if (player.getEquipment().containsName("zamorak")) {
				return false;
			}
		} else if (npc.saradomin_armour_checker()) {
			if (player.getEquipment().containsName("saradomin")) {
				return false;
			}
		}

		if (player.getAttributes().isSet("isMonkey")) {
			return false;
		}

		if (npc.getDefinition().getName().contains("Revenant") && player.getCombatState().getTarget() != npc) {
			int otherCombatLevel = npc.getDefinition().getCombatLevel();
			boolean withinLvl = (otherCombatLevel >= player.getLowestLevel()
					&& otherCombatLevel <= player.getHighestLevel());
			if (!withinLvl) {
				return false;
			}
		}

		CombatAction action = MainCombat.getAction(npc);
		if (action.canAttack(npc, player)) {
			// Distance ignored
			if (npc.getId() == 932 || npc.getId() == 931 || npc.getId() == 935) {
				npc.setInteractingEntity(player);
				npc.getCombatState().startAttack(player);
				return true;
			} else if (Location.isWithinDistance(npc, player, npc.getAggressiveDistance())) {
				npc.setInteractingEntity(player);
				npc.getCombatState().startAttack(player);
				return true;
			}
		}
		return false;
	}

	/*
	 * public static Player nearestPlayer(List<Integer> arr, NPC npc) { int i = 0;
	 * int nearestIndex = 0; int min = Integer.MAX_VALUE; if (arr == null) { return
	 * null; } else { while (i < arr.size()) { Player p =
	 * World.getWorld().getPlayer(i); if (p != null) { if(checkAll(p, npc) == true)
	 * continue; int dist = p.getLocation().distanceToPoint( npc.getLocation()); if
	 * (dist < min) { min = dist; nearestIndex = i; } i++; } } } return
	 * World.getWorld().getPlayer(nearestIndex); }
	 *
	 * private static boolean checkAll(Player p, NPC npc) { if
	 * (p.getLocation().getZ() != npc.getLocation().getZ()) { return false; } if
	 * (!npc.getAttributes().isSet("pestzombie")) { if
	 * (!ProjectilePathFinder.hasLineOfSight(npc, p, false)) { return false; } } if
	 * (!TeleportAreaLocations.isInMultiZone(p, p.getLocation())) { if
	 * (System.currentTimeMillis() - p.getCombatState().getPreviousHit() < 6000) {
	 * return false; } } if (p.getAttributes().isSet("isMonkey")) { return false; }
	 * CombatAction action = MainCombat.getAction(npc); if (!action.canAttack(npc,
	 * p)) return false; if (!Location.isWithinDistance(npc, p,
	 * npc.getAggressiveDistance())) return false;
	 *
	 * if (npc.armadyl_armour_checker()) { if
	 * (p.getEquipment().containsName("armadyl")) return false; } else if
	 * (npc.bandos_armour_checker()) { if (p.getEquipment().containsName("bandos"))
	 * return false; } else if (npc.zamorak_armour_checker()) { if
	 * (p.getEquipment().containsName("zamorak")) return false; } else if
	 * (npc.saradomin_armour_checker()) { if
	 * (p.getEquipment().containsName("saradomin")) return false; } return true; }
	 */

	/**
	 * NPC Guns Rep Count
	 */
	static int overhead_guns_tick = 0;
	static int overhead_guns_tick_last;
	static int guns_rep_count;

	private static void handleOverheadChat(NPC npc) {
		if (npc.getCombatState().isDead()) {
			return;
		}
		int random = NumberUtils.random(45);
		switch (npc.getId()) {
		case 3599: // The Guns
			overhead_guns_tick++;
			if (overhead_guns_tick_last + 3 <= overhead_guns_tick) {
				overhead_guns_tick_last = overhead_guns_tick;
				npc.playForcedChat("" + guns_rep_count);
				guns_rep_count += 1;
			}
			break;
		case 278: // Falador Crier
		case 279: // Ardy Town Crier
		case 277:
		case 280: // Seers Town Crier
			if (random == 10) {
				npc.playForcedChat("Join the 'help' cc for any questions!");
			} else if (random == 27) {
				npc.playForcedChat("You can purchase bonds on our online community!");
			}
			break;
		case 5216: // Benny (varrock Paper)
			if (random == 10) {
				npc.playForcedChat("Get Your Varrock Herald Here!");
			} else if (random == 22) {
				npc.playForcedChat("Read all about it");
			} else if (random == 7) {
				npc.playForcedChat("Varrock Herald, Now only 50 gold!");
			}
			break;
		case 1831: // Swan
			if (random == 10) {
				npc.playForcedChat("Honk!");
			}
			break;
		case 1262: // Ram
		case 2794: // Sheep
			if (random == 10) {
				npc.playForcedChat("Baa");
			}
			break;
		case 1838: // Duck
			if (random == 10) {
				npc.playForcedChat("Quack!");
			}
			break;
		}
	}

	private static void handleNPCActions(NPC npc) {
		if (npc.getCombatState().isDead()) {
			return;
		}
		if (npc.getSpawnLocation() == null && npc.getAttributes().isSet("owner")
				&& !npc.getAttributes().isSet("petOwner")) {
			boolean remove = false;
			if (npc.getCombatState().getLastAttacked() != -1 && npc.getCombatState().getLastAttacked() != 0
					&& (System.currentTimeMillis() - npc.getCombatState().getLastAttacked()) >= 120000) {
				remove = true;
			} else if (npc.getAttributes().get("owner") != null) {
				Player owner = npc.getAttributes().get("owner");
				if (owner == null || (owner != null && owner.getLocation().distance(npc.getLocation()) >= 50))
					remove = true;
			}
			if (remove) {
				System.out
						.println("Removed NPC: " + npc.getId() + ", timer: " + npc.getCombatState().getLastAttacked());
				World.getWorld().removeNPC(npc);
				return;
			}
		}
		if (npc.getId() == 4402) {
			handleSpecialEffects(npc);
		}
		if (!npc.isWalkingHome() && npc.getCombatState().getTarget() == null) {
			if (handleNPCAggression(npc)) {
				return;
			}
		}
		if (npc.getSpawnLocation() == null) {
			return;
		}
		if (!npc.getCombatState().isOutOfCombat(null, false)) {
			return;
		}
		Entity npcTarget = npc.getCombatState().getTarget();
		if (npcTarget != null) {
			return;
		}
		MainCombat.endCombat(npc, 1);
		if (npc.isWalkingHome()) {
			// System.out.println("handle walking home");
			handleWalkingHome(npc);
		} else {
			handleRandomWalking(npc);
		}
	}

	private static void handleSpecialEffects(NPC npc) {
		for (int regionId : npc.getMapRegionsIds()) {
			List<Integer> npcIndexes = World.getWorld().getRegion(regionId, false).getNPCsIndexes();
			if (npcIndexes == null) {
				continue;
			}
			for (Integer npcIndex : npcIndexes) {
				NPC npc2 = World.getWorld().getNPC(npcIndex);
				if (npc2 == null)
					continue;
				if (npc.getId() == 4402 && npc2.getId() == 6308) { // Scorpia
					if (!npc2.isDestroyed() && !npc2.getCombatState().isDead() && npc2.isVisible()
							&& npc2.getHp() > 0) {
						npc.getAttributes().set("lastScorpiaCheck", System.currentTimeMillis());
						if (NumberUtils.random(0, 13) == 10) {
							npc.face(npc2.getLocation());
							npc2.heal(NumberUtils.random(0, 10));

							List<Integer> pIndexes = World.getWorld().getRegion(regionId, false).getPlayerIndexes();
							if (pIndexes == null) {
								continue;
							}
							for (Integer pIndex : pIndexes) {
								Player player = World.getWorld().getPlayer(pIndex);
								if (player != null)
									player.getPacketSender().sendMessage("Scorpia's Guardian heals her a bit.");
							}
						}
					}
				}
			}
		}
	}

	private static void handleRandomWalking(NPC npc) {
		if (npc.getSpawnLocation() == null) {
			return;
		}
		if (!npc.isRandomWalking()) {
			return;
		}
		Location spawn = npc.getSpawnLocation();
		Location loc = npc.getCentreLocation();
		if (NumberUtils.random.nextInt(3) == 0) {
			NormalDirection dir = NormalDirection.forIntValue(NumberUtils.random.nextInt(8));
			if (Location.canMove(npc, dir, npc.getSize(), true)) {
				Location next = npc.getLocation().transform(Directions.DIRECTION_DELTA_X[dir.intValue()],
						Directions.DIRECTION_DELTA_Y[dir.intValue()], 0);
				if (npc.isRoamingWilderness()) {
					if (TeleportAreaLocations.inWilderness(next)
							&& ProjectilePathFinder.hasLineOfSight(loc, next, false)) {
						npc.execute_path(new SizedPathFinder(true), next.getX(), next.getY());
					}
				} else {
					if (next.withinDistance(spawn, 2, false) && ProjectilePathFinder.hasLineOfSight(loc, next, false)) {
						npc.execute_path(new SizedPathFinder(true), next.getX(), next.getY());
					}
				}
			}
		}
	}

	private static void handleWalkingHome(NPC npc) {
		if (npc.getSpawnLocation() != null) {
			Location home = Location.create(npc.getSpawnLocation().getX(), npc.getSpawnLocation().getY(),
					npc.getSpawnLocation().getHeight());
			int lowestX = npc.getSpawnLocation().getX() - 2;
			int lowestY = npc.getSpawnLocation().getY() - 2;
			int highestX = npc.getSpawnLocation().getX() + 2;
			int highestY = npc.getSpawnLocation().getY() + 2;
			if (npc.getLocation().inArea(lowestX, lowestY, highestX, highestY)) {
				npc.setWalkingHome(false);
			} else {
				npc.execute_path(new SizedPathFinder(), home.getX(), home.getY());
			}
		}
	}

}
