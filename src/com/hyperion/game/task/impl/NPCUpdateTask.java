package com.hyperion.game.task.impl;

import java.util.Iterator;
import java.util.List;

import com.hyperion.game.Constants;
import com.hyperion.game.net.Packet;
import com.hyperion.game.net.PacketBuilder;
import com.hyperion.game.world.Loaders;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.ClientModeAnimations;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.UpdateFlags;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

/**
 * A task which creates and sends the NPC update block.
 *
 * @author Graham Edgecombe
 */
public class NPCUpdateTask {

    public static void execute(Player player) {
        /*
         * The update block holds the update masks and data, and is written
         * after the main block.
         */
        PacketBuilder updateBlock = new PacketBuilder();

        /*
         * The main packet holds information about adding, moving and removing
         * NPCs.
         */
        PacketBuilder packet = new PacketBuilder(174, Packet.Type.VARIABLE_SHORT);
        packet.startBitAccess();

        /*
         * Write the current size of the npc list.
         */
        packet.putBits(8, player.getLocalNPCs().size());

        /*
         * Iterate through the local npc list.
         */
        for (Iterator<NPC> it$ = player.getLocalNPCs().iterator(); it$.hasNext(); ) {
            /*
             * Get the next NPC.
             */
            NPC npc = it$.next();

            /*
             * If the NPC should still be in our list.
             */
            if (World.getWorld().containsNPC(npc) != -1 && !npc.isTeleporting() && npc.getLocation().withinActualDistance(player.getLocation(), 14) && npc.isVisible()) {
                /*
                 * Update the movement.
                 */
                updateNPCMovement(packet, npc);

                /*
                 * Check if an update is required, and if so, sendTeleportTitles the update.
                 */
                if (npc.getUpdateFlags().isUpdateRequired()) {
                    updateNPC(player, updateBlock, npc);
                }
            } else {
                /*
                 * Otherwise, remove the NPC from the list.
                 */
                it$.remove();

                /*
                 * Tell the client to remove the NPC from the list.
                 */
                packet.putBits(1, 1);
                packet.putBits(2, 3);
            }
        }

        for (int regionId : player.getMapRegionsIds()) {
            List<Integer> indexes = World.getWorld().getRegion(regionId, false).getNPCsIndexes();
            if (indexes == null)
                continue;
            for (int npcIndex : indexes) {
                if (player.getLocalNPCs().size() >= 255) {
                    break;
                }
                NPC npc = World.getWorld().getNPC(npcIndex);
                if (npc == null || player.getLocalNPCs().contains(npc) || !npc.isVisible() || !npc.getLocation().withinActualDistance(player.getLocation(), 14)) {
                    continue;
                }
                player.getLocalNPCs().add(npc);
                addNewNPC(player, packet, npc);
                if (npc.getUpdateFlags().isUpdateRequired()) {
                    updateNPC(player, updateBlock, npc);
                }
            }
        }

        /*
         * Check if the update block isn't clearAllBoxes.
         */
        if (!updateBlock.isEmpty()) {
            /*
             * If so, put a flag indicating that an update block follows.
             */
            packet.putBits(15, 32767);
            packet.finishBitAccess();

            /*
             * And append the update block.
             */
            packet.put(updateBlock.toPacket().getPayload());
        } else {
            /*
             * Terminate the packet normally.
             */
            packet.finishBitAccess();
        }

        /*
         * Write the packet.
         */
        player.getSession().write(packet.toPacket());
    }

    /**
     * Adds a new NPC.
     *
     * @param packet The main packet.
     * @param npc    The npc to add.
     */
    private static void addNewNPC(Player player, PacketBuilder packet, NPC npc) {
        /*
         * Write the NPC's index.
         */
        packet.putBits(15, npc.getIndex());
        /*
         * Calculate the x and y offsets.
         */
        int yPos = npc.getLocation().getY() - player.getLocation().getY();
        int xPos = npc.getLocation().getX() - player.getLocation().getX();
        /*
         * And write them.
         */
        packet.putBits(5, yPos);
        /*
         * We now write the NPC type id.
         */
        packet.putBits(14, npc.getDefinition().getId());//Constants.USE_DEV_CACHE ? npc.getDefinition().getId() : NPCDefinition.renderIds[npc.getDefinition().getId()]);
        /*
         * And indicate if an update is required.
         */
        packet.putBits(1, npc.getUpdateFlags().isUpdateRequired() ? 1 : 0);
        /*
         * TODO unsure, but probably discards the client-side walk queue.
         */
        packet.putBits(1, 0);
        packet.putBits(3, npc.getDirection());
        packet.putBits(5, xPos);
    }

    /**
     * Update an NPC's movement.
     *
     * @param packet The main packet.
     * @param npc    The npc.
     */
    private static void updateNPCMovement(PacketBuilder packet, NPC npc) {
        /*
         * Check if the NPC is running.
         */
        if (npc.getSprites().getSecondarySprite() == -1) {
            /*
             * They are not, so check if they are walking.
             */
            if (npc.getSprites().getPrimarySprite() == -1) {
                /*
                 * They are not walking, check if the NPC needs an update.
                 */
                if (npc.getUpdateFlags().isUpdateRequired()) {
                    /*
                     * Indicate an update is required.
                     */
                    packet.putBits(1, 1);

                    /*
                     * Indicate we didn't move.
                     */
                    packet.putBits(2, 0);
                } else {
                    /*
                     * Indicate no update or movement is required.
                     */
                    packet.putBits(1, 0);
                }
            } else {
                /*
                 * They are walking, so indicate an update is required.
                 */
                packet.putBits(1, 1);

                /*
                 * Indicate the NPC is walking 1 tile.
                 */
                packet.putBits(2, 1);

                /*
                 * And write the direction.
                 */
                packet.putBits(3, npc.getSprites().getPrimarySprite());

                /*
                 * And write the update flag.
                 */
                packet.putBits(1, npc.getUpdateFlags().isUpdateRequired() ? 1 : 0);
            }
        } else {
            /*
             * They are running, so indicate an update is required.
             */
            packet.putBits(1, 1);

            /*
             * Indicate the NPC is running 2 tiles.
             */
            packet.putBits(2, 2);

            /*
             * And write the directions.
             */
            packet.putBits(3, npc.getSprites().getPrimarySprite());
            packet.putBits(3, npc.getSprites().getSecondarySprite());

            /*
             * And write the update flag.
             */
            packet.putBits(1, npc.getUpdateFlags().isUpdateRequired() ? 1 : 0);
        }
    }

    /**
     * Update an NPC.
     *
     * @param npc The npc.
     */
    private static void updateNPC(Player player, PacketBuilder updateBlock, NPC npc) {
        /*
         * Calculate the mask.
         */
        int mask = 0;
        final UpdateFlags flags = npc.getUpdateFlags();
        if (flags.get(UpdateFlag.ANIMATION)) {
            mask |= 0x40;
        }
        if (flags.get(UpdateFlag.FORCED_CHAT)) {
            mask |= 0x80;
        }
        if (flags.get(UpdateFlag.GRAPHICS)) {
            mask |= 0x8;
        }
        if (flags.get(UpdateFlag.FACE_COORDINATE)) {
            mask |= 0x1;
        }
        if (flags.get(UpdateFlag.HIT)) {
            mask |= 0x10;
        }
        if (flags.get(UpdateFlag.FACE_ENTITY)) {
            mask |= 0x4;
        }
        if (flags.get(UpdateFlag.HIT_2)) {
            mask |= 0x20;
        }
        if (flags.get(UpdateFlag.TRANSFORM)) {
            mask |= 0x2;
        }
        //if(mask >= 0x100) {
        //	mask |= 0x8;
        //	updateBlock.put((byte) (mask & 0xFF));
        //	updateBlock.put((byte) (mask >> 8));
        //} else {
        updateBlock.put((byte) (mask));
        //}
        if (flags.get(UpdateFlag.ANIMATION)) {
            appendAnimationUpdate(player, npc, updateBlock);
        }
        if (flags.get(UpdateFlag.FORCED_CHAT)) {
            appendForceTextUpdate(npc, updateBlock);
        }
        if (flags.get(UpdateFlag.GRAPHICS)) {
            appendGraphicsUpdate(npc, updateBlock);
        }
        if (flags.get(UpdateFlag.FACE_COORDINATE)) {
            appendFaceLocationUpdate(npc, updateBlock);
        }
        if (flags.get(UpdateFlag.HIT)) {
            appendHitUpdate(npc, updateBlock, player);
        }
        if (flags.get(UpdateFlag.FACE_ENTITY)) {
            appendEntityFocusUdate(npc, updateBlock);
        }
        if (flags.get(UpdateFlag.HIT_2)) {
            appendHit2Update(npc, updateBlock, player);
        }
        if (flags.get(UpdateFlag.TRANSFORM)) {
            appendTransformationUpdate(npc, updateBlock);
        }
    }

    private static void appendHitUpdate(NPC npc, PacketBuilder updateBlock, Player thisPlayer) {
        double max = npc.getDefinition().getHitpoints();
        double hp = npc.getHp();
        double calc = hp / max;
        int percentage = (int) (calc * 100);
        if (percentage > 100) {
            percentage = 100;
        }
        Hit hit = npc.getPrimaryHit();
        updateBlock.putByteS((byte) hit.getDamage());
        updateBlock.putByteA((byte) hit.getType().getType());
        updateBlock.putByteA((byte) hit.getCombatType());
        updateBlock.putByteA((byte) (hit.getOwner() == thisPlayer ? 0 : 1));
        updateBlock.putByteA((byte) percentage);
        updateBlock.putByteS((byte) 100);
    }

    private static void appendHit2Update(NPC npc, PacketBuilder updateBlock, Player thisPlayer) {
        double max = npc.getDefinition().getHitpoints();
        double hp = npc.getHp();
        double calc = hp / max;
        int percentage = (int) (calc * 100);
        if (percentage > 100) {
            percentage = 100;
        }
        Hit hit = npc.getSecondaryHit();
        updateBlock.put((byte) hit.getDamage());
        updateBlock.putByteA((byte) hit.getType().getType());
        updateBlock.putByteA((byte) hit.getCombatType());
        updateBlock.putByteA((byte) (hit.getOwner() == thisPlayer ? 0 : 1));
        updateBlock.put((byte) percentage);
        updateBlock.put((byte) 100);
    }

    private static void appendAnimationUpdate(Player player, NPC npc, PacketBuilder updateBlock) {
        Animation currentAnimation = npc.getAnimationQueue().getHighestPriority();
        int animation = ClientModeAnimations.getFixedAnimations(player, currentAnimation.getId());
        if (animation == -1 || animation == 65535)
            updateBlock.putShortA(-1);
        else if (animation > Loaders.animIds.length)
            updateBlock.putShortA(animation);
        else
            updateBlock.putShortA(Constants.USE_DEV_CACHE ? animation : Loaders.animIds[animation]);
        updateBlock.putByteA((byte) currentAnimation.getDelay());
    }

    private static void appendEntityFocusUdate(NPC npc, PacketBuilder updateBlock) {
        Entity entity = npc.getInteractingEntity();
        updateBlock.putShort(entity == null ? -1 : entity.getClientIndex());
    }

    private static void appendGraphicsUpdate(NPC npc, PacketBuilder updateBlock) {
        updateBlock.putLEShort(npc.getCurrentGraphic().getId());
        updateBlock.putInt1(npc.getCurrentGraphic().getDelay() | npc.getCurrentGraphic().getHeight() << 16);
    }

    private static void appendTransformationUpdate(NPC npc, PacketBuilder updateBlock) {
        int transformationId = npc.getTransformationId();
        if (transformationId == -1) {
            transformationId = npc.getId();
        }
        updateBlock.putShort(transformationId);//Constants.USE_DEV_CACHE ? transformationId : NPCDefinition.renderIds[transformationId]);
    }

    private static void appendForceTextUpdate(NPC npc, PacketBuilder updateBlock) {
        updateBlock.putRS2String(npc.getForcedChat());
    }

    private static void appendFaceLocationUpdate(NPC npc, PacketBuilder updateBlock) {
        Location loc = npc.getFaceLocation();
        int x = loc.getX();
        int y = loc.getY();
        updateBlock.putShortA(x = 2 * x + 1);
        updateBlock.putShort(y = 2 * y + 1);
    }
}