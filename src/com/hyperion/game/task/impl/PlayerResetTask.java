package com.hyperion.game.task.impl;

import com.hyperion.game.world.entity.player.Player;

/**
 * A task which resets a player after an update cycle.
 * @author Graham Edgecombe
 *
 */
public class PlayerResetTask {
	
	public static void execute(Player player) {
		player.resetHits();
		player.getUpdateFlags().reset();
		if (player.isTeleporting()) {
			player.resetTeleportTarget();
			player.setTeleporting(false);
		}
		if (player.isMapRegionChanging()) {
			player.setMapRegionChanging(false);
		}
		if (!player.clientHasLoadedMapRegion()) {
			player.setClientHasLoadedMapRegion();
			player.getVariables().refreshSpawnedObjects();
			player.getVariables().refreshSpawnedItems();
		}
		player.resetCachedUpdateBlock();
		player.reset();
		player.getVariables().setNextForceMovement(null);
	}
}
