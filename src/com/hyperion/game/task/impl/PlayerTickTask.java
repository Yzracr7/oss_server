package com.hyperion.game.task.impl;

import java.util.Queue;

import com.hyperion.game.content.skills.prayer.PrayerData;
import com.hyperion.game.item.Item;
import com.hyperion.game.world.AreaHandler;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.masks.ChatMessage;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Prayers;
import com.hyperion.game.world.entity.player.packets.ItemPackets;
import com.hyperion.game.world.entity.player.packets.NPCPackets;
import com.hyperion.game.world.entity.player.packets.ObjectPackets;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.game.world.grounditems.GroundItem;

/**
 * A task which is executed before an <code>UpdateTask</code>. It is similar to
 * the call to <code>process()</code> but you should use <code>Event</code>s
 * instead of putting timers in this class.
 *
 * @author Graham Edgecombe
 */
public class PlayerTickTask {

    public static void execute(Player player) {
		/*Queue<Packet> packets = player.getVariables().getIncomingPackets();
		while (packets.size() > 0) {
			Packet packet = packets.poll();
			PacketManager.getPacketManager().handle(player, packet);
		}*/
        Queue<ChatMessage> messages = player.getChatMessageQueue();
        if (messages.size() > 0) {
            ChatMessage message = player.getChatMessageQueue().poll();
            player.setCurrentChatMessage(message);
            player.getUpdateFlags().flag(UpdateFlag.CHAT);
        } else {
            player.setCurrentChatMessage(null);
        }
        if (player.getCombatState().isTeleBlocked()) {
            player.getCombatState().deductTbTime(1);
        }
        //Super antipoison timer
        if (player.getAttributes().isSet("super_antipoison_active")) {
            int ticks = player.getAttributes().getInt("super_antipoison_active");
            if (ticks - 1 <= 0) {
                player.getAttributes().remove("super_antipoison_active");
                player.sendMessage("Your superantipoison has worn off.");
            } else {
                player.getAttributes().set("super_antipoison_active", ticks - 1);
            }
        }
        //NPC Aggression Timeout
        if (player.getAttributes().isSet("aggressive_timer")) {
            int ticks = player.getAttributes().getInt("aggressive_timer");
            if (ticks - 1 <= 0) {
                player.getAttributes().remove("aggressive_timer");
            } else
                player.getAttributes().set("aggressive_timer", ticks - 1);
        } else {
            int hits = player.getAttributes().getInt("aggressive_hits");
            if (player.getAttributes().get("aggressive_hits") != null && hits >= 135) {
                player.getAttributes().set("aggressive_timer", 1250);
            }
        }
        if (player.getAttributes().isSet("halveMelee")) {
            int ticks = player.getAttributes().getInt("halveMelee");
            if (ticks - 1 <= 0) {
                player.getAttributes().remove("halveMelee");
                player.playGraphic(Graphic.create(2320));
                player.getPacketSender().sendMessage("Your protection from Melee damage has worn off.");
            } else
                player.getAttributes().set("halveMelee", ticks - 1);
        }
        if (player.getVariables().getFoodTimer() > 0) {
            player.getVariables().setFoodTimer(player.getVariables().getFoodTimer() - 1);
        }
        if (player.getVariables().getPotionTimer() > 0) {
            player.getVariables().setPotionTimer(player.getVariables().getPotionTimer() - 1);
        }
        if (player.getCombatState().getCombatCycles() > 0) {
            player.getCombatState().deductCombatCycles(1);
        }
        if (player.getCombatState().getTarget() != null) {
            player.getFollowing().setFollowing(player.getCombatState().getTarget(), true);
        }
        if (player.getFollowing() != null) {
            if (player.getFollowing().getOther() != null) {
                player.getFollowing().executeFollowing();
            }
        }
        player.processQueuedHits();
        player.getWalkingQueue().processMovement();
        player.updateCoverage(player.getLocation());
        AreaHandler.handleAreas(player);
        if (player.getCombatState().getTarget() != null) {
            MainCombat.executeCombatCycles(player, player.getCombatState().getTarget());
        }
        if (!player.getWalkingQueue().isMoving()) {
            if (player.getAttributes().isSet("objectPacket")) {
                ObjectPackets.handleOption(player);
            } else if (player.getAttributes().isSet("npcPacket")) {
                NPCPackets.handleOptions(player);
            } else if (player.getAttributes().isSet("itemOnObject")) {
                ItemPackets.handleItemOnObject(player, (GameObject) player.getAttributes().get("itemOnObject"), (Item) player.getAttributes().get("itemOnObjectItem"));
            } else if (player.getAttributes().isSet("pickupitem")) {
                ItemPackets.handleItemPickup(player, (GroundItem) player.getAttributes().get("pickupitem"));
            }
        }
        if (player.getInterfaceSettings().isClientFocused() && player.getAttributes().isSet("last_click_received")
                && player.getAttributes().getLong("last_click_received") <= 300000) {
            player.getVariables().incrementPlayTime();
        }
        if (player.getVariables().getExtraExpTimer() > 0) {
            player.getVariables().setExtraExpTimer(player.getVariables().getExtraExpTimer() - 1);
            if (player.getVariables().getExtraExpTimer() % 1000 == 1) {
                player.getPacketSender().sendMessage("You have " + (int) ((player.getVariables().getExtraExpTimer()) * 0.6 / 60) + " minutes of Double XP left.");
            } else if (player.getVariables().getExtraExpTimer() == 0) {
                player.getPacketSender().sendMessage("Your Double XP has run out. You may vote again in 11 hours.");
            }
        }
        player.getVariables().setPacketsExecuted(0);
    }
}
