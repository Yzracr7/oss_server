package com.hyperion.game.task.impl;

import java.util.Iterator;
import java.util.List;

import com.hyperion.game.Constants;
import com.hyperion.game.item.Item;
import com.hyperion.game.item.ItemDefinition.EquipmentType;
import com.hyperion.game.net.Packet;
import com.hyperion.game.net.PacketBuilder;
import com.hyperion.game.world.Loaders;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.ClientModeAnimations;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Appearance;
import com.hyperion.game.world.entity.masks.ChatMessage;
import com.hyperion.game.world.entity.masks.ForceMovement;
import com.hyperion.game.world.entity.masks.UpdateFlags;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.utility.Censor;
import com.hyperion.utility.ProtocolUtils;

/**
 * A task which creates and sends the player update block.
 *
 * @author Graham Edgecombe
 */
public class PlayerUpdateTask {

    public static void execute(Player player) {
        /*
         * If the map region changed sendTeleportTitles the new one.
         * We do this immediately as the client can begin loading it before the
         * actual packet is received.
         */
        if (player.isMapRegionChanging()) {
            player.loadMapRegions();
            if (player.getAttributes().isSet("aggressive_timer") || player.getAttributes().isSet("aggressive_hits")) {
                player.getAttributes().remove("aggressive_timer");
                player.getAttributes().remove("aggressive_hits");
            }
        }

        /*
         * The update block packet holds update blocks and is sendTeleportTitles after the
         * main packet.
         */
        PacketBuilder updateBlock = new PacketBuilder();

        /*
         * The main packet is written in bits instead of bytes and holds
         * information about the local list, players to add and remove,
         * movement and which updates are required.
         */
        PacketBuilder packet = new PacketBuilder(90, Packet.Type.VARIABLE_SHORT);
        packet.startBitAccess();

        /*
         * Updates this player.
         */
        updateThisPlayerMovement(player, packet);
        updatePlayer(player, updateBlock, player, false);

        /*
         * Write the current size of the player list.
         */
        packet.putBits(8, player.getLocalPlayers().size());

        /*
         * Iterate through the local player list.
         */
        for (Iterator<Player> it$ = player.getLocalPlayers().iterator(); it$.hasNext(); ) {
            /*
             * Get the next player.
             */
            Player otherPlayer = it$.next();

            /*
             * If the player should still be in our list.
             */
            if (World.getWorld().containsPlayer(otherPlayer) != -1 && !otherPlayer.isTeleporting() && otherPlayer.getLocation().withinActualDistance(player.getLocation(), 14) && otherPlayer.isVisible()) {
                /*
                 * Update the movement.
                 */
                updatePlayerMovement(packet, otherPlayer);

                /*
                 * Check if an update is required, and if so, sendTeleportTitles the update.
                 */
                if (otherPlayer.getUpdateFlags().isUpdateRequired()) {
                    updatePlayer(player, updateBlock, otherPlayer, false);
                }
            } else {
                /*
                 * Otherwise, remove the player from the list.
                 */
                it$.remove();

                /*
                 * Tell the client to remove the player from the list.
                 */
                packet.putBits(1, 1);
                packet.putBits(2, 3);
            }
        }

        for (int regionId : player.getMapRegionsIds()) {
            List<Integer> indexes = World.getWorld().getRegion(regionId, false).getPlayerIndexes();
            if (indexes == null)
                continue;
            for (int playerIndex : indexes) {
                if (player.getLocalPlayers().size() >= 255) {
                    break;
                }
                Player otherPlayer = World.getWorld().getPlayer(playerIndex);
                if (otherPlayer == null || otherPlayer == player || player.getLocalPlayers().contains(otherPlayer) || !otherPlayer.getLocation().withinActualDistance(player.getLocation(), 14) || !otherPlayer.isVisible()) {
                    continue;
                }
                player.getLocalPlayers().add(otherPlayer);
                addNewPlayer(player, packet, otherPlayer);
                updatePlayer(player, updateBlock, otherPlayer, true);
            }
        }

        /*
         * Check if the update block is not clearAllBoxes.
         */
        if (!updateBlock.isEmpty()) {
            /*
             * Write a magic id indicating an update block follows.
             */
            packet.putBits(11, 2047);
            packet.finishBitAccess();

            /*
             * Add the update block at the end of this packet.
             */
            packet.put(updateBlock.toPacket().getPayload());
        } else {
            /*
             * Terminate the packet normally.
             */
            packet.finishBitAccess();
        }

        /*
         * Write the packet.
         */
        player.getSession().write(packet.toPacket());
    }

    /**
     * Updates a non-this player's movement.
     *
     * @param packet      The packet.
     * @param otherPlayer The player.
     */
    private static void updatePlayerMovement(PacketBuilder packet, Player otherPlayer) {
        /*
         * Check which type of movement took place.
         */
        if (otherPlayer.getSprites().getPrimarySprite() == -1) {
            /*
             * If no movement did, check if an update is required.
             */
            if (otherPlayer.getUpdateFlags().isUpdateRequired()) {
                /*
                 * Signify that an update happened.
                 */
                packet.putBits(1, 1);

                /*
                 * Signify that there was no movement.
                 */
                packet.putBits(2, 0);
            } else {
                /*
                 * Signify that nothing changed.
                 */
                packet.putBits(1, 0);
            }
        } else if (otherPlayer.getSprites().getSecondarySprite() == -1) {
            /*
             * The player moved but didn't run. Signify that an update is
             * required.
             */
            packet.putBits(1, 1);

            /*
             * Signify we moved one tile.
             */
            packet.putBits(2, 1);

            /*
             * Write the primary sprite (i.e. walk direction).
             */
            packet.putBits(3, otherPlayer.getSprites().getPrimarySprite());

            /*
             * Write a flag indicating if a block update happened.
             */
            packet.putBits(1, otherPlayer.getUpdateFlags().isUpdateRequired() ? 1 : 0);
        } else {
            /*
             * The player ran. Signify that an update happened.
             */
            packet.putBits(1, 1);

            /*
             * Signify that we moved two tiles.
             */
            packet.putBits(2, 2);

            /*
             * Write the primary sprite (i.e. walk direction).
             */
            packet.putBits(3, otherPlayer.getSprites().getPrimarySprite());

            /*
             * Write the secondary sprite (i.e. run direction).
             */
            packet.putBits(3, otherPlayer.getSprites().getSecondarySprite());

            /*
             * Write a flag indicating if a block update happened.
             */
            packet.putBits(1, otherPlayer.getUpdateFlags().isUpdateRequired() ? 1 : 0);
        }
    }

    /**
     * Adds a new player.
     *
     * @param packet      The packet.
     * @param otherPlayer The player.
     */
    private static void addNewPlayer(Player player, PacketBuilder packet, Player otherPlayer) {
        /*
         * Write the player index.
         */
        packet.putBits(11, otherPlayer.getIndex());
        /*
         * Calculate the x and y offsets.
         */
        int yPos = otherPlayer.getLocation().getY() - player.getLocation().getY();
        int xPos = otherPlayer.getLocation().getX() - player.getLocation().getX();
        packet.putBits(5, yPos);
        packet.putBits(3, 6);//dir
        packet.putBits(1, 1);
        packet.putBits(1, 1);
        packet.putBits(5, xPos);
    }

    /**
     * Updates this player's movement.
     *
     * @param packet The packet.
     */
    private static void updateThisPlayerMovement(Player player, PacketBuilder packet) {
        /*
         * Check if the player is teleporting.
         */
        if (player.isTeleporting() || player.isMapRegionChanging()) {
            packet.putBits(1, 1);
            packet.putBits(2, 3);
            packet.putBits(2, player.getLocation().getZ());
            packet.putBits(7, player.getLocation().getLocalX(player.getLastKnownRegion()));
            packet.putBits(1, (player.isTeleporting()) ? 1 : 0);
            packet.putBits(7, player.getLocation().getLocalY(player.getLastKnownRegion()));
            packet.putBits(1, player.getUpdateFlags().isUpdateRequired() ? 1 : 0);
        } else {
            /*
             * Otherwise, check if the player moved.
             */
            if (player.getSprites().getPrimarySprite() == -1) {
                /*
                 * The player didn't move. Check if an update is required.
                 */
                if (player.getUpdateFlags().isUpdateRequired()) {
                    /*
                     * Signifies an update is required.
                     */
                    packet.putBits(1, 1);

                    /*
                     * But signifies that we didn't move.
                     */
                    packet.putBits(2, 0);
                } else {
                    /*
                     * Signifies that nothing changed.
                     */
                    packet.putBits(1, 0);
                }
            } else {
                /*
                 * Check if the player was running.
                 */
                if (player.getSprites().getSecondarySprite() == -1) {
                    /*
                     * The player walked, an update is required.
                     */
                    packet.putBits(1, 1);

                    /*
                     * This indicates the player only walked.
                     */
                    packet.putBits(2, 1);

                    /*
                     * This is the player's walking direction.
                     */
                    packet.putBits(3, player.getSprites().getPrimarySprite());

                    /*
                     * This flag indicates an update block is appended.
                     */
                    packet.putBits(1, player.getUpdateFlags().isUpdateRequired() ? 1 : 0);
                } else {
                    /*
                     * The player ran, so an update is required.
                     */
                    packet.putBits(1, 1);

                    /*
                     * This indicates the player ran.
                     */
                    packet.putBits(2, 2);

                    /*
                     * This is the walking direction.
                     */
                    packet.putBits(3, player.getSprites().getPrimarySprite());

                    /*
                     * And this is the running direction.
                     */
                    packet.putBits(3, player.getSprites().getSecondarySprite());

                    /*
                     * And this flag indicates an update block is appended.
                     */
                    packet.putBits(1, player.getUpdateFlags().isUpdateRequired() ? 1 : 0);
                }
            }
        }
    }

    /**
     * Updates a player.
     *
     * @param packet          The packet.
     * @param otherPlayer     The other player.
     * @param forceAppearance The force appearance flag.
     */
    private static void updatePlayer(Player player, PacketBuilder packet, Player otherPlayer, boolean forceAppearance) {
        /*
         * We are updating the the other player's appearance to our client
         */


        /*
         * If no update is required and we don't have to force an appearance
         * update, don't write anything.
         */
        if (!otherPlayer.getUpdateFlags().isUpdateRequired() && !forceAppearance) {
            return;
        }

        /*
         * We can used the cached update block!
         */
        synchronized (otherPlayer) {
            if (otherPlayer.hasCachedUpdateBlock() && otherPlayer != player && !forceAppearance) {
                packet.put(otherPlayer.getCachedUpdateBlock().getPayload().flip());
                return;
            }

            /*
             * We have to construct and cache our own block.
             */
            PacketBuilder block = new PacketBuilder();

            /*
             * Calculate the bitmask.
             */
            int mask = 0;
            final UpdateFlags flags = otherPlayer.getUpdateFlags();
            if (flags.get(UpdateFlag.HIT)) {
                mask |= 0x100;
            }
            if (flags.get(UpdateFlag.GRAPHICS)) {
                mask |= 0x200;
            }
            if (flags.get(UpdateFlag.FACE_ENTITY)) {
                mask |= 0x8;
            }
            if (flags.get(UpdateFlag.FACE_COORDINATE)) {
                mask |= 0x4;
            }
            if (flags.get(UpdateFlag.FORCE_MOVEMENT)) {
                mask |= 0x400;
            }
            if (flags.get(UpdateFlag.FORCED_CHAT)) {
                mask |= 0x80;
            }
            if (flags.get(UpdateFlag.ANIMATION)) {
                mask |= 0x20;
            }
            if (flags.get(UpdateFlag.CHAT)) {
                mask |= 0x1;
            }
            if (flags.get(UpdateFlag.HIT_2)) {
                mask |= 0x2;
            }
            if (flags.get(UpdateFlag.APPEARANCE) || forceAppearance) {
                mask |= 0x40;
            }
            /*
             * Check if the bitmask would overflow a byte.
             */
            if (mask >= 0x100) {
                /*
                 * Write it as a short and indicate we have done so.
                 */
                mask |= 0x10;
                block.put((byte) (mask & 0xff));
                block.put((byte) (mask >> 8));
            } else {
                /*
                 * Write it as a byte.
                 */
                block.put((byte) (mask & 0xFF));
            }

            /*
             * Append the appropriate updates.
             */
            if (flags.get(UpdateFlag.HIT)) {
                appendHitUpdate(otherPlayer, block, player);
            }
            if (flags.get(UpdateFlag.GRAPHICS)) {
                appendGraphicsUpdate(block, otherPlayer);
            }
            if (flags.get(UpdateFlag.FACE_ENTITY)) {
                appendFaceEntityUpdate(otherPlayer, block);
            }
            if (flags.get(UpdateFlag.FACE_COORDINATE)) {
                appendFaceLocationUpdate(otherPlayer, block);
            }
            if (flags.get(UpdateFlag.FORCE_MOVEMENT)) {
                appendForceMovement(player, block, otherPlayer);
            }
            if (flags.get(UpdateFlag.FORCED_CHAT)) {
                appendForceTextUpdate(otherPlayer, block);
            }
            if (flags.get(UpdateFlag.ANIMATION)) {
                appendAnimationUpdate(player, block, otherPlayer);
            }
            if (flags.get(UpdateFlag.CHAT)) {
                boolean censor = player.getDetails().isCensorOn();
                appendChatUpdate(block, otherPlayer, censor);
            }
            if (flags.get(UpdateFlag.HIT_2)) {
                appendHit2Update(otherPlayer, block, player);
            }
            if (flags.get(UpdateFlag.APPEARANCE) || forceAppearance) {
                appendPlayerAppearanceUpdate(player, block, otherPlayer);
            }

            /*
             * Convert the block builder to a packet.
             */
            Packet blockPacket = block.toPacket();

            /*
             * Now it is over, cache the block if we can.
             */
            if (otherPlayer != player && !forceAppearance) {
                otherPlayer.setCachedUpdateBlock(blockPacket);
            }

            /*
             * And finally append the block at the end.
             */
            packet.put(blockPacket.getPayload());
        }
    }

    private static void appendHit2Update(final Player player, final PacketBuilder updateBlock, Player thisPlayer) {
        Hit secondary = player.getSecondaryHit();
        updateBlock.putByteA((byte) secondary.getDamage());
        updateBlock.putByteS((byte) secondary.getType().getType());
        updateBlock.putByteA((byte) secondary.getCombatType());
        updateBlock.putByteA((byte) (secondary.getOwner() == thisPlayer || player == thisPlayer ? 0 : 1));
        updateBlock.putByteA((byte) player.getSkills().getLevel(Skills.HITPOINTS));
        updateBlock.putByteA((byte) player.getSkills().getLevelForXp(Skills.HITPOINTS));
    }

    private static void appendHitUpdate(final Player player, final PacketBuilder updateBlock, Player thisPlayer) {
        Hit primary = player.getPrimaryHit();
        updateBlock.putByteS((byte) primary.getDamage());
        updateBlock.putByteS((byte) primary.getType().getType());
        updateBlock.putByteA((byte) primary.getCombatType());
        updateBlock.putByteA((byte) (primary.getOwner() == thisPlayer || player == thisPlayer ? 0 : 1));
        updateBlock.putByteA((byte) player.getSkills().getLevel(Skills.HITPOINTS));
        updateBlock.putByteA((byte) player.getSkills().getLevelForXp(Skills.HITPOINTS));
    }

    private static void appendFaceEntityUpdate(Player otherPlayer, PacketBuilder updateBlock) {
        Entity interacting = otherPlayer.getInteractingEntity();
        updateBlock.putLEShort(interacting == null ? -1 : interacting.getClientIndex());
    }

    private static void appendForceTextUpdate(Player otherPlayer, PacketBuilder updateBlock) {
        updateBlock.putRS2String(otherPlayer.getForcedChat());
    }

    private static void appendFaceLocationUpdate(Player otherPlayer, PacketBuilder updateBlock) {
        int x = otherPlayer.getFaceLocation().getX();
        int y = otherPlayer.getFaceLocation().getY();
        updateBlock.putLEShortA(x = 2 * x + 1);
        updateBlock.putLEShortA(y = 2 * y + 1);
    }

    /**
     * Appends an animation update.
     *
     * @param block       The update block.
     * @param otherPlayer The player.
     */
    private static void appendAnimationUpdate(Player player, PacketBuilder block, Player otherPlayer) {
        Animation currentAnimation = otherPlayer.getAnimationQueue().getHighestPriority();
        Animation lastAnimation = otherPlayer.getAnimationQueue().getLastAnim();
        if (!Animation.canPerform(lastAnimation, currentAnimation, otherPlayer.getAnimationQueue().getLastAnimTime())) {
            currentAnimation = lastAnimation;
        } else
            otherPlayer.getAnimationQueue().setLastAnim(currentAnimation);

        int animToSend = ClientModeAnimations.getFixedAnimations(player, currentAnimation.getId());
        if (animToSend == -1 || animToSend == 65535)
            block.putLEShort(-1);
        else
            block.putLEShort(Constants.USE_DEV_CACHE ? animToSend : Loaders.animIds[animToSend]);
        block.putByteC((byte) currentAnimation.getDelay());
    }

    /**
     * Appends a graphics update.
     *
     * @param block       The update block.
     * @param otherPlayer The player.
     */
    private static void appendGraphicsUpdate(PacketBuilder block, Player otherPlayer) {
        block.putShort(otherPlayer.getCurrentGraphic().getId());
        block.putInt2(otherPlayer.getCurrentGraphic().getDelay() | otherPlayer.getCurrentGraphic().getHeight() << 16);
    }

    /**
     * Appends a chat text update.
     *
     * @param packet      The packet.
     * @param otherPlayer The player.
     */
    private static void appendChatUpdate(PacketBuilder packet, Player otherPlayer, boolean censor) {
        ChatMessage cm = otherPlayer.getCurrentChatMessage();
        if (censor)
            cm.setChatText(Censor.filterChat(cm.getChatText(), cm.getPlayer()));
        packet.putLEShortA((cm.getColour() & 0xFF) << 8 | cm.getEffect() & 0xFF);
        packet.putByteA((byte) otherPlayer.getDetails().getRights());
        byte[] chatStr = new byte[256];
        chatStr[0] = (byte) cm.getChatText().length();
        int offset = 1 + ProtocolUtils.encryptPlayerChat(chatStr, 0, 1, cm.getChatText().length(), cm.getChatText().getBytes());
        packet.put((byte) offset);
        packet.putReverse(chatStr, 0, offset);
    }

    /**
     * Moves the player.
     *
     * @param packet
     */
    private static void appendForceMovement(Player player, PacketBuilder packet, final Player p) {
        ForceMovement forceMovement = p.getVariables().getNextForceMovement();
        Location lastRegion = player.getLastKnownRegion();
        Location location = p.getLocation();
        int firstSpeed = (forceMovement.getFirstTileTicketDelay());
        int secondSpeed = (forceMovement.getSecondTileTicketDelay());
        int dir = forceMovement.getDirection();
        int firstX = forceMovement.getToFirstTile().getX() - location.getX();
        int firstY = forceMovement.getToFirstTile().getY() - location.getY();
        int secondX = forceMovement.getToSecondTile().getX() - location.getX();
        int secondY = forceMovement.getToSecondTile().getY() - location.getY();
        packet.putByteS((byte) (location.getLocalX(lastRegion) + firstX)); // first
        packet.putByteS((byte) (location.getLocalY(lastRegion) + firstY)); // first
        packet.putByteS((byte) (location.getLocalX(lastRegion) + secondX)); // second
        packet.putByteA((byte) (location.getLocalY(lastRegion) + secondY)); // second
        packet.putShortA(firstSpeed); // speed going to
        packet.putShort(secondSpeed); // speed returning to
        packet.putByteC((byte) dir); // direction
    }

    /**
     * Appends an appearance update.
     *
     * @param packet      The packet.
     * @param otherPlayer The player.
     */
    private static void appendPlayerAppearanceUpdate(Player player, PacketBuilder packet, Player otherPlayer) {
        PacketBuilder playerProps = new PacketBuilder();
        Appearance app = otherPlayer.getAppearance();
        playerProps.put((byte) app.getGender());
        playerProps.put((byte) otherPlayer.getPrayers().getPkIcon());
        playerProps.put((byte) otherPlayer.getPrayers().getHeadIcon());
        if (!app.isInvisible()) {
            if (!app.isNpc()) {
                for (int i = 0; i < 4; i++) {
                    if (otherPlayer.getEquipment() != null && otherPlayer.getEquipment().getItemInSlot(i) != -1) {
                        playerProps.putShort((short) 512 + otherPlayer.getEquipment().getSlot(i).fetch_render());
                    } else {
                        playerProps.put((byte) 0);
                    }
                }
                if (otherPlayer.getEquipment() != null && otherPlayer.getEquipment().getItemInSlot(4) != -1) {
                    playerProps.putShort((short) 512 + otherPlayer.getEquipment().getSlot(4).fetch_render());
                } else {
                    playerProps.putShort((short) 0x100 + app.getLook(2));
                }
                if (otherPlayer.getEquipment() != null && otherPlayer.getEquipment().getItemInSlot(5) != -1) {
                    playerProps.putShort((short) 512 + otherPlayer.getEquipment().getSlot(5).fetch_render());
                } else {
                    playerProps.put((byte) 0);
                }
                Item chest = otherPlayer.getEquipment().getSlot(4);
                if (otherPlayer.getEquipment() != null && chest != null) {
                    if (!chest.getDefinition().matchesEquipmentType(EquipmentType.PLATEBODY)) {
                        playerProps.putShort((short) 0x100 + app.getLook(3));
                    } else {
                        playerProps.put((byte) 0);
                    }
                } else {
                    playerProps.putShort((short) 0x100 + app.getLook(3));
                }
                if (otherPlayer.getEquipment() != null && otherPlayer.getEquipment().getItemInSlot(7) != -1) {
                    playerProps.putShort((short) 512 + otherPlayer.getEquipment().getSlot(7).fetch_render());
                } else {
                    playerProps.putShort((short) 0x100 + app.getLook(5));
                }
                Item hat = otherPlayer.getEquipment().getSlot(0);
                if (otherPlayer.getEquipment() != null && hat != null) {
                    if (!hat.getDefinition().matchesEquipmentType(EquipmentType.FULL_HELM) && !hat.getDefinition().matchesEquipmentType(EquipmentType.FULL_MASK)) {
                        playerProps.putShort((short) 0x100 + app.getLook(0));
                    } else {
                        playerProps.put((byte) 0);
                    }
                } else {
                    playerProps.putShort((short) 0x100 + app.getLook(0));
                }
                if (otherPlayer.getEquipment() != null && otherPlayer.getEquipment().getItemInSlot(9) != -1) {
                    playerProps.putShort((short) 512 + otherPlayer.getEquipment().getSlot(9).fetch_render());
                } else {
                    playerProps.putShort((short) 0x100 + app.getLook(4));
                }
                if (otherPlayer.getEquipment() != null && otherPlayer.getEquipment().getItemInSlot(10) != -1) {
                    playerProps.putShort((short) 512 + otherPlayer.getEquipment().getSlot(10).fetch_render());
                } else {
                    playerProps.putShort((short) 0x100 + app.getLook(6));
                }
                if (otherPlayer.getEquipment() != null && hat != null) {
                    if (!hat.getDefinition().matchesEquipmentType(EquipmentType.FULL_MASK)) {
                        playerProps.putShort((short) 0x100 + app.getLook(1));
                    } else {
                        playerProps.put((byte) 0);
                    }
                } else {
                    playerProps.putShort((short) 0x100 + app.getLook(1));
                }
            } else {
                playerProps.putShort(-1);
                playerProps.putShort(app.getNpcId());
            }
        } else {
            for (int i = 0; i < 12; i++) {
                playerProps.put((byte) 0);
            }
        }
        for (int colour : app.getColoursArray()) {
            playerProps.put((byte) colour);
        }
        int animToSend = ClientModeAnimations.getFixedAnimations(player, otherPlayer.getVariables().getStandAnimation());
        playerProps.putShort((short) (Constants.USE_DEV_CACHE ? animToSend : Loaders.animIds[animToSend]));//stand
        animToSend = ClientModeAnimations.getFixedAnimations(player, otherPlayer.getVariables().getTurnAnimation());
        playerProps.putShort((short) (Constants.USE_DEV_CACHE ? animToSend : Loaders.animIds[animToSend]));//stand turn
        animToSend = ClientModeAnimations.getFixedAnimations(player, otherPlayer.getVariables().getWalkAnimation());
        playerProps.putShort((short) (Constants.USE_DEV_CACHE ? animToSend : Loaders.animIds[animToSend]));//walk
        animToSend = ClientModeAnimations.getFixedAnimations(player, otherPlayer.getVariables().getTurn180Animation());
        playerProps.putShort((short) (Constants.USE_DEV_CACHE ? animToSend : Loaders.animIds[animToSend]));//turn 180
        animToSend = ClientModeAnimations.getFixedAnimations(player, otherPlayer.getVariables().getTurn90Clockwise());
        playerProps.putShort((short) (Constants.USE_DEV_CACHE ? animToSend : Loaders.animIds[animToSend]));//turn 90 cw
        animToSend = ClientModeAnimations.getFixedAnimations(player, otherPlayer.getVariables().getTurn90CounterClockwise());
        playerProps.putShort((short) (Constants.USE_DEV_CACHE ? animToSend : Loaders.animIds[animToSend]));//turn 90 ccw
        animToSend = ClientModeAnimations.getFixedAnimations(player, otherPlayer.getVariables().getRunAnimation());
        playerProps.putShort((short) (Constants.USE_DEV_CACHE ? animToSend : Loaders.animIds[animToSend]));//run
        playerProps.putRS2String(otherPlayer.getName());
        playerProps.putRS2String(otherPlayer.getVariables().getTitle());
        playerProps.putShort((short) (otherPlayer.getVariables().isRankHidden() ? 1 : 0));
        playerProps.put((byte) otherPlayer.getSkills().getCombatLevel()); // combat level
        playerProps.putShort(0); // (skill-level instead of combat-level) otherPlayer.getSkills().getTotalLevel()); // total level
        Packet propsPacket = playerProps.toPacket();
        packet.putByteA((byte) propsPacket.getLength());
        packet.put(propsPacket.getPayload());
    }
}
