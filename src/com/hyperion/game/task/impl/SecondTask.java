package com.hyperion.game.task.impl;

import com.hyperion.game.GameEngine;
import com.hyperion.game.content.minigames.pestcontrol.PestWaiting;
import com.hyperion.game.task.Task;

public class SecondTask implements Task {

	@Override
	public void execute(GameEngine context) {
		PestWaiting.displayBoatSettings();
	}
}
