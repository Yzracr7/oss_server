package com.hyperion.game.tickable.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormatSymbols;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.hyperion.game.Constants;
import com.hyperion.game.discord.Discord;
import com.hyperion.game.tickable.Tickable;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Apr 3, 2015
 */
public class BackupGeneratorTick extends Tickable {

    /**
     * @param ticks
     */
    public BackupGeneratorTick() {
        super(100);
    }

    /*
     * (non-Javadoc)
     *
     * @see com.hyperion.game.tickable.Tickable#execute()
     */
    @Override
    public void execute() {
        try {
            long timeFromFile = getTimeFromFile();
            /**
             * Checking to see if there should be a backup generated
             */
            if (timeFromFile == -1 || TimeUnit.MILLISECONDS.toHours(System.currentTimeMillis() - getTimeFromFile()) >= 24) {
                PrintWriter writer;
                backupCharacters();
                try {
                    writer = new PrintWriter(getBackupFile());
                    writer.print(System.currentTimeMillis());
                    writer.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                System.out.println("Generated a backup!");
                Discord.speak("```Generated player files backup archive...```");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the long time from the file
     *
     * @return A {@code Long} {@code Object}
     */
    private long getTimeFromFile() {
        long time = -1;
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(getBackupFile()));
            String line;
            while ((line = reader.readLine()) != null) {
                time = Long.parseLong(line);
                break;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return time;
    }

    /**
     * Zips the directory to a .zip file
     *
     * @param folder  The folder to zip
     * @param archive The zip file
     * @throws IOException Exception thrown
     */
    private void zipDirectory(File folder, File archive) throws IOException {
        ZipOutputStream stream = new ZipOutputStream(new FileOutputStream(archive));
        zip(folder, folder, stream);
        stream.close();
    }

    @SuppressWarnings("deprecation")
    private void backupCharacters() {
        //File characters = new File(SQLWorldLoader.ACCOUNTS_DIRECTORY);
        String name = "" + DateFormatSymbols.getInstance().getMonths()[new Date().getMonth()] + "/" + new Date().getDate();
        File archive = new File(Constants.PLAYERS_FILE_PATH + "backups/" + name + ".zip");
        if (!archive.getParentFile().exists())
            archive.getParentFile().mkdirs();
        if (!archive.exists()) {
            try {
                //if (characters.list().length == 0) {
                //System.out.println("[Auto-Backup] The characters folder is clearAllBoxes.");
                //return;
                //}
                //zipDirectory(characters, archive);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Zips a directory into an archive
     *
     * @param directory The directory to zip to
     * @param base      The base folder
     * @param zos       The outputstream
     * @throws IOException Exception thrown
     */
    private void zip(File directory, File base, ZipOutputStream zos) throws IOException {
        File[] files = directory.listFiles();
        byte[] buffer = new byte[8192];
        int read = 0;
        for (File file2 : files) {
            if (file2.isDirectory()) {
                zip(file2, base, zos);
            } else {
                FileInputStream in = new FileInputStream(file2);
                ZipEntry entry = new ZipEntry(file2.getPath().substring(base.getPath().length() + 1));
                zos.putNextEntry(entry);
                while (-1 != (read = in.read(buffer))) {
                    zos.write(buffer, 0, read);
                }
                in.close();
            }
        }
    }

    /**
     * @return the backupFile
     */
    public static File getBackupFile() {
        File file = BACKUP_FILE;
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return BACKUP_FILE;
    }

    private static final File BACKUP_FILE = new File(Constants.FILES_PATH + "backups/backup.txt");

}
