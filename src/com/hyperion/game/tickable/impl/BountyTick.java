package com.hyperion.game.tickable.impl;

import com.hyperion.game.content.wilderness.BountyHunter;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.TeleportAreaLocations;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;

public class BountyTick extends Tickable {

	/**
	 * The delay in ticks (600ms)
	 */
	private static final int DELAY = 30;


	/**
	 * Sets the server to run event to run every x ticks.
	 */
	public BountyTick() {
		super(DELAY);
	}
	
	
	//TODO:
	//Killstreak - announce to the world
	//Can't get bounty from same player twice
	//Same IP also doesn't drop anything
	//Extra EP in hotzones - save ep variable
	//Fix inter sprite and more transparent
	
	//Shop
	//Add container to normal shops with yellow text to show points - thats about it

	@Override
	public void execute() {
		for (final String name : World.getWorld().getPlayersInWilderness()) {
			if (name == null)
				continue;
			Player player = World.getWorld().find_player_by_name(name);
			// Check if player is not in wildy and remove -- unless they just
			// left and are still under the 2 minutes timer
			if (player == null
					|| (!TeleportAreaLocations.isInPVP(player) && !player.getAttributes()
							.isSet("wildyTimer"))) {
				World.getWorld().getPlayersInWilderness().remove(name);
				World.getWorld().getPossibleTargets().remove(name);
				continue;
			}

			// If timer is set, check if player re-entered.
			//If so remove timer, and continue with updating.
			//If still in safe-zone, check if reducing timer <= 0 and if so reset their target if not in wilderness
			if (player.getAttributes().isSet("wildyTimer")) {
				if (TeleportAreaLocations.isInPVP(player)) {
					player.getAttributes().remove("wildyTimer");
				} else {
					if (player.getAttributes().getInt("wildyTimer") - DELAY <= 0) {
						player.getAttributes().remove("wildyTimer");
						//Not in PVP
						if(player.getAttributes().isSet("target"))
							player.getAttributes().remove("target");
						World.getWorld().getPossibleTargets().remove(player.getName());
						World.getWorld().getPlayersInWilderness().remove(player.getName());
						player.getPacketSender().sendMessage("Your Wilderness target has been removed.");
						player.getPacketSender().sendHintArrow(null);
					} else {
						player.getAttributes().set("wildyTimer", player.getAttributes().getInt("wildyTimer") - DELAY);
					}
					continue;
				}
			}

			// Add EP
			// TODO: Hot zones

			// Find target/update target info
			if (!player.getAttributes().isSet("target")) {
				BountyHunter.findTarget(player);
				World.getWorld().getPossibleTargets().add(player.getName());
			} else {
				Player other = World.getWorld().find_player_by_name(player.getAttributes().get("target"));

				if (other != null) {
					// Check if the other is still in wildy -- or has timer
					if (!TeleportAreaLocations.isInPVP(other) && !other.getAttributes().isSet("wildyTimer")) {
						BountyHunter.targetLeft(player, false);
						continue;
					}
					BountyHunter.sendInterfaceDetails(player, other);
				} else
					BountyHunter.targetLeft(player, false);
			}
		}

	}
}