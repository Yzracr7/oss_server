package com.hyperion.game.tickable.impl;

import com.hyperion.game.Constants;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.region.MapBuilder;
import com.hyperion.game.world.region.Region;
import com.hyperion.utility.NumberUtils;

/**
 * A Tick which runs periodically and performs tasks such as garbage collection.
 * 
 * @author Graham Edgecombe
 *
 */
public class CleanupTick extends Tickable {

	/**
	 * The delay in milliseconds between consecutive cleanups.
	 */
	private static int CLEANUP_CYCLE_TIME = 500;

	/**
	 * Creates the cleanup event to run every 5 minutes.
	 */
	public CleanupTick() {
		super(CLEANUP_CYCLE_TIME);
	}

	@Override
	public void execute() {
		boolean force = (Runtime.getRuntime().freeMemory() < Constants.MIN_FREE_MEM_ALLOWED);
		if (force) {
			skip: for (Region region : World.getWorld().getRegionManager().getRegions().values()) {
				for (int regionId : MapBuilder.FORCE_LOAD_REGIONS)
					if (regionId == region.getId())
						continue skip;
				region.unloadMap();
			}
		}
		System.gc();
		System.runFinalization();
		CLEANUP_CYCLE_TIME = NumberUtils.random(500, 1000);
	}

}
