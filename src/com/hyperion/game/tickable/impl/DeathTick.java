package com.hyperion.game.tickable.impl;

import com.hyperion.game.Constants;
import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.content.minigames.Settings;
import com.hyperion.game.content.pets.BossPetDropHandler;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.Killcount;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.impl.wilderness.Vet_ion;
import com.hyperion.game.world.entity.player.Player;

public class DeathTick extends Tickable {

	private Entity deadEntity;
	private Entity lastAttacker;
	private int counter;

	public DeathTick(Entity deadEntity, Entity lastHitter) {
		super(1);
		this.counter = deadEntity.isPlayer() ? 4 : 4;
		this.deadEntity = deadEntity;
		this.lastAttacker = lastHitter;
		immediateRemoval();
	}

	@Override
	public void execute() {
		if (this.counter > 0) {
			this.counter--;
			return;
		}
		this.stop();
		if (!this.deadEntity.getCombatState().isDead()) {
			return;
		}
		if (this.deadEntity.isNPC()) {
			final NPC npc = (NPC) this.deadEntity;
			if (npc.getId() == 6611) {
				boolean firstForm = !npc.getAttributes().isSet("vetionsecondform");
				if (firstForm) {
					npc.playForcedChat("Do it again!!");
					npc.resetVariables();
					npc.getCombatState().setDead(false);
					npc.getAttributes().set("vetionsecondform", true);
					npc.getAttributes().remove("guardsSummoned");
					npc.setTransformationId(6612);
					return;
				} else {
					npc.animate(Animation.create(npc.getDeathAnimation(), AnimationPriority.HIGH));
					npc.getAttributes().remove("vetionsecondform");
					npc.getAttributes().remove("guardsSummoned");
					npc.setTransformationId(-1);
				}
			} else if (npc.getId() == 6615) {
				npc.getAttributes().remove("guardsSummoned");
			} else if (npc.getId() == 6613 || npc.getId() == 6614) {
				Vet_ion.guardsSpawned--;
				World.getWorld().removeNPC(npc);
			} else if (npc.getId() == 6617) {
				World.getWorld().removeNPC(npc);
			} else if (npc.getId() == 1158) {
				boolean firstForm = !npc.getAttributes().isSet("kqsecondform");
				if (firstForm) {
					npc.setVisible(false);
					World.getWorld().send_regional_graphic(npc.getCentreLocation(), 1055);
					World.getWorld().submit(new Tickable(5) {
						@Override
						public void execute() {
							this.stop();
							MainCombat.endCombat(npc, 1);
							npc.resetVariables();
							npc.getCombatState().setDead(false);
							npc.getAttributes().set("kqsecondform", true);
							npc.getAttributes().set("stopCombat", true);
							npc.setTransformationId(1160);
							npc.setDefinition(1160);
							npc.setVisible(true);
							npc.animate(Animation.create(6270, AnimationPriority.HIGH));
							npc.playGraphic(1055);
							World.getWorld().submit(new Tickable(11) {
								@Override
								public void execute() {
									this.stop();
									npc.getAttributes().remove("stopCombat");
								}
							});
						}
					});
					return;
				} else if (!firstForm) { // 2nd form
					npc.getAttributes().remove("kqsecondform");
					npc.setDefinition(1158);
					npc.setTransformationId(-1);
				}
			}
		}
		/*
		 * The killer of this entity.
		 */
		Entity killer = null;
		if (this.deadEntity.getCombatState().getDamageMap().highestDamage() != null && !this.deadEntity.isDestroyed())
			killer = this.deadEntity.getCombatState().getDamageMap().highestDamage();
		else if (lastAttacker != null)
			killer = lastAttacker;
		else
			killer = this.deadEntity;

		if (Settings.handleDeath(this.deadEntity, killer)) { // died in a minigame?
			return;
		}

		if (killer.isPlayer()) {

			if (lastAttacker != null && lastAttacker.isPlayer()
					&& ((Player) killer).getName().equalsIgnoreCase(((Player) lastAttacker).getName()))
				killer = lastAttacker;

			Player player = (Player) killer;
			if (this.deadEntity.isNPC()) {
				final NPC npc = (NPC) this.deadEntity;
				Killcount.handleKill(player, npc);
				BossPetDropHandler.roll(player, npc);
			}
			if (this.deadEntity.isPlayer() && !killer.equals(this.deadEntity)) {
				player.getPacketSender().sendMessage(
						((Player) this.deadEntity).getDetails().getName() + " regrets the day they met you in combat.");
				Achievements.increase(player, 5);
			}
		}
		if (this.deadEntity.isPlayer()) {
			final Player player = (Player) this.deadEntity;
			player.getAttributes().set("last_death", System.currentTimeMillis());
			if (this.lastAttacker != null && !this.lastAttacker.equals(killer)) {
				if (this.lastAttacker.isPlayer() && killer.isPlayer()) {
					Player lastAtker = (Player) this.lastAttacker;
					Player realKiller = (Player) killer;
					if (!realKiller.equals(lastAtker) && !realKiller.equals(this.deadEntity)
							&& realKiller.getName() != lastAtker.getName()) {
						lastAtker.getPacketSender().sendMessage("You killed " + player.getDetails().getName() + " but "
								+ realKiller.getDetails().getName() + " did more damage so they got the drop.");
						Achievements.increase(player, 5);
					}
				}
			}
			player.getPacketSender().sendMessage("Oh dear, you are dead!");
			player.getVariables().setLastLocation(Constants.RESPAWN_LOCATION);
			// player.getPacketSender().sendConfig(428, 0); // poison
			Achievements.increase(player, 4);
		}
		this.deadEntity.teleport(this.deadEntity.getDeathArea());
		if (this.deadEntity.isNPC() && ((NPC) deadEntity).getId() == 2042) {
			this.deadEntity.dropLoot(killer);
			this.deadEntity.dropLoot(killer);
		} else {
			this.deadEntity.dropLoot(killer);
		}
		resetEntity(killer);
		if (this.deadEntity.isPlayer()) {
			final Player player = (Player) this.deadEntity;
			player.getPacketSender().sendInterface(153, false);
		}
	}

	private void resetEntity(Entity killer) {
		if (this.deadEntity.isNPC()) {
			NPC npc = (NPC) this.deadEntity;
			boolean should_unregister = npc.getDefinition().getRespawnTime() == -1;
			if (should_unregister) {
				World.getWorld().removeNPC(npc);
				return;
			} else {
				this.deadEntity.setVisible(false);
				/*
				 * if (TeleportAreaLocations.atBandosChamber(npc.getLocation())) {
				 * World.getWorld().removeNPC(npc);
				 * ChamberRooms.increase_chamber_deaths(npc.getId(), npc.getLocation()); return;
				 * }
				 */
				World.getWorld().submit(
						new Tickable(this.deadEntity, ((NPC) this.deadEntity).getDefinition().getRespawnTime()) {
							@Override
							public void execute() {
								this.stop();
								deadEntity.animate(-1);
								deadEntity.setVisible(true);
								deadEntity.getCombatState().setDead(false);
							}
						});
			}
		} else if (this.deadEntity.isPlayer()) {
			Player player = (Player) this.deadEntity;
			World.getWorld().unregister_player_npcs(player);
		}
		this.deadEntity.resetVariables();
		MainCombat.endCombat(deadEntity, 1);
		if (this.deadEntity.isPlayer()) {
			Player player = (Player) this.deadEntity;
			player.getPacketSender().sendInterface(153, false);

		}
	}

	/**
	 * Stuff that gets reset right away.
	 */
	private void immediateRemoval() {
		if (this.deadEntity.isPlayer()) {
			((Player) this.deadEntity).resetActionAttributes();
		} else if (this.deadEntity.isNPC()) {
			if (this.lastAttacker != null) {
				if (this.lastAttacker.isPlayer()) {
					((Player) this.lastAttacker).getCombatState().setPreviousHit(0);
					((Player) this.lastAttacker).getCombatState().setNpcLastAttacked(0);
				}
			}
		}
	}
}