package com.hyperion.game.tickable.impl;

import com.hyperion.game.discord.Discord;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.utility.NumberUtils;

public class DiscordSlaveTick extends Tickable {

    /**
     * The delay in ticks (600ms)
     */
    private static final int DELAY = 15000;

    /**
     * Sets the server to run event to run every x ticks.
     */
    public DiscordSlaveTick() {
        super(DELAY);
    }

    public String message() {
        int random = NumberUtils.random(4);
        switch (random) {
            case 1:
                return "```All our past updates are listed right on our forum! http://smite.io/forum/index.php?/forum/27-updates/```";
            case 2:
                return "```There are currently " + World.getWorld().getPlayerCount() + " players online in our alpha beta. To play visit http://smite.io/play.html and find the oskit download.```";
            case 3:
                return "```There are currently " + World.getWorld().getPlayersInWilderness() + " player in the wilderness.```";
        }
        return "```Remember to like and bump our thread to get us noticed! https://www.rune-server.ee/runescape-development/rs-503-client-server/projects/667427-smite-enticing-oldschool-experience.html```";
    }

    @Override
    public void execute() {
        Discord.speak(message());
    }
}