package com.hyperion.game.tickable.impl;

import com.hyperion.Logger;
import com.hyperion.game.content.grandexchange.inter.GrandExchangeMainInterface;
import com.hyperion.game.content.grandexchange.offer.BuyingLimitation;
import com.hyperion.game.content.grandexchange.offer.GrandExchangeOffer;
import com.hyperion.game.content.grandexchange.resources.GEResourceManager;
import com.hyperion.game.content.grandexchange.offer.OfferState;
import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Map;

public class GrandExchangeMap extends Tickable {

    /**
     * If the database should be dumped.
     */
    private static boolean dumpDatabase;

    /**
     * The delay in ticks (600ms)
     */
    private static final int DELAY = 35;

    /**
     * The mapping of all current offers.
     */
    private static final Map<Long, GrandExchangeOffer> OFFER_MAPPING = new HashMap<>();

    /**
     * The update notification.
     */
    public static final String UPDATE_NOTIFICATION = "One or more of your grand exchange offers have been updated.";

    boolean initiated = false;

    /**
     * Sets the server to run event to run every x ticks.
     */
    public GrandExchangeMap() {
        super(DELAY);
        if (!initiated) {
            init();
            initiated = true;
        }
    }


    @Override
    public void execute() {
        for (GrandExchangeOffer offer : OFFER_MAPPING.values()) {
            //System.out.println("[GE] Tick! [" + offer.getPlayerUID() + "] [Offered Item=" + ItemDefinition.forId(offer.getItemId()).getName() + "] [Offer Price=" + offer.getOfferedValue() + "] [General Price=" + ItemDefinition.forId(offer.getItemId()).getGeneralPrice() + "]");
            if (offer.isActive() && offer.isLimitation()) {
                updateOffer(offer);
            }
        }
        BuyingLimitation.clear();

        if (dumpDatabase) {
            com.hyperion.game.task.TaskExecutor.execute(() -> {
                synchronized (GrandExchangeMap.this) {
                    dump("./data/");
                }
            });
            dumpDatabase = false;
        }
    }

    /**
     * Updates the offer.
     *
     * @param offer The G.E. offer to update.
     */
    public static void updateOffer(GrandExchangeOffer offer) {
        System.out.println("Updating offers...");
        if (!offer.isActive())
            return;
        for (GrandExchangeOffer o : OFFER_MAPPING.values()) {
            if (o.isSell() != offer.isSell() && o.getItemId() == offer.getItemId() && o.isActive()) {
                exchange(offer, o);
                if (offer.getState() == OfferState.COMPLETED)
                    break;
            }
        }

        if (offer.getState() != OfferState.COMPLETED) {
            for (GrandExchangeOffer o : GEResourceManager.getStock()) {
                if (o.isSell() != offer.isSell() && o.getItemId() == offer.getItemId() && o.isActive()) {
                    exchange(offer, o);
                    if (offer.getState() == OfferState.COMPLETED)
                        break;
                }
            }
        }
    }

    /**
     * Dispatches an offer.
     *
     * @param player The player.
     * @param offer  The grand exchange offer.
     * @return {@code True} if successful.
     */
    public static boolean dispatch(Player player, GrandExchangeOffer offer) {
        if (offer.getAmount() < 1) {
            player.sendMessage("You must choose the quantity you wish to buy!");
            return false;
        }
        if (offer.getOfferedValue() < 1) {
            player.sendMessage("You must choose the price you wish to buy for!");
            return false;
        }
        if (!offer.isSell() && player.getInventory().getItemAmount(995) < offer.getOfferedValue()) {
            player.sendMessage("You dont have enough coins to complete this offer.");
            return false;
        } else if (offer.isSell() && player.getInventory().getItemAmount(offer.getItemId()) < offer.getAmount()) {
            player.sendMessage("You dont have enough to offer that amount.");
            return false;
        }
        if (offer.getState() != OfferState.PENDING || offer.getUid() != 0) {
            return false;
        }
        offer.setPlayer_name(player.getName());
        offer.setPlayerUID(player.getUid());
        offer.setUid(nextUID());
        player.getBound_offer_uids()[player.getGrandeExchange().getOpenedOfferIndex()] = offer.getUid();
        offer.setState(OfferState.REGISTERED);
        OFFER_MAPPING.put(offer.getUid(), offer);
        offer.setTimeStamp(System.currentTimeMillis());
        dumpDatabase = true;
        dump("./data/");
        System.out.println("Grand exchange offer has been dispatched " + OFFER_MAPPING.values().size());
        player.getGrandeExchange().setTemporaryOffer(null);
        if (!offer.isSell()) {
            player.getInventory().deleteItem(995, offer.getOfferedValue() * offer.getAmount());
        } else {
            player.getInventory().deleteItem(offer.getItemId(), offer.getAmount());
        }
        GrandExchangeMainInterface.open(player, false);
        return true;
    }

    /**
     * Exchanges between 2 offers.
     *
     * @param offer The grand exchange offer to update.
     * @param o     The other offer to exchange with.
     */
    private static void exchange(GrandExchangeOffer o, GrandExchangeOffer offer) {
        if (o.isSell() == offer.isSell())
            return;
        if ((offer.isSell() && o.getOfferedValue() < offer.getOfferedValue()) || (!offer.isSell() && o.getOfferedValue() > offer.getOfferedValue()))
            return;
        int amount = offer.getAmountLeft(true);
        if (amount > o.getAmountLeft(true))
            amount = o.getAmountLeft(true);
        if (amount < 1)
            return;
        int coinDifference = offer.isSell() ? (o.getOfferedValue() - offer.getOfferedValue()) : (offer.getOfferedValue() - o.getOfferedValue());
        if (coinDifference < 0)
            return;
        // if (EconomyManagement.getEcoState() == EcoStatus.DRAINING)
        //   coinDifference *= (1.0 - EconomyManagement.getModificationRate());
        offer.setCompletedAmount(offer.getCompletedAmount() + amount);
        o.setCompletedAmount(o.getCompletedAmount() + amount);
        offer.setState(offer.getAmountLeft() < 1 ? OfferState.COMPLETED : OfferState.UPDATED);
        o.setState(o.getAmountLeft() < 1 ? OfferState.COMPLETED : OfferState.UPDATED);
        if (offer.isSell()) {
            offer.addWithdraw(995, amount * offer.getOfferedValue());
            o.addWithdraw(o.getItemId(), amount);
            BuyingLimitation.updateBoughtAmount(o.getItemId(), o.getPlayerUID(), amount);
        }
        if (!offer.isSell()) {
            offer.addWithdraw(offer.getItemId(), amount);
            o.addWithdraw(995, amount * o.getOfferedValue());
            BuyingLimitation.updateBoughtAmount(offer.getItemId(), offer.getPlayerUID(), amount);
        }
        // if (coinDifference > 0)
        //addCoinDifference(offer, o, coinDifference, amount);
        //offer.getEntry().influenceValue(offer.getOfferedValue());
        Player plr_1 = World.getPlayer(offer.getPlayer_name());
        Player plr_2 = World.getPlayer(o.getPlayer_name());

        if (plr_1 != null) {
            plr_1.sendMessage(UPDATE_NOTIFICATION);
            if (plr_1.getVariables().isUsing_grand_exchange() && plr_1.getAttributes().get("grandexchangemenu") != null && plr_1.getAttributes().get("grandexchangemenu").equals(671)) {
                System.out.println("[P1] bewbewbewbewbebwebwebwew");
            }
        }
        if (plr_2 != null) {
            plr_2.sendMessage(UPDATE_NOTIFICATION);
            if (plr_2.getVariables().isUsing_grand_exchange() && plr_2.getAttributes().get("grandexchangemenu") != null && plr_2.getAttributes().get("grandexchangemenu").equals(671)) {
                System.out.println("[P2] bewbewbewbewbebwebwebwew");
            }
        }

        dumpDatabase = true;
    }

    /**
     * The database path.
     */
    private static final String DB_PATH = "exchange/offer_dispatch_db.tre";

    /**
     * The offset of the offer UIDs.
     */
    private static long offsetUID = 1;

    /**
     * Initializes the Grand Exchange.
     */

    public static void init() {
        File file = new File("./data/" + DB_PATH);
        //dump("./data/");
        if (!file.exists()) {
            System.err.println("[GEOfferDispatch]: Could not locate database! [path=" + file.getAbsolutePath() + "]");
            return;
        }
        try (RandomAccessFile raf = new RandomAccessFile(file, "rw"); FileChannel c = raf.getChannel()) {
            ByteBuffer b = c.map(FileChannel.MapMode.READ_WRITE, 0, c.size());
            offsetUID = b.getLong();
            long uid;
            while ((uid = b.getLong()) != 0) {
                int itemId = b.getShort();
                boolean sale = b.get() == 1;
                GrandExchangeOffer offer = new GrandExchangeOffer(itemId, sale);
                offer.setUid(uid);
                offer.setAmount(b.getInt());
                offer.setCompletedAmount(b.getInt());
                offer.setOfferedValue(b.getInt());
                offer.setTimeStamp(b.getLong());
                offer.setState(OfferState.values()[b.get()]);
                offer.setTotalCoinExchange(b.getInt());
                offer.setPlayerUID(b.getInt());
                int idx = -1;
                while ((idx = b.get()) != -1)
                    offer.getWithdraw()[idx] = new Item(b.getShort(), b.getInt());
                OFFER_MAPPING.put(uid, offer);
            }
            Logger.getInstance().warning("Loaded Grand Exchange Offer Map [" + OFFER_MAPPING.values().size() + "]");
            raf.close();
            c.close();
        } catch (Throwable t) {
            t.printStackTrace();
        }
        GEResourceManager.init();
    }

    /**
     * Dumps the grand exchange offers.
     *
     * @param directory The directory to save to.
     */
    public static void dump(String directory) {
        File file = new File(directory + DB_PATH);
        ByteBuffer b = ByteBuffer.allocate(50_000_000);
        b.putLong(offsetUID);
        for (long uid : OFFER_MAPPING.keySet()) {
            GrandExchangeOffer offer = OFFER_MAPPING.get(uid);
            if (offer == null)
                continue;
            b.putLong(uid);
            b.putShort((short) offer.getItemId());
            b.put((byte) (offer.isSell() ? 1 : 0));
            b.putInt(offer.getAmount());
            b.putInt(offer.getCompletedAmount());
            b.putInt(offer.getOfferedValue());
            b.putLong(offer.getTimeStamp());
            b.put((byte) offer.getState().ordinal());
            b.putInt(offer.getTotalCoinExchange());
            b.putInt(offer.getPlayerUID());
            for (int i = 0; i < 2; i++) {
                Item item;
                if ((item = offer.getWithdraw()[i]) != null) {
                    b.put((byte) i);
                    b.putShort((short) item.getId());
                    b.putInt(item.getCount());
                }
            }
            b.put((byte) -1);
        }
        b.putLong(0);
        try (RandomAccessFile raf = new RandomAccessFile(file, "rw"); FileChannel c = raf.getChannel()) {
            b.flip();
            c.write(b);
            raf.close();
            c.close();
        } catch (Throwable t) {
            t.printStackTrace();
        }
        GEResourceManager.dump(directory);
    }

    /**
     * Gets the offer for the given UID.
     *
     * @param uid The unique ID given to the offer.
     * @return The grand exchange offer.
     */
    public static GrandExchangeOffer forUID(long uid) {
        return OFFER_MAPPING.get(uid);
    }

    /**
     * Gets the next UID.
     *
     * @return The UID.
     */
    private static long nextUID() {
        long id = offsetUID++;
        if (id == 0)
            return nextUID();
        return id;
    }

}