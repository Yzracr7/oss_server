package com.hyperion.game.tickable.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.sql.SQLManager;

public class MinuteTick extends Tickable {

	/**
	 * The delay in milliseconds between healing.
	 */
	private static final int DELAY = 100;

	/**
	 * Sets the server to run event to run every 60 seconds.
	 */
	public MinuteTick() {
		super(DELAY);
	}

	@Override
	public void execute() {
		for (final Player player : World.getWorld().getPlayers()) {
			if (player == null)
				continue;
			player.heal(1);
			for (int i = 0; i < Skills.SKILL_COUNT; i++) {
				if (player.getSkills().getLevel(i) > player.getSkills().getLevelForXp(i)) {
					player.getSkills().decrementLevel(i);
				} else if (player.getSkills().getLevel(i) < player.getSkills().getLevelForXp(i)) {
					if(i != Skills.HITPOINTS && i != Skills.PRAYER)
						player.getSkills().incrementLevel(i);
				}
			}
			if (player.getSettings().isSkulled() && !player.getCombatState().isDead()) {
				player.getSettings().setSkullCycles(player.getSettings().getSkullCycles() - 1);
			}
			if (player.getAttributes().isSet("cyclopsroom")) {
				if (player.getInventory().hasItemAmount(8851, 20)) {
					player.getInventory().deleteItem(8851, 20);
					player.getPacketSender().sendMessage("20 of your tokens crumble away.");
				} else {
					player.getAttributes().remove("cyclopsroom");
					MainCombat.endCombat(player, 1);
					Entity other = player.getCombatState().getCurrentAttacker();
					if (other != null) {
						MainCombat.endCombat(other, 1);
					}
					player.teleport(2845, 3540, 2);
					player.getPacketSender().sendNPCHead(4289, 241, 2);
					player.getPacketSender().modifyText("Kamfreena", 241, 3);
					player.getPacketSender().modifyText("You've run out of tokens.", 241, 4);
					player.getPacketSender().animateInterface(9847, 241, 2);
					player.getPacketSender().sendChatboxInterface(241);
					player.getAttributes().set("nextDialogue", -1);
				}
			}
			// the last time the player clicked on the game, in seconds
			if(player.getAttributes().isSet("last_click_received")) {
				long lastClickTime = System.currentTimeMillis() - player.getAttributes().getLong("last_click_received");
				if (lastClickTime <= 300000) { //300 seconds - 5 mins
					int minsActive = player.getAttributes().isSet("mins_active") ? player.getAttributes().getInt("mins_active") : 0;
					player.getAttributes().set("mins_active", minsActive + 1);
					if (minsActive >= 10) {
						player.getVariables().incrementLoyaltyPoints();
						player.getAttributes().set("mins_active", 0);
					}
				} else {
					player.getAttributes().set("mins_active", 0);
				}
			}
		}
		SQLManager.processQuery();
	}
}