package com.hyperion.game.tickable.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.entity.npc.NPC;

public class NPCRespawn extends Tickable {

	private NPC npc;
	
	public NPCRespawn(NPC npc, int ticks) {
		super(ticks);
		this.npc = npc;
	}
	
	@Override
	public void execute() {
		npc.setVisible(true);
		this.stop();
	}
}
