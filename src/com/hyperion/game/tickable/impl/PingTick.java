package com.hyperion.game.tickable.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.sql.login.LoginManager;

public class PingTick extends Tickable {

	/**
	 * The delay in milliseconds between healing.
	 */
	private static final int DELAY = 30;

	/**
	 * Sets the server to run event to run every 30 game cycles(18 seconds)
	 */
	public PingTick() {
		super(DELAY);
	}

	public void execute() {
		for (String name : World.getWorld().getPingMap().keySet()) {
			if(World.getWorld().getPingMap().get(name) == null) {
				continue;
			}
			long lastPing = (long)  World.getWorld().getPingMap().get(name);
			if (System.currentTimeMillis() - lastPing > 10000) {
				Player p = World.getWorld().find_player_by_name(name);
				if (p != null) {
					if(!p.getAttributes().isSet("sessionConnected"))
						continue;
					boolean canLog = !(System.currentTimeMillis() - p.getCombatState().getLogoutTimer() < 10000);
					if(!canLog)
						continue;
					if(p.getAttributes().isSet("forcelogout"))
						continue;
					if (p.getSession() == null || !p.getSession().isConnected()) {
						System.out.println("Kicked null session player in ping tick "
								+ p.getName());
						LoginManager.unregister_player(p);

						 World.getWorld().getPingMap().remove(name);
						continue;
					}

					// p.getPacketSender().forceLogout();
					System.out.println("PushMessage player kick 10 seconds: " + p.getName());
					LoginManager.unregister_player(p);
					World.getWorld().getPingMap().remove(name);
					 /*if (World.getWorld().removePlayer(p) != -1) {
						System.out.println("Kicked player " + name
								+ " for 20 seconds w/o ping.");
						
					} else {
						// LoginManager.unregister_player(p);
						System.out
								.println("Could not kick "
										+ name
										+ " as player is null. Tried LoginManager.unregister..");
					}*/
				}
			}
		}
	}
}
