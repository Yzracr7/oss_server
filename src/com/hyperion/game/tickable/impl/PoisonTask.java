package com.hyperion.game.tickable.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;

public class PoisonTask extends Tickable {

    private Entity target;

    public PoisonTask(Entity target, int amount) {
        super(50 + NumberUtils.random(50));
        this.target = target;
        initialize(amount, true);
    }

    public PoisonTask(Entity target, int amount, boolean message) {
        super(50 + NumberUtils.random(50));
        this.target = target;
        initialize(amount, message);
    }

    private void initialize(int poisonAmount, boolean message) {
        if (target.isPlayer()) {
            if ((Player) target.getAttributes().get("super_antipoison_active") != null){

            }
            if (message) {
                ((Player) target).getPacketSender().sendMessage("You have been poisoned!");
            }
            ((Player) target).getPacketSender().sendConfig(428, 1);
        }
        target.getCombatState().setPoisonAmount(poisonAmount);
    }

    @Override
    public void execute() {
        // TODO Auto-generated method stub
        if (!target.getCombatState().isPoisoned() || target.getCombatState().isDead()) {
            stop();
            return;
        }
        if (target.isPlayer()) {
            ((Player) target).getInterfaceSettings().closeInterfaces(true);
        }
        target.inflictDamage(new Hit(target.getCombatState().getPoisonAmount(), -1), true);
        if (NumberUtils.random(200) >= 100) {
            target.getCombatState().setPoisonAmount(target.getCombatState().getPoisonAmount() - 1);
        }
        if (NumberUtils.random(10) == 0) {
            target.getCombatState().setPoisonAmount(0);
            if (target.isPlayer())
                ((Player) target).getPacketSender().sendConfig(428, 0);
            stop();
            return;
        }
        this.setTickDelay(50 + NumberUtils.random(50));
    }
}
