package com.hyperion.game.tickable.impl;

import java.util.List;
import java.util.ListIterator;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.utility.json.JsonManager;
import com.hyperion.utility.json.impl.PunishmentLoader;
import com.hyperion.utility.json.sub.Punishment;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class PunishmentTick extends Tickable {

	public PunishmentTick() {
		super(TICK_PROCESS_TIME);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.game.tickable.Tickable#execute()
	 */
	@Override
	public void execute() {
		synchronized (LOCK_OBJECT) {
			if (!JsonManager.loaded) {
				return;
			}
			if (loader == null) {
				loader = JsonManager.getSingleton().getLoader(PunishmentLoader.class);
			}
			if (loader == null) {
				throw new RuntimeException("Could not save punishments! PunishmentLoader was null");
			}
			List<Punishment> punishments = loader.getPunishments();
			ListIterator<Punishment> it$ = punishments.listIterator();
			boolean updated = false;
			while (it$.hasNext()) {
				Punishment punishment = it$.next();
				if (punishment.getKey() == null || punishment.getKey().equalsIgnoreCase("null") || punishment.isComplete()) {
					updated = true;
					punishment.onRemove();
					it$.remove();
					System.out.println("Removing punishment: " + punishment);
				}
			}
			if (updated) {
				loader.save(punishments);
				loader.initialize();
			}
		}
	}
	
	/**
	 * The instance of the punishment loader
	 */
	private PunishmentLoader loader;

	/**
	 * The amount of ticks (600ms intervals) between processes for this class
	 */
	private static final int TICK_PROCESS_TIME = 100;

	/**
	 * The synchronization object
	 */
	public static final Object LOCK_OBJECT = new Object();

}
