package com.hyperion.game.tickable.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.entity.player.Player;

public class RestingTask extends Tickable {
	
	private static final int REST_ANIMATION = 11786,
			STAND_UP_ANIMATION = 11788, ENERGY_INCREASE = 4;
	private Player player;
	
	public RestingTask(Player p) {
		super(1); //tick = 600ms
		this.player = p;
		initialize();
	}

	private void initialize() {
		player.getWalkingQueue().reset();
		player.getPacketSender().clearMapFlag();
		player.animate(REST_ANIMATION);
		player.getAttributes().set("stopActions", true);
		player.getAttributes().set("isResting", true);
	}
	
	private void stopResting() {
		player.getAttributes().remove("isResting");
		player.getAttributes().remove("stopActions");
		//player.animate(STAND_UP_ANIMATION);
		player.getPacketSender().softCloseInterfaces();
	}

	private int ticks = 0;
	
	@Override
	public void execute() {
		// TODO Auto-generated method stub
		if (player.getCombatState().isDead()) {
			player.getAttributes().remove("isResting");
			player.getAttributes().remove("stopActions");
			this.stop();
			return;
		}
		/*if(player.getAttributes().isSet("stopActions"))
			player.getAttributes().remove("stopActions");*/
		if(!player.getAttributes().isSet("isResting")) {
			stopResting();
			this.stop();
			return;
		}
		ticks++;
		if(ticks > 10) {
			player.animate(REST_ANIMATION);
			ticks = 0;
		}
		if(player.getVariables().getRunningEnergy() < 100) {
			int total = (int) (player.getVariables().getRunningEnergy() + ENERGY_INCREASE);
			if (total > 100) {
				total = 100;
			}
			player.getVariables().setRunningEnergy(total, true);
		}
		this.setTickDelay(1);
	}
}
