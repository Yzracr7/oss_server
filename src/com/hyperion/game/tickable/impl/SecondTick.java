package com.hyperion.game.tickable.impl;

import com.hyperion.game.content.minigames.pestcontrol.PestWaiting;
import com.hyperion.game.tickable.Tickable;

public class SecondTick extends Tickable {

	public SecondTick() {
		super(2);
	}

	@Override
	public void execute() {
		PestWaiting.displayBoatSettings();
	}

}
