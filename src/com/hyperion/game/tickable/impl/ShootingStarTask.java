package com.hyperion.game.tickable.impl;

import java.util.List;

import com.hyperion.game.Constants;
import com.hyperion.game.content.clanchat.ClanChat;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.utility.NumberUtils;

public class ShootingStarTask extends Tickable {

	public static void main(String[] args) {
		for (Location location : STAR_LOCATIONS) {
			System.out.println(location + "\t" + getNameForLocation(location));
		}
	}

	private int time, health = 0;
	private GameObject gameObject;
	private boolean found = false;

	private static final int[] STAR_ID = { 38668, 38667, 38666, 38665, 38664, 38663, 38662, 38661, 38660 };

	public ShootingStarTask() {
		super(1);
		int index = STAR_ID[NumberUtils.random(STAR_ID.length - 1)];
		Location location = getRandomLocation();
		System.out.println("Spawning Shooting star - id: " + index + " at location: " + location.getX() + ", " + location.getY());
		gameObject = new GameObject(location, index, 10, 0);
		health = gameObject.getHealth();
		World.spawnObject(gameObject);
		for (Player p : World.getWorld().getPlayers()) {
			if (p != null) {
				p.getPacketSender().sendMessage("<col=E65D02>A Shooting Star has crashed near " + getNameForLocation(gameObject.getLocation()) + "!");
			}
		}
	}

	private static final Location[] STAR_LOCATIONS = { Location.create(3117, 3522, 0), Location.create(3100, 3500, 0), Location.create(3031, 3347, 0), Location.create(2974, 3238, 0), Location.create(3246, 3503, 0), Location.create(2461, 3427, 0), Location.create(3340, 3268, 0), Location.create(3087, 3962, 0), Location.create(2527, 3868, 0), Location.create(3190, 3706, 0), Location.create(3061, 3888, 0), Location.create(3048, 3949, 0), Location.create(2705, 3334, 0), Location.create(2925, 3339, 0) };

	private static final String[] LOCATIONS = { "Edgeville", "Edgeville", "Falador", "Rimmington", "Varrock", "Tree Gnome Village", "Al Kharid", "Mage Bank", "Miscellania", "Graveyard of Shadows", "Lava Maze", "Pirate's Hideout", "Legend's Guild", "Falador" };

	private Location getRandomLocation() {
		return STAR_LOCATIONS[NumberUtils.random(STAR_LOCATIONS.length - 1)];
	}

	private static String getNameForLocation(Location l) {
		for (int i = 0; i < STAR_LOCATIONS.length; i++) {
			if (STAR_LOCATIONS[i] == l) {
				return LOCATIONS[i];
			}
		}
		return "Edgeville";
	}

	private void removeStar() {
		World.getWorld();
		World.removeObject(this.gameObject);
	}

	private void foundStar(Player player) {
		found = true;
		int starXp = (player.getSkills().getLevel(Skills.MINING) * 5) * Constants.NON_COMBAT_EXP_MULTIPLIER;
		player.getSkills().addExperience(Skills.MINING, starXp);
		player.getPacketSender().sendChatboxDialogue(true, "Congratulations, you were the first to find this star!", "You receive " + starXp + " Mining XP as a reward.");
	}

	private void checkPlayersFound() {
		List<Integer> playerIndexes = World.getWorld().getRegion(gameObject.getLocation().getRegionId(), false).getPlayerIndexes();
		if (playerIndexes == null) {
			return;
		}
		for (Integer playerIndex : playerIndexes) {
			Player target = World.getWorld().getPlayer(playerIndex);
			if (target == null)
				return;
			if (target.getCombatState().isDead()) {
				return;
			}
			if (target.getLocation().distanceToPoint(gameObject.getLocation()) < 3) {
				target.reset();
				target.getWalkingQueue().reset();
				target.animate(-1);
				if (target.getAttributes().isSet("miningRock"))
					target.getAttributes().remove("miningRock");
				foundStar(target);
			}
		}
	}

	private void checkPlayersMining() {
		List<Integer> playerIndexes = World.getWorld().getRegion(gameObject.getLocation().getRegionId(), false).getPlayerIndexes();
		if (playerIndexes == null) {
			return;
		}
		for (Integer playerIndex : playerIndexes) {
			Player target = World.getWorld().getPlayer(playerIndex);
			if (target == null)
				return;
			if (target.getCombatState().isDead()) {
				return;
			}
			if (target.getLocation().distanceToPoint(gameObject.getLocation()) < 3) {
				target.reset();
				target.resetActionAttributes();
				target.getWalkingQueue().reset();
				target.animate(-1);
				if (target.getAttributes().isSet("miningRock"))
					target.getAttributes().remove("miningRock");
				target.getPacketSender().sendChatboxDialogue(true, "The crashed star has degraded!");
			}
		}
	}

	@Override
	public void execute() {
		if (!found) {
			checkPlayersFound();
		}

		health = gameObject.getHealth(); // update current status

		if (health <= 0) {
			time = 999999; // remove star
		}

		if (time < 500) {
			time++;
		} else {
			checkPlayersMining();
			gameObject.setHealth(0);
			removeStar();
			checkPlayersMining();
			final int nextStar = NumberUtils.random(3000, 18000);
			// 1 hour = 6000
			System.out.println("Next star in: " + nextStar + " ticks(600ms)");
			String time = nextStar > 12000 ? "approximately 2.5 hours" : nextStar > 6000 ? "approximately 1 hour" : "less than an hour";
			for (Player p : World.getWorld().getPlayers()) {
				if (p != null) {
					p.getPacketSender().sendMessage("<col=E65D02>The shooting star has turned to dust.");
					p.getPacketSender().sendMessage("<col=E65D02>The next one is predicted to appear in " + time + "!");
				}
			}
			ClanChat.save();
			System.gc();
			World.getWorld().submit(new Tickable(nextStar) {
				@Override
				public void execute() {
					ClanChat.save();
					World.getWorld().submit(new ShootingStarTask());
					this.stop();
				}
			});
			this.stop();
		}
	}
}
