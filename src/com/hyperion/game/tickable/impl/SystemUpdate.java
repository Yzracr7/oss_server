package com.hyperion.game.tickable.impl;

import com.hyperion.game.content.clanchat.ClanChat;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.sql.login.LoginManager;

public class SystemUpdate extends Tickable {

    private int displayTime = 100;

    public SystemUpdate(int time) {
        super(1);
        this.displayTime = time * 50 / 30;
    }

    @Override
    public void execute() {
        for (Player p : World.getWorld().getPlayers()) {
            if (p != null) {
                p.getPacketSender().sendSystemUpdate(displayTime);
            }
        }
        if (displayTime > 0)
            displayTime--;
        if (displayTime <= 0) {
            this.stop();
            for (Player p : World.getWorld().getPlayers()) {
                if (p != null) {
                    p.getPacketSender().forceLogout();
                    LoginManager.unregister_player(p);
                }
            }
            ClanChat.save();
            System.exit(-1);
        }
    }
}
