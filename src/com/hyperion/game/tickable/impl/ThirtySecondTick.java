package com.hyperion.game.tickable.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;

public class ThirtySecondTick extends Tickable {

	/**
	 * The delay in milliseconds between healing.
	 */
	private static final int DELAY = 50;

	/**
	 * Sets the server to run event to run every 60 seconds.
	 */
	public ThirtySecondTick() {
		super(DELAY);
	}

	@Override
	public void execute() {
		for (final Player player : World.getWorld().getPlayers()) {
			if (player == null)
				continue;
			if (player.getSettings().getSpecialAmount() < 100) {
				player.getSettings().increaseSpecialAmount(10);
				if (player.getSettings().getSpecialAmount() > 100) {
					player.getSettings().setSpecialAmount(100, false);
				}
				player.getSettings().setSpecialAmount(player.getSettings().getSpecialAmount(), true);
			}
		}
	}
}