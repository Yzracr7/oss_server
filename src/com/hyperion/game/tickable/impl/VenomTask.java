package com.hyperion.game.tickable.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;

public class VenomTask extends Tickable {

    private Entity target;

    public VenomTask(Entity target, int amount) {
        super(12);
        this.target = target;
        initialize(amount, true);
    }

    public VenomTask(Entity target, int amount, boolean message) {
        super(12);
        this.target = target;
        initialize(amount, message);
    }

    private void initialize(int poisonAmount, boolean message) {
        if (target.isPlayer()) {
            if ((Player) target.getAttributes().get("anti_venom") != null) {
            } else {
                if (message) {
                    ((Player) target).getPacketSender().sendMessage("You have been venomized!");
                }
            }
            ((Player) target).getPacketSender().sendConfig(428, 1);
        }
        target.getCombatState().setVenomAmount(poisonAmount);
    }

    @Override
    public void execute() {
        // TODO Auto-generated method stub
        System.out.println("Exectuting venom");
        if (!target.getCombatState().isVenomized() || target.getCombatState().isDead()) {
            stop();
            return;
        }
        if (target.isPlayer()) {
            ((Player) target).getInterfaceSettings().closeInterfaces(true);
        }

        target.inflictDamage(new Hit(target.getCombatState().getVenomAmount(), -1), true);
        if (target.getCombatState().getVenomAmount() < 20) {
            target.getCombatState().setVenomAmount(target.getCombatState().getVenomAmount() + 2);
        }

        this.setTickDelay(12 + NumberUtils.random(3));
    }
}
