package com.hyperion.game.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.world.entity.player.Skills.Skill;

public class ConverterTool {

	private static Map<Integer, ItemDefinition> definitions = null;
	public static ItemList[] itemList = new ItemList[16000];

	public static void convertToXml() {
		BufferedWriter e = null;
		try {
		    try {
			e = new BufferedWriter(new FileWriter("./data/tools/itemDefinitions.txt", true));
			e.write("<list>");
			e.newLine();
			for (ItemList list : itemList) {
				if (list != null) {
					e.write("\t<itemDefinition>");
					e.newLine();
					e.write("\t\t<id>" + list.id + "</id>");
					e.newLine();
					e.write("\t\t<name>" + list.name + "</name>");
					e.newLine();
					if (list.examine.equalsIgnoreCase("Swap this note at any bank for the equivalent item.")) {
						e.write("\t\t<examine>null</examine>");
						e.newLine();
					} else {
						e.write("\t\t<examine>" + list.examine + "</examine>");
						e.newLine();
					}
					e.write("\t\t<members>" + list.members + "</members>");
					e.newLine();
					e.write("\t\t<equipSlot>" + list.equipSlot + "</equipSlot>");
					e.newLine();
					e.write("\t\t<notedId>" + list.notedId + "</notedId>");
					e.newLine();
					e.write("\t\t<parentId>" + list.parentId + "</parentId>");
					e.newLine();
					e.write("\t\t<noteable>" + list.noteable + "</noteable>");
					e.newLine();
					e.write("\t\t<noted>" + list.noted + "</noted>");
					e.newLine();
					e.write("\t\t<twoHanded>" + list.twoHanded + "</twoHanded>");
					e.newLine();
					e.write("\t\t<wearable>" + list.wearable + "</wearable>");
					e.newLine();
					e.write("\t\t<destroyable>" + list.destroyable + "</destroyable>");
					e.newLine();
					e.write("\t\t<buryable>" + list.buryable + "</buryable>");
					e.newLine();
					e.write("\t\t<playerBound>" + list.playerBound + "</playerBound>");
					e.newLine();
					e.write("\t\t<equipId>" + list.equipId + "</equipId>");
					e.newLine();
					e.write("\t\t<renderId>" + list.renderId + "</renderId>");
					e.newLine();
					e.write("\t\t<sellPrice>" + list.sellPrice + "</sellPrice>");
					e.newLine();
					e.write("\t\t<buyPrice>" + list.buyPrice + "</buyPrice>");
					e.newLine();
					e.write("\t\t<bonuses>");
					e.newLine();// bonuses speak here
					for (int i = 0; i < 13; i++) {
						e.write("\t\t\t<int>" + list.bonuses[i] + "</int>");
						e.newLine();
					}
					e.write("\t\t</bonuses>");
					e.newLine();// bonuses end here
					e.write("\t\t<skillRequirements>");
					e.newLine();// reqs speak here
					for (Skill skill : list.getSkillRequirements().keySet()) {
						int lvl = list.getSkillRequirement(Skill.skillForId(skill.getId()).getId());
						e.write("\t\t\t<entry>");
						e.newLine();// speak here
						e.write("\t\t\t\t<skillRequirement>" + skill.toString() + "</skillRequirement>");
						e.newLine();
						e.write("\t\t\t\t<int>" + lvl + "</int>");
						e.newLine();
						e.write("\t\t\t</entry>");
						e.newLine();// speak here

					}
					e.write("\t\t</skillRequirements>");
					e.newLine();// reqs end here
					e.write("\t</itemDefinition>");
					e.newLine();
				}
			}
			e.write("</list>");
			e.flush();
		    } finally {
			if (e != null) {
				try {
					e.close();
				} catch (IOException e2) {
				}
			}
		}
		} catch (IOException d) {
			d.printStackTrace();
		}
	}

	public static void rename() {
		File item = new File("data/tools/itemDefinitions.txt");
		item.renameTo(new File("data/tools/itemDefinitions.xml"));
		System.out.println("Changed To XML File.");
	}

	public static void main(String[] args) {
		/*
		 * try { Cache.init(); } catch (IOException e1) { // TODO Auto-generated
		 * catch block e1.printStackTrace(); } try { ItemDefinition.load();
		 * definitions = ItemDefinition.getDefinitions(); for (ItemDefinition
		 * def : definitions.values()) { if (def != null) { int id =
		 * def.getId(); CachedItemDefinition cachedDef =
		 * CachedItemDefinition.forId(id); String name = def.getName(); String
		 * examine = def.getExamine(); boolean members = def.isMembers(); int
		 * equipSlot = def.getEquipSlot(); int notedId = def.getNotedId(); int
		 * parentId = def.getParentId(); boolean noteable = def.isNoteable();
		 * boolean noted = def.isNoted(); boolean twoHanded = def.isDoubleHanded();
		 * boolean wearable = false; if (cachedDef != null) { wearable =
		 * cachedDef.isWearItem(); } boolean destroyable = false; if (cachedDef
		 * != null) { destroyable = cachedDef.isDestroyItem(); } boolean
		 * buryable = false; if (cachedDef != null) { buryable =
		 * cachedDef.isBuryItem(); } boolean playerBound = def.isPlayerBound();
		 * int equipId = def.getEquipId(); int renderId = def.getRenderId(); int
		 * sellPrice = def.getSellPrice(); int buyPrice = def.getBuyPrice();
		 * int[] bonuses = def.getBonuses(); if (id == -1) { continue; }
		 * itemList[id] = new ItemList(id); itemList[id].id = id;
		 * itemList[id].name = name; itemList[id].examine = examine;
		 * itemList[id].members = members; itemList[id].equipSlot = equipSlot;
		 * itemList[id].notedId = notedId; itemList[id].parentId = parentId;
		 * itemList[id].noteable = noteable; itemList[id].noted = noted;
		 * itemList[id].twoHanded = twoHanded; itemList[id].wearable = wearable;
		 * itemList[id].destroyable = destroyable; itemList[id].buryable =
		 * buryable; itemList[id].playerBound = playerBound;
		 * itemList[id].equipId = equipId; itemList[id].renderId = renderId;
		 * itemList[id].sellPrice = sellPrice; itemList[id].buyPrice = buyPrice;
		 * itemList[id].bonuses = bonuses; } } } catch (IOException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); } try {
		 * loadMemberItems(); } catch (IOException e) { // TODO Auto-generated
		 * catch block e.printStackTrace(); } loadEquipIds();
		 * loadItemRequirements(); convertToXml(); rename();
		 */
	}

	private static void loadItemRequirements() {
		String line = "", token = "", token2 = "", token2_2 = "", token3[] = new String[10];
		boolean EndOfFile = false;
		BufferedReader file = null;
		int itemId = 0;
		try {
			file = new BufferedReader(new FileReader("./data/tools/item/itemrequirements.txt"));
		} catch (FileNotFoundException fileex) {
			return;
		}
		try {
			line = file.readLine();
		} catch (IOException ioexception) {
			if (file != null)
				try {
					file.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			return;
		}
		while (EndOfFile == false && line != null) {
			line = line.trim();
			int spot = line.indexOf(":");
			if (spot > -1) {
				token = line.substring(0, spot);
				token = token.trim();
				token2 = line.substring(spot + 1);
				token2 = token2.trim();
				token2_2 = token2.replaceAll("\t\t", "\t");
				token2_2 = token2.replaceAll("\t\t", "\t");
				token2_2 = token2.replaceAll("\t\t", "\t");
				token2_2 = token2.replaceAll("\t\t", "\t");
				token2_2 = token2.replaceAll("\t\t", "\t");
				token2_2 = token2.replaceAll("\t\t", "\t");
				token2_2 = token2.replaceAll("\t\t", "\t");
				token3 = token2_2.split("\t");
				if (token.contains("item")) {
					itemId = Integer.parseInt(token3[0]);
				}
				if (token.contains("skills")) {
					String[] skill = token3[0].split(";");
					ItemDefinition def = definitions.get(itemId);
					if (def != null) {
						for (int i = 0; i < skill.length; i++) {
							String[] skillId = skill[i].split(":");
							int skillReq = Integer.parseInt(skillId[0]);
							int lvlReq = Integer.parseInt(skillId[1]);
							if (skillReq >= 0 && lvlReq > 0) {
								/*
								 * if (itemList[def.getId()] != null) {
								 * itemList[
								 * def.getId()].skillRequirements.put(Skill
								 * .skillForId(skillReq), lvlReq); }
								 */
							}
						}
					}
				}
			} else {
				if (line.equals("[ENDOFLIST]")) {
					try {
						file.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			try {
				line = file.readLine();
			} catch (IOException ioexception1) {
				EndOfFile = true;
			}
		}
		try {
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return;
	}

	public static void loadMemberItems() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader("./data/tools/item/memberitems.txt"));
		try {
			String line = br.readLine();
			while (line != null) {
				String[] array = line.split(":");
				if (array[1] != null) {
					int item = Integer.parseInt(array[1]);
					ItemList def = itemList[item];
					if (def != null) {
						def.members = true;
					}
				}
				line = br.readLine();
			}
		} finally {
			br.close();
		}
	}

	public static void loadEquipIds() {
		String line = "", token = "", token2 = "", token2_2 = "", token3[] = new String[10];
		BufferedReader list = null;
		try {
			list = new BufferedReader(new FileReader("./data/tools/item/592equips.txt"));
			line = list.readLine().trim();
		} catch (Exception e) {
			e.printStackTrace();
		}
		while (line != null) {
			int spot = line.indexOf(":");
			if (spot > -1) {
				token = line.substring(0, spot).trim();
				token2 = line.substring(spot + 1).trim();
				token2_2 = token2.replaceAll("\t\t", "\t");
				token2_2 = token2_2.replaceAll("\t\t", "\t");
				token2_2 = token2_2.replaceAll("\t\t", "\t");
				token3 = token2_2.split("\t");
				if (token.equals("ItemId")) {
					int item = Integer.parseInt(token3[0]);
					int equip = Integer.parseInt(token3[1]);
					ItemList def = itemList[item];
					if (def != null) {
						def.equipId = equip;
					}
				}
			} else {
				if (line.equals("[ENDOFEQUIPLIST]")) {
					try {
						list.close();
					} catch (Exception exception) {
					}
					list = null;
					return;
				}
			}
			try {
				line = list.readLine().trim();
			} catch (Exception exception1) {
				try {
					list.close();
				} catch (Exception exception) {
				}
				list = null;
				return;
			}
		}
	}
}
