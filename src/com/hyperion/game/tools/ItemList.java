package com.hyperion.game.tools;

import java.util.HashMap;
import java.util.Map;

import com.hyperion.game.world.entity.player.Skills.Skill;

public class ItemList {

	public int id;
	public String name;
	public String examine;
	public boolean members;
	public int equipSlot = -1;
	public int notedId = -1;
	public int parentId = -1;
	public boolean noteable;
	public boolean noted;
	public boolean twoHanded;
	public boolean wearable;
	public boolean destroyable;
	public boolean buryable;
	public boolean playerBound;
	public int equipId;
	public int renderId;
	public int sellPrice;
	public int buyPrice;
	public int[] bonuses = new int[14];
	private Map<Skill, Integer> skillRequirements = new HashMap<Skill, Integer>();

	/**
	 * Gets the weapons skill requirements.
	 * 
	 * @return The weapons skill requirements.
	 */
	public Map<Skill, Integer> getSkillRequirements() {
		return skillRequirements;
	}

	/**
	 * Gets the weapons skill requirements.
	 * 
	 * @param index
	 *            The skill index.
	 * @return The weapons skill requirements.
	 */
	public int getSkillRequirement(int index) {
		return skillRequirements.get(Skill.skillForId(index));
	}

	public ItemList(int id) {
		this.id = id;
	}
}
