package com.hyperion.game.tools;

public class WikiDumper {

	/*
	private static boolean replaceExamines = false;
	public static void main(String[] args) throws Exception {
		loadAllFiles();
		if(replaceExamines) {
			replaceExamines();
			writeNew("replacedExamines.txt");
			System.exit(0);
		}
		for (int i = 10000; i < Loaders.getItemDefinitions().length; i++) {
			ItemDefinition def = Loaders.getItemDefinitions()[i];
			String examine;
			if (def == null) {
				examine = i + ":" + "It's a null.";
				examines.add(examine);
				continue;
			}
			String name = def.getName();
			String existingExamine = def.getExamine();
			//System.out.println("def examine: " + existingExamine);
			if(!existingExamine.contains("It's a " + name) && !existingExamine.contains("It's an " + name)) {
				examines.add(i + ":" + existingExamine);
				continue;
			}
			//examine = getExamine("http://2007.runescape.wikia.com/wiki/", name).trim();
			
			//if(examine == null) {
				examine = getExamine("http://runescape.wikia.com/wiki/", name).trim();
			//}
			
				
			if (examine != null && examine != ("null") && !examine.contains("Unknown edit")) {
				examine = i + ":" + examine;
			} else {
				if(name != null && name != "null") {
					boolean isVowel = false;
					for (String vowel : TextUtils.vowels) {
						if(name != null && name.trim().charAt(0) == vowel.toUpperCase().toCharArray()[0]) {
							isVowel = true;
							//System.out.println("Vowel " +vowel.toCharArray()[0] + " in name " + name );
						} //else {
							//System.out.println("Not vowel: " + name.trim().charAt(0) + " in name " + name);
						//}
					}
					examine = i + ":" + "It's " + (isVowel ? "an" : "a") + " " + name + ".";
				} else {
					examine = i + ":" + "It's a null.";
				}
				
			}
			examines.add(examine);
			System.out.println(examine);
		}
		write("examines.txt");
		System.exit(0);
	}

	private static void loadAllFiles() throws Exception {
		Cache.init();
		Loaders.loadItemDefinitions();
		NPCDefinition.init();
	}

	private static List<String> examines = new ArrayList<String>();
	
	private static void replaceExamines() {
		try {
			BufferedReader buf = new BufferedReader(new FileReader("./newExamines.txt"));
			String line = "";
			try {
				while ((line = buf.readLine()) != null) {
					if(line.contains(":")) {
						String[] split = line.split(":");
						int id = Integer.parseInt(split[0]);
						System.out.println("ID: " + id);
						if(line.equalsIgnoreCase(id + ":"))
							continue;
						String examine = split[1];
						if(Loaders.getItemDefinitions()[id] != null) {
							if(examine != null)
								Loaders.getItemDefinitions()[id].setExamine(examine);
							else 
								Loaders.getItemDefinitions()[id].setExamine("It's a null.");
						}
					}
					//itemExamines[id] = examine;
				}
			} finally {
				buf.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void writeNew (String filename) throws IOException {
		  BufferedWriter outputWriter = null;
		  outputWriter = new BufferedWriter(new FileWriter(filename));
		  for(ItemDefinition def : Loaders.getItemDefinitions()) {
			  if(def != null) {
		    outputWriter.write(def.getId() + ":" + def.getExamine());
		    outputWriter.newLine();
			  }
		  }
		  outputWriter.flush();  
		  outputWriter.close();  
		}
	
	private static String getExamine(String urlString, String name) {
		String examine = "null";
		try {
			URL url = new URL(urlString
					+ name.replaceAll(" ", "_"));
			URLConnection con = url.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String line;
			int next = 0;
			while ((line = in.readLine()) != null) {
				if(urlString.contains("2007")) {
				if (line.contains("<td colspan=\"2\" style=\"padding:3px 7px 3px 7px; line-height:140%; text-align:center;\"> ")
						/*|| line.contains("<th style=\"white-space: nowrap;\"><a href=\"/wiki/Examine\" title=\"Examine\">Examine</a></th><td> \"")
						&& !line.contains("Unknown <small>")) {
					examine = line.replace("<td colspan=\"2\" style=\"padding:3px 7px 3px 7px; line-height:140%; text-align:center;\"> ",
									"").replace("<th style=\"white-space: nowrap;\"><a href=\"/wiki/Examine\" title=\"Examine\">Examine</a></th><td> \"", "").replace("<i>X</i> dose", getWithinParentheses(name) != null ? getWithinParentheses(name) + " dose": "Multiple dose").replace("X dose", getWithinParentheses(name) != null ? getWithinParentheses(name) + " dose" : "Multiple dose").
									replace("<b>x</b>", "Multiple").replace("<b>&#8226;</b>", "");
					if(examine.contains(" <br />"))
						examine = examine.split("<br />")[0];
					//remove html tags
					examine =  examine.replaceAll("\\<.*?>","");
					if(examine.contains("./")) // ?
						examine = examine.split("./")[0];
					return examine;
				}
				} else {
					if (line.contains("<th style=\"white-space: nowrap;\"><a href=\"/wiki/Examine\" title=\"Examine\">Examine</a>")
							&& !line.contains("Unknown <small>")) {
						next++;
					}
					if (line.contains("</th><td> ") && next == 1) {
						examine = line.replace("</th><td> ", "");
						
						examine = line.replace("<td colspan=\"2\" style=\"padding:3px 7px 3px 7px; line-height:140%; text-align:center;\"> ",
								"")
				.replace("(i) dose", getWithinParentheses(name) != null ? getWithinParentheses(name) + " dose": "Multiple dose")
				.replace("X dose", getWithinParentheses(name) != null ? getWithinParentheses(name) + " dose" : "Multiple dose")
				.replace("(X) dose", getWithinParentheses(name) != null ? getWithinParentheses(name) + " dose" : "Multiple dose").replace("<th style=\"white-space: nowrap;\"><a href=\"/wiki/Examine\" title=\"Examine\">Examine</a></th><td> \"", "")"<i>X</i> dose", getWithinParentheses(name) != null ? getWithinParentheses(name) + " dose": "Multiple").replace("X dose", getWithinParentheses(name) != null ? getWithinParentheses(name) + " dose" : "Multiple dose").
								replace("<b>x</b>", "Multiple").replace("<b>&#8226;</b>", "");
				if(examine.contains(" <br />"))
					examine = examine.split("<br />")[0];
				//remove html tags
				examine =  examine.replaceAll("\\<.*?>","");
				if(examine.contains("./")) // ?
					examine = examine.split("./")[0];
						
						return examine;
					}
				}
				}
			in.close();
			return examine;
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return examine;
	}

	public static String getWithinParentheses(String examine) {
		Matcher m = Pattern.compile("\\(([^)]+)\\)").matcher(examine);
	     while(m.find()) {
	    	// System.out.println("Found within parentheses: " + m.group(1));
	     //  System.out.println(m.group(1));   
	       return m.group(1);
	     }
	     return null;
	}

	public static void write (String filename) throws IOException {
		  BufferedWriter outputWriter = null;
		  outputWriter = new BufferedWriter(new FileWriter(filename));
		  for(String s : examines) {
		    outputWriter.write(s);
		    outputWriter.newLine();
		  }
		  outputWriter.flush();  
		  outputWriter.close();  
		}
	
	*/

	/*private static void convertToXml() {
		BufferedWriter e = null;
		try {
			try {
				e = new BufferedWriter(new FileWriter(
						"./data/tools/itemDefinitions.txt", true));
				e.write("<list>");
				e.newLine();
				for (List<String> list : examines) {
					if (list != null) {
						e.write("\t<itemDefinition>");
						e.newLine();
						e.write("\t\t<id>" + list.id + "</id>");
						e.newLine();
						e.write("\t\t<name>" + list.name + "</name>");
						e.newLine();
						if (list.examine
								.equalsIgnoreCase("Swap this note at any bank for the equivalent item.")) {
							e.write("\t\t<examine>null</examine>");
							e.newLine();
						} else {
							e.write("\t\t<examine>" + list.examine
									+ "</examine>");
							e.newLine();
						}
						e.write("\t\t<members>" + list.members + "</members>");
						e.newLine();
						e.write("\t\t<equipSlot>" + list.equipSlot
								+ "</equipSlot>");
						e.newLine();
						e.write("\t\t<notedId>" + list.notedId + "</notedId>");
						e.newLine();
						e.write("\t\t<parentId>" + list.parentId
								+ "</parentId>");
						e.newLine();
						e.write("\t\t<noteable>" + list.noteable
								+ "</noteable>");
						e.newLine();
						e.write("\t\t<noted>" + list.noted + "</noted>");
						e.newLine();
						e.write("\t\t<twoHanded>" + list.twoHanded
								+ "</twoHanded>");
						e.newLine();
						e.write("\t\t<wearable>" + list.wearable
								+ "</wearable>");
						e.newLine();
						e.write("\t\t<destroyable>" + list.destroyable
								+ "</destroyable>");
						e.newLine();
						e.write("\t\t<buryable>" + list.buryable
								+ "</buryable>");
						e.newLine();
						e.write("\t\t<playerBound>" + list.playerBound
								+ "</playerBound>");
						e.newLine();
						e.write("\t\t<equipId>" + list.equipId + "</equipId>");
						e.newLine();
						e.write("\t\t<renderId>" + list.renderId
								+ "</renderId>");
						e.newLine();
						e.write("\t\t<sellPrice>" + list.sellPrice
								+ "</sellPrice>");
						e.newLine();
						e.write("\t\t<buyPrice>" + list.buyPrice
								+ "</buyPrice>");
						e.newLine();
						e.write("\t\t<bonuses>");
						e.newLine();// bonuses speak here
						for (int i = 0; i < 13; i++) {
							e.write("\t\t\t<int>" + list.bonuses[i] + "</int>");
							e.newLine();
						}
						e.write("\t\t</bonuses>");
						e.newLine();// bonuses end here
						e.write("\t\t<skillRequirements>");
						e.newLine();// reqs speak here
						for (Skill skill : list.getSkillRequirements().keySet()) {
							int lvl = list.getSkillRequirement(Skill
									.skillForId(skill.getId()).getId());
							e.write("\t\t\t<entry>");
							e.newLine();// speak here
							e.write("\t\t\t\t<skillRequirement>"
									+ skill.toString() + "</skillRequirement>");
							e.newLine();
							e.write("\t\t\t\t<int>" + lvl + "</int>");
							e.newLine();
							e.write("\t\t\t</entry>");
							e.newLine();// speak here

						}
						e.write("\t\t</skillRequirements>");
						e.newLine();// reqs end here
						e.write("\t</itemDefinition>");
						e.newLine();
					}
				}
				e.write("</list>");
				e.flush();
			} finally {
				if (e != null) {
					try {
						e.close();
					} catch (IOException e2) {
					}
				}
			}
		} catch (IOException d) {
			d.printStackTrace();
		}
	}*/
}
