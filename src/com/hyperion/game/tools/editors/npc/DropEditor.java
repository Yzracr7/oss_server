package com.hyperion.game.tools.editors.npc;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.NumberFormat;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.world.entity.npc.NPCDefinition;
import com.hyperion.utility.XStreamUtil;

public class DropEditor extends JFrame {

	private static final long serialVersionUID = 1L;
	NumberFormat percentFormat = NumberFormat.getPercentInstance();
	private DefaultMutableTreeNode dropsNode = new DefaultMutableTreeNode("Drops");
	private DefaultTreeModel dropsTreeModel = new DefaultTreeModel(this.dropsNode);
	private JPopupMenu tablePopup;
	private JButton addNew;
	private JButton addNewButton, addIds;
	private JButton deleteSelected;
	private JTable dropTable;
	private JLabel dropsForLabel;
	private JPanel dropsPanel;
	private JTree dropsTree;
	private JMenuBar fileMenu;
	private JSeparator headerSeparator;
	private JMenu jMenu1;
	private JSeparator middleSeparator;
	private JLabel npcIdLabel;
	private JButton removeDrop;
	private JButton repackDrop;
	private JTextField searchField;
	private JLabel searchIdLabel;
	private JScrollPane tableScroll;
	private JScrollPane treeScroll;

	public DropEditor() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception localException) {
		}
		try {
			EditorUtils.loadAllFiles();
		} catch (Exception e) {
			e.printStackTrace();
		}
		for (NpcDropController npcDrop : NpcDropController.list) {
			this.dropsNode.add(new DefaultMutableTreeNode(npcDrop));
		}
		initComponents();
	}

	private static List<NPCDefinition> defs = null;

	public static void main(String[] args) {
		/*
		 * EventQueue.invokeLater(new Runnable() { public void run() {
		 */
		try {
			defs = (List<NPCDefinition>) XStreamUtil.getXStream().fromXML(new FileInputStream("./data/world/npcs/npcDefinitions.xml"));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		new DropEditor().setVisible(true);
		/*
		 * } });
		 */
	}

	private void initComponents() {
		this.treeScroll = new JScrollPane();
		this.dropsTree = new JTree();
		this.searchField = new JTextField();
		this.searchIdLabel = new JLabel();
		this.middleSeparator = new JSeparator();
		this.dropsPanel = new JPanel();
		this.dropsForLabel = new JLabel();
		this.headerSeparator = new JSeparator();
		this.npcIdLabel = new JLabel();
		this.tableScroll = new JScrollPane();
		this.dropTable = new JTable();
		this.tablePopup = new JPopupMenu();
		this.addNew = new JButton();
		this.addIds = new JButton();
		this.deleteSelected = new JButton();
		this.repackDrop = new JButton();
		this.addNewButton = new JButton();
		this.removeDrop = new JButton();
		this.fileMenu = new JMenuBar();
		this.jMenu1 = new JMenu();
		setDefaultCloseOperation(3);
		setTitle("Drop Editor");

		this.dropsTree.setModel(this.dropsTreeModel);
		this.dropsTree.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent evt) {
				DropEditor.this.dropsTreeValueChanged(evt);
			}
		});
		this.treeScroll.setViewportView(this.dropsTree);

		this.searchField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				DropEditor.this.searchFieldActionPerformed(evt);
			}
		});
		this.searchIdLabel.setFont(new Font("Tahoma", 0, 12));
		this.searchIdLabel.setText("Search ID:");

		this.middleSeparator.setOrientation(1);

		this.dropsForLabel.setFont(new Font("Tahoma", 0, 14));
		this.dropsForLabel.setText("Drops for NPC(s):");

		this.npcIdLabel.setFont(new Font("Tahoma", 0, 14));
		this.npcIdLabel.setText(" ");

		this.dropTable.setModel(new DefaultTableModel(new Object[0][], new String[0]));

		this.dropTable.addMouseListener(new MouseListener() {
			public void mouseClicked(MouseEvent arg0) {
				if (arg0.getButton() == 3) {
					int row = DropEditor.this.dropTable.rowAtPoint(arg0.getPoint());
					DropEditor.this.dropTable.changeSelection(row, DropEditor.this.dropTable.getSelectedColumn(), false, false);
					DropEditor.this.tablePopup.show(DropEditor.this.dropTable, arg0.getX(), arg0.getY());
				}
			}

			public void mouseEntered(MouseEvent arg0) {
			}

			public void mouseExited(MouseEvent arg0) {
			}

			public void mousePressed(MouseEvent arg0) {
			}

			public void mouseReleased(MouseEvent arg0) {
			}
		});
		this.tableScroll.setViewportView(this.dropTable);

		this.addIds.setText("Add New Ids");
		this.addIds.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				DropEditor.this.addIdsActionPerformed(evt);
			}
		});

		this.addNew.setText("Add New Drop");
		this.addNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				DropEditor.this.addNewActionPerformed(evt);
			}
		});
		this.deleteSelected.setText("Delete Selected Drop");
		this.deleteSelected.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				DropEditor.this.deleteSelectedActionPerformed(evt);
			}
		});
		this.repackDrop.setText("Repack");
		this.repackDrop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				DropEditor.this.repackDropActionPerformed(evt);
			}
		});
		GroupLayout dropsPanelLayout = new GroupLayout(this.dropsPanel);
		this.dropsPanel.setLayout(dropsPanelLayout);
		dropsPanelLayout.setHorizontalGroup(dropsPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, dropsPanelLayout.createSequentialGroup().addContainerGap().addGroup(dropsPanelLayout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.headerSeparator, GroupLayout.Alignment.LEADING, -1, 424, 32767).addGroup(GroupLayout.Alignment.LEADING, dropsPanelLayout.createSequentialGroup().addComponent(this.dropsForLabel).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.npcIdLabel, -2, 200, -2)).addComponent(this.tableScroll, GroupLayout.Alignment.LEADING, -1, 424, 32767).addGroup(GroupLayout.Alignment.LEADING, dropsPanelLayout.createSequentialGroup().addComponent(this.addNew, -1, 101, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.deleteSelected, -1, 210, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.repackDrop, -1, 101, 32767))).addContainerGap()));
		dropsPanelLayout.setVerticalGroup(dropsPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(dropsPanelLayout.createSequentialGroup().addGroup(dropsPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.dropsForLabel).addComponent(this.npcIdLabel)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.headerSeparator, -2, 9, -2).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.tableScroll, -1, 327, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(dropsPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.addNew).addComponent(this.deleteSelected).addComponent(this.repackDrop)).addContainerGap()));

		this.addNewButton.setText("Add New NPC Drop");
		this.addNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				DropEditor.this.addNewButtonActionPerformed(evt);
			}
		});
		this.removeDrop.setText("Remove NPC Drop");
		this.removeDrop.setToolTipText("Removes the currently Selected NPC Drop");
		this.removeDrop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				DropEditor.this.removeDropActionPerformed(evt);
			}
		});
		this.jMenu1.setText("File");

		this.fileMenu.add(this.jMenu1);

		setJMenuBar(this.fileMenu);

		GroupLayout layout = new GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.removeDrop, -1, -1, 32767).addGroup(layout.createSequentialGroup().addComponent(this.searchIdLabel).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.searchField, -2, 67, -2)).addComponent(this.treeScroll, -1, 200, 32767).addComponent(this.addIds, -1, 100, 32767).addComponent(this.addNewButton, -1, -1, 32767)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.middleSeparator, -2, -1, -2).addGap(18, 18, 18).addComponent(this.dropsPanel, -1, -1, 32767).addContainerGap()));
		layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addGroup(GroupLayout.Alignment.LEADING, layout.createSequentialGroup().addContainerGap().addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.dropsPanel, GroupLayout.Alignment.TRAILING, -1, -1, 32767).addComponent(this.middleSeparator, -1, 410, 32767))).addGroup(layout.createSequentialGroup().addGap(15, 15, 15).addComponent(this.treeScroll).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.addIds).addComponent(this.addNewButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.removeDrop).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.searchIdLabel).addComponent(this.searchField, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED))).addContainerGap()));
		pack();
	}

	private DefaultMutableTreeNode searchNode(String nodeStr) {
		DefaultMutableTreeNode nodeToSearch = null;
		Enumeration e = this.dropsNode.breadthFirstEnumeration();
		while (e.hasMoreElements()) {
			nodeToSearch = (DefaultMutableTreeNode) e.nextElement();
			if (nodeStr.equals(nodeToSearch.getUserObject().toString())) {
				return nodeToSearch;
			}
		}
		return null;
	}

	private void searchFieldActionPerformed(ActionEvent evt) {
		try {
			DefaultMutableTreeNode n = searchNode(this.searchField.getText());
			TreePath path = new TreePath(this.dropsTreeModel.getPathToRoot(n));
			this.dropsTree.scrollPathToVisible(path);
			this.dropsTree.setSelectionPath(path);
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "Could not find the item.");
		}
	}

	private void dropsTreeValueChanged(TreeSelectionEvent evt) {
		if ((this.dropsTree.getLastSelectedPathComponent() != null) && (!"Drops".equals(this.dropsTree.getLastSelectedPathComponent().toString())))
			loadDrop((NpcDropController) ((DefaultMutableTreeNode) this.dropsTree.getLastSelectedPathComponent()).getUserObject());
	}

	private void deleteSelectedActionPerformed(ActionEvent evt) {
		try {
			NpcDropController ndc = (NpcDropController) ((DefaultMutableTreeNode) this.dropsTree.getLastSelectedPathComponent()).getUserObject();
			System.out.println("Deleted row: " + this.dropTable.getSelectedRow());
			ndc.getDropsArray().remove(this.dropTable.getSelectedRow());
			loadDrop((NpcDropController) ((DefaultMutableTreeNode) this.dropsTree.getLastSelectedPathComponent()).getUserObject());
			packFile();
		} catch (NullPointerException e) {
			JOptionPane.showMessageDialog(this, "Please select a drop first!");
		}
	}

	private void addNewActionPerformed(ActionEvent evt) {
		NpcDropController finalDrops = null;
		if (lastController != null) {
			lastController.insertDrop(new NPCItemDrop(Integer.parseInt(JOptionPane.showInputDialog("Enter the item ID for this drop.")), 1, 0, 0));
			finalDrops = lastController;
		} else {
			try {
				NpcDropController drops = (NpcDropController) ((DefaultMutableTreeNode) this.dropsTree.getLastSelectedPathComponent()).getUserObject();
				if (drops != null) {
					drops.insertDrop(new NPCItemDrop(Integer.parseInt(JOptionPane.showInputDialog("Enter the item ID for this drop.")), 1, 0, 0));
					finalDrops = drops;
				}
			} catch (Exception e) {
				System.out.println(e);
			}
		}

		if (finalDrops != null) {
			loadDrop(finalDrops);
			packFile();
		}
	}

	private void addIdsActionPerformed(ActionEvent evt) {
		NpcDropController drops = (NpcDropController) ((DefaultMutableTreeNode) this.dropsTree.getLastSelectedPathComponent()).getUserObject();
		final int[] current_ids = drops.getNpcIds();
		String[] incoming_ids = JOptionPane.showInputDialog("Enter new NPC IDs for this drop in the format:\nid,id,id,id").split(",");
		int[] new_ids = new int[current_ids.length + incoming_ids.length];
		for (int i = 0; i < current_ids.length; i++) {
			new_ids[i] = current_ids[i];
		}
		ArrayDeque<Integer> ids_to_add = new ArrayDeque<Integer>();
		for (int i = 0; i < incoming_ids.length; i++) {
			try {
				ids_to_add.add(Integer.parseInt(incoming_ids[i]));
			} catch (Exception localException) {
			}
		}
		for (int i = 0; i < new_ids.length; i++) {
			if (new_ids[i] == 0) {
				new_ids[i] = ids_to_add.poll();
			}
		}
		drops.setIds(new_ids);
		reload();
		packFile();
	}

	private void addNewButtonActionPerformed(ActionEvent evt) {
		String[] idsS = JOptionPane.showInputDialog("Please enterInstancedRoom the NPC IDs for this drop in the format:\n#,#,#,#").split(",");
		int[] ids = new int[idsS.length];
		for (int i = 0; i < ids.length; i++)
			try {
				ids[i] = Integer.parseInt(idsS[i]);
			} catch (Exception localException) {
			}
		new NpcDropController(ids);
		packFile();
	}

	private void removeDropActionPerformed(ActionEvent evt) {
		try {
			if (!this.dropsTree.getLastSelectedPathComponent().toString().equals("")) {
				NpcDropController.list.remove((NpcDropController) ((DefaultMutableTreeNode) this.dropsTree.getLastSelectedPathComponent()).getUserObject());
				packFile();
			}
		} catch (NullPointerException e) {
			JOptionPane.showMessageDialog(this, "Please select a drop first!");
		}
	}

	private void repackDropActionPerformed(ActionEvent evt) {
		packFile();
	}

	private NpcDropController lastController = null;

	private void loadDrop(final NpcDropController drops) {
		lastController = drops;
		NPCDefinition[] definitions = null;
		ArrayList<String> names = new ArrayList<String>();
		/*
		 * for (int npcId : drops.getNpcIds()) { name =
		 * CachedNpcDefinition.getNPCDefinitions
		 * (NPCDefinition.renderIds[npcId]).name; if (!names.contains(name)) {
		 * names.add(name); } }
		 */
		if (defs != null)
			for (NPCDefinition def : defs) {
				for (int npcId : drops.getNpcIds()) {
					if (def.getId() == npcId) {
						String name2 = def.getName();
						if (name2 != null)
							if (!names.contains(name2))
								names.add(name2);
					}
				}
			}
		this.npcIdLabel.setText(Arrays.toString(names.toArray()));
		DefaultTableModel model = new DefaultTableModel();
		model.addTableModelListener(new TableModelListener() {
			public void tableChanged(TableModelEvent e) {
				try {
					if ((e.getColumn() != -1) && (e.getColumn() != 4) && (e.getColumn() != 3)) {
						((NPCItemDrop) drops.getDropsArray().get(DropEditor.this.dropTable.getSelectedRow())).setId(Integer.parseInt(DropEditor.this.dropTable.getValueAt(DropEditor.this.dropTable.getSelectedRow(), 0).toString().split(" - ")[0]));
						String[] test = ((String) DropEditor.this.dropTable.getValueAt(DropEditor.this.dropTable.getSelectedRow(), 1)).split("-");
						((NPCItemDrop) drops.getDropsArray().get(DropEditor.this.dropTable.getSelectedRow())).setCount(Integer.parseInt(test[0]));
						((NPCItemDrop) drops.getDropsArray().get(DropEditor.this.dropTable.getSelectedRow())).setMaxCount(Integer.parseInt(test[1]));
						if (e.getColumn() == 0) {
							DropEditor.this.dropTable.getModel().setValueAt(ItemDefinition.forId(((NPCItemDrop) drops.getDropsArray().get(DropEditor.this.dropTable.getSelectedRow())).getId()).getName(), DropEditor.this.dropTable.getSelectedRow(), 4);
						} else if (e.getColumn() == 2) {
							((NPCItemDrop) drops.getDropsArray().get(DropEditor.this.dropTable.getSelectedRow())).setPercentage(Double.parseDouble(DropEditor.this.dropTable.getValueAt(DropEditor.this.dropTable.getSelectedRow(), 2).toString()));
						}
					}
				} catch (Exception f) {
					f.printStackTrace();
				}
			}
		});
		model.addColumn("Item ID");
		model.addColumn("Count");
		model.addColumn("Regular Drop %");
		model.addColumn("Name");
		for (NPCItemDrop d : drops.getDropsArray()) {
			if (d != null) {
				String itemName = "n/a";
				ItemDefinition def = ItemDefinition.forId(d.getId());
				if (def != null) {
					itemName = def.getName();
				}
				model.addRow(new Object[] { Integer.valueOf(d.getId()), "" + d.getCount() + "-" + d.getMaxCount() + "", Double.valueOf(d.getPercentage()), itemName });
			}
		}
		this.dropTable.setModel(model);
	}

	private void packFile() {
		try {
			FileOutputStream fos = new FileOutputStream("data/world/npcs/npcDrops.xml");/*
																						 * (
																						 * "data/world/npcs/npcDropsNew.xml"
																						 * )
																						 * ;
																						 */
			EditorUtils.getxStream().toXML(NpcDropController.list, fos);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		reload();
	}

	private void reload() {
		this.dropsNode.removeAllChildren();
		for (NpcDropController npcDrop : NpcDropController.list) {
			this.dropsNode.add(new DefaultMutableTreeNode(npcDrop));
		}
		this.dropsTreeModel.reload(this.dropsNode);
	}
}