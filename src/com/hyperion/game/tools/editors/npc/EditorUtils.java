package com.hyperion.game.tools.editors.npc;

import com.hyperion.cache.Cache;
import com.hyperion.game.item.Item;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.world.entity.npc.NPCDefinition;
import com.thoughtworks.xstream.XStream;

public class EditorUtils {

    private static EditorUtils instance = new EditorUtils();
    private static XStream xStream = new XStream();

    static {
        xStream.alias("dropController", NpcDropController.class);
        xStream.alias("npcDrop", NPCItemDrop.class);
        xStream.alias("itemDefinition", ItemDefinition.class);
        xStream.alias("item", Item.class);
        xStream.alias("npcDefinition", NPCDefinition.class);
    }

    public static EditorUtils getInstance() {
        return instance;
    }

    public static XStream getxStream() {
        return xStream;
    }

    public static void loadAllFiles() throws Exception {
        Cache.init();
        //Loaders.loadItemDefinitions();
       // NPCDefinition.init();
        NpcDropController.init(false);
    }
}
