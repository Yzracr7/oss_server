package com.hyperion.game.tools.editors.npc;

import com.hyperion.game.item.Item;

public class NPCItemDrop {
	private int id;
	private int count = 1, maxCount = 0;
	private double percentage;

	public void setId(int id) {
		this.id = id;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}

	public NPCItemDrop(int id, int count, int maxCount, double percentage) {
		this.id = id;
		this.count = count;
		this.maxCount = maxCount;
		this.percentage = percentage;
	}

	public boolean shouldAlwaysDrop() {
		return this.percentage == 100;
	}

	public int getId() {
		return this.id;
	}

	public int getCount() {
		return this.count;
	}

	public double getPercentage() {
		return this.percentage;
	}

	public Item getItem() {
		return new Item(this.id, this.count);
	}

	public int getMaxCount() {
		return maxCount;
	}

	public void setMaxCount(int maxCount) {
		this.maxCount = maxCount;
	}
}