package com.hyperion.game.tools.editors.npc;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.hyperion.Logger;
import com.hyperion.cache.Cache;
import com.hyperion.cache.definitions.CachedNpcDefinition;
import com.hyperion.game.world.entity.npc.NPCDefinition;

public class NpcDropController {

    public static void main(String[] args) throws IOException {
        // finding npcs that drop a certain item
        Cache.init();
        init(false);
        int dropId = 2714; // casket
        for (NpcDropController dropController : list) {
            ArrayList<NPCItemDrop> drops = dropController.drops;
            int[] npcIds = dropController.npcIds;
            StringBuilder bldr = new StringBuilder();
            for (int npcId : npcIds) {
                bldr.append("(" + CachedNpcDefinition.getNPCDefinitions(npcId).getName() + ", " + npcId + "), ");
            }
            boolean hasDrop = false;
            for (Iterator<NPCItemDrop> it$ = drops.iterator(); it$.hasNext(); ) {
                NPCItemDrop drop = it$.next();
                if (drop.getId() == dropId) {
                    hasDrop = true;
                    bldr.append("\n\t[" + drop.getId() + " x{" + drop.getCount() + "," + drop.getMaxCount() + "} @ " + drop.getPercentage() + "]");
                }
            }
            if (hasDrop) {
                System.out.println(bldr);
            }
        }
    }

    public static List<NpcDropController> list;
    private int[] npcIds;
    private ArrayList<NPCItemDrop> drops;

    NpcDropController(int[] npcIds) {
        this.npcIds = npcIds;
        this.drops = new ArrayList<NPCItemDrop>();
        list.add(this);
    }

    @SuppressWarnings("unchecked")
    public static void init(boolean serverstartup) throws IOException {
        list = (List<NpcDropController>) EditorUtils.getxStream().fromXML(new FileInputStream("data/world/npcs/npcDrops.xml"));
        for (NpcDropController npcDropsList : list) {
            npcDropsList.readResolve(serverstartup);
        }
        //Logger.getInstance().warning("Loaded " + list.size() + " npc drop sets.");
    }

    private Object readResolve(boolean serverstartup) {
        if (serverstartup) {
            for (int i = 0; i < npcIds.length; i++) {
                NPCDefinition.forId(npcIds[i]).setDrop(this);
            }
        }
        return this;
    }

    public ArrayList<NPCItemDrop> getDropsArray() {
        return this.drops;
    }

    public void insertDrop(NPCItemDrop drop) {
        this.drops.add(drop);
    }

    public int[] getNpcIds() {
        return this.npcIds;
    }

    public void setIds(int[] new_array) {
        this.npcIds = new_array;
    }

    public String toString() {
        return Arrays.toString(this.npcIds);
    }
}