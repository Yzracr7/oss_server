package com.hyperion.game.world;

import com.hyperion.game.content.Greegree;
import com.hyperion.game.content.godwars.Godwars;
import com.hyperion.game.content.minigames.barrows.Barrows;
import com.hyperion.game.content.wilderness.BountyHunter;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.player.Player;

public class AreaHandler {

	public static void handleAreas(Entity entity) {
		Location loc = entity.getLocation();
		int currentLevel = entity.getWildLevel();
		if (TeleportAreaLocations.inWilderness(loc)) {
			if (currentLevel != entity.getLastWildLevel()) {
				if (currentLevel > 0) {
					entity.setLastwildLevel(currentLevel);
					if(entity instanceof Player)
						((Player)entity).getPacketSender().modifyText("Level: " + ((Player)entity).getWildLevel(), 650, 8);
				}
			}
			if (!entity.getAttributes().isSet("wildy")) {
				entity.getAttributes().set("wildy", true);
				if (entity.isPlayer()) {
					Player player = (Player) entity;
					if(!player.getAttributes().isSet("target")) {
						BountyHunter.sendBlankInterface(player);
					}
					player.getPacketSender().sendOverlay(650); //381
					World.getWorld().getPlayersInWilderness().add(player.getName());
					if(player.getAttributes().isSet("isMonkey")) {
						Greegree.resetAnimations(player);
					}
				}
			}
		} else {
			if (entity.getAttributes().isSet("wildy")) {
				entity.getAttributes().remove("wildy");
				if (entity.isPlayer()) {
					Player player = (Player) entity;
					player.getPacketSender().sendRemoveOverlay();
					//World.getWorld().getPlayersInWilderness().remove(player.getName());
					if(player.getAttributes().isSet("target")) {
						player.getAttributes().set("wildyTimer", 192);
						player.getPacketSender().sendMessage("If you do not return to the Wilderness within two minutes, you will lose your target.");
					}
				}
			}
		}
		if (TeleportAreaLocations.inAttackableArea(loc) || entity.getAttributes().isSet("clansession")) {
			if (!entity.getAttributes().isSet("attackablearea")) {
				entity.getAttributes().set("attackablearea", true);
				if (entity.isPlayer()) {
					Player player = (Player) entity;
					player.getPacketSender().sendPlayerOption(entity.getAttributes().isSet("duelArea") ? "Fight" : "Attack", 1, entity.getAttributes().isSet("duelArea"));
				}
			}
		} else {
			if (entity.getAttributes().isSet("attackablearea")) {
				entity.getAttributes().remove("attackablearea");
				if (!entity.getAttributes().isSet("challengearea")) {
					if (entity.isPlayer()) {
						Player player = (Player) entity;
						player.getPacketSender().sendPlayerOption("null", 1, entity.getAttributes().isSet("duelArea"));
					}
				} else if (entity.getAttributes().isSet("challengearea")) {
					if (entity.isPlayer()) {
						Player player = (Player) entity;
						player.getPacketSender().sendPlayerOption("Challenge", 1, false);
					}
				}
			}
		}
		if (TeleportAreaLocations.inGodWars(loc)) {
			if (!entity.getAttributes().isSet("godwarsarea")) {
				entity.getAttributes().set("godwarsarea", true);
				if (entity.isPlayer()) {
					Player player = (Player) entity;
					player.getPacketSender().sendOverlay(599);
					Godwars.updateKills(player);
				}
			}
		} else {
			if (entity.getAttributes().isSet("godwarsarea")) {
				entity.getAttributes().remove("godwarsarea");
				if (entity.isPlayer()) {
					Player player = (Player) entity;
					player.getPacketSender().sendRemoveOverlay();
				}
			}
		}
		if (TeleportAreaLocations.inChallengeArea(loc)) {
			if (!entity.getAttributes().isSet("challengearea")) {
				entity.getAttributes().set("challengearea", true);
				if (entity.isPlayer()) {
					Player player = (Player) entity;
					player.getPacketSender().sendPlayerOption("Challenge", 1, false);
				}
			}
		} else {
			if (entity.getAttributes().isSet("challengearea")) {
				entity.getAttributes().remove("challengearea");
				if (!entity.getAttributes().isSet("attackablearea")) {
					if (entity.isPlayer()) {
						Player player = (Player) entity;
						player.getPacketSender().sendPlayerOption("null", 1, false);
					}
				} else if (entity.getAttributes().isSet("attackablearea")) {
					if (entity.isPlayer()) {
						Player player = (Player) entity;
						player.getPacketSender().sendPlayerOption("Attack", 1, false);
					}
				}
			}
		}
		if (TeleportAreaLocations.atDuelArena(loc)) {
			if (!entity.getAttributes().isSet("duelArea")) {
				if (entity.isPlayer()) {
					Player player = (Player) entity;
					player.getPacketSender().sendOverlay(105);
				}
				entity.getAttributes().set("duelArea", true);
			}
		} else if (!TeleportAreaLocations.atDuelArena(loc)) {
			if (entity.getAttributes().isSet("duelArea")) {
				if (entity.isPlayer()) {
					Player player = (Player) entity;
					player.getPacketSender().sendRemoveOverlay();
				}
				entity.getAttributes().remove("duelArea");
			}
		}
		if (TeleportAreaLocations.isInMultiZone(entity, loc)) {
			if (!entity.getAttributes().isSet("multi")) {
				entity.getAttributes().set("multi", true);
				if (entity.isPlayer()) {
					Player player = (Player) entity;
					player.getPacketSender().sendInterfaceConfig(548, 65, true);
				}
			}
		} else if (!TeleportAreaLocations.isInMultiZone(entity, loc)) {
			if (entity.getAttributes().isSet("multi")) {
				entity.getAttributes().remove("multi");
				if (entity.isPlayer()) {
					Player player = (Player) entity;
					player.getPacketSender().sendInterfaceConfig(548, 65, false);
				}
			}
		} else if (TeleportAreaLocations.inCyclopsRoom(loc)) {
			if (!entity.getAttributes().isSet("cyclopsroom")) {
				entity.getAttributes().set("cyclopsroom", true);
			}
		} else if (!TeleportAreaLocations.inCyclopsRoom(loc)) {
			if (entity.getAttributes().isSet("cyclopsroom")) {
				entity.getAttributes().remove("cyclopsroom");
			}
		} if (TeleportAreaLocations.atBarrows(loc)) {
			if (!entity.getAttributes().isSet("barrowsarea")) {
				entity.getAttributes().set("barrowsarea", true);
				if (entity.isPlayer()) {
					Player player = (Player) entity;
					player.getPacketSender().sendOverlay(651);
					Barrows.updateKills(player);
				}
			}
		} else {
			if (entity.getAttributes().isSet("barrowsarea")) {
				entity.getAttributes().remove("barrowsarea");
				if (entity.isPlayer()) {
					Player player = (Player) entity;
					player.getPacketSender().sendRemoveOverlay();
				}
			}
		}
	}
}
