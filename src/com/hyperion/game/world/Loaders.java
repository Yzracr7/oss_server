package com.hyperion.game.world;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.hyperion.Logger;
import com.hyperion.cache.json.impl.InfernoItemDefinitionLoader;
import com.hyperion.cache.json.impl.NPCDefinitionLoader;
import com.hyperion.cache.json.impl.NpcNodeLoader;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.world.entity.npc.NPCDefinition;
import com.thoughtworks.xstream.XStream;
import com.hyperion.game.tools.editors.npc.NpcDropController;
public class Loaders {

    private static XStream xStream = new XStream();

    // private static ItemDefinition[] itemDefinitions = new ItemDefinition[23000];
    //private static int[] renderIds = new int[23000];
    //public static String[] itemExamines = new String[23000];
    public static int[] renderIdsViseVersa = new int[23000];
    public static int[] animIds = new int[14000];

    public static void loadAllDefinitions() throws FileNotFoundException, IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {

        /**
         * New JSON Definition Loaders
         *
         * TODO Cleanup everything that handles shit with old definitions and use these new ones
         * which are currently not used
         */
        //ITEMS
        //ItemDefinitionLoader itemloader = new ItemDefinitionLoader();
        //itemloader.load();

        //ITEMS
        InfernoItemDefinitionLoader infernoitemloader = new InfernoItemDefinitionLoader();
        infernoitemloader.load();
        //infernoitemloader.dumpNames();

        //NPCS
        NPCDefinitionLoader npcloader = new NPCDefinitionLoader();
        npcloader.load();
        BufferedReader buf = new BufferedReader(new FileReader("./data/npcs.txt"));
        String line = "";
        try {
            while ((line = buf.readLine()) != null) {
                String[] split = line.split(":");
                int origId = Integer.parseInt(split[0]);
                int fakeId = Integer.parseInt(split[1]);
                NPCDefinition.renderIds[origId] = fakeId;
            }
        } finally {
            buf.close();
        }

        //NPC SPAWNS
        NpcNodeLoader npcspawnloader = new NpcNodeLoader();
        npcspawnloader.load();
        Logger.getInstance().warning("World has loaded " + World.getWorld().getNPCCount() + " new npc spawns");
        NpcDropController.init(true);

        //OLD SHIT
        //NPCDefinition.init();
        /*
                loadItemDefinitions();
        loadBonusDefinitions();
        for (ItemDefinition def : itemDefinitions) {
            if (def != null) {
                Startup.load_item_definitions_extras(def);
                if (!def.isNoted() && def.getNotedId() != -1) {
                    int price = def.getItemValue();
                    ItemDefinition notedDef = itemDefinitions[def.getNotedId()];
                    if (notedDef != null)
                        notedDef.setItemValue(price);
                }
            }
        }
         */
    }

  /*   public static void loadItemDefinitions() throws FileNotFoundException, IOException {
        BufferedReader br = new BufferedReader(new FileReader("data/world/items/definitions.json"));
        ItemDefinition[] obj = World.getWorld().gson.fromJson(br, ItemDefinition[].class);
        for (ItemDefinition defs : obj) {
            if (defs != null) {
                itemDefinitions[defs.getId()] = defs;
            }
        }
    }
    //render ids
       try {
            BufferedReader buf = new BufferedReader(new FileReader("./data/items.txt"));
			String line = "";
			try {
				while ((line = buf.readLine()) != null) {
					// changed slayer helm(not full) 13263:6814
					String[] split = line.split(":");
					int origId = Integer.parseInt(split[0]);
					int fakeId = Integer.parseInt(split[1]);
					// renderIds[origId] = fakeId;
					// renderIdsViseVersa[fakeId] = origId;
					renderIds[origId] = origId;
					renderIdsViseVersa[origId] = origId;
				}
			} finally {
				buf.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

        try {
            BufferedReader buf = new BufferedReader(new FileReader("./data/world/items/examine.txt"));
            String line = "";
            try {
                while ((line = buf.readLine()) != null) {
                    String[] split = line.split(":");
                    int id = Integer.parseInt(split[0]);
                    String examine = split[1];
                    if (itemDefinitions[id] != null) {
                        if (examine != null)
                            itemDefinitions[id].(examine);
                        else
                            itemDefinitions[id].setExamine("It's a null.");
                    }
                    //itemExamines[id] = examine;
                }
            } finally {
                buf.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            BufferedReader buf = new BufferedReader(new FileReader("./data/world/items/weights.txt"));
            String line = "";
            try {
                while ((line = buf.readLine()) != null) {
                    if (line.startsWith("//")) {
                        continue;
                    }
                    String[] split = line.split(" - ");
                    int id = Integer.parseInt(split[0]);
                    double weight = Double.parseDouble(split[1]);

                    if (id >= itemDefinitions.length)
                        break;
                    if (itemDefinitions[id] != null) {
                        if (split != null)
                            itemDefinitions[id].setWeight(weight);
                    }
                }
            } finally {
                buf.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Loaded all item weight values.");


        try {
            BufferedReader buf = new BufferedReader(new FileReader("./data/anims.txt"));
            String line = "";
            try {
                while ((line = buf.readLine()) != null) {
                    String[] split = line.split(":");
                    int origId = Integer.parseInt(split[0]);
                    int fakeId = Integer.parseInt(split[1]);
                    animIds[origId] = fakeId;
                }
            } finally {
                buf.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            BufferedReader buf = new BufferedReader(new FileReader("./data/objects.txt"));
            String line = "";
            try {
                while ((line = buf.readLine()) != null) {
                    String[] split = line.split(":");
                    int origId = Integer.parseInt(split[0]);
                    int fakeId = Integer.parseInt(split[1]);
                    CachedObjectDefinition.renderIds[origId] = fakeId;
                    CachedObjectDefinition.renderIdsViseVersa[fakeId] = origId;
                }
            } finally {
                buf.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Loaded item definitions.");
    }

    public static void loadBonusDefinitions() throws FileNotFoundException, IOException {
        final String PACKED_PATH = "data/world/items/bonuses.ib";
        try {
            RandomAccessFile in = new RandomAccessFile(PACKED_PATH, "r");
            FileChannel channel = in.getChannel();
            ByteBuffer buffer = channel.map(MapMode.READ_ONLY, 0, channel.size());
            ItemDefinition.setBonuses(new HashMap<Integer, int[]>(buffer.remaining() / 26));
            while (buffer.hasRemaining()) {
                int itemId = buffer.getShort() & 0xffff;
                int[] bonuses = new int[12];
                for (int index = 0; index < bonuses.length; index++) {
                    bonuses[index] = buffer.getShort();
                }
                ItemDefinition.getBonuses().put(itemId, bonuses);
            }
            channel.close();
            in.close();
            System.out.println("Loaded item bonuses.");
        } catch (Throwable e) {
            World.getWorld().handleError(e);
        }
    }

        public static ItemDefinition[] getItemDefinitions () {
            return itemDefinitions;
        }*/

    /**
     * Sets the xStream
     *
     * @param xStream the xStream
     */

    public static void setxStream(final XStream xStream) {
        Loaders.xStream = xStream;
    }

    /**
     * Gets the xStream
     *
     * @return the xStream
     */
    private static XStream getxStream() {
        return xStream;
    }

    static {
        /** ItemDefinition */
        getxStream().alias("itemDef", ItemDefinition.class);
        getxStream().alias("shieldDefinition", ItemDefinition.EquipmentDefinition.ShieldDefinition.class);
        getxStream().alias("equipmentDefinition", ItemDefinition.EquipmentDefinition.class);
        getxStream().alias("weaponDefinition", ItemDefinition.WeaponDefinition.class);
        getxStream().alias("attackStyles", ItemDefinition.WeaponDefinition.AttackStyles.class);
        getxStream().alias("specialAttackData", ItemDefinition.WeaponDefinition.SpecialAttackData.class);
        getxStream().alias("rangedWeaponDefinition", ItemDefinition.WeaponDefinition.RangedWeaponDefinition.class);
    }
}
