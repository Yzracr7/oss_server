package com.hyperion.game.world;

import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.pathfinders.Directions;
import com.hyperion.game.world.pathfinders.PrimitivePathFinder;
import com.hyperion.game.world.pathfinders.Directions.NormalDirection;
import com.hyperion.utility.NumberUtils;

/**
 * Represents a single location in the game world.
 * 
 * @author Graham Edgecombe
 *
 */
public class Location {

	/**
	 * The x coordinate.
	 */
	private final int x;

	/**
	 * The y coordinate.
	 */
	private final int y;

	/**
	 * The z coordinate.
	 */
	private final int z;

	private int width, height;

	/**
	 * Creates a location.
	 * 
	 * @param x
	 *            The x coordinate.
	 * @param y
	 *            The y coordinate.
	 * @param z
	 *            The z coordinate.
	 * @return The location.
	 */
	public static Location create(int x, int y, int z) {
		return new Location(x, y, z);
	}

	/**
	 * Creates a location.
	 * 
	 * @param x
	 *            The x coordinate.
	 * @param y
	 *            The y coordinate.
	 * @param z
	 *            The z coordinate.
	 * @return The location.
	 */
	public static Location create(int x, int y) {
		return new Location(x, y, 0);
	}

	/**
	 * Creates a location.
	 * 
	 * @param x
	 *            The x coordinate.
	 * @param y
	 *            The y coordinate.
	 * @param z
	 *            The z coordinate.
	 */
	public Location(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * Gets the absolute x coordinate.
	 * 
	 * @return The absolute x coordinate.
	 */
	public int getX() {
		return x;
	}

	/**
	 * Gets the absolute y coordinate.
	 * 
	 * @return The absolute y coordinate.
	 */
	public int getY() {
		return y;
	}

	/**
	 * Gets the z coordinate, or height.
	 * 
	 * @return The z coordinate.
	 */
	public int getZ() {
		return z;
	}

	/**
	 * Gets the local x coordinate relative to this region.
	 * 
	 * @return The local x coordinate relative to this region.
	 */
	public int getLocalX() {
		return getLocalX(this);
	}

	/**
	 * Gets the local y coordinate relative to this region.
	 * 
	 * @return The local y coordinate relative to this region.
	 */
	public int getLocalY() {
		return getLocalY(this);
	}

	/**
	 * Gets the local x coordinate relative to a specific region.
	 * 
	 * @param l
	 *            The region the coordinate will be relative to.
	 * @return The local x coordinate.
	 */
	public int getLocalX(Location l) {
		return x - 8 * (l.getChunkX() - 6);
	}

	/**
	 * Gets the local y coordinate relative to a specific region.
	 * 
	 * @param l
	 *            The region the coordinate will be relative to.
	 * @return The local y coordinate.
	 */
	public int getLocalY(Location l) {
		return y - 8 * (l.getChunkY() - 6);
	}

	/**
	 * Gets the region x coordinate.
	 * 
	 * @return The region x coordinate.
	 */
	public int getChunkX() {
		return (x >> 3);
	}

	/**
	 * Gets the region y coordinate.
	 * 
	 * @return The region y coordinate.
	 */
	public int getChunkY() {
		return (y >> 3);
	}

	public int getRegionX() {
		return (x >> 6);
	}

	public int getRegionY() {
		return (y >> 6);
	}

	public int getXInRegion() {
		return x & 0x3F;
	}

	public int getYInRegion() {
		return y & 0x3F;
	}

	public int getXInChunk() {
		return x & 0x7;
	}

	public int getYInChunk() {
		return y & 0x7;
	}

	public int getRegionId() {
		return ((getRegionX() << 8) + getRegionY());
	}

	public boolean withinActualDistance(Location other, int dist) {
		if (other.z != z) {
			return false;
		}
		return ((int) getDistance(other)) <= dist;
	}

	private double getDistance(Location other) {
		int xdiff = this.getX() - other.getX();
		int ydiff = this.getY() - other.getY();
		return Math.sqrt(xdiff * xdiff + ydiff * ydiff);
	}

	public boolean isDiagnol(Location loc) {
		return (loc.equals(getNorthEast()) || loc.equals(getSouthEast()) || loc.equals(getNorthWest()) || loc.equals(getSouthWest()));
	}

	public Location getNorth() {
		return transform(0, 1, 0);
	}

	public Location getSouth() {
		return transform(0, -1, 0);
	}

	public Location getEast() {
		return transform(-1, 0, 0);
	}

	public Location getNorthEast() {
		return transform(1, 1, 0);
	}

	public Location getSouthEast() {
		return transform(1, -1, 0);
	}

	public Location getWest() {
		return transform(1, 0, 0);
	}

	public Location getNorthWest() {
		return transform(-1, 1, 0);
	}

	public Location getSouthWest() {
		return transform(-1, -1, 0);
	}

	/**
	 * Checks if this location is within range of another.
	 * 
	 * @param other
	 *            The other location.
	 * @return <code>true</code> if the location is in range, <code>false</code>
	 *         if not.
	 */
	public boolean isWithinDistance(Location other) {
		if (z != other.z) {
			return false;
		}
		int deltaX = other.x - x, deltaY = other.y - y;
		return deltaX <= 14 && deltaX >= -15 && deltaY <= 14 && deltaY >= -15;
	}

	/**
	 * Checks if a coordinate is within range of another.
	 *
	 * @return <code>true</code> if the location is in range, <code>false</code>
	 *         if not.
	 */
	public boolean isWithinDistance(int width, int height, Location otherLocation, int otherWidth, int otherHeight, int distance) {
		Location myClosestTile = this.closestTileOf(otherLocation, width, height);
		Location theirClosestTile = otherLocation.closestTileOf(this, otherWidth, otherHeight);
		return myClosestTile.distanceToPoint(theirClosestTile) <= distance;
	}

	public static boolean isWithinDistance(Entity entity, Entity other, int distance) {
		Location myClosestTile = entity.getLocation().closestTileOf(other.getLocation(), entity.getSize(), entity.getSize());
		Location theirClosestTile = other.getLocation().closestTileOf(entity.getLocation(), other.getSize(), other.getSize());
		return myClosestTile.distanceToPoint(theirClosestTile) <= distance;
	}

	public static boolean isWithinDistance(Location entity, Location other, int size, int distance) {
		Location myClosestTile = entity.closestTileOf(other, size, size);
		Location theirClosestTile = other.closestTileOf(entity, size, size);
		return myClosestTile.distanceToPoint(theirClosestTile) <= distance;
	}

	/**
	 * Gets the closest tile of this location from a specific point.
	 */
	private Location closestTileOf(Location from, int width, int height) {
		if (width < 2 && height < 2) {
			return this;
		}
		Location location = null;
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				Location loc = Location.create(this.x + x, this.y + y, this.z);
				if (location == null || loc.distanceToPoint(from) < location.distanceToPoint(from)) {
					location = loc;
				}
			}
		}
		return location;
	}

	public boolean withinDistance(Location other, int dist, boolean ignoreZ) {
		if (!ignoreZ) {
			if (other.getZ() != z) {
				return false;
			}
		}
		int deltaX = other.x - x, deltaY = other.y - y;
		return (deltaX <= (dist - 1) && deltaX >= -dist && deltaY <= (dist - 1) && deltaY >= -dist);
	}

	public int distanceToPoint(Location other) {
		int absX = x;
		int absY = y;
		int pointX = other.getX();
		int pointY = other.getY();
		return (int) Math.sqrt(Math.pow(absX - pointX, 2) + Math.pow(absY - pointY, 2));
	}

	@Override
	public int hashCode() {
		return z << 30 | x << 15 | y;
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Location)) {
			return false;
		}
		Location loc = (Location) other;
		return loc.x == x && loc.y == y && loc.z == z;
	}

	@Override
	public String toString() {
		return "[" + x + "," + y + "," + z + "],[" + getLocalX() + "," + getLocalY() + "]";
	}

	/**
	 * Creates a new location based on this location.
	 * 
	 * @param diffX
	 *            X difference.
	 * @param diffY
	 *            Y difference.
	 * @param diffZ
	 *            Z difference.
	 * @return The new location.
	 */
	public Location transform(int diffX, int diffY, int diffZ) {
		return Location.create(x + diffX, y + diffY, z + diffZ);
	}

	public int getWildernessLevel() {
		if (getX() >= 2944 && getX() < 3392 && getY() >= 3525 && getY() < 6400) {
			return 1 + (getY() - 3525) / 8;
		} else {
			return 0;
		}
	}

	public NormalDirection direction(Location next) {
		if (next == null)
			return null;
		return Directions.directionFor(this, next);
	}

	public boolean isInArea(int bottomLeftX, int bottomLeftY, int topRightX, int topRightY) {
		return (x >= bottomLeftX && x <= topRightX && y >= bottomLeftY && y <= topRightY);
	}

	public boolean inArea(int lowestX, int lowestY, int highestX, int highestY) {
		return x >= lowestX && y >= lowestY && x <= highestX && y <= highestY;
	}

	public boolean inArea(int z1, int z2, int lowestX, int lowestY, int highestX, int highestY) {
		if (z1 != z2) {
			return false;
		}
		return x >= lowestX && y >= lowestY && x <= highestX && y <= highestY;
	}

	public int getDistance2(Location pos) {
		return (int) distance(pos);
	}

	public double distance(Location other) {
		if (z != other.z) {
			return Integer.MAX_VALUE - 1;
		}
		return distanceFormula(x, y, other.x, other.y);
	}

	private static double distanceFormula(int x, int y, int x2, int y2) {
		return Math.sqrt(Math.pow(x2 - x, 2) + Math.pow(y2 - y, 2));
	}

	public static double distanceFormula(double x, double y, double x2, double y2) {
		return Math.sqrt(Math.pow(x2 - x, 2) + Math.pow(y2 - y, 2));
	}

	public static boolean standingOn(Entity entity, Entity other) {
		int firstSize = entity.getSize();
		int secondSize = other.getSize();
		int x = entity.getLocation().getX();
		int y = entity.getLocation().getY();
		int vx = other.getLocation().getX();
		int vy = other.getLocation().getY();
		for (int i = x; i < x + firstSize; i++) {
			for (int j = y; j < y + firstSize; j++) {
				if (i >= vx && i < secondSize + vx && j >= vy && j < secondSize + vy) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean isDiagonal(Entity source, Entity target) {
		Location l = source.getLocation();
		Location l2 = target.getLocation();
		if (l.getSouthEast().equals(l2)) {
			return true;
		}
		if (l.getSouthWest().equals(l2)) {
			return true;
		}
		if (l.getNorthEast().equals(l2)) {
			return true;
		}
		if (l.getNorthWest().equals(l2)) {
			return true;
		}
		return false;
	}

	public boolean withinDistance(Location other) {
		if (z != other.z) {
			return false;
		}
		int deltaX = other.x - x, deltaY = other.y - y;
		return deltaX <= 14 && deltaX >= -15 && deltaY <= 14 && deltaY >= -15;
	}

	public static Location create(Location loc, int randomize) {
		int finalX = (loc.getX() + NumberUtils.random(randomize * 2) - randomize);
		int finalY = (loc.getY() + NumberUtils.random(randomize * 2) - randomize);
		Location created = Location.create(finalX, finalY, loc.getZ());
		return created;
	}

	public static boolean canMove(Entity mob, NormalDirection dir, int size, boolean npcCheck) {
		return PrimitivePathFinder.canMove(mob, mob.getLocation(), dir, size, npcCheck);
	}

	// TODO - fix npc checking
	public boolean canMove(NormalDirection dir, int size, boolean checkType) {
		//final int npcHeight = z;
	//	boolean checkingNPCs = checkType;
		if (dir == null) {
			return true;
		}
		switch (dir) {
		case WEST:
			for (int k = y; k < y + size; k++) {
				/*
				 * if (checkingNPCs &&
				 * TileControl.getSingleton().locationOccupied(x - 1, k,
				 * npcHeight, 1)) return false;
				 */
				if ((World.getMask(x - 1, k, z) & 0x1280108) != 0)
					return false;
			}
			break;
		case EAST:
			for (int k = y; k < y + size; k++) {
				/*
				 * if (checkingNPCs &&
				 * TileControl.getSingleton().locationOccupied(x + size, k,
				 * npcHeight, 1)) return false;
				 */
				if ((World.getMask(x + size, k, z) & 0x1280180) != 0)
					return false;
			}
			break;
		case SOUTH:
			for (int i = x; i < x + size; i++) {
				/*
				 * if (checkingNPCs &&
				 * TileControl.getSingleton().locationOccupied(i, y - 1,
				 * npcHeight, 1)) return false;
				 */
				if ((World.getMask(i, y - 1, z) & 0x1280102) != 0)
					return false;
			}
			break;
		case NORTH:
			for (int i = x; i < x + size; i++) {
				/*
				 * if (checkingNPCs &&
				 * TileControl.getSingleton().locationOccupied(i, y + size,
				 * npcHeight, 1)) return false;
				 */
				if ((World.getMask(i, y + size, z) & 0x1280120) != 0)
					return false;
			}
			break;
		case SOUTH_WEST:
			for (int i = x; i < x + size; i++) {
				int s = World.getMask(i, y - 1, z);
				int w = World.getMask(i - 1, y, z);
				int sw = World.getMask(i - 1, y - 1, z);
				/*
				 * if (checkingNPCs &&
				 * TileControl.getSingleton().locationOccupied(i - 1, y - 1,
				 * npcHeight, 1)) return false;
				 */
				if ((sw & 0x128010e) != 0 || (s & 0x1280102) != 0 || (w & 0x1280108) != 0)
					return false;
			}
			for (int k = y; k < y + size; k++) {
				int s = World.getMask(x, k - 1, z);
				int w = World.getMask(x - 1, k, z);
				int sw = World.getMask(x - 1, k - 1, z);
				/*
				 * if (checkingNPCs &&
				 * TileControl.getSingleton().locationOccupied(x - 1, k - 1,
				 * npcHeight, 1)) return false;
				 */
				if ((sw & 0x128010e) != 0 || (s & 0x1280102) != 0 || (w & 0x1280108) != 0)
					return false;
			}
			break;
		case SOUTH_EAST:
			for (int i = x; i < x + size; i++) {
				int s = World.getMask(i, y - 1, z);
				int e = World.getMask(i + 1, y, z);
				int se = World.getMask(i + 1, y - 1, z);
				/*
				 * if (checkingNPCs &&
				 * TileControl.getSingleton().locationOccupied(i + 1, y - 1,
				 * npcHeight, 1)) return false;
				 */
				if ((se & 0x1280183) != 0 || (s & 0x1280102) != 0 || (e & 0x1280180) != 0)
					return false;
			}
			for (int k = y; k < y + size; k++) {
				int s = World.getMask(x + size - 1, k - 1, z);
				int e = World.getMask(x + size, k, z);
				int se = World.getMask(x + size, k - 1, z);
				/*
				 * if (checkingNPCs &&
				 * TileControl.getSingleton().locationOccupied(x + 1, k - 1,
				 * npcHeight, 1)) return false;
				 */
				if ((se & 0x1280183) != 0 || (s & 0x1280102) != 0 || (e & 0x1280180) != 0)
					return false;
			}
			break;
		case NORTH_WEST:
			for (int i = x; i < x + size; i++) {
				int n = World.getMask(i, y + size, z);
				int w = World.getMask(i - 1, y + size - 1, z);
				int nw = World.getMask(i - 1, y + size, z);
				/*
				 * if (checkingNPCs &&
				 * TileControl.getSingleton().locationOccupied(i - 1, y + size,
				 * npcHeight, 1)) return false;
				 */
				if ((nw & 0x1280138) != 0 || (n & 0x1280102) != 0 || (w & 0x1280108) != 0)
					return false;
			}
			for (int k = y; k < y + size; k++) {
				int n = World.getMask(x, y, z);
				int w = World.getMask(x - 1, y, z);
				int nw = World.getMask(x - 1, y + 1, z);
				/*
				 * if (checkingNPCs &&
				 * TileControl.getSingleton().locationOccupied(x - 1, y + 1,
				 * npcHeight, 1)) return false;
				 */
				if ((nw & 0x1280138) != 0 || (n & 0x1280102) != 0 || (w & 0x1280108) != 0)
					return false;
			}
			break;
		case NORTH_EAST:
			for (int i = x; i < x + size; i++) {
				int n = World.getMask(i, y + size, z);
				int e = World.getMask(i + 1, y + size - 1, z);
				int ne = World.getMask(i + 1, y + size, z);
				/*
				 * if (checkingNPCs &&
				 * TileControl.getSingleton().locationOccupied(i + 1, y + size,
				 * npcHeight, 1)) return false;
				 */
				if ((ne & 0x12801e0) != 0 || (n & 0x1280120) != 0 || (e & 0x1280180) != 0)
					return false;
			}
			for (int k = y; k < y + size; k++) {
				int n = World.getMask(x + size - 1, k + 1, z);
				int e = World.getMask(x + size, k, z);
				int ne = World.getMask(x + size, k + 1, z);
				/*
				 * if (checkingNPCs &&
				 * TileControl.getSingleton().locationOccupied(x + size, k + 1,
				 * npcHeight, 1)) return false;
				 */
				if ((ne & 0x12801e0) != 0 || (n & 0x1280120) != 0 || (e & 0x1280180) != 0)
					return false;
			}
			break;
		}
		return true;
	}

	public boolean right(Location t) {
		return x > t.x;
	}

	public boolean left(Location t) {
		return x < t.x;
	}

	public boolean above(Location t) {
		return y > t.y;
	}

	public boolean under(Location t) {
		return y < t.y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public static Location createSurroundingTile(Entity entity, Entity other,
			NormalDirection direction) {
		Location loc = null;
		int x = other.getLocation().getX();
		int y = other.getLocation().getY();
		int z = other.getLocation().getZ();
		int size = 1;//entity.getSize();
		switch(direction) {
		//TODO: NE/SE/NW/SW?
		case NORTH:
			loc = Location.create(x, y + size, z);
			break;
		case EAST:
			loc = Location.create(x + size, y, z);
			break;
		case SOUTH:
			loc = Location.create(x, y - size, z);
			break;
		case WEST:
			loc = Location.create(x - size, y, z);
			break;
		default:
			return null;
		}
		return loc;
	}
}
