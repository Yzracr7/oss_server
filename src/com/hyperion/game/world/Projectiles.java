package com.hyperion.game.world;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.masks.Hits;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.Hits.HitType;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.utility.NumberUtils;

public class Projectiles {

	/**
	 * The id.
	 */
	private int id;

	/**
	 * The delay.
	 */
	private int startSpeed;

	/**
	 * The angle.
	 */
	private int angle;

	/**
	 * The speed.
	 */
	private int speed;

	/**
	 * The speak height.
	 */
	private int startHeight;

	/**
	 * The end height.
	 */
	private int endHeight;

	/**
	 * The lockon.
	 */
	private Entity lockon;

	/**
	 * The starting location
	 */
	private Location start;

	/**
	 * The finishing location
	 */
	private Location finish;

	private int slope;

	private int radius;

	private int hitDelay;

	/**
	 * Creates a new projectile
	 *
	 * @param start
	 * @param finish
	 * @param lockOn
	 * @param id
	 * @param startSpeed
	 * @param speed
	 * @param angle
	 * @param startHeight
	 * @param endHeight
	 * @return
	 */
	public static Projectiles create(Location start, Location finish, Entity lockOn, int id, int startSpeed, int speed,
			int angle, int startHeight, int endHeight) {
		return new Projectiles(start, finish, lockOn, id, startSpeed, speed, angle, startHeight, endHeight);
	}

	public static Projectiles create(Location start, Location finish, Entity lockOn, int id, int startSpeed, int speed,
			int angle, int startHeight, int endHeight, int slope, int radius) {
		return new Projectiles(start, finish, lockOn, id, startSpeed, speed, angle, startHeight, endHeight, slope,
				radius);
	}

	public static void sendTripleProjectiles(int gfx, int gfxHeight, int projectile, int projectileStartSpeed,
			int projectileSpeed, int delay, NPC npc, Player pTargets, String message, CombatType type, Hit hit,
			int attackTick, int hitDistance, int protectFromMageHit, boolean protectionImmune) {
		final int randomX1 = NumberUtils.random((int) (1));
		final int randomX2 = NumberUtils.random((int) (1));
		final int randomX3 = NumberUtils.random((int) (1));
		final int randomY1 = NumberUtils.random((int) (1));
		final int randomY2 = NumberUtils.random((int) (1));
		final int randomY3 = NumberUtils.random((int) (1));
		final int randomXPlusOrMinus1 = NumberUtils.random((int) (1));
		final int randomXPlusOrMinus2 = NumberUtils.random((int) (1));
		final int randomXPlusOrMinus3 = NumberUtils.random((int) (1));
		final int randomYPlusOrMinus1 = NumberUtils.random((int) (1));
		final int randomYPlusOrMinus2 = NumberUtils.random((int) (1));
		final int randomYPlusOrMinus3 = NumberUtils.random((int) (1));
		final Location firstLocation = new Location(
				randomXPlusOrMinus1 == 0 ? pTargets.getLocation().getX() - randomX1
						: pTargets.getLocation().getX() + randomX1,
				randomYPlusOrMinus1 == 0 ? pTargets.getLocation().getY() - randomY1
						: pTargets.getLocation().getY() + randomY1,
				pTargets.getLocation().getZ());
		final Location secondLocation = new Location(
				randomXPlusOrMinus2 == 0 ? pTargets.getLocation().getX() - randomX2
						: pTargets.getLocation().getX() + randomX2,
				randomYPlusOrMinus2 == 0 ? pTargets.getLocation().getY() - randomY2
						: pTargets.getLocation().getY() + randomY2,
				pTargets.getLocation().getZ());
		final Location thirdLocation = new Location(
				randomXPlusOrMinus3 == 0 ? pTargets.getLocation().getX() - randomX3
						: pTargets.getLocation().getX() + randomX3,
				randomYPlusOrMinus3 == 0 ? pTargets.getLocation().getY() - randomY3
						: pTargets.getLocation().getY() + randomY3,
				pTargets.getLocation().getZ());
		Projectiles send_projectile = Projectiles.create(npc.getCentreLocation(), firstLocation, null, projectile,
				projectileStartSpeed, projectileSpeed, 50, 40, 34, 25, 25);
		npc.executeProjectile(send_projectile);
		Projectiles send_projectile2 = Projectiles.create(npc.getCentreLocation(), secondLocation, null, projectile,
				projectileStartSpeed, projectileSpeed, 50, 40, 34, 25, 25);
		npc.executeProjectile(send_projectile2);
		Projectiles send_projectile3 = Projectiles.create(npc.getCentreLocation(), thirdLocation, null, projectile,
				projectileStartSpeed, projectileSpeed, 50, 40, 34, 25, 25);
		npc.executeProjectile(send_projectile3);
		Graphic graphic = new Graphic(gfx, gfxHeight, 0);
		pTargets.getPacketSender().sendStillGraphicsWithDelay(firstLocation, graphic, 0, delay);
		pTargets.getPacketSender().sendStillGraphicsWithDelay(secondLocation, graphic, 0, delay);
		pTargets.getPacketSender().sendStillGraphicsWithDelay(thirdLocation, graphic, 0, delay);
		pTargets.getCombatState().setCurrentAttacker(npc);
		pTargets.getCombatState().setPreviousAttacker(npc);

		CombatState.refreshPreviousHit(npc, pTargets);
		final Hit combatHit = hit;
		if (protectionImmune)  {
			combatHit.setType(HitType.NORMAL_DAMAGE);
			combatHit.setDamage(NumberUtils.random(protectFromMageHit));
		}
		final int tick = attackTick;

		World.getWorld().submit(new Tickable(tick) {
			@Override
			public void execute() {
				this.stop();
				if (combatHit.getDamage() > pTargets.getHp()) {
					combatHit.setDamage(pTargets.getHp());
				}
				if (pTargets.getLocation().getDistance2(firstLocation) < hitDistance
						|| pTargets.getLocation().getDistance2(secondLocation) < hitDistance
						|| pTargets.getLocation().getDistance2(thirdLocation) < hitDistance) {
					if (!message.isEmpty()) {
						pTargets.getPacketSender().sendMessage(message);
					}
					pTargets.animate(
							Animation.create(CombatAnimations.getDefendAnim(pTargets), AnimationPriority.NONE));
					
					if (!pTargets.getPrayers().isPrayerActive("Protect from Magic")) {
						
						combatHit.setDamage(NumberUtils.random(protectFromMageHit));
						if (combatHit.getDamage() > 0) {
							combatHit.setType(Hits.HitType.NORMAL_DAMAGE);
						}
					}
					pTargets.inflictDamage(combatHit, false);
				}
			}
		});
	}

	/**
	 * @param start
	 *            where it begins
	 * @param finish
	 *            where it finishes
	 * @param lockOn
	 *            the entity its going to follow
	 * @param id
	 *            - the projectile gfx id
	 * @param startSpeed
	 *            - speak speed
	 * @param speed
	 *            - speed it will travel at
	 * @param angle
	 *            - the angle
	 * @param startHeight
	 *            - speak height
	 * @param endHeight
	 *            - end height
	 */
	private Projectiles(Location start, Location finish, Entity lockOn, int id, int startSpeed, int speed, int angle,
			int startHeight, int endHeight) {
		this.start = start;
		if (lockOn == null) {
			setLockon(lockOn, finish);
		} else {
			setLockon(lockOn, lockOn.getLocation());
		}
		this.id = id;
		this.startSpeed = startSpeed;
		this.angle = angle;
		this.startHeight = startHeight;
		this.endHeight = endHeight;
		this.speed = speed;
	}

	private Projectiles(Location start, Location finish, Entity lockOn, int id, int startSpeed, int speed, int angle,
			int startHeight, int endHeight, int slope, int radius) {
		this.start = start;
		if (lockOn == null) {
			setLockon(lockOn, finish);
		} else {
			setLockon(lockOn, lockOn.getLocation());
		}
		this.id = id;
		this.startSpeed = startSpeed;
		this.angle = angle;
		this.startHeight = startHeight;
		this.endHeight = endHeight;
		this.speed = speed;
		this.slope = slope;
		this.radius = radius;
	}

	/**
	 * Sets the lockon entity.
	 *
	 * @param entity
	 *            The lockon.
	 * @param loc
	 *            TODO
	 */
	public void setLockon(Entity entity, Location loc) {
		if (entity == null) {
			this.lockon = null;
			this.finish = loc;
		} else {
			this.lockon = entity;
			this.finish = entity.getLocation();
		}
	}

	public void setMagicSpeed(Entity cast, Entity source) {
		this.start = cast.getLocation();
		this.finish = source.getLocation();
		int gfxDelay;
		if (Location.isWithinDistance(cast, source, 1)) {
			speed = 30;
		} else if (Location.isWithinDistance(cast, source, 5)) {
			speed = 40;
		} else if (Location.isWithinDistance(cast, source, 8)) {
			speed = 45;
		} else {
			speed = 55;
		}
		gfxDelay = speed + 20;
		setHitDelay((int) ((gfxDelay / 20) - 1));
	}

	public void setSpeedRange(Entity attacker, Entity victim) {
		this.start = attacker.getLocation();
		this.finish = victim.getLocation();
		int gfxDelay;
		if (Location.isWithinDistance(attacker, victim, 1)) {
			speed = 20;
		} else if (Location.isWithinDistance(attacker, victim, 3)) {
			speed = 25;
		} else if (Location.isWithinDistance(attacker, victim, 8)) {
			speed = 30;
		} else {
			speed = 40;
		}
		gfxDelay = speed + 20;
		hitDelay = (int) ((gfxDelay / 20) - 2);
	}

	public void setCannonSpeedRange(GameObject attacker, Entity victim) {
		this.start = attacker.getLocation();
		this.finish = victim.getLocation();
		int gfxDelay;
		if (Location.isWithinDistance(start, finish, 3, 1)) {
			speed = 20;
		} else if (Location.isWithinDistance(start, finish, 3, 3)) {
			speed = 25;
		} else if (Location.isWithinDistance(start, finish, 3, 8)) {
			speed = 30;
		} else {
			speed = 40;
		}
		gfxDelay = speed + 20;
		hitDelay = (int) ((gfxDelay / 20) - 2);
	}

	public void setSpeedRange(Entity attacker, Entity victim, boolean second) {
		this.start = attacker.getLocation();
		this.finish = victim.getLocation();
		int gfxDelay;
		if (attacker.getLocation().withinDistance(victim.getLocation(), 1, false)) {
			speed = 50;
		} else if (attacker.getLocation().withinDistance(victim.getLocation(), 3, false)) {
			speed = 50;
		} else if (attacker.getLocation().withinDistance(victim.getLocation(), 8, false)) {
			speed = 60;
		} else {
			speed = 65;
		}
		if (second) {
			speed += 15;
		}
		gfxDelay = speed + 20;
		hitDelay = (int) ((gfxDelay / 20) - 2);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStartSpeed() {
		return startSpeed;
	}

	public void setStartSpeed(int startSpeed) {
		this.startSpeed = startSpeed;
	}

	public int getAngle() {
		return angle;
	}

	public void setAngle(int angle) {
		this.angle = angle;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getStartHeight() {
		return startHeight;
	}

	public void setStartHeight(int startHeight) {
		this.startHeight = startHeight;
	}

	public int getEndHeight() {
		return endHeight;
	}

	public void setEndHeight(int endHeight) {
		this.endHeight = endHeight;
	}

	public Entity getLockon() {
		return lockon;
	}

	public Location getStart() {
		return start;
	}

	public void setStart(Location start) {
		this.start = start;
	}

	public Location getFinish() {
		return finish;
	}

	public void setFinish(Location finish) {
		this.finish = finish;
	}

	public int getHitDelay() {
		return hitDelay;
	}

	public void setHitDelay(int hitDelay) {
		this.hitDelay = hitDelay;
	}

	public int getSlope() {
		return slope;
	}

	public void setSlope(int slope) {
		this.slope = slope;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}
}
