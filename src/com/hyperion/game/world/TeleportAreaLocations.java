package com.hyperion.game.world;

import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.player.Player;

public class TeleportAreaLocations {

    public enum TeleportWizardLocations {
        KING_BLACK_DRAGON(new Location(3067, 10257, 0), 5000, "KBD");

        Location loc;
        int fee;
        String abv;

        TeleportWizardLocations(Location loc, int fee, String abv) {
            this.loc = loc;
            this.fee = fee;
            this.abv = abv;
        }

        public Location getLoc() {
            return loc;
        }

        public void setLoc(Location loc) {
            this.loc = loc;
        }

        public int getFee() {
            return fee;
        }

        public void setFee(int fee) {
            this.fee = fee;
        }

        public String getAbv() {
            return abv;
        }

        public void setAbv(String abv) {
            this.abv = abv;
        }

    }

    /**
     * Determines if the location is in a MultiCombat zone.
     *
     * @param loc The location.
     * @return In wilderness.
     */
    public static boolean isInMultiZone(Entity entity, Location loc) {
        int absX = loc.getX();
        int absY = loc.getY();
        if (entity.getAttributes().get("in_zulruh_room") != null || inNearTrolliem(loc) || scorpia(loc) || atGodwars(loc) || inFightPits(loc) || camelotMulti(loc) || ardyMulti(loc) || fallyMulti(loc)
                || draynorMulti(loc) || barbarianVillageMulti(loc) || alkharidMulti(loc) || atArmadylChamber(loc)
                || atBandosChamber(loc) || atTormentedDemons(loc) || atDagKingsLair(loc) || atKBDArea(loc)
                || atCorpArea(loc) || isInMultiAreaInFfa(loc) || atPestControlGameArea(loc)
                || atBandits(loc)
                || atKrakenArea(loc) || entity.getAttributes().isSet("cavesession") || atKqLair(loc) || entity.getAttributes().isSet("pestsession")
                || entity.getAttributes().isSet("clansession")) {
            return true;
        }
        return
                (absX >= 3136 && absX <= 3327 && absY >= 3520 && absY <= 3607) ||
                        (absX >= 3190 && absX <= 3327 && absY >= 3648 && absY <= 3839) ||
                        (absX >= 3200 && absX <= 3390 && absY >= 3840 && absY <= 3967) ||
                        (absX >= 2992 && absX <= 3007 && absY >= 3912 && absY <= 3967) ||
                        (absX >= 2946 && absX <= 2959 && absY >= 3816 && absY <= 3831) ||
                        (absX >= 3008 && absX <= 3199 && absY >= 3856 && absY <= 3903) ||
                        (absX >= 3008 && absX <= 3071 && absY >= 3600 && absY <= 3711) ||
                        (absX >= 3072 && absX <= 3327 && absY >= 3608 && absY <= 3647);
    }

    private static boolean camelotMulti(Location location) {
        return location.inArea(2651, 3409, 2687, 3447) || location.inArea(2647, 3486, 2664, 3513)
                || location.inArea(2820, 3455, 2891, 3525);
    }

    private static boolean ardyMulti(Location location) {
        return location.inArea(2513, 3215, 2543, 3251) || location.inArea(2659, 3220, 2684, 3254);
    }

    private static boolean fallyMulti(Location location) {
        return location.inArea(2946, 3391, 3008, 3456) || location.inArea(2943, 3305, 3008, 3391)
                || location.inArea(3008, 3303, 3024, 3327);
    }

    private static boolean draynorMulti(Location location) {
        return location.inArea(3105, 3232, 3135, 3261);
    }

    private static boolean barbarianVillageMulti(Location location) {
        return location.inArea(3073, 3390, 3138, 3453);
    }

    private static boolean alkharidMulti(Location location) {
        return location.inArea(3272, 3150, 3310, 3182);
    }

    /**
     * Determines if we are in the wilderness.
     *
     * @param l
     * @return
     */
    public static boolean inWilderness(Location l) {
        int x = l.getX();
        int y = l.getY();
        if ((x > 3009 && x < 3060 && y > 10303 && y < 10356)
                || (x > 2941 && x < 3392 && y > 3524 && y < 3966)
                || (x >= 3063 && x <= 3071 && y >= 10252 && y <= 10263)) {
            return true;
        }
        return false;
    }

    public static boolean inGodWars(Location l) {
        return l.inArea(2820, 5245, 2964, 5380);
    }

    public static boolean inAttackableArea(Location l) {
        return inDuelArenas(l) || inWilderness(l) || isInAttackableAreaInFfa(l);
    }

    public static boolean inChallengeArea(Location l) {
        return atDuelArena(l) || atClanWarsLobby(l);
    }

    /**
     * Determines if we are at duel arena challenge area.
     *
     * @param l
     * @return
     */
    public static boolean atDuelArena(Location l) {
        return l.inArea(3318, 3247, 3327, 3247) ||
                l.inArea(3324, 3247, 3328, 3264) ||
                l.inArea(3327, 3262, 3342, 3270) ||
                l.inArea(3342, 3262, 3387, 3280) ||
                l.inArea(3387, 3262, 3394, 3271) ||
                l.inArea(3312, 3224, 3325, 3247) ||
                l.inArea(3326, 3200, 3398, 3267); // Entire arena
    }

    /**
     * Determines if the location is in the duel arenas.
     *
     * @param l
     * @return
     */
    public static boolean inDuelArenas(Location l) {
        //West arenas
        return (l.inArea(3331, 3243, 3359, 3259)
                || l.inArea(3331, 3224, 3359, 3240)
                || l.inArea(3331, 3205, 3359, 3221)
                //East Arenas
                || l.inArea(3362, 3205, 3390, 3221)
                || l.inArea(3362, 3224, 3390, 3240)
                || l.inArea(3362, 3243, 3390, 3259))
                &&
                //Not in 2 center paths
                !(l.inArea(3356, 3237, 3365, 3246)
                        || l.inArea(3356, 3218, 3365, 3227));
    }

    /**
     * Determines if the location is at free for all clan wars
     *
     * @param loc
     * @return in ffa clan wars
     */
    public static boolean isInFreeForAllClanWars(Location loc) {
        return loc.inArea(2752, 5505, 2878, 5630);
    }

    /**
     * Determines if the location is within an attackable area at ffa clan wars.
     *
     * @param loc
     * @return in ffa clan wars
     */
    private static boolean isInAttackableAreaInFfa(Location loc) {
        return loc.inArea(2752, 5512, 2878, 5630);
    }

    /**
     * Determines if the location is within the multi line at ffa.
     *
     * @param loc
     * @return
     */
    private static boolean isInMultiAreaInFfa(Location loc) {
        return loc.inArea(2756, 5512, 2879, 5569);//2798, 5627);
    }

    /**
     * Determines if the location is in the cyclops room.
     *
     * @param loc
     * @return
     */
    public static boolean inCyclopsRoom(Location loc) {
        return loc.inArea(loc.getZ(), 2, 2838, 3543, 2876, 3556) || loc.inArea(loc.getZ(), 2, 2847, 3532, 2876, 3542);
    }

    /**
     * Determines if the location is within mage arena.
     *
     * @param l
     * @return
     */
    public static boolean atMageArena(Location l) {
        return l.inArea(3092, 3921, 3117, 3946);
    }

    /**
     * Determines if the location is within fight pits.
     *
     * @param l
     * @return
     */
    private static boolean inFightPits(Location l) {
        return l.inArea(2376, 5127, 2422, 5168);
    }

    /**
     * Determines if the location is within fight pits.
     *
     * @param l
     * @return
     */
    private static boolean inNearTrolliem(Location l) {
        return l.inArea(2845, 3593, 2931, 3647);
    }

    private static boolean scorpia(Location l) {
        return l.inArea(3218, 10329, 3249, 10353);
    }

    /**
     * Determines if the location is within tzhaar.
     *
     * @param l
     * @return
     */
    public static boolean inTzHaar(Location l) {
        return l.inArea(2370, 5120, 2541, 5185);
    }

    /**
     * Determines if the location is within PVP.
     *
     * @param entity
     * @return
     */
    public static boolean isInPVP(Entity entity) {
        Location loc = entity.getLocation();
        return inWilderness(loc);
    }

    /**
     * Determines if the entity is within an attackable area.
     *
     * @param entity
     * @return
     */
    public static boolean isInAttackableArea(Entity entity) {
        Location loc = entity.getLocation();
        return isInAttackableAreaInFfa(loc) || isInPVP(entity) || inDuelArenas(loc);
    }

    public static boolean atFishingGuild(Location location) {
        return location.inArea(2579, 3393, 2616, 3426);
    }

    public static boolean atCraftingGuild(Location location) {
        return location.inArea(2914, 3266, 2944, 3292);
    }

    public static boolean atWarriorsGuild(Location location) {
        return location.inArea(2837, 3534, 2876, 3556);
    }

    public static boolean atBarrows(Location location) {
        return (location.inArea(3545, 3265, 3586, 3312) || location.inArea(3567, 9701, 3580, 9711)
                || location.inArea(3548, 9709, 3561, 9721) || location.inArea(3549, 9691, 3562, 9706) || location.inArea(3532, 9698, 3546, 9710)
                || location.inArea(3544, 9677, 3559, 9689) || location.inArea(3563, 9680, 3577, 9694));
    }

    public static boolean atSlayerTower(Location location) {
        return location.inArea(3402, 3529, 3453, 3581);
    }

    public static boolean atMageBank(Location location) {
        return location.inArea(2528, 4710, 2548, 4723) || location.inArea(2497, 4681, 2522, 4730);
    }

    public static boolean atDessousArea(Location location) {
        return location.inArea(3557, 3392, 3583, 3413);
    }

    public static boolean atKamilArea(Location location) {
        return location.inArea(2823, 3807, 2853, 3812);
    }

    public static boolean atFareedArea(Location location) {
        return location.inArea(3305, 9358, 3325, 9393);
    }

    public static boolean atDamisArea(Location location) {
        return location.inArea(2719, 5066, 2751, 5117);
    }

    private static boolean atKqLair(Location location) {
        return location.inArea(3460, 3508, 9479, 9518);
    }

    private static boolean atKBDArea(Location location) {
        return location.inArea(2251, 4676, 2289, 4716);
    }

    public static boolean atBarrelChestArea(Location location) {
        return location.inArea(3806, 2840, 3816, 2848);
    }

    public static boolean atHomeArea(Location location) {
        return location.inArea(3466, 3462, 3518, 3516);
    }

    public static boolean alkharidBank(Location location) {
        return location.inArea(3265, 3161, 3272, 3173);
    }

    public static boolean atArmadylChamber(Location location) {
        return location.inArea(2824, 5296, 2842, 5308);
    }

    public static boolean atBandosChamber(Location location) {
        return location.inArea(2864, 5351, 2876, 5369);
    }

    public static boolean atSaradominChamber(Location location) {
        return location.inArea(2889, 5258, 2907, 5276);
    }

    private static boolean atGodwars(Location location) {
        return location.inArea(2820, 5245, 2964, 5380);
    }

    public static boolean atBandits(Location location) {
        return location.inArea(3156, 2975, 3166, 2991);
    }

    public static boolean atKrakenArea(Location location) {
        return location.inArea(2365, 4667, 2433, 4693);
    }

    public static boolean atDaggs(Location location) {
        return location.inArea(2425, 10104, 2570, 10184);
    }

    private static boolean atTormentedDemons(Location location) {
        return location.inArea(2563, 5697, 2630, 5766);
    }

    private static boolean atDagKingsLair(Location location) {
        return location.inArea(2898, 4434, 2932, 4464);
    }

    public static boolean atChaosAltarArea(Location location) {
        return location.inArea(2945, 3815, 2957, 3826);
    }

    public static boolean atPiratesClueArea(Location location) {
        return location.inArea(3038, 3949, 3044, 3959);
    }

    public static boolean atGoblinClueArea(Location location) {
        return location.inArea(2954, 3510, 2961, 3515);
    }

    private static boolean atCorpArea(Location location) {
        return location.inArea(2918, 4352, 2971, 4416);
    }

    public static boolean atRockCrabs(Location location) {
        return location.inArea(2597, 3642, 2692, 3737);
    }

    public static boolean atMiscMonsters(Location location) {
        return location.inArea(2693, 9408, 2753, 9472);
    }

    public static boolean atCorpEntrance(Location location) {
        return location.inArea(2880, 4357, 2920, 4424);
    }

    private static boolean atMageBankHotZone(Location location) {
        return location.inArea(2944, 3904, 3393, 3971);
    }

    private static boolean at44sHotZone(Location location) {
        return location.inArea(2946, 3815, 3046, 3903);
    }

    private static boolean atOtherHotZones(Location location) {
        return location.inArea(3143, 3522, 3398, 3825);
    }

    public static boolean isInHotZones(Location loc) {
        return (atMageBankHotZone(loc) || at44sHotZone(loc) || atOtherHotZones(loc));
    }

    public static boolean atPestControlGameArea(Location loc) {
        return loc.inArea(2625, 2555, 2686, 2622);
    }

    public static boolean inPestControlWaitingBoat(Location loc) {
        return loc.inArea(2660, 2638, 2663, 2644);
    }

    public static boolean atClanWarsLobby(Location base) {
        return base.inArea(6464, 3672, 6479, 3695);
    }

    public static void sendLoginInterfaces(Player player) {
        // TODO Auto-generated method stub
        //Location loc = player.getLocation();
        //SEE: AreasHandler
        /*player.getFrames().sendInterfaceConfig(274, 4, false);
		player.getVariables().sendQuestTab();
		player.getVariables().sendEp(true);
		if (!inWilderness(loc)) {
			player.getVariables().setSafeZoneTimerStarted(false, false);
		}
		if (isInMultiZone(player, loc)) {
			player.getFrames().sendInterfaceConfig(745, 1, true);
			player.getAttributes().set("multi", true);
		} else if (!isInMultiZone(player, loc)) {
			player.getAttributes().remove("multi");
			player.getFrames().sendInterfaceConfig(745, 1, false);
		}*/
        //FogWaitingLobby.currentWaiters.add(player);
        //FogWaitingLobby.displayLobbyInterface(player);
    }
}
