package com.hyperion.game.world;

import com.hyperion.Logger;
import com.hyperion.cache.UpdateServer;
import com.hyperion.game.Constants;
import com.hyperion.game.GameEngine;
import com.hyperion.game.SQLWorldLoader;
import com.hyperion.game.WorldLoader;
import com.hyperion.game.content.clanchat.ClanChat;
import com.hyperion.game.content.clues.ClueScrollManagement;
import com.hyperion.game.content.commands.CommandHandler;
import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.content.skills.SkillLoaders;
import com.hyperion.game.discord.Discord;
import com.hyperion.game.event.EventHandler;
import com.hyperion.game.item.Item;
import com.hyperion.game.net.PacketManager;
import com.hyperion.game.packet.PacketHandler;
import com.hyperion.game.task.BlockingExecutorService;
import com.hyperion.game.task.Task;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.tickable.TickableManager;
import com.hyperion.game.tickable.impl.*;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.event.Event;
import com.hyperion.game.world.event.EventManager;
import com.hyperion.game.world.gameobjects.DoorManager;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.game.world.gameobjects.ObjectManager;
import com.hyperion.game.world.grounditems.GroundItem;
import com.hyperion.game.world.grounditems.GroundItemManager;
import com.hyperion.game.world.region.MapBuilder;
import com.hyperion.game.world.region.Mapdata;
import com.hyperion.game.world.region.Region;
import com.hyperion.game.world.region.RegionManager;
import com.hyperion.game.world.shopping.ShopManager;
import com.hyperion.sql.login.LoginManager;
import com.hyperion.utility.*;
import com.hyperion.utility.json.JsonManager;
import com.hyperion.utility.logger.LoggingThread;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
//import com.hyperion.game.SQLWorldLoader;

/**
 * Holds data global to the game world.
 *
 * @author Graham Edgecombe d
 */
public class World {

    /**
     * An executor service which handles background loading tasks.
     */
    private BlockingExecutorService backgroundLoader = new BlockingExecutorService(Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()));

    private static final Player[] players = new Player[Constants.MAX_PLAYERS];
    private static CopyOnWriteArrayList<Player> activePlayers = new CopyOnWriteArrayList<Player>();
    private static CopyOnWriteArrayList<String> playersInWilderness = new CopyOnWriteArrayList<String>();
    private static CopyOnWriteArrayList<String> possibleTargets = new CopyOnWriteArrayList<String>();
    private static final NPC[] npcs = new NPC[Constants.MAX_NPCS];
    private static int playerCount = 0, npcCount = 0;
    private int votesClaimed = 0;
    private boolean updateInProgress;
    private final long serverStartupTime = System.currentTimeMillis();
    private static Map<String, Long> pingMap = new ConcurrentHashMap<String, Long>();

    private static final SimpleDateFormat sdf =
            new SimpleDateFormat("hh:mm a z");//"EEE, MMM d, yyyy hh:mm:ss a z");

    private static ConcurrentHashMap<Integer, CopyOnWriteArrayList<GameObject>> objectsToDelete = new ConcurrentHashMap<Integer, CopyOnWriteArrayList<GameObject>>();

    private static final World world = new World();
    private GameEngine engine;
    private EventManager eventManager;
    private TickableManager tickManager;
    private ObjectManager objectManager;
    private WorldLoader loader;
    private RegionManager regionManager = new RegionManager();

    // private ForumSQL forumSql;
    //private SqlCentre sqlCentre;
    public Mapdata mapdata;
    public LoggingThread loggingThread;

    /**
     * Creates the world and begins background loading tasks.
     */
    public World() {
        backgroundLoader.submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                // Js5Engine.getJs5Engine();
                if (!Constants.USE_SEPARATE_FILE_SERVER)
                    UpdateServer.loadCache();
                if (Constants.USE_DEV_CACHE)
                    mapdata = new Mapdata();
                MapBuilder.init();
                return null;
            }
        });
        backgroundLoader.submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                Loaders.loadAllDefinitions();
                return null;
            }
        });
        backgroundLoader.submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                SkillLoaders.load();
                ShopManager.load();
                return null;
            }
        });
        backgroundLoader.submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                DialogueManager.load();
                ClueScrollManagement.load();
                CommandHandler.initialize();
                JsonManager.getSingleton().initialize();
                EventHandler.initialize();
                return null;
            }
        });
        backgroundLoader.submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                ClanChat.init();
                Censor.load();
                return null;
            }
        });
        backgroundLoader.submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                StarterMap.init();
                MACStarterMap.init();
                sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                return null;
            }
        });
        backgroundLoader.submit(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                //GEResourceManager.init();
                //GrandExchangeDatabase.init();
                return null;
            }
        });
    }

    /**
     * Initialises the world: loading configuration and registering global
     * events.
     *
     * @param engine The engine processing this world's tasks.
     * @throws IOException            if an I/O error occurs loading configuration.
     * @throws ClassNotFoundException if a class loaded through reflection was not found.
     * @throws IllegalAccessException if a class could not be accessed.
     * @throws InstantiationException if a class could not be created.
     * @throws IllegalStateException  if the world is already initialised.
     */
    public void init(GameEngine engine) throws Exception, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, FileNotFoundException {
        if (this.engine != null) {
            throw new IllegalStateException("The world has already been initialised.");
        } else {
            this.objectManager = new ObjectManager();
            this.engine = engine;
            this.eventManager = new EventManager(engine);
            this.tickManager = new TickableManager();
            loggingThread = new LoggingThread();
            this.registerGlobalEvents();
            this.registerGlobalTicks();
            this.loadConfiguration();
            //ObjectManager.loadCustomObjects();
            this.getGlobalObjects().loadLevers();
            DoorManager.init();
        }
    }

    public static Player getPlayer(String name) {
        for (int i = 0; i < Constants.MAX_PLAYERS; i++) {
            if (players[i] != null && players[i].getName().equalsIgnoreCase(name)) {
                return players[i];
            }
        }

        return null;
    }

    /**
     * Loads server configuration.
     *
     * @throws IOException            if an I/O error occurs.
     * @throws ClassNotFoundException if a class loaded through reflection was not found.
     * @throws IllegalAccessException if a class could not be accessed.
     * @throws InstantiationException if a class could not be created.
     */
    private void loadConfiguration() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        FileInputStream fis = new FileInputStream("data/configuration.cfg");
        try {
            ConfigurationParser p = new ConfigurationParser(fis);
            Map<String, String> mappings = p.getMappings();
            if (mappings.containsKey("worldLoader")) {
                String worldLoaderClass = mappings.get("worldLoader");
                Class<?> loader = Class.forName(worldLoaderClass);
                this.loader = (WorldLoader) loader.newInstance();
                Logger.getInstance().warning("WorldLoader set to : " + worldLoaderClass);
            } else {
                this.loader = new SQLWorldLoader();
                Logger.getInstance().warning("WorldLoader set to default");
            }
            Map<String, Map<String, String>> complexMappings = p.getComplexMappings();
            if (complexMappings.containsKey("packetHandlers")) {
                Map<Class<?>, Object> loadedHandlers = new HashMap<Class<?>, Object>();
                for (Map.Entry<String, String> handler : complexMappings.get("packetHandlers").entrySet()) {
                    int id = Integer.parseInt(handler.getKey());
                    Class<?> handlerClass = Class.forName(handler.getValue());
                    Object handlerInstance;
                    if (loadedHandlers.containsKey(handlerClass)) {
                        handlerInstance = loadedHandlers.get(loadedHandlers.get(handlerClass));
                    } else {
                        handlerInstance = handlerClass.newInstance();
                    }
                    PacketManager.getPacketManager().bind(id, (PacketHandler) handlerInstance);
                    //	if (Constants.DEBUG_MODE)
                    //	Logger.getInstance().warning("Bound " + handler.getValue() + " to opcode : " + id);
                }
            }
        } finally {
            fis.close();
        }
    }

    public int setRandomIndex(Player player) {
        activePlayers.remove(player);
        player.getAttributes().remove("random_pid");
        int index = NumberUtils.random(1, 2047);
        activePlayers.add(player);
        player.getAttributes().set("random_pid", index);
        return index;
    }

    public int addPlayer(Player player) {
        for (int i = 1; i < players.length; i++) {
            if (players[i] == null) {
                playerCount++;
                players[i] = player;
                player.setIndex(i);
                setRandomIndex(player);
                return i;
            }
        }
        return -1;
    }

    public int removePlayer(Player player) {
        if (player != null) {
            int index = player.getIndex();
            if (players[index] != null) {
                playerCount--;
                players[index] = null;
                activePlayers.remove(player);
                player.getAttributes().remove("random_pid");
                return index;
            }
            return -1;
        }
        return -1;
    }

    public int containsPlayer(Player player) {
        int index = player.getIndex();
        if (players[index] != null) {
            if (players[index] == player) {
                return index;
            }
        }
        return -1;
    }

    public Player getPlayer(int index) {
        if (players[index] != null) {
            return players[index];
        }
        return null;
    }

    public NPC getNPC(int index) {
        if (npcs[index] != null) {
            return npcs[index];
        }
        return null;
    }

    public void addPing(String name) {
        if (pingMap.get(name) != null)
            pingMap.remove(name);
        pingMap.put(name, System.currentTimeMillis());
    }

    public void setLogoutPing(String name) {
        pingMap.remove(name);
    }

    public Long getLastPing(String name) {
        if (pingMap.get(name) != -1) {
            // Logged in
            return pingMap.get(name);
        } else {
            return (long) -1;
        }
    }

    public boolean isActivePing(String name) {
        if (pingMap.get(name) != -1) {
            return true;
        }
        return false;
    }

    public Map<String, Long> getPingMap() {
        return pingMap;
    }

    public ConcurrentHashMap<Integer, CopyOnWriteArrayList<GameObject>> getObjectsToDelete() {
        return objectsToDelete;
    }

    public boolean npc_already_spawned_in_world(Location spawn) {
        for (NPC npc : npcs) {
            if (npc != null) {
                if (npc.getSpawnLocation() != null) {
                    if (npc.getSpawnLocation() == spawn) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Registers a new npc.
     *
     * @param npc The npc to register.
     */
    public int addNPC(NPC npc) {
        for (int i = 1; i < npcs.length; i++) {
            if (npcs[i] == null) {
                npcCount++;
                npcs[i] = npc;
                npc.setIndex(i);
                npc.animate(-1);
                npc.loadMapRegions();
                return i;
            }
        }
        return -1;
    }

    /**
     * Unregisters an old npc.
     *
     * @param npc The npc to unregister.
     */
    public int removeNPC(NPC npc) {
        int index = npc.getIndex();
        if (index != -1) {
            if (npcs[index] != null) {
                npcCount--;
                npcs[index] = null;
                npc.setIndex(-1);
                World.updateEntityRegion(npc, true);
                return index;
            }
        }
        return -1;
    }

    /*
     * Unregisters all NPCs on the players list
     */
    public void unregister_player_npcs(Player player) {
        for (NPC npc : player.getVariables().getOwnedNPCS()) {
            if (npc != null) {
                player.getVariables().getOwnedNPCS().remove(npc);
                World.getWorld().removeNPC(npc);
            }
        }
    }

    /*
     * Unregisters only the specified NPC
     */
    public void unregister_player_npc(Player player, NPC npc) {
        if (npc == null)
            return;
        for (NPC npc2 : player.getVariables().getOwnedNPCS()) {
            if (npc2 != null && npc2.getIndex() == npc.getIndex()) {
                player.getVariables().getOwnedNPCS().remove(npc);
                World.getWorld().removeNPC(npc);
            }
        }
    }

    public int containsNPC(NPC npc) {
        for (int i = 1; i < npcs.length; i++) {
            if (npcs[i] != null) {
                if (npcs[i] == npc) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * This method sends a message to all the players online
     *
     * @param message   The message
     * @param staffOnly If only staff should receive the message
     */
    public void sendGlobalMessage(String message, boolean staffOnly) {
        for (Player player : World.getWorld().getPlayers()) {
            if (player != null) {
                if (!staffOnly || (staffOnly && player.getDetails().isStaff()))
                    player.getPacketSender().sendMessage("[<col=556B2F>Broadcast</col>] " + message);
            }
        }
        /*for(String name : getActivePlayers()) {
            Player player = find_player_by_name(name);
			if(player == null) {
				World.getWorld().getActivePlayers().remove(name);
				continue;
			} else {
				if(!staffOnly || (staffOnly && player.getDetails().isStaff()))
				player.getPacketSender().sendMessage(message);
			}
		}*/
    }

    /**
     * Registers global events such as updating.
     */
    private void registerGlobalEvents() {
        // submit(new UpdateEvent());
    }

    /**
     * Registers global ticks
     */
    private void registerGlobalTicks() {
        submit(new CleanupTick());
        submit(new MinuteTick());
        submit(new ThirtySecondTick());
        submit(new SecondTick());
        submit(new PingTick());
        submit(new PunishmentTick());
        submit(new BountyTick());
        submit(new GrandExchangeMap());
        submit(new DiscordSlaveTick());

        if (Constants.SERVER_HOSTED) {
            submit(new BackupGeneratorTick());
        }

    }

    /**
     * Submits a new event.
     *
     * @param event The event to submit.
     */
    public void submit(Event event) {
        this.eventManager.submit(event);
    }

    /**
     * Submits a new task.
     *
     * @param task The task to submit.
     */
    public void submit(Task task) {
        this.engine.pushTask(task);
    }

    /**
     * Submits a new tickable.
     *
     * @param tickable The tickable to submit.
     */
    public void submit(final Tickable tickable) {
        // submit(new Task() {
        // @Override
        // public void execute(GameEngine context) {
        // DO NOT REMOVE THIS CODE, IT PREVENTS CONCURRENT MODIFICATION
        // PROBLEMS!
        World.this.tickManager.submit(tickable);
        // }
        // });
    }

    public void send_regional_graphic(Location location, int gfx) {
        /*
         * for (Region regions :
         * this.getRegionManager().getRegionByLocation(location
         * ).getSurroundingRegions()) { for (Player players :
         * regions.getPlayers()) {
         * players.getFrames().sendStillGraphics(location, Graphic.create(gfx,
         * 0), 0); } }
         */
    }

    @SuppressWarnings("unused")
    public void send_regional_projectile(Entity receiver, Location start, Location dest, int startSpeed, int gfx, int angle, int startHeight, int endHeight, int speed) {
        int offsetX = 3;
        int offsetY = 2;
        /*
         * for (Region regions :
         * this.getRegionManager().getRegionByLocation(speak
         * ).getSurroundingRegions()) { for (Player players :
         * regions.getPlayers()) {
         * //players.getFrames().sendProjectile2(offsetX, offsetY, speak, dest,
         * startSpeed, gfx, angle, startHeight, endHeight, speed, null); } }
         */
    }

    /**
     * Handles an exception in any of the pools.
     *
     * @param t The exception.
     */
    public void handleError(Throwable t) {
        for (Player players : getPlayers()) {
            if (players != null) {
                Logger.getInstance().warning("Kicked player " + players.getName() + " due to error in World class");
                LoginManager.unregister_player(players);
            }
        }
        Logger.getInstance().warning("An error occurred in an executor service! The server will be halted immediately.");
        t.printStackTrace();
        System.exit(1);
    }

    public boolean isPlayerOnline(String name) {
        name = TextUtils.formatName(name);
        for (Player player : players) {
            if (player != null) {
                if (player.getName().equalsIgnoreCase(name)) {
                    return true;
                }
            }
        }
        return false;
    }

    public Player find_player_by_name(String name) {
        name = TextUtils.formatName(name);
        for (Player player : players) {
            if (player != null) {
                if (player.getName().equalsIgnoreCase(name)) {
                    return player;
                }
            }
        }
        return null;
    }

    public boolean isUpdateInProgress() {
        return updateInProgress;
    }

    public void setUpdateInProgress(boolean updateInProgress) {
        Discord.speak("```css" +
                "Server update now in progress, if you are logged in please restart your client shortly... If you are not logged in and trying to login, you must wait till the update is over.```");
        this.updateInProgress = updateInProgress;
    }

    public long getServerStartupTime() {
        return serverStartupTime;
    }

    public String getServerTime() {
        final Date currentTime = new Date();
        return sdf.format(currentTime);
    }

    public String getTime() {
        long l = System.currentTimeMillis() - World.getWorld().getServerStartupTime();
        long days = (l / 86400000L);
        long hours = ((l / 3600000L) % 24L);
        long minutes = ((l / 60000L) % 60L);
        // long seconds = ((l / 1000L) % 60L);
        String string = "";
        if (days > 0) {
            String s = days == 1 ? " day " : " days ";
            string += days + s;
        }
        if (hours > 0) {
            String s = hours == 1 ? " hour " : " hours ";
            string += hours + s;
        }
        if (minutes > 0) {
            String s = minutes == 1 ? " min " : " mins ";
            string += minutes + s;
        }
        if (string == "")
            string = "1 minute";
        return string;
    }

    public RegionManager getRegionManager() {
        return regionManager;
    }

    public final Region getRegion(int id, boolean force) {
        return regionManager.getRegion(id, force);
    }

    public static World getWorld() {
        return world;
    }

    public TickableManager getTickableManager() {
        return tickManager;
    }

    public BlockingExecutorService getBackgroundLoader() {
        return backgroundLoader;
    }

    public WorldLoader getWorldLoader() {
        return loader;
    }

    public GameEngine getEngine() {
        return engine;
    }

    public NPC[] getNPCS() {
        return npcs;
    }

    public Player[] getPlayers() {
        return players;
    }

    public CopyOnWriteArrayList<Player> getActivePlayers() {
        return activePlayers;
    }

    public CopyOnWriteArrayList<String> getPlayersInWilderness() {
        return playersInWilderness;
    }

    public CopyOnWriteArrayList<String> getPossibleTargets() {
        return possibleTargets;
    }

    public int getPlayerCount() {
        return playerCount;
    }

    public int getNPCCount() {
        return npcCount;
    }

    public ObjectManager getGlobalObjects() {
        return objectManager;
    }

    public static final void updateEntityRegion(Entity entity, boolean logged) {
        if (logged) {
            if (entity.isPlayer())
                getWorld().getRegion(entity.getLastRegionId(), false).removePlayerIndex(entity.getIndex());
            else
                getWorld().getRegion(entity.getLastRegionId(), false).removeNPCIndex(entity.getIndex());
            return;
        }
        int regionId = entity.getLocation().getRegionId();
        if (entity.getLastRegionId() != regionId) { // map region entity at
            // changed
            if (entity.isPlayer()) {
                if (entity.getLastRegionId() > 0)
                    getWorld().getRegion(entity.getLastRegionId(), false).removePlayerIndex(entity.getIndex());
                Region region = getWorld().getRegion(regionId, false);
                region.addPlayerIndex(entity.getIndex());
            } else {
                if (entity.getLastRegionId() > 0)
                    getWorld().getRegion(entity.getLastRegionId(), false).removeNPCIndex(entity.getIndex());
                getWorld().getRegion(regionId, false).addNPCIndex(entity.getIndex());
            }
            entity.setLastRegionId(regionId);
        } else {

        }
    }

    public static int getClipedOnlyMask(int x, int y, int plane) {
        Location tile = Location.create(x, y, plane);
        Region region = getWorld().getRegion(tile.getRegionId(), false);
        if (region == null)
            return -1;
        return region.getMaskClipedOnly(tile.getZ(), tile.getXInRegion(), tile.getYInRegion());
    }

    public static int getMask(int x, int y, int z) {
        Location tile = Location.create(x, y, z);
        Region region = getWorld().getRegion(tile.getRegionId(), false);
        if (region == null) {
            return -1;
        }
        return region.getMask(tile.getZ(), tile.getXInRegion(), tile.getYInRegion());
    }

    public static final GameObject getWallObject(Location tile) {
        return getWorld().getRegion(tile.getRegionId(), false).getWallObject(tile.getZ(), tile.getXInRegion(), tile.getYInRegion());
    }

    public static final GameObject getObjectWithId(Location tile, int id) {
        return getWorld().getRegion(tile.getRegionId(), false).getObjectWithId(tile.getZ(), tile.getXInRegion(), tile.getYInRegion(), id);
    }

    private static final boolean isSpawnedObject(GameObject object) {
        return getWorld().getRegion(object.getLocation().getRegionId(), false).getSpawnedObjects().contains(object);
    }

    public static final void spawnObject(GameObject object) {
        object.setAlive(true);
        getWorld().getRegion(object.getLocation().getRegionId(), false).spawnObject(object, object.getPlane(), object.getLocation().getXInRegion(), object.getLocation().getYInRegion(), false);
    }

    public static final void unclipTile(Location tile) {
        getWorld().getRegion(tile.getRegionId(), false).unclip(tile.getZ(), tile.getXInRegion(), tile.getYInRegion());
    }

    public static final void clipObject(GameObject obj) {
        getWorld().getRegion(obj.getLocation().getRegionId(), false).clip(obj, obj.getLocation().getXInRegion(), obj.getLocation().getYInRegion());
    }

    public static final void removeObject(GameObject object) {
        Location loc = object.getLocation();
        object.setAlive(false);
        getWorld().getRegion(loc.getRegionId(), false).removeObject(object, loc.getZ(), loc.getXInRegion(), loc.getYInRegion());
    }

    public static final void spawnFakeObjectTemporary(final Player player, final GameObject temp, final GameObject original, int delayTillStart, final int delayTillFinish) {
        if (delayTillStart != -1) {
            World.getWorld().submit(new Tickable(delayTillStart) {
                @Override
                public void execute() {
                    player.executeObjectChange(temp.getLocation(), temp.getId(), temp.getType(), temp.getRotation());
                    World.getWorld().submit(new Tickable(delayTillFinish) {
                        @Override
                        public void execute() {
                            player.executeObjectChange(original.getLocation(), original.getId(), original.getType(), original.getRotation());
                            this.stop();
                        }
                    });
                    this.stop();
                }
            });
        } else {
            player.executeObjectChange(temp.getLocation(), temp.getId(), temp.getType(), temp.getRotation());
            World.getWorld().submit(new Tickable(delayTillFinish) {
                @Override
                public void execute() {
                    player.executeObjectChange(original.getLocation(), original.getId(), original.getType(), original.getRotation());
                    this.stop();
                }
            });
        }
    }

    public static final void spawnObjectTemporary(final GameObject object, int time) {
        spawnObject(object);
        World.getWorld().submit(new Tickable(time) {
            @Override
            public void execute() {
                if (!World.isSpawnedObject(object))
                    return;
                removeObject(object);
                this.stop();
            }
        });
    }

    public static final void spawnFireTemporary(final GameObject object, int time, final Player player) {
        spawnObject(object);
        final Location loc = object.getLocation();
        World.getWorld().submit(new Tickable(time) {
            @Override
            public void execute() {
                if (!World.isSpawnedObject(object))
                    return;
                removeObject(object);

                GroundItem groundItem = new GroundItem(new Item(592, 1), loc,
                        player);
                groundItem.setSpawnDelay(-1);
                GroundItemManager.create(groundItem, player);

                //GroundItem ashes = new GroundItem(new Item(592, 1), loc, null);
                //ashes.setGlobal(true);
                //ashes.setLocation(loc);
                //GroundItemManager.create(ashes, player);
                this.stop();
            }
        });
    }

    public static final void replace(final GameObject o, int time, Player plr, final GameObject rep) {
        final int oldId = o.getId();
        if (o.getId() == rep.getId())
            return;
        o.setAlive(false);
        plr.executeObjectChange(o.getLocation(), rep.getId(), rep.getType(), rep.getRotation());
        World.getWorld().submit(new Tickable(time) {
            @Override
            public void execute() {
                if (o.getId() == oldId) {
                    this.stop();
                }
                plr.executeObjectChange(o.getLocation(), oldId, o.getType(), o.getRotation());
                o.setAlive(true);
                // o.regenerateHealth();
                this.stop();
            }
        });
    }


    public static final void replaceResourceObject(final GameObject o, int time, final Player player, int newId) {
        final int oldId = o.getId();
        if (o.getId() == newId)
            return;
        o.setAlive(false);
        player.executeObjectChange(o.getLocation(), newId, o.getType(), o.getRotation());
        World.getWorld().submit(new Tickable(time) {
            @Override
            public void execute() {
                if (o.getId() == oldId) {
                    this.stop();
                }
                player.executeObjectChange(o.getLocation(), oldId, o.getType(), o.getRotation());
                o.setAlive(true);
                // o.regenerateHealth();
                this.stop();
            }
        });
    }

    public static final void spawnObjectTemporary(final GameObject object, int delayTillStart, final int delayTillRemove) {
        World.getWorld().submit(new Tickable(delayTillStart) {
            @Override
            public void execute() {
                spawnObject(object);
                World.getWorld().submit(new Tickable(delayTillRemove) {
                    @Override
                    public void execute() {
                        if (!World.isSpawnedObject(object))
                            return;
                        removeObject(object);
                        this.stop();
                    }
                });
                this.stop();
            }
        });
    }

    public static final boolean removeObjectTemporary(final GameObject object, int time) {
        removeObject(object);
        World.getWorld().submit(new Tickable(time) {
            @Override
            public void execute() {
                spawnObject(object);
                this.stop();
            }
        });
        return true;
    }

    public static final boolean removeObjectTemporary(final GameObject object, int delayTillStart, final int delayTillRemove) {
        World.getWorld().submit(new Tickable(delayTillStart) {
            @Override
            public void execute() {
                removeObject(object);
                World.getWorld().submit(new Tickable(delayTillRemove) {
                    @Override
                    public void execute() {
                        spawnObject(object);
                        this.stop();
                    }
                });
                this.stop();
            }
        });
        return true;
    }

    public static final void sendObjectAnimation(GameObject object, int animation) {
        sendObjectAnimation(null, object, animation);
    }

    public static final void sendObjectAnimation(Entity creator, GameObject object, int animation) {
        if (creator == null) {
            for (Player player : getWorld().getPlayers()) {
                if (player == null || !player.getLocation().withinDistance(object.getLocation()))
                    continue;
                player.getPacketSender().sendObjectAnimation(animation, object);
            }
        } else {
            for (int regionId : creator.getMapRegionsIds()) {
                List<Integer> playersIndexes = getWorld().getRegion(regionId, false).getPlayerIndexes();
                if (playersIndexes == null)
                    continue;
                for (Integer playerIndex : playersIndexes) {
                    Player player = getWorld().getPlayer(playerIndex);
                    if (player == null || !player.getLocation().withinDistance(object.getLocation()))
                        continue;
                    player.getPacketSender().sendObjectAnimation(animation, object);
                }
            }
        }
    }

    public static final GameObject getStandardObject(Location tile) {
        return getWorld().getRegion(tile.getRegionId(), false).getStandartObject(tile.getZ(), tile.getXInRegion(), tile.getYInRegion());
    }

    public static ArrayList<Player> getRegionalPlayers(Entity entity) {
        ArrayList<Player> regionalPlayers = new ArrayList<Player>();
        for (int regionId : entity.getMapRegionsIds()) {
            List<Integer> playersIndexes = World.getWorld().getRegion(regionId, false).getPlayerIndexes();
            if (playersIndexes == null) {
                continue;
            }
            for (Integer playerIndex : playersIndexes) {
                Player p = World.getWorld().getPlayer(playerIndex);
                if (p == null)
                    continue;
                regionalPlayers.add(p);
            }
        }
        return regionalPlayers;
    }

    public int getVotesClaimed() {
        return votesClaimed;
    }

    public void incrementVotes() {
        votesClaimed++;
    }

    public void resetVotesClaimed() {
        votesClaimed = 0;
    }
}
