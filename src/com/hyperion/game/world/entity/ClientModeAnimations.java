package com.hyperion.game.world.entity;

import com.hyperion.game.world.entity.player.Player;

public class ClientModeAnimations {

	//TODO - zombie pest anim is bugged on 602 - anim changed in 602?
	//TODO - wolfs at wilderness agility course anim
	//TODO - npc id 2599 at tzhaar messed up on 562, possibly 530 and higher
	
	public static int getFixedAnimations(Player player, int current_anim) {
		//System.out.println("Mode: " + player.getDetails().getClientMode() + ", current_anim: " + current_anim);
		int mode = player.getDetails().getClientMode();
		if (mode <= 474) {
			if (current_anim == 10080)
				return 808;
			if (current_anim == 10580) //Agility(pipe)
				return 746;
			if (current_anim == 10579)//Agility(pipe2)
				return 748;
			if (current_anim == 4853) //Agility(clim obstacle
				return 839;
		} else {
			if (mode > 530) {
				if (current_anim == 1659) //Whip block
					return 11974;
				else if (current_anim == 10080)  //Whip stand
					return 11973;
				else if (current_anim == 412) //scimitar lunge
					return 13049;
				else if (current_anim == 451) //scimitar slash
					 return 13048;
				else if (current_anim == 404) //scimitar block
					 return 13042;
			}
			if (current_anim == 2612 || current_anim == 2610)
				return 9286;
			else if (current_anim == 2606)
				return 9287;
			else if (current_anim == 2607)
				return 9288;
			else if (current_anim == 2621)
				return 9232;
			else if (current_anim == 2622)
				return 9231;
			else if (current_anim == 2620)
				return 9230;
			else if (current_anim == 2625)
				return 9233;
			else if (current_anim == 2626)
				return 9235;
			else if (current_anim == 2627)
				return 9234;
			else if (current_anim == 2628)
				return 9243;
			else if (current_anim == 2629)
				return 9242;
			else if (current_anim == 2630)
				return 9239;
			else if (current_anim == 2637)
				return 9246;
			else if (current_anim == 2635)
				return 9248;
			else if (current_anim == 2638)
				return 9247;
			else if (current_anim == 2644)
				return 9265;
			else if (current_anim == 2645)
				return 9268;
			else if (current_anim == 2646)
				return 9269;
			
			else if (current_anim == 345) //Old tb -- new tb
				return 1843;
			
			
			
			//else if (current_anim == 422) //punch
			//	return 9286;
			//else if (current_anim == 404) //block
			//	return 9287;
			//else if (current_anim == 2304) //Some type of death anim
			//	return 9288;
			//All fine on 530?
			else if (mode >= 562 && current_anim == 405) //spear attack
					 return 438;
			/*
			else if (current_anim == 422) 
					return 8989; //punch/block - messed up on higher data so we update it�*/
		}
		return current_anim;
	}
}
