package com.hyperion.game.world.entity;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.hyperion.game.Constants;
import com.hyperion.game.world.TeleportAreaLocations;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.masks.Sprites;
import com.hyperion.game.world.entity.masks.UpdateFlags;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.BasicPoint;
import com.hyperion.game.world.pathfinders.PathFinder;
import com.hyperion.game.world.pathfinders.PathState;
import com.hyperion.game.world.pathfinders.Directions.NormalDirection;
import com.hyperion.game.world.region.DynamicRegion;

/**
 * Represents a character in the game world, i.e. a <code>Player</code> or an
 * <code>NPC</code>.
 *
 * @author Graham Edgecombe
 */
public abstract class Entity {

    private CopyOnWriteArrayList<Integer> mapRegionsIds;
    private int mapSize;
    private boolean isAtDynamicRegion;
    private int lastRegionId;
    private boolean visible = true;
    private int index;
    private Location location = Constants.HOME_LOCATION;
    private Location teleportTarget = null;
    private final UpdateFlags updateFlags = new UpdateFlags();
    private final List<Player> localPlayers = new LinkedList<Player>();
    private final List<NPC> localNpcs = new LinkedList<NPC>();
    private Attributes attributes = new Attributes();
    private boolean teleporting = false;
    private Hit primaryHit;
    private Hit secondaryHit;
    private final WalkingQueue walkingQueue = new WalkingQueue(this);
    private final CombatState combatState = new CombatState(this);
    private final Following following = new Following(this);
    private Coverage coverage = null;
    private final Sprites sprites = new Sprites();
    private Location lastKnownRegion = this.getLocation();
    private boolean mapRegionChanging = false;
    private Graphic currentGraphic;
    private Entity interactingEntity;

    public void setFace(Location face) {
        this.face = face;
    }

    private Location face;
    private int lastWildLevel;

    /**
     * The entity's animation queue.
     */
    private AnimationQueue animationQueue = new AnimationQueue();

    /**
     * Gets the animation queue.
     *
     * @return The animation queue.
     */
    public AnimationQueue getAnimationQueue() {
        return animationQueue;
    }

    public Entity() {
        mapRegionsIds = new CopyOnWriteArrayList<Integer>();
    }

    public Entity(Location location) {
        setLocation(location);
        this.lastKnownRegion = this.location;
    }

    public void loadMapRegions() {
        mapRegionsIds.clear();
        isAtDynamicRegion = false;
        int chunkX = location.getChunkX();
        int chunkY = location.getChunkY();
        int mapHash = Constants.MAP_SIZES[mapSize] >> 4;
        int minRegionX = (chunkX - mapHash) / 8;
        int minRegionY = (chunkY - mapHash) / 8;
        for (int xCalc = minRegionX < 0 ? 0 : minRegionX; xCalc <= ((chunkX + mapHash) / 8); xCalc++) {
            for (int yCalc = minRegionY < 0 ? 0 : minRegionY; yCalc <= ((chunkY + mapHash) / 8); yCalc++) {
                int regionId = yCalc + (xCalc << 8);
                if (World.getWorld().getRegion(regionId, isPlayer()) instanceof DynamicRegion)
                    isAtDynamicRegion = true;
                mapRegionsIds.add(regionId);
            }
        }
    }

    /**
     * Makes this entity face a location.
     *
     * @param location The location to face.
     */
    public void face(Location location) {
        this.face = location;
        this.updateFlags.flag(UpdateFlag.FACE_COORDINATE);
        //System.out.println("facing");
    }

    public void face(int x, int y, int z) {
        Location location = Location.create(x, y, z);
        this.face = location;
        this.updateFlags.flag(UpdateFlag.FACE_COORDINATE);
        //System.out.println("facing");
    }

    /**
     * Checks if this entity is facing a location.
     *
     * @return The entity face flag.
     */
    public boolean isFacing() {
        return face != null;
    }

    /**
     * Resets the facing location.
     */
    public void resetFace() {
        this.face = null;
        this.updateFlags.flag(UpdateFlag.FACE_COORDINATE);
    }

    /**
     * Gets the face location.
     *
     * @return The face location, or <code>null</code> if the entity is not
     * facing.
     */
    public Location getFaceLocation() {
        return face;
    }

    /**
     * Checks if this entity is interacting with another entity.
     *
     * @return The entity interaction flag.
     */
    public boolean isInteracting() {
        return interactingEntity != null;
    }

    /**
     * Sets the interacting entity.
     *
     * @param entity The new entity to interact with.
     */
    public void setInteractingEntity(Entity entity) {
        this.interactingEntity = entity;
        this.updateFlags.flag(UpdateFlag.FACE_ENTITY);
    }

    /**
     * Resets the interacting entity.
     */
    public void resetInteractingEntity() {
        this.interactingEntity = null;
        this.updateFlags.flag(UpdateFlag.FACE_ENTITY);
    }

    /**
     * Gets the interacting entity.
     *
     * @return The entity to interact with.
     */
    public Entity getInteractingEntity() {
        return interactingEntity;
    }

    /**
     * Gets the current graphic.
     *
     * @return The current graphic.
     */
    public Graphic getCurrentGraphic() {
        return currentGraphic;
    }

    /**
     * Resets attributes after an update cycle.
     */
    public void reset() {
        this.animationQueue.getQueue().poll();
        this.currentGraphic = null;
    }

    public int timeSinceLastAnimation() {
        if (this.getAttributes().isSet("lastAnimation")) {
            int timeElapsed = (int) (System.currentTimeMillis() - this.getAttributes().getInt("lastAnimation"));
            return timeElapsed;
        } else {
            return 0;
        }
    }

    private boolean canAnimate(int id) {
        if (this.getAttributes().isSet("isMonkey") || ((id == 11788 || id == 11786) && this.getAttributes().isSet("isResting"))) {
            return false;
        }
        return true;
    }

    /**
     * Animates the entity.
     *
     * @param animation The animation.
     */
    public void animate(Animation animation) {
        if (!canAnimate(animation.getId()))
            return;
        this.getAttributes().set("lastAnimation", System.currentTimeMillis());

        this.animationQueue.addAnimation(animation);
        this.getUpdateFlags().flag(UpdateFlag.ANIMATION);
    }

    /**
     * Animates the entity.
     *
     * @param anim The animation.
     */
    public void animate(int anim, int delay) {
        Animation animation = Animation.create(anim, delay);
        animate(animation);
    }

    /**
     * Animates the entity.
     *
     * @param anim The animation.
     */
    public void animate(int anim) {
        Animation animation = Animation.create(anim);
        animate(animation);
    }

    /**
     * Plays graphics.
     *
     * @param graphic The graphics.
     */
    public void playGraphic(Graphic graphic) {
        this.currentGraphic = graphic;
        this.getUpdateFlags().flag(UpdateFlag.GRAPHICS);
    }

    public void playGraphic(int id) {
        Graphic gfx = Graphic.create(id);
        this.currentGraphic = gfx;
        this.getUpdateFlags().flag(UpdateFlag.GRAPHICS);
    }

    public void playGraphic(int id, int delay) {
        Graphic gfx = Graphic.create(id, delay);
        this.currentGraphic = gfx;
        this.getUpdateFlags().flag(UpdateFlag.GRAPHICS);
    }

    public void playGraphic(int id, int delay, int height) {
        Graphic gfx = Graphic.create(id, delay, height);
        this.currentGraphic = gfx;
        this.getUpdateFlags().flag(UpdateFlag.GRAPHICS);
    }

    /**
     * Gets the walking queue.
     *
     * @return The walking queue.
     */
    public WalkingQueue getWalkingQueue() {
        return walkingQueue;
    }

    /**
     * Sets the last known map region.
     *
     * @param lastKnownRegion The last known map region.
     */
    public void setLastKnownRegion(Location lastKnownRegion) {
        this.lastKnownRegion = lastKnownRegion;
    }

    /**
     * Gets the last known map region.
     *
     * @return The last known map region.
     */
    public Location getLastKnownRegion() {
        return lastKnownRegion;
    }

    /**
     * Checks if the map region has changed in this cycle.
     *
     * @return The map region changed flag.
     */
    public boolean isMapRegionChanging() {
        return mapRegionChanging;
    }

    /**
     * Sets the map region changing flag.
     *
     * @param mapRegionChanging The map region changing flag.
     */
    public void setMapRegionChanging(boolean mapRegionChanging) {
        this.mapRegionChanging = mapRegionChanging;
    }

    /**
     * Checks if this entity has a target to teleport to.
     *
     * @return <code>true</code> if so, <code>false</code> if not.
     */
    public boolean hasTeleportTarget() {
        return teleportTarget != null;
    }

    /**
     * Gets the teleport target.
     *
     * @return The teleport target.
     */
    public Location getTeleportTarget() {
        return teleportTarget;
    }

    /**
     * Sets the teleport target.
     *
     * @param teleportTarget The target location.
     */
    public void teleport(Location teleportTarget) {
        this.teleportTarget = teleportTarget;
    }

    public void teleport(int x, int y, int z) {
        Location teleportTarget = Location.create(x, y, z);
        this.teleportTarget = teleportTarget;
    }

    /**
     * Resets the teleport target.
     */
    public void resetTeleportTarget() {
        this.teleportTarget = null;
    }

    /**
     * Gets the sprites.
     *
     * @return The sprites.
     */
    public Sprites getSprites() {
        return sprites;
    }

    /**
     * Checks if this player is teleporting.
     *
     * @return <code>true</code> if so, <code>false</code> if not.
     */
    public boolean isTeleporting() {
        return teleporting;
    }

    /**
     * Sets the teleporting flag.
     *
     * @param teleporting The teleporting flag.
     */
    public void setTeleporting(boolean teleporting) {
        this.teleporting = teleporting;
    }

    /**
     * Gets the list of local players.
     *
     * @return The list of local players.
     */
    public List<Player> getLocalPlayers() {
        return localPlayers;
    }

    /**
     * Gets the list of local npcs.
     *
     * @return The list of local npcs.
     */
    public List<NPC> getLocalNPCs() {
        return localNpcs;
    }

    /**
     * Sets the entity's index.
     *
     * @param index The index.
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * Gets the entity's index.
     *
     * @return The index.
     */
    public int getIndex() {
        return index;
    }

    /**
     * Sets the current location.
     *
     * @param location The current location.
     */
    public void setLocation(Location location) {
        Location lastLocation = this.location;
        this.location = location;
        setCoverage();
        if (lastLocation != null) {
            if (!lastLocation.equals(location)) {
                if (this.isPlayer()) {
                    if (lastLocation.equals(Constants.HOME_LOCATION)) {
                        lastLocation = this.location;
                    }
                    ((Player) this).getVariables().setLastLocation(lastLocation);
                }
            }
        }
    }

    /**
     * Gets the centre location of the entity.
     *
     * @return The centre location of the entity.
     */
    public abstract Location getCentreLocation();

    public abstract int getSize();

    public abstract boolean isNPC();

    public abstract boolean isPlayer();

    public abstract boolean isDestroyed();

    public abstract void inflictDamage(Hit hit, boolean poison);

    public abstract void sendDamage(Hit hit, boolean poison);

    public abstract int getHp();

    public abstract int getMaxHp();

    public abstract void heal(int amt);

    public abstract void setHp(int val);

    public abstract boolean isAutoRetaliating();

    public abstract void resetVariables();

    public abstract void dropLoot(Entity killer);

    public abstract Location getDeathArea();

    /**
     * Gets the current location.
     *
     * @return The current location.
     */
    public Location getLocation() {
        return location;
    }

    /**
     * Gets the update flags.
     *
     * @return The update flags.
     */
    public UpdateFlags getUpdateFlags() {
        return updateFlags;
    }

    /**
     * Gets the client-side index of an entity.
     *
     * @return The client-side index.
     */
    public abstract int getClientIndex();

    private String forcedChat;

    public void playForcedChat(String forcedChat) {
        this.forcedChat = forcedChat;
        this.getUpdateFlags().flag(UpdateFlag.FORCED_CHAT);
    }

    public String getForcedChat() {
        return forcedChat;
    }

    public final Attributes getAttributes() {
        if (attributes == null) {
            attributes = new Attributes();
        }
        return attributes;
    }

    public void executeProjectile(Projectiles projectile) {
        for (int regionId : getMapRegionsIds()) {
            List<Integer> playersIndexes = World.getWorld().getRegion(regionId, false).getPlayerIndexes();
            if (playersIndexes == null) {
                continue;
            }
            for (Integer playerIndex : playersIndexes) {
                Player p = World.getWorld().getPlayer(playerIndex);
                if (p == null)
                    continue;
                if (p.getLocation().withinDistance(this.getLocation())) {
                    p.getPacketSender().sendProjectile(projectile.getStart(), projectile.getFinish(), projectile.getId(), projectile.getStartSpeed(), projectile.getAngle(), projectile.getSpeed(), projectile.getStartHeight(), projectile.getEndHeight(), projectile.getLockon(), projectile.getSlope(), projectile.getRadius());
                }
            }
        }
    }

    public void executeObjectChange(Location loc, int newId, int type, int direction) {
        for (int regionId : getMapRegionsIds()) {
            List<Integer> playersIndexes = World.getWorld().getRegion(regionId, false).getPlayerIndexes();
            if (playersIndexes == null)
                continue;
            for (Integer playerIndex : playersIndexes) {
                Player player = World.getWorld().getPlayer(playerIndex);
                if (player == null || !player.getLocation().withinDistance(loc))
                    continue;
                player.getPacketSender().createObject(newId, loc, direction, type);
            }
        }
    }

    public void executeRegionStillGraphic(Location loc, int id) {
        for (int regionId : getMapRegionsIds()) {
            List<Integer> playersIndexes = World.getWorld().getRegion(regionId, false).getPlayerIndexes();
            if (playersIndexes == null)
                continue;
            for (Integer playerIndex : playersIndexes) {
                Player p = World.getWorld().getPlayer(playerIndex);
                if (p == null || !p.getLocation().withinDistance(loc))
                    continue;
                p.getPacketSender().sendStillGraphics(loc, Graphic.create(id, 0), 0);
            }
        }
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    /**
     * Sets the primary hit.
     *
     * @param hit The primary hit.
     */
    public void setPrimaryHit(Hit hit) {
        this.primaryHit = hit;
    }

    /**
     * Sets the secondary hit.
     *
     * @param hit The secondary hit.
     */
    public void setSecondaryHit(Hit hit) {
        this.secondaryHit = hit;
    }

    /**
     * Gets the primary hit.
     *
     * @return The primary hit.
     */
    public Hit getPrimaryHit() {
        return primaryHit;
    }

    /**
     * Gets the secondary hit.
     *
     * @return The secondary hit.
     */
    public Hit getSecondaryHit() {
        return secondaryHit;
    }

    /**
     * Resets the primary and secondary hits.
     */
    public void resetHits() {
        primaryHit = null;
        secondaryHit = null;
    }

    public CombatState getCombatState() {
        return combatState;
    }

    public Following getFollowing() {
        return following;
    }

    public Coverage getCoverage() {
        return coverage;
    }

    private void setCoverage() {
        coverage = new Coverage(getLocation(), getSize());
    }

    public void updateCoverage(NormalDirection direction) {
        coverage.update(direction, getSize());
    }

    public void updateCoverage(Location loc) {
        if (coverage == null) {
            setCoverage();
        }
        coverage.update(loc, getSize());
    }

    public PathState execute_path(PathFinder pathFinder, int x, int y) {
        return execute_path(pathFinder, null, x, y);
    }

    public PathState execute_path(PathFinder pathFinder, Entity target, int x, int y) {
        return execute_path(pathFinder, target, x, y, false, true);
    }

    private PathState execute_path(final PathFinder pathFinder, final Entity target, final int x, final int y, final boolean ignoreLastStep, boolean addToWalking) {
        // if(this instanceof Player && target instanceof Player) {
        // if(target.getFollowing() != null && target.getFollowing().getOther()
        // == this) {
        // if(x == target.getLocation().getX() && y ==
        // target.getLocation().getY()) {
        // }
        // }

        if (getCombatState().isDead()) {
            PathState state = new PathState();
            state.routeFailed();
            return state;
        }
        if (isPlayer()) {
            Player player = (Player) this;
            if (player.getDuelSession() != null) {
                if (player.getDuelSession().ruleEnabled(1)) {
                    MainCombat.endCombat(player, 1);
                    player.resetActionAttributes();
                    PathState state = new PathState();
                    state.routeFailed();
                    return state;
                }
            }
        }
        Location destination = Location.create(x, y, getLocation().getZ());
        Location base = getLocation();
        int srcX = base.getLocalX();
        int srcY = base.getLocalY();
        int destX = destination.getLocalX(base);
        int destY = destination.getLocalY(base);
        PathState state = pathFinder.findPath(this, target, getLocation(), srcX, srcY, destX, destY, 1, getWalkingQueue().isRunning(), ignoreLastStep, true);
        if (state != null && addToWalking) {
            getWalkingQueue().reset();
            for (final BasicPoint step : state.getPoints()) {
                getWalkingQueue().addStep(step.getX(), step.getY());
            }
            getWalkingQueue().finish();
            if (this instanceof Player) {
                this.getAttributes().set("stationary", true);
            }
        }
        return state;
    }

    public CopyOnWriteArrayList<Integer> getMapRegionsIds() {
        return mapRegionsIds;
    }

    public boolean isAtDynamicRegion() {
        return isAtDynamicRegion;
    }

    public void setLastRegionId(int lastRegionId) {
        this.lastRegionId = lastRegionId;
    }

    public int getLastRegionId() {
        return lastRegionId;
    }

    public int getLastWildLevel() {
        return lastWildLevel;
    }

    public void setLastwildLevel(int currentLevel) {
        this.lastWildLevel = currentLevel;
    }

    public int getWildLevel() {
        int y = getLocation().getY();
        if (!TeleportAreaLocations.inWilderness(getLocation())) {
            return -1;
        }
        int level = 1 + (y - 3520) / 8;
        return level > 840 ? level - 800 : level;
    }

    public int getMapSize() {
        return mapSize;
    }

    public void setMapSize(int size) {
        this.mapSize = size;
        loadMapRegions();
    }
}
