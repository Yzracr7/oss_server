package com.hyperion.game.world.entity;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.combat.CombatAction;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.SizedPathFinder;
import com.hyperion.game.world.pathfinders.VariablePathFinder;
import com.hyperion.game.world.pathfinders.Directions.NormalDirection;
import com.hyperion.utility.game.Tile;
/**
 * @author Nando
 */
public class Following {
	
	private Entity entity;
	private Entity other;
	private boolean combatFollow;
	
	public Following(Entity entity) {
		this.entity = entity;
		this.combatFollow = false;
	}
	
	public boolean executeFollowing() {
		if (other == null || entity.getCombatState().isDead() || other.getCombatState().isDead() || other.isDestroyed()) {
			other = null;
			entity.getWalkingQueue().reset();
			return false;
		}
		if (entity.getAttributes().isSet("stunned") || entity.getAttributes().isSet("stopActions")
				|| entity.getAttributes().isSet("isResting")) {
			other = null;
			return false;
		}
		if (entity.getCombatState().isFrozen()) {
			other = null;
			return false;
		}
		if (entity.isPlayer() && other.isPlayer()) {
			Player player = (Player) entity;
			if (player.getDuelSession() != null) {
				if (player.getDuelSession().ruleEnabled(1)) {
					other = null;
					return false;
				}
			}
		}
		if (entity.isNPC()) {
			NPC npc = (NPC) entity;
			if (entity.getAttributes().isSet("stopCombat") || !npc.isMoveable()) {
				other = null;
				return false;
			}
		}
		CombatAction action = MainCombat.getAction(entity);
		entity.setInteractingEntity(other);
		Location others_location = other.getLocation();
		int dist = entity.getLocation().distanceToPoint(others_location);
		
		if(dist > 20 && !entity.getAttributes().isSet("petOwner")) {
			other = null;
			entity.getWalkingQueue().reset();
			return false;
		}
		
		int x = others_location.getX();
		int y = others_location.getY();
		int sizeX = other.getSize();
		int sizeY = other.getSize();
		int dir = 0;
		if (combatFollow) {
			if (action.isInCombatDistance(entity, other) && !Location.standingOn(entity, other)) {
				if (entity.isPlayer()) {
					CombatType combatType = entity.getCombatState().getCombatType();
					if (combatType.equals(CombatType.MAGE)) {
						if (dist < 9) {
							entity.setInteractingEntity(other);
							entity.getWalkingQueue().reset();
							return false;
						}
					} else if (combatType.equals(CombatType.RANGE)) {
						if (dist < 8) {
							entity.setInteractingEntity(other);
							entity.getWalkingQueue().reset();
							return false;
						}
					}
				} else if (entity.isNPC()) {
					int id = ((NPC) entity).getId();
					boolean skip = false;
					int[] ids = {1592, 1591, 1590, 941, 1472, 8349, 8133, 750, 1913, 1977, 1915, 477}; //npcs that dont need to follow
					for (int i = 0; i < ids.length; i++) {
						if (ids[i] == id) {
							skip = true;
						}
					}
					if (id == 73) {
						if (entity.getAttributes().isSet("pestzombie")) {
							skip = true;
						}
					}
					if (!skip) {
						if (!action.getCombatType().equals(CombatType.MELEE)) {
							entity.getWalkingQueue().reset();
							return false;
						}
					}
				}
			}
		}
		if (entity.isPlayer()) {
			if (combatFollow) {
				//Player-npc following
				int walkToData = 0;
				if (other.isNPC()) {
					walkToData = 0x80000000;
				}
				entity.execute_path(new VariablePathFinder(-1, walkToData, dir, sizeX, sizeY), other, x, y);
			} else {
				//Player-Player following
				Location path = ((Player) other).getVariables().getLastLocation();
				
				//TODO: if path = player.getLocation(no last step) use dumbpathfinder to move around them
				if (path == null) {
					path = others_location;
				} else if (other.getLocation().distanceToPoint(path) > 10) {
					//Last step is far away from them(just teleported?)
					path = Tile.getSurroundingTile(entity, other);
				}
				/*final*/ int toX = path.getX();
				/*final*/ int toY = path.getY();
				int delay = 1;
				
				if(!other.getWalkingQueue().isMoving()
						&& other.getLocation().getX() == toX 
						&& other.getLocation().getY() == toY) {
					//other entity does not have a last step location
					Location newLoc = Tile.getSurroundingTile(entity, other);
					toX = newLoc.getX();
					toY = newLoc.getY();
				}
				
				if(!other.getWalkingQueue().isMoving()
						&& !entity.getWalkingQueue().isMoving()
						&& toX == entity.getLocation().getX()
						&& toY == entity.getLocation().getY()
						&& entity.getLocation().distanceToPoint(other.getLocation()) <= 1
						&& other.getFollowing().getOther() == entity) {
					//Following each other horizontally/vertically
					toX = other.getLocation().getX();
					toY = other.getLocation().getY();
				} 
				
				if(other.getFollowing().getOther() == entity
						&& entity.getLocation().distanceToPoint(other.getLocation()) <= 1) {
					//following each other very closely
					other.getAttributes().remove("stationary");
					entity.getAttributes().remove("stationary");
				}
				
				
				/*if(!entity.getWalkingQueue().isMoving()) { //we're not moving but other just started moving
					int otherDirection = -1;
					if(other.getWalkingQueue() != null)
						otherDirection = other.getWalkingQueue().getNextPointDir();
					NormalDirection ourDirection = entity.getLocation().direction(entity.getFaceLocation());
					if(ourDirection != null) {
						int ourDirectionValue = ourDirection.intValue();
						if(otherDirection != -1) {
							if(ourDirectionValue != otherDirection) {
								int diff = ourDirectionValue - otherDirection;
								if(diff > 3) { //Opposite direction
									delay = 2;
								}
							}
						}
						}
					
				}*/
				
				
				final int finalDelay = delay;
				final int finalX = toX;
				final int finalY = toY;
				
				if((other.getWalkingQueue().isRunning()
						|| entity.getLocation().distanceToPoint(other.getLocation()) > 1
						|| other.getWalkingQueue().isMoving()
						|| !(other.getLocation().equals(entity.getLocation()) && !other.getWalkingQueue().isRunning()))
						//&& !(other.getFollowing() != null && other.getFollowing().getOther() == entity) //This last line means they're NOT following us.
						&& !(other.getAttributes().isSet("stationary"))) {

					entity.execute_path(new SizedPathFinder(), finalX, finalY);
				} else {
					if(entity.getLocation().distanceToPoint(other.getLocation()) > 1) { //2
						entity.execute_path(new SizedPathFinder(), finalX, finalY);
					} else
					World.getWorld().submit(new Tickable(delay) { //1 Second delay
						@Override
						public void execute() {
						
							entity.execute_path(new SizedPathFinder(), finalX, finalY);
						
							this.stop();
						}
					});
				}
				if(other.getWalkingQueue().isMoving() && other.getAttributes().isSet("stationary"))
					other.getAttributes().remove("stationary");
			}
		} else {
			if(dist > 10 && entity.getAttributes().isSet("petOwner")) {
				NormalDirection direction = Tile.hasLocationToMove(entity, other);
				if(direction != null) {
					Location loc = Location.createSurroundingTile(entity, other, direction);
					//if(ProjectilePathFinder.hasLineOfSight(loc, other.getLocation(), false)) {
					if(loc != null) {
						entity.teleport(loc);
						entity.playGraphic(((NPC)entity).getSize() > 1 ? 1315 : 1314);
					}
					//}
				}
			}
			NPCFollowing.executePathFinding(entity, other, true);
		}
		return true;
	}
	
	public Entity getOther() {
		return other;
	}
	
	public void setFollowing(Entity other, boolean combatFollow) {
		if(other == null && entity.getAttributes().isSet("petOwner"))
			return;
		if (combatFollow) {
			this.combatFollow = true;
		} else {
			this.combatFollow = false;
		}
		this.other = other;
	}
}