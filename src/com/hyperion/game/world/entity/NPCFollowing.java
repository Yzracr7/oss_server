package com.hyperion.game.world.entity;

import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.Directions;
import com.hyperion.game.world.pathfinders.Directions.NormalDirection;
/**
 * @author Ziotic src
 */
public class NPCFollowing {

	public static boolean executePathFinding(Entity entity, Entity partner, boolean combat) {
    	try {
        entity.setInteractingEntity(partner);
        Location loc = entity.getLocation();
        int dx = loc.getX() - partner.getLocation().getX();
        int dy = loc.getY() - partner.getLocation().getY();//eCenter.getY() - pCenter.getY();
        int distance = (int) entity.getCoverage().center().distance(partner.getCoverage().center());
        boolean success = false;
        int x = entity.getLocation().getX();
        int y = entity.getLocation().getY();
       // if(!combat && !entity.isPlayer() && distance > 8 && entity.getAttributes().isSet("owner")) {
       // 	entity.setLocation(getSurroundingTile(partner));
       // }
        int counter = 1;
        if (entity.isPlayer()) {
        	Player player = (Player) entity;
        	if (partner.isPlayer()) {
        		counter = (player.getWalkingQueue().isMoving() || partner.getWalkingQueue().isMoving()) ? 2 : 1;
        	} else {
        		counter = player.getWalkingQueue().isMoving() ? 2 : 1;
        	}
        } else {
        	counter = 1;
        }
        for (int i = 0; i < counter; i++) {
        	success = false;
            loc = Location.create(x, y, entity.getLocation().getZ());
            NextNode next = getNextNode(loc, dx, dy, distance, combat, entity, partner);
            if (next == null) {
                break;
            }
            if (next.tile == null) {
                break;
            }
            if (next.canMove) {
                if (partner.getCoverage().within(next.tile) && !Location.standingOn(entity, partner)) {
                	success = true;
                	continue;
                }
                x = next.tile.getX();
                y = next.tile.getY();
                dx = x - partner.getLocation().getX(); 
                dy = y - partner.getLocation().getY(); 
            	success = true;
            	entity.updateCoverage(next.tile);
            	entity.getWalkingQueue().addStep(next.tile.getX(), next.tile.getY());
        		entity.getWalkingQueue().finish();
            } else {
                // TODO handle being stuck!
                break;
            }
        }
        return success;
    	} catch (Exception e) {
    		e.printStackTrace();
    		return false;
    	}
    }
	
	private static NextNode getNextNode(Location loc, int dx, int dy, int distance, boolean combat, Entity entity, Entity partner) {
        NormalDirection direction = null;
        boolean npcCheck = (entity.isNPC() && !entity.getAttributes().isSet("petOwner"));
        if (combat) {
        	if (entity.getCoverage().correctCombatPosition(entity, partner, partner.getCoverage(), 1, CombatType.MELEE)) {
        		return null;
        	}
        } else {
        	if (entity.getCoverage().correctFinalFollowPosition(partner.getCoverage())) {
        		return null;
        	}
        }
        if (entity.getSize() > 1) {
    		Location eCenter = entity.getCoverage().center();
    		Location pCenter = partner.getCoverage().center();
        	if (entity.getCoverage().intersect(partner.getCoverage())) {
        		if (eCenter == pCenter) {
        			if (Location.canMove(entity, NormalDirection.SOUTH_WEST, entity.getSize(), npcCheck)) {
    					direction = NormalDirection.SOUTH_WEST;
    				} else if (Location.canMove(entity, NormalDirection.WEST, entity.getSize(), npcCheck)) {
    					direction = NormalDirection.WEST;
    				} else if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
    					direction = NormalDirection.SOUTH;
    				} else if (Location.canMove(entity, NormalDirection.NORTH_WEST, entity.getSize(), npcCheck)) {
    					direction = NormalDirection.NORTH_WEST;
    				} else if (Location.canMove(entity, NormalDirection.NORTH_EAST, entity.getSize(), npcCheck)) {
    					direction = NormalDirection.NORTH_EAST;
    				} else if (Location.canMove(entity, NormalDirection.SOUTH_EAST, entity.getSize(), npcCheck)) {
    					direction = NormalDirection.SOUTH_EAST;
    				} else if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
    					direction = NormalDirection.EAST;
    				} else if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
    					direction = NormalDirection.NORTH;
    				} 
        		} else if (eCenter.right(pCenter)) {
        			if (eCenter.above(pCenter)) {
        				if (Location.canMove(entity, NormalDirection.NORTH_EAST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.NORTH_EAST;
        				} else if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.EAST;
        				} else if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.NORTH;
        				}
        			} else if (pCenter.under(pCenter)) {
        				if (Location.canMove(entity, NormalDirection.SOUTH_EAST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.SOUTH_EAST;
        				} else if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.EAST;
        				} else if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.SOUTH;
        				}
        			} else {
        				if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.EAST;
        				} else if (Location.canMove(entity, NormalDirection.NORTH_EAST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.NORTH_EAST;
        				} else if (Location.canMove(entity, NormalDirection.SOUTH_EAST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.SOUTH_EAST;
        				} else if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.NORTH;
        				} else if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.SOUTH;
        				}
        			}
        		} else if (eCenter.left(pCenter)) {
        			if (eCenter.above(pCenter)) {
        				if (Location.canMove(entity, NormalDirection.NORTH_WEST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.NORTH_WEST;
        				} else if (Location.canMove(entity, NormalDirection.WEST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.WEST;
        				} else if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.NORTH;
        				}
        			} else if (pCenter.under(pCenter)) {
        				if (Location.canMove(entity, NormalDirection.SOUTH_WEST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.SOUTH_WEST;
        				} else if (Location.canMove(entity, NormalDirection.WEST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.WEST;
        				} else if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.SOUTH;
        				}
        			} else {
        				if (Location.canMove(entity, NormalDirection.WEST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.WEST;
        				} else if (Location.canMove(entity, NormalDirection.NORTH_WEST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.NORTH_WEST;
        				} else if (Location.canMove(entity, NormalDirection.SOUTH_WEST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.SOUTH_WEST;
        				} else if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.NORTH;
        				} else if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.SOUTH;
        				}
        			}
        		} else {
        			if (eCenter.above(pCenter)) {
        				if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.NORTH;
        				} else if (Location.canMove(entity, NormalDirection.NORTH_EAST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.NORTH_EAST;
        				} else if (Location.canMove(entity, NormalDirection.NORTH_WEST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.NORTH_WEST;
        				} else if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.EAST;
        				} else if (Location.canMove(entity, NormalDirection.WEST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.WEST;
        				}
        			} else if (eCenter.under(pCenter)) {
        				if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.SOUTH;
        				} else if (Location.canMove(entity, NormalDirection.SOUTH_EAST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.SOUTH_EAST;
        				} else if (Location.canMove(entity, NormalDirection.SOUTH_WEST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.SOUTH_WEST;
        				} else if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.EAST;
        				} else if (Location.canMove(entity, NormalDirection.WEST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.WEST;
        				}
        			}
        		}
        	} else {
        		Coverage eC = entity.getCoverage();
        		Coverage pC = partner.getCoverage();
        		int absDX = Math.abs(dx);
        		int absDY = Math.abs(dy);
        		if (eC.right(pC)) {
        			if (eC.above(pC)) {
        				if (Location.canMove(entity, NormalDirection.SOUTH_WEST, entity.getSize(), npcCheck)) {
        					if (absDY <= 1 && absDY >= 0) {
        						if (Location.canMove(entity, NormalDirection.WEST, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.WEST;
        						}
        					} else if (absDX <= 1 && absDX >= 0) {
        						if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.SOUTH;
        						}
        					} else {
        						direction = NormalDirection.SOUTH_WEST;
        					}
        				} else {
        					if (dx > dy) {
        						if (Location.canMove(entity, NormalDirection.WEST, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.WEST;
        						} else if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.SOUTH;
        						}
        					} else if (dx < dy) {
        						if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.SOUTH;
        						} else if (Location.canMove(entity, NormalDirection.WEST, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.WEST;
        						}
        					} else {
        						if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.SOUTH;
        						} else if (Location.canMove(entity, NormalDirection.WEST, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.WEST;
        						}
        					}
        				}
        			} else if (eC.under(pC)) {
        				if (Location.canMove(entity, NormalDirection.NORTH_WEST, entity.getSize(), npcCheck)) {
        					if (absDY <= 1 && absDY >= 0) {
        						if (Location.canMove(entity, NormalDirection.WEST, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.WEST;
        						}
        					} else if (absDX <= 1 && absDX >= 0) {
        						if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.NORTH;
        						}
        					} else {
        						direction = NormalDirection.NORTH_WEST;
        					}
        				} else {
        					if (dx > -dy) {
        						if (Location.canMove(entity, NormalDirection.WEST, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.WEST;
        						} else if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.NORTH;
        						}
        					} else if (dx < -dy) {
        						if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.NORTH;
        						} else if (Location.canMove(entity, NormalDirection.WEST, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.WEST;
        						}
        					} else {
        						if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.NORTH;
        						} else if (Location.canMove(entity, NormalDirection.WEST, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.WEST;
        						}
        					}
        				}
        			} else {
        				if (Location.canMove(entity, NormalDirection.WEST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.WEST;
        				}
        			}
        		} else if (eC.left(pC)) {
        			if (eC.above(pC)) {
        				if (Location.canMove(entity, NormalDirection.SOUTH_EAST, entity.getSize(), npcCheck)) {
        					if (absDY <= 1 && absDY >= 0) {
        						if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.EAST;
        						}
        					} else if (absDX <= 1 && absDX >= 0) {
        						if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.SOUTH;
        						}
        					} else {
        						direction = NormalDirection.SOUTH_EAST;
        					}
        				} else {
        					if (-dx > dy) {
        						if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.EAST;
        						} else if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.SOUTH;
        						}
        					} else if (-dx < dy) {
        						if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.SOUTH;
        						} else if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.EAST;
        						}
        					} else {
        						if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.SOUTH;
        						} else if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.EAST;
        						}
        					}
        				}
        			} else if (eC.under(pC)) {
        				if (Location.canMove(entity, NormalDirection.NORTH_EAST, entity.getSize(), npcCheck)) {
        					if (absDY <= 1 && absDY >= 0) {
        						if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.EAST;
        						}
        					} else if (absDX <= 1 && absDX >= 0) {
        						if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.NORTH;
        						}
        					} else {
        						direction = NormalDirection.NORTH_EAST;
        					}
        				} else {
        					if (-dx > -dy) {
        						if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.EAST;
        						} else if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.NORTH;
        						}
        					} else if (-dx < -dy) {
        						if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.NORTH;
        						} else if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.EAST;
        						}
        					} else {
        						if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.NORTH;
        						} else if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
                					direction = NormalDirection.EAST;
        						}
        					}
        				}
        			} else {
        				if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.EAST;
        				}
        			}
        		} else {
        			if (eC.above(pC)) {
        				if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.SOUTH;
        				}
        			} else if (eC.under(pC)) {
        				if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
        					direction = NormalDirection.NORTH;
        				}
        			}
        		}
        	}
        	if (direction == null) {
        		return null;
        	}
        	return new NextNode(loc, direction, Location.canMove(entity, direction, entity.getSize(), npcCheck));
        } else {
	        if (dx > 0) {
	            if (dy > 0) {
	                if (dx == 1 && dy == 1 && combat) {
	                    if (Location.canMove(entity, NormalDirection.WEST, entity.getSize(), npcCheck)) {
	                        direction = NormalDirection.WEST;
	                    } else if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
	                        direction = NormalDirection.SOUTH;
	                    } else {
	                        direction = NormalDirection.WEST; // random w/e
	                    }
	                } else {
	                    if (Location.canMove(entity, NormalDirection.SOUTH_WEST, entity.getSize(), npcCheck)) {
	                        direction = NormalDirection.SOUTH_WEST;
	                    } else {
	                        if (dy > dx) {
	                            if (Location.canMove(entity, NormalDirection.WEST, entity.getSize(), npcCheck)) {
	                                direction = NormalDirection.WEST;
	                            } else {
	                                direction = NormalDirection.SOUTH;
	                            }
	                        } else if (dy < dx) {
	                            if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
	                                direction = NormalDirection.SOUTH;
	                            } else {
	                                direction = NormalDirection.WEST;
	                            }
	                        } else {
	                        	if (Location.canMove(entity, NormalDirection.SOUTH_WEST, entity.getSize(), npcCheck)) {
	                        		direction = NormalDirection.SOUTH_WEST;
	                        	} else if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
	                                direction = NormalDirection.SOUTH;
	                            } else {
	                                direction = NormalDirection.WEST;
	                            }
	                        }
	                    }
	                }
	            } else if (dy < 0) {
	                if (dx == 1 && dy == -1 && combat) {
	                    if (Location.canMove(entity, NormalDirection.WEST, entity.getSize(), npcCheck)) {
	                        direction = NormalDirection.WEST;
	                    } else if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
	                        direction = NormalDirection.NORTH;
	                    } else {
	                        direction = NormalDirection.WEST; // random w/e
	                    }
	                } else {
	                    if (Location.canMove(entity, NormalDirection.NORTH_WEST, entity.getSize(), npcCheck)) {
	                        direction = NormalDirection.NORTH_WEST;
	                    } else {
	                        if (Math.abs(dy) > Math.abs(dx)) {
	                            if (Location.canMove(entity, NormalDirection.WEST, entity.getSize(), npcCheck)) {
	                                direction = NormalDirection.WEST;
	                            } else {
	                                direction = NormalDirection.NORTH;
	                            }
	                        } else if (Math.abs(dy) < Math.abs(dx)) {
	                            if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
	                                direction = NormalDirection.NORTH;
	                            } else {
	                                direction = NormalDirection.WEST;
	                            }
	                        } else {
	                        	if (Location.canMove(entity, NormalDirection.NORTH_WEST, entity.getSize(), npcCheck)) {
	                        		direction = NormalDirection.NORTH_WEST;
	                        	} else if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
	                                direction = NormalDirection.NORTH;
	                            } else {
	                                direction = NormalDirection.WEST;
	                            }
	                        }
	                    }
	                }
	            } else {
	                direction = NormalDirection.WEST;
	            }
	        } else if (dx < 0) {
	            if (dy > 0) {
	                if (dx == -1 && dy == 1 && combat) {
	                    if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
	                        direction = NormalDirection.EAST;
	                    } else if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
	                        direction = NormalDirection.SOUTH;
	                    } else {
	                        direction = NormalDirection.EAST; // random w/e
	                    }
	                } else {
	                    if (Location.canMove(entity, NormalDirection.SOUTH_EAST, entity.getSize(), npcCheck)) {
	                        direction = NormalDirection.SOUTH_EAST;
	                    } else {
	                        if (Math.abs(dy) > Math.abs(dx)) {
	                            if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
	                                direction = NormalDirection.EAST;
	                            } else if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
	                                direction = NormalDirection.SOUTH;
	                            }
	                        } else if (Math.abs(dy) < Math.abs(dx)) {
	                            if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
	                                direction = NormalDirection.SOUTH;
	                            } else if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
	                                direction = NormalDirection.EAST;
	                            }
	                        } else {
	                        	if (Location.canMove(entity, NormalDirection.SOUTH_EAST, entity.getSize(), npcCheck)) {
	                        		direction = NormalDirection.SOUTH_EAST;
	                        	} else if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
	                                direction = NormalDirection.SOUTH;
	                            } else if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
	                                direction = NormalDirection.EAST;
	                            }
	                        }
	                    }
	                }
	            } else if (dy < 0) {
	                if (dx == -1 && dy == -1 && combat) {
	                    if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
	                        direction = NormalDirection.EAST;
	                    } else if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
	                        direction = NormalDirection.NORTH;
	                    } else {
	                        direction = NormalDirection.EAST; // random w/e
	                    }
	                } else {
	                    if (Location.canMove(entity, NormalDirection.NORTH_EAST, entity.getSize(), npcCheck)) {
	                        direction = NormalDirection.NORTH_EAST;
	                    } else {
	                        if (Math.abs(dy) > Math.abs(dx)) {
	                            if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
	                                direction = NormalDirection.EAST;
	                            } else if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
	                                direction = NormalDirection.NORTH;
	                            }
	                        } else if (Math.abs(dy) < Math.abs(dx)) {
	                            if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
	                                direction = NormalDirection.NORTH;
	                            } else if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
	                                direction = NormalDirection.EAST;
	                            }
	                        } else {
	                        	if (Location.canMove(entity, NormalDirection.NORTH_EAST, entity.getSize(), npcCheck)) {
	                        		direction = NormalDirection.NORTH_EAST;
	                        	} else if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
	                                direction = NormalDirection.NORTH;
	                            } else if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
	                                direction = NormalDirection.EAST;
	                            }
	                        }
	                    }
	                }
	            } else {
	                direction = NormalDirection.EAST;
	            }
	        } else {
	            if (dy > 0) {
	                direction = NormalDirection.SOUTH;
	            } else if (dy < 0) {
	                direction = NormalDirection.NORTH;
	            } else {
	                if (Location.canMove(entity, NormalDirection.WEST, entity.getSize(), npcCheck)) {
	                    direction = NormalDirection.WEST;
	                } else if (Location.canMove(entity, NormalDirection.EAST, entity.getSize(), npcCheck)) {
	                    direction = NormalDirection.EAST;
	                } else if (Location.canMove(entity, NormalDirection.NORTH, entity.getSize(), npcCheck)) {
	                    direction = NormalDirection.NORTH;
	                } else if (Location.canMove(entity, NormalDirection.SOUTH, entity.getSize(), npcCheck)) {
	                    direction = NormalDirection.SOUTH;
	                } else {
	                    direction = NormalDirection.SOUTH; // random w/e
	                }
	
	            }
	        }
	        if (direction == null) {
	            return null;
	        }
	        return new NextNode(loc, direction, Location.canMove(entity, direction, entity.getSize(), npcCheck));
        }
    }
	
	private static class NextNode {
        Location tile = null;
        boolean canMove = false;
        public NextNode(Location loc, NormalDirection dir, boolean canMove) {
            this.canMove = canMove;
            if (canMove) {
                tile = loc.transform(Directions.DIRECTION_DELTA_X[dir.intValue()], Directions.DIRECTION_DELTA_Y[dir.intValue()], 0);
            }
        }
    }
}
