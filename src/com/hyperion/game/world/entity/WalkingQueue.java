package com.hyperion.game.world.entity;

import java.util.Deque;
import java.util.LinkedList;

import com.hyperion.game.Constants;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
import com.hyperion.game.world.pathfinders.Directions;

/**
 * <p>A <code>WalkingQueue</code> stores steps the client needs to walk and
 * allows this queue of steps to be modified.</p>
 * <p>
 * <p>The class will also process these steps when
 * {@link #processMovement()} is called. This should be called once
 * per server cycle.</p>
 *
 * @author Graham Edgecombe
 */
public class WalkingQueue {

    /**
     * Represents a single point in the queue.
     *
     * @author Graham Edgecombe
     */
    private static class Point {

        /**
         * The x-coordinate.
         */
        private final int x;

        /**
         * The y-coordinate.
         */
        private final int y;

        /**
         * The direction to walk to this point.
         */
        private final int dir;

        /**
         * Creates a point.
         *
         * @param x   X coord.
         * @param y   Y coord.
         * @param dir Direction to walk to this point.
         */
        public Point(int x, int y, int dir) {
            this.x = x;
            this.y = y;
            this.dir = dir;
        }

    }

    /**
     * The maximum size of the queue. If there are more points than this size,
     * they are discarded.
     */
    private static final int MAXIMUM_SIZE = 50;

    /**
     * The entity.
     */
    private Entity entity;

    /**
     * The queue of waypoints.
     */
    private Deque<Point> waypoints = new LinkedList<Point>();

    /**
     * Run toggle (button in client).
     */
    private boolean runToggled = false;

    /**
     * Run for this queue (CTRL-CLICK) toggle.
     */
    private boolean runQueue = false;

    /**
     * Creates the <code>WalkingQueue</code> for the specified
     * <code>Entity</code>.
     *
     * @param entity The entity whose walking queue this is.
     */
    public WalkingQueue(Entity entity) {
        this.entity = entity;
    }

    /**
     * Sets the run toggled flag.
     *
     * @param runToggled The run toggled flag.
     */
    public void setRunningToggled(boolean runToggled) {
        this.runToggled = runToggled;
    }

    /**
     * Sets the run queue flag.
     *
     * @param runQueue The run queue flag.
     */
    public void setRunningQueue(boolean runQueue) {
        this.runQueue = runQueue;
    }

    /**
     * Gets the run toggled flag.
     *
     * @return The run toggled flag.
     */
    public boolean isRunningToggled() {
        return runToggled;
    }

    /**
     * Gets the running queue flag.
     *
     * @return The running queue flag.
     */
    public boolean isRunningQueue() {
        return runQueue;
    }

    /**
     * Checks if any running flag is set.
     *
     * @return <code>true</code. if so, <code>false</code> if not.
     */
    public boolean isRunning() {
        return runToggled || runQueue;
    }

    /**
     * Resets the walking queue so it contains no more steps.
     */
    public void reset() {
        runQueue = false;
        waypoints.clear();
        waypoints.add(new Point(entity.getLocation().getX(), entity.getLocation().getY(), -1));
    }

    /**
     * Checks if the queue is clearAllBoxes.
     *
     * @return <code>true</code> if so, <code>false</code> if not.
     */
    public boolean isEmpty() {
        return waypoints.isEmpty();
    }

    /**
     * Removes the first waypoint which is only used for calculating
     * directions. This means walking begins at the correct time.
     */
    public void finish() {
        waypoints.removeFirst();
    }

    public void addStep(Location loc) {
        addStep(loc.getX(), loc.getY());
    }

    /**
     * Adds a single step to the walking queue, filling in the points to the
     * previous point in the queue if necessary.
     *
     * @param x The local x coordinate.
     * @param y The local y coordinate.
     */
    public void addStep(int x, int y) {
        /*
         * The RuneScape client will not sendTeleportTitles all the points in the queue.
		 * It just sends places where the direction changes.
		 * 
		 * For instance, walking from a route like this:
		 * 
		 * <code>
		 * *****
		 *     *
		 *     *
		 *     *****
		 * </code>
		 * 
		 * Only the places marked with X will be sent:
		 * 
		 * <code>
		 * X***X
		 *     *
		 *     *
		 *     X***X
		 * </code>
		 * 
		 * This code will 'fill in' these points and then add them to the
		 * queue.
		 */

		/*
		 * We need to know the previous point to fill in the path.
		 */
        if (waypoints.size() == 0) {
			/*
			 * There is no last point, reset the queue to add the player's
			 * current location.
			 */
            reset();
        }
		
		/*
		 * We retrieve the previous point here.
		 */
        Point last = waypoints.peekLast();
		
		/*
		 * We now work out the difference between the points.
		 */
        int diffX = x - last.x;
        int diffY = y - last.y;
		
		/*
		 * And calculate the number of steps there is between the points.
		 */
        int max = Math.max(Math.abs(diffX), Math.abs(diffY));
        for (int i = 0; i < max; i++) {
			/*
			 * Keep lowering the differences until they reach 0 - when our
			 * route will be complete.
			 */
            if (diffX < 0) {
                diffX++;
            } else if (diffX > 0) {
                diffX--;
            }
            if (diffY < 0) {
                diffY++;
            } else if (diffY > 0) {
                diffY--;
            }
			
			/*
			 * Add this next step to the queue.
			 */
            addStepInternal(x - diffX, y - diffY);
        }
    }

    /**
     * Adds a single step to the queue internally without counting gaps.
     * This method is unsafe if used incorrectly so it is private to protect
     * the queue.
     *
     * @param x The x coordinate of the step.
     * @param y The y coordinate of the step.
     */
    private void addStepInternal(int x, int y) {
		/*
		 * Check if we are going to violate capacity restrictions.
		 */
        if (waypoints.size() >= MAXIMUM_SIZE) {
			/*
			 * If we are we'll just skip the point. The player won't get a
			 * complete route by large routes are not probable and are more
			 * likely sent by bots to crash servers.
			 */
            return;
        }
		
		/*
		 * We retrieve the previous point (this is to calculate the direction
		 * to move in).
		 */
        Point last = waypoints.peekLast();
		
		/*
		 * Now we work out the difference between these steps.
		 */
        int diffX = x - last.x;
        int diffY = y - last.y;
		
		/*
		 * And calculate the direction between them.
		 */
        int dir = Directions.getMoveDirection(diffX, diffY);
		
		/*
		 * Check if we actually move anywhere.
		 */
        if (dir > -1) {
			/*
			 * We now have the information to add a point to the queue! We create
			 * the actual point object and add it.
			 */
            waypoints.add(new Point(x, y, dir));

        }
    }

    /**
     * Processes the next player's movement.
     */
    public void processMovement() {
		/*if(entity.getAttributes().isSet("isResting")) {
			reset();
		}*/
		/*
		 * Store the teleporting flag.
		 */
        boolean teleporting = entity.hasTeleportTarget();
        int lastPlane = entity.getLocation().getZ();
		/*
		 * The points which we are walking to.
		 */
        Point walkPoint = null, runPoint = null;
		
		/*
		 * Checks if the player is teleporting i.e. not walking.
		 */
        if (teleporting) {
			/*
			 * Reset the walking queue as it will no longer apply after the
			 * teleport.
			 */
            reset();
			/*
			 * Set the 'teleporting' flag which indicates the player is
			 * teleporting.
			 */
            entity.setTeleporting(true);
			/*
			 * Sets the player's new location to be their target.
			 */
            entity.setLocation(entity.getTeleportTarget());

            entity.updateCoverage(entity.getTeleportTarget());
			/*
			 * Resets the teleport target.
			 */
            entity.resetTeleportTarget();
        } else {
			/*
			 * If the player isn't teleporting, they are walking (or standing
			 * still). We get the next direction of movement here.
			 */
            walkPoint = getNextPoint();
			
			/*
			 * Technically we should check for running here.
			 */
            if (runToggled || runQueue) {
                if (!entity.getAttributes().isSet("stopActions")) {
                    runPoint = getNextPoint();
                }
            }
			/*
			 * Run energy
			 */

            if (runPoint != null) {
                if (entity.isPlayer()) {
                    Player player = (Player) entity;
                    if (player.getVariables().getRunningEnergy() > 0) {
                        double modifier = (player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.BOOTS.ordinal()) == 88 ? 0.90 : 1)
                                * (player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.PLATE.ordinal()) == 14936 ? 0.90 : 1)
                                * (player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.LEGS.ordinal()) == 14938 ? 0.90 : 1);
                        // Item weight reduction?????
                        player.getBonuses().setWeight(player.getBonuses().getWeight() + modifier);
                        //player.getVariables().setRunningEnergy(player.getVariables().getRunningEnergy() - (1 * modifier), true);
                        //	System.out.println("Reduction: " + getEnergyDepletion(player));
                        player.getVariables().setRunningEnergy(player.getVariables().getRunningEnergy() - (getEnergyDepletion(player) * modifier), true);
                    } else {
                        player.getPacketSender().sendConfig(173, 0);
                        player.getWalkingQueue().setRunningQueue(false);
                        player.getWalkingQueue().setRunningToggled(false);
                    }
                }
            } else {
                if (entity.isPlayer()) {
                    Player p = (Player) entity;
                    if (p.getVariables().getRunningEnergy() < 100) {
                        //double restoreAmount = p.getSkills().getLevel(Skills.AGILITY) / 100;
                        //p.getVariables().setRunningEnergy(p.getVariables().getRunningEnergy() + (restoreAmount < 0.2 ? 0.2 : restoreAmount), true);
                        //System.out.println("Restore: " + getEnergyRecovery(p));
                        p.getVariables().setRunningEnergy(p.getVariables().getRunningEnergy() + getEnergyRecovery(p), true);
                    }
                }
            }
			/*
			 * Now set the sprites.
			 */
            int walkDir = walkPoint == null ? -1 : walkPoint.dir;
            int runDir = runPoint == null ? -1 : runPoint.dir;
            entity.getSprites().setSprites(walkDir, runDir);
        }
		/*
		 * Check for a map region change, and if the map region has changed, set
		 * the appropriate flag so the new map region packet is sent.
		 */
        Location entity_location = entity.getLocation();
        Location last_region = entity.getLastKnownRegion();
        int lastMapRegionX = last_region.getChunkX();
        int lastMapRegionY = last_region.getChunkY();
        int regionX = entity_location.getChunkX();
        int regionY = entity_location.getChunkY();
        int size = ((Constants.MAP_SIZES[entity.getMapSize()] >> 3) / 2) - 1;
        boolean changed = Math.abs(lastMapRegionX - regionX) >= size || Math.abs(lastMapRegionY - regionY) >= size;
        World.updateEntityRegion(entity, false);
        if (changed) {
            entity.setMapRegionChanging(true);
        } else if (entity.isPlayer() && lastPlane != entity.getLocation().getZ()) {
            ((Player) entity).setClientHasntLoadedMapRegion();
        }
    }


    private static double getEnergyDepletion(Player player) {
        final double energy_used_per_tile = 0.318;
        return energy_used_per_tile * 2.5 * Math.pow(Math.E, 0.0027725887222397812376689284858327062723020005374410 * player.getBonuses().getWeight());
    }

    private static double getEnergyRecovery(Player player) {
        final double basePerTick = 0.096;
        return basePerTick * Math.pow(Math.E, 0.0162569486104454583293005993255170468638949631744294 * player.getSkills().getLevel(Skills.AGILITY));
    }

    /**
     * Gets the next point of movement.
     *
     * @return The next point.
     */
    public Point getNextPoint() {
		/*
		 * Take the next point from the queue.
		 */
        Point p = waypoints.poll();
			
		/*
		 * Checks if there are no more points.
		 */
        if (p == null || p.dir == -1) {
			/*
			 * Return <code>null</code> indicating no movement happened.
			 */
            return null;
        } else {
			/*
			 * Set the player's new location.
			 */
            int diffX = Directions.MOVEMENT_DIRECTION_DELTA_X[p.dir];
            int diffY = Directions.MOVEMENT_DIRECTION_DELTA_Y[p.dir];
            entity.setLocation(entity.getLocation().transform(diffX, diffY, 0));
            entity.updateCoverage(entity.getLocation().transform(diffX, diffY, 0));
			/*
			 * And return the direction.
			 */
            return p;
        }
    }

    public boolean isMoving() {
        if (entity.getSprites().getPrimarySprite() != -1 || entity.getSprites().getSecondarySprite() != -1) {
            return true;
        }
        return false;
    }

}
