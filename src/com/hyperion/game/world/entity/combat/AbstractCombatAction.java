package com.hyperion.game.world.entity.combat;

import com.hyperion.game.content.minigames.clanwars.session.WarSession;
import com.hyperion.game.content.minigames.duelarena.DuelSession;
import com.hyperion.game.content.miniquests.impl.MageArena;
import com.hyperion.game.content.skills.slayer.SlayerRequirements;
import com.hyperion.game.world.TeleportAreaLocations;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.data.WeaponData;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.combat.data.magic.MagicEffects;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.AttackVars;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.entity.player.AttackVars.CombatStyle;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;

public abstract class AbstractCombatAction implements CombatAction {

    @Override
    public void executeAttack(Entity aggressor, Entity victim, int weapon, int ammo) {
        if (victim.isPlayer()) {
            Player player = (Player) victim;
            player.getAttributes().set("skipResetFace", true);
            player.getInterfaceSettings().closeInterfaces(false);
            MainCombat.setSkull(aggressor, victim);
        }
        aggressor.getCombatState().setLastTarget(victim);
        CombatState.refreshPreviousHit(aggressor, victim);
        aggressor.getCombatState().setLastAttacked(System.currentTimeMillis());
        victim.getCombatState().setCurrentAttacker(aggressor);
        victim.getCombatState().setPreviousAttacker(aggressor);
    }

    @Override
    public boolean canAttack(Entity aggressor, Entity victim) {
        if (victim.isNPC() && victim.getLocation().getRegionId() == 9275) {
            NPC npc = (NPC) victim;
            if (npc.getId() == 1 || npc.getId() == 20) {
                if (aggressor.isPlayer()) {
                    ((Player) aggressor).getPacketSender().sendMessage("You can't attack this npc.");
                }
                return false;
            }
        }
        if (aggressor.getCombatState().isDead()) {
            return false;
        }
        if (victim.getCombatState().isDead()) {
            if (aggressor.isPlayer() && victim.isPlayer()) {
                aggressor.setInteractingEntity(victim);
            }
            return false;
        }
        if (aggressor.getLocation().getZ() != victim.getLocation().getZ()) {
            return false;
        }
        if (aggressor.isNPC()) {
            NPC npc = (NPC) aggressor;
            if (npc.getSpawnLocation().withinActualDistance(victim.getLocation(), 20)) {
                double player_distance = Location.distanceFormula(victim.getLocation().getX(), victim.getLocation().getY(), npc.getSpawnLocation().getX(), npc.getSpawnLocation().getY());
                double npc_distance = Location.distanceFormula(npc.getLocation().getX(), npc.getLocation().getY(), npc.getSpawnLocation().getX(), npc.getSpawnLocation().getY());
                boolean notClose = false;
                //System.out.println("npc follow distance : " + npc.getFollowDistance() + " ~ cur_plr_dis : " + player_distance + " ~ cur_npc_dis : " + npc_distance);
                if (player_distance > 13 || npc_distance > npc.getFollowDistance()) {
                    notClose = true;
                }
                if (notClose) {
                    MainCombat.endCombat(aggressor, 1);
                    npc.getCombatState().resetCombatTimers();
                    npc.setWalkingHome(true);
                    npc.getWalkingQueue().addStep(npc.getSpawnLocation().getX(), npc.getSpawnLocation().getY());
                    return false;
                }
            }
            if (aggressor.getAttributes().isSet("stopCombat")) {
                return false;
            }
            if (((NPC) aggressor).getId() == 183) {
                if (!TeleportAreaLocations.atPiratesClueArea(victim.getLocation())) {
                    ((NPC) aggressor).setWalkingHome(true);
                    return false;
                }
            }
        }
        if (aggressor.isPlayer()) {
            if (aggressor.getAttributes().isSet("stopActions")) {
                return false;
            }
        }
        if (victim.isNPC()) {
            NPC npc = (NPC) victim;
            if (npc.getMaxHp() <= 0) {
                return false;
            }
            if (aggressor.isPlayer()) {
                Player player = (Player) aggressor;
                /*if (npc.getSpawn() == null && !npc.getAttributes().isSet("attackable")) {
                    if (!npc.isAttackable(player)) {
						MainCombat.endCombat(player, 2);
						player.getPacketSender().sendMessage("That is not yours to kill.");
						return false;
					}
				}*/
                int slayerLvl = SlayerRequirements.getRequiredSlayerLevel(npc.getDefinition().getName());
                if (slayerLvl > 1) {
                    if (player.getSkills().getLevelForXp(Skills.SLAYER) < slayerLvl) {
                        MainCombat.endCombat(player, 2);
                        player.getPacketSender().sendMessage("You need " + slayerLvl + " Slayer to attack this monster.");
                        return false;
                    }
                }
				/*if (npc.getId() == 6222 || npc.getId() == 6305 || npc.getId() == 6306) {
					CombatType type = aggressor.getCombatState().getCombatType();
					if (type.equals(CombatType.MELEE)) {
						player.getPacketSender().sendMessage("I can't reach that!");
						MainCombat.endCombat(aggressor, 2);
						return false;
					}
				}*/
                if (TeleportAreaLocations.atMageArena(player.getLocation()) || MageArena.atArea(player)) {
                    if (!(MagicEffects.isCasting(player) || MagicEffects.isAutoCasting(player))) {
                        MainCombat.endCombat(player, 2);
                        player.getPacketSender().sendMessage("You may only use Magic spells within this arena!");
                        return false;
                    }
                }
            }
        }
        if (aggressor.isPlayer() && victim.isPlayer()) {
            Player player = (Player) aggressor;
            Player other = (Player) victim;
            DuelSession duelSession = player.getDuelSession();
            DuelSession duelSession2 = other.getDuelSession();
            if (duelSession != null) {
                if (duelSession.isOpponent(other)) {
                    if (duelSession.isCommencing()) {
                        return true;
                    } else {
                        //player.getFrames().sendMessage("The duel hasn't started yet!");
                        return false;
                    }
                } else if (!duelSession.isOpponent(other)) {
                    MainCombat.endCombat(aggressor, 2);
                    player.getPacketSender().sendMessage("That player is not your opponent.");
                    return false;
                }
            } else if (duelSession2 != null) {
                if (!duelSession2.isOpponent(other)) {
                    MainCombat.endCombat(aggressor, 2);
                    player.getPacketSender().sendMessage("That player is not your opponent.");
                    return false;
                }
            } else if (duelSession == null || duelSession2 == null) {
                if (TeleportAreaLocations.inDuelArenas(victim.getLocation()) || TeleportAreaLocations.inDuelArenas(aggressor.getLocation())) {
                    //We've already checked for duel sessions
                    MainCombat.endCombat(aggressor, 2);
                    player.getPacketSender().sendMessage("That player is not your opponent.");
                    return false;
                }
            }
            boolean victimInAttackableArea = TeleportAreaLocations.inAttackableArea(victim.getCentreLocation());
            boolean attackerInAttackableArea = TeleportAreaLocations.inAttackableArea(aggressor.getCentreLocation());

            if (!(victimInAttackableArea || attackerInAttackableArea) && !(victim.getAttributes().isSet("clansession")
                    && aggressor.getAttributes().isSet("clansession"))) {
                MainCombat.endCombat(aggressor, 2);
                player.getPacketSender().sendMessage("You cannot attack another player outside of the Wilderness.");
                return false;
            }
            if (player.getAttributes().isSet("clansession")) {
                WarSession war_session = player.getAttributes().get("clansession");
                if (war_session.getTeam1().contains(player)) {
                    if (war_session.getTeam1().contains(other) || war_session.getDefeatedPlayers1().contains(other)
                            || war_session.getDefeatedPlayers1().contains(player)) {
                        player.getPacketSender().sendMessage("You can not attack this player.");
                        return false;
                    }
                } else {
                    if (war_session.getTeam2().contains(other) || war_session.getDefeatedPlayers2().contains(other)
                            || war_session.getDefeatedPlayers2().contains(player)) {
                        player.getPacketSender().sendMessage("You can not attack this player.");
                        return false;
                    }
                }
            }
            if (TeleportAreaLocations.isInPVP(victim) || TeleportAreaLocations.isInPVP(player)) {
                int otherCombatLevel = other.getSkills().getCombatLevel();
                boolean withinLvl = (otherCombatLevel >= player.getLowestLevel() && otherCombatLevel <= player.getHighestLevel());
                if (!withinLvl) {
                    MainCombat.endCombat(aggressor, 2);
                    player.getPacketSender().sendMessage("Your level difference is too great!");
                    player.getPacketSender().sendMessage("You need to move deeper into the Wilderness.");
                    return false;
                }
            }
        }
        if (!TeleportAreaLocations.isInMultiZone(aggressor, aggressor.getLocation()) || !TeleportAreaLocations.isInMultiZone(victim, victim.getLocation())) {
            if (aggressor.getCombatState().getPreviousHit() > 0) {
                if (aggressor.getCombatState().getPreviousHit() > (System.currentTimeMillis() - 4000)) {
                    if (aggressor.getCombatState().getPreviousAttacker() != null && victim != aggressor.getCombatState().getPreviousAttacker()) {
                        if (aggressor.isPlayer()) {
                            ((Player) aggressor).getPacketSender().sendMessage("You're already under attack.");
                        }
                        MainCombat.endCombat(aggressor, 2);
                        return false;
                    }
                }
            }
            if (aggressor.getCombatState().getNpcLastAttacked() > 0) {
                if (aggressor.getCombatState().getNpcLastAttacked() > (System.currentTimeMillis() - 4000)) {
                    if (aggressor.getCombatState().getPreviousAttacker() != null && victim != aggressor.getCombatState().getPreviousAttacker()) {
                        if (aggressor.isPlayer()) {
                            ((Player) aggressor).getPacketSender().sendMessage("You're already under attack.");
                        }
                        MainCombat.endCombat(aggressor, 2);
                        return false;
                    }
                }
            }
            if (victim.getCombatState().getPreviousHit() > 0) {
                if (victim.getCombatState().getPreviousHit() > (System.currentTimeMillis() - 4000)) {
                    if (victim.getCombatState().getPreviousAttacker() != null && aggressor != victim.getCombatState().getPreviousAttacker()) {
                        if (aggressor.isPlayer()) {
                            ((Player) aggressor).getPacketSender().sendMessage("Someone else is already fighting your opponent.");
                        }
                        MainCombat.endCombat(aggressor, 2);
                        return false;
                    }
                }
            }
            if (aggressor.isNPC() || victim.isNPC()) {
                if (victim.getCombatState().getNpcLastAttacked() > 0) {
                    if (victim.getCombatState().getNpcLastAttacked() > (System.currentTimeMillis() - 4000)) {
                        if (victim.getCombatState().getPreviousAttacker() != null && aggressor != victim.getCombatState().getPreviousAttacker()) {
                            if (aggressor.isPlayer()) {
                                ((Player) aggressor).getPacketSender().sendMessage("Someone else is already fighting your opponent.");
                            }
                            MainCombat.endCombat(aggressor, 2);
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    @Override
    public boolean isInCombatDistance(Entity aggressor, Entity victim) {
        int dist = 0;
        if (aggressor.isNPC()) {
            if (Location.standingOn(aggressor, victim)) {
                return false;
            }
        }
        if (aggressor.isPlayer() && victim.isPlayer()) {
            if (aggressor.getCombatState().getCombatType().equals(CombatType.MELEE)) {
                if (aggressor.getCombatState().isFrozen()) {
                    if (Location.isDiagonal(aggressor, victim)) {
                        return false;
                    }
                }
            }
        }
        if (Location.standingOn(aggressor, victim)) {
            if (aggressor.getCombatState().isFrozen()) {
                MainCombat.endCombat(aggressor, 1);
                return false;
            }
        }
        if (!ProjectilePathFinder.hasLineOfSight(aggressor, victim, false)) {
            if (!Location.standingOn(aggressor, victim)) {
                return false;
            } else if (Location.standingOn(aggressor, victim)) {
                if (aggressor.getCombatState().isFrozen()) {
                    return false;
                }
            }
        }
        boolean aggressorRunning = (aggressor.getSprites().getSecondarySprite() != -1);
        boolean bothRunning = (aggressor.getSprites().getSecondarySprite() != -1 && victim.getSprites().getSecondarySprite() != -1);
        if (bothRunning) {
            dist += 2; //2
        } else if (aggressorRunning) {
            dist += 1;
        }
        if (aggressor.getCombatState().getCombatType().equals(CombatType.RANGE)) {
            if (aggressor.isPlayer()) {
                AttackVars av = ((Player) aggressor).getSettings().getAttackVars();
                if (av.getStyle().equals(CombatStyle.RANGE_DEFENSIVE)) {
                    return Location.isWithinDistance(aggressor, victim, 9 + dist);
                }
            }
            return (Location.isWithinDistance(aggressor, victim, 7 + dist));
        } else if (aggressor.getCombatState().getCombatType().equals(CombatType.MAGE)) {
            return (Location.isWithinDistance(aggressor, victim, 8 + dist));
        } else if (aggressor.getCombatState().getCombatType().equals(CombatType.MELEE)) {
            if (aggressor.isPlayer()) {
                if (victim.isPlayer()) {
                    if (Location.isDiagonal(aggressor, victim)) {
                        return false;
                    }
                    if (bothRunning) {
                        return Location.isWithinDistance(aggressor, victim, 2);
                    }
                }
            }
            return (Location.isWithinDistance(aggressor, victim, WeaponData.getWeaponDistance(aggressor)));
        }
        return false;
    }

    @Override
    public boolean canPerformSpecial(Entity aggressor, Entity victim, int weapon, int ammo) {
        return false;
    }

    @Override
    public int getDamage(Entity aggressor, Entity victim, int weapon, int ammo, int spell, boolean special) {
        return 0;
    }

    @Override
    public void handleDamage(Entity aggressor, Entity victim, int damage, int weapon, int ammo, int spell, int extraTicks, Graphic graphic, boolean secondHit, boolean multiSpell) {

    }

    @Override
    public CombatType getCombatType() {
        return null;
    }
}
