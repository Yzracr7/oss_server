package com.hyperion.game.world.entity.combat;

import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Graphic;

public interface CombatAction {

	/**
	 * Starts the attack.
	 * @param aggressor - entity who's attacking.
	 * @param target - entity who's getting hit.
	 * @param weapon - has a weapon?
	 * @param ammo - has ammo?
	 */
	public void executeAttack(Entity aggressor, Entity victim, int weapon, int ammo);
	
	/**
	 * 
	 * @param aggressor
	 * @param target
	 * @return
	 */
	public boolean canAttack(Entity aggressor, Entity victim);
	
	/**
	 * 
	 * @param aggressor
	 * @param target
	 * @return
	 */
	public boolean isInCombatDistance(Entity aggressor, Entity victim);
	
	/**
	 * 
	 * @param aggressor
	 * @param victim
	 * @param weapon
	 * @param ammo
	 * @return
	 */
	public boolean canPerformSpecial(Entity aggressor, Entity victim, int weapon, int ammo);
	
	/**
	 * 
	 * @param aggressor
	 * @param victim
	 */
	public int getDamage(Entity aggressor, Entity victim, int weapon, int ammo, int spell, boolean special);
	
	/**
	 * 
	 * @param aggressor
	 * @param victim
	 * @param damage
	 */
	public void handleDamage(Entity aggressor, Entity victim, int damage, int weapon, int ammo, int spell, int extraTicks, Graphic graphic, boolean secondHit, boolean multiSpell);
	
	public void handleDamage(Entity aggressor, Entity victim, int damage, int weapon, boolean special);
	
	public void handleDamage(Entity aggressor, Entity victim, int damage, int weapon, int extraTicks, Graphic graphic);
	
	public void handleDamage(Entity aggressor, Entity victim, int damage, int weapon, int ammo, int extraTicks, Graphic graphic, boolean secondHit);
	
	public void handleDamage(Entity aggressor, Entity victim, int damage, int weapon, int ammo, int extraTicks, Graphic graphic, boolean secondHit, boolean spec);
	
	public void handleDamage(Entity aggressor, Entity victim, int damage, int weapon, int ammo, int extraTicks, boolean secondHit, Graphic graphic);
	
	public void handleDamage(Entity aggressor, Entity victim, int damage, int spell, Graphic graphic, boolean multiSpell);
	
	public CombatType getCombatType();
}
