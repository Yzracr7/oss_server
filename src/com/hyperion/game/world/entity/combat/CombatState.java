package com.hyperion.game.world.entity.combat;

import java.util.LinkedList;
import java.util.List;

import com.hyperion.game.content.skills.magic.RuneReplacers;
import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
import com.hyperion.utility.NumberUtils;

/**
 * Holds details related to a specific entity's state in combat.
 *
 * @author Graham Edgecombe
 * @author Nando - edits from the original class.
 */
public final class CombatState {

    /**
     * Creates combat state for an entity.
     *
     * @param attacker
     */
    public CombatState(Entity attacker) {
        this.attacker = attacker;
        setDefaultStates();
    }

    /**
     * Attacks the target.
     *
     * @param victim
     */
    public void startAttack(Entity victim) {
        if (attacker.isNPC()) {
            // if (attacker.getHp() <= 0)
            // return;

        } else if (attacker.isPlayer()) {
            ((Player) attacker).resetActionAttributes();
        }
        // attacker.face(victim.getLocation());
        attacker.setInteractingEntity(victim);
        // PathState pathState = player.execute_path(new VariablePathFinder(-1,
        // 0, 0, npc.getSize(), npc.getSize()), x, y);
        // attacker.face(victim.getLocation());
        attacker.getCombatState().setTarget(victim);
        attacker.getFollowing().setFollowing(victim, true);

    }

    private void setDefaultStates() {
        this.lastTarget = null;
        this.hitDelay = 1;
        this.canMove = true;
        this.eatingState = true;
        this.drinkingState = true;
        this.isFreezable = true;
        this.logoutTimer = (long) 0;
        this.lastVengeance = (long) 0;
        this.previousHit = (long) 0;
        this.lastAttacked = (long) 0;
        this.lastMaged = (long) 0;
        this.lastMeleed = (long) 0;
        this.lastRanged = (long) 0;
        this.npcLastAttacked = (long) 0;
        this.damageMap = new DamageMap();
        this.combatType = CombatType.MELEE;
    }

    private final List<Hit> hits = new LinkedList<Hit>();

    /**
     * The damage map of this entity.
     */
    private transient DamageMap damageMap = new DamageMap();

    /**
     * The attacker of this combat state.
     */
    private transient Entity attacker;

    /**
     * The attacker's victim.
     */
    private transient Entity target;

    /**
     * Last entity we attacked.
     */
    private transient Entity lastTarget;

    /**
     * The attackers combat cycles.
     */
    private transient int combatCycles;

    /**
     * Used for combat combos.
     */
    private transient Long lastMaged;

    private transient Long lastRanged;

    private transient Long lastMeleed;

    /**
     * This attackers combat type.
     */
    private transient CombatType combatType = CombatType.MELEE;

    /**
     * This attackers last combat type.
     */
    private transient CombatType lastCombatType = CombatType.MELEE;

    /**
     * Used for g maul.
     */
    private transient boolean ignoreCycles;

    /**
     * Delay till damage hits.
     */
    private transient int hitDelay;

    /**
     * The attackers death state.
     */
    private transient boolean isDead;

    /**
     * This attackers poison amount.
     */
    private int poisonAmount;

    /**
     * This attackers venom amount.
     */
    private int venomAmount;

    /**
     * This attackers teleblock timer.
     */
    private int teleblockTime;

    /**
     * This attackers last previous hit.
     */
    private transient Long previousHit;

    /**
     * This attackers last attack on an entity.
     */
    private transient Long lastAttacked;

    /**
     * The entity who last hit us.
     */
    private transient Entity previousAttacker;

    /**
     * The entity who is hitting us.
     */
    private transient Entity currentAttacker;

    /**
     * The movement flag.
     */
    private transient boolean canMove = true;

    /**
     * The frozen flag.
     */
    private transient boolean isFreezable;

    /**
     * Frozen state.
     */
    private transient boolean frozen;

    /**
     * Vengeance state.
     */
    private transient boolean vengeance = false;

    /**
     * Last time this entity venged.
     */
    private transient Long lastVengeance;

    /**
     * Recoil state.
     */
    private int recoilCount = 40;

    /**
     * Eating state.
     */
    private transient boolean eatingState = true;

    /**
     * Drinking state.
     */
    private transient boolean drinkingState = true;

    /**
     * Logout state.
     */
    private transient Long logoutTimer, npcLastAttacked;

    /**
     * Charge state.
     */
    private transient boolean isCharged;

    public int getCombatCycles() {
        return combatCycles;
    }

    public void setCombatCycles(int combatCycles) {
        this.combatCycles = combatCycles;
    }

    public void increaseCombatCycles(int amount) {
        this.combatCycles += amount;
    }

    public void deductCombatCycles(int amount) {
        if (combatCycles - amount < 0)
            this.combatCycles = 0;
        else
            this.combatCycles -= amount;
    }

    public boolean isDead() {
        return isDead;
    }

    public void setDead(boolean isDead) {
        this.isDead = isDead;
    }

    public boolean isPoisoned() {
        return poisonAmount > 0;
    }

    public int getVenomAmount() {
        return venomAmount;
    }

    public void setVenomAmount(int venomAmount) {
        this.venomAmount = venomAmount;
    }

    public boolean isVenomized() {
        return venomAmount > 0;
    }

    public int getPoisonAmount() {
        return poisonAmount;
    }

    public void setPoisonAmount(int poisonAmount) {
        this.poisonAmount = poisonAmount;
    }

    public Long getPreviousHit() {
        return previousHit;
    }

    public void setPreviousHit(int amt) {
        this.previousHit = (long) amt;
    }

    private void refreshPreviousHit() {
        this.previousHit = System.currentTimeMillis();
    }

    public static void refreshPreviousHit(Entity attacker, Entity victim) {
        if (attacker.isPlayer()) {
            victim.getCombatState().refreshPreviousHit();
        } else if (attacker.isNPC()) {
            victim.getCombatState().refreshNPCLastAttacked();
        }
        if (MainCombat.getAction(attacker).canAttack(attacker, victim)) {
            // Fix for unable to talk to npc while npc targetting you
            attacker.getCombatState().setLogoutTimer(System.currentTimeMillis());
            victim.getCombatState().setLogoutTimer(System.currentTimeMillis());
        }
    }

    public void resetCombatTimers() {
        this.previousHit = (long) 0;
        this.lastAttacked = (long) 0;
    }

    public Entity getPreviousAttacker() {
        return previousAttacker;
    }

    public void setPreviousAttacker(Entity previousAttacker) {
        this.previousAttacker = previousAttacker;
    }

    public boolean canAttackerMove() {
        return canMove;
    }

    public void setCanMove(boolean canMove) {
        this.canMove = canMove;
    }

    public boolean isFreezable() {
        return isFreezable;
    }

    public void setFreezable(boolean isFreezable) {
        this.isFreezable = isFreezable;
    }

    public boolean isTeleBlocked() {
        return getTeleblockTime() > 0;
    }

    public boolean isCharged() {
        return isCharged;
    }

    public void setCharged(boolean set) {
        this.isCharged = set;
    }

    public void executeCharge() {
        if (isDead()) {
            return;
        }
        if (this.isCharged) {
            return;
        }
        if (attacker instanceof Player) {
            Item[] runes = {new Item(554, 3), new Item(565, 3), new Item(556, 3)};
            if (!RuneReplacers.hasEnoughRunes((Player) this.attacker, runes, true)) {
                return;
            }
            if (((Player) attacker).getSkills().getLevel(6) < 80) {
                ((Player) attacker).getPacketSender().sendMessage("You need a Magic level of 80 to cast charge.");
                return;
            }
            int weapon = ((Player) attacker).getEquipment().getItemInSlot(Equipment.EQUIPMENT.WEAPON.ordinal());
            boolean deleteRunes = true;

            if (weapon == 11791 || weapon == 12904) {
                deleteRunes = NumberUtils.random(0, 7) == 1;
            }
            if (deleteRunes) {
                RuneReplacers.deleteRunes((Player) this.attacker, runes);
            } else {
                ((Player) attacker).getPacketSender().sendMessage("Your staff allows you to cast this spell without runes.");
                attacker.animate(811);
                ((Player) attacker).getPacketSender().sendMessage("You feel charged with magic power.");
            }
        }
        this.isCharged = true;
        final int tick = (100 * NumberUtils.random(200));
        World.getWorld().submit(new Tickable(attacker, tick) {
            @Override
            public void execute() {
                this.stop();
                isCharged = false;
                if (attacker instanceof Player) {
                    ((Player) attacker).getPacketSender().sendMessage("Your Magical charge fades away.");
                }
            }
        });
    }

    public boolean isVenged() {
        return vengeance;
    }

    public void setVenged(boolean vengeance) {
        this.vengeance = vengeance;
    }

    public int getRecoilCount() {
        return recoilCount;
    }

    public void setRecoilCount(int recoilCount) {
        this.recoilCount = recoilCount;
    }

    public void deductRecoilCount(int amount) {
        this.recoilCount -= amount;
    }

    public boolean getEatingState() {
        return eatingState;
    }

    public void setEatingState(boolean eatingState) {
        this.eatingState = eatingState;
    }

    public boolean getDrinkingState() {
        return drinkingState;
    }

    public void setDrinkingState(boolean drinkingState) {
        this.drinkingState = drinkingState;
    }

    public Entity getTarget() {
        return target;
    }

    public void setTarget(Entity victim) {
        this.target = victim;
    }

    public int getHitDelay() {
        return hitDelay;
    }

    public void setHitDelay(int hitDelay) {
        this.hitDelay = hitDelay;
    }

    public void increaseHitDelay(int amount) {
        this.hitDelay += amount;
    }

    public boolean isOutOfCombat(String message, boolean send) {
        boolean isOutOfCombat = MainCombat.isXSecondsSinceCombat(this.attacker, this.attacker.getCombatState().getPreviousHit(), 10000) && MainCombat.isXSecondsSinceCombat(this.attacker, this.attacker.getCombatState().getLastAttacked(), 10000);
        if (send && !isOutOfCombat) {
            if ((!message.isEmpty() && message != null)) {
                if (this.attacker instanceof Player) {
                    Player player = (Player) this.attacker;
                    player.getPacketSender().sendMessage(message);
                }
            }
        }
        return isOutOfCombat;
    }

    public CombatType getCombatType() {
        if (this.attacker.isNPC()) {
            return ((NPC) this.attacker).getDefinition().getCombatType();
        }
        return combatType;
    }

    public void setCombatType(CombatType combatType) {
        if (this.attacker.isNPC()) {
            ((NPC) this.attacker).getDefinition().setCombatType(combatType);
        } else {
            this.combatType = combatType;
        }
    }

    public boolean isFrozen() {
        return frozen;
    }

    public void setFrozen(boolean frozen) {
        this.frozen = frozen;
    }

    public boolean isIgnoringCycles() {
        return ignoreCycles;
    }

    public void setIgnoreCycles(boolean ignoreCycles) {
        this.ignoreCycles = ignoreCycles;
    }

    public Entity getCurrentAttacker() {
        return currentAttacker;
    }

    public void setCurrentAttacker(Entity currentAttacker) {
        this.currentAttacker = currentAttacker;
    }

    /**
     * Gets the damage map of this entity.
     *
     * @return The damage map.
     */
    public DamageMap getDamageMap() {
        return damageMap;
    }

    public int getTeleblockTime() {
        return teleblockTime;
    }

    public void setTbTime(int teleblockTime) {
        this.teleblockTime = teleblockTime;
    }

    public void deductTbTime(int amount) {
        this.teleblockTime -= amount;
    }

    public CombatType getLastCombatType() {
        return lastCombatType;
    }

    public void setLastCombatType(CombatType lastCombatType) {
        this.lastCombatType = lastCombatType;
    }

    public Long getLastAttacked() {
        return lastAttacked;
    }

    public void setLastAttacked(Long lastAttacked) {
        this.lastAttacked = lastAttacked;
    }

    public Long getLastVengeance() {
        return lastVengeance;
    }

    public void setLastVengeance(Long lastVengeance) {
        this.lastVengeance = lastVengeance;
    }

    public Entity getLastTarget() {
        return lastTarget;
    }

    public void setLastTarget(Entity lastTarget) {
        this.lastTarget = lastTarget;
    }

    public Long getLastMaged() {
        return lastMaged;
    }

    public void setLastMaged(Long lastMaged) {
        this.lastMaged = lastMaged;
    }

    public Long getLastRanged() {
        return lastRanged;
    }

    public void setLastRanged(Long lastRanged) {
        this.lastRanged = lastRanged;
    }

    public Long getLastMeleed() {
        return lastMeleed;
    }

    public void setLastMeleed(Long lastMeleed) {
        this.lastMeleed = lastMeleed;
    }

    public Long getLogoutTimer() {
        return logoutTimer;
    }

    public void setLogoutTimer(Long logoutTimer) {
        this.logoutTimer = logoutTimer;
    }

    public Long getNpcLastAttacked() {
        return npcLastAttacked;
    }

    public void setNpcLastAttacked(int amt) {
        this.npcLastAttacked = (long) amt;
    }

    private void refreshNPCLastAttacked() {
        this.npcLastAttacked = System.currentTimeMillis();
    }

    public List<Hit> getHitQueue() {
        return hits;
    }
}
