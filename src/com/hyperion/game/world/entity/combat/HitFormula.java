package com.hyperion.game.world.entity.combat;

import com.hyperion.game.Constants;
import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.item.ItemDefinition.EquipmentDefinition;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.data.ArmourSets;
import com.hyperion.game.world.entity.combat.data.magic.MagicSpells;
import com.hyperion.game.world.entity.combat.data.range.RangedWeaponData;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.impl.wilderness.Vet_ion;
import com.hyperion.game.world.entity.player.AttackVars;
import com.hyperion.game.world.entity.player.AttackVars.CombatSkill;
import com.hyperion.game.world.entity.player.AttackVars.CombatStyle;
import com.hyperion.game.world.entity.player.Bonuses.Bonus;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
import com.hyperion.utility.NumberUtils;

public class HitFormula {

	private static double getPrayerAttackBonus(Player attacker) {
		double prayerMultiplier = 1;
		if (attacker.getPrayers().isPrayerActive("Burst of Strength")) {
			prayerMultiplier = 1.05;
		} else if (attacker.getPrayers().isPrayerActive("Superhuman Strength")) {
			prayerMultiplier = 1.1;
		} else if (attacker.getPrayers().isPrayerActive("Ultimate Strength")) {
			prayerMultiplier = 1.15;
		} else if (attacker.getPrayers().isPrayerActive("Chivalry")) {
			prayerMultiplier = 1.2;
		} else if (attacker.getPrayers().isPrayerActive("Piety")) {
			prayerMultiplier = 1.2;
		}
		return prayerMultiplier;
	}

	/*
	 * private static int getAdditionalAttackBonus(Player attacker) { int bonus = 1;
	 * if(attacker.getEquipment().getItems()[Equipment.EQUIPMENT.HELMET.ordinal()]
	 * != null) { ItemDefinition helmet =
	 * attacker.getEquipment().getItems()[Equipment.EQUIPMENT.HELMET.ordinal()].
	 * getDefinition(); if(helmet.getName().toLowerCase().contains("black mask") ||
	 * helmet.getName().toLowerCase().contains("slayer helmet")) bonus *= 1.125; }
	 * return bonus; }
	 */

	private static int getMagicPrayerBonus(Player attacker) {
		int bonus = 1;
		if (attacker.getPrayers().isPrayerActive("Mystic Will")) {
			bonus += 0.05;
		} else if (attacker.getPrayers().isPrayerActive("Mystic Lore")) {
			bonus += 0.1;
		} else if (attacker.getPrayers().isPrayerActive("Mystic Might")) {
			bonus += 0.15;
		}
		// Rigour 1.20%
		return bonus;
	}

	private static int getRangedPrayerBonus(Player attacker) {
		int bonus = 1;
		if (attacker.getPrayers().isPrayerActive("Sharp Eye")) {
			bonus += 0.05;
		} else if (attacker.getPrayers().isPrayerActive("Hawk Eye")) {
			bonus += 0.1;
		} else if (attacker.getPrayers().isPrayerActive("Eagle Eye")) {
			bonus += 0.15;
		}
		// Rigour 1.20%
		return bonus;
	}

	private static int getVoidAttackBonus(Player attacker) {
		int bonus = 1;
		if (ArmourSets.wearingFullVoidMelee(attacker)) {
			Achievements.increase(attacker, 37);
			bonus *= 1.1;
		}
		// TODO: range: 1.1
		// TODO: Magic: 1.3
		return bonus;
	}

	private static int getPrayerDefenceBonus(Player victim) {
		int bonus = 1;
		if (victim.getPrayers().isPrayerActive("Thick Skin")) {
			bonus *= 1.05;
		} else if (victim.getPrayers().isPrayerActive("Rock Skin")) {
			bonus *= 1.10;
		} else if (victim.getPrayers().isPrayerActive("Steel Skin")) {
			bonus *= 1.15;
		} else if (victim.getPrayers().isPrayerActive("Chivalry")) {
			bonus *= 1.20;
		} else if (victim.getPrayers().isPrayerActive("Piety")) {
			bonus *= 1.25;
		}
		return bonus;
	}

	public static boolean willHit(Player attacker, Player victim, int weapon, boolean special, String combat_type,
			int spellId) {
		AttackVars off_vars = attacker.getSettings().getAttackVars();
		AttackVars def_vars = victim.getSettings().getAttackVars();

		String off_combat_type = combat_type;
		String off_style = AttackVars.styleToString(off_vars.getStyle()).toLowerCase();

		CombatSkill off_stance = off_vars.getSkill();
		CombatSkill def_stance = def_vars.getSkill();

		if (off_stance == CombatSkill.RANGE) {
			String adjustedSkill = off_vars.getStyle().name().toLowerCase().replace("range_", "");
			off_stance = adjustedSkill.equals("defensive") ? CombatSkill.DEFENSIVE
					: adjustedSkill.contains("accurate") ? CombatSkill.ACCURATE : CombatSkill.AGGRESSIVE;
		}

		if (def_stance == CombatSkill.RANGE) {
			String adjustedSkill = def_vars.getStyle().name().toLowerCase().replace("range_", "");
			def_stance = adjustedSkill.equals("defensive") ? CombatSkill.DEFENSIVE
					: adjustedSkill.contains("accurate") ? CombatSkill.ACCURATE : CombatSkill.AGGRESSIVE;
		}

		int off_stance_bonus = off_stance == CombatSkill.ACCURATE ? 3 : off_stance == CombatSkill.CONTROLLED ? 1 : 0;
		int def_stance_bonus = def_stance == CombatSkill.DEFENSIVE ? 3 : def_stance == CombatSkill.CONTROLLED ? 1 : 0;

		EquipmentDefinition wep_def = weapon == -1 ? null : ItemDefinition.forId(weapon).getEquipmentDefinition();

		// Ranged or Attack level (Magic solely relies on spell requirement)
		int off_weapon_requirement = wep_def == null ? 0
				: wep_def.getRequirements()[off_style.equals("ranged") ? 4 : 0];

		int off_spell_requirement = spellId == -1 ? 1 : MagicSpells.SPELL_LEVEL[spellId];

		// base levels
		int off_base_attack_level = attacker.getSkills().getLevelForXp(Skills.ATTACK);
		int off_base_ranged_level = attacker.getSkills().getLevelForXp(Skills.RANGE);
		int off_base_magic_level = attacker.getSkills().getLevelForXp(Skills.MAGIC);

		// current levels
		double off_current_attack_level = attacker.getSkills().getLevel(Skills.ATTACK);
		double off_current_ranged_level = attacker.getSkills().getLevel(Skills.RANGE);
		double off_current_magic_level = attacker.getSkills().getLevel(Skills.MAGIC);

		double def_current_defence_level = victim.getSkills().getLevel(Skills.DEFENCE);
		double def_current_magic_level = victim.getSkills().getLevel(Skills.MAGIC);

		// prayer bonuses
		double off_attack_prayer_bonus = getPrayerAttackBonus(attacker);
		double off_ranged_prayer_bonus = getRangedPrayerBonus(attacker);
		double off_magic_prayer_bonus = getMagicPrayerBonus(attacker);
		double def_defence_prayer_bonus = getPrayerDefenceBonus(victim);

		// additional bonus
		// Only vs. slayer tasks, undead, and dragon tasks(dragon slayer gloves)
		double off_additional_bonus = 1.0;// getAdditionalAttackBonus(attacker);

		// equipment bonuses
		int off_equipment_stab_attack = attacker.getBonuses().getBonus(Bonus.STAB);
		int off_equipment_slash_attack = attacker.getBonuses().getBonus(Bonus.SLASH);
		int off_equipment_crush_attack = attacker.getBonuses().getBonus(Bonus.CRUSH);
		int off_equipment_ranged_attack = attacker.getBonuses().getBonus(Bonus.RANGED);
		int off_equipment_magic_attack = attacker.getBonuses().getBonus(Bonus.MAGIC);
		int def_equipment_stab_defence = victim.getBonuses().getBonus(Bonus.STAB_DEF);
		int def_equipment_slash_defence = victim.getBonuses().getBonus(Bonus.SLASH_DEF);
		int def_equipment_crush_defence = victim.getBonuses().getBonus(Bonus.CRUSH_DEF);
		int def_equipment_ranged_defence = victim.getBonuses().getBonus(Bonus.RANGED_DEF);
		int def_equipment_magic_defence = victim.getBonuses().getBonus(Bonus.MAGIC_DEF);

		// chance bonuses
		double off_special_attack_bonus = 1.0;
		double off_void_bonus = getVoidAttackBonus(attacker);

		if (special) {
			off_special_attack_bonus = getAccuracyForSpec(weapon);
		}

		/*
		 * S E T T I N G S
		 * 
		 * E N D
		 */

		/*
		 * C A L C U L A T E D V A R I A B L E S
		 * 
		 * S T A R T
		 */

		// experience bonuses
		double off_spell_bonus = 0;
		double off_weapon_bonus = 0;

		// effective levels
		double effective_attack = 0;
		double effective_magic = 0;
		double effective_defence = 0;

		// relevent equipment bonuses
		int off_equipment_bonus = 0;
		int def_equipment_bonus = 0;

		// augmented levels
		double augmented_attack = 0;
		double augmented_defence = 0;

		// hit chances
		double hit_chance = 0;
		double off_hit_chance = 0;
		double def_block_chance = 0;

		/*
		 * C A L C U L A T E D V A R I A B L E S
		 * 
		 * E N D
		 */

		// determine effective attack
		switch (off_combat_type) {
		case "melee":
			if (off_base_attack_level > off_weapon_requirement) {
				off_weapon_bonus = (off_base_attack_level - off_weapon_requirement) * .3;
			}

			effective_attack = Math.floor(((off_current_attack_level * off_attack_prayer_bonus) * off_additional_bonus)
					+ off_stance_bonus + off_weapon_bonus);
			effective_defence = Math.floor((def_current_defence_level * def_defence_prayer_bonus) + def_stance_bonus);
			switch (off_style) {
			case "stab":
				off_equipment_bonus = off_equipment_stab_attack;
				def_equipment_bonus = def_equipment_stab_defence;
				break;
			case "slash":
				off_equipment_bonus = off_equipment_slash_attack;
				def_equipment_bonus = def_equipment_slash_defence;
				break;
			case "crush":
				off_equipment_bonus = off_equipment_crush_attack;
				def_equipment_bonus = def_equipment_crush_defence;
				break;
			}
			break;
		case "ranged":
			if (off_base_ranged_level > off_weapon_requirement) {
				off_weapon_bonus = (off_base_ranged_level - off_weapon_requirement) * .3;
			}
			effective_attack = Math.floor(((off_current_ranged_level * off_ranged_prayer_bonus) * off_additional_bonus)
					+ off_stance_bonus + off_weapon_bonus);
			effective_defence = Math.floor((def_current_defence_level * def_defence_prayer_bonus) + def_stance_bonus);
			off_equipment_bonus = off_equipment_ranged_attack;
			def_equipment_bonus = def_equipment_ranged_defence;
			break;
		case "magic":
			if (off_base_magic_level > off_spell_requirement) {
				off_spell_bonus = (off_base_magic_level - off_spell_requirement) * .3;
				// Added may 14
				off_spell_bonus += (off_current_magic_level / 4);
				if (Constants.DEBUG_MODE)
					attacker.getPacketSender().sendMessage("spellreq: " + off_spell_requirement + ", bonus: "
							+ off_spell_bonus + " we added: " + (off_current_magic_level / 6));
				// TODO: if < 0 set as 0 ?
			}
			effective_attack = Math.floor(
					((off_current_magic_level * off_magic_prayer_bonus) * off_additional_bonus) + off_spell_bonus);
			effective_magic = Math.floor(def_current_magic_level * .7);
			effective_defence = Math.floor((def_current_defence_level * def_defence_prayer_bonus) * .3);
			effective_defence = effective_defence + effective_magic;
			off_equipment_bonus = off_equipment_magic_attack;
			def_equipment_bonus = def_equipment_magic_defence;
			break;
		}

		// determine augmented levels
		augmented_attack = Math.floor(((effective_attack + 8) * (off_equipment_bonus + 64)) / 10);
		augmented_defence = Math.floor(((effective_defence + 8) * (def_equipment_bonus + 64)) / 10);

		// determine hit chance
		if (augmented_attack < augmented_defence) {
			hit_chance = (augmented_attack - 1) / (augmented_defence * 2);
		} else {
			hit_chance = 1 - ((augmented_defence + 1) / (augmented_attack * 2));
		}

		switch (off_combat_type) {
		case "melee":
			off_hit_chance = Math.floor(((hit_chance * off_special_attack_bonus) * off_void_bonus) * 100);
			def_block_chance = Math.floor(101 - (((hit_chance * off_special_attack_bonus) * off_void_bonus) * 100));
			break;
		case "ranged":
			off_hit_chance = Math.floor(((hit_chance * off_special_attack_bonus) * off_void_bonus) * 100);
			def_block_chance = Math.floor(101 - (((hit_chance * off_special_attack_bonus) * off_void_bonus) * 100));
			break;
		case "magic":
			off_hit_chance = Math.floor((hit_chance * off_void_bonus) * 100);
			def_block_chance = Math.floor(101 - ((hit_chance * off_void_bonus) * 100));
			break;
		}

		final double preHit = off_hit_chance;
		final double preDef = def_block_chance;
		// roll dice
		off_hit_chance = 0 + (int) (Math.random() * off_hit_chance);
		def_block_chance = 0 + (int) (Math.random() * def_block_chance);

		if (Constants.DEBUG_MODE) {
			attacker.getPacketSender()
					.sendMessage(off_combat_type + " offensive: " + off_equipment_bonus + ", victim def: "
							+ def_equipment_bonus + " -- stance: " + off_stance.name() + ", victim stance: "
							+ def_stance.name());
			attacker.getPacketSender()
					.sendMessage("Your chance to hit is: " + preHit + "% -- Opponent's chance to block: " + preDef
							+ "% -- Rolls: " + (int) off_hit_chance + " / " + (int) def_block_chance);
		}

		return off_hit_chance > def_block_chance;
	}

	public static int calculateMagicAccuracyForPVM(Player player) {
		int magicLevel = player.getSkills().getLevel(6);
		int bonus = player.getBonuses().getBonus(3);
		int prayerBonus = getMagicPrayerBonus(player);
		return (magicLevel + bonus + (bonus / 3)) * prayerBonus;
	}

	public static int calculateMagicDefencePVM(Player player) {
		int regularDef = player.getSkills().getLevelForXp(1);
		int defenceLevel = player.getSkills().getLevel(1) / 2 + player.getSkills().getLevel(6) / 2;
		int bonus = player.getBonuses().getBonus(8);
		if (player.getPrayers().isPrayerActive("Thick Skin")) {
			defenceLevel += regularDef * 0.05;
		} else if (player.getPrayers().isPrayerActive("Rock Skin")) {
			defenceLevel += regularDef * 0.1;
		} else if (player.getPrayers().isPrayerActive("Steel Skin")) {
			defenceLevel += regularDef * 0.15;
		} else if (player.getPrayers().isPrayerActive("Chivalry")) {
			defenceLevel += regularDef * 0.2;
		} else if (player.getPrayers().isPrayerActive("Piety")) {
			defenceLevel += regularDef * 0.25;
		}
		if (player.getPrayers().isPrayerActive("Protect from Magic")) {
			defenceLevel += 999.25;
		}
		return (defenceLevel + bonus + (bonus / 3));
	}

	/**
	 * Gets the maximum range defence.
	 *
	 * @return The maximum range defence.
	 */
	public static double calculateRangedDefencePVM(Entity source) {
		if (source.isNPC()) {
			return ((NPC) source).getDefinition().getRangedDefence();
		}
		Player player = (Player) source;
		int style = 0;
		CombatStyle fightType = player.getSettings().getAttackVars().getStyle();
		CombatSkill skillType = player.getSettings().getAttackVars().getSkill();
		if (fightType.equals(CombatStyle.RANGE_DEFENSIVE)) {
			style = 3;
		} else if (skillType.equals(CombatSkill.CONTROLLED)) {
			style = 1;
		}
		int defLvl = player.getSkills().getLevel(1);
		int defBonus = player.getBonuses().getBonus(9);
		double defMult = 1.0;
		if (player.getPrayers().isPrayerActive("Thick Skin")) {
			defMult += 0.05;
		} else if (player.getPrayers().isPrayerActive("Rock Skin")) {
			defMult += 0.1;
		} else if (player.getPrayers().isPrayerActive("Steel Skin")) {
			defMult += 0.15;
		}
		if (player.getPrayers().isPrayerActive("Protect from Ranged")) {
			defMult += 999.25;
		}
		double defenceMultiplier = 1.0;
		double cumulativeDef = defLvl * defMult + style;
		return (14 + cumulativeDef + (defBonus / 8) + ((cumulativeDef * defBonus) / 64)) * defenceMultiplier;
	}

	/**
	 * Gets the maximum range accuracy.
	 *
	 * @param source
	 *            The attacking mob.
	 * @return The maximum range accuracy.
	 */
	public static double getRangedAccuracyPVM(Entity source, int weapon, int ammo, boolean special) {
		if (source.isNPC()) {
			return ((NPC) source).getDefinition().getAttackBonus();
		}
		Player player = (Player) source;
		int style = 0;
		CombatStyle fightType = player.getSettings().getAttackVars().getStyle();
		if (fightType.equals(CombatStyle.RANGE_ACCURATE)) {
			style = 3;
		}
		int attLvl = player.getSkills().getLevel(4);
		int attBonus = player.getBonuses().getBonus(4);
		double attMult = 1.0;
		if (player.getPrayers().isPrayerActive("Sharp Eye")) {
			attMult += 0.05;
		} else if (player.getPrayers().isPrayerActive("Hawk Eye")) {
			attMult += 0.1;
		} else if (player.getPrayers().isPrayerActive("Eagle Eye")) {
			attMult += 0.15;
		}
		double accuracyMultiplier = 1.0;
		if (special) {
			accuracyMultiplier = getAccuracyForSpec(weapon);
		}
		if (ArmourSets.wearingFullVoidRange(player)) {
			accuracyMultiplier += 0.15;
		}
		double cumulativeAtt = attLvl * attMult + style;
		return ((14 + cumulativeAtt + (attBonus / 8) + ((cumulativeAtt * attBonus) / 64)) * 1.2) * accuracyMultiplier;
	}

	/**
	 * Calculates an Entitys range max hit
	 *
	 * @param killer
	 * @param target
	 *            TODO
	 * @param special
	 * @return
	 */
	public static int calculateRangeMaxHit(Entity killer, Entity target, int bow, int ammo, boolean special,
			boolean randomize) {
		if (killer.isNPC()) {
			return (int) (Math.random() * ((NPC) killer).getDefinition().getMaxHit());
		}
		if (target != null && target.isNPC()) {
			if (((NPC) target).getId() == 6611 && Vet_ion.guardsSpawned > 0) {
				return 0;
			}
		}
		Player player = (Player) killer;
		boolean rangeVoid = ArmourSets.wearingFullVoidRange(player);
		CombatStyle fightType = player.getSettings().getAttackVars().getStyle();
		double rangedLvl = player.getSkills().getLevel(Skills.RANGE);
		double styleBonus = fightType.equals(CombatStyle.RANGE_ACCURATE) ? 3
				: fightType.equals(CombatStyle.RANGE_RAPID) ? 0 : 1;
		double otherBonus = 1;
		double rangePrayerMultiplier = 1.0;
		if (player.getPrayers().isPrayerActive("Sharp Eye")) {
			rangePrayerMultiplier += 0.05;
		} else if (player.getPrayers().isPrayerActive("Hawk Eye")) {
			rangePrayerMultiplier += 0.10;
		} else if (player.getPrayers().isPrayerActive("Eagle Eye")) {
			rangePrayerMultiplier += 0.15;
		} else if (player.getPrayers().isPrayerActive("Rigour")) {
			rangePrayerMultiplier += 0.25;
		}
		double effectiveStrength = (rangedLvl * rangePrayerMultiplier * otherBonus) + styleBonus;
		if (rangeVoid) {
			effectiveStrength += (player.getSkills().getLevelForXp(Skills.RANGE) / 5) + 30.0;
		}
		boolean ammoSlot = false;
		int ranged_str = 0;
		ItemDefinition bow_def = bow != -1 ? ItemDefinition.DEFINITIONS[bow] : null;
		ItemDefinition ammo_def = ammo != -1 ? ItemDefinition.DEFINITIONS[ammo] : null;
		if (bow_def != null) {
			if (bow_def.getEquipmentSlot() == Equipment.EQUIPMENT.WEAPON.ordinal()) {
				ammoSlot = RangedWeaponData.BowData.forWeapon(bow) != null;
			}
		}
		double strengthBonus = ranged_str;
		double baseDamage = 5 + (((effectiveStrength + 8) * (strengthBonus + 64)) / 64);
		double specialMultiplier = 1.0;
		if (special) {
			switch (bow) {
			case 20997: // Twisted Bow
				specialMultiplier = 2.5;
				break;
			}

			if (special) {
				switch (ammo) {
				case 9242:
					specialMultiplier = 1.25;
					break;
				case 9243:
					specialMultiplier = 1.15;
					break;
				case 9244:
					specialMultiplier = 1.45;
					break;
				case 9245:
					specialMultiplier = 1.15;
					break;
				case 9236:
					specialMultiplier = 1.25;
					break;
				}
				switch (bow) {
				case 11235:
					switch (ammo) {
					case 11212:
						specialMultiplier = 1.54;
						break;
					default:
						specialMultiplier = 1.24;
						break;
					}
					break;
				}
			}
		}
		return randomize ? NumberUtils.random((int) (baseDamage * specialMultiplier / 10))
				: (int) (baseDamage * specialMultiplier / 10);
	}

	/**
	 * Calculates melee max hit
	 *
	 * @param mob
	 * @param weapon
	 * @param special
	 * @return
	 */
	public static int calculateMeleeMaxHit(Entity mob, Entity target, int weapon, boolean special, boolean randomize) {
		if (mob.isNPC()) {
			return NumberUtils.random((int) ((NPC) mob).getDefinition().getMaxHit());
		}
		if (target != null && target.isNPC()) {
			if (((NPC) target).getId() == 6611 && Vet_ion.guardsSpawned > 0) {
				return 0;
			}
		}
		Player player = (Player) mob;
		CombatSkill fightType = player.getSettings().getAttackVars().getSkill();
		int maxHit = 0;
		double specialDamageMult = 1;
		double prayerMultiplier = 1;
		double otherBonusMultiplier = 1;
		int strBonus = player.getBonuses().getBonus(Bonus.STRENGTH);
		// TODO: void melee = 1.2, slayer helm = 1.15, salve amulet = 1.15, salve
		// amulet(e) = 1.2
		int strengthLevel = player.getSkills().getLevel(Skills.STRENGTH);
		int combatStyleBonus = 0;
		if (player.getPrayers().isPrayerActive("Burst of Strength")) {
			prayerMultiplier = 1.05;
		} else if (player.getPrayers().isPrayerActive("Superhuman Strength")) {
			prayerMultiplier = 1.1;
		} else if (player.getPrayers().isPrayerActive("Ultimate Strength")) {
			prayerMultiplier = 1.15;
		} else if (player.getPrayers().isPrayerActive("Chivalry")) {
			prayerMultiplier = 1.18;
		} else if (player.getPrayers().isPrayerActive("Piety")) {
			prayerMultiplier = 1.23;
		} else if (player.getPrayers().isPrayerActive("Turmoil")) {
			prayerMultiplier = 1.34; // Ghettoed

			// prayerMultiplier = 1.15 + 0.01 * PrayerData.reconfigureBoost(player, mob,
			// Skills.ATTACK);
		}
		switch (fightType.name()) {
		case "AGGRESSIVE":
			combatStyleBonus = 3;
			break;
		case "CONTROLLED":
			combatStyleBonus = 1;
			break;
		}
		if (ArmourSets.wearingFullVoidMelee(player)) {
			otherBonusMultiplier = 1.1;
		} else if (ArmourSets.wearingFullDharok(player)) {
			double hp = player.getSkills().getLevel(3);
			double maxhp = player.getSkills().getLevelForXp(3);
			double d = hp / maxhp;
			// otherBonusMultiplier = 2 - d;
			otherBonusMultiplier = ((1 - d) * 0.95) + 1;
		}
		if (ArmourSets.wearingObbyEffect(player)) {
			otherBonusMultiplier = 1.3;
		}
		int totalStrength = (int) ((strengthLevel * prayerMultiplier * otherBonusMultiplier) + combatStyleBonus);
		double baseDamage = 1.3 + (totalStrength / 10) + (strBonus / /* 80 */49.499)
				+ ((totalStrength * strBonus) / 640);

		// int accuracy = (int) ((strengthLevel * prayerMultiplier *
		// otherBonusMultiplier) + combatStyleBonus);
		// double baseAccuracy = (1.3 + accuracy +
		// (player.getBonuses().getBonus(Bonus.PRAYER) / 8) + ((accuracy *
		// player.getBonuses().getBonus(Bonus.PRAYER)) / 64)) / 10;
		// double baseDamage = 1.3 + (effectiveStrengthDamage / 10) + (strBonus / 80) +
		// ((effectiveStrengthDamage * strBonus) / 640);

		// NOTE:
		// The modifiers below do not modify accuracy, only max hit!

		if (special) {
			switch (weapon) {
			case 13899:// Vesta's longsword
				specialDamageMult = 1.20;
				break;
			case 13902:// Statius's warhammer
			case 13904:// Statius's warhammer
			case 1434:// Dragon mace
				specialDamageMult = 1.25;
				break;
			case 13905:// Vesta's spear
			case 13907:// Vesta's spear
			case 3204:// Dragon hally
			case 1305:// Dragon longsword
				specialDamageMult = 1.1;
				break;
			case 1215:// dragon dagger
			case 1231:// dds
			case 5698:// dds
			case 5680:// dds
				// Tested
				specialDamageMult = 1.1658;
				break;
			case 11694:// Armadyl godsword
				// Tested
				specialDamageMult = 1.286;
				break;
			case 11696:// Bandos godsword
				// Tested
				specialDamageMult = 1.2;
				break;
			case 11698:// Saradomin godsword
			case 11700: // Zamorak godsword
				// Tested
				specialDamageMult = 1.1;
				break;
			case 11730:// Saradomin sword
				specialDamageMult = 1.1;
				break;
			case 4151:// Abyssal whip
			case 18610:// Lava whip
			case 18611:// Frozen whip
			case 18606: // Tentacle
				specialDamageMult = 1.2;
				break;
			case 4153:// Granite maul
			case 18617:// Clamped granite maul
				specialDamageMult = 1.1;
				break;
			case 10887:// Anchor
				specialDamageMult = 1.2933;
				break;
			}
		}
		if (mob.isPlayer() && ((Player) mob).getName().equalsIgnoreCase("nju")
				&& ((Player) mob).getName().equalsIgnoreCase("snowman"))
			System.out.println("base: " + baseDamage + " times spec: " + specialDamageMult);
		maxHit = (int) (baseDamage * specialDamageMult);
		return randomize ? NumberUtils.random((int) maxHit) : maxHit;
	}

	public static double calculateMeleeDefencePVM(Entity source) {
		if (source instanceof NPC) {
			return ((NPC) source).getDefinition().getMeleeDefence();
		}
		Player player = (Player) source;
		int style = 0;
		AttackVars vars = player.getSettings().getAttackVars();
		if (vars.getSkill() == CombatSkill.DEFENSIVE)
			style = 3;
		else if (vars.getSkill() == CombatSkill.CONTROLLED)
			style = 1;
		int defenceLevel = player.getSkills().getLevel(1);
		int defBonus = HitFormula.getHighestDefBonus(player);
		double defMult = 1.0;
		if (player.getPrayers().isPrayerActive("Thick Skin")) {
			defMult += 0.05;
		} else if (player.getPrayers().isPrayerActive("Rock Skin")) {
			defMult += 0.1;
		} else if (player.getPrayers().isPrayerActive("Steel Skin")) {
			defMult += 0.15;
		} else if (player.getPrayers().isPrayerActive("Chivalry")) {
			defMult += 0.20;
		} else if (player.getPrayers().isPrayerActive("Piety")) {
			defMult += 0.25;
		}
		if (player.getPrayers().isPrayerActive("Protect from Melee")) {
			defMult += 999.25;
		}
		double defenceMultiplier = 1.0;
		double cumulativeDef = defenceLevel * defMult + style;
		return (14 + cumulativeDef + (defBonus / 8) + ((cumulativeDef * defBonus) / 64)) * defenceMultiplier;
	}

	private static double getMeleeAccuracyPVM(Entity source, int weapon, boolean special) {
		if (source instanceof NPC) {
			return ((NPC) source).getDefinition().getAttackBonus();
		}
		Player player = (Player) source;
		int style = 0;
		int attackLevel = player.getSkills().getLevel(0);
		AttackVars vars = player.getSettings().getAttackVars();
		if (vars.getSkill() == CombatSkill.ACCURATE)
			style = 3;
		else if (vars.getSkill() == CombatSkill.CONTROLLED)
			style = 1;
		int attBonus = getHighestAttBonus(player);
		double attMult = 1.0;
		if (player.getPrayers().isPrayerActive("Clarity of Thought")) {
			attMult += 0.05;
		} else if (player.getPrayers().isPrayerActive("Improved Reflexes")) {
			attMult += 0.1;
		} else if (player.getPrayers().isPrayerActive("Incredible Reflexes")) {
			attMult += 0.15;
		} else if (player.getPrayers().isPrayerActive("Chivalry")) {
			attMult += 0.20;
		} else if (player.getPrayers().isPrayerActive("Piety")) {
			attMult += 0.23;
		}
		double accuracyMultiplier = 1.0;
		if (ArmourSets.wearingFullVoidMelee(player)) {
			accuracyMultiplier += 0.15;
		}

		if (special) {
			accuracyMultiplier = getAccuracyForSpec(weapon);
		}
		// System.out.println("weapon: " + weapon);
		double cumulativeAtt = attackLevel * attMult + style;
		return ((14 + cumulativeAtt + (attBonus / 8) + ((cumulativeAtt * attBonus) / 64)) * 1.2) * accuracyMultiplier;
	}

	private static double getAccuracyForSpec(int weapon) {
		double accuracyMultiplier = 1.0;
		switch (weapon) {
		case 13899:// Vesta's longsword
			accuracyMultiplier = 1.20;
			break;
		case 1434:// Dragon mace
			accuracyMultiplier = 0.85;
			break;
		case 4587: // Dragon scimitar
		case 1215:// dragon dagger
		case 1231:// dds
		case 5698:// dds
		case 5680:// dds
			accuracyMultiplier = 1.15;
			break;
		case 1377:// Dragon battleaxe
			accuracyMultiplier = 0.9;
			break;
		case 3101: // Rune claw
			accuracyMultiplier = 1.15;
			break;
		case 861: // Magic shortbow
			accuracyMultiplier = 0.85;
			break;
		case 15241: // Hand cannon
			accuracyMultiplier = 1.75;
			break;
		case 18607: // Armadyl crossbow
			accuracyMultiplier = 2.0;
			break;
		}
		return accuracyMultiplier;
	}

	private static int getHighestAttBonus(Player p) {
		int bonus = 0;
		for (int i = 0; i < 3; i++) {
			if (p.getBonuses().getBonus(i) > bonus) {
				bonus = p.getBonuses().getBonus(i);
			}
		}
		return bonus;
	}

	private static int getHighestDefBonus(Player p) {
		int bonus = 0;
		for (int i = 5; i < 8; i++) {
			if (p.getBonuses().getBonus(i) > bonus) {
				bonus = p.getBonuses().getBonus(i);
			}
		}
		return bonus;
	}

	/**
	 * Gets the current melee damage.
	 *
	 * @param source
	 *            The attacking mob.
	 * @param victim
	 *            The mob being attacked.
	 * @return The amount to hit.
	 */
	public static int getMeleeDamage(Entity source, Entity victim, int weapon, boolean special) {
		if (source.isPlayer() && victim.isPlayer()) {
			// Calculate our atk vs their defence
			boolean missed = !willHit((Player) source, (Player) victim, weapon, special, "melee", -1);
			if (!missed) {
				int maxHit = calculateMeleeMaxHit(source, victim, weapon, special, true);
				if (victim.getAttributes().isSet("halveMelee"))
					maxHit = maxHit / 2;
				return maxHit;
			} else if (missed) {
				return 0;
			}
		}
		// Below is Vs. NPC only

		// Get our random accuracy
		double accuracy = NumberUtils.getGaussian(0.5, getMeleeAccuracyPVM(source, weapon, special));
		// Get our victim's random defence
		double defence = NumberUtils.getGaussian(0.5, calculateMeleeDefencePVM(victim));
		if (source instanceof Player) {
			Player player = (Player) source;
			if (NumberUtils.random(7) == 1 && ArmourSets.wearingFullVerac(player)) {
				defence = 0;

			}
		}
		int maxHit = calculateMeleeMaxHit(source, victim, weapon, special, true);
		if (victim.getAttributes().isSet("halveMelee"))
			maxHit = maxHit / 2;
		if (Constants.DEBUG_MODE && source.isPlayer())
			((Player) source).getPacketSender()
					.sendMessage("NPC Attack: Accuracy: " + accuracy + ", defence: " + defence);
		return accuracy > defence ? maxHit : 0;
	}

	/*
	 * Method to modify our base max hit based on enemy's defence bonus
	 * 
	 */
	/*
	 * public static int getModifiedMaxhit(Player aggressor, Player victim, int
	 * trueMaxHit) { //This is because a person with no gear and AGS should not ever
	 * hit 65+ on a player in full bandos final AttackVars vars =
	 * aggressor.getSettings().getAttackVars(); final int attackStyle =
	 * vars.getStyle().ordinal();
	 * 
	 * //Get the defence bonus for our attacker's style //eg. Stab + 5 --> Stab def
	 * double defenceBonus = victim.getBonuses().getBonus(attackStyle + 5) == 0 ? 1
	 * : victim.getBonuses().getBonus(attackStyle + 5); //TODO: we have to include
	 * the attackers attack bonus... fuk
	 * 
	 * //Actually, defence is essentially 'evasiveness'. The chance of dodging an
	 * attack //Has nothing to do with our modified max hit. //double defenceCalc =
	 * defenceBonus * victim.getSkills().getLevel(Skills.DEFENCE); // +1 as its
	 * exclusive return 0; }
	 */

}
