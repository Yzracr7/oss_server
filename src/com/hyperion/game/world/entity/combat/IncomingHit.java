package com.hyperion.game.world.entity.combat;

import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;

/**
 * @author Nando
 */
public class IncomingHit {

	private Entity attacker;
	private CombatType type;
	private int weapon = -1;
	private int spellId = -1;
	private int damage;

	public IncomingHit(Entity attacker,CombatType type, int wep, int spell, int damage) {
		this.type = type;
		this.weapon = wep;
		this.spellId = spell;
		this.damage = damage;
		this.attacker = attacker;
	}

	public int getWeapon() {
		return weapon;
	}

	public int getSpellId() {
		return spellId;
	}

	public int getDamage() {
		return damage;
	}

	public CombatType getType() {
		return type;
	}

	public Entity getAttacker() {
		return attacker;
	}
}