package com.hyperion.game.world.entity.combat;

import com.hyperion.game.Constants;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.TeleportAreaLocations;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.actions.MagicAction;
import com.hyperion.game.world.entity.combat.actions.MeleeAction;
import com.hyperion.game.world.entity.combat.actions.RangeAction;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.combat.data.magic.MagicEffects;
import com.hyperion.game.world.entity.combat.data.magic.MagicSpells;
import com.hyperion.game.world.entity.combat.data.range.RangeData;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.AttackVars.CombatSkill;
import com.hyperion.game.world.entity.player.AttackVars.CombatStyle;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;

/**
 * @author Nando // Konceal
 */
public class MainCombat {

    public static CombatAction getAction(Entity killer) {
        if (MagicEffects.isCasting(killer) || MagicEffects.isAutoCasting(killer)) {
            killer.getCombatState().setCombatType(CombatType.MAGE);
            return MagicAction.getAction();
        } else if (RangeData.isRanging(killer)) {
            killer.getCombatState().setCombatType(CombatType.RANGE);
            return RangeAction.getAction();
        } else {
            killer.getCombatState().setCombatType(CombatType.MELEE);
        }
        return MeleeAction.getAction();
    }


    public static void executeCombatCycles(Entity killer, Entity victim) {
        CombatAction action = getAction(killer);
        if (killer.getCombatState().isDead()) {
            endCombat(killer, 1);
            return;
        }
        if (victim == null) {
            endCombat(killer, 1);
            return;
        }
        if (killer.getAttributes().isSet("isMonkey") || victim.getAttributes().isSet("isMonkey")) {
            endCombat(killer, 1);
            return;
        }
        if (victim.getCombatState().isDead() || victim.isDestroyed()) {
            if (killer.isPlayer() && victim.isPlayer()) {
                killer.setInteractingEntity(victim);
            }
            endCombat(killer, 1);
            return;
        }
        if (victim.isPlayer()) {
            Player player = (Player) victim;
            if (player.getTradingSession() != null) {
                player.getTradingSession().decline(true);
            }
            if (!player.getAttributes().isSet("closed_screen")) {
                player.getPacketSender().softCloseInterfaces();
                player.getPacketSender().closeChatboxInterface();
                player.getPacketSender().closeWelcome();
            }
        }
        if (killer.isNPC()) {
            if (killer.getHp() <= 0) {
                return;
            }
            if (NPCAttacks.hasAttack((NPC) killer, victim)) {
                return;
            }
            if (killer.getCombatState().getHitQueue().size() > 0 && killer.getCombatState().getHitQueue().get(0).getDelay() == 0
                    && killer.getCombatState().getHitQueue().get(0).getDamage() >= killer.getHp()) {
                victim.face(killer.getLocation());
                return;
            }
        }
        if (killer.isPlayer()) {
            Player player = (Player) killer;
            if (player.getDuelSession() != null) {
                if (!action.isInCombatDistance(killer, victim)) {
                    return;
                }
            }
        }
        if (!action.isInCombatDistance(killer, victim)) {
            return;
        }
        if (!action.canAttack(killer, victim)) {
            return;
        }
        int weaponId = -1;
        int ammo = -1;
        if (killer.isPlayer()) {
            weaponId = ((Player) killer).getEquipment().getItemInSlot(3);
            ammo = ((Player) killer).getEquipment().getItemInSlot(13);
        }
        if (killer.getCombatState().getCombatCycles() == 0 || killer.getCombatState().isIgnoringCycles()) {
            if (action.getCombatType().equals(CombatType.RANGE)) {//range can attack faster then melee/mage.
                if (System.currentTimeMillis() - killer.getCombatState().getLastMaged() < 2600) {
                    return;
                }
            } else {
                if (System.currentTimeMillis() - killer.getCombatState().getLastMaged() < 2800) {
                    return;
                }
            }
            action.executeAttack(killer, victim, weaponId, ammo);
        }
    }

    /**
     * Sets skull.
     *
     * @param entity
     * @param target
     */
    public static void setSkull(Entity entity, Entity target) {
        if (!TeleportAreaLocations.isInPVP(entity) || !TeleportAreaLocations.isInPVP(target)) {
            return;
        }
        if (entity.isPlayer() && target.isPlayer()) {
            Player player = (Player) entity;
            if ((player.getCombatState().getPreviousAttacker() == null
                    || !player.getCombatState().getPreviousAttacker().equals(target))) {
                if (!player.getSettings().isSkulled())
                    player.getSettings().renewSkull();
            }
        }
    }

    /**
     * @param killer
     * @param target
     * @param damage
     */
    public static void addCombatExp(Entity killer, Entity target, CombatType combatType, int damage, int spell) {
        if (damage <= 0) {
            return;
        }
        if (killer.isNPC()) {
            target.getCombatState().getDamageMap().incrementTotalDamage(killer, damage);
            return;
        }
        Player p = (Player) killer;
        CombatSkill fightType = p.getSettings().getAttackVars().getSkill();
        CombatStyle fightStyle = p.getSettings().getAttackVars().getStyle();
        if (combatType == CombatType.MELEE) {
            if (!fightType.equals(CombatSkill.CONTROLLED)) {
                int skill = 0;
                if (fightType.equals(CombatSkill.ACCURATE)) {
                    skill = Skills.ATTACK;
                } else if (fightType.equals(CombatSkill.DEFENSIVE)) {
                    skill = Skills.DEFENCE;
                } else if (fightType.equals(CombatSkill.AGGRESSIVE)) {
                    skill = Skills.STRENGTH;
                }
                p.getSkills().addExperience(skill, damage * 4);
                p.getSkills().addExperience(Skills.HITPOINTS, damage * 1.33);
            } else {
                double sharedXP = damage * 1.33;
                p.getSkills().addExperience(Skills.ATTACK, sharedXP);
                p.getSkills().addExperience(Skills.DEFENCE, sharedXP);
                p.getSkills().addExperience(Skills.STRENGTH, sharedXP);
                p.getSkills().addExperience(Skills.HITPOINTS, damage * 1.33);
            }
        } else if (combatType == CombatType.RANGE) {
            if (fightStyle.equals(CombatStyle.RANGE_ACCURATE) || fightStyle.equals(CombatStyle.RANGE_RAPID)) {
                p.getSkills().addExperience(Skills.RANGE, damage * 4);
            } else if (fightStyle.equals(CombatStyle.RANGE_DEFENSIVE)) {
                p.getSkills().addExperience(Skills.RANGE, damage * 2);
                p.getSkills().addExperience(Skills.DEFENCE, damage * 2);
            }
            p.getSkills().addExperience(Skills.HITPOINTS, damage * 1.33);
        } else if (combatType == CombatType.MAGE) {
            MagicSpells.addMagicXp(p, target, damage, spell, true);
        }
        target.getCombatState().getDamageMap().incrementTotalDamage(killer, damage);
    }

    /**
     * End combat for an entity
     *
     * @param killer
     */
    public static void endCombat(Entity killer, int type) {
        if (killer.getCombatState().getTarget() != null) {
            Entity targetsAttacker = killer.getCombatState().getTarget().getCombatState().getCurrentAttacker();
            if (targetsAttacker != null) {
                if (targetsAttacker.equals(killer)) {
                    killer.getCombatState().getTarget().getCombatState().setCurrentAttacker(null);
                }
            }
        }
        if (MagicEffects.isCasting(killer)) {
            killer.getAttributes().remove("regularCast");
        }
        killer.getCombatState().setTarget(null);
        if (type == 1) {
            killer.getFollowing().setFollowing(null, false);
            killer.getWalkingQueue().reset();
            killer.setInteractingEntity(null);
        } else if (type == 2) {
            killer.getFollowing().setFollowing(null, false);
            killer.getWalkingQueue().reset();
        }
    }

    /**
     * Used for xlogging.
     *
     * @param entity
     * @param timeSinceHit
     * @param time
     * @return
     */
    public static boolean isXSecondsSinceCombat(Entity entity, long timeSinceHit, int time) {
        if (entity.getCombatState().isDead()) {
            return false;
        }
        return System.currentTimeMillis() - timeSinceHit > time;
    }


    public static boolean checkAutoRetaliate(Entity aggressor, Entity victim) {
        boolean retaliate = false;
        if (victim.isAutoRetaliating()) {
            if (!victim.getWalkingQueue().isMoving())
                retaliate = true;
            if (!retaliate && victim instanceof NPC) {
                NPC npc = (NPC) victim;
                if (!npc.isWalkingHome()) {
                    retaliate = true;
                } else if (npc.isWalkingHome() && aggressor.getCombatState().getCombatType().equals(CombatType.MELEE))
                    retaliate = true;
            } else if (victim instanceof Player) {
                //Players don't switch between attackers in multi-combat like NPCs
                //Entity targetsAttacker = victim.getCombatState().getTarget().getCombatState().getCurrentAttacker();
                Entity victimsTarget = victim.getCombatState().getTarget();
                if (victimsTarget != null) {
                    if (!victimsTarget.equals(aggressor)) {
                        retaliate = false;
                    }
                }
            }
            if (retaliate) {
                victim.setInteractingEntity(aggressor);
                victim.getCombatState().startAttack(aggressor);
                return true;
            }
        }
        return false;
    }

    /*
     * Lowers the delay between attacks if you're going from melee to mage or backwards
     */

    public static void handleSwitches(Player player) {
        //TODO: If distance to target == 1 make it deduct 2 combat cycles?
        //	CombatType type = player.getCombatState().getCombatType();
        //	CombatType lastType = player.getCombatState().getLastCombatType();
        //if((type.equals(CombatType.MAGE) && lastType.equals(CombatType.MELEE))
        //	|| 	(type.equals(CombatType.MELEE) && lastType.equals(CombatType.MAGE)))
        //player.getCombatState().deductCombatCycles(3);
        //if(System.currentTimeMillis() - player.getCombatState().getLogoutTimer() < 2800) {
        //    player.getCombatState().setCombatCycles(0);
        ///    MainCombat.executeCombatCycles(player, p2);
        //}
    }


    /*
     * Check for pending spells before moving(eg. Casting ice blitz and then attacking with dds, or switching to cast mage then run)
     */
    public static boolean checkPending(Player player, boolean walk) {
        Entity target = player.getCombatState().getTarget() != null ? player.getCombatState().getTarget() : player.getCombatState().getLastTarget();
        if (target != null && player.getInteractingEntity() == target) {
            if (System.currentTimeMillis() - player.getCombatState().getLastAttacked() < 1200) {
                //TODO: Tickable if walk
                if (walk) {
                    if (Constants.DEBUG_MODE)
                        player.getPacketSender().sendMessage("Magic too soon - but walking so execute");
                    World.getWorld().submit(new Tickable(1) {
                        @Override
                        public void execute() {
                            player.getCombatState().setCombatCycles(0);
                            executeCombatCycles(player, target);
                            this.stop();
                        }
                    });
                    return true;
                }
                if (Constants.DEBUG_MODE)
                    player.getPacketSender().sendMessage("Magic too soon");
                return false;
            }
            player.getCombatState().setCombatCycles(0);
            executeCombatCycles(player, target);
            return true;
        }
        return false;
    }
}