package com.hyperion.game.world.entity.combat;

import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.actions.MagicAction;
import com.hyperion.game.world.entity.combat.actions.MeleeAction;
import com.hyperion.game.world.entity.combat.actions.RangeAction;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.impl.Barrelchest;
import com.hyperion.game.world.entity.npc.impl.Brutal_Green_Dragon;
import com.hyperion.game.world.entity.npc.impl.CaveKraken;
import com.hyperion.game.world.entity.npc.impl.ChaosDruid;
import com.hyperion.game.world.entity.npc.impl.Dagannoth;
import com.hyperion.game.world.entity.npc.impl.DarkWizard_7;
import com.hyperion.game.world.entity.npc.impl.DefaultAttack;
import com.hyperion.game.world.entity.npc.impl.DemonicGorilla;
import com.hyperion.game.world.entity.npc.impl.Dessous;
import com.hyperion.game.world.entity.npc.impl.Dragons;
import com.hyperion.game.world.entity.npc.impl.Elf_Warrior;
import com.hyperion.game.world.entity.npc.impl.Fareed;
import com.hyperion.game.world.entity.npc.impl.Flockleader_Geerin;
import com.hyperion.game.world.entity.npc.impl.Graador;
import com.hyperion.game.world.entity.npc.impl.Inadequacy;
import com.hyperion.game.world.entity.npc.impl.Jungle_Demon;
import com.hyperion.game.world.entity.npc.impl.KalphiteQueen;
import com.hyperion.game.world.entity.npc.impl.Kamil;
import com.hyperion.game.world.entity.npc.impl.KarmelAttacks;
import com.hyperion.game.world.entity.npc.impl.Ket_Zek;
import com.hyperion.game.world.entity.npc.impl.Kraken;
import com.hyperion.game.world.entity.npc.impl.Lava_dragon;
import com.hyperion.game.world.entity.npc.impl.Mith_Dragon;
import com.hyperion.game.world.entity.npc.impl.Monkey_Guards;
import com.hyperion.game.world.entity.npc.impl.PestZombie;
import com.hyperion.game.world.entity.npc.impl.Prayer_Draining_NPC;
import com.hyperion.game.world.entity.npc.impl.Sergeant_Grimspike;
import com.hyperion.game.world.entity.npc.impl.Sergeant_Steelwill;
import com.hyperion.game.world.entity.npc.impl.Spinolayp;
import com.hyperion.game.world.entity.npc.impl.Spiritual_Mage_GWD;
import com.hyperion.game.world.entity.npc.impl.Tzhaar_Jad;
import com.hyperion.game.world.entity.npc.impl.Water_Fiend;
import com.hyperion.game.world.entity.npc.impl.Wizard_9;
import com.hyperion.game.world.entity.npc.impl.godwars.ThrowerTrolls;
import com.hyperion.game.world.entity.npc.impl.godwars.armadyl.Armadyl_Kree;
import com.hyperion.game.world.entity.npc.impl.godwars.armadyl.Avianise;
import com.hyperion.game.world.entity.npc.impl.godwars.armadyl.Wingman_Skree;
import com.hyperion.game.world.entity.npc.impl.godwars.saradomin.Commander_Zilyana;
import com.hyperion.game.world.entity.npc.impl.wilderness.Callisto;
import com.hyperion.game.world.entity.npc.impl.wilderness.ChaosElemental;
import com.hyperion.game.world.entity.npc.impl.wilderness.ChaosFanatic;
import com.hyperion.game.world.entity.npc.impl.wilderness.Corporeal_Beast;
import com.hyperion.game.world.entity.npc.impl.wilderness.CrazyArchealogist;
import com.hyperion.game.world.entity.npc.impl.wilderness.KingBlackDragon;
import com.hyperion.game.world.entity.npc.impl.wilderness.Scorpia;
import com.hyperion.game.world.entity.npc.impl.wilderness.Scorpia_Offspring;
import com.hyperion.game.world.entity.npc.impl.wilderness.Venenatis;
import com.hyperion.game.world.entity.npc.impl.wilderness.Vet_ion;
import com.hyperion.game.world.entity.npc.impl.zulrah.Zulrah;

public class NPCAttacks {

    public static boolean hasAttack(NPC npc, Entity target) {
        CombatType type = npc.getCombatState().getCombatType();
        CombatAction action = null;
        if (type.equals(CombatType.MELEE)) {
            action = MeleeAction.getAction();
        } else if (type.equals(CombatType.RANGE)) {
            action = RangeAction.getAction();
        } else if (type.equals(CombatType.MAGE)) {
            action = MagicAction.getAction();
        }
        if (!action.canAttack(npc, target)) {
            return false;
        }
        int id = npc.getId();
        if (id == 73) {
            if (npc.getAttributes().isSet("pestzombie")) {
                new PestZombie().executeAttacks(npc, target);
                return true;
            }
        }
        switch (id) {
            case 264: // Green Dragon
            case 263: // Green Dragon
            case 262: // Green Dragon
            case 261: // Green Dragon
            case 260: // Green Dragon
            case 265: // Blue Dragon
            case 267: // Blue Dragon
            case 268: // Blue Dragon
            case 269: // Blue Dragon
            case 247: // Red Dragon
            case 248: // Red Dragon
            case 249: // Red Dragon
            case 250: // Red Dragon
            case 251: // Red Dragon
            case 252: // Black Dragon
            case 253: // Black Dragon
            case 254: // Black Dragon
            case 255: // Black Dragon
            case 256: // Black Dragon
            case 257: // Black Dragon
            case 258: // Black Dragon
            case 259: // Black Dragon
                new Dragons().executeAttacks(npc, target);
                return true;
            case 2042:
            case 2043:
            case 2044:
                new Zulrah().executeAttacks(npc, target);
                break;
            case 7144:
            case 7145:
            case 7146:
            case 7147:
            case 7148:
            case 7149:
                new DemonicGorilla().executeAttacks(npc, target);
                return true;
            case 3169:
            case 3170:
            case 3171:
            case 3172:
            case 3173:
            case 3174:
            case 3175:
            case 3176:
            case 3177:
            case 3178:
            case 3179:
            case 3180:
            case 3181:
            case 3182:
            case 3183:
                new Avianise().executeAttacks(npc, target);
                return true;
            case 3428: // Elf Warrior
            case 3429: // Elf Warrior
                new Elf_Warrior().executeAttacks(npc, target);
                return true;
            case 5275://Monkey guards
            case 5276://Monkey guards
                new Monkey_Guards().executeAttacks(npc, target);
                return true;
            case 2215://General Graador
                new Graador().executeAttacks(npc, target);
                return true;
            case 2205: // Commander Zilyana
                new Commander_Zilyana().executeAttacks(npc, target);
                return true;
            case 3097: // Wizard Lvl 9
                new Wizard_9().executeAttacks(npc, target);
                return true;
            case 2868: // Dark Wizard Lvl 20
            case 2869: // Ivgor Dark Wizard Lvl 20
            case 2870: // Dark Wizard Lvl 7
                new DarkWizard_7().executeAttacks(npc, target);
                return true;
            case 2878: // Chaos Druid
                new ChaosDruid().executeAttacks(npc, target);
                return true;
            case 2917: // Waterfiend
                new Water_Fiend().executeAttacks(npc, target);
                return true;
            case 2918://Brutal green drags
                new Brutal_Green_Dragon().executeAttacks(npc, target);
                return true;
            case 6593: //Lava_dragon
                new Lava_dragon().executeAttacks(npc, target);
                return true;
            case 2919://Mith dragons
                new Mith_Dragon().executeAttacks(npc, target);
                return true;
            case 3473://Inadequacy
                new Inadequacy().executeAttacks(npc, target);
                return true;
            case 5947://Spinolyp
            case 5961://Spinolyp
            case 5963://Spinolyp
                new Spinolayp().executeAttacks(npc, target);
                return true;
            case 963://kalph queen
                new KalphiteQueen().executeAttacks(npc, target);
                return true;
            case 319://Corp
                new Corporeal_Beast().executeAttacks(npc, target);
                return true;
            case 6618: // Crazy Archeologist
                new CrazyArchealogist().executeAttacks(npc, target);
                return true;
            case 492: // Cave kraken
            case 493: // Cave kraken
                new CaveKraken().executeAttacks(npc, target);
                return true;
            case 494: // Kraken
                new Kraken().executeAttacks(npc, target);
                return true;
            case 6615:
                new Scorpia().executeAttacks(npc, target);
                return true;
            case 6616:
                new Scorpia_Offspring().executeAttacks(npc, target);
                return true;
            case 6610: //Venenatis
                new Venenatis().executeAttacks(npc, target);
                return true;
            case 6611: //Vet'ion
                new Vet_ion().executeAttacks(npc, target);
                return true;
            case 6503: //Callisto
                new Callisto().executeAttacks(npc, target);
                return true;
            case 239://King black dragon
                new KingBlackDragon().executeAttacks(npc, target);
                return true;
            case 6619:
                new ChaosFanatic().executeAttacks(npc, target);
                break;
            case 2054:
                new ChaosElemental().executeAttacks(npc, target);
                break;
            case 931: // Thrower Troll
            case 932: // Thrower Troll
            case 933: // Thrower Troll
            case 934: // Thrower Troll
            case 4135: // Thrower Troll
            case 4136: // Thrower Troll
            case 4137: // Thrower Troll
            case 4138: // Thrower Troll
                new ThrowerTrolls().executeAttacks(npc, target);
                break;
            case 140://Dagg
            case 970://Dagg
            case 971://Dagg
            case 972://Dagg
            case 973://Dagg
            case 974://Dagg
            case 975://Dagg
            case 976://Dagg
            case 977://Dagg
            case 978://Dagg
            case 979://Dagg
            case 980://dag mother
            case 981://dag mother
            case 982://dag mother
            case 983://dag mother
            case 984://dag mother
            case 985://dag mother
            case 986://dag mother
            case 987://dag mother
            case 988://dag mother
                new Dagannoth().executeAttacks(npc, target);
                return true;
            case 2217://Sergeant Steelwill
                new Sergeant_Steelwill().executeAttacks(npc, target);
                return true;
            case 2218://Sergeant Grimspike
                new Sergeant_Grimspike().executeAttacks(npc, target);
                return true;
            case 3162://Kree'arra
                new Armadyl_Kree().executeAttacks(npc, target);
                return true;
            case 3163://Wingman Skree
                new Wingman_Skree().executeAttacks(npc, target);
                return true;
            case 3164://FlockLeaderGeerin
                new Flockleader_Geerin().executeAttacks(npc, target);
                return true;
            case 6342://Barrelchest
                new Barrelchest().executeAttacks(npc, target);
                return true;
            case 6371://karamel
                new KarmelAttacks().executeAttacks(npc, target);
                return true;
            case 3127://Jad
                new Tzhaar_Jad().executeAttacks(npc, target);
                return true;
            case 3117: //Tz-Kih(Level 22)
                new Prayer_Draining_NPC().executeAttacks(npc, target);
                return true;
            case 3118://Ket-Zek (lvl 360)
            case 3119://Ket-Zek (lvl 360)
                new Ket_Zek().executeAttacks(npc, target);
                return true;
            case 1472:
            case 477:
                new Jungle_Demon().executeAttacks(npc, target);
                return true;
            case 3459://Dessous
            case 3460://Dessous
                new Dessous().executeAttacks(npc, target);
                return true;
            case 3458://Kamil
                new Kamil().executeAttacks(npc, target);
                return true;
            case 3456://Fareed
                new Fareed().executeAttacks(npc, target);
                return true;
            case 6221: //Zammy mage
            case 6231: //Aviansie mage
            case 6257: //Saradomin mage
            case 6278: //Armoured bandos mage
                new Spiritual_Mage_GWD().executeAttacks(npc, target);
                return true;
            default:
                new DefaultAttack().executeAttacks(npc, target);
                break;
        }
        return false;
    }
}
