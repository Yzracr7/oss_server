package com.hyperion.game.world.entity.combat;

/**
 * A static utility class that displays holds and displays data for weapon
 * interfaces.
 *
 * @author lare96
 */
public final class WeaponInterfaces {

    /**
     * All of the interfaces for weapons and the data needed to display these
     * interfaces properly.
     *
     * @author lare96
     */
    public enum WeaponInterface {
        UNARMED(5855, 5857, 4, new FightType[]{FightType.UNARMED_PUNCH,
                FightType.UNARMED_KICK, FightType.UNARMED_BLOCK}),
        TRIDENT(90, 10, 5, new FightType[]{FightType.STAFF_BASH, FightType.STAFF_POUND, FightType.STAFF_FOCUS}),
        STAFF(90, 10, 5, new FightType[]{FightType.STAFF_BASH, FightType.STAFF_POUND, FightType.STAFF_FOCUS}),
        WAND(90, 10, 5, new FightType[]{FightType.STAFF_BASH, FightType.STAFF_POUND, FightType.STAFF_FOCUS}),
        GREATAXE(75, 10, 7, new FightType[]{FightType.GREATAXE_CHOP,
                FightType.GREATAXE_HACK, FightType.GREATAXE_SMASH,
                FightType.GREATAXE_BLOCK}),
        PICKAXE(83, 10, 7, new FightType[]{FightType.PICKAXE_SPIKE,
                FightType.PICKAXE_IMPALE, FightType.PICKAXE_SMASH,
                FightType.PICKAXE_BLOCK}),
        WARHAMMER(76, 10, 6, new FightType[]{FightType.WARHAMMER_POUND,
                FightType.WARHAMMER_PUMMEL, FightType.WARHAMMER_BLOCK}, 7624, 7636),
        TENTACLE(93, 10, 4, new FightType[]{FightType.WHIP_FLICK,
                FightType.WHIP_LASH, FightType.WHIP_DEFLECT}, 12323, 12335),
        WHIP(93, 10, 4, new FightType[]{FightType.WHIP_FLICK,
                FightType.WHIP_LASH, FightType.WHIP_DEFLECT}, 12323, 12335),
        SCYTHE(86, 10, 4, new FightType[]{FightType.SCYTHE_REAP,
                FightType.SCYTHE_CHOP, FightType.SCYTHE_JAB,
                FightType.SCYTHE_BLOCK}),
        FLAIL(76, 10, 5, new FightType[]{FightType.VERACS_FLAIL_POUND,
                FightType.VERACS_FLAIL_PUMMEL, FightType.VERACS_FLAIL_SPIKE,
                FightType.VERACS_FLAIL_BLOCK}),
        TWISTED_BOW(77, 10, 4, new FightType[]{FightType.SHORTBOW_ACCURATE,
                FightType.SHORTBOW_RAPID, FightType.SHORTBOW_LONGRANGE}, 7549, 7561),
        CROSSBOW(79, 10, 6, new FightType[]{FightType.CROSSBOW_ACCURATE,
                FightType.CROSSBOW_RAPID, FightType.CROSSBOW_LONGRANGE}, 7549, 7561),
        KARILS_CROSSBOW(79, 10, 4, new FightType[]{FightType.KARILS_CROSSBOW_ACCURATE,
                FightType.KARILS_CROSSBOW_RAPID, FightType.KARILS_CROSSBOW_LONGRANGE}, 7549, 7561),
        SHORTBOW(77, 10, 4, new FightType[]{FightType.SHORTBOW_ACCURATE,
                FightType.SHORTBOW_RAPID, FightType.SHORTBOW_LONGRANGE}, 7549, 7561),
        LONGBOW(81, 10, 6, new FightType[]{FightType.LONGBOW_ACCURATE,
                FightType.LONGBOW_RAPID, FightType.LONGBOW_LONGRANGE}, 7549, 7561),
        THROWNAXE(91, 10, 4, new FightType[]{
                FightType.THROWNAXE_ACCURATE, FightType.THROWNAXE_RAPID,
                FightType.THROWNAXE_LONGRANGE}, 7649, 7661),
        DART(91, 10, 3, new FightType[]{FightType.DART_ACCURATE,
                FightType.DART_RAPID, FightType.DART_LONGRANGE}, 7649, 7661),
        JAVELIN(91, 10, 4, new FightType[]{FightType.JAVELIN_ACCURATE,
                FightType.JAVELIN_RAPID, FightType.JAVELIN_LONGRANGE}, 7649, 7661),
        DRAGON_DAGGER(89, 10, 4, new FightType[]{FightType.DRAGON_DAGGER_STAB,
                FightType.DRAGON_DAGGER_LUNGE, FightType.DRAGON_DAGGER_SLASH,
                FightType.DRAGON_DAGGER_BLOCK}, 7574, 7586),
        DAGGER(89, 10, 4, new FightType[]{FightType.DAGGER_STAB,
                FightType.DAGGER_LUNGE, FightType.DAGGER_SLASH,
                FightType.DAGGER_BLOCK}, 7574, 7586),
        SWORD(82, 10, 5, new FightType[]{FightType.SWORD_STAB,
                FightType.SWORD_LUNGE, FightType.SWORD_SLASH,
                FightType.SWORD_BLOCK}, 7649, 7661),
        SCIMITAR(82, 10, 4, new FightType[]{FightType.SCIMITAR_CHOP,
                FightType.SCIMITAR_SLASH, FightType.SCIMITAR_LUNGE,
                FightType.SCIMITAR_BLOCK}),
        LONGSWORD(81, 10, 5, new FightType[]{FightType.LONGSWORD_CHOP,
                FightType.LONGSWORD_SLASH, FightType.LONGSWORD_LUNGE,
                FightType.LONGSWORD_BLOCK}, 7599, 7611),
        MACE(76, 10, 5, new FightType[]{FightType.MACE_POUND,
                FightType.MACE_PUMMEL, FightType.MACE_SPIKE,
                FightType.MACE_BLOCK}, 7624, 7636),
        KNIFE(89, 10, 3, new FightType[]{FightType.KNIFE_ACCURATE,
                FightType.KNIFE_RAPID, FightType.KNIFE_LONGRANGE}, 7649, 7661),
        CLAWS(78, 10, 4, new FightType[]{FightType.CLAWS_CHOP,
                FightType.CLAWS_SLASH, FightType.CLAWS_LUNGE,
                FightType.CLAWS_BLOCK}, 7800, 7812),
        /*CLAWS(93, 10, 4, new FightType[]{FightType.WHIP_FLICK,
                FightType.WHIP_LASH, FightType.WHIP_DEFLECT}, 7800, 7812),*/
        HALBERD(84, 10, 7, new FightType[]{FightType.HALBERD_JAB,
                FightType.HALBERD_SWIPE, FightType.HALBERD_FEND}, 8493, 8505),
        BALLISTA(79, 10, 7, new FightType[]{FightType.BALLISTA_ACCURATE,
                FightType.BALLISTA_RAPID, FightType.BALLISTA_LONGRANGE}, 7549, 7561),
        BLOWPIPE(91, 10, 3, new FightType[]{FightType.BLOWPIPE_ACCURATE,
                FightType.BLOWPIPE_RAPID, FightType.BLOWPIPE_LONGRANGE}, 7549, 7561),
        ELDER_MAUL(76, 10, 7, new FightType[]{FightType.MAUL_POUND,
                FightType.MAUL_PUMMEL, FightType.MAUL_BLOCK}),
        BATTLEAXE(75, 10, 5, new FightType[]{FightType.BATTLEAXE_CHOP,
                FightType.BATTLEAXE_HACK, FightType.BATTLEAXE_SMASH,
                FightType.BATTLEAXE_BLOCK}, 12323, 12335),
        ABYSSAL_DAGGER(89, 10, 4, new FightType[]{FightType.DRAGON_DAGGER_STAB,
                FightType.DRAGON_DAGGER_LUNGE, FightType.DRAGON_DAGGER_SLASH,
                FightType.DRAGON_DAGGER_BLOCK}, 7574, 7586),
        OBBY_RINGS(91, 10, 4, new FightType[]{FightType.OBBY_RING_ACCURATE,
                FightType.OBBY_RING_RAPID, FightType.OBBY_RING_LONGRANGE}, 7649, 7661),
        SPEAR(87, 10, 5, new FightType[]{FightType.SPEAR_LUNGE,
                FightType.SPEAR_SWIPE, FightType.SPEAR_POUND,
                FightType.SPEAR_BLOCK}, 7674, 7686),
        WARSPEAR(87, 10, 5, new FightType[]{FightType.SPEAR_LUNGE,
                FightType.SPEAR_SWIPE, FightType.SPEAR_POUND,
                FightType.SPEAR_BLOCK}, 7674, 7686),
        MAUL(76, 10, 7, new FightType[]{FightType.MAUL_POUND,
                FightType.MAUL_PUMMEL, FightType.MAUL_BLOCK}, 7474, 7486),

        //int interfaceId, int nameLineId, int speed, FightType[] fightType, int specialBar, int specialMeter) {
        ABYSSAL_BLUDGEON(76, 10, 6, new FightType[]{
                FightType.ABYSSAL_BLUDGEON_CHOP, FightType.ABYSSAL_BLUDGEON_SLASH,
                FightType.ABYSSAL_BLUDGEON_SMASH, FightType.ABYSSAL_BLUDGEON_BLOCK}, 7624, 7636);

        /*

        GRANITE_MAUL(425, 428, 7, new FightType[]{FightType.GRANITE_MAUL_POUND,
                FightType.GRANITE_MAUL_PUMMEL, FightType.GRANITE_MAUL_BLOCK}, 7474, 7486),

        DHAROKS_GREATAXE(1698, 1701, 7, new FightType[]{FightType.GREATAXE_CHOP,
                FightType.GREATAXE_HACK, FightType.GREATAXE_SMASH,
                FightType.GREATAXE_BLOCK}, 7499, 7511),

        TWO_HANDED_SWORD(4705, 4708, 7, new FightType[]{
                FightType.TWOHANDEDSWORD_CHOP, FightType.TWOHANDEDSWORD_SLASH,
                FightType.TWOHANDEDSWORD_SMASH, FightType.TWOHANDEDSWORD_BLOCK}, 7699, 7711),
        PICKAXE(5570, 5573, 5, new FightType[]{FightType.PICKAXE_SPIKE,
                FightType.PICKAXE_IMPALE, FightType.PICKAXE_SMASH,
                FightType.PICKAXE_BLOCK}),
        UNARMED(5855, 5857, 4, new FightType[]{FightType.UNARMED_PUNCH,
                FightType.UNARMED_KICK, FightType.UNARMED_BLOCK}),
        WHIP(12290, 12293, 4, new FightType[]{FightType.WHIP_FLICK,
                FightType.WHIP_LASH, FightType.WHIP_DEFLECT}, 12323, 12335),

        ANCIENT_STAFF(328, 355, 4, new FightType[]{FightType.STAFF_BASH, FightType.STAFF_POUND, FightType.STAFF_FOCUS}),
        DARK_BOW(1764, 1767, 8, new FightType[]{FightType.LONGBOW_ACCURATE,
                FightType.LONGBOW_RAPID, FightType.LONGBOW_LONGRANGE}, 7549, 7561),
        GODSWORD(4705, 4708, 6, new FightType[]{
                FightType.TWOHANDEDSWORD_CHOP, FightType.TWOHANDEDSWORD_SLASH,
                FightType.TWOHANDEDSWORD_SMASH, FightType.TWOHANDEDSWORD_BLOCK}, 7699, 7711),
        ABYSSAL_BLUDGEON(4705, 4708, 4, new FightType[]{
                FightType.ABYSSAL_BLUDGEON_CHOP, FightType.ABYSSAL_BLUDGEON_SLASH,
                FightType.ABYSSAL_BLUDGEON_SMASH, FightType.ABYSSAL_BLUDGEON_BLOCK}, 7699, 7711),
        SARADOMIN_SWORD(4705, 4708, 4, new FightType[]{
                FightType.TWOHANDEDSWORD_CHOP, FightType.TWOHANDEDSWORD_SLASH,
                FightType.TWOHANDEDSWORD_SMASH, FightType.TWOHANDEDSWORD_BLOCK}, 7699, 7711),
        ELDER_MAUL(425, 428, 6, new FightType[]{FightType.ELDER_MAUL_POUND,
                FightType.ELDER_MAUL_PUMMEL, FightType.ELDER_MAUL_BLOCK}, 7474, 7486);
*/
        /**
         * The inter that will be displayed on the sidebar.
         */
        private int interfaceId;

        /**
         * The line that the name of the item will be printed to.
         */
        private int nameLineId;

        /**
         * The attack speed of weapons using this inter.
         */
        private int speed;

        /**
         * The fight types that correspond with this inter.
         */
        private FightType[] fightType;

        /**
         * The id of the special bar for this inter.
         */
        private int specialBar;

        /**
         * The id of the special meter for this inter.
         */
        private int specialMeter;

        /**
         * Creates a new weapon inter.
         *
         * @param interfaceId  the inter that will be displayed on the sidebar.
         * @param nameLineId   the line that the name of the item will be printed to.
         * @param speed        the attack speed of weapons using this inter.
         * @param fightType    the fight types that correspond with this inter.
         * @param specialBar   the id of the special bar for this inter.
         * @param specialMeter the id of the special meter for this inter.
         */
        private WeaponInterface(int interfaceId, int nameLineId, int speed,
                                FightType[] fightType, int specialBar, int specialMeter) {
            this.interfaceId = interfaceId;
            this.nameLineId = nameLineId;
            this.speed = speed;
            this.fightType = fightType;
            this.specialBar = specialBar;
            this.specialMeter = specialMeter;
        }

        /**
         * Creates a new weapon inter.
         *
         * @param interfaceId the inter that will be displayed on the sidebar.
         * @param nameLineId  the line that the name of the item will be printed to.
         * @param speed       the attack speed of weapons using this inter.
         * @param fightType   the fight types that correspond with this inter.
         */
        private WeaponInterface(int interfaceId, int nameLineId, int speed,
                                FightType[] fightType) {
            this(interfaceId, nameLineId, speed, fightType, -1, -1);
        }

        /**
         * Gets the inter that will be displayed on the sidebar.
         *
         * @return the inter id.
         */
        public int getInterfaceId() {
            return interfaceId;
        }

        /**
         * Gets the line that the name of the item will be printed to.
         *
         * @return the name line id.
         */
        public int getNameLineId() {
            return nameLineId;
        }

        /**
         * Gets the attack speed of weapons using this inter.
         *
         * @return the attack speed of weapons using this inter.
         */
        public int getSpeed() {
            return speed;
        }

        /**
         * Gets the fight types that correspond with this inter.
         *
         * @return the fight types that correspond with this inter.
         */
        public FightType[] getFightType() {
            return fightType;
        }

        /**
         * Gets the id of the special bar for this inter.
         *
         * @return the id of the special bar for this inter.
         */
        public int getSpecialBar() {
            return specialBar;
        }

        /**
         * Gets the id of the special meter for this inter.
         *
         * @return the id of the special meter for this inter.
         */
        public int getSpecialMeter() {
            return specialMeter;
        }

        public boolean hasSpecialAttack() {
            // TODO Auto-generated method stub
            return true;
        }
    }

    /**
     * Assigns an inter to the combat sidebar based on the argued weapon.
     */
    public static void assign() {
        //TODO
    }

    public static boolean changeCombatSettings() {
        //TODO
        return false;
    }
}
