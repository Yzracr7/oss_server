package com.hyperion.game.world.entity.combat.actions;

import com.hyperion.game.content.minigames.clanwars.session.*;
import com.hyperion.game.content.skills.magic.LunarSpells;
import com.hyperion.game.content.skills.magic.RuneReplacers;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.AbstractCombatAction;
import com.hyperion.game.world.entity.combat.CombatAction;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.combat.data.magic.MagicEffects;
import com.hyperion.game.world.entity.combat.data.magic.MagicSpells;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.Hits.HitType;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

/**
 * @author Nando
 */
public class MagicAction extends AbstractCombatAction {

	private static final MagicAction INSTANCE = new MagicAction();

	/**
	 * Gets the singleton instance.
	 * @return The singleton instance.
	 */
	public static CombatAction getAction() {
		return INSTANCE;
	}

	@Override
	public void executeAttack(Entity aggressor, Entity victim, int weapon, int ammo) {
		super.executeAttack(aggressor, victim, weapon, ammo);
		if (aggressor.isNPC()) {
			NPCMagicAction.execute((NPC) aggressor, victim);
			return;
		}
		final Player player = (Player) aggressor;
		final boolean autoCast = MagicSpells.isAutoCasting(player);
		final boolean regularCast = MagicSpells.isCasting(player);
		final int spell = regularCast ? MagicSpells.getSpellIndex(player, player.getAttributes().getInt("regularCast")) : autoCast ? player.getAttributes().getInt("autocastspell") : -1;
		// spell doesn't exist
		if (spell == -1) {
			MainCombat.endCombat(player, 1);
			return;
		}
		aggressor.face(victim.getLocation());
		if (LunarSpells.handleMagicOnOtherSpells(player, victim, spell)) {
			return;
		}
		// checking levels
		if (player.getSkills().getLevel(6) < MagicSpells.SPELL_LEVEL[spell]) {
			player.getPacketSender().sendMessage("You need a Magic level of " + MagicSpells.SPELL_LEVEL[spell]+ " to cast this spell.");
			MainCombat.endCombat(player, 1);
			return;
		}
		// runes check
		if (!RuneReplacers.hasEnoughRunes(player, MagicSpells.RUNES[spell], true)) {
			MainCombat.endCombat(player, 2);
			return;
		}
		// check for requirements for casting the spell
		if (!MagicSpells.canCastSpell(player, victim, spell)) {
			return;
		}
		player.getCombatState().setLastMaged(System.currentTimeMillis());
		player.getCombatState().setLastCombatType(CombatType.MAGE);
		// delete runes
		RuneReplacers.deleteRunes(player, MagicSpells.RUNES[spell]);
		// speak animation
		if (MagicSpells.getSpellAnimation(player, spell) != -1) {
			player.animate(Animation.create(MagicSpells.getSpellAnimation(player, spell), AnimationPriority.HIGH));
		}
		// speak gfx
		if (MagicSpells.START_GFX[spell] != -1) {
			player.playGraphic(Graphic.create(MagicSpells.START_GFX[spell], 0, MagicSpells.getSpellStartHeight(player, spell)));
		}
		if (victim.getWalkingQueue().isMoving()) {
			if (spell == 53 || spell == 48) {//barrage/blitz
				final Projectiles mageRunninProj = Projectiles.create(player.getLocation(), victim.getLocation(), victim, 368, 50, 95, 50, 45, 30);
				player.executeProjectile(mageRunninProj);
			}
		}
		//speak projectile
		MagicSpells.sendProjectile(player, victim, spell);
		int endGfx = MagicSpells.END_GFX[spell];
		int endHeight = MagicSpells.getSpellEndGfxHeight(player, spell);
		if(endGfx == 369 && victim.getCombatState().isFrozen()) {//barrage orb
			endGfx = 1677;
			endHeight = 100;
		}
		handleDamage(aggressor, victim, getDamage(aggressor, victim, weapon, ammo, spell, false), spell, new Graphic(endGfx, 0, endHeight), false);
		MagicSpells.endRegularCast(player, victim, regularCast, autoCast);
	}

	@Override
	public boolean canAttack(Entity aggressor, Entity victim) {
		if (!super.canAttack(aggressor, victim)) {
			return false;
		}
		return true;
	}

	@Override
	public boolean isInCombatDistance(Entity aggressor, Entity victim) {
		if (!super.isInCombatDistance(aggressor, victim)) {
			return false;
		}
		return true;
	}

	@Override
	public int getDamage(Entity aggressor, Entity victim, int weapon, int ammo, int spell, boolean special) {
		int damage = MagicEffects.getRandomizedSpellMaxHit((Player) aggressor, victim, spell, weapon);
		if (victim.isPlayer()) {
			if (((Player) victim).getPrayers().isPrayerActive("protect from magic")) {
				damage *= 0.60;
			}
		}
		//With extra our hit could go over maxhit
		if(damage != 0) {
			if(victim.getAttributes().isSet("noExtraDamage")) {
				victim.getAttributes().remove("noExtraDamage");
			} else {
				int extra = CombatEffects.getExtraDamage(aggressor, victim, CombatType.MAGE, weapon, damage);
				if (extra != -1) {
					damage += extra;
				}
			}
		}
		return damage;
	}

	@Override
	public void handleDamage(final Entity aggressor, final Entity victim, int damage, final int weapon, final int ammo, final int spell, int extraTicks, Graphic graphic, final boolean secondHit, final boolean multiSpell) {
		int hitDelay = 3;
		Hit fixedHit = null;
		if(damage > (MagicEffects.getMaxHit((Player)aggressor, spell, weapon) * 0.9))
			fixedHit = new Hit(HitType.CRITICAL, damage, 2);
		else
			fixedHit = new Hit(damage, 2);


		final Hit combatHit = fixedHit;
		combatHit.setIncomingHit(new IncomingHit(aggressor, getCombatType(), weapon, spell, combatHit.getDamage()));
		combatHit.setTarget(victim);
		combatHit.setOwner(aggressor);
		CombatEffects.altarCombatDamage(aggressor, victim, combatHit);
		boolean splashing = combatHit.getDamage() <= 0;
		boolean halve = false;
		if (splashing) {
			graphic = Graphic.create(85, 0, 100);
		} else if (!splashing) {
			if (MagicSpells.FREEZE_TIMERS[spell] > 0) {
				if (victim.getCombatState().isFreezable()) {
					if (victim.isPlayer()) {
						//	halve = true;
						((Player) victim).getPacketSender().sendMessage("You have been frozen!");
					}
					MagicEffects.freezeTarget(aggressor, victim, halve, spell, damage);
				}
			} else if (!victim.getCombatState().isFreezable()) {
				if (spell == 53) {
					graphic = Graphic.create(1677, 0, 100);
				}
			}
		}
		if (combatHit.getDamage() > victim.getHp()) {
			combatHit.setDamage(victim.getHp());
		}
		CombatEffects.executeInitialEffects(aggressor, victim, combatHit.getDamage(), weapon);
		MainCombat.addCombatExp(aggressor, victim, getCombatType(), combatHit.getDamage(), spell);
		hitDelay += extraTicks;
		final Graphic endGraphic = graphic;
		final boolean splashed = splashing;
		if (multiSpell) {
			if (endGraphic != null) {
				victim.playGraphic(endGraphic.getId(), 0, endGraphic.getHeight());
			}
			if (victim.getAttributes().isSet("stopActions")) {
				return;
			}
			if (combatHit.getDamage() > victim.getHp()) {
				combatHit.setDamage(victim.getHp());
			}
			if (!splashed) {
				victim.inflictDamage(combatHit, false);
			}
			if (!victim.getCombatState().isDead()) {
				int defAnim = CombatAnimations.getDefendAnim(victim);
				if (defAnim != -1 && defAnim != 65535) {
					victim.animate(Animation.create(defAnim, AnimationPriority.NONE, CombatAnimations.BLOCK_ANIM_LENGTH));
				}
			}
			MainCombat.checkAutoRetaliate(aggressor, victim);
			CombatEffects.executeCombatEffects(aggressor, victim, weapon, combatHit.getDamage());
			if (!splashed) {
				MagicEffects.handleSpellEffects((Player) aggressor, victim, spell, combatHit.getDamage());
			}
			/*if (victim.isAutoRetaliating() && ((System.currentTimeMillis() - victim.getCombatState().getLastAttacked()) > 4000)) { 
				victim.getCombatState().startAttack(aggressor);
			}*/
			return;
		}
		boolean delay = true;
		if (aggressor.isPlayer() && victim.isPlayer()) {
			Player p = (Player) aggressor;
			Player pV = (Player) victim;
			int pid = pV.getAttributes().getInt("random_pid");
			if (p.getAttributes().getInt("random_pid") < pid) {
				delay = false;
			}
		}
		if (delay) {
			World.getWorld().submit(new Tickable(hitDelay) {
				@Override
				public void execute() {
					this.stop();
					if (endGraphic != null) {
						victim.playGraphic(endGraphic.getId(), 0, endGraphic.getHeight());
					}
					if (victim.getAttributes().isSet("stopActions")) {
						return;
					}
					if (combatHit.getDamage() > victim.getHp()) {
						combatHit.setDamage(victim.getHp());
					}
					if (!splashed) {
						victim.inflictDamage(combatHit, false);
					}
					if (!victim.getCombatState().isDead()) {
						int defAnim = CombatAnimations.getDefendAnim(victim);
						if (defAnim != -1 && defAnim != 65535) {
							victim.animate(Animation.create(defAnim, AnimationPriority.NONE, CombatAnimations.BLOCK_ANIM_LENGTH));
						}
					}
					if (victim.isAutoRetaliating() && ((System.currentTimeMillis() - victim.getCombatState().getLastAttacked()) > 4000)) { 
						victim.getCombatState().startAttack(aggressor);
					}
					CombatEffects.executeCombatEffects(aggressor, victim, weapon, combatHit.getDamage());
					if (!multiSpell) {
						if (!splashed) {
							Player player = (Player)aggressor;
							if(player.getAttributes().isSet("clansession")) {
								WarSession war = player.getAttributes().get("clansession");
								if(war != null && war.getMiscRules()[2]) { //Single spells only
									MagicEffects.handleSpellEffects((Player) aggressor, victim, spell, combatHit.getDamage());
									return;
								}
							}
							MagicEffects.executeMultiSpell((Player) aggressor, victim, spell);
							MagicEffects.handleSpellEffects((Player) aggressor, victim, spell, combatHit.getDamage());
						}
					}
				}
			});
		} else {
			if (endGraphic != null) {
				victim.playGraphic(endGraphic.getId(), 0, endGraphic.getHeight());
			}
			if (victim.getAttributes().isSet("stopActions")) {
				return;
			}
			if (combatHit.getDamage() > victim.getHp()) {
				combatHit.setDamage(victim.getHp());
			}
			if (!splashed) {
				victim.inflictDamage(combatHit, false);
			}
			if (!victim.getCombatState().isDead()) {
				int defAnim = CombatAnimations.getDefendAnim(victim);
				if (defAnim != -1 && defAnim != 65535) {
					victim.animate(Animation.create(defAnim, AnimationPriority.NONE, CombatAnimations.BLOCK_ANIM_LENGTH));
				}
			}
			if (victim.isAutoRetaliating() && ((System.currentTimeMillis() - victim.getCombatState().getLastAttacked()) > 4000)) { 
				victim.getCombatState().startAttack(aggressor);
			}
			CombatEffects.executeCombatEffects(aggressor, victim, weapon, combatHit.getDamage());
			if (!multiSpell) {
				if (!splashed) {
					Player player = (Player)aggressor;
					if(player.getAttributes().isSet("clansession")) {
						WarSession war = player.getAttributes().get("clansession");
						if(war != null && war.getMiscRules()[2]) { //Single spells only
							MagicEffects.handleSpellEffects((Player) aggressor, victim, spell, combatHit.getDamage());
							return;
						}
					}
					MagicEffects.executeMultiSpell((Player) aggressor, victim, spell);
					MagicEffects.handleSpellEffects((Player) aggressor, victim, spell, combatHit.getDamage());
				}
			}
		}
		
	}

	@Override
	public void handleDamage(Entity aggressor, Entity victim, int damage, int weapon, boolean special) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleDamage(Entity aggressor, Entity victim, int damage, int weapon,
			int extraTicks, Graphic graphic) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleDamage(Entity aggressor, Entity victim, int damage, int weapon,
			int ammo, int extraTicks, Graphic graphic, boolean secondHit) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleDamage(Entity aggressor, Entity victim, int damage, int weapon,
			int ammo, int extraTicks, Graphic graphic, boolean secondHit,
			boolean spec) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleDamage(Entity aggressor, Entity victim, int damage, int weapon,
			int ammo, int extraTicks, boolean secondHit, Graphic graphic) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleDamage(Entity aggressor, Entity victim, int damage, int spell, Graphic graphic, boolean multiSpell) {
		// TODO Auto-generated method stub
		this.handleDamage(aggressor, victim, damage, -1, -1, spell, 0, graphic, false, multiSpell);
	}

	@Override
	public CombatType getCombatType() {
		return CombatType.MAGE;
	}
}
