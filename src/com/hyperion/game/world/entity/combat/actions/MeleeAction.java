package com.hyperion.game.world.entity.combat.actions;

import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.content.minigames.clanwars.ClanWarsData;
import com.hyperion.game.content.minigames.clanwars.session.WarSession;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.AbstractCombatAction;
import com.hyperion.game.world.entity.combat.CombatAction;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.combat.data.ArmourSets;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.Speeds;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.Hits.HitPriority;
import com.hyperion.game.world.entity.masks.Hits.HitType;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;

/**
 * @author Nando
 */
public class MeleeAction extends AbstractCombatAction {

	private static final MeleeAction INSTANCE = new MeleeAction();
	
	/**
	 * Gets the singleton instance.
	 * @return The singleton instance.
	 */
	public static CombatAction getAction() {
		return INSTANCE;
	}
	
	@Override
	public void executeAttack(Entity aggressor, Entity victim, int weapon, int ammo) {
		super.executeAttack(aggressor, victim, weapon, ammo);
		aggressor.getCombatState().setLastCombatType(CombatType.MELEE);
		aggressor.getCombatState().setCombatType(CombatType.MELEE);
		aggressor.getCombatState().setLastMeleed(System.currentTimeMillis());
		if (!aggressor.getCombatState().isIgnoringCycles()) {
			aggressor.getCombatState().setCombatCycles(Speeds.getCombatSpeeds(aggressor, weapon));
		}
		if (canPerformSpecial(aggressor, victim, weapon, ammo)) {
			return;
		}
		aggressor.animate(Animation.create(CombatAnimations.getAttackAnim(aggressor, weapon), AnimationPriority.HIGH, CombatAnimations.getAttackAnimLength(aggressor, weapon, false)));
		handleDamage(aggressor, victim, getDamage(aggressor, victim, weapon, ammo, -1, canPerformSpecial(aggressor, victim, weapon, ammo)), weapon, false);
	}
	
	@Override
	public boolean canAttack(Entity aggressor, Entity victim) {
		if (!super.canAttack(aggressor, victim)) {
			return false;
		}
		if (aggressor.isPlayer()) {
			Player player = (Player) aggressor;
			if (player.getDuelSession() != null) {
				if (player.getDuelSession().ruleEnabled(3)) {
					MainCombat.endCombat(player, 2);
					player.getPacketSender().sendMessage("Melee combat has been disabled this duel.");
					return false;
				}
			}
			if(player.getAttributes().isSet("clansession")) {
				WarSession session = player.getAttributes().get("clansession");
				if(session != null && session.getRules()[ClanWarsData.Groups.MELEE.ordinal()] == ClanWarsData.Rules.MELEE_OFF) {
					player.getPacketSender().sendMessage("Melee combat has been disabled in this war.");
					return false;
				}
			}
		}
		return true;
	}
	
	@Override
	public boolean isInCombatDistance(Entity aggressor, Entity victim) {
		if (!super.isInCombatDistance(aggressor, victim)) {
			return false;
		}
		return true;
	}
	
	@Override
	public boolean canPerformSpecial(Entity aggressor, Entity victim, int weapon, int ammo) {
		if (aggressor.isPlayer()) {
			Player player = (Player) aggressor;
			return SpecialAttacks.canPerformSpecialAttack(player, victim, weapon, ammo);
		}
		return false;
	}
	
	@Override
	public int getDamage(Entity aggressor, Entity victim, int weapon, int ammo, int spell, boolean special) {
		int damage = HitFormula.getMeleeDamage(aggressor, victim, weapon, special);

		if (!special) {
			int extra = CombatEffects.getExtraDamage(aggressor, victim, CombatType.MELEE, weapon, damage);
			if (extra != -1) {
				damage += extra;
			}
		}
		//TODO: This is unneccesary now
		//int trueMaxHit = HitFormula.calculateMeleeMaxHit(aggressor, victim, weapon, special, false);
		//if (damage > trueMaxHit) {
		//	damage = trueMaxHit;
		//}
		if (aggressor.isPlayer()) {
			if (victim.isPlayer()) {//Player attacking a player.
				//MODIFY MAX HIT TO MATCH VICTIM'S DEFENCE BONUS
				
				//int newMaxHit = HitFormula.getModifiedMaxhit(aggressor, victim, trueMaxHit);
				//I wanted to just set a modified maximum max hit
				//But then out atker would just consistently hit his maxhit
				//We need to use the maxReducer formula and subtract
				//randomizedMaxHit(Above) - random(maxReducer)
				
				if (!ArmourSets.wearingFullVerac((Player) aggressor)) {
					if (((Player) victim).getPrayers().isPrayerActive("protect from melee")) {
						damage *= 0.60;
					}
				}
			}
		} else if (aggressor.isNPC()) {
			int id = ((NPC) aggressor).getId();
			if (victim.isPlayer()) {
				if (id != 2030) {//Verac barrows brother
					if (((Player) victim).getPrayers().isPrayerActive("protect from melee")) {
						damage = 0;
					}
				}
			}
		}
		return damage;
	}
	
	@Override
	public void handleDamage(final Entity aggressor, final Entity victim, int damage, final int weapon, int ammo, int spell, int extraTicks, Graphic graphic, boolean secondHit, boolean multiSpell) {
		int hit_delay = 1;

		int maxHit = HitFormula.calculateMeleeMaxHit(aggressor, victim, weapon, false, false);
		Hit fixedHit = null;
		if(damage > (maxHit * 0.9)) {
			fixedHit = new Hit(HitType.CRITICAL, damage, 0);
		} else {
			fixedHit = new Hit(damage, 0);
		}
		
		final Hit combatHit = fixedHit;

		combatHit.setIncomingHit(new IncomingHit(aggressor, getCombatType(), weapon, -1, combatHit.getDamage()));
		combatHit.setTarget(victim);
		combatHit.setOwner(aggressor);
		CombatEffects.altarCombatDamage(aggressor, victim, combatHit);
		if (combatHit.getDamage() > victim.getHp()) {
			combatHit.setDamage(victim.getHp());
		}
		
		CombatEffects.executeInitialEffects(aggressor, victim, combatHit.getDamage(), weapon);
		MainCombat.addCombatExp(aggressor, victim, getCombatType(), combatHit.getDamage(), -1);
		
		hit_delay += extraTicks;
		final int final_hit_delay = hit_delay;
		if (!victim.getCombatState().isDead()) {
			int defAnim = CombatAnimations.getDefendAnim(victim);
			if (defAnim != -1 && defAnim != 65535) {
					victim.animate(Animation.create(defAnim, AnimationPriority.NONE, CombatAnimations.BLOCK_ANIM_LENGTH));
			}
		}
		boolean delay = true;
		if (aggressor.isPlayer() && victim.isPlayer()) {
			Player p = (Player) aggressor;
			Player pV = (Player) victim;
			int pid = pV.getAttributes().getInt("random_pid");
			if (p.getAttributes().getInt("random_pid") < pid) {
				delay = false;
			}
		}
		if (!delay) {
			if (victim.getAttributes().isSet("stopActions")) {
				return;
			}
			if (combatHit.getDamage() > victim.getHp()) {
				combatHit.setDamage(victim.getHp());
			}
			if(aggressor.isPlayer() && combatHit.getDamage() >= 60) {
				Achievements.increase(((Player)aggressor), 29);
			}
			victim.inflictDamage(combatHit, false);
			MainCombat.checkAutoRetaliate(aggressor, victim);
			CombatEffects.executeCombatEffects(aggressor, victim, weapon, combatHit.getDamage());
		} else {
			World.getWorld().submit(new Tickable(1, true) {
				int count = final_hit_delay;
				@Override
				public void execute() {
					if (count-- > 0) {
						return;
					}
					this.stop();
					if (victim.getAttributes().isSet("stopActions")) {
						return;
					}
					if (combatHit.getDamage() > victim.getHp()) {
						combatHit.setDamage(victim.getHp());
					}
					if(aggressor.isPlayer() && combatHit.getDamage() >= 60) {
						Achievements.increase(((Player)aggressor), 29);
					}
					victim.inflictDamage(combatHit, false);
					MainCombat.checkAutoRetaliate(aggressor, victim);
					CombatEffects.executeCombatEffects(aggressor, victim, weapon, combatHit.getDamage());
				}
			});
		}
		
	}

	@Override
	public void handleDamage(final Entity aggressor, final Entity victim, int damage, int weapon, boolean special) {
		// TODO Auto-generated method stub
		if (weapon == 13652 && special) {//DRAGON CLAWS
			int damage2 = -1, damage3 = -1, damage4 = -1;
			Hit[] clawHits = new Hit[4];
			if (damage > victim.getHp()) {
				damage = victim.getHp();
			}
			if(damage > 0) {
				damage2 = (int) Math.floor(damage * 0.50);
				damage3 = (int) Math.floor(damage2 * 0.50);
				damage4 = damage3 + 1;
			} else if (damage <= 0) {
				damage2 = (getDamage(aggressor, victim, weapon, -1, -1, true));
				if (damage2 > 0) {
					damage3 = (int) Math.floor(damage2 * 0.50);
					damage4 = damage3;
				} else if (damage2 <= 0) {
					damage3 = (getDamage(aggressor, victim, weapon, -1, -1, true));
					damage4 = damage3;
					if (damage3 <= 0) {
						damage4 = (int)((getDamage(aggressor, victim, weapon, -1, -1, true)) * 1.50);
						if (damage4 <= 0) {
							damage = 0;
							damage2 = 1;
							damage3 = -1;
							damage4 = -1;
						}
					}
				}
			}
			int maxHit = HitFormula.calculateMeleeMaxHit(aggressor, victim, weapon, special, false);
			if(damage > maxHit * 0.9)
				clawHits[0] = new Hit(HitType.CRITICAL, damage, 0);
			else
				clawHits[0] = new Hit(damage, 0);
			clawHits[0].setOwner(aggressor);
			clawHits[0].setPriority(HitPriority.HIGH_PRIORITY);
			clawHits[0].setCombatType(CombatType.MELEE.ordinal());
			if (damage2 > maxHit * 0.9)
				clawHits[1] = new Hit(HitType.CRITICAL, damage2, 0);
			else
				clawHits[1] = new Hit(damage2, 0);
			clawHits[1].setOwner(aggressor);
			clawHits[1].setPriority(HitPriority.HIGH_PRIORITY);
			clawHits[1].setCombatType(CombatType.MELEE.ordinal());
			if (damage3 > maxHit * 0.9)
				clawHits[2] = new Hit(HitType.CRITICAL, damage3, 0);
			else
				clawHits[2] = new Hit(damage3, 0);
			clawHits[2].setOwner(aggressor);
			clawHits[2].setPriority(HitPriority.HIGH_PRIORITY);
			clawHits[2].setCombatType(CombatType.MELEE.ordinal());
			if(damage4 > maxHit * 0.9)
				clawHits[3] = new Hit(HitType.CRITICAL, damage4, 0);
			else
				clawHits[3] = new Hit(damage4, 0);
			clawHits[3].setOwner(aggressor);
			clawHits[3].setPriority(HitPriority.HIGH_PRIORITY);
			clawHits[3].setCombatType(0);
			
			clawHits[0].setIncomingHit(new IncomingHit(aggressor, getCombatType(), weapon, -1, clawHits[0].getDamage()));
			for (int i = 0; i < 4; i++) {
			    if(clawHits[i] != null) {
				CombatEffects.altarCombatDamage(aggressor, victim, clawHits[i]);
				CombatEffects.executeInitialEffects(aggressor, victim, clawHits[i].getDamage(), weapon);
			    } else 
				System.out.println("FATAL: Claw hit " + i + " was null");
			}
			int total = clawHits[0].getDamage() + clawHits[1].getDamage() + clawHits[2].getDamage() + clawHits[3].getDamage();
			MainCombat.addCombatExp(aggressor, victim, getCombatType(), total, -1);
			
			final Hit[] clawSpecs = clawHits;
			World.getWorld().submit(new Tickable(1) {
				@Override
				public void execute() {
					this.stop();
					if (victim.getAttributes().isSet("stopActions")) {
						return;
					}
					for (int i = 0; i < 4; i++) {
						if (!victim.getCombatState().isDead()) {
							int defAnim = CombatAnimations.getDefendAnim(victim);
							if (defAnim != -1 && defAnim != 65535) {
									victim.animate(Animation.create(defAnim, AnimationPriority.NONE, CombatAnimations.BLOCK_ANIM_LENGTH));
							}
						}
						if (clawSpecs[i].getDamage() > victim.getHp()) {
							clawSpecs[i].setDamage(victim.getHp());
						}
						if (clawSpecs[i].getDamage() != -1) {
							victim.inflictDamage(clawSpecs[i], false);
						}
						CombatEffects.executeCombatEffects(aggressor, victim, 13652, clawSpecs[i].getDamage());
						MainCombat.checkAutoRetaliate(aggressor, victim);
					}
				}
			});
			return;
		}
		handleDamage(aggressor, victim, damage, weapon, -1, -1, 0, null, false, false);
	}

	@Override
	public void handleDamage(Entity aggressor, Entity victim, int damage, int weapon,
			int extraTicks, Graphic graphic) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleDamage(Entity aggressor, Entity victim, int damage, int weapon,
			int ammo, int extraTicks, Graphic graphic, boolean secondHit) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleDamage(Entity aggressor, Entity victim, int damage, int weapon,
			int ammo, int extraTicks, Graphic graphic, boolean secondHit,
			boolean spec) {
		// TODO Auto-generated method stub
		handleDamage(aggressor, victim, damage, weapon, ammo, -1, extraTicks, graphic, secondHit, false);
	}

	@Override
	public void handleDamage(Entity aggressor, Entity victim, int damage, int weapon,
			int ammo, int extraTicks, boolean secondHit, Graphic graphic) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleDamage(Entity aggressor, Entity victim, int damage,
			int spell, Graphic graphic, boolean multiSpell) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public CombatType getCombatType() {
		return CombatType.MELEE;
	}
}
