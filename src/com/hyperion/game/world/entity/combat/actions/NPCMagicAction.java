package com.hyperion.game.world.entity.combat.actions;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.combat.data.magic.MagicSpells;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;
/**
 * @author Nando
 */
public class NPCMagicAction extends MagicSpells {

	public static void execute(final NPC entity, final Entity target) {
		final int spell = getNPCSpell(entity);
		if (spell == -1) {
			//System.out.println("stop");
			return;
		}
		entity.getCombatState().setCombatCycles(entity.getDefinition().getAttackSpeed());
		entity.animate(Animation.create(CombatAnimations.getAttackAnim(entity, -1), AnimationPriority.HIGH));
		entity.getCombatState().setCombatType(CombatType.MAGE);
		int startGfx = START_GFX[spell];
		if (startGfx != -1) {
			if (!entity.getAttributes().isSet("pestzombie")) {
				entity.playGraphic(startGfx, 0, getSpellStartHeight(entity, spell));
			}
		}
		sendProjectile(entity, target, spell);
		final int damage = getSpellDamage(entity, target, spell);
		int endGfx = END_GFX[spell];
		int endGfxHeight = getSpellEndGfxHeight(entity, spell);
		if (damage <= 0) {
			endGfx = 85;
			endGfxHeight = 100;
		}
		final int finalDamage = damage;
		final int finalEndGfx = endGfx;
		final int finalEndGfxHeight = endGfxHeight;
		World.getWorld().submit(new Tickable(3) {
			@Override
			public void execute() {
				this.stop();
				if (finalEndGfx != -1) {
					target.playGraphic(finalEndGfx, 0, finalEndGfxHeight);
				}
				if (target.getCombatState().isDead()) {
					return;
				}
				MainCombat.checkAutoRetaliate(entity, target);
				if (finalDamage > 0) {
					target.inflictDamage(new Hit(finalDamage, 2), false);
				}
				target.animate(Animation.create(CombatAnimations.getDefendAnim(target), AnimationPriority.NONE, CombatAnimations.BLOCK_ANIM_LENGTH));
				CombatEffects.executeCombatEffects(entity, target, -1, finalDamage);
			}
		});
	}
	
	private static void sendProjectile(NPC killer, Entity target, int spell) {
		if (PROJECTILE_GFX[spell] == -1) {
			return;
		}
		String spellName = getSpellName(killer, spell);
		int distance = (int) killer.getLocation().distance(target.getLocation());
		int delay = 50;
		int clientSpeed = 130;
		int startHeight = getSpellProjectileStartHeight(killer, spell);
		int endHeight = getSpellProjectileEndHeight(killer, spell);
		if (distance < 6) {
			clientSpeed = 90;
		} else if (distance < 9) {
			clientSpeed = 85;
		}
		//instant projectiles; wave spells
		if (spellName.endsWith("Wave")) {
			delay = 20;
		}
		if (spellName.equalsIgnoreCase("Teleport Block")) {
			delay = 60;
		}
		Location location = killer.getLocation();
		if (centerLocation(killer)) {
			location = killer.getCentreLocation();
		}
		Projectiles mageProjectile = Projectiles.create(location, target.getLocation(), target, PROJECTILE_GFX[spell], delay, clientSpeed, 50, startHeight, endHeight);
		killer.executeProjectile(mageProjectile);
	}
	
	private static int getSpellDamage(NPC npc, Entity target, int spell) {
		int damage = SPELL_MAX_HIT[spell];
		if (usesOwnMaxHit(npc)) {
			damage = npc.getDefinition().getMaxHit();
		}
		if (target instanceof Player) {
			if (((Player) target).getPrayers().isPrayerActive("protect from magic")) {
				damage = 0;
			}
			int def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM((Player) target));
			int attk = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
			if (def > attk) {
				damage = 0;
			}
		}
		return NumberUtils.random((int) damage);
	}
	
	private static int getNPCSpell(NPC npc) {
		int random = NumberUtils.random(3);
		switch (npc.getDefinition().getId()) {
		case 6250:
			return 55;
		case 750://Enraged zombies
			return 14;
		case 1643://Infernal mages
		case 1644:
		case 1645:
			return 19;
		case 174://Dark wizards
		case 172://dark wizard
			if (NumberUtils.random(2) == 0)
				return 4;
			else 
				return 3;
		case 912://Zammy
			return 22;
		case 913://Sara
			return 20;
		case 914://guthix
			return 21;
		case 2882://Prime
			return 24;
		case 3493://Agrith
			return 19;
		case 907://first kolodion
			return 2;
		case 908:
		case 909:
		case 910:
			if (random > 1) 
				return 20;
			else 
				return 21;
		case 911:
		case 997:
			return 22;
		case 2025:
			if (random == 0)
				return 23;
			else if (random == 1)
				return 24;
			else if (random == 2)
				return 26;
			else if (random == 3)
				return 29;
			break;
		}
		return -1;
	}
	
	private static boolean centerLocation(NPC npc) {
		switch (npc.getId()) {
		case 2882:
		case 3493:
			return true;
		}
		return false;
	}
	
	private static boolean usesOwnMaxHit(NPC npc) {
		switch (npc.getId()) {
		case 2882:
		case 750:
			return true;
		}
		return false;
	}
}
