package com.hyperion.game.world.entity.combat.actions;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;
/**
 * @author Nando
 */
public class NPCRangeAction {

	public static void executeAction(final NPC killer, final Entity victim) {
		killer.getCombatState().setCombatCycles(killer.getDefinition().getAttackSpeed());
		killer.animate(Animation.create(getAnimation(killer), AnimationPriority.HIGH));
		killer.getCombatState().setCombatType(CombatType.RANGE);
		sendProjectile(killer, victim);
		damage(killer, victim);
	}
	
	private static void damage(final NPC npc, final Entity victim) {
		int damage = NumberUtils.random((int) npc.getDefinition().getMaxHit());
		int attk = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
		int def = NumberUtils.random((int) HitFormula.calculateRangedDefencePVM(victim));
		if (victim instanceof Player) {
			if (((Player) victim).getPrayers().isPrayerActive("protect from ranged")) {
				damage = 0;
			}
		}
		if (def > attk) {
			damage = 0;
		}
		final int finalDamage = damage;
		World.getWorld().submit(new Tickable(npc, 2) {
			@Override
			public void execute() {
				this.stop();
				if (victim.getCombatState().isDead()) {
					return;
				}
				victim.inflictDamage(new Hit(finalDamage, 1), false);
				victim.animate(Animation.create(CombatAnimations.getDefendAnim(victim), AnimationPriority.NONE, CombatAnimations.BLOCK_ANIM_LENGTH));
				MainCombat.checkAutoRetaliate(npc, victim);
				CombatEffects.executeCombatEffects(npc, victim, -1, finalDamage);
			}
		});
	}
	
	private static void sendProjectile(NPC killer, Entity target) {
		Projectiles projectile = Projectiles.create(killer.getCentreLocation(), target.getLocation(), target, getRangeProjectileGfx(killer), getStartDelay(killer), getAirSpeed(killer, target), 50, 43, 36);
		projectile.setSpeedRange(killer, target, false);
		killer.executeProjectile(projectile);
	}
	
	private static int getAnimation(NPC npc) {
		return CombatAnimations.getAttackAnim(npc, -1);
	}
	
	private static int getRangeProjectileGfx(NPC npc) {
		switch (npc.getDefinition().getId()) {
		case 2028://karils
			return 27;
		case 2631://fight cave - lvl 90s
		case 2740:
		case 2739:
			return 443;
		case 6240:
		case 6241:
		case 6238:
		case 6230:
		case 6233:
			return 1190;
		case 2881:
			return 475;
		}
		return -1;
	}
	
	private static int getStartDelay(NPC npc) {
		switch (npc.getId()) {
		case 2631://fight cave - lvl 90s
		case 2740:
		case 2739:
			return 14;
		case 2881:
			return 42;
		}
		return 45;
	}
	
	private static int getAirSpeed(NPC npc, Entity target) {
		int airSpeed = 55;
		//int distance = (int) npc.getLocation().distance(target.getLocation());
		//airSpeed = 55 + (distance * 5);
		return airSpeed;
	}
}
