package com.hyperion.game.world.entity.combat.actions;

import com.hyperion.game.content.minigames.clanwars.ClanWarsData;
import com.hyperion.game.content.minigames.clanwars.session.WarSession;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.AbstractCombatAction;
import com.hyperion.game.world.entity.combat.CombatAction;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.Speeds;
import com.hyperion.game.world.entity.combat.data.range.RangeData;
import com.hyperion.game.world.entity.combat.data.range.RangeEffects;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.Hits.HitType;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;

/**
 * @author Nando
 */
public class RangeAction extends AbstractCombatAction {

	private static final RangeAction INSTANCE = new RangeAction();

	/**
	 * Gets the singleton instance.
	 * 
	 * @return The singleton instance.
	 */
	public static CombatAction getAction() {
		return INSTANCE;
	}

	@Override
	public void executeAttack(Entity aggressor, Entity victim, int weapon, int ammo) {
		super.executeAttack(aggressor, victim, weapon, ammo);
		if (aggressor.isNPC()) {
			NPCRangeAction.executeAction((NPC) aggressor, victim);
			return;
		}
		Player player = (Player) aggressor;
		int cape = player.getEquipment().getItemInSlot(1);
		ItemDefinition arrowDef = ItemDefinition.forId(ammo);
		ItemDefinition weaponDef = ItemDefinition.forId(weapon);
		String ammoName = "";
		String weaponName = "";
		if (weaponDef != null) {
			weaponName = weaponDef.getName();
		}
		if (arrowDef != null) {
			ammoName = arrowDef.getName();
		}
		if (!RangeData.containsAmmo(player, weapon, ammo)) {
			MainCombat.endCombat(player, 2);
			return;
		}
		if (!RangeData.containsCorrectArrows(player, weapon, ammo)) {
			MainCombat.endCombat(player, 2);
			final String toAdd = (ammoName.contains("rack") || ammoName.contains("arrow")) ? "s" : "";
			player.getPacketSender().sendMessage("You can't use " + ammoName + "" + toAdd + " with a " + weaponName + ".");
			return;
		}
		if (!aggressor.getCombatState().isIgnoringCycles()) {
			aggressor.getCombatState().setCombatCycles(Speeds.getCombatSpeeds(aggressor, weapon));
		}
		if (canPerformSpecial(aggressor, victim, weapon, ammo)) {
			return;
		}
		RangeEffects.applyAmmoSaving(player, cape);
		RangeEffects.executeAmmoRemoving(player, weapon);
		aggressor.animate(Animation.create(CombatAnimations.getAttackAnim(aggressor, weapon), AnimationPriority.HIGH, CombatAnimations.getAttackAnimLength(aggressor, weapon, false)));
		final int drawBack = RangeData.getDrawBack(player, weapon, ammo);
		if (drawBack != -1) {
			player.playGraphic(drawBack, 0, 90);
		}
		RangeEffects.displayRangeProjectile(player, victim, weapon, ammo);
		RangeEffects.displayStartGraphic(player, weapon, ammo);
		player.getCombatState().setLastRanged(System.currentTimeMillis());
		player.getCombatState().setLastCombatType(CombatType.RANGE);
		int endGfx = -1;
		boolean spec = false;
		if (weaponName.endsWith("crossbow")) {
			int specChance = NumberUtils.random(10);
			if (specChance == 3 || specChance == 5) {
				switch (ammo) {
				case 9242:// Ruby bolts e
					spec = true;
					player.getAttributes().set("rubyspec", true);
					break;
				case 9245:// Onyx e
					spec = true;
					player.getAttributes().set("boltheal", true);
					break;
				case 9241:// emerald
					spec = true;
					victim.getAttributes().set("boltpoison", true);
					break;
				case 9244:// d bolts
					if (victim.isPlayer()) {
						Player pTarget = (Player) victim;
						if (!pTarget.getEquipment().containsDragonShield()) {
							spec = true;
						}
					} else {
						spec = true;
					}
					break;
				case 9243:// diamonds
					spec = true;
					victim.getAttributes().set("diamondEffect", true);
					break;
				}
			}
		}
		if (spec) {// means we r using bolts
			endGfx = RangeData.getBoltSpecialGfx(ammo);
		}
		handleDamage(aggressor, victim, getDamage(aggressor, victim, weapon, ammo, -1, false), weapon, ammo, 0, Graphic.create(endGfx), false, false);
		if (RangeData.isDoubleFire(weapon)) {
			handleDamage(aggressor, victim, getDamage(aggressor, victim, weapon, ammo, -1, false), weapon, ammo, 1, Graphic.create(endGfx), true);
		}
	}

	@Override
	public boolean canAttack(Entity aggressor, Entity victim) {
		if (!super.canAttack(aggressor, victim)) {
			return false;
		}
		if (aggressor.isPlayer()) {
			Player player = (Player) aggressor;
			if (player.getDuelSession() != null) {
				if (player.getDuelSession().ruleEnabled(2)) {
					MainCombat.endCombat(player, 2);
					player.getPacketSender().sendMessage("Ranged combat has been disabled this duel.");
					return false;
				}
			}
			if(player.getAttributes().isSet("clansession")) {
				WarSession session = player.getAttributes().get("clansession");
				if(session != null && session.getRules()[ClanWarsData.Groups.RANGING.ordinal()] == ClanWarsData.Rules.RANGING_OFF) {
					player.getPacketSender().sendMessage("Ranged combat has been disabled in this war.");
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public boolean isInCombatDistance(Entity aggressor, Entity victim) {
		if (!super.isInCombatDistance(aggressor, victim)) {
			return false;
		}
		return true;
	}

	@Override
	public boolean canPerformSpecial(Entity aggressor, Entity victim, int weapon, int ammo) {
		if (aggressor.isPlayer()) {
			Player player = (Player) aggressor;
			return SpecialAttacks.canPerformSpecialAttack(player, victim, weapon, ammo);
		}
		return false;
	}

	@Override
	public int getDamage(Entity aggressor, Entity victim, int weapon, int ammo, int spell, boolean special) {
		//int damage = (RangeFormulas.getDamage(aggressor, victim, weapon, ammo, special));
		int damage = 0;
		if(aggressor.isPlayer() && victim.isPlayer()) {
			boolean missed = !HitFormula.willHit((Player) aggressor, (Player) victim, weapon, special, "ranged", -1);
			if (!missed) {
				damage = HitFormula.calculateRangeMaxHit(aggressor, (Player) victim, weapon, ammo, special, true);
			} else if (missed) {
				damage = 0;
			}
		} else {
			  double accuracy = NumberUtils.getGaussian(0.5, HitFormula.getRangedAccuracyPVM(aggressor, weapon, ammo, special));
		        double defence = NumberUtils.getGaussian(0.5, HitFormula.calculateRangedDefencePVM(victim));
		        if (accuracy > defence) {
		        	damage = HitFormula.calculateRangeMaxHit(aggressor, (NPC) victim, weapon, ammo, special, true);
		        }
		}
		
		if (special) {
			if (damage > HitFormula.calculateRangeMaxHit(aggressor, (NPC) victim, weapon, ammo, special, false)) {
				damage = HitFormula.calculateRangeMaxHit(aggressor, (NPC) victim, weapon, ammo, special, false);
			}
		}
		if (victim.isPlayer()) {
			if (((Player) victim).getPrayers().isPrayerActive("protect from ranged")) {
				damage *= 0.60;
			}
		}
		int extra = CombatEffects.getExtraDamage(aggressor, victim, CombatType.RANGE, weapon, damage);
		if (extra != -1) {
			damage += extra;
		}
		if (special) {
			switch(weapon) {
			case 1135:
			case 15701: // 0x Dark bow
			case 15702: // 0x Dark bow
			case 15703: // 0x Dark bow
			case 15704: // 0x Dark bow
				if (ammo == 11212) {
					if (damage < 8) {
						damage = 8;
					}
				} else {
					if (damage < 4) {
						damage = 4;
					}
				}
				break;
			}
		}
		return damage;
	}

	@Override
	public void handleDamage(final Entity aggressor, final Entity victim, int damage, final int weapon, final int ammo, int spell, int extraTicks, final Graphic graphic, final boolean secondHit, boolean multiSpell) {
		int hitDelay = 2;

		int maxHit = HitFormula.calculateRangeMaxHit(aggressor, victim, weapon, ammo, false, false);
		Hit fixedHit = null;
		if (damage > (maxHit * 0.9)) {
			fixedHit = new Hit(HitType.CRITICAL, damage, 1);
		} else {
			fixedHit = new Hit(damage, 1);
		}

		final Hit combatHit = fixedHit;

		combatHit.setIncomingHit(new IncomingHit(aggressor, getCombatType(), weapon, -1, combatHit.getDamage()));
		combatHit.setTarget(victim);
		combatHit.setOwner(aggressor);
		CombatEffects.altarCombatDamage(aggressor, victim, combatHit);
		if (combatHit.getDamage() > victim.getHp()) {
			combatHit.setDamage(victim.getHp());
		}
		CombatEffects.executeInitialEffects(aggressor, victim, combatHit.getDamage(), weapon);
		MainCombat.addCombatExp(aggressor, victim, getCombatType(), combatHit.getDamage(), -1);
		hitDelay += extraTicks;
		boolean delay = true;
		if (aggressor.isPlayer() && victim.isPlayer()) {
			Player p = (Player) aggressor;
			Player pV = (Player) victim;
			int pid = pV.getAttributes().getInt("random_pid");
			if (p.getAttributes().getInt("random_pid") < pid) {
				delay = false;
			}
		}
		if (delay) {
			World.getWorld().submit(new Tickable(hitDelay) {
				@Override
				public void execute() {
					this.stop();
					if (graphic != null) {
						victim.playGraphic(graphic.getId(), 0, graphic.getHeight());
					}
					if (victim.getAttributes().isSet("stopActions")) {
						return;
					}
					if (combatHit.getDamage() > victim.getHp()) {
						combatHit.setDamage(victim.getHp());
					}
					victim.inflictDamage(combatHit, false);
					if (!victim.getCombatState().isDead()) {
						int defAnim = CombatAnimations.getDefendAnim(victim);
						if (defAnim != -1 && defAnim != 65535) {
								victim.animate(Animation.create(defAnim, AnimationPriority.NONE, CombatAnimations.BLOCK_ANIM_LENGTH));
						}
						if (victim.isAutoRetaliating() && ((System.currentTimeMillis() - victim.getCombatState().getLastAttacked()) > 4000)) {
							victim.getCombatState().startAttack(aggressor);
						}
					}
					CombatEffects.executeCombatEffects(aggressor, victim, weapon, combatHit.getDamage());
					if (!secondHit) {
						RangeEffects.executeEffects(aggressor, victim, weapon, ammo, combatHit.getDamage());
					}
				}
			});
		} else {
			if (graphic != null) {
				victim.playGraphic(graphic.getId(), 0, graphic.getHeight());
			}
			if (victim.getAttributes().isSet("stopActions")) {
				return;
			}
			if (combatHit.getDamage() > victim.getHp()) {
				combatHit.setDamage(victim.getHp());
			}
			victim.inflictDamage(combatHit, false);
			if (!victim.getCombatState().isDead()) {
				int defAnim = CombatAnimations.getDefendAnim(victim);
				if (defAnim != -1 && defAnim != 65535) {
						victim.animate(Animation.create(defAnim, AnimationPriority.NONE, CombatAnimations.BLOCK_ANIM_LENGTH));
				}
				if (victim.isAutoRetaliating() && ((System.currentTimeMillis() - victim.getCombatState().getLastAttacked()) > 4000)) {
					victim.getCombatState().startAttack(aggressor);
				}
			}
			CombatEffects.executeCombatEffects(aggressor, victim, weapon, combatHit.getDamage());
			if (!secondHit) {
				RangeEffects.executeEffects(aggressor, victim, weapon, ammo, combatHit.getDamage());
			}
		}
		
	}

	@Override
	public CombatType getCombatType() {
		return CombatType.RANGE;
	}

	@Override
	public void handleDamage(Entity aggressor, Entity victim, int damage, int weapon, boolean special) {

	}

	@Override
	public void handleDamage(Entity aggressor, Entity victim, int damage, int weapon, int extraTicks, Graphic graphic) {

	}

	@Override
	public void handleDamage(Entity aggressor, Entity victim, int damage, int weapon, int ammo, int extraTicks, Graphic graphic, boolean secondHit) {
		handleDamage(aggressor, victim, damage, weapon, ammo, -1, extraTicks, graphic, secondHit, false);
	}

	@Override
	public void handleDamage(Entity aggressor, Entity victim, int damage, int weapon, int ammo, int extraTicks, Graphic graphic, boolean secondHit, boolean spec) {
		handleDamage(aggressor, victim, damage, weapon, ammo, -1, 0, graphic, secondHit, false);
	}

	@Override
	public void handleDamage(Entity aggressor, Entity victim, int damage, int weapon, int ammo, int extraTicks, boolean secondHit, Graphic graphic) {
		handleDamage(aggressor, victim, damage, weapon, ammo, -1, extraTicks, graphic, secondHit, false);
	}

	@Override
	public void handleDamage(Entity aggressor, Entity victim, int damage, int spell, Graphic graphic, boolean multiSpell) {

	}
}
