package com.hyperion.game.world.entity.combat.actions;

import com.hyperion.game.content.minigames.clanwars.ClanWarsData;
import com.hyperion.game.content.minigames.clanwars.session.WarSession;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatAction;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.combat.data.range.RangeData;
import com.hyperion.game.world.entity.combat.data.range.RangeEffects;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.utility.NumberUtils;
/**
 * @author Nando
 */
public class SpecialAttacks {

	public static boolean canPerformSpecialAttack(final Player player, final Entity target, final int weapon, final int ammo) {
		final int ammoAmount = player.getEquipment().getAmountInSlot(13);
		int currentPower = player.getSettings().getSpecialAmount();
		int neededPower = getRequiredAmount(weapon);
		if (!player.getSettings().isUsingSpecial()) {
			return false;
		}
		if (neededPower > currentPower) {
			player.getPacketSender().sendMessage("You don't have enough power left.");
			player.getSettings().setUsingSpecial(false);
			player.getSettings().refreshBar();
			return false;
		}
		if (player.getDuelSession() != null) {
			if (player.getDuelSession().ruleEnabled(10)) {
				player.getPacketSender().sendMessage("Special attacks have been disabled in this duel.");
				player.getSettings().setUsingSpecial(false);
				player.getSettings().refreshBar();
				return false;
			}
		}
		if(player.getAttributes().isSet("clansession")) {
			WarSession session = player.getAttributes().get("clansession");
			if(session != null && session.getRules()[ClanWarsData.Groups.SPECIALS.ordinal()] == ClanWarsData.Rules.SPECIALS_OFF) {
				player.getPacketSender().sendMessage("Special attacks have been disabled in this war.");
				return false;
			}
		}
		int endGfx = -1;
		int extraTicks = 0;
		int extraHits = 0;
		int ammoRemovalAmount = 0;
		boolean usingMelee = true;
		boolean weaponDamages = true;
		switch (weapon) {
		case 7158:
			player.animate(Animation.create(3157, AnimationPriority.HIGH));
			break;
		case 1249://Dragon spear
		case 11716://zamorakian spear
			weaponDamages = false;
			player.animate(Animation.create(1064, AnimationPriority.HIGH));
			player.playGraphic(253, 0, 100);
			target.playGraphic(254, 0, 100);
			target.getAttributes().set("stopActions", true);
			if (target instanceof Player) {
				((Player) target).getPacketSender().sendMessage("You have been stunned!");
			}
			SpecialEffects.handleDragonSpear(player, target);
			World.getWorld().submit(new Tickable(7) {
				@Override
				public void execute() {
					target.getAttributes().remove("stopActions");
					this.stop();
				}
			});
			break;
		case 4151://Abyssal whip
		case 15441:
		case 15442:
		case 15443:
		case 15444:
		case 18610://Lava whip
		case 18611://Frozen whip
		case 18606:
			player.animate(Animation.create(1658, AnimationPriority.HIGH));
			target.playGraphic(341, 0, 100);
			if(weapon == 18606) {
				if (target.getCombatState().isFreezable()) {
					int freezeTimer = 8;
					int finalTimer = freezeTimer;
					if(target.isPlayer()) {
						if (((Player) target).getPrayers().isPrayerActive("Protect from Magic")) {
							finalTimer = freezeTimer / 2;
						}
					}
					MainCombat.endCombat(target, 2);
					target.getWalkingQueue().reset();
					target.getCombatState().setFreezable(false);
					target.getCombatState().setFrozen(true);
					if (target.isPlayer()) {
						((Player) target).getPacketSender().sendMessage("You have been frozen!");
					}
					World.getWorld().submit(new Tickable(finalTimer + 3) {
						@Override
						public void execute() {
							this.stop();
							target.getCombatState().setFrozen(false);
							World.getWorld().submit(new Tickable(7) {
								@Override
								public void execute() {
									this.stop();
									target.getCombatState().setFreezable(true);
								}
							});
						}
					});
				}
			}
			break;
		case 13652://Dragon claws
			weaponDamages = false;
			player.playGraphic(1171);
			player.animate(Animation.create(7527, AnimationPriority.HIGH));
			CombatAction clawAction = MeleeAction.getAction();
			clawAction.handleDamage(player, target, clawAction.getDamage(player, target, weapon, ammo, -1, true), 13652, true);
			break;
		case 10887://Barrelchest anchor
			player.playGraphic(1027);
			player.animate(Animation.create(5870, AnimationPriority.HIGH, CombatAnimations.getAttackAnimLength(player, weapon, true)));
			break;
		case 4587://Dragon scimitar
			player.playGraphic(347, 0, 100);
			player.animate(Animation.create(1872, AnimationPriority.HIGH));
			if (target.isPlayer()) {
				boolean miss = false;
				final Player pTarget = (Player) target;
				int attackersSlash = NumberUtils.random(player.getBonuses().getBonus(1));
				int defendersSlash = NumberUtils.random(pTarget.getBonuses().getBonus(9));
				if (defendersSlash > attackersSlash) {
					miss = true;
				}
				if (miss) {
					break;
				}
				pTarget.getAttributes().set("disableheadiconprayers", true);
				pTarget.getPrayers().turnRegularPrayersOff(new int[] {16, 17, 18, 19});
				pTarget.getPacketSender().sendMessage("You've been injured and can't use protection prayers!");
				World.getWorld().submit(new Tickable(pTarget, 9) {
					@Override
					public void execute() {
						pTarget.getAttributes().remove("disableheadiconprayers");
						this.stop();
					}
				});
			}
			break;
		case 1434://Dragon mace
			player.playGraphic(251, 0, 75);
			player.animate(Animation.create(1060, AnimationPriority.HIGH));
			break;
		case 11730://Saradomin sword
			extraHits = 1;
			target.playGraphic(1207, 0, 100);
			player.animate(Animation.create(7072, AnimationPriority.HIGH, CombatAnimations.getAttackAnimLength(player, weapon, true)));
			break;

		case 11700: //Zamorak Godsword
			player.playGraphic(1221);
			target.playGraphic(2104, 0 , 0);
			player.animate(Animation.create(7070, AnimationPriority.HIGH, CombatAnimations.getAttackAnimLength(player, weapon, true)));
			if (target.getCombatState().isFreezable()) {
				int freezeTimer = 25;
				int finalTimer = freezeTimer;
				if(target.isPlayer()) {
					if (((Player) target).getPrayers().isPrayerActive("Protect from Magic")) {
						finalTimer = freezeTimer / 2;
					}
				}
				MainCombat.endCombat(target, 2);
				target.getWalkingQueue().reset();
				target.getCombatState().setFreezable(false);
				target.getCombatState().setFrozen(true);
				if (target.isPlayer()) {
					((Player) target).getPacketSender().sendMessage("You have been frozen!");
				}
				World.getWorld().submit(new Tickable(finalTimer + 3) {
					@Override
					public void execute() {
						this.stop();
						target.getCombatState().setFrozen(false);
						World.getWorld().submit(new Tickable(7) {
							@Override
							public void execute() {
								this.stop();
								target.getCombatState().setFreezable(true);
							}
						});
					}
				});
			}



			break;

		case 11698: //saradomin godsword
			player.playGraphic(2109 , 0, 50);
			player.animate(Animation.create(12019, AnimationPriority.HIGH, CombatAnimations.getAttackAnimLength(player, weapon, true)));
			player.getAttributes().set("sgsheal", true);

			break;
		case 11696://Bandos godsword.
			player.playGraphic(1223, 0, 100);
			player.animate(Animation.create(7073, AnimationPriority.HIGH, CombatAnimations.getAttackAnimLength(player, weapon, true)));
			break;
		case 11694://Armadyl godsword.
			player.playGraphic(1222, 0, 100);
			player.animate(Animation.create(7074, AnimationPriority.HIGH, CombatAnimations.getAttackAnimLength(player, weapon, true)));
			break;
		case 1305://Dragon longsword.
			player.animate(Animation.create(1058, AnimationPriority.HIGH));
			player.playGraphic(248, 0, 100);
			break;
		case 1215://Dragon daggers.
		case 1231: 
		case 5680: 
		case 5698:
			extraHits = 1;
			player.animate(Animation.create(1062, AnimationPriority.HIGH));
			player.playGraphic(252, 0, 100);
			break;
		case 4153://Granite maul
		case 18617: //Clamped granite maul
			player.animate(Animation.create(1667, AnimationPriority.HIGH));
			player.playGraphic(340, 0, 100);
			break;
		case 13902://Statius' warhammer
		case 13904://degraded warhammer
			player.animate(Animation.create(10505, AnimationPriority.HIGH));
			player.playGraphic(1840);
			break;
		case 13899://Vesta's longsword
		case 13901://vls deg
			player.animate(Animation.create(10502, AnimationPriority.HIGH));
			break;
		case 3204://d hally
			//if(target.getSize() > 1)
			//     doubleHit = true;
			Location playerLoc = player.getLocation();
			Location targetLoc = target.getLocation();
			int gfx;
			if(targetLoc.getY() > playerLoc.getY())
				gfx = 283;
			else if(targetLoc.getY() < playerLoc.getY())
				gfx = 282;
			else if(targetLoc.getX() < playerLoc.getX())
				gfx = 285;
			else
				gfx = 284;
			// SpecialEffects.handleAdjacentTargetsSpecial(player, target, wep, SpecialEffects.LocationsType.ONE_SQUARE);
			player.animate(Animation.create(1203, AnimationPriority.HIGH));
			player.getPacketSender().sendStillGraphics(targetLoc, Graphic.create(gfx, 0, 100), 0);
			// player.getActionSender().sendStillGraphic(targetLoc, gfx, 100, 0);
			break;
		case 13883://Morrigans throwing axe
			usingMelee = false;
			player.animate(Animation.create(10504, AnimationPriority.HIGH));
			player.playGraphic(1838);
			ammoRemovalAmount = 1;
			final Projectiles axeProj = Projectiles.create(player.getLocation(), target.getLocation(), target, 1839, 45, 55, 51, 15, 20);
			player.executeProjectile(axeProj);
			break;
		case 13879://Morrigans javelin
			ammoRemovalAmount = 1;
			usingMelee = false;
			player.animate(Animation.create(10501, AnimationPriority.HIGH));
			player.playGraphic(1836, 0, 0);
			final Projectiles javProj = Projectiles.create(player.getLocation(), target.getLocation(), target, 1837, 37, 50, 50, 40, 36);
			player.executeProjectile(javProj);
			break;
		case 861://Magic shortbow
			usingMelee = false;
			weaponDamages = false;
			if (ammoAmount < 2) {
				player.getPacketSender().sendMessage("You don't have enough arrows.");
				player.getSettings().setUsingSpecial(false);
				player.getSettings().refreshBar();
				return false;
			}
			ammoRemovalAmount = 2;
			RangeEffects.applyAmmoSaving(player, player.getEquipment().getItemInSlot(1));
			for (int i = 0; i < ammoRemovalAmount; i++) {
				RangeEffects.executeAmmoRemoving(player, weapon);
			}
			extraHits = 1;
			player.animate(Animation.create(1074, AnimationPriority.HIGH));
			player.playGraphic(256, 0, 90);
			final Projectiles projectile = Projectiles.create(player.getLocation(), target.getLocation(), target, 249, 20, 30, 50, 40, 34);
			player.executeProjectile(projectile);
			//2nd proj
			World.getWorld().submit(new Tickable(1) {
				@Override
				public void execute() {
					this.stop();
					player.playGraphic(256, 0, 90);
					player.executeProjectile(projectile);
				}
			});
			CombatAction msbAction = RangeAction.getAction();
			msbAction.handleDamage(player, target, msbAction.getDamage(player, target, weapon, ammo, -1, true), weapon, ammo, 0, false, Graphic.create(endGfx));
			msbAction.handleDamage(player, target, msbAction.getDamage(player, target, weapon, ammo, -1, true), weapon, ammo, 0, true, Graphic.create(endGfx));
			break;
		case 18607:
			usingMelee = false;
			weaponDamages = false;
			if (ammoAmount < 1) {
				player.getPacketSender().sendMessage("You don't have enough arrows.");
				player.getSettings().setUsingSpecial(false);
				player.getSettings().refreshBar();
				return false;
			}
			ammoRemovalAmount = 1;
			RangeEffects.applyAmmoSaving(player, player.getEquipment().getItemInSlot(1));
			RangeEffects.executeAmmoRemoving(player, weapon);
			extraHits = 0;
			player.animate(Animation.create(4230, AnimationPriority.HIGH));
			player.playGraphic(697, 0, 90);
			//projectiles

			final Projectiles acbProjectile = Projectiles.create(player.getLocation(), target.getLocation(), target, 698/*696 red projectile*/, 45, 55, 50, 43, 36, 5, 86);
			player.executeProjectile(acbProjectile);

			CombatAction acbAction = RangeAction.getAction();
			acbAction.handleDamage(player, target, acbAction.getDamage(player, target, weapon, ammo, -1, true), weapon, ammo, 0, false, Graphic.create(endGfx));
			break;
		case 15701: // 0x Dark bow
		case 15702: // 0x Dark bow
		case 15703: // 0x Dark bow
		case 15704: // 0x Dark bow
		case 11235://Dark bow
			usingMelee = false;
			weaponDamages = false;
			player.getCombatState().setLastCombatType(CombatType.RANGE);
			if (ammoAmount < 2) {
				player.getPacketSender().sendMessage("You don't have enough arrows.");
				player.getSettings().setUsingSpecial(false);
				player.getSettings().refreshBar();
				return false;
			}
			ammoRemovalAmount = 1;
			RangeEffects.applyAmmoSaving(player, player.getEquipment().getItemInSlot(1));
			for (int i = 0; i < ammoRemovalAmount; i++) {
				RangeEffects.executeAmmoRemoving(player, weapon);
			}
			extraHits = 1;
			final int drawBack = RangeData.getDrawBack(player, weapon, ammo);
			if (drawBack != -1) {
				player.playGraphic(drawBack, 0, 90);
			}
			endGfx = 1100;
			player.animate(Animation.create(426, AnimationPriority.HIGH));
			final int distance = player.getLocation().distanceToPoint(target.getLocation());
			//projectiles
			int airSpeed = 55;
			if (distance <= 8) {
				airSpeed = 65;
			} else if (distance > 8) {
				airSpeed = 75;
			}
			airSpeed += 30;
			final Projectiles dbProjectile = Projectiles.create(player.getLocation(), target.getLocation(), target, ammo == 11212 ? 1099 : 1102, 45, airSpeed - 10, 50, 46, 36, 15 - 6, 64);
			player.executeProjectile(dbProjectile);
			//2nd projectile
			Projectiles dbProjectile2 = Projectiles.create(player.getLocation(), target.getLocation(), target, ammo == 11212 ? 1099 : 1102, 45 + 5, airSpeed + 10, 50, 46, 36, 15, 86);
			player.executeProjectile(dbProjectile2);
			//hit delays
			int dbowHitDelay1 = 0;
			int dbowHitDelay2 = 1;
			if (distance > 3) {
				dbowHitDelay1 = 1;
				dbowHitDelay2 += 1;
			}
			CombatAction darkBowAction = RangeAction.getAction();
			darkBowAction.handleDamage(player, target, darkBowAction.getDamage(player, target, weapon, ammo, -1, true), weapon, ammo, dbowHitDelay1, false, Graphic.create(endGfx, 0, 100));
			darkBowAction.handleDamage(player, target, darkBowAction.getDamage(player, target, weapon, ammo, -1, true), weapon, ammo, dbowHitDelay2, true, null);
			break;
		}
		player.getSettings().setUsingSpecial(false);
		player.getSettings().deductSpecialAmount(neededPower);
		player.getSettings().refreshBar();
		if (!weaponDamages) {
			return true;
		}
		if (usingMelee) {
			CombatAction meleeAction = MeleeAction.getAction();
			int damage = meleeAction.getDamage(player, target, weapon, ammo, -1, true);

			if(player.getAttributes().isSet("sgsheal")) {
				player.getAttributes().remove("sgsheal");
				int percentage = (int) (damage / 2);
				player.heal(percentage);
				double prayerPoints = player.getSettings().getPrayerPoints();
				int base = player.getSkills().getLevelForXp(Skills.PRAYER);
				player.getSettings().setPrayerPoints(prayerPoints + (percentage /2) > base ? base : prayerPoints + (percentage /2));
				player.getPacketSender().sendSkillLevel(Skills.PRAYER);
				player.getPacketSender().sendMessage("Your Hitpoints and Prayer are restored by the power of Saradomin.");
			}

			meleeAction.handleDamage(player, target, damage, weapon, ammo, extraTicks, null, false, true);
			for (int i = 0; i < extraHits; i++) {
				meleeAction.handleDamage(player, target, meleeAction.getDamage(player, target, weapon, ammo, -1, true), weapon, ammo, extraTicks, null, true, true);

			}
		} else if (!usingMelee) {
			RangeEffects.applyAmmoSaving(player, player.getEquipment().getItemInSlot(1));
			for (int i = 0; i < ammoRemovalAmount; i++) {
				RangeEffects.executeAmmoRemoving(player, weapon);
			}
			CombatAction rangeAction = RangeAction.getAction();
			rangeAction.handleDamage(player, target, rangeAction.getDamage(player, target, weapon, ammo, -1, true), weapon, ammo, 0, false, Graphic.create(endGfx));
		}
		return true;
	}

	public static int getRequiredAmount(int weapon) {
		int[] weapons = {13901, 13904, 11716, 4151, 1215, 1231, 5680, 5698, 1305, 11694, 11696, 11698, 11700, 11730, 1434, 1377, 3204, 4587, 13652, 1249,
				6739, 7158, 8880, 861, 859, 10284, 805, 6724, 11235, 3101, 4153, 18617, 10887, 11061, 13902, 13899, 13905, 13883, 13879,
				15441, 15442, 15443, 15444, 15701, 15702, 15703, 15704, 18610, 18611, 18607, 18600, 15486, 18606
		};
		int[] amount = {
				25, //vls deg
				35, // Statius' warhammer deg
				25, //zammy spear
				50, // Abyssal whip
				25, // Dragon dagger
				25, // Dragon dagger
				25, // Dragon dagger
				25, // Dragon dagger
				25, // Dragon longsword
				50, // Armadyl godsword
				100, // Bandos godsword
				50, // Saradomin godsword
				60, // Zamorak godsword
				100, // Saradomin sword
				25, // Dragon mace
				100, // Dragon battleaxe
				30, // Dragon halberd
				55, // Dragon scimitar
				50, // Dragon claws
				25, // Dragon spear
				100, // Dragon axe
				55, // Dragon 2h sword
				90, // Dorgeshuun crossbow
				55, // Magic shortbow
				40, // Magic longbow
				40, // Magic composite bow
				10, // Rune thrownaxe
				100, // Seercull
				65, // Dark bow
				30, // Rune claws
				50, // Granite maul
				50, // Clamped granite maul
				50, // Barrelchest anchor
				100, // Ancient mace
				35, // Statius' warhammer
				25, // Vesta's longsword
				50, // Vesta's spear
				50, // Morrigans throwing axe
				50, // Morrigans javelin
				50, // Yellow whip
				50, // Blue whip
				50,	// White whip
				50,	// Green whip
				65, // Dark bow
				65, // Dark bow
				65, // Dark bow
				65, // Dark bow	
				50, //Lava whip
				50, //Frozen whip
				40, //Armadyl crossbow
				100, //Staff of the dead
				100, //Staff of light
				50, //Tentacle
		};
		for (int i = 0; i < weapons.length; i++) {
			if (weapon == weapons[i]) {
				return amount[i];
			}
		}
		return 0;
	}
}
