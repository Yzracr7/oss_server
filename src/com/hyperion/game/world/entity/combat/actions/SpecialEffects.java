package com.hyperion.game.world.entity.combat.actions;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.DumbPathFinder;
import com.hyperion.game.world.pathfinders.SizedPathFinder;

public class SpecialEffects {
	
	/**
	 * Dragon spear moving
	 * @param killer
	 * @param target
	 */
	public static void handleDragonSpear(Player killer, Entity target) {
		int dir = -1;
		if (!DumbPathFinder.blockedNorth(target.getLocation(), target)) {
			dir = 0;
		} else if (!DumbPathFinder.blockedSouth(target.getLocation(), target)) {
			dir = 4;
		} else if (!DumbPathFinder.blockedEast(target.getLocation(), target)) {
			dir = 8;
		} else if (!DumbPathFinder.blockedWest(target.getLocation(), target)) {
			dir = 12;
		}
		boolean foundPath = false;
		int targX = target.getLocation().getX();
		int targY = target.getLocation().getY();
		if (killer.getLocation().getNorth().equals(target.getLocation())) {
			if (!DumbPathFinder.blockedNorth(target.getLocation(), target)) {
				foundPath = true;
				targY += 1;
			}
		} else if (killer.getLocation().getSouth().equals(target.getLocation())) {
			if (!DumbPathFinder.blockedSouth(target.getLocation(), target)) {
				foundPath = true;
				targY -= 1;
			}
		} else if (killer.getLocation().getWest().equals(target.getLocation())) {
			if (!DumbPathFinder.blockedWest(target.getLocation(), target)) {
				foundPath = true;
				targX += 1;
			}
		} else if (killer.getLocation().getEast().equals(target.getLocation())) {
			if (!DumbPathFinder.blockedEast(target.getLocation(), target)) {
				foundPath = true;
				targX -= 1;
			}
		}
		if (!foundPath) {
			if (dir == 0) {
				targY += 1;
			} else if (dir == 4) {
				targY -= 1;
			} else if (dir == 8) {
				targX -= 1;
			} else if (dir == 12) {
				targX += 1;
			}
		}
		MainCombat.endCombat(target, 1);
		target.execute_path(new SizedPathFinder(), targX, targY);
	}

	public static void handleSpecialEffects(Player player, final Entity target, final int weapon, final int ammo, final int damage) {
		if (weapon == 13879 && damage > 0) {// Morrigans javelin
			final int fiveDamages = damage / 5;
			final int totalAmount = damage;
			final int damageLeft = totalAmount - 5 * fiveDamages;
			World.getWorld().submit(new Tickable(2) {
				int count = fiveDamages;
				@Override
				public void execute() {
					if (target.getAttributes().isSet("stopActions")) {
						return;
					}
					if (target.getCombatState().isDead()) {
						this.stop();
						return;
					}
					if (count <= 0) {
						if (damageLeft > 0) {
							target.inflictDamage(new Hit(damageLeft, 1), false);
						}
						this.stop();
						return;
					}
					target.inflictDamage(new Hit(5, 1), false);
					count--;
				}
			});
		}
	}
}

