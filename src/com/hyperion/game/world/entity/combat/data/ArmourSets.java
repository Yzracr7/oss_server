package com.hyperion.game.world.entity.combat.data;

import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.item.ItemDefinition.DegradableType;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
/**
 * @author Nando
 */
public class ArmourSets {
	
	private enum DegradableArmourSet {
		
		//helm, plate, legs, weapon
		DHAROKS_SET(new DegradableType[] {DegradableType.dharoks_helmet, DegradableType.dharoks_plate, DegradableType.dharoks_legs, DegradableType.dharoks_axe}),
		VERACS_SET(new DegradableType[] {DegradableType.veracs_helm, DegradableType.veracs_plate, DegradableType.veracs_skirt, DegradableType.veracs_flail}),
		GUTHANS_SET(new DegradableType[] {DegradableType.guthans_helm, DegradableType.guthans_plate, DegradableType.guthans_legs, DegradableType.guthans_spear}),
		KARILS_SET(new DegradableType[] {DegradableType.karils_coif, DegradableType.karils_top, DegradableType.karils_bottom, DegradableType.karils_bow}),
		AHRIMS_SET(new DegradableType[] {DegradableType.ahrims_hood, DegradableType.ahrims_top, DegradableType.ahrims_bottom, DegradableType.ahrims_staff}),
		TORAGS_SET(new DegradableType[] {DegradableType.torags_helm, DegradableType.torags_plate, DegradableType.torags_legs, DegradableType.torags_hammers}),
		
		;
		
		private DegradableType[] degradable_types = new DegradableType[4];
		
		private DegradableArmourSet(DegradableType[] degradable_types) {
			this.degradable_types = degradable_types;
		}
		
		public DegradableType[] getTypes() {
			return degradable_types;
		}
	}
	
	public static boolean wearingDegradableArmourSet(Player player, DegradableArmourSet armour_type) {
		int helmet = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.HELMET.ordinal());
		int weapon = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.WEAPON.ordinal());
		int plate = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.PLATE.ordinal());
		int legs = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.LEGS.ordinal());
		ItemDefinition helmet_def = ItemDefinition.forId(helmet);
		ItemDefinition weapon_def = ItemDefinition.forId(weapon);
		ItemDefinition plate_def = ItemDefinition.forId(plate);
		ItemDefinition legs_def = ItemDefinition.forId(legs);
		if (helmet_def != null && plate_def != null && legs_def != null) {
			DegradableType helmet_type_req = armour_type.getTypes()[0];
			DegradableType plate_type_req = armour_type.getTypes()[1];
			DegradableType legs_type_req = armour_type.getTypes()[2];
			DegradableType weapon_type_req = armour_type.getTypes()[3];
			//
			DegradableType helmet_type = helmet_def.getDegradableType();
			DegradableType plate_type = plate_def.getDegradableType();
			DegradableType legs_type = legs_def.getDegradableType();
			DegradableType weapon_type = weapon_def.getDegradableType();
			if (helmet_type != null && plate_type != null && legs_type != null) {
				if (weapon_type != null) {
					if (weapon_type_req != null) {
						return helmet_type.equals(helmet_type_req) && plate_type.equals(plate_type_req) && legs_type.equals(legs_type_req) && weapon_type.equals(weapon_type_req);
					}
				}
				return helmet_type.equals(helmet_type_req) && plate_type.equals(plate_type_req) && legs_type.equals(legs_type_req);
			}
		}
		return false;
	}
	
	public static boolean wearingFullGuthan(Player player) {
		int helm = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.HELMET.ordinal());
		int plate = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.PLATE.ordinal());
		int legs = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.LEGS.ordinal());
		int wep = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.WEAPON.ordinal());
		return helm == 4724 && wep == 4726 && plate == 4728 && legs == 4730;
		//return wearingDegradableArmourSet(player, DegradableArmourSet.GUTHANS_SET);
	}

	public static boolean wearingFullTorag(Player player) {
		int helm = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.HELMET.ordinal());
		int plate = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.PLATE.ordinal());
		int legs = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.LEGS.ordinal());
		int wep = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.WEAPON.ordinal());
		return helm == 4745 && wep == 4747 && plate == 4749 && legs == 4751;
		//return wearingDegradableArmourSet(player, DegradableArmourSet.TORAGS_SET);
	}

	public static boolean wearingFullKaril(Player player) {
		final int helm = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.HELMET.ordinal());
		final int body = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.PLATE.ordinal());
		int legs = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.LEGS.ordinal());
		int wep = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.WEAPON.ordinal());
		return helm == 4732 && wep == 4734 && body == 4736 && legs == 4738;
		//return wearingDegradableArmourSet(player, DegradableArmourSet.KARILS_SET);
	}

	public static boolean wearingFullAhrim(Player player) {
		final int helm = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.HELMET.ordinal());
		final int body = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.PLATE.ordinal());
		final int legs = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.LEGS.ordinal());
		final int wep = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.WEAPON.ordinal());
		return helm == 4708 && wep == 4710 && body == 4712 && legs == 4714;
		//return wearingDegradableArmourSet(player, DegradableArmourSet.AHRIMS_SET);
	}
	
	public static boolean wearingObbyEffect(Player player) {
		final int wep = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.WEAPON.ordinal());
		final int amulet = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.AMULET.ordinal());
		return wep == 6528 && amulet == 11128;
	}
	
	public static boolean wearingFullDharok(Player player) {
		final int helm = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.HELMET.ordinal());
		final int plate = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.PLATE.ordinal());
		final int legs = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.LEGS.ordinal());
		final int wep = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.WEAPON.ordinal());
		return helm == 4716 && plate == 4720 && legs == 4722 && wep == 4718;
		//return wearingDegradableArmourSet(player, DegradableArmourSet.DHAROKS_SET);
	}
	
	public static boolean wearingFullVerac(Player player) {
		final int helm = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.HELMET.ordinal());
		final int plate = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.PLATE.ordinal());
		final int legs = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.LEGS.ordinal());
		final int wep = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.WEAPON.ordinal());
		return helm == 4753 && plate == 4757 && legs == 4759 && wep == 4755;
		//return wearingDegradableArmourSet(player, DegradableArmourSet.VERACS_SET);
	}
	
	public static boolean wearingFullVoidMelee(Player player) {
		final int helm = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.HELMET.ordinal());
		final int plate = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.PLATE.ordinal());
		final int legs = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.LEGS.ordinal());
		return helm == 11665 && plate == 8839 && legs == 8840;
	}	
	
	public static boolean wearingFullVoidRange(Player player) {
		final int helm = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.HELMET.ordinal());
		final int plate = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.PLATE.ordinal());
		final int legs = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.LEGS.ordinal());
		final int gloves = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.GLOVES.ordinal());
		return helm == 11664 && plate == 8839 && legs == 8840 && gloves == 8842;
	}
	
	public static boolean wearingFullVoidMagic(Player player) {
		final int helm = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.HELMET.ordinal());
		final int plate = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.PLATE.ordinal());
		final int legs = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.LEGS.ordinal());
		final int gloves = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.GLOVES.ordinal());
		return helm == 11663 && plate == 8839 && legs == 8840 && gloves == 8842;
	}
	
	public static boolean wearingAvas(Player player) {
		int cape = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.CAPE.ordinal());
		return cape == 10499 || cape == 10498;
	}
}
