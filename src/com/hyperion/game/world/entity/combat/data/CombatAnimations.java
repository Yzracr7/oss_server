package com.hyperion.game.world.entity.combat.data;

import com.hyperion.game.Constants;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.AttackVars;
import com.hyperion.game.world.entity.player.Player;

public class CombatAnimations {

    public static int getAttackAnim(Entity c, int weapon) {
        if (c.isNPC()) {
            return ((NPC) c).getDefinition().getAttackAnimation();
        }
        AttackVars.CombatSkill style = (((Player) c).getSettings().getAttackVars().getSkill());
        if (Constants.DEBUG_MODE) {
            System.out.println("Attack type = " + ((Player) c).getSettings().getAttackVars().getSkill());
        }
        if (weapon == 20779) { // Hunting Knife
            return 7328;
        }
        if (weapon == -1) {
            switch (style) {
                case AGGRESSIVE:
                    return 423;
                case DEFENSIVE:
                    return 451;
            }
            return 422;
        }
        ItemDefinition definition = ItemDefinition.DEFINITIONS[weapon];
        if (weapon <= 0) {
            return 422;
        }

        //2H
        if (definition.getName().contains(" 2h")) {
            switch (style) {
                case AGGRESSIVE:
                    return 414;
                default:
                    return 407;
            }
        }

        //Longsword && swords
        if (definition.getName().contains("longsword") || definition.getName().contains(" sword")) {
            switch (style) {
                case AGGRESSIVE:
                    return 401;
                case ACCURATE:
                    return 412;
                case DEFENSIVE:
                    return 451;
            }
        }
        if (definition.getName().contains("bulwark")) {
            return 7511;
        }
        if (definition.getName().contains("lder maul")) {
            return 7516;
        }
        if (definition.getName().contains("zamorakian")) {
            return 2080;
        }
        if (definition.getName().contains("ballista")) {
            return 7218;
        }
        if (definition.getName().contains("Toxic blowpipe")) {
            return 5061;
        }
        if (definition.getName().contains("warhammer")) {
            return 401;
        }
        if (definition.getName().contains("dart")) {
            return 6600;
        }
        if (definition.getName().contains("mace")) {
            return 401;
        }
        if (definition.getName().contains("knife") || definition.getName().contains("javelin") || definition.getName().contains("thrownaxe")) {
            return 806;
        }
        if (definition.getName().contains("cross") && !definition.getName().contains("karil") || definition.getName().contains("c'bow") && !definition.getName().contains("karil")) {
            return 4230;
        }
        if (definition.getName().contains("halberd")) {
            return 440;
        }
        if (definition.getName().startsWith("dragon dagger")) {
            return 402;
        }
        if (definition.getName().contains("byssal dagger")) {
            return 3295;
        }
        if (definition.getName().contains("dagger")) {
            return 412;
        }
        if (definition.getName().contains("2h sword") || definition.getName().contains("godsword") || definition.getName().contains("aradomin sword") || definition.getName().contains("blessed sword") || definition.getName().contains("large spade")) {
            return 7045;
        }
        if (definition.getName().contains("greataxe")) {
            return (((Player) c).getSettings().getAttackVars().getStyle() != AttackVars.CombatStyle.CRUSH ? 2067 : 2066);
        }
        if (definition.getName().contains("sword") && !definition.getName().contains("training")) {
            return 451;
        }
        if (definition.getName().contains("karil")) {
            return 2075;
        }
        if (definition.getName().contains("bow") && !definition.getName().contains("'bow") && !definition.getName().contains("karil")) {
            return 426;
        }
        if (definition.getName().contains("'bow") && !definition.getName().contains("karil")) {
            return 4230;
        }
        if (definition.getName().contains("hasta") || definition.getName().contains("spear")) {
            return (((Player) c).getSettings().getAttackVars().getStyle() != AttackVars.CombatStyle.CRUSH ? 400 : (((Player) c).getSettings().getAttackVars().getStyle() != AttackVars.CombatStyle.SLASH ? 407 : 405));
        }
        switch (weapon) {
            case 9703:
                return 412;
            case 13263: // Bludgeon
                return 3298;
            case 6522: // Tzzar Throwing Thing
                return 2614;
            case 11959: // Black Chin
            case 10034: // Red Chin
            case 10033: // Grey Chin
                return 2779;
            case 11791: // STAFF OF DEAD
            case 12904: // TOXIC STAFF OF DEAD
                return 440;
            case 12848:
            case 4153: // granite maul
                return 1665;
            case 4726: // guthan
                return 2080;
            case 4747: // torag
                return 0x814;
            case 4710: // ahrim
                return 406;
            case 4755: // verac
                return 2062;
            case 4734: // karil
                return 2075;
            case 4151: // Whip
            case 12006: // Tentacle
                return 1658;
            case 6528: // tzaar Maul
                return 2661;
            case 10887: // ANCHOR
                return 5865;
            default:
                return 451;
        }
    }


    public static final int BLOCK_ANIM_LENGTH = 600;

    /**
     * Gets the entity defence animation.
     *
     * @param entity
     * @return
     */
    public static int getDefendAnim(Entity entity) {
        if (entity.isNPC()) {
            return ((NPC) entity).getDefinition().getDefenceAnimation();
        }
        Player p = (Player) entity;
        int shield = p.getEquipment().getItemInSlot(5);
        if (shield != -1) {
            ItemDefinition shieldDefinition = ItemDefinition.forId(shield);
            return shieldDefinition.getBlockAnim();
        }
        return 424;
    }

    public static int getStandAnimation(Player player) {
        int weaponId = player.getEquipment().getItemInSlot(3);
        if (weaponId != -1) {
            ItemDefinition def = ItemDefinition.forId(weaponId);
            if (def != null) {
                return def.getStandAnim();
            }
        }
        return 808;
    }

    public static int getRunAnimation(Player player) {
        int weaponId = player.getEquipment().getItemInSlot(3);
        if (weaponId != -1) {
            ItemDefinition def = ItemDefinition.forId(weaponId);
            if (def != null) {
                return def.getRunAnim();
            }
        }
        return 824;
    }

    public static int getWalkAnimation(Player player) {
        int weaponId = player.getEquipment().getItemInSlot(3);
        if (weaponId != -1) {
            ItemDefinition def = ItemDefinition.forId(weaponId);
            if (def != null) {
                return def.getWalkAnim();
            }
        }
        return 819;
    }

    public static int getTurnAnimation(Player player) {
        int weaponId = player.getEquipment().getItemInSlot(3);
        if (weaponId != -1) {
            ItemDefinition def = ItemDefinition.forId(weaponId);
            if (def != null) {
                return def.getStandTurnAnim();
            }
        }
        return 823;
    }


    public static int getTurn90ClockwiseAnimation(Player player) {
        int weaponId = player.getEquipment().getItemInSlot(3);
        if (weaponId != -1) {
            ItemDefinition def = ItemDefinition.forId(weaponId);
            return def.getTurn90CWAnim();
        }
        return 821;
    }

    public static int getTurn90CounterClockwiseAnimation(Player player) {
        int weaponId = player.getEquipment().getItemInSlot(3);
        if (weaponId != -1) {
            ItemDefinition def = ItemDefinition.forId(weaponId);
            return def.getTurn90CCWAnim();
        }
        return 822;
    }

    public static int getTurn180Animation(Player player) {
        int weaponId = player.getEquipment().getItemInSlot(3);
        if (weaponId != -1) {
            ItemDefinition def = ItemDefinition.forId(weaponId);
            if (def != null) {
                return def.getTurn180Anim();
            }
        }
        return 820;
    }


    public static int getAttackAnimLength(Entity killer, int weaponId, boolean special) {
        if (killer.isNPC()) {
            return 0;
        }
        switch (weaponId) {
            case 10887://Barrelchest
                return 1800;
            case 11694://Godswords
            case 11696://
            case 11698://
            case 11700://
            case 11730://Saradomin sword
                return special ? 1800 : 1000;
        }
        return 0;
    }
}