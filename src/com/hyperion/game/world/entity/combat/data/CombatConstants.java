package com.hyperion.game.world.entity.combat.data;

public class CombatConstants {

	public enum CombatType {
		MELEE,
		RANGE,
		MAGE,
		FIRE_BREATH,
		TD_GROUND_ATTACK,
		CORP_MELEE_STOMP,
		CORP_SKILL_DRAIN,
		CORP_MULTI_ATTACK,
		MONKEY_GUARD_HEAL, 
		DISARM,
		TELEOTHER
	}
	
	public static int getId(CombatType type) {
		int index = CombatType.valueOf(type.toString()).ordinal();
		if(index > 2) {
			index = 2;
		} else {
			return index;
		}
		return index;
	}
	
	public static boolean isPoisonable(int weapon) {
		int[] weapons = {1231, 5680, 5698, 1263, 5716, 5730};
		for (int i = 0; i < weapons.length; i++) {
			if (weapon == weapons[i]) {
				return true;
			}
		}
		return false;
	}
}
