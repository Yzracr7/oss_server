package com.hyperion.game.world.entity.combat.data;

import java.util.List;

import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.tickable.impl.PoisonTask;
import com.hyperion.game.world.TeleportAreaLocations;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.combat.data.magic.MagicSpells;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.Hits.HitType;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.TormentedDemon;
import com.hyperion.game.world.entity.npc.impl.DemonicGorilla;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
import com.hyperion.utility.NumberUtils;

public class CombatEffects {

    /**
     * Combat effects.
     *
     * @param entity
     * @param target
     * @param damage
     */
    public static void executeCombatEffects(Entity entity, Entity target, int wep, int damage) {
        if (CombatConstants.isPoisonable(wep)) {
            if (!target.getCombatState().isPoisoned() && NumberUtils.random(5) == 0 && damage > 0) {
                World.getWorld().submit(new PoisonTask(target, 6));
            }
        }
        if (entity.isNPC()) {
            int poisonAmount = ((NPC) entity).getInitialPoisonDamage();
            boolean canPoison = poisonAmount != -1;
            if (canPoison) {
                if (!target.getCombatState().isPoisoned() && damage > 0) {
                    World.getWorld().submit(new PoisonTask(target, poisonAmount));
                }
            }
        }
        executeSmite(entity, target, damage);
        executePheonixNecklace(target);
        executeRecoil(target, entity, damage);
        // executeROL(target);
        executeVengeance(entity, target, damage);
        if (entity instanceof Player) {
            Player player = (Player) entity;
            if (ArmourSets.wearingFullGuthan(player)) {
                if (NumberUtils.random(7) == 1) {
                    if (player.getSkills().getLevel(Skills.HITPOINTS) < player.getSkills().getLevelForXp(Skills.HITPOINTS)) {
                        if (player.getSkills().getLevel(Skills.HITPOINTS) + damage <= player.getSkills().getLevelForXp(Skills.HITPOINTS)) {
                            player.getSkills().setLevel(Skills.HITPOINTS, player.getSkills().getLevel(Skills.HITPOINTS) + damage);
                        }
                    }
                    player.playGraphic(398);
                }
            } else if (ArmourSets.wearingFullTorag(player)) {
                if (NumberUtils.random(7) == 1 && target instanceof Player) {
                    Player pTarget = (Player) target;
                    boolean empty = pTarget.getVariables().getRunningEnergy() - 20 <= 0;
                    pTarget.getVariables().setRunningEnergy(empty ? 0 : pTarget.getVariables().getRunningEnergy() - 20, true);
                    pTarget.getPacketSender().sendMessage("Your run energy has been reduced.");
                    player.getPacketSender().sendMessage("Your target's run energy has been reduced.");
                    pTarget.playGraphic(399);
                }
            } else if (ArmourSets.wearingFullAhrim(player)) {
                if (NumberUtils.random(7) == 1 && target instanceof Player) {
                    Player pTarget = (Player) target;
                    boolean empty = pTarget.getSkills().getLevel(Skills.ATTACK) - 5 <= 0;
                    pTarget.getSkills().setLevel(Skills.ATTACK, empty ? 0 : pTarget.getSkills().getLevel(Skills.ATTACK) - 5);
                    pTarget.getPacketSender().sendMessage("Your Attack level has been reduced.");
                    player.getPacketSender().sendMessage("Your target's Attack level has been reduced.");
                    pTarget.playGraphic(400);
                }
            } else if (ArmourSets.wearingFullKaril(player)) {
                if (NumberUtils.random(7) == 1 && target instanceof Player) {
                    Player pTarget = (Player) target;
                    boolean empty = pTarget.getSkills().getLevel(Skills.AGILITY) - (pTarget.getSkills().getLevel(Skills.AGILITY) / 5) <= 0;
                    pTarget.getSkills().setLevel(Skills.AGILITY, empty ? 0 : pTarget.getSkills().getLevel(Skills.AGILITY) - (pTarget.getSkills().getLevel(Skills.AGILITY) / 5));
                    pTarget.getPacketSender().sendMessage("Your Agility level has been reduced.");
                    player.getPacketSender().sendMessage("Your target's Agility level has been reduced.");
                    pTarget.playGraphic(401);
                }
            }
        }
        MainCombat.checkAutoRetaliate(entity, target);
    }

    public static void executeInitialEffects(Entity entity, Entity target, int damage, int weapon) {
        if (weapon == 11696) {
            executeBandosGodswordEffect(entity, target, damage);
        }
    }

    /**
     * @param entity
     */
    private static void executePheonixNecklace(Entity entity) {
        if (entity.isNPC()) {
            return;
        }
        //TODO: Duel arena rule food off
        Player player = (Player) entity;
        boolean hasNecklace = (player.getEquipment().hasItem(11090));
        if (hasNecklace) {
            if (player.getHp() < player.getSkills().getLevelForXp(3) * 0.2) {
                player.heal((int) (player.getSkills().getLevelForXp(3) * 0.3));
                player.getEquipment().deleteItem(11090);
                player.getEquipment().refresh();
                player.getPacketSender().sendMessage("Your pheonix necklace heals you, but is destroyed in the process.");
            }
        }
    }

    private static void executeRecoil(Entity entity, Entity target, int damage) {
        if (entity.isNPC()) {
            return;
        }
        Player player = (Player) entity;
        boolean hasRing = (player.getEquipment().hasItem(2550));
        if (hasRing) {
            if (damage > 0) {
                int recDamage = damage / 10 + 1;
                player.getCombatState().getDamageMap().incrementTotalDamage(target, recDamage);
                Hit hit = new Hit(HitType.RECOIL, recDamage, 4);
                hit.setOwner(player);
                target.inflictDamage(hit, false);
                int uses = player.getVariables().getRecoilUses();
                if (uses >= 40) {
                    player.getEquipment().deleteItem(2550);
                    player.getPacketSender().sendMessage("Your ring of recoil has shattered.");
                    player.getVariables().setRecoilUses(0);
                } else
                    player.getVariables().setRecoilUses(uses + 1);
            }
        }
    }

    /**
     * Executes bgs spec
     *
     * @param entity
     * @param target
     * @param damage
     */
    private static void executeBandosGodswordEffect(Entity entity, Entity target, int damage) {
        if (entity.isNPC()) {
            return;
        }
        int[] skills = new int[]{Skills.DEFENCE, Skills.STRENGTH, Skills.PRAYER, Skills.ATTACK, Skills.MAGIC, Skills.RANGE};
        int newDmg = damage / 10;
        if (target instanceof Player) {
            Player pTarget = (Player) target;
            for (int i = 0; i < skills.length; i++) {
                if (newDmg > 0) {
                    if (!target.getCombatState().isDead()) {
                        int before = pTarget.getSkills().getLevel(skills[i]);
                        pTarget.getSkills().deductLevel(skills[i], newDmg);
                        int after = before - pTarget.getSkills().getLevel(skills[i]);
                        newDmg -= after;
                    }
                } else {
                    break;
                }
            }
        }
    }

    /**
     * Executes Vengeance.
     *
     * @param entity
     * @param target
     * @param damage
     */
    private static void executeVengeance(final Entity entity, Entity target, int damage) {
        if (damage <= 0 || target.isNPC()) {
            return;
        }
        final Player pTarget = (Player) target;
        if (pTarget.getCombatState().isVenged()) {
            pTarget.getCombatState().setVenged(false);
            int vengDamage = (int) (damage * 0.75);
            pTarget.playForcedChat("Taste vengeance!");
            if (pTarget.getCombatState().isDead()) {
                vengDamage = 0;
            }
            final int vengeanceDmg = vengDamage;
            World.getWorld().submit(new Tickable(1) {
                @Override
                public void execute() {
                    this.stop();
                    if (pTarget.getCombatState().isDead()) {
                        if (vengeanceDmg != 0) {
                            return;
                        }
                    }
                    entity.getCombatState().getDamageMap().incrementTotalDamage(pTarget, vengeanceDmg);
                    Hit hit = new Hit(HitType.RECOIL, vengeanceDmg, 4);
                    hit.setOwner(pTarget);
                    entity.inflictDamage(hit, false);
                }
            });
        }
    }

    /**
     * Executes Smiting in combat.
     *
     * @param killer
     * @param target
     * @param damage
     */
    private static void executeSmite(Entity killer, Entity target, int damage) {
        if (killer.isNPC() || damage <= 0) {
            return;
        }
        Player player = (Player) killer;
        if (target.isPlayer()) {
            Player pTarget = (Player) target;
            if (pTarget.getSettings().getPrayerPoints() > 0 && !pTarget.getCombatState().isDead()) {
                boolean smite = player.getPrayers().isPrayerActive("smite");
                int pray = (int) pTarget.getSettings().getPrayerPoints();
                int drainCount = 4;
                int newPray = pray - (damage / drainCount);
                if (smite) {
                    pTarget.getSkills().setLevel(5, newPray);
                    if ((int) pTarget.getSettings().getPrayerPoints() <= 0) {
                        pTarget.getSkills().setLevel(5, 0);
                    }
                    pTarget.getPacketSender().sendSkillLevel(5);
                }
            }
        }
    }

    /**
     * The percentage of the hit reducted by antifire.
     */
    public static double dragonfireReduction(Player mob) {
        boolean dragonFireShield = mob.getEquipment().containsDragonShield();
        boolean dragonfirePotion = mob.getSettings().isUsingAntifire();
        boolean superDragonfirePotion = mob.getSettings().isUsingSuperAntifire();
        if (dragonFireShield && dragonfirePotion) {
            //  mob.getPacketSender().sendMessage("You shield absorbs most of the dragon fire!");
            // mob.getPacketSender().sendMessage("Your potion protects you from the heat of the dragon's breath!");
            return 1;
        } else if (dragonFireShield) {
            mob.getPacketSender().sendMessage("You shield absorbs most of the dragon fire!");
            return 0.8;// 80%
        } else if (dragonfirePotion) {
            // mob.getPacketSender().sendMessage("Your potion protects you from the heat of the dragon's breath!");
            return 0.5; // 50%
        } else if (superDragonfirePotion) {
            // mob.getPacketSender().sendMessage("Your potion protects you from the heat of the dragon's breath!");
            return 1; // 100%*
        }
        return 0;
    }

    /**
     * Executes retribution effect.
     *
     * @param player
     */
    public static void executeRetributionPrayer(Player player) {
        boolean retribution = player.getPrayers().isPrayerActive("Retribution");
        if (retribution && player.getWildLevel() > 0) {
            final int prayPercentage = (int) (player.getSkills().getLevelForXp(5) * 0.25);
            int dmg = 5;
            if (prayPercentage > 5) {
                dmg = NumberUtils.random(5, prayPercentage);
            } else {
                dmg = 5;
            }
            player.playGraphic(Graphic.create(437, 0, 0));
            for (int regionId : player.getMapRegionsIds()) {
                List<Integer> playerIndexes = World.getWorld().getRegion(regionId, false).getPlayerIndexes();
                if (playerIndexes == null) {
                    continue;
                }
                for (Integer playerIndex : playerIndexes) {
                    Player target = World.getWorld().getPlayer(playerIndex);
                    if (target == null)
                        continue;
                    if (target.getCombatState().isDead()) {
                        continue;
                    }
                    if (target.equals(player)) {
                        continue;
                    }
                    if (!TeleportAreaLocations.isInMultiZone(target, target.getLocation())) {
                        if (player.getCombatState().getDamageMap().highestDamage() != null && player.getCombatState().getDamageMap().highestDamage().equals(target)) {
                            if (target.getLocation().inArea(player.getLocation().getX() - 1, player.getLocation().getY() - 1, player.getLocation().getX() + 1, player.getLocation().getY() + 1)) {
                                target.getCombatState().getDamageMap().incrementTotalDamage(player, dmg);
                                Hit hit = new Hit(HitType.RECOIL, dmg, 4);
                                hit.setOwner(player);
                                target.inflictDamage(hit, false);
                            }
                        }
                    } else {
                        if (target.getLocation().inArea(player.getLocation().getX() - 3, player.getLocation().getY() - 3, player.getLocation().getX() + 3, player.getLocation().getY() + 3)) {
                            if (TeleportAreaLocations.isInAttackableArea(target)) {
                                target.getCombatState().getDamageMap().incrementTotalDamage(player, dmg);
                                Hit hit = new Hit(HitType.RECOIL, dmg, 4);
                                hit.setOwner(player);
                                target.inflictDamage(hit, false);
                            }
                        }
                    }
                }
            }
        }
    }

    public static Hit altarCombatDamage(Entity attacker, Entity target, Hit hit) {
        if (hit == null)
            return hit;
        if (attacker.isPlayer()) {
            Player player = (Player) attacker;
            if (target.isNPC()) {
                NPC npc = (NPC) target;
                if (npc.getId() == 3497) {// rfd
                    int stage = npc.getAttributes().getInt("hfdstage");
                    if (stage == 0) {// White
                        if (hit.getIncomingHit() == null || hit.getIncomingHit().getSpellId() == -1 || hit.getIncomingHit().getWeapon() == 13652)
                            hit.setDamage(0);
                        else if (!MagicSpells.getSpellName(hit.getIncomingHit().getAttacker(), hit.getIncomingHit().getSpellId()).startsWith("Wind")) {
                            hit.setDamage(0);
                        }
                    } else if (stage == 1) {// orange
                        if (hit.getIncomingHit() == null)
                            hit.setDamage(0);
                        else if (!hit.getIncomingHit().getType().equals(CombatType.MELEE)) {
                            hit.setDamage(0);
                        }
                    } else if (stage == 2) {// blue
                        if (hit.getIncomingHit() == null || hit.getIncomingHit().getWeapon() == 13652)
                            hit.setDamage(0);
                        else if (!MagicSpells.getSpellName(hit.getIncomingHit().getAttacker(), hit.getIncomingHit().getSpellId()).startsWith("Water")) {
                            hit.setDamage(0);
                        }
                    } else if (stage == 3) {// red
                        if (hit.getIncomingHit() == null || hit.getIncomingHit().getWeapon() == 13652)
                            hit.setDamage(0);
                        else if (!MagicSpells.getSpellName(hit.getIncomingHit().getAttacker(), hit.getIncomingHit().getSpellId()).startsWith("Fire")) {
                            hit.setDamage(0);
                        }
                    } else if (stage == 4) {// green
                        if (hit.getIncomingHit() == null || hit.getIncomingHit().getWeapon() == 13652)
                            hit.setDamage(0);
                        else if (!hit.getIncomingHit().getType().equals(CombatType.RANGE)) {
                            hit.setDamage(0);
                        }
                    } else if (stage == 5) {// brown
                        if (hit.getIncomingHit() == null || hit.getIncomingHit().getWeapon() == 13652)
                            hit.setDamage(0);
                        else if (!MagicSpells.getSpellName(hit.getIncomingHit().getAttacker(), hit.getIncomingHit().getSpellId()).startsWith("Earth")) {
                            hit.setDamage(0);
                        }
                    }
                } else if (npc.getId() == 963) {// KQ
                    boolean firstForm = !npc.getAttributes().isSet("kqsecondform");
                    if (firstForm) {// 1st form
                        if (hit.getIncomingHit() != null && !(hit.getIncomingHit().getType().equals(CombatType.MELEE) && ArmourSets.wearingFullVerac(player))) {
                            hit.setDamage((int) (hit.getDamage() * 0.50));
                        }
                    } else if (!firstForm) {// 2nd form
                        if (hit.getIncomingHit() != null && hit.getIncomingHit().getType().equals(CombatType.MELEE) && ArmourSets.wearingFullVerac(player)) {
                            hit.setDamage((int) (hit.getDamage() * 0.50));
                        }
                    }
                } else if (npc.getId() == 2265) {// dag supreme
                    if (hit.getIncomingHit() == null || !hit.getIncomingHit().getType().equals(CombatType.MELEE)) {
                        hit.setDamage(0);
                    }
                } else if (npc.getId() == 2266) {// dag prime
                    if (hit.getIncomingHit() == null || !hit.getIncomingHit().getType().equals(CombatType.RANGE)) {
                        hit.setDamage(0);
                    }
                } else if (npc.getId() == 2267) {// dag rex
                    if (hit.getIncomingHit() == null || !hit.getIncomingHit().getType().equals(CombatType.MAGE)) {
                        hit.setDamage(0);
                    }
                }
            }
        }
        if (target.isPlayer()) {
            Player pTarget = (Player) target;
            if (pTarget.getEquipment().hasItem(12817) & NumberUtils.random(100) <= 70 & hit.getDamage() > 0) {
                hit.setDamage((int) Math.ceil(hit.getDamage() * .75));
                pTarget.getPacketSender().sendMessage("Your shield glows as it absorbs some of the damage recieved.");
            } else if (pTarget.getEquipment().hasItem(13740) && pTarget.getSettings().getPrayerPoints() > 0) {
                int remove = (int) Math.ceil(hit.getDamage() * .06);

                if (pTarget.getSkills().getLevel(Skills.PRAYER) >= remove) {
                    pTarget.getSettings().decreasePrayerPoints(remove);
                    hit.setDamage((int) Math.ceil(hit.getDamage() * .70));
                }
            }
        } else if (target.isNPC()) {
            NPC nTarget = (NPC) target;
            if (nTarget.getAttributes().isSet("dem_gorilla")) {
                DemonicGorilla dem = (DemonicGorilla) nTarget.getAttributes().get("dem_gorilla");
                hit.setDamage(dem.handleIncomingAttack(hit));
            }
        }
        return hit;
    }

    public static int getExtraDamage(Entity entity, Entity target, CombatType combat_type, int weapon, int damage) {
        boolean melee = combat_type.equals(CombatType.MELEE);
        boolean ranged = combat_type.equals(CombatType.RANGE);
        boolean magic = combat_type.equals(CombatType.MAGE);
        int helmet = -1;
        int amulet = -1;
        int extra = -1;
        if (entity.isPlayer()) {
            Player player = (Player) entity;
            Item[] equipment = player.getEquipment().getItems();
            if (equipment[Equipment.EQUIPMENT.HELMET.ordinal()] != null) {
                helmet = equipment[Equipment.EQUIPMENT.HELMET.ordinal()].getId();
            }
            if (equipment[Equipment.EQUIPMENT.AMULET.ordinal()] != null) {
                amulet = equipment[Equipment.EQUIPMENT.AMULET.ordinal()].getId();
            }
            if (magic) {
                if (amulet == 18335 || amulet == 18604) {// Stream / occult necklace
                    extra += (int) (damage * (amulet == 18335 ? 0.15 : 0.10));
                }
                if (weapon == 18600) { //Staff of the dead
                    extra += (int) (damage * 0.15);
                } else if (weapon == 18602)
                    extra += (int) (damage * 0.10);
            }
            if (target.isNPC()) {
                NPC npc = (NPC) target;
                String name = npc.getDefinition().getName().toLowerCase();
                if (name.equals("kalphite")) {
                    if (melee) {
                        if (weapon == 10581 || weapon == 10582 || weapon == 10583 || weapon == 10584) {
                            double percentage = damage * 0.33;
                            extra += (int) percentage;
                        }
                    }
                } else if (name.equals("dagannoth")) {
                    if (melee) {
                        if (weapon == 15403) {
                            double percentage = damage * 0.25;
                            extra += (int) percentage;
                        }
                    }
                } else if (name.equals("dagannoth supreme")) {
                    if (melee) {
                        if (weapon == 15403) {
                            double percentage = damage * 0.15;
                            extra += (int) percentage;
                        }
                    }
                } else {
                    String slayer_task = player.getVariables().getTaskName();
                    double multiplier = /*
                     * player.getDetails().isGoldMember() ?
                     * 0.20 :
                     */0.15;
                    if (slayer_task != null) {
                        if (name.equalsIgnoreCase(slayer_task)) {
                            if (helmet == 15492) {// Full Slayer helm
                                double percentage = Math.round(damage * multiplier);
                                extra += (int) percentage;
                            }
                            if (melee) {
                                if ((helmet == 8921) || (helmet == 13263)) {// Black
                                    // Mask
                                    // &
                                    // Slayer
                                    // Helm
                                    double percentage = Math.round(damage * multiplier);
                                    extra += (int) percentage;
                                }
                            } else if (ranged) {
                                if (helmet == 15490) {// Focus sight
                                    double percentage = Math.round(damage * multiplier);
                                    extra += (int) percentage;
                                }
                            } else if (magic) {
                                if (helmet == 15488) {// Hexcrest
                                    double percentage = Math.round(damage * multiplier);
                                    extra += (int) percentage;
                                }
                            }
                        }
                    }
                }
            }
        }
        return extra;
    }
}
