package com.hyperion.game.world.entity.combat.data;

import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.WeaponInterfaces;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.AttackVars;
import com.hyperion.game.world.entity.player.Player;

public class Speeds {

	public static int getCombatSpeeds(Entity killer, int weaponId) {
		if (killer.isNPC()) {
			return ((NPC) killer).getDefinition().getAttackSpeed();//TODO fix
		}
		if (weaponId == -1) {
			return 4;
		}//
		Player player = (Player) killer;
		AttackVars attack = player.getSettings().getAttackVars();
		ItemDefinition def = ItemDefinition.forId(weaponId);
		WeaponInterfaces.WeaponInterface weapon_interface = null;
		for (int index = 0; index < WeaponInterfaces.WeaponInterface.values().length; index++) {
			if (WeaponInterfaces.WeaponInterface.values()[index] != null) {
				String weapon_interface_name = WeaponInterfaces.WeaponInterface.values()[index].toString();

				weapon_interface_name = (weapon_interface_name.replace("_", " "));
				weapon_interface_name = (weapon_interface_name.replace("'", ""));

				String weapon_name = (def.getName().replace("_", " "));
				weapon_name = (weapon_name.replace("'", ""));
				//System.out.println("WEAPON : " + weapon_name.toString() + "    " + weapon_interface_name);

				if (weapon_name.equalsIgnoreCase(weapon_interface_name) || weapon_name.toLowerCase().contains(weapon_interface_name.toLowerCase())) {
					weapon_interface = WeaponInterfaces.WeaponInterface.values()[index];
					//player.sendMessage("[Weap_Inter] Weapon:" + WeaponInterfaces.WeaponInterface.values()[index].name() + " [speed=" + WeaponInterfaces.WeaponInterface.values()[index].getSpeed() + "] [inter: " + WeaponInterfaces.WeaponInterface.values()[index].getInterfaceId() + "]");
					//System.out.println("SUCEEESSSSSSS" + weapon.toString() + "    " + weapon_interface_name);
				}
			}
		}
		if(weapon_interface == null)
			return 10;
		if(weapon_interface.getSpeed() < 0)
			return 10;

		
		if (attack.getSkill().name().equalsIgnoreCase("range")) {
			switch (attack.getStyle().name()) {
			case "RANGE_ACCURATE":
				return weapon_interface.getSpeed() + 1;
			case "RANGE_RAPID":
				return weapon_interface.getSpeed() - 1;
			case "RANGE_DEFENSIVE":
				return weapon_interface.getSpeed() + 1;
			}
		}
		switch (attack.getSkill().name()) {
		case "ACCURATE":
			return weapon_interface.getSpeed();
		case "AGGRESSIVE":
			return weapon_interface.getSpeed();
		case "DEFENSIVE":
			return weapon_interface.getSpeed();
		case "CONTROLLED":
			return weapon_interface.getSpeed();
		}
		return 10;
	}
}
