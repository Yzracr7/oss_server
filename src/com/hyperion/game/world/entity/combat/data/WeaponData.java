package com.hyperion.game.world.entity.combat.data;

import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.player.Player;

public class WeaponData {

	public static int getWeaponDistance(Entity aggressor) {
		if (aggressor.isPlayer()) {
			Player player = (Player) aggressor;
			switch (player.getEquipment().getItemInSlot(3)) {
			case 3204: //Dragon halberd
				return 2;
			default:
				return 1;
			}
		}
		return 1;
	}
}
