package com.hyperion.game.world.entity.combat.data.magic;

import java.util.List;

import com.hyperion.game.Constants;
import com.hyperion.game.content.minigames.clanwars.session.WarSession;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.TeleportAreaLocations;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatAction;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.combat.actions.MagicAction;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.utility.NumberUtils;

public class MagicEffects extends MagicSpells {

    public static int getRandomizedSpellMaxHit(Player player, Entity target, int spell, int wep) {
        int damage = getMaxHit(player, spell, wep);
        if (spell == 20 || spell == 21 || spell == 22) {
            if (player.getCombatState().isCharged()) {
                damage = 30;
            }
        }

        //	int override = -1;

        if (spell == 8 || spell == 16 || spell == 30 || spell == 33) {
            damage = NumberUtils.random(3);
        }

        if (target.isPlayer()) {
            if (!HitFormula.willHit((Player) player, (Player) target, wep, false, "magic", spell))
                damage = 0;
        } else if (target.isNPC()) {
            int accuracy = NumberUtils.random((int) HitFormula.calculateMagicAccuracyForPVM(player));
            int def = NumberUtils.random((int) ((NPC) target).getDefinition().getMagicDefence());
            if (Constants.DEBUG_MODE)
                player.getPacketSender().sendMessage("Magic hit acc: " + accuracy + ", def: " + def);
            if (accuracy < def) {
                damage = 0;
            }
        }
        damage = damage > -1 ? NumberUtils.random((int) damage) : damage;

        return damage;
    }

    public static boolean freezeTarget(final Entity attacker, final Entity target, boolean halve, int spell, int damage) {
        int time = (FREEZE_TIMERS[spell]);
        int index = getSpellIndex(attacker, spell);
        if (target.getAttributes().isSet("stopActions")) {
            return false;
        }
        if (attacker.isPlayer()) {
            Player player = (Player) attacker;
            if (player.getAttributes().isSet("clansession")) {
                WarSession war = player.getAttributes().get("clansession");
                if (war != null && war.getMiscRules()[0]) { //Ignore freezing
                    return false;
                }
            }
        }
        if (damage <= 0) {
            return false;
        }
        if (index == 8 || index == 16 || index == 30) {
            if (halve) {
                time /= 2;
            }
        }
        if (target.getCombatState().isFreezable()) {
            MainCombat.endCombat(target, 1);
            target.getCombatState().setFreezable(false);
            target.getCombatState().setFrozen(true);
            final int finalTimer = time - 1; //-1 for the tickable delay?
            World.getWorld().submit(new Tickable(target, 1) {
                int count = finalTimer;

                @Override
                public void execute() {
                    if (!attacker.getLocation().withinActualDistance(target.getCentreLocation(), 11)) {
                        target.getCombatState().setFrozen(false);
                        World.getWorld().submit(new Tickable(7) {
                            @Override
                            public void execute() {
                                this.stop();
                                target.getCombatState().setFreezable(true);
                            }
                        });
                        this.stop();
                        return;
                    }
                    if (count > 0) {
                        count--;
                        return;
                    } else if (count == 0) {
                        this.stop();
                        target.getCombatState().setFrozen(false);
                        World.getWorld().submit(new Tickable(7) {
                            @Override
                            public void execute() {
                                this.stop();
                                target.getCombatState().setFreezable(true);
                            }
                        });
                        return;
                    }
                }
            });
        }
        return true;
    }

    private static void executeTeleBlock(Player player, final Entity target) {
        int time = 500;
        if (target.isPlayer()) {
            if (player.getCombatState().isTeleBlocked()) {
                player.getPacketSender().sendMessage("Your target is already teleblocked!");
                return;
            }
            if (((Player) target).getPrayers().isPrayerActive("protect from magic")) {
                time /= 2;
            }
            ((Player) target).getPacketSender().sendMessage("A teleport block has been cast on you!");
            ((Player) target).getCombatState().setTbTime(time);
        }
    }

    public static void handleSpellEffects(Player player, Entity target, int spell, int damage) {
        int bloodHeal = -1;
        if (Constants.DEBUG_MODE)
            player.getPacketSender().sendMessage("Spell: " + spell + ", damage: " + damage);
        switch (spell) {
            case 33://Teleport Block
                executeTeleBlock(player, target);
                break;
            case 37://Blood rush
                //TODO: ??
                break;
            case 42://Blood burst
                bloodHeal = (int) (damage * 0.25);
                player.getSkills().increaseLevelToMaximum(3, bloodHeal);
                if (bloodHeal > 0) {
                    //player.getActionSender().sendMessage("You drain some of your opponent's health.");
                }
                break;
            case 47://Blood blitz
                bloodHeal = (damage / 4);
                player.getSkills().increaseLevelToMaximum(3, bloodHeal);
                if (bloodHeal > 0) {
                    //player.getActionSender().sendMessage("You drain some of your opponent's health.");
                }
                break;
            case 52://Blood barrage
                bloodHeal = (int) (damage * 0.25);
                player.getSkills().increaseLevelToMaximum(3, bloodHeal);
                if (bloodHeal > 0) {
                    //player.getActionSender().sendMessage("You drain some of your opponent's health.");
                }
                break;
        }
    }

    public static void executeMultiSpell(Player player, Entity mainTarget, int spell) {
        Graphic endGraphic = new Graphic(MagicSpells.END_GFX[spell], 0, MagicSpells.getSpellEndGfxHeight(player, spell));
        if (!isMultiSpell(spell)) {
            return;
        }
        Location l = mainTarget.getLocation();
        if (!TeleportAreaLocations.isInMultiZone(mainTarget, l)) {
            return;
        }
        if (mainTarget.isNPC()) {
            for (int regionId : player.getMapRegionsIds()) {
                List<Integer> npcIndexes = World.getWorld().getRegion(regionId, false).getNPCsIndexes();
                if (npcIndexes == null) {
                    continue;
                }
                for (Integer npcIndex : npcIndexes) {
                    NPC npc = World.getWorld().getNPC(npcIndex);
                    if (npc == null || mainTarget == null || npc.equals(mainTarget) || npc.getCombatState().isDead())
                        continue;
                    if (npc.getLocation().inArea(l.getX() - 1, l.getY() - 1, l.getX() + 1, l.getY() + 1)) {
                        CombatAction mageAction = MagicAction.getAction();
                        //SOTD/Arcane only provide +15% bonus damage on the first hit, not multi hits.
                        npc.getAttributes().set("noExtraDamage", true);
                        mageAction.handleDamage(player, npc, mageAction.getDamage(player, npc, -1, -1, spell, false), spell, endGraphic, true);
                    }
                }
            }
        } else {
            for (int regionId : player.getMapRegionsIds()) {
                List<Integer> playerIndexes = World.getWorld().getRegion(regionId, false).getPlayerIndexes();
                if (playerIndexes == null) {
                    continue;
                }
                for (Integer playerIndex : playerIndexes) {
                    Player target = World.getWorld().getPlayer(playerIndex);
                    if (target == null || target.equals(player) || mainTarget == null || target.equals(mainTarget) || target.getCombatState().isDead())
                        continue;
                    if (target.getLocation().inArea(l.getX() - 1, l.getY() - 1, l.getX() + 1, l.getY() + 1)) {
                        if (!TeleportAreaLocations.isInAttackableArea(target)) {
                            continue;
                        }
                        if (!ProjectilePathFinder.hasLineOfSight(player, target, false)) {
                            continue;
                        }
                        CombatAction mageAction = MagicAction.getAction();
                        //SOTD/Arcane only provide +15% bonus damage on the first hit, not multi hits.
                        target.getAttributes().set("noExtraDamage", true);
                        mageAction.handleDamage(player, target, mageAction.getDamage(player, target, -1, -1, spell, false), spell, endGraphic, true);
                    }
                }
            }
        }
    }
}
