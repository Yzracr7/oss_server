package com.hyperion.game.world.entity.combat.data.magic;

import com.hyperion.game.content.minigames.clanwars.ClanWarsData;
import com.hyperion.game.content.minigames.clanwars.session.WarSession;
import com.hyperion.game.item.Item;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.DumbPathFinder;

public class MagicSpells {

    public static final int SPELL_LEVEL[] = {
            /* Modern spells */
            1, 3, 5, 9, 11, 13, 17, 19, 20, 23, 29, 35,
            39, 41, 47, 50, 50, 50, 53, 59, 60, 60, 60,
            62, 65, 66, 70, 73, 74, 75, 79, 80, 82, 85,
            90,
            /* Ancient spells */
            50, 52, 56, 58, 61,
            62, 64, 68, 70, 73,
            74, 76, 80, 82, 85,
            86, 88, 92, 94, 97
    };

    public static final int START_GFX[] = {
            /* Modern spells */
            90, 102, 93, 96, 105, 99, 117, 108, 177, 120, 123, 126,
            145, 132, 135, 87, 177, 327, 138, 129, -1, -1, -1,
            158, 161, 167, 164, 170, -1, 155, 177, 173, -1, -1,
            -1,
            /* Ancient spells */
            -1, -1, -1, -1, 1845,
            -1, -1, -1, -1, 1848,
            -1, -1, -1, 366, 1850,
            -1, -1, -1, -1, 1853,
            /** Custom spells */
            1184
    };

    public static final int END_GFX[] = {
            /* Modern spells */
            92, 104, 95, 98, 107, 101, 119, 110, 181, 122, 125, 128,
            147, 134, 137, 89, 180, 329, 140, 131, 76, 77, 78,
            160, 163, 169, 166, 172, -1, 157, 179, 175, -1, 345,
            -1,
            /* Ancient spells */
            385, 379, 373, 361, 1847,
            389, 382, 376, 363, 1849,
            387, 381, 375, 367, 1851,
            391, 383, 377, 369, 1854,
            /** Custom spells */
            -1,
    };

    public static final int PROJECTILE_GFX[] = {
            /* Modern spells */
            91, 103, 94, 97, 106, 100, 118, 109, 178, 121, 124, 127,
            146, 133, 136, 88, 178, 328, 139, 130, -1, -1, -1,
            159, 162, 168, 165, 171, -1, 156, 178, 174, -1, 344,
            -1,
            /* Ancient spells */
            384, 378, -1, 360, 1846,
            -1, -1, -1, -1, -1,
            386, 380, 374, -1, 1852,
            -1, -1, -1, -1, -1,
            /** Custom spells */
            1227
    };


    public static final int SPELL_MAX_HIT[] = {
            /* Modern spells */
            5, -1, 6, 7, -1, 8, 9, -1, -1, 10, 11, 12,
            15, 13, 14, -1, 2, 15, 15, 16, 20, 20, 20,
            17, 18, -1, 19, -1, -1, 20, 3, -1, -1, -1,
            -1,
            /* Ancient spells */
            15, 16, 17, 18, 18,
            19, 20, 21, 22, 24,
            23, 24, 25, 26, 28,
            27, 28, 29, 30, 35,
            /** Custom spells */
            12
    };

    private static int SPELL_STAFFS[] = {
            /* Modern spells */
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, 1409, -1, 4170, -1, -1, 2415, 2416, 2417,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1,
            /* Ancient spells */
            -1, -1, -1, -1, 13867,
            -1, -1, -1, -1, 13867,
            -1, -1, -1, -1, 13867,
            -1, -1, -1, -1, 13867
    };

    public static final int FREEZE_TIMERS[] = {
            /* Modern spells */
            0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, //12
            0, 0, 0, 0, 16, 0, 0, 0, 0, 0, 0, //11
            0, 0, 0, 0, 0, 0, 0, 24, 0, 0, 0, //11
            0,
            /* Ancient spells */
            0, 0, 0, 8, 0,
            0, 0, 0, 16, 0,
            0, 0, 0, 24, 0,
            0, 0, 0, 32, 0,
    };

    protected static boolean isP2pSpell(int spell) {
        if (spell >= 16 && spell != 19) {
            return true;
        }
        return false;
    }

    public static boolean isMultiSpell(int spell) {
        switch (spell) {
            case 40://Smoke burst
            case 41://Shadow burst
            case 42://Blood burst
            case 43://Ice burst
            case 50://Smoke barrage
            case 51://Shadow barrage
            case 52://Blood barrage
            case 53://Ice barrage
            case 44://Miasmic burst
            case 54://Miasmic barrage
                return true;
        }
        return false;
    }

    protected static boolean isOtherSpell(Player player, int spell) {
        switch (spell) {
            case 33://tb
            case 30://tangle
            case 8://bind
            case 16://snare
                return true;
        }
        return false;
    }

    public static int getSpellEndGfxHeight(Entity player, int spell) {
        String name = getSpellName(player, spell);
        if (name == null) {
            return -1;
        }
        if (name.endsWith("Rush")) {
            return 0;
        }
        if (name.endsWith("Burst")) {
            return 0;
        }
        if (name.endsWith("Blitz")) {
            return 0;
        }
        if (name.endsWith("Barrage")) {
            return 0;
        }
        if (name.equals("Teleport Block")) {
            return 0;
        }
        if (name.equals("Flames of Zamorak")) {
            return 0;
        }
        return 100;
    }

    protected static int getSpellProjectileStartHeight(Entity entity, int spell) {
        String name = getSpellName(entity, spell);
        if (name.equalsIgnoreCase("Teleport Block")) {
            return 35;
        }
        if (name.equalsIgnoreCase("blood blitz")) {
            return 20;
        }
        return 43;
    }

    protected static int getSpellProjectileEndHeight(Entity entity, int spell) {
        String name = getSpellName(entity, spell);
        if (name.equalsIgnoreCase("blood blitz")) {
            return 0;
        }
        return 35;
    }

    public static int getSpellStartHeight(Entity player, int spell) {
        String name = getSpellName(player, spell);
        if (name.equals("Teleport Block")) {
            return 30;
        }
        if (name.startsWith("Miasmic")) {
            return 0;
        }
        return 100;
    }

    private static int getSpellSlope(Entity player, int spell) {
        String name = getSpellName(player, spell);
        return 15;
    }

    protected static int getSpellHitDelay(Entity player, Entity target, int spell) {
        int delay = 3;
        int distance = player.getLocation().distanceToPoint(target.getLocation());
        switch (spell) {
            case 48://ice blitz
            case 47://blood blitz
            case 46://shadow blitz
            case 45://smoke blitz
            case 49://miasmic blitz
                if (distance > 2 && distance < 5) {
                    delay = 4;
                } else if (distance >= 5 && distance <= 8) {
                    delay = 5;
                } else if (distance > 8) {
                    delay = 6;
                }
                break;
            case 50://barrages
            case 51:
            case 52:
            case 53:
			/*if (!player.getLocation().withinDistance(target.getLocation(), 5, false)) {
				delay = 4;
			} else if (!player.getLocation().withinDistance(target.getLocation(), 8, false)) {
				delay = 5;
			}*/
                break;
            default:
				/*if (distance > 3) {
					delay = 4;
				} else if (distance > 6) {
					delay = 5;
				}*/
                break;
        }
        return delay;
    }

    public static final int getSpellAnimation(Entity player, int spell) {
        String name = getSpellName(player, spell);
        if (containsElemental(name)) {
            if (name.endsWith("Strike")) {
                return 1162;
            }
            if (name.endsWith("Bolt")) {
                return 1162;
            }
            if (name.endsWith("Blast")) {
                return 1162;
            }
            if (name.endsWith("Wave")) {
                return 1167;
            }
        }
        if (name.equals("Confuse")) {
            return 1165;
        }
        if (name.equals("Weaken")) {
            return 1164;
        }
        if (name.equals("Curse")) {
            return 1163;
        }
        if (name.equals("Bind")) {
            return 1161;
        }
        if (name.equals("Crumble Undead")) {
            return 1165;
        }
        if (name.equals("Iban Blast")) {
            return 708;
        }
        if (name.equals("Snare")) {
            return 1161;
        }
        if (name.equals("Magic Dart")) {
            return 1576;
        }
        if (isGodSpell(spell)) {
            return 811;
        }
        if (name.equals("Entangle")) {
            return 1161;
        }
        if (name.equals("Teleport Block")) {
            return 1820;
        }
        if (name.equals("Vulnerability") || name.equals("Enfeeble") || name.equals("Stun")) {
            return 729;
        }
        if (!name.contains("Miasmic")) {
            if (name.endsWith("Rush")) {
                return 1978;
            }
            if (name.endsWith("Burst")) {
                return 1979;
            }
            if (name.endsWith("Blitz")) {
                return 1978;
            }
            if (name.endsWith("Barrage")) {
                return 1979;
            }
        } else if (name.contains("Miasmic")) {
            if (name.endsWith("Rush")) {
                return 10513;
            }
            if (name.endsWith("Burst")) {
                return 10516;
            }
            if (name.endsWith("Blitz")) {
                return 10524;
            }
            if (name.endsWith("Barrage")) {
                return 10518;
            }
        }
        return -1;
    }

    private static boolean containsElemental(String name) {
        return (name.startsWith("Wind") || name.startsWith("Water") || name.startsWith("Earth") || name.startsWith("Fire"));
    }

    private static boolean isGodSpell(int spell) {
        return (spell == 20 || spell == 21 || spell == 22);
    }

    private static boolean stopCastAfterFrozen(Player player, int spell) {
        String name = getSpellName(player, spell);
        if (name.equals("Entangle")) {
            return true;
        }
        if (name.equals("Snare")) {
            return true;
        }
        if (name.equals("Bind")) {
            return true;
        }
        return false;
    }

    public static int getSpellForAutoCastButton(Player player, int button) {
        //System.out.println("button: " + button);
        //System.out.println("type: " + player.getSettings().getMagicType());
        //int wep = player.getEquipment().getItemInSlot(3);
        if (player.getSettings().getMagicType() == 1) {//Modern
            switch (button) {
                case 0://wind strike
                    return 0;
                case 1://water strike
                    return 2;
                case 2://earth strike
                    return 3;
                case 3://fire strike
                    return 5;
                case 4://wind bolt
                    return 6;
                case 5://water bolt
                    return 9;
                case 6://earth bolt
                    return 10;
                case 7://fire bolt
                    return 11;
                case 8://wind blast
                    return 13;
                case 9://water blast
                    return 14;
                case 10://earth blast
                    return 18;
                case 11://fire blast
                    return 19;
                case 12://wind wave
                    return 23;
                case 13://water wave
                    return 24;
                case 14://earth wave
                    return 26;
                case 15://fire wave
                    return 29;
            }
        } else if (player.getSettings().getMagicType() == 2) {//Ancients
            switch (button) {
                case 93://Smoke rush
                    return 35;
                case 145://Shadow rush
                    return 36;
                case 51://Blood rush
                    return 37;
                case 7://Ice rush
                    return 38;
                case 119://Smoke burst
                    return 40;
                case 171://Shadow burst
                    return 41;
                case 71://Blood burst
                    return 42;
                case 29://Ice burst
                    return 43;
                case 106://Smoke blitz
                    return 45;
                case 158://Shadow blitz
                    return 46;
                case 62://Blood blitz
                    return 47;
                case 18://Ice blitz
                    return 48;
                case 132://Smoke barrage
                    return 50;
                case 184://Shadow barrage
                    return 51;
                case 82://Blood barrage
                    return 52;
                case 40://Ice barrage
                    return 53;
            }
        }
        return -1;
    }

    public static int getSpellIndex(Entity attacker, int spell) {
        if (attacker instanceof NPC) {
            return spell;
        }
        Player player = (Player) attacker;
        if (player.getSettings().getMagicType() == 1) {//Modern
            switch (spell) {
                case 1://Wind Strike
                    return 0;
                case 2://Confuse
                    return 1;
                case 4://Water Strike
                    return 2;
                case 6://Earth strike
                    return 3;
                case 7://Weaken
                    return 4;
                case 8://Fire Strike
                    return 5;
                case 10://Wind Bolt
                    return 6;
                case 11://Curse
                    return 7;
                case 12://Bind
                    return 8;
                case 14://Water Bolt
                    return 9;
                case 17://Earth Bolt
                    return 10;
                case 20://Fire Bolt
                    return 11;
                case 22://Crumble undead
                    return 12;
                case 24://Wind blast
                    return 13;
                case 27://Water blast
                    return 14;
                case 29://Iban blast
                    return 15;
                case 30://Snare
                    return 16;
                case 31://Magic dart
                    return 17;
                case 33://Earth blast
                    return 18;
                case 38://Fire blast
                    return 19;
                case 41://Saradomin strike
                    return 20;
                case 42://Claws of guthix
                    return 21;
                case 43://Flames of zamorak
                    return 22;
                case 45://Wind wave
                    return 23;
                case 48://Water wave
                    return 24;
                case 50://Vulnerability
                    return 25;
                case 52://Earth wave
                    return 26;
                case 53://Enfeeble
                    return 27;
                case 54://Teleother lumby
                    return 28;
                case 55://Fire wave
                    return 29;
                case 56://Entangle
                    return 30;
                case 57://Stun
                    return 31;
                case 59://Teleother fally
                    return 32;
                case 60://Teleport block
                    return 33;
                case 62://Teleother cammy
                    return 34;
            }
        } else if (player.getSettings().getMagicType() == 2) {//Ancient
            switch (spell) {
                case 8://Smoke rush
                    return 35;
                case 12://Shadow rush
                    return 36;
                case 4://Blood rush
                    return 37;
                case 0://Ice rush
                    return 38;
                case 16://Miasmic rush
                    return 39;
                case 10://Smoke burst
                    return 40;
                case 14://Shadow burst
                    return 41;
                case 6://Blood burst
                    return 42;
                case 2://Ice burst
                    return 43;
                case 18://Miasmic burst
                    return 44;
                case 9://Smoke blitz
                    return 45;
                case 13://Shadow blitz
                    return 46;
                case 5://Blood blitz
                    return 47;
                case 1://Ice blitz
                    return 48;
                case 17://Miasmic blitz
                    return 49;
                case 11://Smoke barrage
                    return 50;
                case 15://Shadow barrage
                    return 51;
                case 7://Blood barrage
                    return 52;
                case 3://Ice barrage
                    return 53;
                case 19://Miasmic barrage
                    return 54;
            }
        } else if (player.getSettings().getMagicType() == 3) {//Lunar
            switch (spell) {
                case 19://Venge other
                    return 55;
            }
        }
        return -1;
    }

    public static String getSpellName(Entity killer, int spellIndex) {
        if (killer instanceof Player) {
            Player player = (Player) killer;
            if (player.getSettings().getMagicType() == 1) {//Modern
                switch (spellIndex) {
                    case 0:
                        return "Wind Strike";
                    case 1:
                        return "Confuse";
                    case 2:
                        return "Water Strike";
                    case 3:
                        return "Earth Strike";
                    case 4:
                        return "Weaken";
                    case 5:
                        return "Fire Strike";
                    case 6:
                        return "Wind Bolt";
                    case 7:
                        return "Curse";
                    case 8:
                        return "Bind";
                    case 9:
                        return "Water Bolt";
                    case 10:
                        return "Earth Bolt";
                    case 11:
                        return "Fire Bolt";
                    case 12:
                        return "Crumble Undead";
                    case 13:
                        return "Wind Blast";
                    case 14:
                        return "Water Blast";
                    case 15:
                        return "Iban Blast";
                    case 16:
                        return "Snare";
                    case 17:
                        return "Magic Dart";
                    case 18:
                        return "Earth Blast";
                    case 19:
                        return "Fire Blast";
                    case 20:
                        return "Saradomin Strike";
                    case 21:
                        return "Claws of Guthix";
                    case 22:
                        return "Flames of Zamorak";
                    case 23:
                        return "Wind Wave";
                    case 24:
                        return "Water Wave";
                    case 25:
                        return "Vulnerability";
                    case 26:
                        return "Earth Wave";
                    case 27:
                        return "Enfeeble";
                    case 28:
                        return "Teleother Lumbridge";
                    case 29:
                        return "Fire Wave";
                    case 30:
                        return "Entangle";
                    case 31:
                        return "Stun";
                    case 32:
                        return "Teleother Falador";
                    case 33:
                        return "Teleport Block";
                }
            } else if (player.getSettings().getMagicType() == 2) {//Ancient
                switch (spellIndex) {
                    case 35:
                        return "Smoke Rush";
                    case 36:
                        return "Shadow Rush";
                    case 37:
                        return "Blood Rush";
                    case 38:
                        return "Ice Rush";
                    case 39:
                        return "Miasmic Rush";
                    case 40:
                        return "Smoke Burst";
                    case 41:
                        return "Shadow Burst";
                    case 42:
                        return "Blood Burst";
                    case 43:
                        return "Ice Burst";
                    case 44:
                        return "Miasmic Burst";
                    case 45:
                        return "Smoke Blitz";
                    case 46:
                        return "Shadow Blitz";
                    case 47:
                        return "Blood Blitz";
                    case 48:
                        return "Ice Blitz";
                    case 49:
                        return "Miasmic Blitz";
                    case 50:
                        return "Smoke Barrage";
                    case 51:
                        return "Shadow Barrage";
                    case 52:
                        return "Blood Barrage";
                    case 53:
                        return "Ice Barrage";
                    case 54:
                        return "Miasmic Barrage";
                }
            } else if (player.getSettings().getMagicType() == 3) {//Lunar

            }
        } else if (killer instanceof NPC) {
            switch (spellIndex) {
                case 0:
                    return "Wind Strike";
                case 1:
                    return "Confuse";
                case 2:
                    return "Water Strike";
                case 3:
                    return "Earth Strike";
                case 4:
                    return "Weaken";
                case 5:
                    return "Fire Strike";
                case 6:
                    return "Wind Bolt";
                case 7:
                    return "Curse";
                case 8:
                    return "Bind";
                case 9:
                    return "Water Bolt";
                case 10:
                    return "Earth Bolt";
                case 11:
                    return "Fire Bolt";
                case 12:
                    return "Crumble Undead";
                case 13:
                    return "Wind Blast";
                case 14:
                    return "Water Blast";
                case 15:
                    return "Iban Blast";
                case 16:
                    return "Snare";
                case 17:
                    return "Magic Dart";
                case 18:
                    return "Earth Blast";
                case 19:
                    return "Fire Blast";
                case 20:
                    return "Saradomin Strike";
                case 21:
                    return "Claws of Guthix";
                case 22:
                    return "Flames of Zamorak";
                case 23:
                    return "Wind Wave";
                case 24:
                    return "Water Wave";
                case 25:
                    return "Vulnerability";
                case 26:
                    return "Earth Wave";
                case 27:
                    return "Enfeeble";
                case 28:
                    return "Teleother Lumbridge";
                case 29:
                    return "Fire Wave";
                case 30:
                    return "Entangle";
                case 31:
                    return "Stun";
                case 32:
                    return "Teleother Falador";
                case 33:
                    return "Teleport Block";
                case 35:
                    return "Smoke Rush";
                case 36:
                    return "Shadow Rush";
                case 37:
                    return "Blood Rush";
                case 38:
                    return "Ice Rush";
                case 39:
                    return "Miasmic Rush";
                case 40:
                    return "Smoke Burst";
                case 41:
                    return "Shadow Burst";
                case 42:
                    return "Blood Burst";
                case 43:
                    return "Ice Burst";
                case 44:
                    return "Miasmic Burst";
                case 45:
                    return "Smoke Blitz";
                case 46:
                    return "Shadow Blitz";
                case 47:
                    return "Blood Blitz";
                case 48:
                    return "Ice Blitz";
                case 49:
                    return "Miasmic Blitz";
                case 50:
                    return "Smoke Barrage";
                case 51:
                    return "Shadow Barrage";
                case 52:
                    return "Blood Barrage";
                case 53:
                    return "Ice Barrage";
                case 54:
                    return "Miasmic Barrage";
            }
        }
        return "Unsupported spell.";
    }

    public static final Item[][] RUNES = {
            {new Item(556, 1), new Item(558, 1)},//Wind strike
            {new Item(555, 3), new Item(557, 2), new Item(559, 1)},//Confuse.
            {new Item(555, 1), new Item(556, 1), new Item(558, 1)},//Water strike.
            {new Item(557, 2), new Item(556, 1), new Item(558, 1)},//Earth strike.
            {new Item(555, 3), new Item(557, 2), new Item(559, 1)},//Weaken.
            {new Item(554, 3), new Item(556, 1), new Item(558, 1)},//Fire strike.
            {new Item(556, 1), new Item(562, 1)},//Wind bolt.
            {new Item(555, 2), new Item(557, 3), new Item(559, 1)},//Curse.
            {new Item(557, 3), new Item(555, 3), new Item(561, 2)},//Bind.
            {new Item(555, 2), new Item(556, 2), new Item(562, 1)},//Water bolt.
            {new Item(557, 3), new Item(556, 2), new Item(562, 1)},//Earth bolt.
            {new Item(554, 3), new Item(556, 2), new Item(562, 1)},//Fire bolt.
            {new Item(557, 2), new Item(556, 2), new Item(562, 1)},//Crumble undead.
            {new Item(556, 3), new Item(560, 1)},//Wind blast.
            {new Item(555, 3), new Item(556, 3), new Item(560, 1)},//Water blast.
            {new Item(554, 5), new Item(560, 1)},//Iban blast.
            {new Item(557, 4), new Item(555, 4), new Item(561, 3)},//Snare.
            {new Item(560, 1), new Item(558, 4)},//Magic dart.
            {new Item(557, 4), new Item(556, 3), new Item(560, 1)},//Earth blast.
            {new Item(554, 5), new Item(556, 4), new Item(560, 1)},//Fire blast.
            {new Item(554, 2), new Item(565, 2), new Item(556, 4)},//Saradomin strike.
            {new Item(554, 1), new Item(565, 2), new Item(556, 4)},//Claws of guthix.
            {new Item(554, 4), new Item(565, 2), new Item(556, 1)},//Flames of zamorak.
            {new Item(556, 5), new Item(565, 1)},//Wind wave.
            {new Item(555, 7), new Item(556, 5), new Item(565, 1)},//Water wave.
            {new Item(557, 5), new Item(555, 5), new Item(566, 1)},//Vulnerability.
            {new Item(557, 7), new Item(556, 5), new Item(565, 1)},//Earth wave.
            {new Item(557, 8), new Item(555, 8), new Item(566, 1)},//Enfeeble.
            {new Item(566, 1), new Item(563, 1), new Item(557, 1)},//Teleother lumbridge.
            {new Item(554, 7), new Item(556, 5), new Item(565, 1)},//Fire wave.
            {new Item(557, 5), new Item(555, 5), new Item(561, 4)},//Entangle.
            {new Item(557, 12), new Item(555, 12), new Item(566, 1)},//Stun.
            {new Item(566, 1), new Item(555, 1), new Item(563, 1)},//Teleother Falador.
            {new Item(562, 1), new Item(563, 1), new Item(560, 1)},//Teleblock.
            {new Item(566, 2), new Item(563, 1)},//Teleother camelot.
            {new Item(562, 2), new Item(560, 2), new Item(554, 1), new Item(556, 1)},//Smoke rush
            {new Item(562, 2), new Item(560, 2), new Item(556, 1), new Item(566, 1)},//Shadow rush
            {new Item(562, 2), new Item(560, 2), new Item(565, 1)},//Blood rush
            {new Item(562, 2), new Item(560, 2), new Item(555, 2)},//Ice rush
            {new Item(562, 2), new Item(557, 1), new Item(566, 1)},//Miasmic rush
            {new Item(562, 4), new Item(560, 2), new Item(554, 2), new Item(556, 2)},//Smoke burst
            {new Item(562, 4), new Item(560, 2), new Item(556, 2), new Item(566, 2)},//Shadow burst
            {new Item(562, 4), new Item(560, 2), new Item(565, 2)},//Blood burst
            {new Item(562, 4), new Item(560, 2), new Item(555, 4)},//Ice burst
            {new Item(562, 4), new Item(557, 2), new Item(566, 2)},//Miasmic burst
            {new Item(560, 2), new Item(565, 2), new Item(554, 2), new Item(556, 2)},//Smoke blitz
            {new Item(560, 2), new Item(565, 2), new Item(556, 2), new Item(566, 2)},//Shadow blitz
            {new Item(560, 2), new Item(565, 4)},//Blood blitz
            {new Item(560, 2), new Item(565, 2), new Item(555, 3)},//Ice blitz
            {new Item(565, 2), new Item(557, 3), new Item(566, 3)},//Miasmic blitz
            {new Item(560, 4), new Item(565, 2), new Item(554, 4), new Item(556, 4)},//Smoke barrage
            {new Item(560, 4), new Item(565, 2), new Item(556, 4), new Item(566, 3)},//Shadow barrage
            {new Item(560, 4), new Item(565, 4), new Item(566, 1)},//Blood barrage
            {new Item(560, 4), new Item(565, 2), new Item(555, 6)},//Ice barrage
            {new Item(565, 4), new Item(557, 4), new Item(566, 4)},//Miasmic barrage
    };

    public static void addMagicXp(Player p, Entity target, int hit, int index, boolean baseXp) {
        double xp = 0;
        switch (index) {
            case 0:
                xp = 5.5;
                break; // Wind strike.
            case 1:
                xp = 7.5;
                break; // Water strike
            case 2:
                xp = 9.5;
                break; // Earth strike.
            case 3:
                xp = 11.5;
                break; // Fire strike.
            case 4:
                xp = 13.5;
                break; // Wind bolt.
            case 5:
                xp = 16.5;
                break; // Water bolt.
            case 6:
                xp = 19.5;
                break; // Earth bolt.
            case 7:
                xp = 21.5;
                break; // Fire bolt.
            case 8:
                xp = 25.5;
                break; // Wind blast.
            case 9:
                xp = 28.5;
                break; // Water blast.
            case 10:
                xp = 31.5;
                break; // Earth blast.
            case 11:
                xp = 34.5;
                break; // Fire blast.
            case 12:
                xp = 36.0;
                break; // Wind wave.
            case 13:
                xp = 37.5;
                break; // Water wave.
            case 14:
                xp = 40.0;
                break; // Earth wave.
            case 15:
                xp = 42.5;
                break; // Fire wave.
            case 32:
                xp = 24.5;
                break; // Crumble undead.
            case 33:
                xp = 30.0;
                break; // Slayer dart.
            case 34:
                xp = 30.0;
                break; // Bind.
            case 35:
                xp = 30.0;
                break; // Iban blast.
            case 36:
                xp = 60.0;
                break; // Snare.
            case 37:
                xp = 61.0;
                break; // Saradomin strike.
            case 38:
                xp = 61.0;
                break; // Claws of Guthix.
            case 39:
                xp = 61.0;
                break; // Flames of Zamorak.
            case 40:
                xp = 89.0;
                break; // Entangle.
            case 41:
                xp = 13.0;
                break; // Confuse.
            case 42:
                xp = 21.0;
                break; // Weaken.
            case 43:
                xp = 29.0;
                break; // Curse.
            case 44:
                xp = 83.0;
                break; // Enfeeble.
            case 45:
                xp = 90.0;
                break; // Stun.
            case 46:
                xp = 76.0;
                break; // Vulnerability.
            case 47:
                xp = 80.0;
                break; // Teleblock.
            case 16:
                xp = 30.0;
                break; // Smoke rush.
            case 17:
                xp = 31.0;
                break; // Shadow rush.
            case 18:
                xp = 33.0;
                break; // Blood rush.
            case 19:
                xp = 34.0;
                break; // Ice rush.
            case 20:
                xp = 36.0;
                break; // Smoke burst.
            case 21:
                xp = 37.0;
                break; // Shadow burst.
            case 22:
                xp = 39.0;
                break; // Blood burst.
            case 23:
                xp = 40.0;
                break; // Ice burst.
            case 24:
                xp = 42.0;
                break; // Smoke blitz.
            case 25:
                xp = 43.0;
                break; // Shadow blitz.
            case 26:
                xp = 45.0;
                break; // Blood blitz.
            case 27:
                xp = 46.0;
                break; // Ice blitz.
            case 28:
                xp = 48.0;
                break; // Smoke barrage.
            case 29:
                xp = 48.0;
                break; // Shadow barrage.
            case 30:
                xp = 51.0;
                break; // Blood barrage.
            case 31:
                xp = 52.0;
                break; // Ice barrage.
            case 48:
                xp = 36.0;
                break; // Miasmic rush.
            case 49:
                xp = 42.0;
                break; // Miasmic burst.
            case 50:
                xp = 48.0;
                break; // Miasmic blitz.
            case 51:
                xp = 54.0;
                break; // Miasmic barrage.
        }
        p.getSkills().addExperience(6, xp + (2 * hit));
        p.getSkills().addExperience(3, hit * 1.33);
        target.getCombatState().getDamageMap().incrementTotalDamage(p, hit);
    }

    public static int getMaxHit(Player player, int spell, int wep) {
        double damage = SPELL_MAX_HIT[spell];
		/*double damageMultiplier = 1;
		int currentMage = player.getSkills().getLevel(6);
		int mageForXp = player.getSkills().getLevelForXp(6);
		if (currentMage > mageForXp && mageForXp >= 95)
			damageMultiplier += .03 * (currentMage - 99);
		else
			damageMultiplier = 1;
		switch (wep) {
		case 18371: // Gravite Staff
			damageMultiplier += .05;
			break;
		case 4675: // Ancient Staff
		case 4710: // Ahrim's Staff
		case 4862: // Ahrim's Staff
		case 4864: // Ahrim's Staff
		case 4865: // Ahrim's Staff
		case 6914: // Master Wand
		case 8841: // Void Knight Mace
		case 13867: // Zuriel's Staff
		case 13869: // Zuriel's Staff (Deg)
			damageMultiplier += .10;
			break;
		case 15486: // Staff of Light
			damageMultiplier += .15;
			break;
		case 18355: // Chaotic Staff
			damageMultiplier += .20;
			break;
		}*/
		/*switch (c.playerEquipment[c.playerAmulet]) {
		case 18333: // Arcane Pulse
			damageMultiplier += .05;
			break;
		case 18334:// Arcane Blast
			damageMultiplier += .10;
			break;
		case 18335:// Arcane Stream
			damageMultiplier += .15;
			break;
		}
		damage *= damageMultiplier;*/
        return (int) damage;
    }

    public static boolean isAutoCasting(Entity killer) {
        if (killer.isNPC()) {
            return ((NPC) killer).getDefinition().getCombatType().equals(CombatType.MAGE);
        }
        Player player = (Player) killer;
        return player.getAttributes().isSet("autocastspell");
    }

    public static boolean isCasting(Entity killer) {
        if (killer instanceof NPC) {
            return false;//TODO
        }
        Player player = (Player) killer;
        return player.getAttributes().isSet("regularCast");
    }

    public static void endRegularCast(Player player, Entity other, boolean regularCast, boolean autoCast) {
        if (regularCast) {
            if (!autoCast) {
                MainCombat.endCombat(player, 2);
            }
            if (Location.standingOn(player, other)) {
                DumbPathFinder.generateMovement(player);
            }
            player.getAttributes().remove("regularCast");
        }
    }

    /**
     * Requirements to cast a spell
     *
     * @param player
     * @param spell
     * @return
     */
    public static boolean canCastSpell(Player player, Entity target, int spell) {
        String spellName = getSpellName(player, spell);
        if (SPELL_STAFFS[spell] > -1) {
            String staffName = ItemDefinition.forId(SPELL_STAFFS[spell]).getName();
            if (player.getEquipment().getItemInSlot(3) != SPELL_STAFFS[spell]) {
                MainCombat.endCombat(player, 1);
                player.getPacketSender().sendMessage("You need a " + staffName + " to cast this spell.");
                return false;
            }
        }
        if (spellName.equals("Saradomin Strike")) {
            if (!player.getEquipment().hasItem(2412)) {
                MainCombat.endCombat(player, 1);
                player.getPacketSender().sendMessage("You need to wear the cape of Saradomin to be able to cast Saradomin Strike.");
                return false;
            }
        }
        if (spellName.equals("Claws of Guthix")) {
            if (!player.getEquipment().hasItem(2413)) {
                MainCombat.endCombat(player, 1);
                player.getPacketSender().sendMessage("You need to wear the cape of Guthix to be able to cast Claws of Guthix.");
                return false;
            }
        }
        if (spellName.equals("Flames of Zamorak")) {
            if (!player.getEquipment().hasItem(2414)) {
                MainCombat.endCombat(player, 1);
                player.getPacketSender().sendMessage("You need to wear the cape of Zamorak to be able to cast Flames of Zamorak.");
                return false;
            }
        }
        if (player.getDuelSession() != null) {
            if (player.getDuelSession().ruleEnabled(4)) {
                MainCombat.endCombat(player, 2);
                player.getPacketSender().sendMessage("Magic combat has been disabled this duel.");
                return false;
            }
        }
        if (player.getAttributes().isSet("clansession")) {
            WarSession session = player.getAttributes().get("clansession");
            if (session != null) {
                ClanWarsData.Rules rule = session.getRules()[ClanWarsData.Groups.MAGIC.ordinal()];
                if (rule == ClanWarsData.Rules.MAGIC_OFF) {
                    MainCombat.endCombat(player, 2);
                    player.getPacketSender().sendMessage("Magic combat has been disabled in this war.");
                    return false;
                } else if (rule == ClanWarsData.Rules.BINDING_ONLY) {
                    if (!stopCastAfterFrozen(player, spell)) {
                        MainCombat.endCombat(player, 2);
                        player.getPacketSender().sendMessage("Only binding spells can be used in this war.");
                        return false;
                    }
                } else if (session.getRules()[ClanWarsData.Groups.MAGIC.ordinal()] == ClanWarsData.Rules.STANDARD_SPELLS
                        || session.getMiscRules()[3] == true) {
                    if (player.getSettings().getMagicType() != 1) {
                        MainCombat.endCombat(player, 2);
                        player.getPacketSender().sendMessage("Only standard spells can be used in this war.");
                        return false;
                    }
                }
                //Standard spells
            }
        }
        //spells that should not re-freeze until 4 second immune is over
        if (stopCastAfterFrozen(player, spell)) {
            if (!target.getCombatState().isFreezable()) {
                String say = target instanceof NPC ? "monster" : "player";
                player.getPacketSender().sendMessage("That " + say + " is already being affected by this spell.");
                MainCombat.endCombat(player, 2);
                return false;
            }
        }
        if (spell == 33 && target.getCombatState().isTeleBlocked()) { //teleblock
            player.getPacketSender().sendMessage("That player is already being affected by this spell.");
            MainCombat.endCombat(player, 2);
            return false;
        }
        return true;
    }

    public static void sendProjectile(Player killer, Entity target, int spell) {
        if (PROJECTILE_GFX[spell] == -1) {
            return;
        }
        String spellName = getSpellName(killer, spell);
        int delay = 50;
        int clientSpeed = 100;
        int startHeight = getSpellProjectileStartHeight(killer, spell);
        int endHeight = getSpellProjectileEndHeight(killer, spell);
        if (Location.isWithinDistance(killer, target, 1)) {
            clientSpeed = 70;
        } else if (Location.isWithinDistance(killer, target, 5)) {
            clientSpeed = 90;
        } else if (Location.isWithinDistance(killer, target, 8)) {
            clientSpeed = 110;
        } else {
            clientSpeed = 130;
        }
        //instant projectiles; wave spells
        if (spellName.endsWith("Wave")) {
            delay = 20;
        }
        if (spellName.equalsIgnoreCase("Teleport Block")) {
            delay = 60;
        }
        Projectiles mageProjectile = Projectiles.create(killer.getLocation(), target.getLocation(), target, PROJECTILE_GFX[spell], delay, clientSpeed, 50, startHeight, endHeight, getSpellSlope(killer, spell), 48);
        killer.executeProjectile(mageProjectile);
    }
}
