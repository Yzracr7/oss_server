package com.hyperion.game.world.entity.combat.data.range;

import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.item.ItemDefinition.DegradableType;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.impl.Equipment;

public class RangeData {

    public static boolean isRanging(Entity entity) {
        if (entity.isNPC()) {
            return ((NPC) entity).getDefinition().getCombatType().equals(CombatType.RANGE);
        }
        Player player = (Player) entity;
        int wep = player.getEquipment().getItemInSlot(3);
        switch (wep) {
            case -1:
                return false;
            case 20779: // Hunting Knife
                return false;
        }
        ItemDefinition def = ItemDefinition.DEFINITIONS[wep];
        if (def != null) {
            for (int i = 0; i < RangedWeaponData.rangeWeapons.length; i++) {
                if (def.getName().toLowerCase().contains(RangedWeaponData.rangeWeapons[i].toLowerCase())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean containsAmmo(Entity entity, int wep, int ammo) {
        if (entity.isNPC()) {
            return true;
        }
        Player player = (Player) entity;
        String wepName = ItemDefinition.DEFINITIONS[wep].getName();
        final String type = (wepName.endsWith("crossbow") || wepName.endsWith("c'bow")) ? "bolts" : wepName.equals("hand cannon") ? "cannon shots" : "arrows";
        if (wepName.contains("crystal"))
            return true;
        if ((!wepName.contains("crystal") && wepName.endsWith("bow")) || wepName.endsWith("shortbow") || wepName.endsWith("crossbow") || wepName.equals("hand cannon")) {
            if (ammo <= 0) {
                player.getPacketSender().sendMessage("You have no " + type + " left in your quiver.");
                return false;
            }
        } else if (wepName.endsWith("knife") || wepName.endsWith("dart") || wepName.contains("javelin")) {
            if (wep <= 0) {
                return false;
            }
        }
        return true;
    }

    public static boolean containsCorrectArrows(Player player, int weapon, int arrows) {
        boolean correctAmmo = false;
        ItemDefinition def = ItemDefinition.DEFINITIONS[weapon];
        String wepName = def.getName().toLowerCase();

        // Throwable
        for (String weps : RangedWeaponData.throwable) {
            if (wepName.contains(weps)) {
                return true;
            }
        }
        if (def.getName().contains("crystal")) {
            return true;
        }
        RangedWeaponData.BowData rangedDef = RangedWeaponData.BowData.forWeapon(weapon);
        if (rangedDef == null) {
            return false;
        }
        int[] arrows_allowed = rangedDef.getArrowsAllowed(weapon);
        if (arrows_allowed != null) {
            for (int i = 0; i < arrows_allowed.length; i++) {
                if (arrows_allowed[i] == arrows) {
                    return true;

                }
            }
        }

        return correctAmmo;
    }

    public static int getDrawBack(Player player, int weapon, int ammo) {
        ItemDefinition def = ItemDefinition.DEFINITIONS[weapon];
        if (def == null || def.getName() == null) {
            return -1;
        }
        String wepName = def.getName().toLowerCase();

        // Throwable
        boolean throwable = false;
        for (String weps : RangedWeaponData.throwable) {
            if (wepName.contains(weps)) {
                throwable = true;
            }
        }
        if (throwable) {
            if (wepName.endsWith("javelin")) {
                return 1855;
            }
            for (int i = 0; i < RangedWeaponData.KNIFE_DB_GFX.length; i++) {
                if (weapon == RangedWeaponData.KNIFE_DB_GFX[i][0]) {
                    return RangedWeaponData.KNIFE_DB_GFX[i][1];
                }
            }
            for (int i = 0; i < RangedWeaponData.DART_DB_GFX.length; i++) {
                if (weapon == RangedWeaponData.DART_DB_GFX[i][0]) {
                    return RangedWeaponData.DART_DB_GFX[i][1];
                }
            }
        } else {
            if (weapon >= 4212 && weapon <= 4223) {
                return 250;
            }
            switch (weapon) {
                case 11235://Dark bow
                    for (int i = 0; i < RangedWeaponData.ARROW_DB_GFX.length; i++) {
                        if (ammo == RangedWeaponData.ARROW_DB_GFX[i][0]) {
                            return RangedWeaponData.DOUBLE_ARROW_DB_GFX[i];
                        } else if (ammo == 11212) {
                            return 1111;
                        }
                    }
                    break;
            }
            for (int i = 0; i < RangedWeaponData.ARROW_DB_GFX.length; i++) {
                if (ammo == RangedWeaponData.ARROW_DB_GFX[i][0]) {
                    return RangedWeaponData.ARROW_DB_GFX[i][1];
                }
            }
        }
        return -1;
    }

    public static int getProjectileGfx(Player player, int weapon, int ammo, boolean spec) {
        ItemDefinition def = ItemDefinition.DEFINITIONS[weapon];
        if (def.getName() == null)
            return -1;

        String wepName = def.getName().toLowerCase();

        // Throwable
        boolean throwable = false;
        for (String weps : RangedWeaponData.throwable) {
            if (wepName.contains(weps)) {
                throwable = true;
            }
        }
        if (throwable) {
            for (int i = 0; i < RangedWeaponData.KNIFE_DB_GFX.length; i++) {
                if (weapon == RangedWeaponData.KNIFE_DB_GFX[i][0]) {
                    return RangedWeaponData.KNIFE_DB_GFX[i][2];
                }
            }
        }

        if (weapon >= 4212 && weapon <= 4223) {
            System.out.println("weapon is crystal " + wepName);
            return 250;
        }

        if (weapon == 6522) {// obby rings
            return 442;
        }
        if (wepName.contains("morrigan's")) {
            if (wepName.endsWith("javelin")) {
                return 1837;
            }
        }

        if (spec) {
            if (ammo == 11212) {
                return 1099;
            }
        }
        if (ammo == 11212) {
            return 1120;
        }
        for (int i = 0; i < RangedWeaponData.ARROW_DB_GFX.length; i++) {
            if (ammo == RangedWeaponData.ARROW_DB_GFX[i][0]) {
                return RangedWeaponData.ARROW_PROJ_GFX[i];
            }
        }
        if (ammo == 4740) {// Bolt Racks | broad bolt
            return 27;
        }
        for (int i = 0; i < RangedWeaponData.BOLTS.length; i++) {
            if (RangedWeaponData.BOLTS[i] == ammo) {
                return 27;
            }
        }
        for (int i = 0; i < RangedWeaponData.ARROW_DB_GFX.length; i++) {
            if (ammo == RangedWeaponData.ARROW_DB_GFX[i][0]) {
                return RangedWeaponData.ARROW_PROJ_GFX[i];
            }
        }
        return -1;
    }

    public static boolean ammoUsesArrowSlot(Entity killer, int bow) {
        if (killer != null) {
            if (killer.isNPC()) {
                return true;
            }
        }
        ItemDefinition def = ItemDefinition.DEFINITIONS[bow];
        if (def == null || def.getName() == null)
            return true;
        String bowName = def.getName().toLowerCase();
        for (int i = 0; i < RangedWeaponData.RANGED_STR_DATA.length; i++) {
            String data = (String) RangedWeaponData.RANGED_STR_DATA[i][0][0];
            data = data.toLowerCase();
            if (bowName.startsWith(data) || bowName.equals(data) || bowName.contains(data) || bowName.endsWith(data)) {
                return false;
            }
        }
        return true;
    }

    public static int getMinimumBowDamage(int wep) {
        switch (wep) {
            case 11235:
                return 8;
        }
        return 0;
    }

    public static int getBoltSpecialGfx(int arrow) {
        int gfx = -1;
        switch (arrow) {
            case 9244:// Dragon.
                return 756;
            case 9243:// Diamond.
                return 758;
            case 9236: // Opal.
                return 749;
            case 9237: // Jade.
                return 756;
            case 9238: // Pearl.
                return 750;
            case 9239: // Topaz.
                return 757;
            case 9240: // Sapphire.
                return 751;
            case 9241: // Emerald.
                return 752;
            case 9242: // Ruby
                return 754;
            case 9245: // Onyx.
                return 753;
        }
        return gfx;
    }

    public static boolean isDoubleFire(int wep) {
        switch (wep) {
            case 11235:
                return true;
        }
        return false;
    }
}
