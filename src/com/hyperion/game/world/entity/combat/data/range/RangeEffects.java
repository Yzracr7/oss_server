package com.hyperion.game.world.entity.combat.data.range;

import com.hyperion.game.item.Item;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.item.ItemDefinition.DegradableType;
import com.hyperion.game.item.ItemDefinition.WeaponDefinition.RangedCombatData;
import com.hyperion.game.tickable.impl.PoisonTask;
import com.hyperion.game.world.TeleportAreaLocations;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
import com.hyperion.game.world.grounditems.GroundItem;
import com.hyperion.game.world.grounditems.GroundItemManager;
import com.hyperion.utility.NumberUtils;

public class RangeEffects extends RangeData {

    public static void applyAmmoSaving(Player player, int cape) {
        boolean accumulator = (cape == 10499);
        boolean attractor = (cape == 10498);

        if (attractor || accumulator) {
            int chance = NumberUtils.random(8);
            if ((accumulator && chance <= 1) || (attractor && chance <= 3)) {
                player.getAttributes().remove("ammosave");
                return;
            } else {
                player.getAttributes().set("ammosave", true);
            }
        } else {
            player.getAttributes().remove("ammosave");
        }
    }

    public static void executeAmmoRemoving(Player player, int wep) {
        if (player.getAttributes().isSet("ammosave")) {
            return;
        }
        boolean ammoSlot = false;
        ItemDefinition bow_def = ItemDefinition.DEFINITIONS[wep];

        if (bow_def.getName().toLowerCase().contains("crystal"))
            return;
        if (bow_def.getName().contains("bow") && !bow_def.getName().contains("crystal")) {
            ammoSlot = true;
        } else if (bow_def.getName().contains("eercull")) {
            ammoSlot = true;
        }
        boolean throwing = !ammoSlot;

        boolean darkBow = false;
        switch (wep) {
            case 11235:
                darkBow = true;
                break;
        }

        if (throwing) {
            Item weapon = player.getEquipment().get(3);
            if (bow_def.getDegradableType() != null) {
                if (bow_def.getDegradableType().equals(DegradableType.crystal_bow)) {
                    return;
                }
            }
            if (weapon != null) {
                int totalAmount = weapon.getCount();
                weapon.setItemAmount(totalAmount - 1);
                if (weapon.getCount() <= 0) {
                    player.getEquipment().set(null, 3, true);
                    player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
                    player.getBonuses().refresh();
                    Equipment.setWeapon(player, true);
                    MainCombat.endCombat(player, 2);
                    player.getPacketSender().sendMessage("You've run out of ammo!");
                } else {
                    player.getEquipment().refresh(weapon, 3);
                }
            }
        } else {
            Item ammo = player.getEquipment().get(13);
            if (ammo != null) {
                int totalAmount = ammo.getCount();
                ammo.setItemAmount(totalAmount - (darkBow ? 2 : 1));
                if (ammo.getCount() <= 0) {
                    player.getEquipment().set(null, 13, true);
                    MainCombat.endCombat(player, 2);
                    player.getPacketSender().sendMessage("You've run out of ammo!");
                } else {
                    player.getEquipment().refresh(ammo, 13);
                }
            }
        }
    }


    public static void executeEffects(Entity killer, Entity target, int wep, int ammo, int damage) {
        if (killer.isNPC()) {
            return;
        }
        Player player = (Player) killer;
        if (ammo == 9241) {
            if (target.getAttributes().isSet("boltpoison")) {
                if (!target.getCombatState().isPoisoned()) {
                    World.getWorld().submit(new PoisonTask(target, 6));
                    target.getAttributes().remove("boltpoison");
                }
            }
        } else if (ammo == 9245) {//Onyx e
            if (killer.getAttributes().isSet("boltheal")) {
                int heal = (int) (damage * 0.25);
                killer.heal(heal);
                killer.getAttributes().remove("boltheal");
            }
        } else if (ammo == 9242) {//Ruby
            if (killer.getAttributes().isSet("rubyspec")) {
                killer.getAttributes().remove("rubyspec");
                int hpRemove = (int) (killer.getHp() * 0.10);
                int targetRemove = (int) (target.getHp() * 0.20);
                player.getSkills().deductLevel(3, hpRemove);
                if (target instanceof Player) {
                    ((Player) target).getSkills().deductLevel(3, targetRemove);
                } else {
                    int currentHp = target.getHp();
                    target.setHp(currentHp - targetRemove);
                }
            }
        } else if (ammo == 9243) {//Diamonds
            if (target.getAttributes().isSet("diamondEffect")) {
                target.getAttributes().remove("diamondEffect");
            }
            executeSeerCulBowEffect(player, target, wep, damage);
        }
        boolean ammoSlot = false;

        ItemDefinition def = ItemDefinition.DEFINITIONS[wep];
        if (def.getName().contains("bow") && !def.getName().contains("crystal")) {
            ammoSlot = true;
        } else if (def.getName().contains("eercull")) {
            ammoSlot = true;
        }

        executeGroundArrows(player, target, wep, ammoSlot ? ammo : wep);
    }


    private static void executeSeerCulBowEffect(Entity entity, Entity target, int wep, int damage) {
        if (wep != 6724) {
            return;
        }
        if (entity.isNPC()) {
            return;
        }
        int[] skills = new int[]{
                Skills.MAGIC
        };
        int newDmg = damage / 10;
        if (target instanceof Player) {
            Player pTarget = (Player) target;
            for (int i = 0; i < skills.length; i++) {
                if (newDmg > 0) {
                    if (!target.getCombatState().isDead()) {
                        int before = pTarget.getSkills().getLevel(skills[i]);
                        pTarget.getSkills().deductLevel(skills[i], newDmg);
                        int after = before - pTarget.getSkills().getLevel(skills[i]);
                        newDmg -= after;
                    }
                } else {
                    break;
                }
            }
        }
    }

    public static int getRangeProjectileGFX(Player player) {
        //  switch (c.lastWeaponUsed) {
        //    case 861:
        //    case 12788:
        //         if (c.usingSpecial)
        //             return 249;
        //  }

        //weapon is bow ?
        //  boolean using2hBow = !player.getEquipment().getSlot(Equipment.EQUIPMENT.WEAPON.ordinal()).getDefinition().getName().contains("cross");

        int ammo = player.getEquipment().getSlot(Equipment.EQUIPMENT.ARROW.ordinal()) != null ? player.getEquipment().getSlot(Equipment.EQUIPMENT.ARROW.ordinal()).getId() : -1;

        player.sendMessage("ammo is " + ammo);
        if (player.getEquipment().hasItem(12926)) {
            // Optional<Integer> original = PoisonedWeapon.getOriginal(c.getToxicBlowpipeAmmo());
            //  int ammo = original.isPresent() ? original.get() : c.getToxicBlowpipeAmmo();
            final int[][] DARTS = {{806, 226}, {807, 227}, {808, 228}, {809, 229}, {810, 230}, {811, 231}, {11230, 1123}};
            for (int index = 0; index < DARTS.length; index++) {
                if (DARTS[index][0] == ammo) {
                    return DARTS[index][1];
                }
            }
        }
        //  if (c.dbowSpec) {
        ///  return Arrow.matchesMaterial(c.lastArrowUsed, Arrow.DRAGON) ? 1099 : 1101;
        //  }
        boolean castingMagic = false;
        if (castingMagic) {
            return -1;
        }
        //  if (c.playerEquipment[c.playerWeapon] == 9185 || c.playerEquipment[c.playerWeapon] == 11785 || c.playerEquipment[c.playerWeapon] == 21012)
        //return 27;
        int str = -1;
        //  Optional<Integer> original = PoisonedWeapon.getOriginal(c.rangeItemUsed);
        //int ammo = original.isPresent() ? original.get() : c.rangeItemUsed;
        int[][] data = {
                // KNIFES
                {863, 213}, {864, 212}, {865, 214}, {866, 216}, {867, 217}, {868, 218}, {869, 215},

                // DARTS
                {806, 226}, {807, 227}, {808, 228}, {809, 229}, {810, 230}, {811, 231},

                // JAVELINS
                {825, 200}, {826, 201}, {827, 202}, {828, 203}, {829, 204}, {830, 205}, {19484, 1301},

                // AXES
                {800, 36}, {801, 35}, {802, 37}, {803, 38}, {804, 39}, {805, 41}, {20849, 1320},

                // ARROWS
                {882, 19}, {884, 18}, {886, 20}, {888, 21}, {890, 22}, {892, 24}, {11212, 1120},

                // CHINCHOMPA
                {10033, 908}, {10034, 909}, {11959, 909},

                // OTHERS
                {6522, 442}, {4740, 27}, {4212, 249}, {4214, 249}, {4215, 249}, {4216, 249}, {4217, 249}, {4218, 249}, {4219, 249}, {4220, 249}, {4221, 249},
                {4222, 249}, {4223, 249},};
        for (int l = 0; l < data.length; l++) {
            if (ammo == data[l][0]) {
                str = data[l][1];
            }
        }
        return str;
    }

    public static void displayRangeProjectile(Player killer, Entity target, int weapon, int arrows) {
        ItemDefinition ammo_def = null;
        if (arrows != -1) {
            ammo_def = ItemDefinition.DEFINITIONS[arrows];
        }
        ItemDefinition weapon_def = ItemDefinition.DEFINITIONS[weapon];
        String wepName = weapon_def.getName().toLowerCase();
        int projectileId = RangeData.getProjectileGfx(killer, weapon, arrows, false);

        // Throwable
        boolean throwable = false;
        for (String weps : RangedWeaponData.throwable) {
            if (wepName.contains(weps)) {
                throwable = true;
            }
        }

        if (!throwable) {
            if (ammo_def != null) {
                if (ammo_def.getWeaponDefinition() != null) {
                    RangedCombatData rangedCombatData = ammo_def.getWeaponDefinition().getRangedCombatData();
                    if (rangedCombatData != null) {
                    }
                }
            }
        }
        switch (weapon) {
            case 11235:
                int airSpeed = 65;
                int startSpeed = 45;
                Projectiles projectile = Projectiles.create(killer.getLocation(), target.getLocation(), target, projectileId, startSpeed, airSpeed - 10, 50, 35, 34, 15 - 6, 64);
                killer.executeProjectile(projectile);
                //2nd proj
                Projectiles projectile2 = Projectiles.create(killer.getLocation(), target.getLocation(), target, projectileId, startSpeed, airSpeed + 5, 50, 35, 34, 15 + 3, 64);
                killer.executeProjectile(projectile2);
                return;
        }
        int slope = 15;
        int startHeight = 43;
        int endHeight = 36;
        int startSpeed = 45;
        int airSpeed = 55;
        int distance = killer.getLocation().distanceToPoint(target.getLocation());
        airSpeed = 55 + (distance * 5);
        for (int i = 0; i < RangedWeaponData.CROSSBOWS.length; i++) {
            if (weapon == RangedWeaponData.CROSSBOWS[i]) {
                airSpeed = 55;
                startSpeed = 45;
                slope = 5;
            }
        }
        if (weapon == 4734) {
            airSpeed = 55;
            startSpeed = 45;
            slope = 5;
        }
        if (weapon == 6522) {
            airSpeed = 65;
        }
        String name = ItemDefinition.forId(weapon).getName().toLowerCase();
        if (name.endsWith("dart")) {
            airSpeed = 35;
            startSpeed = 20;
            slope = 5;
        }
        if (name.equals("hand cannon")) {
            startHeight = 21;
            endHeight = 18;
            startSpeed = 25;
            airSpeed = 40;
        }
        Projectiles projectile = Projectiles.create(killer.getLocation(), target.getLocation(), target, projectileId, startSpeed, airSpeed, 50, startHeight, endHeight, slope, 86);
        projectile.setSpeedRange(killer, target, false);
        killer.executeProjectile(projectile);
    }

    public static void displayStartGraphic(Player player, int weapon, int ammo) {

    }

    private static void executeGroundArrows(Player player, Entity target, int wep, int arrow) {
        if (player.getAttributes().isSet("ammosave")) {
            player.getAttributes().remove("ammosave");
            return;
        }
        if (arrow <= -1) {
            return;
        }
        //ItemDefinition bow_def = ItemDefinition.forId(wep);
        //if(bow_def != null)
        ItemDefinition ammo_def = ItemDefinition.forId(arrow);
        if (ammo_def.getDegradableType() != null) {
            if (ammo_def.getDegradableType().equals(DegradableType.crystal_bow)) {
                return;
            }
        }
        if (ammo_def.getName().toLowerCase().contains("crystal"))
            return;
        if (arrow == 4740) {
            return;
        }
        boolean darkBow = false;
        switch (wep) {
            case 11235:
                darkBow = true;
                break;
        }
        GroundItem groundItem = new GroundItem(new Item(arrow, darkBow ? 2 : 1), TeleportAreaLocations.atKrakenArea(player.getLocation()) ? player.getLocation() : target.getLocation(), player);
        GroundItemManager.create(groundItem, player);
        player.getAttributes().remove("ammosave");
    }
}
