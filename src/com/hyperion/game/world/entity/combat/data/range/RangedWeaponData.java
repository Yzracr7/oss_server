package com.hyperion.game.world.entity.combat.data.range;

/**
 * Ranged weapon data
 *
 * @author trees
 */
public class RangedWeaponData {

    public enum BowData {

        SHORTBOW(841, new int[]{882, 884}), LONGBOW(839, new int[]{882, 884}), OAK_SHORTBOW(843, new int[]{882, 884, 886}), OAK_LONGBOW(845, new int[]{882, 884, 886}), WILLOW_SHORTBOW(849, new int[]{882, 884, 886, 888}), WILLOW_LONGBOW(847, new int[]{882, 884, 886}), MAPLE_SHORTBOW(853, new int[]{882, 884, 886, 888, 890}), MAPLE_LONGBOW(851, new int[]{882, 884, 886, 888, 890}), YEW_LONGBOW(851, new int[]{855, 884, 886, 888, 890, 892}), YEW_SHORTBOW(857, new int[]{882, 884, 886, 888, 890, 892}), MAGIC_SHORTBOW(861, new int[]{882, 884, 886, 888, 890, 892}), MAGIC_LONGBOW(859, new int[]{882, 884, 886, 888, 890, 892}), DARK_BOW(11235, new int[]{882, 884, 886, 888, 890, 892, 11212}), TWISTED_BOW(20997, new int[]{882, 884, 886, 888, 890, 892, 11212}), ARMADYL_CROSSBOW(11785, new int[]{9141, 9142, 9143, 9144, 9145, 9242, 9243, 9244, 9245, 9286, 9287, 9288, 9289, 9290, 9291, 9292, 9293, 9294, 9295, 9296, 9297, 9298, 9299, 9300, 9301, 9302, 9304, 9305, 9306, 9335, 9336, 9337, 9338, 9339, 9340, 9341, 9342}), DRAGON_HUNTER_CROSSBOW(21012, new int[]{9141, 9142, 9143, 9144, 9145, 9242, 9243, 9244, 9245, 9286, 9287, 9288, 9289, 9290, 9291, 9292, 9293, 9294, 9295, 9296, 9297, 9298, 9299, 9300, 9301, 9302, 9304, 9305, 9306, 9335, 9336, 9337, 9338, 9339, 9340, 9341, 9342}), RUNE_CROSSBOW(9185, new int[]{9141, 9142, 9143, 9144, 9145, 9242, 9243, 9244, 9245, 9286, 9287, 9288, 9289, 9290, 9291, 9292, 9293, 9294, 9295, 9296, 9297, 9298, 9299, 9300, 9301, 9302, 9304, 9305, 9306, 9335, 9336, 9337, 9338, 9339, 9340, 9341, 9342}), ADAMANT_CROSSBOW(9183, new int[]{9140, 9141, 9142, 9143, 9145}), MITHRIL_CROSSBOW(9181, new int[]{9140, 9141, 9142, 9145}), STEEL_CROSSBOW(9185, new int[]{9140, 9141, 9145}), IRON_CROSSBOW(9185, new int[]{9140, 9145}), CROSSBOW(837, new int[]{877, 9145});

        int bow_id;
        int[] ammos;

        BowData(int bow_id, int[] ammos) {
            this.bow_id = bow_id;
            this.ammos = ammos;
        }

        public static BowData forWeapon(int id) {
            for (BowData bow : BowData.values()) {
                if (bow.getBow_id() == id) {
                    return bow;
                }
            }
            return null;
        }

        public int[] getArrowsAllowed(int id) {
            for (BowData bow : BowData.values()) {
                if (bow.getBow_id() == id) {
                    return bow.getAmmos();
                }
            }
            return null;
        }

        public int getBow_id() {
            return bow_id;
        }

        public int[] getAmmos() {
            return ammos;
        }
    }

    public static final Object[][][] RANGED_STR_DATA = {{{"red chinchompa"}}, {{"chinchompa"}},
            /* knives */
            {{"Bronze knife"}}, {{"Iron knife"}}, {{"steel knife"}}, {{"black knife"}}, {{"mithril knife"}}, {{"adamant knife"}}, {{"rune knife"}},
            /* Darts */
            {{"bronze dart"}}, {{"iron dart"}}, {{"steel dart"}}, {{"black dart"}}, {{"mithril dart"}}, {{"adamant dart"}}, {{"rune dart"}}, {{"dragon dart"}},
            /* Javelin */
            {{"bronze javelin"}}, {{"steel javelin"}}, {{"black javelin"}}, {{"mithril javelin"}}, {{"adamant javelin"}}, {{"rune javelin"}}, {{"morrigan's javelin"}},
            /* tzhaar */
            {{"toktz-xil-ul"}},
            /* thrownaxe */
            {{"bronze thrownaxe"}}, {{"iron thrownaxe"}}, {{"steel thrownaxe"}}, {{"mithril thrownaxe"}}, {{"adamnt thrownaxe"}}, {{"rune thrownaxe"}}, {{"morrigan's throwing axe"}},
            /* Misc */
            {{"crystal"}}, {{"crystal bow"}},};
    protected static final int[] CROSSBOWS = {9174, 9176, 9177, 9179, 9181, 9183, 9185, 18607};
    protected final static String[] rangeWeapons = {"c'bow", "bow", "crossbow", "knife", "dart", "javelin", "throwing axe", "xil-ul", "ballista"};
    static final int[][] ARROW_DB_GFX = {{882, 18}, {884, 19}, {886, 20}, {888, 21}, {890, 22}, {892, 24}, {11212, 1116}};
    static final int[][] DART_DB_GFX = {{806, 232, 226}, {807, 233, 227}, {808, 234, 228}, {809, 234, 229}, {810, 235, 230}, {811, 233, 227}};

    static final int[][] KNIFE_DB_GFX = {{864, 219, 212}, {863, 220, 213}, {865, 221, 214}, {869, 222, 215}, {866, 223, 216}, {867, 224, 217}, {868, 225, 218}};
    static final int[] DOUBLE_ARROW_DB_GFX = {1104, 1105, 1105, 1107, 1108, 1109};
    static final int[] ARROW_PROJ_GFX = {10, 11, 12, 13, 14, 15};
    static final int[] BOLTS = {9140, 9141, 9142, 9143, 9144, 877, 9335, 9336, 9337, 9338, 9339, 9340, 9341, 9342, 9236, 9237, 9238, 9239, 9240, 9241, 9242, 9243, 9244, 9245, 13280};
    final static String[] throwable = {"knife", "dart", "javelin", "throwing axe", "xil-ul"};

}
