package com.hyperion.game.world.entity.masks;

import com.hyperion.game.world.entity.player.Player;


/**
 * Represents a single chat message.
 * @author Graham Edgecombe
 *
 */
public class ChatMessage {
	
	private Player player;
	private int colour;
	private int numChars;
	private String chatText;
	private int effect;
	private byte[] packed;
	
	public ChatMessage(Player player, int colour, int numChars, String chatText, int effect, byte[] packed) {
		this.player = player;
		this.colour = colour;
		this.numChars = numChars;
		this.chatText = chatText;
		this.effect = effect;
		this.packed = packed;
	}
	
	public int getColour() {
		return colour;
	}
	
	public int getNumChars() {
		return numChars;
	}
	
	public String getChatText() {
		return chatText;
	}
	
	public void setChatText(String text) {
		this.chatText = text;
	}

	public int getEffect() {
		return effect;
	}
	
	public byte[] getPacked() {
		return packed;
	}

	public byte getPacked(int i) {
		return packed[i];
	}

	public Player getPlayer() {
		return player;
	}
}
