package com.hyperion.game.world.entity.masks;

import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.IncomingHit;

/**
 * Handles player hits.
 *
 * @author Graham
 * @author trees
 */
public class Hits {

    public static enum HitType {
        NO_DAMAGE(0),
        NORMAL_DAMAGE(1),
        POISON_DAMAGE(2),
        DISEASE_DAMAGE(3),
        CRITICAL(4),
        RECOIL(5),
        VENOM(8);

        private final int type;

        private HitType(int type) {
            this.type = type;
        }

        public int getType() {
            return this.type;
        }
    }

    public static class Hit {

        private Entity owner;//Person this hit belongs to.
        private Entity target;
        private HitType type;
        private int damage;
        private int delay;
        private boolean potDelay;
        private HitPriority hitPriority;
        private IncomingHit incomingHit;
        private int combatType;

        /* Combat Types
         *
         * -1: None
         * 0: Melee
         * 1: Range
         * 2: Mage
         * 3: Cannon
         * 4: deflect
         *
         */

        /**
         * Creates a standard hit.
         *
         * @param damage The damage dealt by the hit.
         */
        public Hit(int damage, int combatType) {
            this(damage > 0 ? HitType.NORMAL_DAMAGE : HitType.NO_DAMAGE, damage, HitPriority.HIGH_PRIORITY, 0, combatType);
        }

        /**
         * Creates a standard hit.
         *
         * @param damage The damage dealt by the hit.
         */
        public Hit(int damage, int delay, int combatType) {
            this(damage > 0 ? HitType.NORMAL_DAMAGE : HitType.NO_DAMAGE, damage, HitPriority.HIGH_PRIORITY, delay, combatType);
        }

        /**
         * Creates a standard hit.
         *
         * @param damage The damage dealt by the hit.
         */
        public Hit(int damage, HitPriority hitPriority, int combatType) {
            this(damage > 0 ? HitType.NORMAL_DAMAGE : HitType.NO_DAMAGE, damage, hitPriority, 0, combatType);
        }

        /**
         * Creates a standard hit.
         *
         * @param damage The damage dealt by the hit.
         */
        public Hit(HitType type, int damage, int combatType) {
            this(type, damage, HitPriority.HIGH_PRIORITY, 0, combatType);
        }

        /**
         * Creates a standard hit.
         *
         * @param damage The damage dealt by the hit.
         */
        public Hit(int damage, HitPriority hitPriority, int delay, int combatType) {
            this(damage > 0 ? HitType.NORMAL_DAMAGE : HitType.NO_DAMAGE, damage, hitPriority, delay, combatType);
        }

        /**
         * Creates a hit.
         *
         * @param type   The hit type.
         * @param damage The damage.
         */
        private Hit(HitType type, int damage, HitPriority hitPriority, int delay, int combatType) {
            this.type = type;
            this.damage = damage;
            this.delay = delay;
            this.hitPriority = hitPriority;
            this.combatType = combatType;
        }

        public Entity getOwner() {
            return owner;
        }

        public void setOwner(Entity owner) {
            this.owner = owner;
        }

        public HitType getType() {
            return type;
        }

        public int getDamage() {
            return damage;
        }

        public HitPriority getHitPriority() {
            return hitPriority;
        }

        public int getDelay() {
            return delay;
        }

        public void setDelay(int delay) {
            this.delay = delay;
        }

        public void setDamage(int dmg) {
            this.damage = dmg;
        }

        public void setType(HitType type) {
            this.type = type;
        }

        public void setPriority(HitPriority prior) {
            this.hitPriority = prior;
        }

        public boolean isPotDelaying() {
            return potDelay;
        }

        public void setPotDelay(boolean potDelay) {
            this.potDelay = potDelay;
        }

        public Entity getTarget() {
            if (target == null) {
                target = owner;
            }
            return target;
        }

        public void setTarget(Entity target) {
            this.target = target;
        }

        public IncomingHit getIncomingHit() {
            return incomingHit;
        }

        public void setIncomingHit(IncomingHit incomingHit) {
            this.incomingHit = incomingHit;
        }

        public boolean isPotDelay() {
            return potDelay;
        }

        public int getCombatType() {
            return combatType;
        }

        public void setCombatType(int combatType) {
            this.combatType = combatType;
        }
    }

    /**
     * Holds the hit priority types.
     */
    public enum HitPriority {

        /**
         * Low priority means that when the next loop is called that checks the hit queue, if the hit
         * is not picked out, it is never displayed, used for hits such as Ring of Recoil.
         */
        LOW_PRIORITY,

        /**
         * High priority means that the hit will wait in the queue until it's displayed, used for
         * hits such as special attacks.
         */
        HIGH_PRIORITY;

    }

    public Hits() {
        hit1 = null;
        hit2 = null;
    }

    private Hit hit1;
    private Hit hit2;

    public void setHit1(Hit hit) {
        this.hit1 = hit;
    }

    public void setHit2(Hit hit) {
        this.hit2 = hit;
    }

    public int getHitDamage1() {
        if (hit1 == null) {
            return 0;
        }
        return hit1.damage;
    }

    public int getHitDamage2() {
        if (hit2 == null) {
            return 0;
        }
        return hit2.damage;
    }

    public int getHitType1() {
        if (hit1 == null) {
            return HitType.NO_DAMAGE.getType();
        }
        return hit1.type.getType();
    }

    public int getHitType2() {
        if (hit2 == null) {
            return HitType.NO_DAMAGE.getType();
        }
        return hit2.type.getType();
    }

    public void clear() {
        hit1 = null;
        hit2 = null;
    }

}