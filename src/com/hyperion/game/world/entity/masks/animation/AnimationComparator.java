package com.hyperion.game.world.entity.masks.animation;

import java.util.Comparator;

import com.hyperion.game.world.entity.masks.Animation;

/**
 * A simple class to compare the priorities of {@link Animation}s.
 * @author Stephen Andrews
 */
public class AnimationComparator implements Comparator<Animation> {

	@Override
	public int compare(Animation x, Animation y) {
		if (x.getPriority().ordinal() > y.getPriority().ordinal()) {
			return -1;
		} else if (x.getPriority().ordinal() < y.getPriority().ordinal()) {
			return 1;
		}
		
		return 0;
	}

}