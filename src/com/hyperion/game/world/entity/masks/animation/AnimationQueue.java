package com.hyperion.game.world.entity.masks.animation;
import java.util.PriorityQueue;
import java.util.Queue;

import com.hyperion.game.world.entity.masks.Animation;

/**
 * An {@link AnimationQueue} should be assigned to an {@link Entity}
 * in order to allow for animations to be both prioritized and cycled
 * through. For certain animations, a priority isn't necessary. However,
 * in combat, the order in which blocking/attacking animations occur
 * are important.
 * @author Stephen Andrews
 */
public class AnimationQueue {

	/**
	 * The priority of the {@link Animation}. High priority animations
	 * will take presidence over low priority animations. Do not change these,
	 * the ordinal value is utilized in the {@link AnimationComparator} class.
	 * @author Stephen Andrews
	 */
	public static enum AnimationPriority {
		
		/**
		 * A animation that takes no priority.
		 */
		NONE,
		
		/**
		 * A low priority animation.
		 */
		LOW,
		
		/**
		 * A high priority animation.
		 */
		HIGH;
	}
	
	/**
	 * A {@link Queue} of pending animations.
	 */
	private Queue<Animation> queue;
	
	private long lastAnimationTime;
	
	private Animation lastAnimation;
	
	
	/**
	 * Constructs an {@link AnimaitonQueue} instance.
	 */
	public AnimationQueue() {
		queue = new PriorityQueue<Animation>(new AnimationComparator());
	}
	
	/**
	 * Gets the highest priority animation to play and removes
	 * it from the queue.
	 * @return The highest priority animation. 
	 */
	public Animation getHighestPriority() {
		return queue.peek();
	}
	
	public Queue<Animation> getQueue() {
		return queue;
	}
	
	/**
	 * Adds an {@link Animation} to the queue.
	 * @param animation The animation to add.
	 */
	public void addAnimation(Animation animation) {
		queue.add(animation);
	}
	
	/**
	 * Cancels queued animations.
	 */
	public void cancelQueuedAnimations() {
		queue.clear();
	}

	public void setLastAnim(Animation anim) {
		lastAnimation = anim;
		lastAnimationTime = System.currentTimeMillis();
	}
	
	public Animation getLastAnim() {
		return lastAnimation;
	}
	public void setLastAnimTime(long time) {
		lastAnimationTime = time;
	}
	
	public long getLastAnimTime() {
		return lastAnimationTime;
	}
}