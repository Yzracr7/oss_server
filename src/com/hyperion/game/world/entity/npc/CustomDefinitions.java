package com.hyperion.game.world.entity.npc;

import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;

public class CustomDefinitions {

	// Todo: npc combat deifnitions for many, maybe block anims
	// Ork, Zammy spiritual warrior, ALL spiritual rangers....
	// /ALL boss rooms except bandos need to be done. Find combat scripts from
	// 742
	// altars
	// Objects to tele into rooms:
	// http://www.rune-server.org/runescape-development/rs-503-client-server/snippets/533241-fully-objects-godwars-you-need-add-self-player-have-fun.html
	// Killcount + doors(already sort of added, just not npc ids.
	public final static int getGodwarsAttackAnim(int npc) {
		switch (npc) {
		case 6213:
		case 6212:
			return 6547;
		case 6219:
		case 6254:
		case 6255:
		case 6256:
		case 6257:
		case 6258:
		case 6214:
			return 806;
		case 6216:
			return 1582;
		case 6218:
			return 4300;
		case 6221:
			return 811;

		case 6229:
		case 6230:
		case 6231:
		case 6232:
		case 6233:
		case 6234:
		case 6235:
		case 6236:
		case 6237:
		case 6238:
		case 6239:
		case 6240:
		case 6241:
		case 6242:
		case 6243:
		case 6244:
		case 6245:
		case 6246:
			return 6953;
		case 6210:
			return 6581;
		case 6211:
			return 169;
		case 6268:
			return 2935;
		case 6269:
		case 6270:
			return 4652;

		case 6271:
		case 6272:
		case 6274:
			return 4320;
		case 6279:
		case 6280:
		case 6283:
			return 6184;
		case 6276:
		case 6277:
			return 4320;
		case 6275:
			return 164;
		case 6282:
			return 6188;
		default:
			return -1;
		}
	}

	public final static int getGodwarsDefenceAnim(int npc) {
		return -1;
	}

	public final static int getGodwarsDeathAnim(int npc) {
		switch (npc) {
		case 6203:
			return 6946;
		case 6204:
		case 6206:
		case 6208:
			return 67;

		case 6282:
			return 6182;
		case 6275:
			return 167;
		case 6276:
		case 6277:
			return 4321;
		case 6279:
		case 6280:
		case 6283:
			return 6182;
		case 6211:
			return 172;
		case 6212:
			return 6537;
		case 6219:
		case 6221:
		case 6254:
		case 6255:
		case 6256:
		case 6257:
		case 6258:
		case 6214:
			return 0x900;
		case 6216:
			return 1580;
		case 6218:
			return 4302;
		case 6268:
			return 2938;
		case 6269:
		case 6270:
			return 4653;
		case 6229:
		case 6230:
		case 6231:
		case 6232:
		case 6233:
		case 6234:
		case 6235:
		case 6236:
		case 6237:
		case 6238:
		case 6239:
		case 6240:
		case 6241:
		case 6242:
		case 6243:
		case 6244:
		case 6245:
		case 6246:
			return 6956;
		case 6210:
			return 6576;
		case 6271:
		case 6272:
		case 6274:
			return 4321;
		default:
			return -1;
		}
	}

	/**
	 * @param id
	 * @return
	 */
	public static CombatType getCombatType(int id) {
		switch(id) {
		case 6250: // growler:
			return CombatType.MAGE;
		case 6252: // bree
			return CombatType.RANGE;
		}
		return null;
	}
}
