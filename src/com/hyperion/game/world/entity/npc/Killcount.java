package com.hyperion.game.world.entity.npc;

import com.hyperion.game.world.entity.player.Player;

public class Killcount {
	
	public static void handleKill(Player player, NPC npc) {
		int npcId = npc.getId();
		for (int i = 0; i < player.getVariables().killcountNpcs.length; i++) {
			if (player.getVariables().killcountNpcs[i] == npcId) {
				player.getVariables().getKillcount()[npcId] += 1;
				player.getPacketSender().sendMessage("Your " + npc.getDefinition().getName() + " kill count is: " + "<col=FF0000>" + player.getVariables().getKillcount()[npcId] + "</col>" + ".");
			}
		}
	}

}
