package com.hyperion.game.world.entity.npc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

import com.hyperion.cache.json.impl.NpcNodeLoader;
import com.hyperion.game.Constants;
import com.hyperion.game.content.QuestTab;
import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.content.clanchat.ClanChat;
import com.hyperion.game.content.clanchat.loot.LootsharePair;
import com.hyperion.game.content.clues.ClueScrollManagement;
import com.hyperion.game.content.godwars.Godwars;
import com.hyperion.game.content.minigames.barrows.Barrows;
import com.hyperion.game.content.minigames.warriorsguild.WarriorsGuild;
import com.hyperion.game.content.miniquests.impl.DesertTreasure;
import com.hyperion.game.content.miniquests.impl.MonkeyMadness;
import com.hyperion.game.content.miniquests.impl.MountainDaughter;
import com.hyperion.game.content.skills.prayer.BoneBurying;
import com.hyperion.game.content.skills.slayer.SlayerTasks;
import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.tickable.impl.DeathTick;
import com.hyperion.game.tools.editors.npc.NPCItemDrop;
import com.hyperion.game.tools.editors.npc.NpcDropController;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.TeleportAreaLocations;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.Hits.HitPriority;
import com.hyperion.game.world.entity.masks.Hits.HitType;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
import com.hyperion.game.world.grounditems.GroundItem;
import com.hyperion.game.world.grounditems.GroundItemManager;
import com.hyperion.utility.NumberUtils;
import com.hyperion.utility.Utils;

/**
 * <p>
 * Represents a non-player character in the in-game world.
 * </p>
 *
 * @author Graham Edgecombe
 * @author Nando
 */
public class NPC extends Entity {

	public boolean isHealing;
	private int id, hp;
	public long last_shout;
	private NpcNodeLoader spawn;
	private NPCDefinition definition;
	private int transformationId = -1;
	private Location spawnLocation = Constants.RESPAWN_LOCATION;
	private Location minimumCoords = Location.create(0, 0, 0);
	private Location maximumCoords = Location.create(0, 0, 0);
	private boolean walkingHome = false;
	private int direction = 6;
	private boolean isRandomWalking = false;

	public NPC(int id) {
		super();
		this.id = id;
		this.definition = NPCDefinition.forId(id);
		this.hp = definition.getHitpoints();
	}

	@Override
	public int getClientIndex() {
		return this.getIndex();
	}

	@Override
	public boolean isNPC() {
		return true;
	}

	@Override
	public boolean isPlayer() {
		return false;
	}

	@Override
	public Location getCentreLocation() {
		return Location.create((getLocation().getX() + getDefinition().getSize() / 2), getLocation().getY() + (getDefinition().getSize() / 2));
	}

	@Override
	public int getSize() {
		return this.getDefinition().getSize();
	}

	@Override
	public boolean isDestroyed() {
		if (getIndex() == -1)
			return true;
		return World.getWorld().getNPC(this.getIndex()) == null;
	}

	public void processQueuedHits() {
		/*
		 * Gets the next two hits from the queue.
		 */
		List<Hit> hits = getCombatState().getHitQueue();
		Hit first = null;
		if (hits.size() > 0) {
			for (int i = 0; i < hits.size(); i++) {
				Hit hit = hits.get(i);
				if (hit.getDelay() < 1) {
					first = hit;
					hits.remove(hit);
					break;
				}
			}
		}
		if (first != null) {
			setPrimaryHit(first);
			sendDamage(first, first.getType() == HitType.POISON_DAMAGE);
			getUpdateFlags().flag(UpdateFlag.HIT);
		}
		Hit second = null;
		if (hits.size() > 0) {
			for (int i = 0; i < hits.size(); i++) {
				Hit hit = hits.get(i);
				if (hit.getDelay() < 1) {
					second = hit;
					hits.remove(hit);
					break;
				}
			}
		}
		if (second != null) {
			setSecondaryHit(second);
			sendDamage(second, second.getType() == HitType.POISON_DAMAGE);
			getUpdateFlags().flag(UpdateFlag.HIT_2);
		}
		if (hits.size() > 0) {// tells us we still have more hits
			Iterator<Hit> hitIt = hits.iterator();
			while (hitIt.hasNext()) {
				Hit hit = hitIt.next();
				if (hit.getDelay() > 0) {
					hit.setDelay(hit.getDelay() - 1);
				}
				if (hit.getHitPriority() == HitPriority.LOW_PRIORITY) {
					hitIt.remove();
				}
			}
		}
	}

	@Override
	public void inflictDamage(Hit hit, boolean poison) {
		if (hit.getIncomingHit() != null) {
			hit.setOwner(hit.getIncomingHit().getAttacker());
		}
		boolean damageOverZero = hit.getDamage() > 0;
		if (hit.getDamage() > this.hp) {
			hit.setDamage(this.hp);
		}
		if (damageOverZero && hit.getDamage() == 0) {
			hit.setType(HitType.NO_DAMAGE);
		}
		if (poison) {
			hit.setType(HitType.POISON_DAMAGE);
		}
		if (hit.getDamage() <= 0) {
			hit.setDamage(0);
			hit.setType(HitType.NO_DAMAGE);
		}
		if (getCombatState().getHitQueue().size() >= 4) {
			hit = new Hit(hit.getDamage(), HitPriority.LOW_PRIORITY, hit.getCombatType());// if
			// multiple
			// people
			// are
			// hitting
			// on
			// an
			// opponent,
			// this
			// prevents
			// hits
			// from
			// stacking
			// up
			// for
			// a
			// long
			// time
			// and
			// looking
			// off-beat
		}
		getCombatState().getHitQueue().add(hit);
	}
	
	private CopyOnWriteArrayList<NPC> ownedNPCS = new CopyOnWriteArrayList<NPC>();
	
	public CopyOnWriteArrayList<NPC> getOwnedNPCS() {
		return ownedNPCS;
	}
	
	public void addOwnedNPC(NPC npc) {
		ownedNPCS.add(npc);
		if (!npc.getAttributes().isSet("petOwner"))
			npc.getAttributes().set("owner", this);
	}

	@Override
	public void sendDamage(Hit hit, boolean poison) {
		final Entity lastHitter = hit.getOwner();
		hp -= hit.getDamage();
		if (hp <= 0) {
			hp = 0;
			if (!getCombatState().isDead()) {
				getCombatState().setDead(true);
				MainCombat.endCombat(this, 1);
				World.getWorld().submit(new Tickable(3) {
					@Override
					public void execute() {
						this.stop();
						if (getId() == 6611) {
							if (getTransformationId() == 6612) {
								animate(Animation.create(getDeathAnimation(), AnimationPriority.HIGH));
							}
						} else {
							animate(Animation.create(getDeathAnimation(), AnimationPriority.HIGH));
						}
					}
				});
				World.getWorld().submit(new DeathTick(this, lastHitter));
			}
		}
	}

	public int getDeathAnimation() {
		if (this.id == 1158) {
			if (this.getAttributes().isSet("kqsecondform")) {
				return 6233;
			}
		}
		return getDefinition().getDeathAnimation();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTransformationId() {
		return transformationId;
	}

	public void setTransformationId(int transformationId) {
		this.transformationId = transformationId;
		this.getUpdateFlags().flag(UpdateFlag.TRANSFORM);
	}

	@Override
	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public Location getSpawnLocation() {
		return spawnLocation;
	}

	public void setSpawnLocation(Location spawnLocation) {
		this.spawnLocation = spawnLocation;
	}

	public boolean isWalkingHome() {
		return walkingHome;
	}

	public void setWalkingHome(boolean walkingHome) {
		this.walkingHome = walkingHome;
	}

	public Location getMaximumCoords() {
		return maximumCoords;
	}

	public void setMaximumCoords(Location maximumCoords) {
		this.maximumCoords = maximumCoords;
	}

	public Location getMinimumCoords() {
		return minimumCoords;
	}

	public void setMinimumCoords(Location minimumCoords) {
		this.minimumCoords = minimumCoords;
	}

	public NPCDefinition getDefinition() {
		if (definition == null) {
			definition = NPCDefinition.forId(this.id);
		}
		return definition;
	}

	public void setDefinition(int id) {
		this.definition = NPCDefinition.forId(id);
	}

	@Override
	public int getMaxHp() {
		return this.definition.getHitpoints();
	}

	@Override
	public boolean isAutoRetaliating() {
		if (this.getCombatState().isDead())
			return false;
		return hp > 0;
	}

	@Override
	public void resetVariables() {
		getCombatState().getHitQueue().clear();
		setHp(getMaxHp());
		getCombatState().getDamageMap().reset();
		getCombatState().setCharged(false);
		getCombatState().setVenged(false);
		getCombatState().setLastTarget(null);
		getCombatState().setPoisonAmount(0);
		getCombatState().setLastAttacked((long) 0);
		getCombatState().setLastVengeance((long) 0);
		getCombatState().setTbTime(0);
		getCombatState().setTarget(null);
		getCombatState().setHitDelay(1);// one tick.
		getCombatState().setEatingState(true);
		getCombatState().setDrinkingState(true);
		getCombatState().setFreezable(true);
		getCombatState().setFrozen(false);
		getCombatState().setCanMove(true);
		getCombatState().setCurrentAttacker(null);
		getCombatState().setPreviousAttacker(null);
		setTransformationId(-1);
		setWalkingHome(false);
	}

	/**
	 * Sets the spawn
	 *
	 * @param spawn
	 *            the spawn to set
	 */
	public void setSpawn(NpcNodeLoader spawn) {
		this.spawn = spawn;
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public boolean isMoveable() {
		switch (this.id) {
		case 2892:// Spinolayp
			return false;
		}
		return true;
	}

	public int getInitialPoisonDamage() {
		switch (this.id) {
		case 6998:
			return 8;
		case 1157:// Kalphire guard
			return 6;
		case 1154: // Kalphite Soldier
			return 3;

		}
		return -1;
	}

	public int getAggressiveDistance() {
		int id = this.id;
		if (this.getAttributes().isSet("pestzombie")) {
			return 64;
		}
		switch (id) {
		case 6263: // Steelwill
			return 14;
		case 6730:
			return 10;
		case 78:// Giant bats
			return 4;
		case 2036:// wildy course skeles
		case 181:// Chaos druid
			return 3;
		case 95:// wolfs at wildy course
			return 1;
		case 117:// hill giants
		case 5342:// Ghost
		case 1592: // Steel Dragon
		case 1591: // Bronze Dragon
		case 54:// Black Dragon
		case 941:// Green drags
		case 4677: // Frost dragons
		case 4678:// Lava dragon
			return 4;
		case 183:// Pirates at hut
			return 1;
		}
		return 7;
	}

	public int getFollowDistance() {
		int id = this.id;
		if (TeleportAreaLocations.atArmadylChamber(getLocation())
				|| TeleportAreaLocations.atBandosChamber(getLocation())) {
			return 25;
		}
		switch (id) {
		case 239: // KBD
			return 30;
		case 319: // Corp
			return 20;
		default:
			return 11;
		}
	}

	public boolean isRoamingWilderness() {
		int id = this.id;
		switch (id) {
		case 6604:// rev imp
		case 6730:// rev knight
			return true;
		}
		return false;
	}

	public boolean isAttackable() {
		return !(definition.getHitpoints() <= 0 || definition.getCombatLevel() <= 0);
	}

	public boolean bandos_armour_checker() {
		int[] ids = { 3137, 2239, 2238, 2235, 3133, 3135, 2244, 2242, 2243, 2233, 2234, 2246, 2248, 2240, 2247, 2245,
				2249, 2241 };
		for (int index = 0; index < ids.length; index++) {
			if (this.id == ids[index])
				return true;
		}
		return false;
	}

	public boolean armadyl_armour_checker() {
		int[] ids = { 3168, 3167, 3166, 3169, 3170, 3171, 3172, 3173, 3174, 3175, 3176, 3177, 3178, 3179, 3180, 3181,
				3182, 3183 };
		for (int index = 0; index < ids.length; index++) {
			if (this.id == ids[index])
				return true;
		}
		return false;
	}

	public boolean zamorak_armour_checker() {
		int[] ids = { 3141, 3161, 3160, 3159, 6230, 6231, 6270, 6216, 6214, 6211, 6221, 6276, 6277, 6219 };
		for (int index = 0; index < ids.length; index++) {
			if (this.id == ids[index])
				return true;
		}
		return false;
	}

	public boolean saradomin_armour_checker() {
		int[] ids = { 6257, 6256, 6259, 6258, 6254, 6255, 6229, 6230, 6231, 6221, 6276, 6277, 6219 };
		for (int index = 0; index < ids.length; index++) {
			if (this.id == ids[index])
				return true;
		}
		return false;
	}

	@Override
	public void dropLoot(Entity killer) {
		Player player = killer.isPlayer() ? (Player) killer : null;
		if (player == null) {
			return;
		}
		int id = this.getId();

		if (player.getVariables().getCurrentRoundMiniquest() != null) {
			player.getVariables().getCurrentRoundMiniquest().nextRound(player, this);
			return;
		}
		if (DesertTreasure.isDtNPC(this)) {
			DesertTreasure.handleDtNPCDeath(this, player);
			return;
		}

		switch (id) {
		case 1813:
			MountainDaughter.handleFinished(player);
			return;
		case 1615: // Abyssal demon
			Achievements.increase(player, 31);
			break;
		case 8349: // TD
			Achievements.increase(player, 36);
			break;
		case 4678: // Lava dragon
			Achievements.increase(player, 42);
			break;
		case 6610: // ven
		case 6611: // vet
		case 6503: // callisto
		case 6308: // scorpia
			player.getAttributes().set("" + id, true);
			checkWildyBosses(player);
			break;
		case 4291: // Cyclops
			Achievements.increase(player, 53);
			break;
		case 912:
		case 913:
		case 914:
			player.getVariables().increaseMageArenaPoints(5);
			return;
		case 1472:
		case 477:
			MonkeyMadness.end(player, this, true, false);
			return;
		case 1672:
		case 1673:
		case 1674:
		case 1675:
		case 1676:
		case 1677:
			Barrows.handleBrothersDeath(player, this);
			return;
		case 3497:
			if (player.getVariables().getCurrentRoundMiniquest() != null) {
				player.getVariables().getCurrentRoundMiniquest().nextRound(player, this);
			} else {
				player.getVariables().setMother_killed(true);
				QuestTab.openQuestComplete(player, 6);
			}
			return;
		}

		if (getDefinition().getName().toLowerCase().equals("man")
				|| getDefinition().getName().toLowerCase().equals("woman")) {
			Achievements.increase(player, 3);

		} else if (getDefinition().getName().toLowerCase().equals("rock crab")) {
			Achievements.increase(player, 15);
		}
		Achievements.increase(player, 11);
		SlayerTasks.deduct_task(player, this);
		// PetDefinition.roll(player, this);
		if (getDefinition().getName().toLowerCase().contains("revenant")) {
			int itemId = RevenantDrops.getRevenantDrop(id);
			if (itemId != -1) {
				NPCItemDrop revDrop = new NPCItemDrop(itemId, 1, 1, 100);
				sendDrop(player, revDrop);
			}
			return;
		}

		NpcDropController drop = this.getDefinition().getDrop();
		if (drop == null) {
			player.getPacketSender().sendMessage("[NPC drop is null]");
			return;
		}
		ArrayList<NPCItemDrop> drops = drop.getDropsArray();
		if (drops == null) {
			return;
		}
		NPCItemDrop[] possibleDrops = new NPCItemDrop[drops.size()];
		int possibleDropsCount = 0;
		for (NPCItemDrop items : drops) {
			if (items.getPercentage() == 100) {// Always
				sendDrop(player, items);
			} else {
				double roll;
				boolean hasROW = false;
				if (killer.isPlayer()) {
					Player dropFor = (Player) killer;
					if (dropFor.getEquipment().getItemInSlot(Equipment.EQUIPMENT.RING.ordinal()) == 2572) {
						hasROW = true;
					}
				}
				roll = NumberUtils.random(0.0, 100.0 - (hasROW ? Utils.random(1, 10) : 0));
				if (roll <= items.getPercentage()) {
					possibleDrops[possibleDropsCount++] = items;
				}
			}
		}
		if (possibleDropsCount > 0) {
			if (player.getVariables().isEnabled() && player.getAttributes().isSet("clan_chat_id")) {
				NPCItemDrop[] actualDrops = new NPCItemDrop[possibleDropsCount];
				for (int i = 0; i < possibleDropsCount; i++) {
					actualDrops[i] = possibleDrops[i];
				}
				int clanId = player.getAttributes().getInt("clan_chat_id");
				ClanChat clan = ClanChat.getClanById(clanId);
				Objects.requireNonNull(clan);
				clan.getLootshareLadder().sort();
				List<LootsharePair> pairList = clan.getLootshareLadder()
						.fromLootshareLadder(Arrays.asList(actualDrops));
				pairList.forEach(pair -> itemFromLootshare(pair.getPlayer(), pair.getDrop().getItem()));
			} else {
				sendDrop(player, possibleDrops[NumberUtils.random(possibleDropsCount - 1)]);
			}
		}
		sendCustomDrop(player, this.definition.getCombatLevel());
	}

	private static void itemFromLootshare(Player player, Item item) {
		ClanChat.lootshareMessage(player, item);
		player.getInventory().addItem(item);
		player.getInventory().refresh();

	}

	private static void checkWildyBosses(Player player) {
		if (player.getAttributes().isSet("6610") && player.getAttributes().isSet("6611")
				&& player.getAttributes().isSet("6503") && player.getAttributes().isSet("6308"))
			Achievements.increase(player, 43);
	}

	/**
	 * This method handles dropping "custom" drops (drops which are not in the drop
	 * tables for the npc)
	 *
	 * @param player
	 *            The player to receive the custom drop
	 */
	private void sendCustomDrop(Player player, int level) {
		/** Handling the dropping of clue scrolls */
		if (Utils.percentageChance(1)) {
			int chance = Utils.random(30);
			int scrollId = -1;
			if (chance >= 29 && level >= 125) {
				scrollId = ClueScrollManagement.ELITE_SCROLL;
			} else if (chance >= 25 && chance < 29 && level >= 80) {
				scrollId = ClueScrollManagement.HARD_SCROLL;
			} else if (chance >= 20 && chance < 25 && level >= 50) {
				scrollId = ClueScrollManagement.MEDIUM_SCROLL;
			} else if (chance > 15) {
				scrollId = ClueScrollManagement.EASY_SCROLL;
			}

			if (scrollId != -1) {
				sendDrop(player, new NPCItemDrop(scrollId, 1, 1, 100));
			}
		}
		Godwars.handleKillcountNPCDeath(player, this);
	}

	private void sendDrop(Player dropFor, NPCItemDrop drop) {
		NPCItemDrop itemToDrop = new NPCItemDrop(drop.getId(), drop.getCount(), drop.getMaxCount(),
				drop.getPercentage());

		if (itemToDrop.getItem().getId() == 8844) {
			if (dropFor.getInventory().getCount(12954) >= 1 || dropFor.getEquipment().getCount(12954) == 1) {
				int id = NumberUtils.random(0, 100) > 70 ? 12954 : 8850;
				itemToDrop = new NPCItemDrop(id, drop.getCount(), drop.getMaxCount(), drop.getPercentage());

			} else {
				for (int i = 0; i < WarriorsGuild.DEFENDERS.length; i++) {
					if (dropFor.getInventory().getCount(WarriorsGuild.DEFENDERS[i]) >= 1
							|| dropFor.getEquipment().getCount(WarriorsGuild.DEFENDERS[i]) == 1) {
						int id = WarriorsGuild.DEFENDERS[i <= 0 ? 0 : i - 1];

						if (WarriorsGuild.DEFENDERS[i] == 8850 && NumberUtils.random(0, 100) > 74)
							id = 12954;
						itemToDrop = new NPCItemDrop(id, drop.getCount(), drop.getMaxCount(), drop.getPercentage());
						break;
					}
				}
			}
		}

		int maxAmount = itemToDrop.getMaxCount();
		int minAmount = itemToDrop.getCount();
		if (maxAmount == minAmount) {
			maxAmount = 0;
		}
		Item item = new Item(itemToDrop.getId(), minAmount + NumberUtils.random(maxAmount));

		// Drop Location Control
		Location dropLoc = this.getLocation();
		switch (this.getId()) {
		case 6305: // Kraken
		case 6306: // Kraken
		case 2042: // Zulrah
			dropLoc = dropFor.getLocation();
			dropFor.getPacketSender().sendMessage("<col=006622>You received a drop: "
					+ (item.getCount() > 1 ? item.getCount() + " x " : "") + item.getDefinition().getName() + "</col>");
			break;
		}

		GroundItem groundItem = new GroundItem(item, dropLoc, dropFor);

		int amountToLoop = item.getCount();
		if (item.getDefinition().getGeneralPrice() > 1 || itemToDrop.getPercentage() < 1) {
			dropFor.getPacketSender()
					.sendNewsMessage(dropFor.getName().substring(0, 1).toUpperCase() + dropFor.getName().substring(1)
							+ " received <col=556B2F>" + item.getCount() + " x "
							+ item.getDefinition().getName().substring(0, 1).toUpperCase()
							+ item.getDefinition().getName().substring(1) + " as a drop!");
		}
		if (!groundItem.getItem().getDefinition().isStackable()) {
			item.setItemAmount(1);
			groundItem = new GroundItem(item, dropLoc, dropFor);
			for (int i = 0; i < amountToLoop; i++) {
				if (BoneBurying.isItemBone(dropFor, groundItem.getId())) {
					if (dropFor.getInventory().hasItem(18337)) {
						BoneBurying.canCrushBone(dropFor, groundItem.getId());
					} else {
						GroundItemManager.create(groundItem, dropFor);
					}
				} else {
					GroundItemManager.create(groundItem, dropFor);
				}
			}
		} else {
			GroundItemManager.create(groundItem, dropFor);
		}
	}

	@Override
	public void heal(int amount) {
		this.hp += amount;
		if (hp >= this.getDefinition().getHitpoints()) {
			hp = this.getDefinition().getHitpoints();
		}
	}

	public long getLast_shout() {
		return last_shout;
	}

	public void setLast_shout(long last_shout) {
		this.last_shout = last_shout;
	}

	@Override
	public Location getDeathArea() {
		return this.spawnLocation;
	}

	public boolean isRandomWalking() {
		return isRandomWalking;
	}

	public void setRandomWalking(boolean isRandomWalking) {
		this.isRandomWalking = isRandomWalking;
	}

	public boolean isAttackable(Player player) {
		if (!player.getVariables().getOwnedNPCS().contains(this) && !getAttributes().isSet("pestzombie")
				&& !getAttributes().isSet("cavesession"))
			return false;
		return true;
	}
}
