package com.hyperion.game.world.entity.npc;

import com.hyperion.game.Constants;
import com.hyperion.utility.NumberUtils;
import com.hyperion.utility.Utils;

public class RevenantDrops {

	public static int getRevenantDrop(int id) {
		double chance = 0.0;
		switch (id) {
		case 6703:// imp
		case 6707:// gob
		case 6682:// icefiend
		case 6683:// pyrefriend
		case 6684:// hobgoblin
			chance = 0.1;
			break;
		case 6685:// vampire
		case 6686:// werewolf
		case 6687:// cyclops
			chance = 0.3;
			break;
		case 6688: // hellhound
		case 6689: // demon
		case 6696: // ork
			chance = 0.4;
			break;
		case 6691: // dbeast
		case 6998: // dragon
		case 6730: // knight
			chance = 0.6;
			break;
		}
		Double roll = NumberUtils.random(0.0, 100.0);
		if (roll <= chance) {
			return Constants.PVP_ARMOR[Utils.random(0, Constants.PVP_ARMOR.length - 1)];
		}
		return -1;
	}
}
