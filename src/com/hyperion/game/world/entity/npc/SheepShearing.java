package com.hyperion.game.world.entity.npc;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;

public class SheepShearing {

	public SheepShearing() {
		
	}
	
	public static void attemptShearing(Player player, final NPC npc) {
		if (!player.getInventory().hasItem(1735)) {
			player.getPacketSender().sendMessage("You need Shears to do that.");
			return;
		}
		if (player.getInventory().findFreeSlot() == -1) {
			player.getPacketSender().sendMessage("You don't have enough inventory space.");
			return;
		}
		if (!npc.getAttributes().isSet("sheared")) {
			player.animate(893);
			int chance = NumberUtils.random(3);
			if (chance < 1) {//success;
				npc.setTransformationId(42);
				npc.playForcedChat("BAA!");
				player.getInventory().addItem(1737);
				npc.getAttributes().set("sheared", true);
				World.getWorld().submit(new Tickable(20) {
					@Override
					public void execute() {
						npc.setTransformationId(-1);
						npc.getAttributes().remove("sheared");
						this.stop();
					}
				});
			} else {
				player.getPacketSender().sendMessage("The Sheep quickly escapes.");
			}
		}
	}
}