package com.hyperion.game.world.entity.npc;

import com.hyperion.game.item.Item;
import com.hyperion.utility.NumberUtils;

public class StaticDropTables {

	private enum HERB_DROPS {
		
		GRIMY_GUAM(new Item(199, 1), 10.0),
		GRIMY_MARRENTILL(new Item(201, 1), 10.0),
		GRIMY_TARROMIN(new Item(203, 1), 10.0),
		GRIMY_HARRALANDER(new Item(205, 1), 7.5),
		GRIMY_RANARR(new Item(207, 1), 7.5),
		GRIMY_IRIT(new Item(209, 1), 7.0),
		GRIMY_AVANTOE(new Item(211, 1), 7.0),
		GRIMY_KWUARM(new Item(213, 1), 6.0),
		GRIMY_CADANTINE(new Item(215, 1), 6.0),
		GRIMY_DWARF_WEEd(new Item(217, 1), 6.0),
		GRIMY_TOADFLAX(new Item(3049, 1), 6.0),
		GRIMY_LANTADYME(new Item(2485, 1), 6.0),
		GRIMY_SNAPDRAGON(new Item(3051, 1), 6.0),
		GRIMY_TORSTOL(new Item(219, 1), 5.0)
		;
		
		HERB_DROPS(Item item, double percentage) {
			this.item = item;
			this.percentage = percentage;
		}
		
		private Item item;
		private double percentage;
		
		public Item getItem() {
			return item;
		}
		
		public double getPercentage() {
			return percentage;
		}
	}
	
	private enum RARE_DROPS {
		MONEY_CASKET(new Item(2717, 1), 30),
		RARE_MONEY_CASKET(new Item(2720, 1), 10),
		TREASURE_CASKET(new Item(2714, 1), 12.5),
		TOOTH_HALF(new Item(985, 1), 12.5),
		LOOP_HALF(new Item(987, 1), 12.5),
		LEFT_HALF(new Item(2366, 1), 5),
		RIGHT_HALF(new Item(2368, 1), 5),
		UNCUT_DRAGONSTONE(new Item(1631, 1), 12.5)
		;
		
		RARE_DROPS(Item item, double percentage) {
			this.item = item;
			this.percentage = percentage;
		}
		
		private Item item;
		private double percentage;
		
		public Item getItem() {
			return item;
		}
		
		public double getPercentage() {
			return percentage;
		}
	}
	
	public static RARE_DROPS randomized_rare_drop() {
		int index = RARE_DROPS.values().length;
		int randomized_index = NumberUtils.random(index);
		return RARE_DROPS.values()[randomized_index];
	}
	
	public static HERB_DROPS randomized_herb_drop() {
		int index = HERB_DROPS.values().length;
		int randomized_index = NumberUtils.random(index);
		return HERB_DROPS.values()[randomized_index];
	}
	
	public static void access_rares_table(NPC npc) {
		
	}
}
