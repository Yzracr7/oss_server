package com.hyperion.game.world.entity.npc;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.player.Player;

public class TormentedDemon {
	
	private NPC tormentedDemon;
	
	private boolean[] demonPrayer;
	private int[] cachedDamage;
	private int shieldTimer;
	private int prayerTimer;
	private int fixedAmount;
	private int fixedCombatType;
	
	public TormentedDemon(NPC npc) {
		this.tormentedDemon = npc;
		demonPrayer = new boolean[3];
		cachedDamage = new int[3];
		shieldTimer = 0;
		switchPrayers(0);
		World.getWorld().submit(new Tickable(tormentedDemon, 1) {
			@Override
			public void execute() {
				if (tormentedDemon.getCombatState().isDead()) {
					return;
				}
				if (shieldTimer > 0)
					shieldTimer--;
				if (prayerTimer > 0)
					prayerTimer--;
				if (fixedAmount >= 5)
					fixedAmount = 0;
				if (prayerTimer == 0) {
					for (int i = 0; i < cachedDamage.length; i++) {
						if (cachedDamage[i] >= 31) {
							demonPrayer = new boolean[3];
							switchPrayers(i);
							cachedDamage = new int[3];
						}
					}
				}
				for (int i = 0; i < cachedDamage.length; i++) {
					if (cachedDamage[i] >= 31) {
						demonPrayer = new boolean[3];
						switchPrayers(i);
						cachedDamage = new int[3];
					}
				}
			}
		});
	}
	
	public int handleIncomingAttack(Hit hit) {
		int damage = hit.getDamage();
		IncomingHit incomingHit = hit.getIncomingHit();
		CombatType combatType = incomingHit.getType();
		Entity attacker = hit.getOwner();
		if (attacker.isPlayer()) {
			Player player = (Player) attacker;
			int wep = incomingHit.getWeapon();
			if (wep == 6746 || wep == 2402) {
				if (combatType.equals(CombatType.MELEE)) {
					if (damage > 0 && shieldTimer <= 0) {
						shieldTimer = 60;
						player.getPacketSender().sendMessage("The demon is temporarily weakened by your weapon.");
					}
				}
			}
		}
		if (shieldTimer <= 0) {// 75% of damage is absorbed
			tormentedDemon.playGraphic(1885);
			if (combatType.equals(CombatType.MELEE)) {
				if (demonPrayer[0]) {
					damage = (int) (damage * 0.25);
				} else {
					cachedDamage[0] += damage;
				}
			} else if (combatType.equals(CombatType.MAGE)) {
				if (demonPrayer[1]) {
					damage = (int) (damage * 0.25);
				} else {
					cachedDamage[1] += damage;
				}
			} else if (combatType.equals(CombatType.RANGE)) {
				if (demonPrayer[2]) {
					damage = (int) (damage * 0.25);
				} else {
					cachedDamage[2] += damage;
				}
			}
		}
		return damage;
	}
	
	private void switchPrayers(int type) {
		transform(8349 + type);
		demonPrayer[type] = true;
		resetPrayerTimer();
	}
	
	private void resetPrayerTimer() {
		prayerTimer = 16;
	}
	
	private void transform(int id) {
		tormentedDemon.setTransformationId(id);
	}
	
	public void handleDeath() {
		this.cachedDamage = new int[3];
		this.demonPrayer = new boolean[3];
		this.shieldTimer = 0;
		this.switchPrayers(0);
		this.fixedAmount = 0;
		this.fixedCombatType = 0;
	}
	
	public int getFixedAmount() {
		return this.fixedAmount;
	}
	
	public int getFixedCombatType() {
		return fixedCombatType;
	}
	
	public void setFixedCombatType(int fixedCombatType) {
		this.fixedCombatType = fixedCombatType;
	}
	
	public void setFixedAmount(int fixedAmount) {
		this.fixedAmount = fixedAmount;
	}
}
