package com.hyperion.game.world.entity.npc.impl;

import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.npc.NPC;

public abstract class AbstractNPCAttacks {

	public abstract void executeAttacks(final NPC npc, final Entity main_target);
	
	public abstract void handleEndEffects(NPC npc, Entity target, Hit hit);
	
	public abstract boolean canAttack(NPC npc, Entity target, CombatType type);
	
	public abstract Hit getDamage(NPC npc, Entity target, CombatType type);
	
	public abstract CombatType getCombatType(NPC npc, Entity target);
	
	public abstract boolean isWithinDistance(NPC npc, Entity target, CombatType type);
	
}
