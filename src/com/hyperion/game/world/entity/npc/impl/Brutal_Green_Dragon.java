package com.hyperion.game.world.entity.npc.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;
public class Brutal_Green_Dragon extends AbstractNPCAttacks {

	@Override
	public void executeAttacks(final NPC npc, final Entity target) {
		// TODO Auto-generated method stub
		CombatType type = getCombatType(npc, target);
		npc.getCombatState().setCombatType(type);
		if (!isWithinDistance(npc, target, type)) {
			return;
		} else {
			npc.getWalkingQueue().reset();
		}
		if (npc.getCombatState().getCombatCycles() > 0) {
			return;
		}
		if (!canAttack(npc, target, type)) {
			return;
		}
		int anim = npc.getDefinition().getAttackAnimation();
		int projectile = -1;
		int start_speed = 45;
		int air_speed = 85;
		int end_height = 30;
		if (type.equals(CombatType.MAGE)) {
			projectile = 136;
			anim = 6722;
		} else if (type.equals(CombatType.FIRE_BREATH)) {
			anim = 81;
			npc.playGraphic(1, 0, 100);
		}
		if (projectile != -1) {
			Projectiles mage_projectile = Projectiles.create(npc.getCentreLocation(), target.getLocation(), target, projectile, start_speed, air_speed, 50, 32, end_height);
			npc.executeProjectile(mage_projectile);
		}
		npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
		npc.animate(Animation.create(anim, AnimationPriority.HIGH));
		target.getCombatState().setCurrentAttacker(npc);
		target.getCombatState().setPreviousAttacker(npc);
		CombatState.refreshPreviousHit(npc, target);
		final Hit combatHit = getDamage(npc, target, type);
		final int tick = (type.equals(CombatType.MAGE) || type.equals(CombatType.FIRE_BREATH)) ? 3 : 1;
		final boolean splashing = combatHit.getDamage() <= 0 && type.equals(CombatType.MAGE);
		World.getWorld().submit(new Tickable(tick) {
			@Override
			public void execute() {
				this.stop();
				target.animate(CombatAnimations.getDefendAnim(target));
				if (combatHit.getDamage() > target.getHp()) {
					combatHit.setDamage(target.getHp());
				}
				if (!splashing) {
					target.inflictDamage(combatHit, false);
					handleEndEffects(npc, target, combatHit);
				} else {
					target.playGraphic(85, 0, 100);
				}
			}
		});
	}

	@Override
	public void handleEndEffects(NPC npc, Entity target, Hit hit) {
		// TODO Auto-generated method stub
		CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
	}

	@Override
	public boolean canAttack(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public Hit getDamage(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		int max = npc.getDefinition().getMaxHit();
		if (type.equals(CombatType.MAGE)) {
			max = 18;
		} else if (type.equals(CombatType.FIRE_BREATH)) {
			max = 57;
		}
		int damage = NumberUtils.random((int) max);
		int def = 0;
		int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
		if (target.isPlayer()) {
			Player player = (Player) target;
			if (type.equals(CombatType.MELEE)) {
				def = NumberUtils.random((int) HitFormula.calculateMeleeDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Melee")) {
					damage = 0;
				}
			} else if (type.equals(CombatType.MAGE) || type.equals(CombatType.FIRE_BREATH)) {
				def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
				if (type.equals(CombatType.FIRE_BREATH)) {
					double dragonfireReduction = CombatEffects.dragonfireReduction(player);
					if(dragonfireReduction > 0) {
						damage -= (damage * dragonfireReduction);
						if (damage < 0) {
							damage = 0;
						}
					}
				} else {
					if (player.getPrayers().isPrayerActive("Protect from Magic")) {
						damage *= 0.60;
					}
				}
			}
		}
		if (def > accuracy) {
			damage = 0;
		}
		final Hit combatHit = new Hit(damage, type == CombatType.MAGE || type == CombatType.FIRE_BREATH ? 2 : 0);
		combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
		combatHit.setTarget(target);
		combatHit.setOwner(npc);
		CombatEffects.altarCombatDamage(npc, target, combatHit);
		if (combatHit.getDamage() > target.getHp()) {
			combatHit.setDamage(target.getHp());
		}
		return combatHit;
	}

	@Override
	public CombatType getCombatType(NPC npc, Entity target) {
		// TODO Auto-generated method stub
		CombatType type = null;
		if (!Location.isWithinDistance(npc, target, 1)) {
			type = CombatType.MAGE;
		} else {
			switch (NumberUtils.random(2)) {
			case 0://melee
				type = CombatType.MELEE;
				break;
			case 1://range
				type = CombatType.MAGE;
				break;
			case 2://mage
				type = CombatType.FIRE_BREATH;
				break;
			}
		}
		return type;
	}

	@Override
	public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		int distance = 1;
		if (!(type.equals(CombatType.MELEE) || type.equals(CombatType.FIRE_BREATH))) {
			distance = 8;
		}
		if (Location.standingOn(npc, target)) {
			return false;
		}
		return Location.isWithinDistance(npc, target, distance);
	}

}
