package com.hyperion.game.world.entity.npc.impl;

import java.util.ArrayList;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.utility.NumberUtils;

/**
 * Chaos Druid
 *
 * @author trees
 */
public class ChaosDruid extends AbstractNPCAttacks {

    @Override
    public void executeAttacks(final NPC npc, final Entity main_target) {
        CombatType type = NumberUtils.random(0, 29) > 22 ? getCombatType(npc, main_target) : CombatType.MELEE;
        npc.getCombatState().setCombatType(type);
        if (!isWithinDistance(npc, main_target, type)) {
            return;
        } else {
            npc.getWalkingQueue().reset();
        }
        if (npc.getCombatState().getCombatCycles() > 0) {
            return;
        }
        if (!canAttack(npc, main_target, type)) {
            return;
        }
        npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
        int anim = npc.getDefinition().getAttackAnimation();
        if (type.equals(CombatType.MAGE)) {
            npc.playGraphic(177, 0, 100);
            anim = (711);
        }
        npc.animate(Animation.create(anim, AnimationPriority.HIGH));
        main_target.getCombatState().setCurrentAttacker(npc);
        main_target.getCombatState().setPreviousAttacker(npc);
        CombatState.refreshPreviousHit(npc, main_target);
        final Hit combatHit = getDamage(npc, main_target, type);
        final int ticks = type.equals(CombatType.MAGE) ? 3 : 1;
        World.getWorld().submit(new Tickable(ticks) {
            @Override
            public void execute() {
                this.stop();

                if (combatHit.getDamage() > main_target.getHp()) {
                    combatHit.setDamage(main_target.getHp());
                }
                main_target.animate(CombatAnimations.getDefendAnim(main_target));
                main_target.inflictDamage(combatHit, false);
                if (type.equals(CombatType.MAGE)) {
                    main_target.playGraphic(179, 2, 100);
                    main_target.getCombatState().setFrozen(true);
                }
                handleEndEffects(npc, main_target, combatHit);
            }
        });
    }

    @Override
    public void handleEndEffects(NPC npc, Entity target, Hit hit) {
        CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
    }

    @Override
    public boolean canAttack(NPC npc, Entity target, CombatType type) {
        if (!ProjectilePathFinder.hasLineOfSight(npc, target, false)) {
            return false;
        }
        return true;
    }

    @Override
    public Hit getDamage(NPC npc, Entity target, CombatType type) {
        int max_hit = npc.getDefinition().getMaxHit();

        int damage = NumberUtils.random(max_hit);
        int def = 0;
        int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
        if (target.isPlayer()) {
            Player player = (Player) target;
            if (type.equals(CombatType.MAGE)) {
                def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
                if (player.getPrayers().isPrayerActive("Protect from Magic")) {
                    damage *= 0.60;
                }
            }
        }
        if (def > accuracy) {
            damage = 0;
        }
        final Hit combatHit = new Hit(damage, 2);
        combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
        combatHit.setTarget(target);
        combatHit.setOwner(npc);
        CombatEffects.altarCombatDamage(npc, target, combatHit);
        if (combatHit.getDamage() > target.getHp()) {
            combatHit.setDamage(target.getHp());
        }
        return combatHit;
    }

    @Override
    public CombatType getCombatType(NPC npc, Entity target) {
        return CombatType.MAGE;
    }

    @Override
    public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
        int distance = 9;
        if (Location.standingOn(npc, target)) {
            return false;
        }
        return Location.isWithinDistance(npc, target, distance);
    }

    private static ArrayList<Player> getPossibleStompTargets(NPC npc, Entity mainTarget) {
        ArrayList<Player> possibleTarget = new ArrayList<Player>();
        for (Player targets : World.getRegionalPlayers(npc)) {
            if (targets == null)
                continue;
            if (targets.getCombatState().isDead())
                continue;
            if (Location.standingOn(npc, targets)) {
                possibleTarget.add(targets);
            }
        }
        return possibleTarget;
    }

    private static ArrayList<Player> getPossibleTargets(NPC npc, Entity mainTarget) {
        ArrayList<Player> possibleTarget = new ArrayList<Player>();
        for (Player targets : World.getRegionalPlayers(npc)) {
            if (targets == null)
                continue;
            if (targets.getCombatState().isDead())
                continue;
            possibleTarget.add(targets);
        }
        return possibleTarget;
    }
}
