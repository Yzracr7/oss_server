package com.hyperion.game.world.entity.npc.impl;

import java.util.ArrayList;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.utility.NumberUtils;

/**
 * Dark Wizard Lvl 7
 *
 * @author trees
 */
public class DarkWizard_7 extends AbstractNPCAttacks {

    @Override
    public void executeAttacks(final NPC npc, final Entity main_target) {
        CombatType type = getCombatType(npc, main_target);
        boolean confuse = NumberUtils.random(0, 29) < 22;
        npc.getCombatState().setCombatType(type);
        int anim = npc.getDefinition().getAttackAnimation();
        int projectile = -1;
        if (type.equals(CombatType.MAGE)) {
            anim = confuse ? 716 : 711; //711-716
            projectile = confuse ? 103 : 94;
        }
        if (!isWithinDistance(npc, main_target, type)) {
            return;
        } else {
            npc.getWalkingQueue().reset();
        }
        if (npc.getCombatState().getCombatCycles() > 0) {
            return;
        }
        if (!canAttack(npc, main_target, type)) {
            return;
        }
        if (confuse) {
            npc.playGraphic(102, 0, 100);
        }
        npc.animate(Animation.create(anim, AnimationPriority.HIGH));
        npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
        main_target.getCombatState().setCurrentAttacker(npc);
        main_target.getCombatState().setPreviousAttacker(npc);
        CombatState.refreshPreviousHit(npc, main_target);
        final Hit combatHit = getDamage(npc, main_target, type);
        final int ticks = 3;
        if (projectile != -1) {
            Projectiles send_projectile = Projectiles.create(npc.getCentreLocation(), main_target.getLocation(), main_target, projectile, 0, 55, 50, 35, 34);
            npc.executeProjectile(send_projectile);
        }
        World.getWorld().submit(new Tickable(ticks) {
            @Override
            public void execute() {
                this.stop();

                if (combatHit.getDamage() > main_target.getHp()) {
                    combatHit.setDamage(main_target.getHp());
                }
                main_target.animate(CombatAnimations.getDefendAnim(main_target));
                if (!confuse) {
                    main_target.playGraphic(Graphic.create(combatHit.getDamage() <= 0 ? 85 : 137, 0, 100));
                    main_target.inflictDamage(combatHit, false);
                } else {
                    int random_hit_chance = NumberUtils.random(50);
                    Player player = World.getWorld().getPlayer(main_target.getIndex());
                    if (random_hit_chance > 47) {
                        int level = player.getSkills().getLevelForXp(Skills.ATTACK);
                        int level_percentage = level - (int) (Math.round(0.05 * (level)));
                        player.getPacketSender().sendStillGraphics(player.getLocation(), new Graphic(104, 0, 100), 0);
                        player.getSkills().setLevel(Skills.ATTACK, level_percentage > level ? level : level_percentage);
                        player.sendMessage("You have been weakened.");
                    } else {
                        player.getPacketSender().sendStillGraphics(player.getLocation(), new Graphic(85, 0, 100), 0);
                    }
                }
                handleEndEffects(npc, main_target, combatHit);
            }
        });
    }

    @Override
    public void handleEndEffects(NPC npc, Entity target, Hit hit) {
        CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
    }

    @Override
    public boolean canAttack(NPC npc, Entity target, CombatType type) {
        if (!ProjectilePathFinder.hasLineOfSight(npc, target, false)) {
            return false;
        }
        return true;
    }

    @Override
    public Hit getDamage(NPC npc, Entity target, CombatType type) {
        int max_hit = npc.getDefinition().getMaxHit();

        int damage = NumberUtils.random(max_hit);
        int def = 0;
        int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
        if (target.isPlayer()) {
            Player player = (Player) target;
            if (type.equals(CombatType.MAGE)) {
                def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
                if (player.getPrayers().isPrayerActive("Protect from Magic")) {
                    damage *= 0.60;
                }
            }
        }
        if (def > accuracy) {
            damage = 0;
        }
        final Hit combatHit = new Hit(damage, 2);
        combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
        combatHit.setTarget(target);
        combatHit.setOwner(npc);
        CombatEffects.altarCombatDamage(npc, target, combatHit);
        if (combatHit.getDamage() > target.getHp()) {
            combatHit.setDamage(target.getHp());
        }
        return combatHit;
    }

    @Override
    public CombatType getCombatType(NPC npc, Entity target) {
        return CombatType.MAGE;
    }

    @Override
    public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
        int distance = 9;
        if (Location.standingOn(npc, target)) {
            return false;
        }
        return Location.isWithinDistance(npc, target, distance);
    }

    private static ArrayList<Player> getPossibleStompTargets(NPC npc, Entity mainTarget) {
        ArrayList<Player> possibleTarget = new ArrayList<Player>();
        for (Player targets : World.getRegionalPlayers(npc)) {
            if (targets == null)
                continue;
            if (targets.getCombatState().isDead())
                continue;
            if (Location.standingOn(npc, targets)) {
                possibleTarget.add(targets);
            }
        }
        return possibleTarget;
    }

    private static ArrayList<Player> getPossibleTargets(NPC npc, Entity mainTarget) {
        ArrayList<Player> possibleTarget = new ArrayList<Player>();
        for (Player targets : World.getRegionalPlayers(npc)) {
            if (targets == null)
                continue;
            if (targets.getCombatState().isDead())
                continue;
            possibleTarget.add(targets);
        }
        return possibleTarget;
    }
}
