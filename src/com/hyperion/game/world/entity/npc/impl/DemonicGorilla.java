package com.hyperion.game.world.entity.npc.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.utility.NumberUtils;

public class DemonicGorilla extends AbstractNPCAttacks {

    private NPC gorilla;
    private int[] cachedDamage = {0, 0, 0};
    private int missed_hits = 0;
    private int prayerTimer = 45;
    private int gorilla_prayer_index = 0;

    @Override
    public void executeAttacks(final NPC npc, final Entity main_target) {
        npc.getAttributes().set("dem_gorilla", this);

        this.gorilla = npc;
        CombatType type = getCombatType(npc, main_target);
        npc.getCombatState().setCombatType(type);
        if (npc.getAttributes().get("did_switch") == null) {
            npc.getAttributes().set("did_switch", true);
            switchPrayers(7147 + NumberUtils.random(2));
        }
        if (!isWithinDistance(npc, main_target, type)) {
            return;
        } else {
            npc.getWalkingQueue().reset();
        }
        if (npc.getCombatState().getCombatCycles() > 0) {
            return;
        }
        if (!canAttack(npc, main_target, type)) {
            return;
        }

        //Attack animation and projectile
        int anim = npc.getDefinition().getAttackAnimation();
        int projectile = -1;
        if (type == CombatType.MAGE) {
            anim = 7238;
            projectile = 1304;
        } else if (type == CombatType.RANGE) {
            anim = 7227;
            projectile = 1302;
        }

        //Animate and Hit
        npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
        npc.animate(Animation.create(anim, AnimationPriority.HIGH));
        if (projectile != -1) {
            Projectiles send_projectile = Projectiles.create(npc.getCentreLocation(), main_target.getLocation(), main_target, projectile, 30, (projectile == 7238 ? 10 : 55), 50, 55, 34);
            npc.executeProjectile(send_projectile);
        }
        main_target.getCombatState().setCurrentAttacker(npc);
        main_target.getCombatState().setPreviousAttacker(npc);
        CombatState.refreshPreviousHit(npc, main_target);

        final Hit combatHit = getDamage(npc, main_target, type);
        World.getWorld().submit(new Tickable(1) {
            @Override
            public void execute() {
                this.stop();
                World.getWorld().submit(new Tickable(gorilla, 1) {
                    @Override
                    public void execute() {
                        if (gorilla.getCombatState().isDead()) {
                            return;
                        }
                        if (prayerTimer > 0)
                            prayerTimer--;
                        if (prayerTimer == 0) {
                            if (cachedDamage[0] < 10 && (main_target).getCombatState().getCombatType() == CombatType.MAGE) {
                                transform(7149);
                                cachedDamage[0] = 0;
                                gorilla_prayer_index = 2;
                            } else if (cachedDamage[2] < 10 && (main_target).getCombatState().getCombatType() == CombatType.RANGE) {
                                transform(7148);
                                cachedDamage[2] = 0;
                                gorilla_prayer_index = 1;
                            } else if (cachedDamage[1] < 10 && (main_target).getCombatState().getCombatType() == CombatType.MELEE) {
                                transform(7147);
                                cachedDamage[1] = 0;
                                gorilla_prayer_index = 0;
                            }
                            resetPrayerTimer();
                        }
                    }
                });

                main_target.animate(CombatAnimations.getDefendAnim(main_target));
                if (combatHit.getDamage() > main_target.getHp()) {
                    combatHit.setDamage(main_target.getHp());
                }
                if (combatHit.getDamage() == 0) {
                    missed_hits++;
                    System.out.println("Gorilla missed " + missed_hits + " hits in a row.");
                }
                main_target.inflictDamage(combatHit, false);
                handleEndEffects(npc, main_target, combatHit);
            }
        });
    }

    @Override
    public void handleEndEffects(NPC npc, Entity target, Hit hit) {
        CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
    }

    @Override
    public boolean canAttack(NPC npc, Entity target, CombatType type) {
        if (!ProjectilePathFinder.hasLineOfSight(npc, target, false)) {
            return false;
        }
        return true;
    }

    private void transform(int id) {
        gorilla.setTransformationId(id);
    }

    private void switchPrayers(int type) {
        transform(type);
        resetPrayerTimer();
    }

    private void resetPrayerTimer() {
        prayerTimer = 45;
    }

    @Override
    public Hit getDamage(NPC npc, Entity target, CombatType type) {
        int damage = npc.getDefinition().getMaxHit();
        int def = 0;
        int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
        if (target.isPlayer()) {

            //Players Defence
            Player player = (Player) target;
            if (type.equals(CombatType.MELEE)) {
                def = NumberUtils.random((int) HitFormula.calculateMeleeDefencePVM(player));
            } else if (type.equals(CombatType.MAGE)) {
                def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
            } else if (type.equals(CombatType.RANGE)) {
                def = NumberUtils.random((int) HitFormula.calculateRangedDefencePVM(player));
            }

            //Accuracy roll
            if (def > (accuracy * 2)) {
                damage = 0;
            } else {
                damage = (NumberUtils.random(damage));
            }

        }
        final Hit combatHit = new Hit(damage, 0);
        combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
        combatHit.setTarget(target);
        combatHit.setOwner(npc);
        CombatEffects.altarCombatDamage(npc, target, combatHit);
        if (combatHit.getDamage() > target.getHp()) {
            combatHit.setDamage(target.getHp());
        }
        return combatHit;
    }

    public int handleIncomingAttack(Hit hit) {
        int damage = hit.getDamage();
        IncomingHit incomingHit = hit.getIncomingHit();
        CombatType combatType = incomingHit.getType();
        Entity attacker = hit.getOwner();
        if (attacker.isPlayer()) {
            Player player = (Player) attacker;
            //System.out.println("Gorilla Prayer Index " + gorilla.getTransformationId() + " : player combat type " + (player).getCombatState().getCombatType());
            if (gorilla.getTransformationId() == 7147 && (player).getCombatState().getCombatType() == CombatType.MELEE) {
                damage = 0;
            } else if (gorilla.getTransformationId() == 7148 && (player).getCombatState().getCombatType() == CombatType.RANGE) {
                damage = 0;
            } else if (gorilla.getTransformationId() == 7149 && (player).getCombatState().getCombatType() == CombatType.MAGE) {
                damage = 0;
            }
        }
        if (combatType.equals(CombatType.MELEE)) {
            cachedDamage[0] += damage;
        } else if (combatType.equals(CombatType.MAGE)) {
            cachedDamage[1] += damage;
        } else if (combatType.equals(CombatType.RANGE)) {
            cachedDamage[2] += damage;
        }
        return damage;
    }

    @Override
    public CombatType getCombatType(NPC npc, Entity target) {

        //If missed hits is greater than 3 switch styles
        if (missed_hits <= 3) {
            if (((Player) target).getPrayers().isPrayerActive("protect from melee")) {
                return NumberUtils.random(10) > 5 ? CombatType.RANGE : CombatType.MAGE;
            } else if (((Player) target).getPrayers().isPrayerActive("protect from ranged")) {
                return NumberUtils.random(10) > 5 ? CombatType.MELEE : CombatType.MAGE;
            } else if (((Player) target).getPrayers().isPrayerActive("protect from magic")) {
                return NumberUtils.random(10) > 5 ? CombatType.RANGE : CombatType.MELEE;
            }
            missed_hits = 0;
        }
        return CombatType.MELEE;
    }

    @Override
    public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
        int distance = 1;
        if (!(type.equals(CombatType.MELEE))) {
            distance = 8;
        }
        if (Location.standingOn(npc, target)) {
            return false;
        }
        return Location.isWithinDistance(npc, target, distance);
    }
}
