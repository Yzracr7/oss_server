package com.hyperion.game.world.entity.npc.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;

public class Dragons extends AbstractNPCAttacks {

    @Override
    public void executeAttacks(final NPC npc, final Entity target) {
        CombatType type = getCombatType(npc, target);
        npc.getCombatState().setCombatType(type);
        if (!isWithinDistance(npc, target, type)) {
            return;
        } else {
            npc.getWalkingQueue().reset();
        }
        if (npc.getCombatState().getCombatCycles() > 0) {
            return;
        }
        int anim = npc.getDefinition().getAttackAnimation();
        if (type.equals(CombatType.FIRE_BREATH)) {
            anim = 81;
            npc.playGraphic(1, 0, 100);//952
        }
        npc.animate(Animation.create(anim, AnimationPriority.HIGH));
        npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
        target.getCombatState().setCurrentAttacker(npc);
        target.getCombatState().setPreviousAttacker(npc);
        CombatState.refreshPreviousHit(npc, target);
        final int tick = type.equals(CombatType.FIRE_BREATH) ? 3 : 1;
        final Hit combatHit = getDamage(npc, target, type);
        World.getWorld().submit(new Tickable(tick) {
            @Override
            public void execute() {
                this.stop();
                target.animate(Animation.create(CombatAnimations.getDefendAnim(target), AnimationPriority.NONE));
                if (combatHit.getDamage() > target.getHp()) {
                    combatHit.setDamage(target.getHp());
                }
                target.inflictDamage(combatHit, false);
                handleEndEffects(npc, target, combatHit);
            }
        });
    }

    @Override
    public void handleEndEffects(NPC npc, Entity target, Hit hit) {
        CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
    }

    @Override
    public boolean canAttack(NPC npc, Entity target, CombatType type) {
        return true;
    }

    @Override
    public Hit getDamage(NPC npc, Entity target, CombatType type) {
        int damage = NumberUtils.random(type.equals(CombatType.FIRE_BREATH) ? (npc.getId() == 239 ? 50 : 65) : npc.getDefinition().getMaxHit());
        int def = 0;
        int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
        if (target.isPlayer()) {
            Player player = (Player) target;
            if (type.equals(CombatType.MELEE)) {
                def = NumberUtils.random((int) HitFormula.calculateMeleeDefencePVM(player));
                if (player.getPrayers().isPrayerActive("Protect from Melee")) {
                    damage = 0;
                }
            } else if (type.equals(CombatType.FIRE_BREATH)) {
                def = NumberUtils.random(HitFormula.calculateMagicDefencePVM(player));
                double dragonfireReduction = CombatEffects.dragonfireReduction(player);
                if (dragonfireReduction > 0) {
                    damage -= (damage * dragonfireReduction);
                    if (damage < 0) {
                        damage = 0;
                    }
                  //  player.sendMessage("npc accuracy roll : " + (accuracy * 2) + " / player def roll : " + def);
                    if (def > (accuracy * 2)) {
                        damage = 0;
                    } else {
                        damage = (NumberUtils.random(damage));
                    }
                } else {
                    damage = NumberUtils.random(50);
                    player.sendMessage("You are horribly burnt by the fire!");
                }
            }
        }
        final Hit combatHit = new Hit(damage, type == CombatType.FIRE_BREATH ? 2 : 0);
        combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
        combatHit.setTarget(target);
        combatHit.setOwner(npc);
        CombatEffects.altarCombatDamage(npc, target, combatHit);
        if (combatHit.getDamage() > target.getHp()) {
            combatHit.setDamage(target.getHp());
        }
        return combatHit;
    }

    @Override
    public CombatType getCombatType(NPC npc, Entity target) {
        CombatType type = null;
        switch (NumberUtils.random(2)) {
            case 0:
                type = CombatType.FIRE_BREATH;
                break;
            default:
                type = CombatType.MELEE;
                break;
        }
        return type;
    }

    @Override
    public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
        // TODO Auto-generated method stub
        if (Location.standingOn(npc, target)) {
            return false;
        }
        return Location.isWithinDistance(npc, target, 1);
    }
}
