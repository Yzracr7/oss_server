package com.hyperion.game.world.entity.npc.impl;

import java.util.ArrayList;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.TeleportAreaLocations;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.utility.NumberUtils;

public class Graador extends AbstractNPCAttacks {
    // 2865 5354 2
    @Override
    public void executeAttacks(final NPC npc, final Entity main_target) {
        CombatType type = getCombatType(npc, main_target);
        npc.getCombatState().setCombatType(type);
        if (main_target.getLocation().getX() <= 2863 || main_target.getLocation().getY() <= 5350)
            return;
        if (!isWithinDistance(npc, main_target, type)) {
            return;
        } else {
            npc.getWalkingQueue().reset();
        }
        if (npc.getCombatState().getCombatCycles() > 0) {
            return;
        }
        if (!canAttack(npc, main_target, type)) {
            return;
        }
        int anim = npc.getDefinition().getAttackAnimation();
        int projectile = -1;
        if (type.equals(CombatType.RANGE)) {
            anim = 7063;
            projectile = 1200;
        }
        randomMessage(npc, BANDOS_SHOUTS);
        npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
        npc.animate(Animation.create(anim, AnimationPriority.HIGH));
        main_target.getCombatState().setCurrentAttacker(npc);
        main_target.getCombatState().setPreviousAttacker(npc);
        CombatState.refreshPreviousHit(npc, main_target);
        final Hit combatHit = getDamage(npc, main_target, type);
        final int tick = type.equals(CombatType.RANGE) ? 2 : 1;
        if (type.equals(CombatType.RANGE)) {
            for (final Entity targets : getPossibleTargets(npc, main_target)) {
                if (targets.isNPC())
                    continue;
                Projectiles rangeProj = Projectiles.create(npc.getCentreLocation(), targets.getLocation(), targets, projectile, 25, 65, 50, 60, 34);
                npc.executeProjectile(rangeProj);
                if (!ProjectilePathFinder.hasLineOfSight(npc, targets, false)) {
                    continue;
                }
                targets.getCombatState().setCurrentAttacker(npc);
                targets.getCombatState().setPreviousAttacker(npc);
                CombatState.refreshPreviousHit(npc, targets);
                final Hit range_hit = getDamage(npc, targets, type);
                World.getWorld().submit(new Tickable(tick) {
                    @Override
                    public void execute() {
                        this.stop();
                        targets.animate(CombatAnimations.getDefendAnim(targets));
                        if (range_hit.getDamage() > targets.getHp()) {
                            range_hit.setDamage(targets.getHp());
                        }
                        targets.inflictDamage(range_hit, false);
                        handleEndEffects(npc, targets, range_hit);
                    }
                });
            }
            return;
        } else {
            World.getWorld().submit(new Tickable(tick) {
                @Override
                public void execute() {
                    this.stop();
                    main_target.animate(CombatAnimations.getDefendAnim(main_target));
                    if (combatHit.getDamage() > main_target.getHp()) {
                        combatHit.setDamage(main_target.getHp());
                    }
                    main_target.inflictDamage(combatHit, false);
                    handleEndEffects(npc, main_target, combatHit);
                }
            });
        }
    }

    @Override
    public void handleEndEffects(NPC npc, Entity target, Hit hit) {
        CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
    }

    @Override
    public boolean canAttack(NPC npc, Entity target, CombatType type) {
        if (target.getLocation().getX() <= 2863 || target.getLocation().getY() <= 5350) {
            Player player = (Player) target;
            System.out.println(player.getDetails().getName() + " is attacking graador from outside the boss room!!");
            return false;
        }
        return true;
    }

    @Override
    public Hit getDamage(NPC npc, Entity target, CombatType type) {
        int max = npc.getDefinition().getMaxHit();
        if (type.equals(CombatType.RANGE)) {
            max = 35;
        }
        int damage = max;
        int def = 0;
        int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
        if (target.isPlayer()) {
            Player player = (Player) target;
            if (type.equals(CombatType.MELEE)) {
                def = NumberUtils.random((int) HitFormula.calculateMeleeDefencePVM(player));
                if (player.getPrayers().isPrayerActive("Protect from Melee")) {
                    damage = 0;
                }
            } else if (type.equals(CombatType.RANGE)) {
                def = NumberUtils.random((int) HitFormula.calculateRangedDefencePVM(player));
                if (player.getPrayers().isPrayerActive("Protect from Ranged")) {
                    damage = 0;
                }
            }
            if (def > (accuracy * 2)) {
                damage = 0;
            } else {
                damage = (NumberUtils.random(damage));
            }
        }
        final Hit combatHit = new Hit(damage, type == CombatType.RANGE ? 1 : 0);
        combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
        combatHit.setTarget(target);
        combatHit.setOwner(npc);
        CombatEffects.altarCombatDamage(npc, target, combatHit);
        if (combatHit.getDamage() > target.getHp()) {
            combatHit.setDamage(target.getHp());
        }
        return combatHit;
    }

    @Override
    public CombatType getCombatType(NPC npc, Entity target) {
        CombatType type = CombatType.MELEE;
        if (!Location.isWithinDistance(npc, target, 1)) {
            type = CombatType.RANGE;
        }
        return type;
    }

    @Override
    public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
        int distance = 1;
        if (!(type.equals(CombatType.MELEE))) {
            distance = 8;
        }
        /*
         * if (Location.standingOn(npc, target)) { return false; }
		 */
        return Location.isWithinDistance(npc, target, distance);
    }

    private static final String[] BANDOS_SHOUTS = {"Death to our enemies!", "Brargh!", "Break their bones!", "For the glory of the Big High War God!", "Split their skulls!", "We feast on the bones of our enemies tonight!", "CHAAARGE!", "Crush them underfoot!", "All glory to Bandos!", "GRAAAAAAAAAR!",};

    private static void randomMessage(NPC boss, String[] shouts) {
        if (NumberUtils.random(2) != 0) {
            return;
        }
        String message = shouts[NumberUtils.random(shouts.length - 1)];
        boss.playForcedChat(message);
    }

    private static ArrayList<Entity> getPossibleTargets(NPC npc, Entity mainTarget) {
        ArrayList<Entity> possibleTarget = new ArrayList<Entity>();
        for (Player targets : World.getRegionalPlayers(npc)) {
            if (targets == null)
                continue;
            if (targets.getCombatState().isDead())
                continue;
            if (!TeleportAreaLocations.atBandosChamber(targets.getLocation()))
                continue;
            possibleTarget.add(targets);
        }
        return possibleTarget;
    }
}
