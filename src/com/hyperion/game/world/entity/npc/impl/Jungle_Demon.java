package com.hyperion.game.world.entity.npc.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatConstants;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;
public class Jungle_Demon extends AbstractNPCAttacks {

	@Override
	public void executeAttacks(final NPC npc, final Entity target) {
		// TODO Auto-generated method stub
		CombatType type = getCombatType(npc, target);
		npc.getCombatState().setCombatType(type);
		if (!isWithinDistance(npc, target, type)) {
			return;
		} else {
			if (type.equals(CombatType.MELEE)) {
				npc.getWalkingQueue().reset();
			}
		}
		if (npc.getCombatState().getCombatCycles() > 0) {
			return;
		}
		int projectileId = -1;
		int endGfx = -1;
		int endGfxHeight = 0;
		int anim = npc.getDefinition().getAttackAnimation();
		if (type.equals(CombatType.MAGE)) {
			int chance = NumberUtils.random(2);
			switch (chance) {
			case 0:
				projectileId = 156;
				endGfx = 157;
				break;
			case 1:
				projectileId = 159;
				endGfx = 160;
				break;
			case 2:
				projectileId = 162;
				endGfx = 163;
				break;
			}
			Projectiles magicProj = Projectiles.create(npc.getCentreLocation(), target.getCentreLocation(), target, projectileId, 35, 75, 50, 43, 35);
			npc.executeProjectile(magicProj);
		}
		npc.animate(Animation.create(anim, AnimationPriority.HIGH));
		npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
		target.getCombatState().setCurrentAttacker(npc);
		target.getCombatState().setPreviousAttacker(npc);
		CombatState.refreshPreviousHit(npc, target);
		final int tick = type.equals(CombatType.MAGE) ? 3 : 1;
		final Hit combatHit = getDamage(npc, target, type);
		if (combatHit.getDamage() <= 0) {
			endGfx = 85;
			endGfxHeight = 100;
		}
		final int finalEndGfx = endGfx;
		final int finalEndGfxHeight = endGfxHeight;
		World.getWorld().submit(new Tickable(tick) {
			@Override
			public void execute() {
				this.stop();
				if (finalEndGfx != -1) {
					target.playGraphic(finalEndGfx, 0, finalEndGfxHeight);
				}
				target.animate(CombatAnimations.getDefendAnim(target));
				if (combatHit.getDamage() > target.getHp()) {
					combatHit.setDamage(target.getHp());
				}
				if (finalEndGfx != 85) {
					target.inflictDamage(combatHit, false);
					handleEndEffects(npc, target, combatHit);
				}
			}
		});
	}

	@Override
	public void handleEndEffects(NPC npc, Entity target, Hit hit) {
		// TODO Auto-generated method stub
		CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
	}

	@Override
	public boolean canAttack(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public Hit getDamage(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		int damage = NumberUtils.random((int) (type.equals(CombatType.MAGE) ? 58 : npc.getDefinition().getMaxHit()));
		int def = 0;
		int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
		if (target.isPlayer()) {
			Player player = (Player) target;
			if (type.equals(CombatType.MELEE)) {
				def = NumberUtils.random((int) HitFormula.calculateMeleeDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Melee")) {
					damage = 0;
				}
			} else if (type.equals(CombatType.MAGE)) {
				def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Magic")) {
					damage = 0;
				}
			}
		}
		if (def > accuracy) {
			damage = 0;
		}
		final Hit combatHit = new Hit(damage, CombatConstants.getId(type));
		combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
		combatHit.setTarget(target);
		combatHit.setOwner(npc);
		CombatEffects.altarCombatDamage(npc, target, combatHit);
		if (combatHit.getDamage() > target.getHp()) {
			combatHit.setDamage(target.getHp());
		}
		return combatHit;
	}

	@Override
	public CombatType getCombatType(NPC npc, Entity target) {
		// TODO Auto-generated method stub
		CombatType type = null;
		switch (NumberUtils.random(2)) {
		case 0:
		case 1:
			type = CombatType.MAGE;
			break;
		case 2:
			type = CombatType.MELEE;
			break;
		}
		if (!Location.isWithinDistance(npc, target, 1)) {
			type = CombatType.MAGE;
		}
		return type;
	}

	@Override
	public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		int distance = 1;
		if (!(type.equals(CombatType.MELEE))) {
			distance = 8;
		}
		if (Location.standingOn(npc, target)) {
			return false;
		}
		return Location.isWithinDistance(npc, target, distance);
	}
}
