package com.hyperion.game.world.entity.npc.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatConstants;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.utility.NumberUtils;

public class KalphiteQueen extends AbstractNPCAttacks {

    @Override
    public void executeAttacks(final NPC npc, final Entity target) {
        // TODO Auto-generated method stub
        CombatType type = getCombatType(npc, target);
        npc.getCombatState().setCombatType(type);
        if (!isWithinDistance(npc, target, type)) {
            return;
        } else {
            npc.getWalkingQueue().reset();
        }
        if (npc.getCombatState().getCombatCycles() > 0) {
            return;
        }
        if (!canAttack(npc, target, type)) {
            return;
        }
        boolean secondForm = npc.getAttributes().isSet("kqsecondform");
        int projectile = -1;
        int endGfx = -1;
        int npcGfx = -1;
        int projectileStartSpeed = 35;
        int projectileSpeed = 65;
        int anim = npc.getDefinition().getAttackAnimation();
        if (type.equals(CombatType.RANGE) || type.equals(CombatType.MAGE)) {
            if (!secondForm) {
                anim = 6240;
                if (type.equals(CombatType.MAGE)) {
                    projectileStartSpeed = 55;
                    projectileSpeed = 65;
                    npcGfx = 278;
                    projectile = 280;
                    endGfx = 281;
                } else if (type.equals(CombatType.RANGE)) {
                    projectile = 288;
                }
            } else if (secondForm) {
                anim = 6234;
                if (type.equals(CombatType.MAGE)) {
                    npcGfx = 279;
                    projectile = 280;
                    endGfx = 281;
                } else if (type.equals(CombatType.RANGE)) {
                    projectile = 288;
                }
            }
        }
        int hitDelay = 1;//melee default.
        if (type.equals(CombatType.RANGE)) {
            hitDelay = 2;
        } else if (type.equals(CombatType.MAGE)) {
            hitDelay = 3;
        }
        npc.animate(Animation.create(anim, AnimationPriority.HIGH));
        if (npcGfx != -1) {
            npc.playGraphic(npcGfx);
        }
        npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
        if (type.equals(CombatType.MAGE)) {
            for (final Player players : World.getRegionalPlayers(npc)) {
                if (!isWithinDistance(npc, players, type)) {
                    continue;
                }
                if (projectile != -1) {
                    Projectiles sendProj = Projectiles.create(npc.getCentreLocation(), players.getCentreLocation(), players, projectile, projectileStartSpeed, projectileSpeed, 50, 43, 35);
                    npc.executeProjectile(sendProj);
                }
                players.getCombatState().setCurrentAttacker(npc);
                players.getCombatState().setPreviousAttacker(npc);
                CombatState.refreshPreviousHit(npc, players);
                final int finalEndGfx = endGfx;
                final Hit combatHit = getDamage(npc, players, type);
                World.getWorld().submit(new Tickable(hitDelay) {
                    @Override
                    public void execute() {
                        this.stop();
                        if (finalEndGfx != -1) {
                            players.playGraphic(finalEndGfx);
                        }
                        players.animate(CombatAnimations.getDefendAnim(players));
                        if (combatHit.getDamage() > players.getHp()) {
                            combatHit.setDamage(players.getHp());
                        }
                        players.inflictDamage(combatHit, false);
                        handleEndEffects(npc, players, combatHit);
                    }
                });
            }
        } else {
            if (projectile != -1) {
                Projectiles sendProj = Projectiles.create(npc.getCentreLocation(), target.getCentreLocation(), target, projectile, projectileStartSpeed, projectileSpeed, 50, 43, 35);
                npc.executeProjectile(sendProj);
            }
            target.getCombatState().setCurrentAttacker(npc);
            target.getCombatState().setPreviousAttacker(npc);
            CombatState.refreshPreviousHit(npc, target);
            final int finalEndGfx = endGfx;
            final Hit combatHit = getDamage(npc, target, type);
            World.getWorld().submit(new Tickable(hitDelay) {
                @Override
                public void execute() {
                    this.stop();
                    if (finalEndGfx != -1) {
                        target.playGraphic(finalEndGfx);
                    }
                    target.animate(CombatAnimations.getDefendAnim(target));
                    if (combatHit.getDamage() > target.getHp()) {
                        combatHit.setDamage(target.getHp());
                    }
                    target.inflictDamage(combatHit, false);
                    handleEndEffects(npc, target, combatHit);
                }
            });
        }
    }

    @Override
    public void handleEndEffects(NPC npc, Entity target, Hit hit) {
        // TODO Auto-generated method stub
        CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
    }

    @Override
    public boolean canAttack(NPC npc, Entity target, CombatType type) {
        // TODO Auto-generated method stub
        if (!ProjectilePathFinder.hasLineOfSight(npc, target, false)) {
            return false;
        }
        return true;
    }

    @Override
    public Hit getDamage(NPC npc, Entity target, CombatType type) {
        int damage = 31;
        int def = 0;
        int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
        if (target.isPlayer()) {
            Player player = (Player) target;
            if (type.equals(CombatType.MELEE)) {
                def = NumberUtils.random((int) HitFormula.calculateMeleeDefencePVM(player));
                if (player.getPrayers().isPrayerActive("Protect from Melee")) {
                    damage *= 0.50;
                }
            } else if (type.equals(CombatType.RANGE)) {
                def = NumberUtils.random((int) HitFormula.calculateRangedDefencePVM(player));
                if (player.getPrayers().isPrayerActive("Protect from Ranged")) {
                    damage *= 0.50;
                }
            } else if (type.equals(CombatType.MAGE)) {
                def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
                if (player.getPrayers().isPrayerActive("Protect from Magic")) {
                    damage *= 0.50;
                }
            }
          //  player.sendMessage("npc accuracy roll : " + (accuracy * 2) + " / player def roll : " + def);
            if (def > (accuracy * 2)) {
                damage = 0;
            } else {
                damage = (NumberUtils.random(damage));
            }
        }
        final Hit combatHit = new Hit(damage, CombatConstants.getId(type));
        combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
        combatHit.setTarget(target);
        combatHit.setOwner(npc);
        CombatEffects.altarCombatDamage(npc, target, combatHit);
        if (combatHit.getDamage() > target.getHp()) {
            combatHit.setDamage(target.getHp());
        }
        return combatHit;
    }

    @Override
    public CombatType getCombatType(NPC npc, Entity target) {
        // TODO Auto-generated method stub
        CombatType type = CombatType.MELEE;
        if (!Location.isWithinDistance(npc, target, 1)) {
            switch (NumberUtils.random(1)) {
                case 0:
                    type = CombatType.RANGE;
                    break;
                case 1:
                    type = CombatType.MAGE;
                    break;
            }
        } else {
            switch (NumberUtils.random(2)) {
                case 0:
                    type = CombatType.MELEE;
                    break;
                case 1:
                    type = CombatType.MAGE;
                    break;
                case 2:
                    type = CombatType.RANGE;
                    break;
            }
        }
        return type;
    }

    @Override
    public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
        // TODO Auto-generated method stub
        int distance = 1;
        if (!(type.equals(CombatType.MELEE))) {
            distance = 8;
        }
        if (Location.standingOn(npc, target)) {
            return false;
        }
        return Location.isWithinDistance(npc, target, distance);
    }

}
