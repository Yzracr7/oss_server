package com.hyperion.game.world.entity.npc.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatConstants;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;

public class KarmelAttacks extends AbstractNPCAttacks {

	@Override
	public void executeAttacks(final NPC npc, final Entity target) {
		// TODO Auto-generated method stub
		CombatType type = getCombatType(npc, target);
		npc.getCombatState().setCombatType(type);
		if (!isWithinDistance(npc, target, type)) {
			return;
		} else {
			npc.getWalkingQueue().reset();
		}
		if (npc.getCombatState().getCombatCycles() > 0) {
			return;
		}
		int endGfxHeight = 0;
		int endGfx = 369;
		npc.animate(1979);
		npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
		target.getCombatState().setCurrentAttacker(npc);
		target.getCombatState().setPreviousAttacker(npc);
		CombatState.refreshPreviousHit(npc, target);
		final Hit combatHit = getDamage(npc, target, type);
		if (combatHit.getDamage() <= 0) {
			endGfx = 85;
			endGfxHeight = 100;
		}
		final int finalEndGfx = endGfx;
		final int finalEndGfxHeight = endGfxHeight;
		World.getWorld().submit(new Tickable(3) {
			@Override
			public void execute() {
				this.stop();
				if (finalEndGfx != -1) {
					target.playGraphic(finalEndGfx, 0, finalEndGfxHeight);
				}
				target.animate(CombatAnimations.getDefendAnim(target));
				if (combatHit.getDamage() > target.getHp()) {
					combatHit.setDamage(target.getHp());
				}
				if (finalEndGfx != 85) {
					target.inflictDamage(combatHit, false);
					target.inflictDamage(combatHit, false);
					handleEndEffects(npc, target, combatHit);
					handleEndEffects(npc, target, combatHit);
				}
			}
		});
	}

	@Override
	public void handleEndEffects(NPC npc, Entity target, Hit hit) {
		// TODO Auto-generated method stub
		CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
	}

	@Override
	public boolean canAttack(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public Hit getDamage(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		int damage = NumberUtils.random((int) 7);
		int def = 0;
		int attk = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
		if (target.isPlayer()) {
			Player player = (Player) target;
			if (type.equals(CombatType.MELEE)) {
				def = NumberUtils.random((int) HitFormula.calculateMeleeDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Melee")) {
					damage = 0;
				}
			} else if (type.equals(CombatType.MAGE)) {
				def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Magic")) {
					damage = 0;
				}
			}
		}
		if (def > attk) {
			damage = 0;
		}
		final Hit combatHit = new Hit(damage, CombatConstants.getId(type));
		combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
		combatHit.setTarget(target);
		combatHit.setOwner(npc);
		CombatEffects.altarCombatDamage(npc, target, combatHit);
		if (combatHit.getDamage() > target.getHp()) {
			combatHit.setDamage(target.getHp());
		}
		return combatHit;
	}

	@Override
	public CombatType getCombatType(NPC npc, Entity target) {
		// TODO Auto-generated method stub
		CombatType type = CombatType.MELEE;
		if (!Location.isWithinDistance(npc, target, 1)) {
			type = CombatType.RANGE;
		}
		return type;
	}

	@Override
	public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		int distance = 1;
		if (!(type.equals(CombatType.MELEE))) {
			distance = 8;
		}
		if (Location.standingOn(npc, target)) {
			return false;
		}
		return Location.isWithinDistance(npc, target, distance);
	}

}
