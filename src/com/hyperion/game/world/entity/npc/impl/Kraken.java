package com.hyperion.game.world.entity.npc.impl;

import java.util.ArrayList;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.utility.NumberUtils;

public class Kraken extends AbstractNPCAttacks {

    @Override
    public void executeAttacks(final NPC npc, final Entity main_target) {
        // TODO Auto-generated method stub
        CombatType type = getCombatType(npc, main_target);
        npc.getCombatState().setCombatType(type);
        if (!isWithinDistance(npc, main_target, type)) {
            return;
        } else {
            npc.getWalkingQueue().reset();
        }
        if (npc.getCombatState().getCombatCycles() > 0) {
            return;
        }
        if (!canAttack(npc, main_target, type)) {
            return;
        }
        npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
        int anim = npc.getDefinition().getAttackAnimation();
        if (type.equals(CombatType.MAGE)) {
            World.getWorld().submit(new Tickable(1) {
                @Override
                public void execute() {
                    this.stop();
                    npc.animate(Animation.create(anim, AnimationPriority.HIGH));


                    main_target.getCombatState().setCurrentAttacker(npc);
                    main_target.getCombatState().setPreviousAttacker(npc);
                    CombatState.refreshPreviousHit(npc, main_target);
                    npc.executeProjectile(Projectiles.create(npc.getCentreLocation(), main_target.getLocation(), main_target, 162, 5, 55, 50, 60, 34));
                    World.getWorld().submit(new Tickable(3) {
                        @Override
                        public void execute() {
                            this.stop();


                            final Hit combat_hit = getDamage(npc, main_target, CombatType.MAGE);
                            if (combat_hit.getDamage() > main_target.getHp()) {
                                combat_hit.setDamage(main_target.getHp());
                            }
                            main_target.animate(CombatAnimations.getDefendAnim(main_target));
                            main_target.playGraphic(Graphic.create(combat_hit.getDamage() <= 0 ? 85 : 163));
                            main_target.inflictDamage(combat_hit, false);
                            handleEndEffects(npc, main_target, combat_hit);
                        }
                    });


                    for (final Player players : getPossibleTargets(npc, main_target)) {
                        Location targetsLoc = players.getLocation();
                        if (ProjectilePathFinder.hasLineOfSight(targetsLoc, targetsLoc, false)) {
                            continue;
                        }
                        players.getCombatState().setCurrentAttacker(npc);
                        players.getCombatState().setPreviousAttacker(npc);
                        CombatState.refreshPreviousHit(npc, players);
                        npc.executeProjectile(Projectiles.create(npc.getCentreLocation(), players.getLocation(), players, 162, 5, 55, 50, 60, 34));
                        final Hit combat_hit = getDamage(npc, players, CombatType.MAGE);
                        World.getWorld().submit(new Tickable(3) {
                            @Override
                            public void execute() {
                                this.stop();
                                players.animate(CombatAnimations.getDefendAnim(players));
                                main_target.playGraphic(Graphic.create(163));
                                if (combat_hit.getDamage() > players.getHp()) {
                                    combat_hit.setDamage(players.getHp());
                                }
                                players.inflictDamage(combat_hit, false);
                                handleEndEffects(npc, players, combat_hit);
                            }
                        });
                    }
                }
            });
        }
    }

    @Override
    public void handleEndEffects(NPC npc, Entity target, Hit hit) {
        CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
    }

    @Override
    public boolean canAttack(NPC npc, Entity target, CombatType type) {
        if (!ProjectilePathFinder.hasLineOfSight(npc, target, false)) {
            return false;
        }
        return true;
    }

    @Override
    public Hit getDamage(NPC npc, Entity target, CombatType type) {
        int max_hit = npc.getDefinition().getMaxHit();
        int damage = max_hit;
        int def = 0;
        int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
        if (target.isPlayer()) {
            Player player = (Player) target;
            if (type.equals(CombatType.MAGE)) {
                def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
                if (player.getPrayers().isPrayerActive("Protect from Magic")) {
                    //player.getPacketSender().sendMessage("The Kraken's attack breaks through your protection prayer.");
                    damage *= 0.60;
                }
            }
            //player.sendMessage("npc accuracy roll : " + (accuracy * 2) + " / player def roll : " + def);
            if (def > (accuracy * 2)) {
                damage = 0;
            } else {
                damage = (NumberUtils.random(damage));
            }
        }
        final Hit combatHit = new Hit(damage, 2);
        combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
        combatHit.setTarget(target);
        combatHit.setOwner(npc);
        CombatEffects.altarCombatDamage(npc, target, combatHit);
        if (combatHit.getDamage() > target.getHp()) {
            combatHit.setDamage(target.getHp());
        }
        return combatHit;
    }

    @Override
    public CombatType getCombatType(NPC npc, Entity target) {
        return CombatType.MAGE;
    }

    @Override
    public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
        int distance = 9;
        if (Location.standingOn(npc, target)) {
            return false;
        }
        return Location.isWithinDistance(npc, target, distance);
    }


    private static ArrayList<Player> getPossibleTargets(NPC npc, Entity mainTarget) {
        ArrayList<Player> possibleTarget = new ArrayList<Player>();
        for (Player targets : World.getRegionalPlayers(npc)) {
            if (targets == null)
                continue;
            if (targets.getCombatState().isDead())
                continue;
            possibleTarget.add(targets);
        }
        return possibleTarget;
    }
}
