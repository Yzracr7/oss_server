package com.hyperion.game.world.entity.npc.impl;

import com.hyperion.game.content.minigames.pestcontrol.PestSession;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatConstants;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.utility.NumberUtils;
public class PestZombie extends AbstractNPCAttacks {

	@Override
	public void executeAttacks(final NPC npc, final Entity target) {
		// TODO Auto-generated method stub
		CombatType type = getCombatType(npc, target);
		npc.getCombatState().setCombatType(type);
		if (!isWithinDistance(npc, target, type)) {
			return;
		} else {
			npc.getWalkingQueue().reset();
		}
		if (npc.getCombatState().getCombatCycles() > 0) {
			return;
		}
		if (!canAttack(npc, target, type)) {
			return;
		}
		int anim = npc.getDefinition().getAttackAnimation();
		npc.animate(Animation.create(anim, AnimationPriority.HIGH));
		npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
		target.getCombatState().setCurrentAttacker(npc);
		target.getCombatState().setPreviousAttacker(npc);
		CombatState.refreshPreviousHit(npc, target);
		int projectile = -1;
		if (type.equals(CombatType.MAGE)) {
			projectile = 378;
			Projectiles magicProj = Projectiles.create(npc.getCentreLocation(), target.getCentreLocation(), target, projectile, 32, 50, 50, 43, 35);
			npc.executeProjectile(magicProj);
		}
		final int tick = type.equals(CombatType.MAGE) ? 2 : 1;
		final Hit combatHit = getDamage(npc, target, type);
		final boolean splashed = combatHit.getDamage() <= 0 && projectile != -1;
		World.getWorld().submit(new Tickable(tick) {
			@Override
			public void execute() {
				this.stop();
				target.animate(CombatAnimations.getDefendAnim(target));
				if (combatHit.getDamage() > target.getHp()) {
					combatHit.setDamage(target.getHp());
				}
				if (splashed) {
					target.playGraphic(85, 0, 100);
					return;
				}
				target.inflictDamage(combatHit, false);
				handleEndEffects(npc, target, combatHit);
			}
		});
	}

	@Override
	public void handleEndEffects(NPC npc, Entity target, Hit hit) {
		// TODO Auto-generated method stub
		CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
	}

	@Override
	public boolean canAttack(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		if (!ProjectilePathFinder.hasLineOfSight(npc, target, false)) {
			return false;
		}
		return true;
	}

	@Override
	public Hit getDamage(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		int damage = NumberUtils.random((int) 10);
		int def = 0;
		int attack_bonus = 75;
		PestSession pest_control = (PestSession) npc.getAttributes().get("pestsession");
		int pest_round = pest_control.getRound();
		if (pest_round >= 15) {
			attack_bonus += 25;
		} else if (pest_round >= 20) {
			attack_bonus += 35;
		} else if (pest_round >= 30) {
			attack_bonus += 45;
		} else if (pest_round >= 35) {
			attack_bonus += 55;
		} else if (pest_round >= 40) {
			attack_bonus += 65;
		} else if (pest_round >= 45) {
			attack_bonus += 75;
		} else if (pest_round >= 50) {
			attack_bonus += 85;
		}
		int accuracy = NumberUtils.random((int) attack_bonus);
		if (target.isPlayer()) {
			Player player = (Player) target;
			if (type.equals(CombatType.MELEE)) {
				def = NumberUtils.random((int) HitFormula.calculateMeleeDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Melee")) {
					damage = 0;
				}
			} else if (type.equals(CombatType.MAGE)) {
				def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Magic")) {
					damage = 0;
				}
			}
		}
		if (def > accuracy) {
			damage = 0;
		}
		final Hit combatHit = new Hit(damage, CombatConstants.getId(type));
		combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
		combatHit.setTarget(target);
		combatHit.setOwner(npc);
		CombatEffects.altarCombatDamage(npc, target, combatHit);
		if (combatHit.getDamage() > target.getHp()) {
			combatHit.setDamage(target.getHp());
		}
		return combatHit;
	}

	@Override
	public CombatType getCombatType(NPC npc, Entity target) {
		// TODO Auto-generated method stub
		CombatType type = null;
		switch (NumberUtils.random(2)) {
		case 0:
		case 1:
			type = CombatType.MELEE;
			break;
		case 2:
			type = CombatType.MAGE;
			break;
		}
		if (!Location.isWithinDistance(npc, target, 1)) {
			type = CombatType.MAGE;
		}
		return type;
	}

	@Override
	public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		int distance = 1;
		if (!(type.equals(CombatType.MELEE))) {
			distance = 8;
		}
		if (Location.standingOn(npc, target)) {
			return false;
		}
		return Location.isWithinDistance(npc, target, distance);
	}

}
