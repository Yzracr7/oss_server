package com.hyperion.game.world.entity.npc.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.utility.NumberUtils;
public class Prayer_Draining_NPC extends AbstractNPCAttacks {

	@Override
	public void executeAttacks(final NPC npc, final Entity target) {
		// TODO Auto-generated method stub
		CombatType type = getCombatType(npc, target);
		npc.getCombatState().setCombatType(type);
		if (!isWithinDistance(npc, target, type)) {
			return;
		} else {
			npc.getWalkingQueue().reset();
		}
		if (npc.getCombatState().getCombatCycles() > 0) {
			return;
		}
		int anim = npc.getDefinition().getAttackAnimation();
		npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
		npc.animate(Animation.create(anim, AnimationPriority.HIGH));
		target.getCombatState().setCurrentAttacker(npc);
		target.getCombatState().setPreviousAttacker(npc);
		CombatState.refreshPreviousHit(npc, target);
		final Hit combatHit = getDamage(npc, target, type);
		final int tick = 1;
		World.getWorld().submit(new Tickable(tick) {
			@Override
			public void execute() {
				this.stop();
				target.animate(CombatAnimations.getDefendAnim(target));
				if (combatHit.getDamage() > target.getHp()) {
					combatHit.setDamage(target.getHp());
				}
				target.inflictDamage(combatHit, false);
				
				handleEndEffects(npc, target, combatHit);
			}
		});
	}

	@Override
	public void handleEndEffects(NPC npc, Entity target, Hit hit) {
		// TODO Auto-generated method stub
			Player player = (Player) target;
			if (player.getSettings().getPrayerPoints() > 0 && hit.getDamage() > 0) {
				//58 = Shadow Spider
				player.getSettings().decreasePrayerPoints(npc.getId() == 58 ? player.getSettings().getPrayerPoints() / 2 :hit.getDamage());
				player.getPacketSender().sendMessage("Your prayer has been slightly drained.");
			}
		CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
	}

	@Override
	public boolean canAttack(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		if (!ProjectilePathFinder.hasLineOfSight(npc, target, false)) {
			return false;
		}
		return true;
	}

	@Override
	public Hit getDamage(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		int damage = NumberUtils.random((int) (npc.getDefinition().getMaxHit()));
		int def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM((Player) target));
		int attk = NumberUtils.random((int) npc.getDefinition().getAttackBonus() + 60);
		if (target.isPlayer()) {
			Player player = (Player) target;
			if (type.equals(CombatType.MELEE)) {
				def = NumberUtils.random((int) HitFormula.calculateMeleeDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Melee")) {
					damage = 0;
				}
			}
		}
		if (def > attk) {
			damage = 0;
		}
		final Hit combatHit = new Hit(damage, 0);
		combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
		combatHit.setTarget(target);
		combatHit.setOwner(npc);
		CombatEffects.altarCombatDamage(npc, target, combatHit);
		if (combatHit.getDamage() > target.getHp()) {
			combatHit.setDamage(target.getHp());
		}
		return combatHit;
	}

	@Override
	public CombatType getCombatType(NPC npc, Entity target) {
		CombatType type = CombatType.MELEE;
		return type;
	}

	@Override
	public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		int distance = 1;
		if (Location.standingOn(npc, target)) {
			return false;
		}
		return Location.isWithinDistance(npc, target, distance);
	}
}
