package com.hyperion.game.world.entity.npc.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatConstants;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.utility.NumberUtils;
public class Revenant_Hobgoblin extends AbstractNPCAttacks {

	@Override
	public void executeAttacks(final NPC npc, final Entity main_target) {
		// TODO Auto-generated method stub
		final CombatType type = getCombatType(npc, main_target);
		npc.getCombatState().setCombatType(type);
		if (!isWithinDistance(npc, main_target, type)) {
			return;
		} else {
			npc.getWalkingQueue().reset();
		}
		if (npc.getCombatState().getCombatCycles() > 0) {
			return;
		}
		if (!canAttack(npc, main_target, type)) {
			return;
		}
		npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
		int anim = npc.getDefinition().getAttackAnimation();
		if (type.equals(CombatType.MONKEY_GUARD_HEAL)) {
			//anim = 1405;
			npc.animate(Animation.create(anim, AnimationPriority.HIGH));
			int percentage = (int) (npc.getMaxHp() * 0.20);
			npc.heal(percentage);
			npc.getAttributes().subtractInt("food_count", 15);
			return;
		}
		int projectile_id = -1;
		if (type.equals(CombatType.RANGE)) {
			anim = 7516;
			projectile_id = 1278;
			Projectiles projectile = Projectiles.create(npc.getCentreLocation(), main_target.getCentreLocation(), main_target, projectile_id, 25, 75, 50, 43, 35);
			npc.executeProjectile(projectile);
		} else if (type.equals(CombatType.MAGE)) {
			anim = 7503;
			projectile_id = 1276;
			Projectiles projectile = Projectiles.create(npc.getCentreLocation(), main_target.getCentreLocation(), main_target, projectile_id, 32, 50, 50, 43, 35);
			npc.executeProjectile(projectile);
		}
		npc.animate(Animation.create(anim, AnimationPriority.HIGH));
		main_target.getCombatState().setCurrentAttacker(npc);
		main_target.getCombatState().setPreviousAttacker(npc);
		CombatState.refreshPreviousHit(npc, main_target);
		final Hit combatHit = getDamage(npc, main_target, type);
		final int tick = !type.equals(CombatType.MELEE) ? 3 : 1;
		World.getWorld().submit(new Tickable(tick) {
			@Override
			public void execute() {
				this.stop();
				main_target.animate(CombatAnimations.getDefendAnim(main_target));
				if (combatHit.getDamage() > main_target.getHp()) {
					combatHit.setDamage(main_target.getHp());
				}
				if (type.equals(CombatType.RANGE) || type.equals(CombatType.MAGE)) {
					if (combatHit.getDamage() > 0) {
						if (type.equals(CombatType.RANGE)) {
							if (main_target.getCombatState().isFreezable()) {
								if (main_target.isPlayer()) {
									((Player) main_target).getPacketSender().sendMessage("The icy darts freeze your muscles!");
									freeze(npc, main_target);
								}
							}
						} else if (type.equals(CombatType.MAGE)) {
							/*if (main_target.isPlayer()) {
								if (!main_target.getCombatState().isTeleBlocked()) {
									int time = 500;
									if (((Player) main_target).getPrayers().isPrayerActive("protect from magic")) {
										time /= 2;
									}
									((Player) main_target).getPacketSender().sendMessage("A teleport block has been cast on you!");
									((Player) main_target).getCombatState().setTbTime(time);
								}
							}*/
						}
					}
					main_target.inflictDamage(combatHit, false);
				} else {
					main_target.inflictDamage(combatHit, false);
				}
				handleEndEffects(npc, main_target, combatHit);
			}
		});
	}

	@Override
	public void handleEndEffects(NPC npc, Entity target, Hit hit) {
		// TODO Auto-generated method stub
		CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
	}

	@Override
	public boolean canAttack(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		if (!ProjectilePathFinder.hasLineOfSight(npc, target, false)) {
			return false;
		}
		return true;
	}

	@Override
	public Hit getDamage(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		int max = npc.getDefinition().getMaxHit();
		if (type.equals(CombatType.MAGE) || type.equals(CombatType.RANGE)) {
			max = 20;
		}
		int damage = NumberUtils.random((int) max);
		int def = 0;
		int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
		if (target.isPlayer()) {
			Player player = (Player) target;
			if (type.equals(CombatType.MELEE)) {
				def = NumberUtils.random((int) HitFormula.calculateMeleeDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Melee")) {
					damage = 0;
				}
			} else if (type.equals(CombatType.MAGE)) {
				def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Magic")) {
					damage = 0;
				}
			} else if (type.equals(CombatType.RANGE)) {
				def = NumberUtils.random((int) HitFormula.calculateRangedDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Ranged")) {
					damage = 0;
				}
			}
		}
		//System.out.println("def: " + def + ", acc: " + accuracy);
		if (def > accuracy) {
			damage = 0;
		}
		final Hit combatHit = new Hit(damage, CombatConstants.getId(type));
		combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
		combatHit.setTarget(target);
		combatHit.setOwner(npc);
		CombatEffects.altarCombatDamage(npc, target, combatHit);
		if (combatHit.getDamage() > target.getHp()) {
			combatHit.setDamage(target.getHp());
		}
		return combatHit;
	}

	@Override
	public CombatType getCombatType(NPC npc, Entity target) {
		// TODO Auto-generated method stub
		int health = npc.getHp();
		int percentage = (int) (npc.getMaxHp() * 0.15);
		int food_left = npc.getAttributes().getInt("food_count");
		if (food_left == -1) {
			npc.getAttributes().set("food_count", 20);
		}
		if (food_left > 0) {
			if (health < percentage) {
				return CombatType.MONKEY_GUARD_HEAL;
			}
		}
		CombatType type = null;
		if (!Location.isWithinDistance(npc, target, 1)) {
			switch (NumberUtils.random(1)) {
			case 0:
				type = CombatType.MAGE;
				break;
			case 1:
				type = CombatType.RANGE;
				break;
			}
		} else {
			switch (NumberUtils.random(2)) {
			case 0:
				type = CombatType.MAGE;
				break;
			case 1:
				type = CombatType.RANGE;
				break;
			case 2:
				type = CombatType.MELEE;
				break;
			}
		}
		return type;
	}

	@Override
	public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		int distance = 1;
		if (!(type.equals(CombatType.MELEE))) {
			distance = 8;
		}
		if (Location.standingOn(npc, target)) {
			return false;
		}
		return Location.isWithinDistance(npc, target, distance);
	}
	
	private static void freeze(final NPC npc, final Entity target) {
		if (target.getAttributes().isSet("stopActions")) {
			return;
		}
		if (target.getCombatState().isFreezable()) {
			MainCombat.endCombat(target, 1);
			target.getCombatState().setFreezable(false);
			target.getCombatState().setFrozen(true);
			final int finalTimer = 15;
			World.getWorld().submit(new Tickable(target, 1) {
				int count = finalTimer;
				@Override
				public void execute() {
					if (!Location.isWithinDistance(npc, target, 11)) {
						target.getCombatState().setFrozen(false);
						World.getWorld().submit(new Tickable(7) {
							@Override
							public void execute() {
								this.stop();
								target.getCombatState().setFreezable(true);
							}
						});
						this.stop();
						return;
					}
					if (count > 0) {
						count--;
						return;
					} else if (count == 0) {
						this.stop();
						target.getCombatState().setFrozen(false);
						World.getWorld().submit(new Tickable(7) {
							@Override
							public void execute() {
								this.stop();
								target.getCombatState().setFreezable(true);
							}
						});
						return;
					}
				}
			});
		}
	}
}
