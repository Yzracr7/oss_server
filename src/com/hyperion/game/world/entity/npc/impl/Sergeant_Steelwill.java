package com.hyperion.game.world.entity.npc.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.TeleportAreaLocations;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatConstants;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.utility.NumberUtils;

public class Sergeant_Steelwill extends AbstractNPCAttacks {

	@Override
	public void executeAttacks(final NPC npc, final Entity main_target) {
		// TODO Auto-generated method stub
		CombatType type = getCombatType(npc, main_target);
		npc.getCombatState().setCombatType(type);
		if (!isWithinDistance(npc, main_target, type) || npc.getCombatState().getCombatCycles() > 0)
			return;
		int anim = npc.getDefinition().getAttackAnimation();
		int projectile = 1203;
		Projectiles mageProj = Projectiles.create(npc.getLocation(), main_target.getLocation(), main_target, projectile, 35, 80, 50, 40, 34);
		npc.executeProjectile(mageProj);
		npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
		npc.animate(Animation.create(anim, AnimationPriority.HIGH));
		main_target.getCombatState().setCurrentAttacker(npc);
		main_target.getCombatState().setPreviousAttacker(npc);
		CombatState.refreshPreviousHit(npc, main_target);
		final Hit combatHit = getDamage(npc, main_target, type);
		final boolean splashed = combatHit.getDamage() <= 0;
		World.getWorld().submit(new Tickable(3) {
			@Override
			public void execute() {
				this.stop();
				main_target.animate(CombatAnimations.getDefendAnim(main_target));
				if (combatHit.getDamage() > main_target.getHp()) {
					combatHit.setDamage(main_target.getHp());
				}
				if (splashed) {
					main_target.playGraphic(85, 0, 100);
					return;
				}
				main_target.inflictDamage(combatHit, false);
				handleEndEffects(npc, main_target, combatHit);
			}
		});
	}

	@Override
	public void handleEndEffects(NPC npc, Entity target, Hit hit) {
		// TODO Auto-generated method stub
		CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
	}

	@Override
	public boolean canAttack(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		if(target.getLocation().getX() <= 2863
				|| target.getLocation().getY() <= 5350)
			return false;
		if (!ProjectilePathFinder.hasLineOfSight(npc, target, false)) {
			return false;
		}
		return true;
	}

	@Override
	public Hit getDamage(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		int damage = NumberUtils.random((int) npc.getDefinition().getMaxHit());
		int def = 0;
		int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
		if (target.isPlayer()) {
			Player player = (Player) target;
			if (type.equals(CombatType.MAGE)) {
				def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Magic")) {
					damage = 0;
				}
			}
		}
		if (def > accuracy) {
			damage = 0;
		}
		final Hit combatHit = new Hit(damage, CombatConstants.getId(type));
		combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
		combatHit.setTarget(target);
		combatHit.setOwner(npc);
		CombatEffects.altarCombatDamage(npc, target, combatHit);
		if (combatHit.getDamage() > target.getHp()) {
			combatHit.setDamage(target.getHp());
		}
		return combatHit;
	}

	@Override
	public CombatType getCombatType(NPC npc, Entity target) {
		// TODO Auto-generated method stub
		return CombatType.MAGE;
	}

	@Override
	public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		
		if (Location.standingOn(npc, target)) {
			return false;
		}
		return TeleportAreaLocations.atBandosChamber(target.getLocation());
	}

}
