package com.hyperion.game.world.entity.npc.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatConstants;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;

public class Spinolayp extends AbstractNPCAttacks {

	@Override
	public void executeAttacks(final NPC npc, final Entity target) {
		// TODO Auto-generated method stub
		CombatType type = getCombatType(npc, target);
		npc.getCombatState().setCombatType(type);
		if (!isWithinDistance(npc, target, type)) {
			return;
		} else {
			npc.getWalkingQueue().reset();
		}
		if (npc.getCombatState().getCombatCycles() > 0) {
			return;
		}
		int projectile = -1;
		int endGfx = -1;
		int hitDelay = -1;
		int projectileStartSpeed = 20;
		int projectileSpeed = 65;
		switch (type) {
		case MAGE:
			hitDelay = 3;
			projectile = 94;
			endGfx = 95;
			break;
		case RANGE:
			hitDelay = 2;
			projectile = 295;
			projectileSpeed = 40;
			break;
		}
		npc.animate(2868);
		npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
		if (projectile != -1) {
			Projectiles sendProj = Projectiles.create(npc.getCentreLocation(), target.getCentreLocation(), target, projectile, projectileStartSpeed, projectileSpeed, 50, 43, 35);
			npc.executeProjectile(sendProj);
		}
		target.getCombatState().setCurrentAttacker(npc);
		target.getCombatState().setPreviousAttacker(npc);
		CombatState.refreshPreviousHit(npc, target);
		final Hit combatHit = getDamage(npc, target, type);
		if (type == CombatType.MAGE) {
			if (combatHit.getDamage() <= 0) {
				endGfx = 85;
			}
		}
		final int finalEndGfx = endGfx;
		World.getWorld().submit(new Tickable(hitDelay) {
			@Override
			public void execute() {
				this.stop();
				if (finalEndGfx != -1) {
					target.playGraphic(finalEndGfx, 0, 100);
				}
				target.animate(CombatAnimations.getDefendAnim(target));
				if (combatHit.getDamage() > target.getHp()) {
					combatHit.setDamage(target.getHp());
				}
				if (finalEndGfx != 85) {
					target.inflictDamage(combatHit, false);
				}
				handleEndEffects(npc, target, combatHit);
			}
		});
	}

	@Override
	public void handleEndEffects(NPC npc, Entity target, Hit hit) {
		// TODO Auto-generated method stub
		CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
	}

	@Override
	public boolean canAttack(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public Hit getDamage(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		int damage = NumberUtils.random((int) 10);
		int def = 0;
		int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
		if (target.isPlayer()) {
			Player player = (Player) target;
			if (type.equals(CombatType.RANGE)) {
				def = NumberUtils.random((int) HitFormula.calculateRangedDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Ranged")) {
					damage = 0;
				}
			} else if (type.equals(CombatType.MAGE)) {
				def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Magic")) {
					damage = 0;
				}
			}
		}
		if (def > accuracy) {
			damage = 0;
		}
		final Hit combatHit = new Hit(damage, CombatConstants.getId(type));
		combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
		combatHit.setTarget(target);
		combatHit.setOwner(npc);
		CombatEffects.altarCombatDamage(npc, target, combatHit);
		if (combatHit.getDamage() > target.getHp()) {
			combatHit.setDamage(target.getHp());
		}
		return combatHit;
	}

	@Override
	public CombatType getCombatType(NPC npc, Entity target) {
		// TODO Auto-generated method stub
		CombatType type = null;
		switch (NumberUtils.random(1)) {
		case 0:
			type = CombatType.RANGE;
            break;
        case 1:
        	type = CombatType.MAGE;
            break;
		}
		return type;
	}

	@Override
	public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		return Location.isWithinDistance(npc, target, 8);
	}

}
