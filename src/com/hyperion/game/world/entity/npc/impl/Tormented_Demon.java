package com.hyperion.game.world.entity.npc.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatConstants;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.utility.NumberUtils;
public class Tormented_Demon extends AbstractNPCAttacks {

	@Override
	public void executeAttacks(final NPC npc, final Entity main_target) {
		// TODO Auto-generated method stub
		//TormentedDemon td_session = (TormentedDemon) npc.getAttributes().get("td");
		CombatType type = getCombatType(npc, main_target);
		npc.getCombatState().setCombatType(type);
		if (!isWithinDistance(npc, main_target, type)) {
			return;
		} else {
			npc.getWalkingQueue().reset();
		}
		if (npc.getCombatState().getCombatCycles() > 0) {
			return;
		}
		if (!canAttack(npc, main_target, type)) {
			return;
		}
		int start_gfx = -1;
		int anim = npc.getDefinition().getAttackAnimation();
		Projectiles send_projectile = null;
		npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
		if (type.equals(CombatType.MELEE)) {
			anim = 10922;
			start_gfx = 1886;
		} else if (type.equals(CombatType.RANGE)) {
			anim = 10919;
			start_gfx = 1888;
			send_projectile = Projectiles.create(npc.getCentreLocation(), main_target.getLocation(), main_target, 1887, 55, 70, 50, 100, 16);
		} else if (type.equals(CombatType.MAGE)) {
			anim = 10918;
			send_projectile = Projectiles.create(npc.getCentreLocation(), main_target.getLocation(), main_target, 1884, 30, 85, 50, 100, 16);
		} else if (type.equals(CombatType.TD_GROUND_ATTACK)) {
			Location demonLoc = npc.getLocation();
			final Location tile = Location.create(demonLoc.getX() + NumberUtils.random(7), demonLoc.getY() + NumberUtils.random(7), demonLoc.getZ());
			npc.animate(10917);
			World.getWorld().send_regional_projectile(npc, demonLoc, tile, 40, 1884, 50, 34, 16, 85);
			World.getWorld().submit(new Tickable(3) {
				@Override
				public void execute() {
					this.stop();
					World.getWorld().send_regional_graphic(tile, 1883);
				}
			});
			for (final Player players : World.getRegionalPlayers(npc)) {
				if (players.getCombatState().isDead()) {
					continue;
				}
				if (!players.getLocation().withinDistance(tile, 1, false)) {
					continue;
				}
				players.getPacketSender().sendMessage("The demon's magical attack splashes on you.");
				players.getCombatState().setCurrentAttacker(npc);
				players.getCombatState().setPreviousAttacker(npc);
				CombatState.refreshPreviousHit(npc, players);
				final Hit combatHit = getDamage(npc, main_target, type);
				final boolean splashed = combatHit.getDamage() <= 0;
				World.getWorld().submit(new Tickable(3) {
					@Override
					public void execute() {
						this.stop();
						main_target.animate(CombatAnimations.getDefendAnim(main_target));
						if (combatHit.getDamage() > main_target.getHp()) {
							combatHit.setDamage(main_target.getHp());
						}
						if (splashed) {
							main_target.playGraphic(85, 0, 100);
							return;
						}
						main_target.inflictDamage(combatHit, false);
						handleEndEffects(npc, main_target, combatHit);
					}
				});
			}
			return;
		}
		if (start_gfx != -1) {
			npc.playGraphic(start_gfx);
		}
		if (send_projectile != null) {
			npc.executeProjectile(send_projectile);
		}
		npc.animate(Animation.create(anim, AnimationPriority.HIGH));
		main_target.getCombatState().setCurrentAttacker(npc);
		main_target.getCombatState().setPreviousAttacker(npc);
		CombatState.refreshPreviousHit(npc, main_target);
		final Hit combatHit = getDamage(npc, main_target, type);
		final boolean splashed = combatHit.getDamage() <= 0 && type.equals(CombatType.MAGE);
		final int ticks = type.equals(CombatType.MAGE) ? 3 : type.equals(CombatType.RANGE) ? 2 : 1;
		World.getWorld().submit(new Tickable(ticks) {
			@Override
			public void execute() {
				this.stop();
				main_target.animate(CombatAnimations.getDefendAnim(main_target));
				if (combatHit.getDamage() > main_target.getHp()) {
					combatHit.setDamage(main_target.getHp());
				}
				if (splashed) {
					main_target.playGraphic(85, 0, 100);
					return;
				}
				main_target.inflictDamage(combatHit, false);
				handleEndEffects(npc, main_target, combatHit);
			}
		});
	}

	@Override
	public void handleEndEffects(NPC npc, Entity target, Hit hit) {
		// TODO Auto-generated method stub
		CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
	}

	@Override
	public boolean canAttack(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		if (!ProjectilePathFinder.hasLineOfSight(npc, target, false)) {
			return false;
		}
		return true;
	}

	@Override
	public Hit getDamage(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		int max = 0;
		if (type.equals(CombatType.MELEE)) {
			max = 18;
		} else if (type.equals(CombatType.RANGE)) {
			 max = 27;
		} else if (type.equals(CombatType.MAGE)) {
			max = 27;
		} else if (type.equals(CombatType.TD_GROUND_ATTACK)) {
			max = 28;
		}
		int damage = NumberUtils.random((int) max);
		int def = 0;
		int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
		if (target.isPlayer()) {
			Player player = (Player) target;
			if (type.equals(CombatType.MELEE)) {
				def = NumberUtils.random((int) HitFormula.calculateMeleeDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Melee")) {
					damage = 0;
				}
			} else if (type.equals(CombatType.RANGE)) {
				def = NumberUtils.random((int) HitFormula.calculateRangedDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Ranged")) {
					damage = 0;
				}
			} else if (type.equals(CombatType.MAGE)) {
				def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Magic")) {
					damage = 0;
				}
			}
		}
		if (def > accuracy) {
			damage = 0;
		}
		final Hit combatHit = new Hit(damage, CombatConstants.getId(type));
		combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
		combatHit.setTarget(target);
		combatHit.setOwner(npc);
		CombatEffects.altarCombatDamage(npc, target, combatHit);
		if (combatHit.getDamage() > target.getHp()) {
			combatHit.setDamage(target.getHp());
		}
		return combatHit;
	}

	@Override
	public CombatType getCombatType(NPC npc, Entity target) {
		// TODO Auto-generated method stub
		CombatType type = null;
		switch (NumberUtils.random(2)) {
		case 0://melee
			type = CombatType.MELEE;
			break;
		case 1://range
			type = CombatType.MAGE;
			break;
		case 2://mage
			type = CombatType.RANGE;
			break;
		}
		if (!Location.isWithinDistance(npc, target, 1)) {
			switch (NumberUtils.random(1)) {
			case 0:
				type = CombatType.RANGE;
				break;
			case 1:
				type = CombatType.MAGE;
				break;
			}
		}
		if (NumberUtils.random(20) <= 2) {
			type = CombatType.TD_GROUND_ATTACK;
		}
		return type;
	}

	@Override
	public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		int distance = 1;
		if (!type.equals(CombatType.MELEE)) {
			distance = 8;
		}
		if (Location.standingOn(npc, target)) {
			return false;
		}
		return Location.isWithinDistance(npc, target, distance);
	}
}
