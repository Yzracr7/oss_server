package com.hyperion.game.world.entity.npc.impl;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatConstants;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.utility.NumberUtils;
public class Tzhaar_Jad extends AbstractNPCAttacks {

	@Override
	public void executeAttacks(final NPC npc, final Entity target) {
		// TODO Auto-generated method stub
		final CombatType type = getCombatType(npc, target);
		npc.getCombatState().setCombatType(type);
		if (!isWithinDistance(npc, target, type)) {
			return;
		} else {
			npc.getWalkingQueue().reset();
		}
		if (npc.getCombatState().getCombatCycles() > 0) {
			return;
		}
		if (!canAttack(npc, target, type)) {
			return;
		}
		target.getCombatState().setCurrentAttacker(npc);
		target.getCombatState().setPreviousAttacker(npc);
		CombatState.refreshPreviousHit(npc, target);
		npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
		int anim = npc.getDefinition().getAttackAnimation();
		int selfGfx = -1;
		int endGfx = -1;
		if (target.isPlayer()) {
			Player player = (Player) target;
			//if (player.getDetails().getClientMode() > 464) {
				if (type.equals(CombatType.MELEE)) {
					anim = 9277;
				} else if (type.equals(CombatType.RANGE)) {
					anim = 9276;
					selfGfx = 1625;
					endGfx = 157;
				} else if (type.equals(CombatType.MAGE)) {
					anim = 9300;
					selfGfx = 1626;
					endGfx = 157;
					Projectiles mageProj = Projectiles.create(npc.getCentreLocation(), target.getLocation(), target, 1627, 45, 90, 50, 40, 34);
					npc.executeProjectile(mageProj);
				}
		/*	} else {
				if (type.equals(CombatType.RANGE)) {
					anim = 2652;
					selfGfx = 1625;
					endGfx = 157;
				} else if (type.equals(CombatType.MAGE)) {
					anim = 2656;
					selfGfx = 1626;
					endGfx = 157;
					Projectiles mageProj = Projectiles.create(npc.getCentreLocation(), target.getLocation(), target, 1627, 65, 90, 50, 40, 34);
					npc.executeProjectile(mageProj);
				}
			}*/
		}
		if (selfGfx != -1)
			npc.playGraphic(selfGfx);
		npc.animate(Animation.create(anim, AnimationPriority.HIGH));
		final int finalEndGfx = endGfx;
		final int maxTicks = type.equals(CombatType.MELEE) ? 1 : 3;
		World.getWorld().submit(new Tickable(1) {
			int count = maxTicks;
			int status = 0;
			@Override
			public void execute() {
				if (count > 0) {
					count--;
					return;
				}
				if (type.equals(CombatType.RANGE)) {
					if (status == 0) {
						count = maxTicks;
						status++;
						target.playGraphic(451);
						return;
					}
				}
				this.stop();
				if (finalEndGfx != -1) {
					target.playGraphic(finalEndGfx);
				}
				target.animate(CombatAnimations.getDefendAnim(target));
				final Hit combatHit = getDamage(npc, target, type);
				if (combatHit.getDamage() > target.getHp()) {
					combatHit.setDamage(target.getHp());
				}
				target.inflictDamage(combatHit, false);
				handleEndEffects(npc, target, combatHit);
			}
		});
		
	}

	@Override
	public void handleEndEffects(NPC npc, Entity target, Hit hit) {
		// TODO Auto-generated method stub
		CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
	}

	@Override
	public boolean canAttack(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		if (!ProjectilePathFinder.hasLineOfSight(npc, target, false)) {
			return false;
		}
		return true;
	}

	@Override
	public Hit getDamage(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		int damage = 0;
		if (type.equals(CombatType.MAGE) || type.equals(CombatType.RANGE)) {
			damage = NumberUtils.random((int) 96);
		} else {
			damage = NumberUtils.random((int) npc.getDefinition().getMaxHit());
		}
		int def = 0;
		int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
		if (target.isPlayer()) {
			Player player = (Player) target;
			switch (type) {
			case MAGE:
				def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
				if (player.getPrayers().isPrayerActive("protect from magic")) {
					damage = 0;
				}
				break;
			case RANGE:
				def = NumberUtils.random((int) HitFormula.calculateRangedDefencePVM(player));
				if (player.getPrayers().isPrayerActive("protect from ranged")) {
					damage = 0;
				}
				break;
			case MELEE:
				def = NumberUtils.random((int) HitFormula.calculateMeleeDefencePVM(player));
				if (player.getPrayers().isPrayerActive("protect from melee")) {
					damage = 0;
				}
				break;
			}
		}
		if (def > accuracy) {
			damage = 0;
		}
		final Hit combatHit = new Hit(damage, CombatConstants.getId(type));
		combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
		combatHit.setTarget(target);
		combatHit.setOwner(npc);
		CombatEffects.altarCombatDamage(npc, target, combatHit);
		if (combatHit.getDamage() > target.getHp()) {
			combatHit.setDamage(target.getHp());
		}
		return combatHit;
	}

	@Override
	public CombatType getCombatType(NPC npc, Entity target) {
		// TODO Auto-generated method stub
		CombatType type = CombatType.MELEE;
		if (!Location.isWithinDistance(npc, target, 1)) {
			switch (NumberUtils.random(1)) {
			case 0:
				type = CombatType.RANGE;
                break;
            case 1:
            	type = CombatType.MAGE;
                break;
			}
		} else {
			switch (NumberUtils.random(2)) {
			case 0:
				type = CombatType.MELEE;
                break;
            case 1:
            	type = CombatType.MAGE;
                break;
            case 2:
            	type = CombatType.RANGE;
            	break;
			}
		}
		return type;
	}

	@Override
	public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		int distance = 1;
		if (!(type.equals(CombatType.MELEE))) {
			distance = 8;
		}
		if (Location.standingOn(npc, target)) {
			return false;
		}
		return Location.isWithinDistance(npc, target, distance);
	}

}
