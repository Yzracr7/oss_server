package com.hyperion.game.world.entity.npc.impl.godwars;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.impl.AbstractNPCAttacks;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;

/**
 * Hillside Trolls
 *
 * @author trees
 */
public class ThrowerTrolls extends AbstractNPCAttacks {

    @Override
    public void executeAttacks(final NPC npc, final Entity target) {
        CombatType type = getCombatType(npc, target);
        npc.getCombatState().setCombatType(type);
        if (!isWithinDistance(npc, target, type)) {
            return;
        } else {
            npc.getWalkingQueue().reset();
        }
        if (npc.getCombatState().getCombatCycles() > 0) {
            return;
        }
        int projectile = -1;
        int endGfx = -1;
        int hitDelay = -1;
        int projectileStartSpeed = 15;
        int projectileSpeed = 20;
        switch (type) {
            case RANGE:
                hitDelay = 2;
                projectile = 276;
                projectileSpeed = 22;
        }
        npc.animate(npc.getDefinition().getAttackAnimation());

        npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
        if (projectile != -1) {
            Projectiles sendProj = Projectiles.create(npc.getCentreLocation(), target.getCentreLocation(), target, projectile, projectileStartSpeed, projectileSpeed, 50, 43, 35);
            npc.executeProjectile(sendProj);
        }
        target.getCombatState().setCurrentAttacker(npc);
        target.getCombatState().setPreviousAttacker(npc);
        CombatState.refreshPreviousHit(npc, target);
        final Hit combatHit = getDamage(npc, target, type);
        if (type == CombatType.RANGE) {
            if (combatHit.getDamage() <= 0) {
                endGfx = -1;
            }
        }
        final int finalEndGfx = endGfx;
        World.getWorld().submit(new Tickable(hitDelay) {
            @Override
            public void execute() {
                this.stop();
                if (finalEndGfx != -1) {
                    target.playGraphic(finalEndGfx, 0, 100);
                }
                target.animate(CombatAnimations.getDefendAnim(target));
                if (combatHit.getDamage() > target.getHp()) {
                    combatHit.setDamage(target.getHp());
                }
                if (finalEndGfx != 85) {
                    target.inflictDamage(combatHit, false);
                }
                handleEndEffects(npc, target, combatHit);
            }
        });
    }

    @Override
    public void handleEndEffects(NPC npc, Entity target, Hit hit) {
        CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
    }

    @Override
    public boolean canAttack(NPC npc, Entity target, CombatType type) {
        return true;
    }

    @Override
    public Hit getDamage(NPC npc, Entity target, CombatType type) {
        int damage = NumberUtils.random((int) 10);
        int def = 0;
        int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
        if (target.isPlayer()) {
            Player player = (Player) target;
            if (type.equals(CombatType.RANGE)) {
                def = NumberUtils.random((int) HitFormula.calculateRangedDefencePVM(player));
                if (player.getPrayers().isPrayerActive("Protect from Ranged")) {
                    damage = 0;
                }

            }
        }
        if (def > accuracy) {
            damage = 0;
        }
        final Hit combatHit = new Hit(damage, 1);
        combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
        combatHit.setTarget(target);
        combatHit.setOwner(npc);
        CombatEffects.altarCombatDamage(npc, target, combatHit);
        if (combatHit.getDamage() > target.getHp()) {
            combatHit.setDamage(target.getHp());
        }
        return combatHit;
    }

    @Override
    public CombatType getCombatType(NPC npc, Entity target) {
        CombatType type = null;
        switch (NumberUtils.random(0)) {
            case 0:
                type = CombatType.RANGE;
                break;
        }
        return type;
    }

    @Override
    public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
        return Location.isWithinDistance(npc, target, 20);
    }

}
