package com.hyperion.game.world.entity.npc.impl.godwars.armadyl;

import java.util.ArrayList;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.TeleportAreaLocations;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.impl.AbstractNPCAttacks;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.utility.NumberUtils;
public class Armadyl_Kree extends AbstractNPCAttacks {

	@Override
	public void executeAttacks(final NPC npc, final Entity target) {
		// TODO Auto-generated method stub
		CombatType type = getCombatType(npc, target);
		npc.getCombatState().setCombatType(type);
		if (!isWithinDistance(npc, target, type)) {
			return;
		} else {
			npc.getWalkingQueue().reset();
		}
		if (npc.getCombatState().getCombatCycles() > 0) {
			return;
		}
		if (!canAttack(npc, target, type)) {
			return;
		}
		int anim = npc.getDefinition().getAttackAnimation();
		int projectile = -1;
		int endGfx = -1;
		int speed = npc.getDefinition().getAttackSpeed();
		if (type.equals(CombatType.RANGE)) {
			anim = 6976;
			projectile = 1197;
			speed = 5;
		} else if (type.equals(CombatType.MAGE)) {
			anim = 6976;
			projectile = 1198;
			endGfx = 1196;
			speed = 5;
		}
		npc.animate(Animation.create(anim, AnimationPriority.HIGH));
		npc.getCombatState().setCombatCycles(speed);
		if (!type.equals(CombatType.MELEE)) {
			for (Entity targets : getPossibleTargets(npc, target)) {
				final Player pTargets = (Player) targets;
				if (!ProjectilePathFinder.hasLineOfSight(npc, pTargets, false)) {
					continue;
				}
				Projectiles send_projectile = Projectiles.create(npc.getCentreLocation(), pTargets.getLocation(), pTargets, projectile, 32, 70, 50, 40, 34);
				npc.executeProjectile(send_projectile);
				pTargets.getCombatState().setCurrentAttacker(npc);
				pTargets.getCombatState().setPreviousAttacker(npc);
				CombatState.refreshPreviousHit(npc, pTargets);
				final Hit combatHit = getDamage(npc, pTargets, type);
				if (combatHit.getDamage() <= 0) {
					endGfx = 85;
				}
				final int finalEndGfx = endGfx;
				final int tick = type.equals(CombatType.MAGE) ? 3 : 2;
				World.getWorld().submit(new Tickable(tick) {
					@Override
					public void execute() {
						this.stop();
						if (finalEndGfx != -1) {
							pTargets.playGraphic(finalEndGfx, 0, finalEndGfx == 85 ? 100 : 0);
						}
						pTargets.animate(Animation.create(CombatAnimations.getDefendAnim(pTargets), AnimationPriority.NONE));
						if (combatHit.getDamage() > pTargets.getHp()) {
							combatHit.setDamage(pTargets.getHp());
						}
						if (finalEndGfx != 85) {
							pTargets.inflictDamage(combatHit, false);
							handleEndEffects(npc, pTargets, combatHit);
						}
					}
				});
			}
		} else {
			target.getCombatState().setCurrentAttacker(npc);
			target.getCombatState().setPreviousAttacker(npc);
			CombatState.refreshPreviousHit(npc, target);
			final Hit combatHit = getDamage(npc, target, type);
			World.getWorld().submit(new Tickable(1) {
				@Override
				public void execute() {
					this.stop();
					target.animate(CombatAnimations.getDefendAnim(target));
					if (combatHit.getDamage() > target.getHp()) {
						combatHit.setDamage(target.getHp());
					}
					target.inflictDamage(combatHit, false);
					handleEndEffects(npc, target, combatHit);
				}
			});
		}
	}

	@Override
	public void handleEndEffects(NPC npc, Entity target, Hit hit) {
		// TODO Auto-generated method stub
		CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
	}

	@Override
	public boolean canAttack(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		if (!ProjectilePathFinder.hasLineOfSight(npc, target, false)) {
			return false;
		}
		return true;
	}

	@Override
	public Hit getDamage(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		final int maxHit = type.equals(CombatType.RANGE) ? 71 : 25;
		int damage = NumberUtils.random((int) (maxHit));
		int def = 0;
		int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
		if (target.isPlayer()) {
			Player player = (Player) target;
			if (type.equals(CombatType.MELEE)) {
				def = NumberUtils.random((int) HitFormula.calculateMeleeDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Melee")) {
					damage = 0;
				}
			} else if (type.equals(CombatType.MAGE)) {
				def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Magic")) {
					damage = 0;
				}
			} else if (type.equals(CombatType.RANGE)) {
				def = NumberUtils.random((int) HitFormula.calculateRangedDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Ranged")) {
					damage = 0;
				}
			}
		}
		if (def > accuracy) {
			damage = 0;
		}
		final Hit combatHit = new Hit(damage, type == CombatType.RANGE ? 1 : type == CombatType.MAGE ? 2 : 0);
		combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
		combatHit.setTarget(target);
		combatHit.setOwner(npc);
		CombatEffects.altarCombatDamage(npc, target, combatHit);
		if (combatHit.getDamage() > target.getHp()) {
			combatHit.setDamage(target.getHp());
		}
		return combatHit;
	}

	@Override
	public CombatType getCombatType(NPC npc, Entity target) {
		// TODO Auto-generated method stub
		CombatType type = CombatType.MELEE;
		if (!Location.isWithinDistance(npc, target, 1)) {
			switch (NumberUtils.random(1)) {
			case 0:
				type = CombatType.RANGE;
				break;
			case 1:
				type = CombatType.MAGE;
				break;
			}
		} else {
			switch (NumberUtils.random(2)) {
			case 0:
				type = CombatType.RANGE;
				break;
			case 1:
				type = CombatType.MAGE;
				break;
			}
		}
		return type;
	}

	@Override
	public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
		// TODO Auto-generated method stub
		int distance = 1;
		if (!(type.equals(CombatType.MELEE))) {
			distance = 8;
		}
		if (Location.standingOn(npc, target)) {
			return false;
		}
		return Location.isWithinDistance(npc, target, distance);
	}
	
	private static ArrayList<Entity> getPossibleTargets(NPC npc, Entity mainTarget) {
		ArrayList<Entity> possibleTarget = new ArrayList<Entity>();
		for (Player targets : World.getRegionalPlayers(npc)) {
			if (targets == null)
				continue;
			if (targets.isNPC())
				continue;
			if (targets.getCombatState().isDead())
				continue;
			if (!TeleportAreaLocations.atArmadylChamber(targets.getLocation()))
				continue;
			possibleTarget.add(targets);
		}
		return possibleTarget;
	}

}
