package com.hyperion.game.world.entity.npc.impl.godwars.saradomin;

import java.util.ArrayList;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.TeleportAreaLocations;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatConstants;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.impl.AbstractNPCAttacks;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;
/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Apr 4, 2015
 */
public class Commander_Zilyana extends AbstractNPCAttacks {

	@Override
	public void executeAttacks(NPC npc, Entity main_target) {
		CombatType type = getCombatType(npc, main_target);
		npc.getCombatState().setCombatType(type);
		if (!isWithinDistance(npc, main_target, type)) {
			return;
		} else {
			npc.getWalkingQueue().reset();
		}
		if (npc.getCombatState().getCombatCycles() > 0) {
			return;
		}
		if (!canAttack(npc, main_target, type)) {
			return;
		}
		int anim = npc.getDefinition().getAttackAnimation();
		if (type.equals(CombatType.MAGE)) {
			anim = 6967;
		}
		npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
		npc.animate(Animation.create(anim, AnimationPriority.HIGH));
		main_target.getCombatState().setCurrentAttacker(npc);
		main_target.getCombatState().setPreviousAttacker(npc);
		CombatState.refreshPreviousHit(npc, main_target);
		final Hit combatHit = getDamage(npc, main_target, type);
		final int tick = type.equals(CombatType.MAGE) ? 2 : 1;
		if (type.equals(CombatType.MAGE)) {
			for (final Entity targets : getPossibleTargets(npc, main_target)) {
				if (targets.isNPC())
					continue;
				targets.getCombatState().setCurrentAttacker(npc);
				targets.getCombatState().setPreviousAttacker(npc);
				CombatState.refreshPreviousHit(npc, targets);
				final Hit magic_hit = getDamage(npc, targets, type);
				World.getWorld().submit(new Tickable(tick) {
					@Override
					public void execute() {
						this.stop();
						targets.animate(CombatAnimations.getDefendAnim(targets));
						if (magic_hit.getDamage() > targets.getHp()) {
							magic_hit.setDamage(targets.getHp());
						}
						if (magic_hit.getDamage() > 0) {
							targets.playGraphic(1194);
						} else {
							targets.playGraphic(85);
						}
						targets.inflictDamage(magic_hit, false);
						handleEndEffects(npc, targets, magic_hit);
					}
				});
			}
		} else {
			World.getWorld().submit(new Tickable(tick) {
				@Override
				public void execute() {
					this.stop();
					main_target.animate(CombatAnimations.getDefendAnim(main_target));
					if (combatHit.getDamage() > main_target.getHp()) {
						combatHit.setDamage(main_target.getHp());
					}
					main_target.inflictDamage(combatHit, false);
					handleEndEffects(npc, main_target, combatHit);
				}
			});
		}
	}

	@Override
	public void handleEndEffects(NPC npc, Entity target, Hit hit) {
		CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
	}

	@Override
	public boolean canAttack(NPC npc, Entity target, CombatType type) {
		return target.getLocation().getX() >= 2888 && target.getLocation().getX() <= 2907;
	}

	@Override
	public Hit getDamage(NPC npc, Entity target, CombatType type) {
		int max = npc.getDefinition().getMaxHit();
		if (type.equals(CombatType.MAGE)) {
			max = 35;
		}
		int damage = NumberUtils.random((int) max);
		int def = 0;
		int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
		if (target.isPlayer()) {
			Player player = (Player) target;
			if (type.equals(CombatType.MELEE)) {
				def = NumberUtils.random((int) HitFormula.calculateMeleeDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Melee")) {
					damage = 0;
				}
			} else if (type.equals(CombatType.MELEE)) {
				def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Magic")) {
					damage = 0;
				}
			}
		}
		if (def > accuracy) {
			damage = 0;
		}
		final Hit combatHit = new Hit(damage, type == CombatType.MAGE ? CombatConstants.getId(CombatType.MAGE) : 0);
		combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
		combatHit.setTarget(target);
		combatHit.setOwner(npc);
		CombatEffects.altarCombatDamage(npc, target, combatHit);
		if (combatHit.getDamage() > target.getHp()) {
			combatHit.setDamage(target.getHp());
		}
		randomMessage(npc);
		return combatHit;
	}


	@Override
	public CombatType getCombatType(NPC npc, Entity target) {
		CombatType type = CombatType.MELEE;
		if (!Location.isWithinDistance(npc, target, 1)) {
			type = CombatType.MAGE;
		}
		return type;
	}

	@Override
	public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
		int distance = 1;
		if (!(type.equals(CombatType.MELEE))) {
			distance = 8;
		}
		return Location.isWithinDistance(npc, target, distance);
	}

	private static final String[] SHOUTS = { "Death to the enemies of the light!", "Slay the evil ones!", "Saradomin lend me strength!", "By the power of Saradomin!", "May Saradomin be my sword.", "Good will always triumph!", "Forward! Our allies are with us!", "Saradomin is with us!", "In the name of Saradomin!", "Attack! Find the Godsword!", };

	private static void randomMessage(NPC boss) {
		if (NumberUtils.random(2) != 0) {
			return;
		}
		String message = SHOUTS[NumberUtils.random(SHOUTS.length - 1)];
		boss.playForcedChat(message);
	}
	
	private static ArrayList<Entity> getPossibleTargets(NPC npc, Entity mainTarget) {
		ArrayList<Entity> possibleTarget = new ArrayList<Entity>();
		for (Player targets : World.getRegionalPlayers(npc)) {
			if (targets == null)
				continue;
			if (targets.getCombatState().isDead())
				continue;
			if (!TeleportAreaLocations.atSaradominChamber(targets.getLocation()))
				continue;
			possibleTarget.add(targets);
		}
		return possibleTarget;
	}
}
