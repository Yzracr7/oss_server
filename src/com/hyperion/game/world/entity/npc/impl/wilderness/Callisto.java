package com.hyperion.game.world.entity.npc.impl.wilderness;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.ForceMovement;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.impl.AbstractNPCAttacks;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.utility.NumberUtils;

public class Callisto extends AbstractNPCAttacks {

    private boolean roar = false, shockwave = false, magic = false, heal = false;

    @Override
    public void executeAttacks(final NPC npc, final Entity target) {
        CombatType type = getCombatType(npc, target);
        npc.getCombatState().setCombatType(type);
        if (!isWithinDistance(npc, target, type)) {
            return;
        } else {
            npc.getWalkingQueue().reset();
        }
        if (npc.getCombatState().getCombatCycles() > 0) {
            return;
        }
        if (!canAttack(npc, target, type)) {
            return;
        }
        int speed = npc.getDefinition().getAttackSpeed();
        if (NumberUtils.random(0, 10) > 7) {
            roar = true;
        }
        if (NumberUtils.random(0, 10) > 7) {
            shockwave = true;
        }
        if (NumberUtils.random(0, 10) > 7) {
            magic = true;
        }
        if (NumberUtils.random(0, 10) > 7) {
            heal = true;
        }
        npc.getCombatState().setCombatCycles(speed);
        if (roar) {
            roar(npc, (Player) target, type);
        } else if (shockwave) {
            shockwave(npc, (Player) target, type);
        } else if (magic) {
            magic(npc, (Player) target, type);
        } else if (heal) {
            heal(npc, (Player) target, type);
        } else {
            npc.animate(Animation.create(4925, AnimationPriority.HIGH));
            target.getCombatState().setCurrentAttacker(npc);
            target.getCombatState().setPreviousAttacker(npc);
            CombatState.refreshPreviousHit(npc, target);
            final Hit combatHit = getDamage(npc, target, type);
            World.getWorld().submit(new Tickable(1) {
                @Override
                public void execute() {
                    this.stop();
                    target.animate(CombatAnimations.getDefendAnim(target));
                    if (combatHit.getDamage() > target.getHp()) {
                        combatHit.setDamage(target.getHp());
                    }
                    target.inflictDamage(combatHit, false);
                    handleEndEffects(npc, target, combatHit);
                }
            });
        }
        roar = false;
    }

    private void roar(NPC npc, Player player, CombatType type) {
        player.getCombatState().setCurrentAttacker(npc);
        player.getCombatState().setPreviousAttacker(npc);
        CombatState.refreshPreviousHit(npc, player);
        final Hit combatHit = getDamage(npc, player, type);
        if (npc.isHealing) {
            healDamage(npc, player, type);
            npc.isHealing = false;
        }
        player.getPacketSender().sendMessage("Callisto's roar throws you backwards.");
        npc.playGraphic(1255, 0, 0);
        npc.animate(Animation.create(4922, AnimationPriority.HIGH));
        final Location playerLoc = player.getLocation();
        int random2 = NumberUtils.random((int) (1));
        if (random2 == 0) {
            int random = NumberUtils.random((int) (10));
            player.getVariables().setNextForceMovement(new ForceMovement(playerLoc, 33, new Location(player.getLocation().getX() - random, player.getLocation().getY() - random, player.getLocation().getZ()), 60, npc.getDirection()));
            player.getUpdateFlags().flag(UpdateFlag.FORCE_MOVEMENT);
            World.getWorld().submit(new Tickable(2) {
                @Override
                public void execute() {
                    player.getAttributes().remove("stopActions");

                    player.teleport(new Location(player.getLocation().getX() - random, player.getLocation().getY() - random, player.getLocation().getZ()));
                    this.stop();
                }
            });
        } else {
            int random = NumberUtils.random((int) (10));
            player.getVariables().setNextForceMovement(new ForceMovement(playerLoc, 33, new Location(player.getLocation().getX() + random, player.getLocation().getY() + random, player.getLocation().getZ()), 60, npc.getDirection()));
            player.getUpdateFlags().flag(UpdateFlag.FORCE_MOVEMENT);
            World.getWorld().submit(new Tickable(2) {
                @Override
                public void execute() {
                    player.getAttributes().remove("stopActions");
                    player.teleport(new Location(player.getLocation().getX() + random, player.getLocation().getY() + random, player.getLocation().getZ()));
                    this.stop();
                }
            });
        }
        player.animate(Animation.create(1157, AnimationPriority.NONE));
        if (combatHit.getDamage() > player.getHp()) {
            combatHit.setDamage(player.getHp());
        }
        player.inflictDamage(combatHit, false);
        handleEndEffects(npc, player, combatHit);
        player.getAttributes().set("stopMovement", true);
        player.getCombatState().increaseCombatCycles(5);
        World.getWorld().submit(new Tickable(6) {
            @Override
            public void execute() {
                this.stop();
                player.getAttributes().remove("stopMovement");
            }
        });
    }

    private void heal(NPC npc, Player player, CombatType type) {
        npc.isHealing = true;
        player.sendMessage("Callisto absorbs his next attack, healing himself a bit.");
        npc.playGraphic(157, 0, 0);
    }

    private void shockwave(NPC npc, Player player, CombatType type) {
        player.getCombatState().setCurrentAttacker(npc);
        player.getCombatState().setPreviousAttacker(npc);
        CombatState.refreshPreviousHit(npc, player);
        final Hit combatHit = getDamage(npc, player, type);
        final Hit combatHit2 = getDamage(npc, player, type);
        if (npc.isHealing) {
            healDamage(npc, player, type);
            npc.isHealing = false;
        }
        player.getPacketSender().sendMessage("Callisto's fury sends an almighty shockwave through you.");
        npc.animate(Animation.create(4922, AnimationPriority.HIGH));
        player.playGraphic(80, 0, 100);
        if (combatHit.getDamage() > player.getHp()) {
            combatHit.setDamage(player.getHp());
        }
        if (combatHit2.getDamage() > player.getHp()) {
            combatHit2.setDamage(player.getHp());
        }
        player.inflictDamage(combatHit, false);
        handleEndEffects(npc, player, combatHit);
        player.inflictDamage(combatHit2, false);
        handleEndEffects(npc, player, combatHit2);
        player.getCombatState().increaseCombatCycles(5);
        World.getWorld().submit(new Tickable(6) {
            @Override
            public void execute() {
                this.stop();
                player.getAttributes().remove("stopMovement");
            }
        });
    }

    public void healDamage(NPC npc, Player player, CombatType type) {
        final Hit combatHit = getDamage(npc, player, type);
        npc.setHp(npc.getHp() + combatHit.getDamage());
    }

    private void magic(NPC npc, Player player, CombatType type) {
        if (!ProjectilePathFinder.hasLineOfSight(npc, player, false)) {
            return;
        }
        if (!canAttack(npc, player, type)) {
            return;
        }
        int projectile = 395;
        npc.animate(Animation.create(4925, AnimationPriority.HIGH));
        Projectiles send_projectile = Projectiles.create(new Location(npc.getLocation().getX(), npc.getLocation().getY(), npc.getLocation().getZ()), new Location(player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ()), player, projectile, 32, 70, 50, 40, 34);
        npc.executeProjectile(send_projectile);
        CombatState.refreshPreviousHit(npc, player);
        final Hit combatHit = getDamage(npc, player, type);
        if (npc.isHealing) {
            healDamage(npc, player, type);
            npc.isHealing = false;
        }
        final int tick = 3;
        World.getWorld().submit(new Tickable(tick) {
            @Override
            public void execute() {
                this.stop();
                player.playGraphic(431, 0, 100);
                player.animate(Animation.create(CombatAnimations.getDefendAnim(player), AnimationPriority.NONE));
                if (combatHit.getDamage() > player.getHp()) {
                    combatHit.setDamage(player.getHp());
                }
                player.inflictDamage(combatHit, false);
                handleEndEffects(npc, player, combatHit);
            }
        });
    }

    @Override
    public void handleEndEffects(NPC npc, Entity target, Hit hit) {
        // TODO Auto-generated method stub
        CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
    }

    @Override
    public boolean canAttack(NPC npc, Entity target, CombatType type) {
        // TODO Auto-generated method stub
        if (!ProjectilePathFinder.hasLineOfSight(npc, target, false)) {
            return false;
        }
        return true;
    }

    @Override
    public Hit getDamage(NPC npc, Entity target, CombatType type) {
        final int maxHit = roar ? 60 : 30;
        int damage = maxHit;
        int def = 0;
        int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
        if (target.isPlayer()) {
            Player player = (Player) target;
            if (type.equals(CombatType.MELEE)) {
                def = NumberUtils.random((int) HitFormula.calculateMeleeDefencePVM(player));
                if (player.getPrayers().isPrayerActive("Protect from Melee")) {
                    damage *= roar ? 0.70 : 0.60;
                }
            }
          //  player.sendMessage("npc accuracy roll : " + (accuracy * 2) + " / player def roll : " + def);
            if (def > (accuracy * 2)) {
                damage = 0;
            } else {
                damage = (NumberUtils.random(damage));
            }
        }
        final Hit combatHit = new Hit(damage, 0);
        combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
        combatHit.setTarget(target);
        combatHit.setOwner(npc);
        CombatEffects.altarCombatDamage(npc, target, combatHit);
        if (combatHit.getDamage() > target.getHp()) {
            combatHit.setDamage(target.getHp());
        }
        return combatHit;
    }

    @Override
    public CombatType getCombatType(NPC npc, Entity target) {
        // TODO Auto-generated method stub
        return CombatType.MELEE;
    }

    @Override
    public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
        // TODO Auto-generated method stub
        int distance = 1;
        if (Location.standingOn(npc, target)) {
            return false;
        }
        return Location.isWithinDistance(npc, target, distance);
    }
}

