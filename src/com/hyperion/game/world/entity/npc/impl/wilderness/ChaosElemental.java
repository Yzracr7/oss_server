package com.hyperion.game.world.entity.npc.impl.wilderness;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.impl.AbstractNPCAttacks;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.utility.NumberUtils;

public class ChaosElemental extends AbstractNPCAttacks {
	
	CombatType type;

	@Override
	public void executeAttacks(final NPC npc, final Entity target) {
		type = getCombatType(npc, target);
		npc.getCombatState().setCombatType(type);
		if (!isWithinDistance(npc, target, type)) {
			return;
		} else {
			npc.getWalkingQueue().reset();
		}
		if (npc.getCombatState().getCombatCycles() > 0) {
			return;
		}
		if (!canAttack(npc, target, type)) {
			return;
		}
		if (isWithinDistance(npc, target, CombatType.MAGE)) {
			switch (NumberUtils.random(5)) {
			case 0:
				type = CombatType.MAGE;
				break;
			case 1:
				type = CombatType.RANGE;
				break;
			case 2:
				type = CombatType.MELEE;
				break;
			case 3:
				type = CombatType.TELEOTHER;
				break;
			case 4:
				Player player = ((Player)target);
				if (player.getInventory().freeSlots() > 0) {
					if (player.getEquipment().freeSlots() != 14) {
						type = CombatType.DISARM;
					} else {
						int newType = NumberUtils.random(4);
						type = newType == 0 ? CombatType.TELEOTHER : newType == 1 ? CombatType.MAGE : newType == 2 ? CombatType.RANGE : CombatType.MELEE;
					}
				}
				break;
			}
		}
		switch (type) {
		case DISARM:
			npc.animate(Animation.create(3146));
			npc.executeProjectile(Projectiles.create(npc.getCentreLocation(), target.getCentreLocation(), target, 551,
					20, 75, 25, 43, 35, 10, 48));
			break;
		case TELEOTHER:
			npc.animate(Animation.create(3146));
			npc.executeProjectile(Projectiles.create(npc.getCentreLocation(), target.getCentreLocation(), target, 554,
					20, 75, 25, 43, 35, 10, 48));
			break;
		case MAGE:
			npc.animate(Animation.create(3146));
			npc.executeProjectile(Projectiles.create(npc.getCentreLocation(), target.getCentreLocation(), target, 557,
					20, 75, 25, 43, 35, 10, 48));
			break;
		case RANGE:
			npc.animate(Animation.create(3146));
			npc.executeProjectile(Projectiles.create(npc.getCentreLocation(), target.getCentreLocation(), target, 557,
					20, 75, 25, 43, 35, 10, 48));
			break;
		case MELEE:
			npc.animate(Animation.create(3146));
			npc.executeProjectile(Projectiles.create(npc.getCentreLocation(), target.getCentreLocation(), target, 557,
					20, 75, 25, 43, 35, 10, 48));
			break;
		default:
			break;
		}
		npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
		target.getCombatState().setCurrentAttacker(npc);
		target.getCombatState().setPreviousAttacker(npc);
		CombatState.refreshPreviousHit(npc, target);
		final Hit combatHit = getDamage(npc, target, type);
		World.getWorld().submit(new Tickable(3) {
			@Override
			public void execute() {
				this.stop();
				switch (type) {
				case DISARM:
					applyEffect(target, type);
					break;
				case TELEOTHER:
					applyEffect(target, type);
					break;
				case MAGE:
					((Player)target).playGraphic(Graphic.create(558, 0, 100));
					target.animate(CombatAnimations.getDefendAnim(target));
					if (combatHit.getDamage() > target.getHp()) {
						combatHit.setDamage(target.getHp());
					}
					target.inflictDamage(combatHit, false);
					break;
				case RANGE:
					((Player)target).playGraphic(Graphic.create(558, 0, 100));
					target.animate(CombatAnimations.getDefendAnim(target));
					if (combatHit.getDamage() > target.getHp()) {
						combatHit.setDamage(target.getHp());
					}
					target.inflictDamage(combatHit, false);
					break;
				case MELEE:
					((Player)target).playGraphic(Graphic.create(558, 0, 100));
					target.animate(CombatAnimations.getDefendAnim(target));
					if (combatHit.getDamage() > target.getHp()) {
						combatHit.setDamage(target.getHp());
					}
					target.inflictDamage(combatHit, false);
					break;
				default:
					break;
				}
			}
		});
	}

	private Location generateLocation() {
		Location loc = Location.create(3271 - NumberUtils.random(12), 3922 - NumberUtils.random(6));
		if (NumberUtils.random(1) > 0) {
			loc = Location.create(3271 + NumberUtils.random(12), 3922 - NumberUtils.random(6));
		}
		return loc;
	}

	private void applyEffect(final Entity victim, CombatType style) {
		Player player = ((Player) victim);
		switch (style) {
		case TELEOTHER:
			player.playGraphic(Graphic.create(555, 0, 100));
			player.teleport(generateLocation());
			break;
		case DISARM:
			player.playGraphic(Graphic.create(552, 0, 100));
			disarmPlayer(player);
			break;
		default:
			break;
		}
	}

	public void disarmPlayer(Player player) {
		int weapon = player.getEquipment().get(Equipment.EQUIPMENT.WEAPON.ordinal()) != null
				? player.getEquipment().get(Equipment.EQUIPMENT.WEAPON.ordinal()).getId()
				: -1;
		int shield = player.getEquipment().get(Equipment.EQUIPMENT.SHIELD.ordinal()) != null
				? player.getEquipment().get(Equipment.EQUIPMENT.SHIELD.ordinal()).getId()
				: -1;
		int amulet = player.getEquipment().get(Equipment.EQUIPMENT.AMULET.ordinal()) != null
				? player.getEquipment().get(Equipment.EQUIPMENT.AMULET.ordinal()).getId()
				: -1;
		int ammo = player.getEquipment().get(Equipment.EQUIPMENT.ARROW.ordinal()) != null
				? player.getEquipment().get(Equipment.EQUIPMENT.ARROW.ordinal()).getId()
				: -1;
		int helm = player.getEquipment().get(Equipment.EQUIPMENT.HELMET.ordinal()) != null
				? player.getEquipment().get(Equipment.EQUIPMENT.HELMET.ordinal()).getId()
				: -1;
		int boots = player.getEquipment().get(Equipment.EQUIPMENT.BOOTS.ordinal()) != null
				? player.getEquipment().get(Equipment.EQUIPMENT.BOOTS.ordinal()).getId()
				: -1;
		int body = player.getEquipment().get(Equipment.EQUIPMENT.PLATE.ordinal()) != null
				? player.getEquipment().get(Equipment.EQUIPMENT.PLATE.ordinal()).getId()
				: -1;
		int legs = player.getEquipment().get(Equipment.EQUIPMENT.LEGS.ordinal()) != null
				? player.getEquipment().get(Equipment.EQUIPMENT.LEGS.ordinal()).getId()
				: -1;
		int ring = player.getEquipment().get(Equipment.EQUIPMENT.RING.ordinal()) != null
				? player.getEquipment().get(Equipment.EQUIPMENT.RING.ordinal()).getId()
				: -1;
		int gloves = player.getEquipment().get(Equipment.EQUIPMENT.GLOVES.ordinal()) != null
				? player.getEquipment().get(Equipment.EQUIPMENT.GLOVES.ordinal()).getId()
				: -1;
		int cape = player.getEquipment().get(Equipment.EQUIPMENT.CAPE.ordinal()) != null
				? player.getEquipment().get(Equipment.EQUIPMENT.CAPE.ordinal()).getId()
				: -1;
		if (player.getInventory().hasEnoughRoomFor(weapon)) {
			if (player.getInventory().addItem(weapon)) {
				player.getEquipment().deleteItem(weapon);
			}
		}
		if (player.getInventory().hasEnoughRoomFor(shield)) {
			if (player.getInventory().addItem(shield)) {
				player.getEquipment().deleteItem(shield);
			}
		}
		if (player.getInventory().hasEnoughRoomFor(amulet)) {
			if (player.getInventory().addItem(amulet)) {
				player.getEquipment().deleteItem(amulet);
			}
		}
		if (player.getInventory().hasEnoughRoomFor(ammo)) {
			if (player.getInventory().addItem(ammo, player.getInventory().getItemAmount(ammo))) {
				player.getEquipment().deleteItem(ammo, player.getInventory().getItemAmount(ammo));
			}
		}
		if (player.getInventory().hasEnoughRoomFor(helm)) {
			if (player.getInventory().addItem(helm)) {
				player.getEquipment().deleteItem(helm);
			}
		}
		if (player.getInventory().hasEnoughRoomFor(boots)) {
			if (player.getInventory().addItem(boots)) {
				player.getEquipment().deleteItem(boots);
			}
		}
		if (player.getInventory().hasEnoughRoomFor(body)) {
			if (player.getInventory().addItem(body)) {
				player.getEquipment().deleteItem(body);
			}
		}
		if (player.getInventory().hasEnoughRoomFor(legs)) {
			if (player.getInventory().addItem(legs)) {
				player.getEquipment().deleteItem(legs);
			}
		}
		if (player.getInventory().hasEnoughRoomFor(ring)) {
			if (player.getInventory().addItem(ring)) {
				player.getEquipment().deleteItem(ring);
			}
		}
		if (player.getInventory().hasEnoughRoomFor(gloves)) {
			if (player.getInventory().addItem(gloves)) {
				player.getEquipment().deleteItem(gloves);
			}
		}
		if (player.getInventory().hasEnoughRoomFor(cape)) {
			if (player.getInventory().addItem(cape)) {
				player.getEquipment().deleteItem(cape);
			}
		}
		player.getEquipment().refresh();
		player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
	}

	@Override
	public void handleEndEffects(NPC npc, Entity target, Hit hit) {
		CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
	}

	@Override
	public boolean canAttack(NPC npc, Entity target, CombatType type) {
		if (!ProjectilePathFinder.hasLineOfSight(npc, target, false)) {
			return false;
		}
		return true;
	}

	@Override
	public Hit getDamage(NPC npc, Entity target, CombatType type) {
		int damage = NumberUtils.random((int) npc.getDefinition().getMaxHit());
		int def = 0;
		int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
		if (target.isPlayer()) {
			Player player = (Player) target;
			if (type.equals(CombatType.MAGE)) {
				def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
			} else if (type.equals(CombatType.MELEE)) {
				def = NumberUtils.random((int) HitFormula.calculateMeleeDefencePVM(player));
			} else if (type.equals(CombatType.RANGE)) {
				def = NumberUtils.random((int) HitFormula.calculateRangedDefencePVM(player));
			}
			if (type == CombatType.MAGE && player.getPrayers().isPrayerActive("Protect from Magic")) {
				damage = 0;
			} else if (type == CombatType.MELEE && player.getPrayers().isPrayerActive("Protect from Melee")) {
				damage = 0;
			} else if (type == CombatType.RANGE && player.getPrayers().isPrayerActive("Protect from Missiles")) {
				damage = 0;
			}
		}
		if (def > accuracy) {
			damage = 0;
		}
		final Hit combatHit = new Hit(damage, type == CombatType.MAGE ? 2 : 0);
		combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
		combatHit.setTarget(target);
		combatHit.setOwner(npc);
		CombatEffects.altarCombatDamage(npc, target, combatHit);
		if (combatHit.getDamage() > target.getHp()) {
			combatHit.setDamage(target.getHp());
		}
		return combatHit;
	}

	@Override
	public CombatType getCombatType(NPC npc, Entity target) {
		return CombatType.MAGE;
	}

	@Override
	public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
		return Location.isWithinDistance(npc, target, 15);
	}

}
