package com.hyperion.game.world.entity.npc.impl.wilderness;

import java.util.ArrayList;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.impl.AbstractNPCAttacks;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.utility.NumberUtils;

public class ChaosFanatic extends AbstractNPCAttacks {

	static String[] shouts = { "Burn!", "WEUGH!", "Devilish Oxen Roll!", "All your wilderness are belong to them!",
			"AhehHeheuhHhahueHuUEehEahAH", "I shall call him squidgy and he shall be my squidgy!" };

	@Override
	public void executeAttacks(final NPC npc, final Entity target) {
		CombatType type = getCombatType(npc, target);
		npc.getCombatState().setCombatType(type);
		Player player = (Player) target;
		if (!isWithinDistance(npc, target, type)) {
			return;
		} else {
			npc.getWalkingQueue().reset();
		}
		if (npc.getCombatState().getCombatCycles() > 0) {
			return;
		}
		if (!canAttack(npc, target, type)) {
			return;
		}
		npc.playForcedChat("" + shouts[NumberUtils.random(1, shouts.length - 1)]);
		int weapon = player.getEquipment().get(Equipment.EQUIPMENT.WEAPON.ordinal()) != null
				? player.getEquipment().get(Equipment.EQUIPMENT.WEAPON.ordinal()).getId()
				: -1;
		int shield = player.getEquipment().get(Equipment.EQUIPMENT.SHIELD.ordinal()) != null
				? player.getEquipment().get(Equipment.EQUIPMENT.SHIELD.ordinal()).getId()
				: -1;
		int amulet = player.getEquipment().get(Equipment.EQUIPMENT.AMULET.ordinal()) != null
				? player.getEquipment().get(Equipment.EQUIPMENT.AMULET.ordinal()).getId()
				: -1;
		int ammo = player.getEquipment().get(Equipment.EQUIPMENT.ARROW.ordinal()) != null
				? player.getEquipment().get(Equipment.EQUIPMENT.ARROW.ordinal()).getId()
				: -1;
		int helm = player.getEquipment().get(Equipment.EQUIPMENT.HELMET.ordinal()) != null
				? player.getEquipment().get(Equipment.EQUIPMENT.HELMET.ordinal()).getId()
				: -1;
		int boots = player.getEquipment().get(Equipment.EQUIPMENT.BOOTS.ordinal()) != null
				? player.getEquipment().get(Equipment.EQUIPMENT.BOOTS.ordinal()).getId()
				: -1;
		int body = player.getEquipment().get(Equipment.EQUIPMENT.PLATE.ordinal()) != null
				? player.getEquipment().get(Equipment.EQUIPMENT.PLATE.ordinal()).getId()
				: -1;
		int legs = player.getEquipment().get(Equipment.EQUIPMENT.LEGS.ordinal()) != null
				? player.getEquipment().get(Equipment.EQUIPMENT.LEGS.ordinal()).getId()
				: -1;
		int ring = player.getEquipment().get(Equipment.EQUIPMENT.RING.ordinal()) != null
				? player.getEquipment().get(Equipment.EQUIPMENT.RING.ordinal()).getId()
				: -1;
		int gloves = player.getEquipment().get(Equipment.EQUIPMENT.GLOVES.ordinal()) != null
				? player.getEquipment().get(Equipment.EQUIPMENT.GLOVES.ordinal()).getId()
				: -1;
		int cape = player.getEquipment().get(Equipment.EQUIPMENT.CAPE.ordinal()) != null
				? player.getEquipment().get(Equipment.EQUIPMENT.CAPE.ordinal()).getId()
				: -1;
		int disarmGfx = 71;
		int disarm = NumberUtils.random(550);
		if (disarm > 0 && disarm < 4 && weapon != -1) {
			if (player.getInventory().hasEnoughRoomFor(weapon)) {
				if (player.getInventory().addItem(weapon)) {
					player.getEquipment().deleteItem(weapon);
				}
			}
		} else if (disarm >= 4 && disarm < 8 && shield != -1) {
			if (player.getInventory().hasEnoughRoomFor(shield)) {
				if (player.getInventory().addItem(shield)) {
					player.getEquipment().deleteItem(shield);
				}
			}
		} else if (disarm >= 8 && disarm < 12 && amulet != -1) {
			if (player.getInventory().hasEnoughRoomFor(amulet)) {
				if (player.getInventory().addItem(amulet)) {
					player.getEquipment().deleteItem(amulet);
				}
			}
		} else if (disarm >= 12 && disarm < 16 && ammo != -1) {
			if (player.getInventory().hasEnoughRoomFor(ammo)) {
				if (player.getInventory().addItem(ammo, player.getInventory().getItemAmount(ammo))) {
					player.getEquipment().deleteItem(ammo, player.getInventory().getItemAmount(ammo));
				}
			}
		} else if (disarm >= 16 && disarm < 20 && helm != -1) {
			if (player.getInventory().hasEnoughRoomFor(helm)) {
				if (player.getInventory().addItem(helm)) {
					player.getEquipment().deleteItem(helm);
				}
			}
		} else if (disarm >= 20 && disarm < 24 && boots != -1) {
			if (player.getInventory().hasEnoughRoomFor(boots)) {
				if (player.getInventory().addItem(boots)) {
					player.getEquipment().deleteItem(boots);
				}
			}
		} else if (disarm >= 24 && disarm < 28 && body != -1) {
			if (player.getInventory().hasEnoughRoomFor(body)) {
				if (player.getInventory().addItem(body)) {
					player.getEquipment().deleteItem(body);
				}
			}
		} else if (disarm >= 28 && disarm < 32 && legs != -1) {
			if (player.getInventory().hasEnoughRoomFor(legs)) {
				if (player.getInventory().addItem(legs)) {
					player.getEquipment().deleteItem(legs);
				}
			}
		} else if (disarm >= 32 && disarm < 36 && ring != -1) {
			if (player.getInventory().hasEnoughRoomFor(ring)) {
				if (player.getInventory().addItem(ring)) {
					player.getEquipment().deleteItem(ring);
				}
			}
		} else if (disarm >= 36 && disarm < 40 && gloves != -1) {
			if (player.getInventory().hasEnoughRoomFor(gloves)) {
				if (player.getInventory().addItem(gloves)) {
					player.getEquipment().deleteItem(gloves);
				}
			}
		} else if (disarm >= 40 && disarm < 44 && cape != -1) {
			if (player.getInventory().hasEnoughRoomFor(cape)) {
				if (player.getInventory().addItem(cape)) {
					player.getEquipment().deleteItem(cape);
				}
			}
		}
		player.getEquipment().refresh();
		player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
		int anim = npc.getDefinition().getAttackAnimation();
		if (type.equals(CombatType.MAGE)) {
			anim = 811;
		}

		npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
		npc.animate(Animation.create(anim, AnimationPriority.HIGH));
		target.getCombatState().setCurrentAttacker(npc);
		target.getCombatState().setPreviousAttacker(npc);
		CombatState.refreshPreviousHit(npc, target);
		final Hit combatHit = getDamage(npc, target, type);
		final int tick = type.equals(CombatType.RANGE) ? 2 : 1;
		World.getWorld().submit(new Tickable(tick) {
			@Override
			public void execute() {
				this.stop();
				boolean special = NumberUtils.random(50) > 45;
				if (combatHit.getDamage() > target.getHp()) {
					combatHit.setDamage(target.getHp());
				}

				if (special) {
					npc.animate(3353);
					specialAttack(npc, player);
				} else {
					target.animate(CombatAnimations.getDefendAnim(target));
					Projectiles send_projectile = Projectiles.create(npc.getCentreLocation(), target.getLocation(),
							target, 554, 20, 55, 50, 55, 34);
					npc.executeProjectile(send_projectile);
					target.inflictDamage(combatHit, false);
					boolean landingGfx = NumberUtils.random(55) < 8;
					if (landingGfx) {
    						player.playGraphic(Graphic.create(71));
					}
					handleEndEffects(npc, target, combatHit);
				}
			}
		});
	}

	@Override
	public void handleEndEffects(NPC npc, Entity target, Hit hit) {
		CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
	}

	@Override
	public boolean canAttack(NPC npc, Entity target, CombatType type) {
		if (!ProjectilePathFinder.hasLineOfSight(npc, target, false)) {
			return false;
		}
		return true;
	}

	@Override
	public Hit getDamage(NPC npc, Entity target, CombatType type) {
		int damage = NumberUtils.random((int) npc.getDefinition().getMaxHit());
		int def = 0;
		int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
		if (target.isPlayer()) {
			Player player = (Player) target;
			if (type.equals(CombatType.MAGE)) {
				def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
			}
			if (player.getPrayers().isPrayerActive("Protect from Magic")) {
				damage = 0;
			}
		}

		if (def > accuracy) {
			damage = 0;
		}
		final Hit combatHit = new Hit(damage, type == CombatType.MAGE ? 2 : 0);
		combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
		combatHit.setTarget(target);
		combatHit.setOwner(npc);
		CombatEffects.altarCombatDamage(npc, target, combatHit);
		if (combatHit.getDamage() > target.getHp()) {
			combatHit.setDamage(target.getHp());
		}
		return combatHit;
	}

	@Override
	public CombatType getCombatType(NPC npc, Entity target) {
		return CombatType.MAGE;
	}

	@Override
	public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
		return Location.isWithinDistance(npc, target, 8);
	}

	private static ArrayList<Player> getPossibleTargets(NPC npc, Entity mainTarget) {
		ArrayList<Player> possibleTarget = new ArrayList<Player>();
		for (Player targets : World.getRegionalPlayers(npc)) {
			if (targets == null)
				continue;
			if (targets.getCombatState().isDead())
				continue;
			possibleTarget.add(targets);
		}
		return possibleTarget;
	}

	public void specialAttack(NPC npc, Entity target) {
		for (Entity targets : getPossibleTargets(npc, target)) {
			final Player pTargets = (Player) targets;
			handleMagicSpecial(npc, pTargets, CombatType.MAGE);
		}
	}

	private void handleMagicSpecial(NPC npc, Player pTargets, CombatType type) {
		if (!ProjectilePathFinder.hasLineOfSight(npc, pTargets, false)) {
			return;
		}

		if (!canAttack(npc, pTargets, type)) {
			return;
		}

		final Hit combatHit = getDamage(npc, pTargets, type);
		Projectiles.sendTripleProjectiles(157, 100, 551, 25, 120, 145, npc, pTargets, "", type, combatHit, 5, 1, 31,
				true);
	}

}
