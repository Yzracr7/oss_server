package com.hyperion.game.world.entity.npc.impl.wilderness;

import java.util.ArrayList;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.impl.AbstractNPCAttacks;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.utility.NumberUtils;

public class Corporeal_Beast extends AbstractNPCAttacks {

	private final Location safeSpot = Location.create(2927, 4368, 0);
	@Override
	public void executeAttacks(final NPC npc, final Entity main_target) {
		// TODO Auto-generated method stub
		CombatType type = getCombatType(npc, main_target);
		npc.getCombatState().setCombatType(type);
		if (!isWithinDistance(npc, main_target, type)) {
			return;
		} else {
			npc.getWalkingQueue().reset();
		}
		if (npc.getCombatState().getCombatCycles() > 0) {
			return;
		}
		if (!canAttack(npc, main_target, type)) {
			return;
		}
		npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
		boolean stomp = false;
		int npcX = npc.getLocation().getX();
		int npcY = npc.getLocation().getY();
		for (final Player t : getPossibleStompTargets(npc, main_target)) {
			int distanceX = t.getLocation().getX() - npcX;
			int distanceY = t.getLocation().getY() - npcY;
			if (distanceX < 4 && distanceX > -1 && distanceY < 4 && distanceY > -1) {
				stomp = true;
				t.getCombatState().setCurrentAttacker(npc);
				t.getCombatState().setPreviousAttacker(npc);
				CombatState.refreshPreviousHit(npc, t);
				final Hit stomp_hit = getDamage(npc, t, CombatType.CORP_MELEE_STOMP);
				World.getWorld().submit(new Tickable(1) {
					@Override
					public void execute() {
						this.stop();
						t.animate(CombatAnimations.getDefendAnim(t));
						if (stomp_hit.getDamage() > t.getHp()) {
							stomp_hit.setDamage(t.getHp());
						}
						t.inflictDamage(stomp_hit, false);
						handleEndEffects(npc, t, stomp_hit);
					}
				});
			}
		}
		if (stomp) {
			npc.animate(Animation.create(10496, AnimationPriority.HIGH));
			npc.playGraphic(1834);
			return;
		}
		int anim = npc.getDefinition().getAttackAnimation();
		int projectile = -1;
		if (type.equals(CombatType.MAGE)) {
			anim = 10410;
			projectile = 1825;
		} else if (type.equals(CombatType.CORP_SKILL_DRAIN)) {
			anim = 10410;
			projectile = 1823;
			int skill = NumberUtils.random(2);
			skill = skill == 0 ? Skills.MAGIC : Skills.PRAYER;
			Player player = (Player) main_target;
			if (skill == Skills.PRAYER) {
				player.getSettings().decreasePrayerPoints(1 + NumberUtils.random(4));
			} else {
				player.getSkills().deductLevel(skill, 1 + NumberUtils.random(4));
			}
			player.getPacketSender().sendMessage("Your " + Skills.SKILL_NAME[skill]+ " has been slightly drained!");
		} else if (type.equals(CombatType.CORP_MULTI_ATTACK)) {
			anim = 10410;
			projectile = 1824;
			final Location targetsTile = main_target.getLocation();
			World.getWorld().submit(new Tickable(1) {
				@Override
				public void execute() {
					this.stop();
					for (int i = 0; i < 6; i++) {
						final Location newTile = Location.create(targetsTile, 3);
						World.getWorld().send_regional_projectile(npc, targetsTile, newTile, 25, 1824, 50, 0, 0, 65);
						World.getWorld().submit(new Tickable(2) {
							@Override
							public void execute() {
								this.stop();
								World.getWorld().send_regional_graphic(newTile, 1806);
							}
						});
						for (final Player players : getPossibleTargets(npc, main_target)) {
							Location targetsLoc = players.getLocation();
							if (newTile.distanceToPoint(targetsLoc) > 0 || ProjectilePathFinder.hasLineOfSight(targetsLoc, newTile, false)) {
								continue;
							}
							players.getCombatState().setCurrentAttacker(npc);
							players.getCombatState().setPreviousAttacker(npc);
							CombatState.refreshPreviousHit(npc, players);
							final Hit combat_hit = getDamage(npc, players, CombatType.CORP_MULTI_ATTACK);
							World.getWorld().submit(new Tickable(3) {
								@Override
								public void execute() {
									this.stop();
									players.animate(CombatAnimations.getDefendAnim(players));
									if (combat_hit.getDamage() > players.getHp()) {
										combat_hit.setDamage(players.getHp());
									}
									players.inflictDamage(combat_hit, false);
									handleEndEffects(npc, players, combat_hit);
								}
							});
						}
					}
				}
			});
		}
		npc.animate(Animation.create(anim, AnimationPriority.HIGH));
		if (projectile != -1) {
			Projectiles send_projectile = Projectiles.create(npc.getCentreLocation(), main_target.getLocation(), main_target, projectile, 5, 55, 50, 60, 34);
			npc.executeProjectile(send_projectile);
		}
		main_target.getCombatState().setCurrentAttacker(npc);
		main_target.getCombatState().setPreviousAttacker(npc);
		CombatState.refreshPreviousHit(npc, main_target);
		final Hit combatHit = getDamage(npc, main_target, type);
		final int ticks = type.equals(CombatType.MELEE) ? 1 : 3;
		World.getWorld().submit(new Tickable(ticks) {
			@Override
			public void execute() {
				this.stop();
				main_target.animate(CombatAnimations.getDefendAnim(main_target));
				if (combatHit.getDamage() > main_target.getHp()) {
					combatHit.setDamage(main_target.getHp());
				}
				main_target.inflictDamage(combatHit, false);
				handleEndEffects(npc, main_target, combatHit);
			}
		});
	}

	@Override
	public void handleEndEffects(NPC npc, Entity target, Hit hit) {
		CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
	}

	@Override
	public boolean canAttack(NPC npc, Entity target, CombatType type) {
		if(target.getLocation().distanceToPoint(safeSpot) == 0)
			return true;
		if (!ProjectilePathFinder.hasLineOfSight(npc, target, false)) {
			return false;
		}
		return true;
	}

	@Override
	public Hit getDamage(NPC npc, Entity target, CombatType type) {
		int max_hit = npc.getDefinition().getMaxHit();
		if (type.equals(CombatType.CORP_MELEE_STOMP)) {
			max_hit = 50;
		} else if (type.equals(CombatType.CORP_SKILL_DRAIN)) {
			max_hit = 60;
		} else if (type.equals(CombatType.CORP_MULTI_ATTACK)) {
			max_hit = 60;
		}
		int damage = NumberUtils.random(max_hit);
		int def = 0;
		int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
		if (target.isPlayer()) {
			Player player = (Player) target;
			if (type.equals(CombatType.MELEE) || type.equals(CombatType.CORP_MELEE_STOMP)) {
				def = NumberUtils.random((int) HitFormula.calculateMeleeDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Melee")) {
					damage = 0;
				}
			} else if (type.equals(CombatType.MAGE) || type.equals(CombatType.CORP_SKILL_DRAIN) || type.equals(CombatType.CORP_MULTI_ATTACK)) {
				def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Magic")) {
					damage *= 0.60;
				}
			}
		}
		if (def > accuracy) {
			damage = 0;
		}
		final Hit combatHit = new Hit(damage, type == CombatType.MELEE || type == CombatType.CORP_MELEE_STOMP ? 0 : 2);
		combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
		combatHit.setTarget(target);
		combatHit.setOwner(npc);
		CombatEffects.altarCombatDamage(npc, target, combatHit);
		if (combatHit.getDamage() > target.getHp()) {
			combatHit.setDamage(target.getHp());
		}
		return combatHit;
	}

	@Override
	public CombatType getCombatType(NPC npc, Entity target) {
		CombatType type = CombatType.MELEE;
		switch (NumberUtils.random(3)) {
		case 1:
			type = CombatType.MAGE;
			break;
		case 2:
			type = CombatType.CORP_SKILL_DRAIN;
			break;
		case 3:
			type = CombatType.CORP_MULTI_ATTACK;
			break;
		}
		if (!Location.isWithinDistance(npc, target, 1)) {
			switch (NumberUtils.random(8)) {
			case 0:
				type = CombatType.MAGE;
				break;
			case 1:
				type = CombatType.CORP_SKILL_DRAIN;
				break;
			case 2:
				type = CombatType.CORP_MULTI_ATTACK;
				break;
			}
		}
		return type;
	}

	@Override
	public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
		int distance = 1;
		if (!(type.equals(CombatType.MELEE))) {
			distance = 9;
		}
		if (Location.standingOn(npc, target)) {
			return false;
		}
		return Location.isWithinDistance(npc, target, distance);
	}

	private static ArrayList<Player> getPossibleStompTargets(NPC npc, Entity mainTarget) {
		ArrayList<Player> possibleTarget = new ArrayList<Player>();
		for (Player targets : World.getRegionalPlayers(npc)) {
			if (targets == null)
				continue;
			if (targets.getCombatState().isDead())
				continue;
			if (Location.standingOn(npc, targets)) {
				possibleTarget.add(targets);
			}
		}
		return possibleTarget;
	}
	
	private static ArrayList<Player> getPossibleTargets(NPC npc, Entity mainTarget) {
		ArrayList<Player> possibleTarget = new ArrayList<Player>();
		for (Player targets : World.getRegionalPlayers(npc)) {
			if (targets == null)
				continue;
			if (targets.getCombatState().isDead())
				continue;
			possibleTarget.add(targets);
		}
		return possibleTarget;
	}
}
