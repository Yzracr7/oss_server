
package com.hyperion.game.world.entity.npc.impl.wilderness;

import java.util.ArrayList;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.impl.AbstractNPCAttacks;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.utility.NumberUtils;


public class CrazyArchealogist extends AbstractNPCAttacks {

    static String[] shouts = {"Rain of knowledge!", "I'm Bellock - respect me!", "Get off my site!", "No-one messes with Bellock's dig!", "These ruins are mine!",
            "Taste my knowledge!", "You belong in a museum!"};


    @Override
    public void executeAttacks(final NPC npc, final Entity main_target) {
        CombatType type = NumberUtils.random(0, 29) > 22 ? getCombatType(npc, main_target) : CombatType.RANGE;
        npc.getCombatState().setCombatType(type);
        if (!isWithinDistance(npc, main_target, type)) {
            return;
        } else {
            npc.getWalkingQueue().reset();
        }
        if (npc.getCombatState().getCombatCycles() > 0) {
            return;
        }
        if (!canAttack(npc, main_target, type)) {
            return;
        }
        npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
        int anim = npc.getDefinition().getAttackAnimation();
        if (NumberUtils.random(15) < 10) {
            npc.playForcedChat("" + shouts[NumberUtils.random(1, shouts.length - 1)]);
        }

        final Hit combatHit = getDamage(npc, main_target, type);
        final int ticks = type.equals(CombatType.MAGE) ? 3 : 1;
        if (NumberUtils.random(7) == 6) {
            combatHit.setCombatType(CombatType.MELEE.ordinal());
        }
        npc.animate(Animation.create(type == CombatType.RANGE ? 3353 : anim, AnimationQueue.AnimationPriority.HIGH));
        main_target.getCombatState().setCurrentAttacker(npc);
        main_target.getCombatState().setPreviousAttacker(npc);
        CombatState.refreshPreviousHit(npc, main_target);

        World.getWorld().submit(new Tickable(ticks) {
            @Override
            public void execute() {
                this.stop();

                if (combatHit.getDamage() > main_target.getHp()) {
                    combatHit.setDamage(main_target.getHp());
                }
                if (type.equals(CombatType.MAGE)) {
                    combatHit.setCombatType(CombatType.MAGE.ordinal());
                    boolean special = NumberUtils.random(0, 50) > 25;
                    if (special) {
                        npc.animate(3353);
                        specialAttack(npc, main_target);
                        main_target.inflictDamage(combatHit, false);
                        main_target.animate(CombatAnimations.getDefendAnim(main_target));
                    }
                } else if (type.equals(CombatType.RANGE)) {
                    combatHit.setCombatType(CombatType.RANGE.ordinal());
                    final Player pTargets = (Player) main_target;
                    if (!pTargets.getPrayers().isPrayerActive("Protect from Ranged")) {
                        combatHit.setDamage(NumberUtils.random(3, 15));
                        if (combatHit.getDamage() > 0) {
                            combatHit.setType(Hits.HitType.NORMAL_DAMAGE);
                        }
                    } else {
                        combatHit.setDamage(0);
                        combatHit.setType(Hits.HitType.NO_DAMAGE);

                    }
                    Projectiles send_projectile = Projectiles.create(npc.getCentreLocation(), main_target.getLocation(), main_target, 1259, 20, 55, 50, 55, 34);
                    npc.executeProjectile(send_projectile);
                    main_target.animate(CombatAnimations.getDefendAnim(main_target));
                    main_target.inflictDamage(combatHit, false);
                }
                handleEndEffects(npc, main_target, combatHit);
            }
        });
    }

    public void specialAttack(NPC npc, Entity target) {
        for (Entity targets : getPossibleTargets(npc, target)) {
            final Player pTargets = (Player) targets;
            handleMagicSpecial(npc, pTargets, CombatType.MAGE);
        }
        //Shout!
        npc.playForcedChat("" + shouts[0]);

    }

    private void handleMagicSpecial(NPC npc, Player pTargets, CombatType type) {
        if (!ProjectilePathFinder.hasLineOfSight(npc, pTargets, false)) {
            return;
        }

        if (!canAttack(npc, pTargets, type)) {
            return;
        }

        final Hit combatHit = getDamage(npc, pTargets, type);
        Projectiles.sendTripleProjectiles(157, 100, 1260, 25, 120, 145, npc, pTargets, "", type, combatHit, 5, 1, 24, true);
    }

    @Override
    public void handleEndEffects(NPC npc, Entity target, Hit hit) {
        CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
    }

    @Override
    public boolean canAttack(NPC npc, Entity target, CombatType type) {
        if (!ProjectilePathFinder.hasLineOfSight(npc, target, false)) {
            return false;
        }
        return true;
    }

    @Override
    public Hit getDamage(NPC npc, Entity target, CombatType type) {
        int max_hit = npc.getDefinition().getMaxHit();

        int damage = NumberUtils.random(max_hit);
        int def = 0;
        int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
        if (target.isPlayer()) {
            Player player = (Player) target;
            if (type.equals(CombatType.MAGE)) {
                def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
                if (player.getPrayers().isPrayerActive("Protect from Magic")) {
                    damage *= 0.005;
                }
            } else if (type.equals(CombatType.RANGE)) {
                def = NumberUtils.random((int) HitFormula.calculateRangedDefencePVM(player));
                if (player.getPrayers().isPrayerActive("Protect from Ranged")) {
                    damage *= 0.5;
                }
            }
        }
        if (def > accuracy) {
            damage = 0;
        }
        final Hit combatHit = new Hit(damage, 2);
        combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
        combatHit.setTarget(target);
        combatHit.setOwner(npc);
        CombatEffects.altarCombatDamage(npc, target, combatHit);
        if (combatHit.getDamage() > target.getHp()) {
            combatHit.setDamage(target.getHp());
        }
        return combatHit;
    }

    @Override
    public CombatType getCombatType(NPC npc, Entity target) {
        return CombatType.MAGE;
    }

    @Override
    public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
        int distance = 9;
        if (Location.standingOn(npc, target)) {
            return false;
        }
        return Location.isWithinDistance(npc, target, distance);
    }

    private static ArrayList<Player> getPossibleStompTargets(NPC npc, Entity mainTarget) {
        ArrayList<Player> possibleTarget = new ArrayList<Player>();
        for (Player targets : World.getRegionalPlayers(npc)) {
            if (targets == null)
                continue;
            if (targets.getCombatState().isDead())
                continue;
            if (Location.standingOn(npc, targets)) {
                possibleTarget.add(targets);
            }
        }
        return possibleTarget;
    }

    private static ArrayList<Player> getPossibleTargets(NPC npc, Entity mainTarget) {
        ArrayList<Player> possibleTarget = new ArrayList<Player>();
        for (Player targets : World.getRegionalPlayers(npc)) {
            if (targets == null)
                continue;
            if (targets.getCombatState().isDead())
                continue;
            possibleTarget.add(targets);
        }
        return possibleTarget;
    }
}
