package com.hyperion.game.world.entity.npc.impl.wilderness;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.tickable.impl.PoisonTask;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatConstants;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.impl.AbstractNPCAttacks;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;

public class KingBlackDragon extends AbstractNPCAttacks {

    @Override
    public void executeAttacks(final NPC npc, final Entity target) {
        CombatType type = getCombatType(npc, target);
        npc.getCombatState().setCombatType(type);
        if (!isWithinDistance(npc, target, type)) {
            return;
        } else {
            npc.getWalkingQueue().reset();
        }
        if (npc.getCombatState().getCombatCycles() > 0) {
            return;
        }
        int projectileId = -1;
        int anim = npc.getDefinition().getAttackAnimation();
        if (type.equals(CombatType.MAGE)) {
            anim = 83;
        }
        if (type.equals(CombatType.MAGE)) {
            projectileId = NumberUtils.random(393, 396);
            Projectiles magicProj = Projectiles.create(npc.getCentreLocation(), target.getCentreLocation(), target, projectileId, 32, 65, 50, 43, 35);
            npc.executeProjectile(magicProj);
        }
        npc.animate(Animation.create(anim, AnimationPriority.HIGH));
        npc.getCombatState().setCombatCycles(npc.getDefinition().getAttackSpeed());
        target.getCombatState().setCurrentAttacker(npc);
        target.getCombatState().setPreviousAttacker(npc);
        CombatState.refreshPreviousHit(npc, target);
        final int tick = type.equals(CombatType.MAGE) ? 3 : 1;
        final int finalProj = projectileId;
        final Hit combatHit = getDamage(npc, target, type);
        World.getWorld().submit(new Tickable(tick) {
            @Override
            public void execute() {
                this.stop();
                target.animate(CombatAnimations.getDefendAnim(target));
                if (combatHit.getDamage() > target.getHp()) {
                    combatHit.setDamage(target.getHp());
                }
                target.inflictDamage(combatHit, false);
                handleEndEffects(npc, target, combatHit);
                handleKBDEffects(npc, target, finalProj, combatHit.getDamage());
            }
        });
    }

    @Override
    public void handleEndEffects(NPC npc, Entity target, Hit hit) {
        // TODO Auto-generated method stub
        CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
    }

    @Override
    public boolean canAttack(NPC npc, Entity target, CombatType type) {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public Hit getDamage(NPC npc, Entity target, CombatType type) {
        int maxHit = type.equals(CombatType.MAGE) ? 65 : npc.getDefinition().getMaxHit();
        int damage = NumberUtils.random((int) (maxHit));
        int def = 0;
        int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
        if (target.isPlayer()) {
            Player player = (Player) target;
            if (type.equals(CombatType.MELEE)) {
                def = NumberUtils.random((int) HitFormula.calculateMeleeDefencePVM(player));

                if (player.getPrayers().isPrayerActive("Protect from Melee")) {
                    damage = 0;
                }
            } else if (type.equals(CombatType.MAGE)) {
                def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
                double dragonfireReduction = CombatEffects.dragonfireReduction(player);
                // player.sendMessage("[Dragon fire reduction] db [" + damage + "] da " + (damage - (damage * dragonfireReduction)) + " : dfr: " + dragonfireReduction);
                if (dragonfireReduction > 0) {
                    damage -= (damage * dragonfireReduction);
                    if (damage < 0) {
                        damage = 0;
                    }
                }
            }
        }
        if (def > accuracy) {
            damage = 0;
        }
        final Hit combatHit = new Hit(damage, CombatConstants.getId(type));
        combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
        combatHit.setTarget(target);
        combatHit.setOwner(npc);
        CombatEffects.altarCombatDamage(npc, target, combatHit);
        if (combatHit.getDamage() > target.getHp()) {
            combatHit.setDamage(target.getHp());
        }
        return combatHit;
    }

    @Override
    public CombatType getCombatType(NPC npc, Entity target) {
        // TODO Auto-generated method stub
        CombatType type = CombatType.MAGE;
        if (!Location.isWithinDistance(npc, target, 1)) {
            type = CombatType.MAGE;
        } else {
            switch (NumberUtils.random(2)) {
                case 0:
                    type = CombatType.MELEE;
                    break;
                case 1:
                    type = CombatType.MAGE;
                    break;
            }
        }
        return type;
    }

    @Override
    public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
        // TODO Auto-generated method stub
        int distance = 1;
        if (!(type.equals(CombatType.MELEE))) {
            distance = 8;
        }
        if (Location.standingOn(npc, target)) {
            return false;
        }
        return Location.isWithinDistance(npc, target, distance);
    }

    private static void handleKBDEffects(NPC npc, final Entity target, final int projectile, final int damage) {
        if (projectile != -1) {
            switch (projectile) {
                case 394://Poison
                    if (damage > 0) {
                        if (!target.getCombatState().isPoisoned()) {
                            if (NumberUtils.random(10) < 7) {
                                World.getWorld().submit(new PoisonTask(target, 8));
                            }
                        }
                    }
                    break;
                case 395://Freeze
                    if (NumberUtils.random(10) < 7) {
                        if (damage > 0) {
                            if (target.getCombatState().isFreezable()) {
                                int freezeTimer = 25;
                                int finalTimer = freezeTimer;
                                if (((Player) target).getPrayers().isPrayerActive("Protect from Magic")) {
                                    finalTimer = freezeTimer / 2;
                                }
                                MainCombat.endCombat(target, 2);
                                target.getWalkingQueue().reset();
                                target.getCombatState().setFreezable(false);
                                target.getCombatState().setFrozen(true);
                                if (target instanceof Player) {
                                    ((Player) target).getPacketSender().sendMessage("You have been frozen!");
                                }
                                World.getWorld().submit(new Tickable(finalTimer + 3) {
                                    @Override
                                    public void execute() {
                                        this.stop();
                                        target.getCombatState().setFrozen(false);
                                        World.getWorld().submit(new Tickable(7) {
                                            @Override
                                            public void execute() {
                                                this.stop();
                                                target.getCombatState().setFreezable(true);
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }
                    break;
                case 396://pray deducting
                    if (NumberUtils.random(10) < 3) {
                        if (damage > 0) {
                            Player player = (Player) target;
                            player.getSkills().deductLevel(5, NumberUtils.random(3));
                            player.getPacketSender().sendMessage("You're shocked and weakened!");
                        }
                    }
                    break;
            }
        }
    }
}
