package com.hyperion.game.world.entity.npc.impl.wilderness;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.tickable.impl.PoisonTask;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.impl.AbstractNPCAttacks;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.utility.NumberUtils;

public class Scorpia extends AbstractNPCAttacks {

	public static void handleHealers(final Player player, final NPC scorpia) {
		for (int i = 0; i < 2; i++) {
			final NPC healers = new NPC(6617);
			Location getLoc = i == 1
					? Location.create(scorpia.getLocation().getX() - 1, scorpia.getLocation().getY() + 3,
							scorpia.getLocation().getZ())
					: Location.create(scorpia.getLocation().getX() - 1, scorpia.getLocation().getY() + 1,
							scorpia.getLocation().getZ());
			healers.setLocation(getLoc);
			healers.setLastKnownRegion(getLoc);
			scorpia.addOwnedNPC(healers);
			scorpia.getAttributes().set("guardsSummoned", true);
			World.getWorld().addNPC(healers);
			healers.playGraphic(new Graphic(144, 0, 0));
			healers.setInteractingEntity(scorpia);
			healers.getFollowing().setFollowing(scorpia, false);
			healers.setFace(scorpia.getCentreLocation());
			World.getWorld().submit(new Tickable(4) {
				@Override
				public void execute() {
					if (healers.isDestroyed() || healers.getCombatState().isDead()) {
						this.stop();
						return;
					}
					if (scorpia.getHp() == 0) {
						World.getWorld().removeNPC(healers);
						this.stop();
						return;
					}
					if (player.isDestroyed()) {
						World.getWorld().removeNPC(healers);
						this.stop();
						return;
					}
					if (Location.isWithinDistance(healers, scorpia, 1)) {
						healers.animate(2639);
						healers.executeProjectile(Projectiles.create(healers.getCentreLocation(),
								scorpia.getCentreLocation(), scorpia, 142, 20, 55, 50, 55, 34));
						scorpia.heal(15);
					}

				}
			});
		}
	}

	@Override
	public void executeAttacks(final NPC npc, final Entity target) {
		CombatType type = getCombatType(npc, target);
		npc.getCombatState().setCombatType(type);
		if (!isWithinDistance(npc, target, type)) {
			return;
		} else {
			npc.getWalkingQueue().reset();
		}
		if (npc.getCombatState().getCombatCycles() > 0) {
			return;
		}
		if (!canAttack(npc, target, type)) {
			return;
		}
		if (npc.getHp() < 100 && !npc.getAttributes().isSet("guardsSummoned")) {
			// TODO guardians
			handleHealers(((Player) target), npc);
		}
		int anim = npc.getDefinition().getAttackAnimation();
		int speed = npc.getDefinition().getAttackSpeed();
		npc.animate(Animation.create(anim, AnimationPriority.HIGH));
		npc.getCombatState().setCombatCycles(speed);
		target.getCombatState().setCurrentAttacker(npc);
		target.getCombatState().setPreviousAttacker(npc);
		CombatState.refreshPreviousHit(npc, target);
		final Hit combatHit = getDamage(npc, target, type);
		World.getWorld().submit(new Tickable(1) {
			@Override
			public void execute() {
				this.stop();
				target.animate(CombatAnimations.getDefendAnim(target));
				if (combatHit.getDamage() > target.getHp()) {
					combatHit.setDamage(target.getHp());
				}
				target.inflictDamage(combatHit, false);
				handleEndEffects(npc, target, combatHit);
			}
		});
	}

	@Override
	public void handleEndEffects(NPC npc, Entity target, Hit hit) {
		if (!target.getCombatState().isPoisoned()) {
			if (NumberUtils.random(10) < 4) {
				World.getWorld().submit(new PoisonTask(target, 20));
			}
		}
		CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
	}

	@Override
	public boolean canAttack(NPC npc, Entity target, CombatType type) {
		if (!ProjectilePathFinder.hasLineOfSight(npc, target, false)) {
			return false;
		}
		return true;
	}

	@Override
	public Hit getDamage(NPC npc, Entity target, CombatType type) {
		final int maxHit = 16;
		int damage = NumberUtils.random((int) (maxHit));
		int def = 0;
		int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
		if (target.isPlayer()) {
			Player player = (Player) target;
			if (type.equals(CombatType.MELEE)) {
				def = NumberUtils.random((int) HitFormula.calculateMeleeDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Melee")) {
					damage *= 0.70;
				}
			}
		}
		if (def > accuracy) {
			damage = 0;
		}
		final Hit combatHit = new Hit(damage, 0);
		combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
		combatHit.setTarget(target);
		combatHit.setOwner(npc);
		CombatEffects.altarCombatDamage(npc, target, combatHit);
		if (combatHit.getDamage() > target.getHp()) {
			combatHit.setDamage(target.getHp());
		}
		return combatHit;
	}

	@Override
	public CombatType getCombatType(NPC npc, Entity target) {
		return CombatType.MELEE;
	}

	@Override
	public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
		int distance = 1;
		if (Location.standingOn(npc, target)) {
			return false;
		}
		return Location.isWithinDistance(npc, target, distance);
	}
}
