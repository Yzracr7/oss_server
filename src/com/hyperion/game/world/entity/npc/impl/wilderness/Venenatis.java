package com.hyperion.game.world.entity.npc.impl.wilderness;

import java.util.ArrayList;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.tickable.impl.PoisonTask;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.impl.AbstractNPCAttacks;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.utility.NumberUtils;

public class Venenatis extends AbstractNPCAttacks {


    private boolean special = false;
    private boolean web = false;

    @Override
    public void executeAttacks(final NPC npc, final Entity target) {
        // TODO Auto-generated method stub
        CombatType type = getCombatType(npc, target);
        npc.getCombatState().setCombatType(type);
        if (!isWithinDistance(npc, target, type)) {
            return;
        } else {
            npc.getWalkingQueue().reset();
        }
        if (npc.getCombatState().getCombatCycles() > 0) {
            return;
        }
        if (!canAttack(npc, target, type)) {
            return;
        }
        int speed = NumberUtils.random(2) == 1 ? npc.getDefinition().getAttackSpeed() - (npc.getDefinition().getAttackSpeed() - NumberUtils.random(3)) : npc.getDefinition().getAttackSpeed();
        if (NumberUtils.random(0, 10) > 7) {
            special = true;
        }
        if (NumberUtils.random(0, 10) > 7) {
            web = true;
        }
        npc.getCombatState().setCombatCycles(speed);
        if (!type.equals(CombatType.MELEE)) {
            npc.animate(Animation.create(5330, AnimationPriority.HIGH));
            for (Entity targets : getPossibleTargets(npc, target)) {
                final Player pTargets = (Player) targets;
                handleMagic(npc, pTargets, type);
            }
        } else {
            if (web) { //stun
                Player player = (Player) target;
                player.sendMessage("Venenatis hurls her wab at you, sticking you to the ground.");
                target.playGraphic(80, 0, 100);
                npc.animate(Animation.create(5327, AnimationPriority.HIGH));
                target.getCombatState().setCurrentAttacker(npc);
                target.getCombatState().setPreviousAttacker(npc);
                CombatState.refreshPreviousHit(npc, target);
                final Hit combatHit = getDamage(npc, target, type);
                World.getWorld().submit(new Tickable(1) {
                    @Override
                    public void execute() {
                        this.stop();
                        target.animate(CombatAnimations.getDefendAnim(target));
                        if (combatHit.getDamage() > target.getHp()) {
                            combatHit.setDamage(target.getHp());
                        }
                        target.inflictDamage(combatHit, false);
                        handleEndEffects(npc, target, combatHit);
                    }
                });
            } else {
                npc.animate(Animation.create(5327, AnimationPriority.HIGH));
                target.getCombatState().setCurrentAttacker(npc);
                target.getCombatState().setPreviousAttacker(npc);
                CombatState.refreshPreviousHit(npc, target);
                final Hit combatHit = getDamage(npc, target, type);
                World.getWorld().submit(new Tickable(1) {
                    @Override
                    public void execute() {
                        this.stop();
                        target.animate(CombatAnimations.getDefendAnim(target));
                        if (combatHit.getDamage() > target.getHp()) {
                            combatHit.setDamage(target.getHp());
                        }
                        target.inflictDamage(combatHit, false);
                        handleEndEffects(npc, target, combatHit);
                    }
                });
            }
            //For all other players, magic attack
            for (Entity targets : getPossibleTargets(npc, target)) {
                final Player pTargets = (Player) targets;
                if (pTargets != target)
                    handleMagic(npc, pTargets, type);
            }
        }
        special = false;
        web = false;
    }

    private void handleMagic(NPC npc, Player pTargets, CombatType type) {
        if (!ProjectilePathFinder.hasLineOfSight(npc, pTargets, false)) {
            return;
        }
        if (!isWithinDistance(npc, pTargets, type)) {
            return;
        }
        if (!canAttack(npc, pTargets, type)) {
            return;
        }
        int projectile = special ? 171 : 165;
        int endGfx = special ? 172 : -1;
        Projectiles send_projectile = Projectiles.create(npc.getCentreLocation(), pTargets.getLocation(), pTargets, projectile, 32, 70, 50, 40, 34);
        npc.executeProjectile(send_projectile);
        pTargets.getCombatState().setCurrentAttacker(npc);
        pTargets.getCombatState().setPreviousAttacker(npc);
        CombatState.refreshPreviousHit(npc, pTargets);
        final Hit combatHit = getDamage(npc, pTargets, type);
        if (combatHit.getDamage() <= 0) {
            endGfx = 85;
        }
        final int finalEndGfx = endGfx;
        final int tick = type.equals(CombatType.MAGE) ? 3 : 2;
        World.getWorld().submit(new Tickable(tick) {
            @Override
            public void execute() {
                this.stop();
                if (finalEndGfx == 172) {
                    pTargets.sendMessage("Your prayer was drained!");
                    if (pTargets.getSettings().getPrayerPoints() < 25) {
                        pTargets.getSettings().setPrayerPoints(pTargets.getSettings().getPrayerPoints() / 4);
                    } else {
                        pTargets.getSettings().setPrayerPoints(pTargets.getSettings().getPrayerPoints() - pTargets.getSettings().getPrayerPoints() / 2);
                    }
                }
                if (finalEndGfx != -1) {
                    pTargets.playGraphic(finalEndGfx, 0, 100);
                }
                pTargets.animate(Animation.create(CombatAnimations.getDefendAnim(pTargets), AnimationPriority.NONE));
                if (combatHit.getDamage() > pTargets.getHp()) {
                    combatHit.setDamage(pTargets.getHp());
                }
                if (finalEndGfx != 85) {
                    pTargets.inflictDamage(combatHit, false);
                    handleEndEffects(npc, pTargets, combatHit);
                }
            }
        });
    }

    @Override
    public void handleEndEffects(NPC npc, Entity target, Hit hit) {
        // TODO Auto-generated method stub
        if (!target.getCombatState().isPoisoned()) {
            if (NumberUtils.random(10) < 2) {
                World.getWorld().submit(new PoisonTask(target, 8));
            }
        } else if (NumberUtils.random(10) < 2) {
            int drain = hit.getDamage() / 10;
            ((Player) target).getSettings().decreasePrayerPoints(drain < 3 ? 3 : drain);
        }
        CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
    }

    @Override
    public boolean canAttack(NPC npc, Entity target, CombatType type) {
        if (!ProjectilePathFinder.hasLineOfSight(npc, target, false)) {
            return false;
        }
        return true;
    }

    @Override
    public Hit getDamage(NPC npc, Entity target, CombatType type) {
        final int maxHit = type.equals(CombatType.MAGE) ? (special ? 9 : 30) : (web ? 50 : 25);
        int damage = maxHit;
        int def = 0;
        int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());

        if (target.isPlayer()) {
            Player player = (Player) target;
            if (type.equals(CombatType.MELEE)) {
                def = NumberUtils.random((int) HitFormula.calculateMeleeDefencePVM(player));
                if (player.getPrayers().isPrayerActive("Protect from Melee")) {
                    damage *= 0.60;
                }
            } else if (type.equals(CombatType.MAGE)) {
                def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
                if (player.getPrayers().isPrayerActive("Protect from Magic")) {
                    damage = 0;
                }
            }
          //  player.sendMessage("npc accuracy roll : " + (accuracy * 2) + " / player def roll : " + def);
            if (def > (accuracy * 2)) {
                damage = 0;
            } else {
                damage = (NumberUtils.random(damage));
            }
        }
        final Hit combatHit = new Hit(damage, type == CombatType.MAGE ? 2 : 0);
        combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
        combatHit.setTarget(target);
        combatHit.setOwner(npc);
        CombatEffects.altarCombatDamage(npc, target, combatHit);
        if (combatHit.getDamage() > target.getHp()) {
            combatHit.setDamage(target.getHp());
        }
        return combatHit;
    }

    @Override
    public CombatType getCombatType(NPC npc, Entity target) {
        // TODO Auto-generated method stub
        CombatType type = CombatType.MELEE;
        if (!Location.isWithinDistance(npc, target, 1)) {
            type = CombatType.MAGE;
        } else {
            switch (NumberUtils.random(1)) {
                case 1:
                    type = CombatType.MAGE;
                    break;
            }
        }
        return type;
    }

    @Override
    public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
        // TODO Auto-generated method stub
        int distance = 1;
        if (!(type.equals(CombatType.MELEE))) {
            distance = 8;
        }
        if (Location.standingOn(npc, target)) {
            return false;
        }
        return Location.isWithinDistance(npc, target, distance);
    }

    private static ArrayList<Entity> getPossibleTargets(NPC npc, Entity mainTarget) {
        ArrayList<Entity> possibleTarget = new ArrayList<Entity>();
        for (Player targets : World.getRegionalPlayers(npc)) {
            if (targets == null)
                continue;
            if (targets.isNPC())
                continue;
            if (targets.getCombatState().isDead())
                continue;
            possibleTarget.add(targets);
        }
        return possibleTarget;
    }

}