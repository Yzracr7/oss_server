package com.hyperion.game.world.entity.npc.impl.wilderness;

import java.util.ArrayList;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.impl.AbstractNPCAttacks;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.pathfinders.ProjectilePathFinder;
import com.hyperion.utility.NumberUtils;

public class Vet_ion extends AbstractNPCAttacks {

	public static int guardsSpawned;
	public int special;

	@Override
	public void executeAttacks(final NPC npc, final Entity target) {
		CombatType type = getCombatType(npc, target);
		npc.getCombatState().setCombatType(type);
		npc.getWalkingQueue().reset();
		if (npc.getCombatState().getCombatCycles() > 0) {
			return;
		}
		if (!canAttack(npc, target, type)) {
			return;
		}
		if (npc.getHp() < 128 && !npc.getAttributes().isSet("guardsSummoned")) {
			if (npc.getTransformationId() == 6612)  {
				npc.playForcedChat("Bahh! Go, Dogs!!");
				for (int i = 0; i < 2; i++) {
					int npcChat = NumberUtils.random((int) (1));
					int npcChat2 = NumberUtils.random((int) (1));
					NPC npc2 = new NPC(6614);
					npc2.setLocation(new Location(npc.getLocation().getX() - 2, npc.getLocation().getY() - (i == 1 ? 1 : 0),
							npc.getLocation().getZ()));
					npc2.getAttributes().set("attackable", true);
					if (i == 0 && npcChat == 1) {
						npc2.playForcedChat("GRRRRRRRRRRRR");
					}
					if (i == 1 && npcChat2 == 1) {
						npc2.playForcedChat("GRRRRRRRRRRRR");
					}
					World.getWorld().addNPC(npc2);
					guardsSpawned = 2;
				}
			} else {
				npc.playForcedChat("Kill, my pets!");
				for (int i = 0; i < 2; i++) {
					int npcChat = NumberUtils.random((int) (1));
					int npcChat2 = NumberUtils.random((int) (1));
					NPC npc2 = new NPC(6613);
					npc2.setLocation(new Location(npc.getLocation().getX() - 2, npc.getLocation().getY() - (i == 1 ? 1 : 0),
							npc.getLocation().getZ()));
					npc2.getAttributes().set("attackable", true);
					if (i == 0 && npcChat == 1) {
						npc2.playForcedChat("GRRRRRRRRRRRR");
					}
					if (i == 1 && npcChat2 == 1) {
						npc2.playForcedChat("GRRRRRRRRRRRR");
					}
					World.getWorld().addNPC(npc2);
					guardsSpawned = 2;
				}
			}
			npc.getAttributes().set("guardsSummoned", true);
		}
		int speed = npc.getDefinition().getAttackSpeed();
		if (!isWithinDistance(npc, target, type)) {
			npc.animate(Animation.create(5499, AnimationPriority.HIGH));
			npc.getCombatState().setCombatCycles(speed);
			target.getCombatState().setCurrentAttacker(npc);
			target.getCombatState().setPreviousAttacker(npc);
			CombatState.refreshPreviousHit(npc, target);
			for (Entity targets : getPossibleTargets(npc, target)) {
				final Player pTargets = (Player) targets;
				handleMagic(npc, pTargets, type);
			}
		} else {
			special = NumberUtils.random((int) (2));
			if (special == 0) {
				((Player)target).getPacketSender().sendMessage("Vet'ion pummels the ground sending a shattering earthquake shockwave through you.");
				npc.animate(Animation.create(5507, AnimationPriority.HIGH));
			} else {
				npc.animate(Animation.create(5485, AnimationPriority.HIGH));
			}
			npc.getCombatState().setCombatCycles(speed);
			target.getCombatState().setCurrentAttacker(npc);
			target.getCombatState().setPreviousAttacker(npc);
			CombatState.refreshPreviousHit(npc, target);
			final Hit combatHit = getDamage(npc, target, type);
			World.getWorld().submit(new Tickable(1) {
				@Override
				public void execute() {
					this.stop();
					target.animate(CombatAnimations.getDefendAnim(target));
					if (combatHit.getDamage() > target.getHp()) {
						combatHit.setDamage(target.getHp());
					}
					target.inflictDamage(combatHit, false);
					handleEndEffects(npc, target, combatHit);
				}
			});
		}
	}

	private void handleMagic(NPC npc, Player pTargets, CombatType type) {
		if (!ProjectilePathFinder.hasLineOfSight(npc, pTargets, false)) {
			return;
		}
		if (!canAttack(npc, pTargets, type)) {
			return;
		}
		final Hit combatHit = getDamage(npc, pTargets, type);
		Projectiles.sendTripleProjectiles(281, 100, 280, 25, 125, 125, npc, pTargets,
				"", type, combatHit, 5,
				1, 25, false);
	}

	@Override
	public void handleEndEffects(NPC npc, Entity target, Hit hit) {
		CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
	}

	@Override
	public boolean canAttack(NPC npc, Entity target, CombatType type) {
		if (!ProjectilePathFinder.hasLineOfSight(npc, target, false)) {
			return false;
		}
		return true;
	}

	private static ArrayList<Player> getPossibleTargets(NPC npc, Entity mainTarget) {
		ArrayList<Player> possibleTarget = new ArrayList<Player>();
		for (Player targets : World.getRegionalPlayers(npc)) {
			if (targets == null)
				continue;
			if (targets.isNPC())
				continue;
			if (targets.getCombatState().isDead())
				continue;
			possibleTarget.add(targets);
		}
		return possibleTarget;
	}

	@Override
	public Hit getDamage(NPC npc, Entity target, CombatType type) {
		final int maxHit = special == 0 ? 45 : 30;
		int damage = maxHit;
		int def = 0;
		int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
		if (target.isPlayer()) {
			Player player = (Player) target;
			if (!isWithinDistance(npc, target, type)) {
				def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
				if (player.getPrayers().isPrayerActive("Protect from Magic")) {
					damage *= 0.40;
				}
			} else if (type.equals(CombatType.MELEE)) {
				def = NumberUtils.random((int) HitFormula.calculateMeleeDefencePVM(player));
			}
			if (def > (accuracy * 2)) {
				damage = 0;
			} else {
				damage = (NumberUtils.random(damage));
			}
		}
		final Hit combatHit = new Hit(damage, isWithinDistance(npc, target, type) ? 0 : 2);
		combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
		combatHit.setTarget(target);
		combatHit.setOwner(npc);
		CombatEffects.altarCombatDamage(npc, target, combatHit);
		if (combatHit.getDamage() > target.getHp()) {
			combatHit.setDamage(target.getHp());
		}
		return combatHit;
	}

	@Override
	public CombatType getCombatType(NPC npc, Entity target) {
		return CombatType.MAGE;
	}

	@Override
	public boolean isWithinDistance(NPC npc, Entity target, CombatType type) {
		if (Location.standingOn(npc, target)) {
			return false;
		}
		return Location.isWithinDistance(npc, target, 2);
	}
}
