package com.hyperion.game.world.entity.npc.impl.zulrah;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.tickable.impl.VenomTask;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.CombatState;
import com.hyperion.game.world.entity.combat.HitFormula;
import com.hyperion.game.world.entity.combat.IncomingHit;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.combat.data.CombatConstants;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Hits;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.impl.AbstractNPCAttacks;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.utility.NumberUtils;

public class Zulrah extends AbstractNPCAttacks {

    int[][] snakeling = {{25, 24}, {15, 24}, {24, 21}, {16, 20}};

    public int[][] getSnakeling() {
        return snakeling;
    }

    @Override
    public void executeAttacks(NPC npc, Entity target) {
        if (target.getAttributes().get("in_zulruh_room") == null || target.getAttributes().get("zulruh_tick") == null || npc.getAttributes().get("transforming") != null && npc.getAttributes().get("transforming").equals(true)) {
            return;
        }
        CombatConstants.CombatType type = getCombatType(npc, target);
        npc.getCombatState().setCombatType(type);
        if (!isWithinDistance(npc, target, type)) {
            return;
        }

        if (npc.getCombatState().getCombatCycles() > 0) {
            return;
        }

        if (target.getAttributes().get("fumes_south") == null || target.getAttributes().get("fumes_east") == null || target.getAttributes().get("fumes_west") == null) {
            spitFumes(npc, target);
            return;
        }

        if (NumberUtils.random(20) == 6 || NumberUtils.random(20) == 17) {
            int[] spawnling_to_spawn = getSnakeling()[NumberUtils.random(3)];
            NPC snakeling = new NPC(2045);
            snakeling.getAttributes().set("in_zulruh_room", true);
            snakeling.setVisible(false);
            Location loc = target.getAttributes().get("baselocation");
            snakeling.setLocation(Location.create(loc.getX() + spawnling_to_spawn[0], loc.getY() + spawnling_to_spawn[1], 0));
            World.spawnObjectTemporary(new GameObject(Location.create(loc.getX() + spawnling_to_spawn[0], loc.getY() + spawnling_to_spawn[1], 0), 11700, 10, 0), 35);
            snakeling.setLastKnownRegion(loc);
            World.getWorld().addNPC(snakeling);
            snakeling.setVisible(true);
            snakeling.animate(5073); // up
            return;
        }

        if (obj_fume_1 != null && target.getAttributes().getInt("zulruh_tick") >= 8 && target.getAttributes().getInt("zulruh_tick") <= 31 && (target.getLocation().withinDistance(obj_fume_1.getLocation(), 2, false) || target.getLocation().withinDistance(obj_fume_2.getLocation(), 2, false) || target.getLocation().withinDistance(obj_fume_3.getLocation(), 2, false) || target.getLocation().withinDistance(obj_fume_4.getLocation(), 2, false) || target.getLocation().withinDistance(obj_fume_5.getLocation(), 2, false) || target.getLocation().withinDistance(obj_fume_6.getLocation(), 2, false))) {
            if (!target.getCombatState().isVenomized()) {
                World.getWorld().submit(new VenomTask(target, 6));
            }
        }
        ZulrahForm form_to_use = null;
        for (int i = 0; i < ZulrahForm.values().length; i++) {
            if (npc.getTransformationId() != -1 && ZulrahForm.values()[i].form_id == npc.getTransformationId()) {
                form_to_use = ZulrahForm.values()[i];
            }
        }
        int hit_delay = 1;
        int till_next_hit = -1;
        if (form_to_use != null) {
            switch (form_to_use) {
                case MELEE:
                    npc.animate(5806);
                    hit_delay = 6;
                    till_next_hit = 12;
                    break;
                case MAGIC:
                    npc.animate(5069);
                    hit_delay = 3;
                    ((Player) target).getPacketSender().sendProjectile(npc.getCentreLocation(), target.getLocation(), 1046, 40, 20, 90, 60, 34, target, 14, 40);
                    break;
                case RANGE:
                case JAD_RM:
                case JAD_MR:
                    if (form_to_use.ordinal() != 8) {
                        npc.animate(5069);
                        hit_delay = 3;
                        ((Player) target).getPacketSender().sendProjectile(npc.getCentreLocation(), target.getLocation(), 1044, 40, 20, 90, 60, 34, target, 14, 40);
                        break;
                    } else {
                        System.out.println("Jad phase");
                        npc.animate(5069);
                        hit_delay = 3;
                        ((Player) target).getPacketSender().sendProjectile(npc.getCentreLocation(), target.getLocation(), getCombatType(npc, target) == CombatConstants.CombatType.MAGE ? 1046 : 1044, 40, 20, 90, 60, 34, target, 14, 40);
                        break;
                    }
            }
        }

        target.getCombatState().setCurrentAttacker(npc);
        target.getCombatState().setPreviousAttacker(npc);
        CombatState.refreshPreviousHit(npc, target);
        final Hits.Hit range_hit = getDamage(npc, target, type);
        World.getWorld().submit(new Tickable(hit_delay) {
            @Override
            public void execute() {
                this.stop();
                target.animate(CombatAnimations.getDefendAnim(target));
                if (range_hit.getDamage() > target.getHp()) {
                    range_hit.setDamage(target.getHp());
                }
                target.inflictDamage(range_hit, false);
                if (NumberUtils.random(100) > 93) {
                    if (!target.getCombatState().isVenomized()) {
                        World.getWorld().submit(new VenomTask(target, 6));
                    }
                }
                handleEndEffects(npc, target, range_hit);
            }
        });
        npc.getCombatState().setCombatCycles(till_next_hit > -1 ? till_next_hit : (npc).getDefinition().getAttackSpeed());
    }

    @Override
    public void handleEndEffects(NPC npc, Entity target, Hits.Hit hit) {

    }

    @Override
    public boolean canAttack(NPC npc, Entity target, CombatConstants.CombatType type) {
        return true;
    }

    @Override
    public Hits.Hit getDamage(NPC npc, Entity target, CombatConstants.CombatType type) {
        int damage = NumberUtils.random((int) npc.getDefinition().getMaxHit());
        int def = 0;
        int accuracy = NumberUtils.random((int) npc.getDefinition().getAttackBonus());
        if (target.isPlayer()) { // Chance of prayer fail
            Player player = (Player) target;
            if (type.equals(CombatConstants.CombatType.RANGE)) {
                def = NumberUtils.random((int) HitFormula.calculateRangedDefencePVM(player));
                if (player.getPrayers().isPrayerActive("Protect from Ranged")) {
                    damage = 0;
                }
            } else if (type.equals(CombatConstants.CombatType.MAGE)) {
                def = NumberUtils.random((int) HitFormula.calculateMagicDefencePVM(player));
                if (player.getPrayers().isPrayerActive("Protect from Magic")) {
                    damage = 0;
                }
            } else if (type.equals(CombatConstants.CombatType.MELEE)) {
                def = NumberUtils.random((int) HitFormula.calculateMeleeDefencePVM(player));
                if (player.getPrayers().isPrayerActive("Protect from Melee")) {
                    damage = 0;
                }
            }
        }
        if (def > accuracy) {
            damage = 0;
        }
        final Hits.Hit combatHit = new Hits.Hit(damage, 1);
        combatHit.setIncomingHit(new IncomingHit(npc, type, -1, -1, combatHit.getDamage()));
        combatHit.setTarget(target);
        combatHit.setOwner(npc);
        CombatEffects.altarCombatDamage(npc, target, combatHit);
        if (combatHit.getDamage() > target.getHp()) {
            combatHit.setDamage(target.getHp());
        }
        return combatHit;
    }

    @Override
    public CombatConstants.CombatType getCombatType(NPC npc, Entity target) {
        ZulrahForm form_to_use = null;
        for (int i = 0; i < ZulrahForm.values().length; i++) {
            if (npc.getTransformationId() != -1 && ZulrahForm.values()[i].form_id == npc.getTransformationId()) {
                form_to_use = ZulrahForm.values()[i];
            }
        }
        if (form_to_use != null) {
            switch (form_to_use) {
                case MELEE:
                    return CombatConstants.CombatType.MELEE;
                case RANGE:
                    return CombatConstants.CombatType.RANGE;
                case MAGIC:
                    return CombatConstants.CombatType.MAGE;
                case JAD_RM:
                    return (NumberUtils.random(5) == 3) ? CombatConstants.CombatType.RANGE : CombatConstants.CombatType.MAGE;
                case JAD_MR:
                    return (NumberUtils.random(5) == 3) ? CombatConstants.CombatType.RANGE : CombatConstants.CombatType.MAGE;

            }
        }
        return CombatConstants.CombatType.RANGE;
    }

    @Override
    public boolean isWithinDistance(NPC npc, Entity target, CombatConstants.CombatType type) {
        return true;
    }


    Location fume_loc_1 = null;
    Location fume_loc_2 = null;
    Location fume_loc_3 = null;
    Location fume_loc_4 = null;
    Location fume_loc_5 = null;
    Location fume_loc_6 = null;


    GameObject obj_fume_1 = null;
    GameObject obj_fume_2 = null;
    GameObject obj_fume_3 = null;
    GameObject obj_fume_4 = null;
    GameObject obj_fume_5 = null;
    GameObject obj_fume_6 = null;

    // Do fume action
    public void spitFumes(Entity npc, Entity target) {
        Location base_location = target.getAttributes().get("baselocation");
        int projectile = 1045;

        // Set fume locations
        fume_loc_1 = Location.create(base_location.getX() + 21, base_location.getY() + 20);
        fume_loc_2 = Location.create(base_location.getX() + 17, base_location.getY() + 20);
        fume_loc_3 = Location.create(base_location.getX() + 14, base_location.getY() + 21);
        fume_loc_4 = Location.create(base_location.getX() + 14, base_location.getY() + 25);
        fume_loc_5 = Location.create(base_location.getX() + 24, base_location.getY() + 21);
        fume_loc_6 = Location.create(base_location.getX() + 24, base_location.getY() + 25);

        // Create fume objects
        obj_fume_1 = new GameObject(fume_loc_1, 11700, 10, 0);
        obj_fume_2 = new GameObject(fume_loc_2, 11700, 10, 0);
        obj_fume_3 = new GameObject(fume_loc_3, 11700, 10, 0);
        obj_fume_4 = new GameObject(fume_loc_4, 11700, 10, 0);
        obj_fume_5 = new GameObject(fume_loc_5, 11700, 10, 0);
        obj_fume_6 = new GameObject(fume_loc_6, 11700, 10, 0);

        npc.getCombatState().setCombatCycles(5);

        //facing
        if (target.getAttributes().get("fumes_south") == null) {
            npc.face(fume_loc_1);
            npc.animate(Animation.create(5069, AnimationQueue.AnimationPriority.HIGH));
            ((Player) target).getPacketSender().sendProjectile(npc.getCentreLocation(), fume_loc_1, projectile, 30, 50, 80, 60, 34, null, 40, 40);
            World.getWorld().submit(new Tickable(2) {
                @Override
                public void execute() {
                    if (target.getAttributes().isSet("fumes_south")) {
                        this.stop();
                    }
                    World.spawnObjectTemporary(obj_fume_1, 75);
                    World.spawnObjectTemporary(obj_fume_2, 72);
                    target.getAttributes().set("fumes_south", true);
                }
            });
        } else if (target.getAttributes().get("fumes_south") != null && target.getAttributes().get("fumes_west") == null) {
            npc.face(fume_loc_3);
            npc.animate(Animation.create(5069, AnimationQueue.AnimationPriority.HIGH));
            ((Player) target).getPacketSender().sendProjectile(npc.getCentreLocation(), fume_loc_3, projectile, 30, 50, 80, 60, 34, null, 40, 40);
            World.getWorld().submit(new Tickable(2) {
                @Override
                public void execute() {
                    if (target.getAttributes().isSet("fumes_west")) {
                        this.stop();
                    }
                    World.spawnObjectTemporary(obj_fume_3, 72);
                    World.spawnObjectTemporary(obj_fume_4, 73);
                    target.getAttributes().set("fumes_west", true);
                }
            });
        } else if (target.getAttributes().get("fumes_south") != null && target.getAttributes().get("fumes_west") != null && target.getAttributes().get("fumes_east") == null) {
            npc.face(fume_loc_6);
            npc.animate(Animation.create(5069, AnimationQueue.AnimationPriority.HIGH));
            ((Player) target).getPacketSender().sendProjectile(npc.getCentreLocation(), fume_loc_6, projectile, 30, 50, 80, 60, 34, null, 40, 40);
            World.getWorld().submit(new Tickable(2) {
                @Override
                public void execute() {
                    if (target.getAttributes().isSet("fumes_east")) {
                        this.stop();
                    }
                    World.spawnObjectTemporary(obj_fume_5, 72);
                    World.spawnObjectTemporary(obj_fume_6, 73);
                    target.getAttributes().set("fumes_east", true);
                }
            });
        }

        target.getCombatState().setCurrentAttacker(npc);
        target.getCombatState().setPreviousAttacker(npc);
        npc.getCombatState().setCombatCycles(((NPC) npc).getDefinition().getAttackSpeed());
    }

}
