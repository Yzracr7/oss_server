package com.hyperion.game.world.entity.npc.impl.zulrah;

import com.hyperion.game.content.miniquests.AbstractRoundBasedMiniquest;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.region.MapBuilder;
import com.hyperion.utility.NumberUtils;
import com.hyperion.utility.TextUtils;

/**
 * Zulruh room creation
 *
 * @author trees
 */
public class ZulrahBossRoom extends AbstractRoundBasedMiniquest {

    NPC boss;
    Location base_location;
    ZulrahForm current_form;
    Player instance_owner;

    // On Exit
    static final Location EXIT_LOCATION = Location.create(2211, 3056, 0);

    // Vars
    int rotation_count = 0, tick = 0, last_tick = 0;
    boolean needs_to_spawn = true, transforming = true;

    // Rotation 1 Data
    int[][] rotation_one = new int[][]{{18, 25}, {18, 25}, {18, 25}, {17, 14}, {18, 25}, {9, 21}, {17, 14}, {17, 14}, {9, 21}, {18, 25}, {18, 25}};// {26, 20}, {9, 21}};
    ZulrahForm[] rotation_one_form = new ZulrahForm[]{ZulrahForm.RANGE, ZulrahForm.MELEE, ZulrahForm.MAGIC, ZulrahForm.RANGE, ZulrahForm.MELEE, ZulrahForm.MAGIC, ZulrahForm.RANGE, ZulrahForm.MAGIC, ZulrahForm.JAD_RM, ZulrahForm.MELEE, ZulrahForm.RANGE};

    // Enter the instanced room
    public void enterInstancedRoom(Player player) {
        player.getAttributes().set("zulruhsession", true);
        enter(player);
    }

    @Override
    public void enter(final Player player) {
        instance_owner = player;
        Location north_east = Location.create(2288, 3092, 0);
        Location south_west = Location.create(2250, 3055, 0);
        int width = (north_east.getX() - south_west.getX()) / 8 + 3;
        int height = (north_east.getY() - south_west.getY()) / 8 + 1;
        int[] newCoords = MapBuilder.findEmptyChunkBound(width, height);
        MapBuilder.copyAllPlanesMap(south_west.getChunkX(), south_west.getChunkY(), newCoords[0], newCoords[1], width, height);
        base_location = Location.create(newCoords[0] << 3, newCoords[1] << 3, 0);
        base_location.setWidth(width);
        base_location.setHeight(height);
        player.getAttributes().set("baselocation", base_location);
        player.getAttributes().set("in_zulruh_room", true);
        player.getAttributes().set("zulruh_start_time", System.currentTimeMillis());

        Location player_start = Location.create(base_location.getX() + 22, base_location.getY() + 20, 0);
        Location zulruh_start = Location.create(base_location.getX() + 18, base_location.getY() + 25, 0);

        NPC nextForms = new NPC(ZulrahForm.RANGE.form_id);
        boss = nextForms;
        boss.setTransformationId(ZulrahForm.RANGE.form_id);
        boss.getAttributes().set("in_zulruh_room", true);
        nextForms.setVisible(false);
        Location loc = zulruh_start;
        nextForms.setLocation(loc);
        nextForms.setLastKnownRegion(loc);
        //player.getVariables().addOwnedNPC(nextForms);
        //nextForms.getAttributes().set("cavesession", true);
        World.getWorld().addNPC(nextForms);
        boss.getCombatState().setCombatCycles(25);
        player.teleport(player_start);
        loop(3);
    }

    //Transformation and movement
    public void loop(int loop_tick) {
        World.getWorld().submit(new Tickable(loop_tick) {
            @Override
            public void execute() {
                if (((NPC) boss).getHp() <= 0) {
                    end(instance_owner, true, false, false);
                    this.stop();
                }
                if (needs_to_spawn && boss != null) {
                    boss.animate(5073); // up
                    needs_to_spawn = false;
                } else if (transforming) {
                    if (tick > 15) {
                        boss.getCombatState().setCombatCycles(5);

                        rotation_count++;
                        if (rotation_count > 10) {
                            rotation_count = 0;
                        }

                        boss.teleport(base_location.getX() + rotation_one[rotation_count][0], base_location.getY() + rotation_one[rotation_count][1], 0);
                        current_form = rotation_one_form[rotation_count];
                        boss.setTransformationId(current_form.form_id);
                    }
                    boss.setVisible(true);
                    boss.getAttributes().remove("transforming");
                    transforming = false;
                    boss.animate(5073); // up
                }
                if ((tick >= (last_tick + NumberUtils.random(15, 25)) && !transforming)) {
                    boss.animate(5072); // down
                    last_tick = tick; // Cooldown invis
                    transforming = true;
                    boss.getAttributes().set("transforming", true);
                }
                tick++;
                instance_owner.getAttributes().set("zulruh_tick", tick);
                if (tick >= 1000 || !instance_owner.isAtDynamicRegion()) {
                    System.out.println("Destroying zulruh map chunks");
                    if (instance_owner != null) {
                        exit(instance_owner);
                    }
                    MapBuilder.destroyMap(base_location.getChunkX(), base_location.getChunkY(), base_location.getWidth(), base_location.getHeight());
                    this.stop();
                }
                if (tick >= 35 || transforming && (tick >= (last_tick + 2))) {
                    if (instance_owner.getAnimationQueue().getLastAnim() != null && instance_owner.getAnimationQueue().getLastAnim().getId() == 5072) {
                        boss.setVisible(false);
                        boss.getAttributes().set("transforming", true);
                    }
                }
                if (boss.isVisible() && tick <= 35 && tick >= 5 && !transforming && (tick >= (last_tick + 2))) {

                }
            }
        });
    }

    // Kill the variables and move player
    public void exit(Player player) {
        tick = 0;

        if (player.getVariables().getCurrentRoundMiniquest() != null) {
            player.getVariables().getCurrentRoundMiniquest().end(player, false, false, false);
        } else {
            //player.teleport(EXIT_LOCATION);
            player.resetVariables();
            player.getAttributes().remove("baselocation");
            player.getAttributes().remove("in_zulruh_room");
            player.getAttributes().remove("fumes_south");
            player.getAttributes().remove("fumes_east");
            player.getAttributes().remove("fumes_west");
        }
    }

    @Override
    public void nextRound(Player player, NPC deadNPC) {
    }

    @Override
    public void end(Player player, boolean won_game, boolean died, boolean logged) {
        if (player.getAttributes().isSet("zulruh_start_time")) {
            long startTime = player.getAttributes().get("zulruh_start_time");
            long elapsedTimeNs = System.currentTimeMillis() - startTime;
            player.getAttributes().remove("zulruh_start_time");
            World.getWorld().sendGlobalMessage(TextUtils.formatName(player.getDetails().getName()) + " has slain zulrah in " + NumberUtils.formatMillis(elapsedTimeNs) + "!", false);
        }
    }

    @Override
    public String nameOfQuest() {
        return "";
    }

}
