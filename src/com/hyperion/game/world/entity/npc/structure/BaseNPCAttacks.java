package com.hyperion.game.world.entity.npc.structure;

import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.impl.AbstractNPCAttacks;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Apr 4, 2015
 */
public abstract class BaseNPCAttacks extends AbstractNPCAttacks {
	
	public void sendPreviousAttacks(NPC npc, Entity main_target) {
		final CombatType type = getCombatType(npc, main_target);
		npc.getCombatState().setCombatType(type);
		if (!isWithinDistance(npc, main_target, type)) {
			return;
		} else {
			npc.getWalkingQueue().reset();
		}
		if (npc.getCombatState().getCombatCycles() > 0) {
			return;
		}
		if (!canAttack(npc, main_target, type)) {
			return;
		}
	}

	@Override
	public void executeAttacks(NPC npc, Entity main_target) {
		sendPreviousAttacks(npc, main_target);
	}

	@Override
	public void handleEndEffects(NPC npc, Entity target, Hit hit) {
		CombatEffects.executeCombatEffects(npc, target, -1, hit.getDamage());
	}

	@Override
	public abstract boolean canAttack(NPC npc, Entity target, CombatType type);

	@Override
	public abstract Hit getDamage(NPC npc, Entity target, CombatType type);

	@Override
	public abstract CombatType getCombatType(NPC npc, Entity target);

	@Override
	public abstract boolean isWithinDistance(NPC npc, Entity target, CombatType type);

}
