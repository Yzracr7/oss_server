package com.hyperion.game.world.entity.player;

import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.combat.data.magic.StaffsInterface;
import com.hyperion.game.world.entity.player.AttackVars.CombatSkill;
import com.hyperion.game.world.entity.player.AttackVars.CombatStyle;

public class AttackInterfaceConfig {

	public AttackInterfaceConfig() {

	}

	public static void configureButton(Player p, int interfaceId, int button) {
		AttackVars av = p.getSettings().getAttackVars();
		switch (interfaceId) {
		case 92: // Unarmed attack inter.
			switch (button) {
			case 2: // Punch (Attack XP) - Crush
				av.setSkill(AttackVars.CombatSkill.ACCURATE);
				av.setStyle(AttackVars.CombatStyle.CRUSH);
				av.setSlot(0);
				break;

			case 3: // Kick (Strength XP) - Crush
				av.setSkill(AttackVars.CombatSkill.AGGRESSIVE);
				av.setStyle(AttackVars.CombatStyle.CRUSH);
				av.setSlot(1);
				break;

			case 4: // Block (Defence XP) - Crush
				av.setSkill(AttackVars.CombatSkill.DEFENSIVE);
				av.setStyle(AttackVars.CombatStyle.CRUSH);
				av.setSlot(2);
				break;
			}
			break;

		case 93: // Whip attack inter.
			switch (button) {
			case 2: // Flick (Attack XP) - Slash
				av.setSkill(AttackVars.CombatSkill.ACCURATE);
				av.setStyle(AttackVars.CombatStyle.SLASH);
				av.setSlot(0);
				break;

			case 3: // Lash (Shared XP) - Slash
				av.setSkill(AttackVars.CombatSkill.CONTROLLED);
				av.setStyle(AttackVars.CombatStyle.SLASH);
				av.setSlot(1);
				break;

			case 4: // Deflect (Defence XP) - Slash
				av.setSkill(AttackVars.CombatSkill.DEFENSIVE);
				av.setStyle(AttackVars.CombatStyle.SLASH);
				av.setSlot(2);
				break;
			}
			break;

		case 89: // Dagger attack inter.
			switch (button) {
			case 2: // Stab (Attack XP) - Stab
				av.setSkill(AttackVars.CombatSkill.ACCURATE);
				av.setStyle(AttackVars.CombatStyle.STAB);
				av.setSlot(0);
				break;

			case 3: // Lunge (Strength XP) - Stab
				av.setSkill(AttackVars.CombatSkill.AGGRESSIVE);
				av.setStyle(AttackVars.CombatStyle.STAB);
				av.setSlot(1);
				break;

			case 4: // Slash (Strength XP) - Slash
				av.setSkill(AttackVars.CombatSkill.AGGRESSIVE);
				av.setStyle(AttackVars.CombatStyle.SLASH);
				av.setSlot(2);
				break;

			case 5: // Block (Defence XP) - Stab
				av.setSkill(AttackVars.CombatSkill.DEFENSIVE);
				av.setStyle(AttackVars.CombatStyle.STAB);
				av.setSlot(3);
				break;
			}
			break;

		case 82: // Godswords
			switch (button) {
			case 2: // Chop (Attack XP) - Slash
				av.setSkill(AttackVars.CombatSkill.ACCURATE);
				av.setStyle(AttackVars.CombatStyle.SLASH);
				av.setSlot(0);
				break;

			case 3: // Slash (Strength XP) - Slash
				av.setSkill(AttackVars.CombatSkill.AGGRESSIVE);
				av.setStyle(AttackVars.CombatStyle.SLASH);
				av.setSlot(1);
				break;

			case 4: // Smash (Strength XP) - Crush
				av.setSkill(AttackVars.CombatSkill.AGGRESSIVE);
				av.setStyle(AttackVars.CombatStyle.CRUSH);
				av.setSlot(2);
				break;

			case 5: // Block (Defence XP) - Slash
				av.setSkill(AttackVars.CombatSkill.DEFENSIVE);
				av.setStyle(AttackVars.CombatStyle.SLASH);
				av.setSlot(3);
				break;
			}
			break;

		case 87: // Spears
			switch (button) {
			case 1: // Lunge (Attack XP) - Stab
				av.setSkill(AttackVars.CombatSkill.CONTROLLED);
				av.setStyle(AttackVars.CombatStyle.STAB);
				av.setSlot(0);
				break;

			case 2: // Swipe (Strength XP) - Slash
				av.setSkill(AttackVars.CombatSkill.CONTROLLED);
				av.setStyle(AttackVars.CombatStyle.SLASH);
				av.setSlot(1);
				break;

			case 3: // Pound (Strength XP) - Crush
				av.setSkill(AttackVars.CombatSkill.CONTROLLED);
				av.setStyle(AttackVars.CombatStyle.CRUSH);
				av.setSlot(2);
				break;

			case 4: // Block (Defence XP) - Stab
				av.setSkill(AttackVars.CombatSkill.DEFENSIVE);
				av.setStyle(AttackVars.CombatStyle.SLASH);
				av.setSlot(3);
				break;
			}
			break;

		case 78: // Claw attack inter.
			switch (button) {
			case 2: // Chop (Attack XP) - Slash
				av.setSkill(AttackVars.CombatSkill.ACCURATE);
				av.setStyle(AttackVars.CombatStyle.SLASH);
				av.setSlot(0);
				break;

			case 5: // Slash (Strength XP) - Slash
				av.setSkill(AttackVars.CombatSkill.AGGRESSIVE);
				av.setStyle(AttackVars.CombatStyle.SLASH);
				av.setSlot(1);
				break;

			case 4: // Lunge (Shared XP) - Stab
				av.setSkill(AttackVars.CombatSkill.CONTROLLED);
				av.setStyle(AttackVars.CombatStyle.STAB);
				av.setSlot(2);
				break;

			case 3: // Block (Defence XP) - Slash
				av.setSkill(AttackVars.CombatSkill.DEFENSIVE);
				av.setStyle(AttackVars.CombatStyle.SLASH);
				av.setSlot(3);
				break;
			}
			break;

		case 81: // Longsword/scimitar attack inter.
			switch (button) {
			case 2: // Chop (Attack XP) - Slash
				av.setSkill(AttackVars.CombatSkill.ACCURATE);
				av.setStyle(AttackVars.CombatStyle.SLASH);
				av.setSlot(0);
				break;

			case 3: // Slash (Strength XP) - Slash
				av.setSkill(AttackVars.CombatSkill.AGGRESSIVE);
				av.setStyle(AttackVars.CombatStyle.SLASH);
				av.setSlot(1);
				break;

			case 4: // Lunge (Shared XP) - Stab
				av.setSkill(AttackVars.CombatSkill.CONTROLLED);
				av.setStyle(AttackVars.CombatStyle.SLASH);
				av.setSlot(2);
				break;

			case 5: // Block (Defence XP) - Slash
				av.setSkill(AttackVars.CombatSkill.DEFENSIVE);
				av.setStyle(AttackVars.CombatStyle.SLASH);
				av.setSlot(3);
				break;
			}
			break;

		case 88: // Mace attack inter.
			switch (button) {
			case 2: // Pound (Attack XP) - Crush
				av.setSkill(AttackVars.CombatSkill.ACCURATE);
				av.setStyle(AttackVars.CombatStyle.CRUSH);
				av.setSlot(0);
				break;

			case 3: // Pummel (Strength XP) - Crush
				av.setSkill(AttackVars.CombatSkill.AGGRESSIVE);
				av.setStyle(AttackVars.CombatStyle.CRUSH);
				av.setSlot(1);
				break;

			case 4: // Spike (Shared XP) - Stab
				av.setSkill(AttackVars.CombatSkill.CONTROLLED);
				av.setStyle(AttackVars.CombatStyle.STAB);
				av.setSlot(2);
				break;

			case 5: // Block (Defence XP) - Crush
				av.setSkill(AttackVars.CombatSkill.DEFENSIVE);
				av.setStyle(AttackVars.CombatStyle.CRUSH);
				av.setSlot(3);
				break;
			}
			break;

		case 76: // Granite maul attack inter.
			switch (button) {
			case 2: // Pound (Attack XP) - Crush
				av.setSkill(AttackVars.CombatSkill.ACCURATE);
				av.setStyle(AttackVars.CombatStyle.CRUSH);
				av.setSlot(0);
				break;

			case 4: // Pummel (Strength XP) - Crush
				av.setSkill(AttackVars.CombatSkill.AGGRESSIVE);
				av.setStyle(AttackVars.CombatStyle.CRUSH);
				av.setSlot(1);
				break;

			case 3: // Block (Defence XP) - Crush
				av.setSkill(AttackVars.CombatSkill.DEFENSIVE);
				av.setStyle(AttackVars.CombatStyle.CRUSH);
				av.setSlot(2);
				break;
			}
			break;

		case 77: // Bow attack inter.
		case 79:// crossbow attack inter
			switch (button) {
			case 2: // Accurate (Range XP) - Accurate
				av.setSkill(AttackVars.CombatSkill.RANGE);
				av.setStyle(AttackVars.CombatStyle.RANGE_ACCURATE);
				av.setSlot(0);
				break;

			case 4: // Rapid (Range XP) - Rapid
				av.setSkill(AttackVars.CombatSkill.RANGE);
				av.setStyle(AttackVars.CombatStyle.RANGE_RAPID);
				av.setSlot(1);
				break;

			case 3: // Longrange (Range XP) - Defensive
				av.setSkill(AttackVars.CombatSkill.RANGE);
				av.setStyle(AttackVars.CombatStyle.RANGE_DEFENSIVE);
				av.setSlot(2);
				break;
			}
			break;

		case 75: // Battleaxe attack inter.
			switch (button) {
			case 2: // Chop (Attack XP) - Slash
				av.setSkill(AttackVars.CombatSkill.ACCURATE);
				av.setStyle(AttackVars.CombatStyle.SLASH);
				av.setSlot(0);
				break;

			case 5: // Hack (Strength XP) - Slash
				av.setSkill(AttackVars.CombatSkill.AGGRESSIVE);
				av.setStyle(AttackVars.CombatStyle.SLASH);
				av.setSlot(1);
				break;

			case 4: // Smash (Strength XP) - Crush
				av.setSkill(AttackVars.CombatSkill.AGGRESSIVE);
				av.setStyle(AttackVars.CombatStyle.CRUSH);
				av.setSlot(2);
				break;

			case 3: // Block (Defence XP) - Slash
				av.setSkill(AttackVars.CombatSkill.DEFENSIVE);
				av.setStyle(AttackVars.CombatStyle.SLASH);
				av.setSlot(3);
				break;
			}
			break;

		case 91: // Thrown weapon
			switch (button) {
			case 2: // Accurate (Range XP) - Accurate
				av.setSkill(AttackVars.CombatSkill.RANGE);
				av.setStyle(AttackVars.CombatStyle.RANGE_ACCURATE);
				av.setSlot(0);
				break;

			case 3: // Rapid (Range XP) - Rapid
				av.setSkill(AttackVars.CombatSkill.RANGE);
				av.setStyle(AttackVars.CombatStyle.RANGE_RAPID);
				av.setSlot(1);
				break;

			case 4: // Longrange (Range XP) - Defensive
				av.setSkill(AttackVars.CombatSkill.RANGE);
				av.setStyle(AttackVars.CombatStyle.RANGE_DEFENSIVE);
				av.setSlot(2);
				break;
			}
			break;

		case 84: // Spear
			switch (button) {
			case 2:// Bash (Attack XP) - Crush
				av.setSkill(AttackVars.CombatSkill.CONTROLLED);
				av.setStyle(AttackVars.CombatStyle.STAB);
				av.setSlot(0);
				break;

			case 3:// Pound (Strength XP) - Crush
				av.setSkill(AttackVars.CombatSkill.AGGRESSIVE);
				av.setStyle(AttackVars.CombatStyle.SLASH);
				av.setSlot(1);
				break;

			case 4:// Block (Defense XP) - Crush
				av.setSkill(AttackVars.CombatSkill.DEFENSIVE);
				av.setStyle(AttackVars.CombatStyle.STAB);
				av.setSlot(2);
				break;
			}
			break;

		case 90: // Staff inter
		case 85: // Wand inter
			if (interfaceId == 90) {
				if (button >= 1 && button <= 3) {
					if (p.getAttributes().isSet("autocastspell")) {
						int type = p.getCombatState().getTarget() != null ? 1 : 0;
						MainCombat.endCombat(p, type);
					}
					StaffsInterface.cancel(p, true);
				}
				switch (button) {
				case 1: // Bash (Attack XP) - Crush
					av.setSkill(AttackVars.CombatSkill.ACCURATE);
					av.setStyle(AttackVars.CombatStyle.CRUSH);
					av.setSlot(0);
					break;
				case 2: // Pound (Strength XP) - Crush
					av.setSkill(AttackVars.CombatSkill.AGGRESSIVE);
					av.setStyle(AttackVars.CombatStyle.CRUSH);
					av.setSlot(1);
					break;
				case 3: // Focus (Defense XP) - Crush
					av.setSkill(AttackVars.CombatSkill.DEFENSIVE);
					av.setStyle(AttackVars.CombatStyle.CRUSH);
					av.setSlot(2);
					break;
				}
			} else if (interfaceId == 85) {
				if (button >= 2 && button <= 4) {
					if (p.getAttributes().isSet("autocastspell")) {
						int type = p.getCombatState().getTarget() != null ? 1 : 0;
						MainCombat.endCombat(p, type);
					}
					StaffsInterface.cancel(p, true);
				}
				switch (button) {
				case 2: // Bash (Attack XP) - Crush
					av.setSkill(AttackVars.CombatSkill.ACCURATE);
					av.setStyle(AttackVars.CombatStyle.CRUSH);
					av.setSlot(0);
					break;
				case 3: // Pound (Strength XP) - Crush
					av.setSkill(AttackVars.CombatSkill.AGGRESSIVE);
					av.setStyle(AttackVars.CombatStyle.CRUSH);
					av.setSlot(1);
					break;
				case 4: // Focus (Defense XP) - Crush
					av.setSkill(AttackVars.CombatSkill.DEFENSIVE);
					av.setStyle(AttackVars.CombatStyle.CRUSH);
					av.setSlot(2);
					break;
				}
			}
			break;
		}
	}

	public static void setButtonForAttackStyle(Player p, int interfaceId) {
		if (interfaceId == -1) {
			return;
		}
		AttackVars av = p.getSettings().getAttackVars();
		CombatSkill type = av.getSkill();
		CombatStyle type2 = av.getStyle();
		int slot = av.getSlot();
		int button = -1;
		switch (interfaceId) {
		case 92: // Unarmed
			if (type.equals(AttackVars.CombatSkill.ACCURATE) || slot == 0) {
				button = 2;
				p.getPacketSender().sendConfig(43, 0);
			} else if (type.equals(AttackVars.CombatSkill.AGGRESSIVE) || slot == 1) {
				button = 3;
				p.getPacketSender().sendConfig(43, 1);
			} else if (type.equals(AttackVars.CombatSkill.DEFENSIVE) || slot == 2 || slot == 3) {
				button = 4;
				p.getPacketSender().sendConfig(43, 2);
				av.setSlot(2);
			}
			break;

		case 93: // Whip attack inter.
		//	p.getPacketSender().sendConfig(43, 0);
			//button = 2;
			
			  if (type.equals(AttackVars.CombatSkill.ACCURATE) || slot == 0) {
			  p.getPacketSender().sendConfig(43, 0); button = 2; 
			  } else if (type.equals(AttackVars.CombatSkill.CONTROLLED) || slot == 1) {
			  p.getPacketSender().sendConfig(43, 1); button = 3; 
			  } else if (type.equals(AttackVars.CombatSkill.DEFENSIVE) || slot == 2 || slot == 3) { 
				  p.getPacketSender().sendConfig(43, 2); av.setSlot(2);
			  button = 4;
			  }
			 
			break;

		case 89: // Dagger attack inter.
			if (type.equals(AttackVars.CombatSkill.ACCURATE) || slot == 0) {
				button = 2;
				p.getPacketSender().sendConfig(43, 0);
			} else if ((type.equals(AttackVars.CombatSkill.AGGRESSIVE) && type2.equals(AttackVars.CombatStyle.STAB)) || slot == 1) {
				button = 3;
				p.getPacketSender().sendConfig(43, 1);
			} else if ((type.equals(AttackVars.CombatSkill.AGGRESSIVE) && type2.equals(AttackVars.CombatStyle.SLASH))) {
				button = 4;
				p.getPacketSender().sendConfig(43, 2);
			} else if (type.equals(AttackVars.CombatSkill.DEFENSIVE) || slot == 2 || slot == 3) {
				button = 5;
				p.getPacketSender().sendConfig(43, 3);
				av.setSlot(3);
			}
			break;

		case 81: // Longsword/scimitar attack inter.
			if (type.equals(AttackVars.CombatSkill.ACCURATE) || slot == 0) {
				button = 2;
				p.getPacketSender().sendConfig(43, 0);
			} else if (type.equals(AttackVars.CombatSkill.AGGRESSIVE) || slot == 1) {
				button = 3;
				p.getPacketSender().sendConfig(43, 1);
			} else if (type.equals(AttackVars.CombatSkill.CONTROLLED)) {
				button = 4;
				p.getPacketSender().sendConfig(43, 2);
			} else if (type.equals(AttackVars.CombatSkill.DEFENSIVE) || slot == 2 || slot == 3) {
				button = 5;
				p.getPacketSender().sendConfig(43, 3);
				av.setSlot(3);
			}
			break;

		case 78: // Claw attack inter.
			if (type.equals(AttackVars.CombatSkill.ACCURATE) || slot == 0) {
				button = 2;
				p.getPacketSender().sendConfig(43, 0);
			} else if (type.equals(AttackVars.CombatSkill.AGGRESSIVE) || slot == 1) {
				button = 5;
				p.getPacketSender().sendConfig(43, 1);
			} else if (type.equals(AttackVars.CombatSkill.CONTROLLED)) {
				button = 4;
				p.getPacketSender().sendConfig(43, 2);
			} else if (type.equals(AttackVars.CombatSkill.DEFENSIVE) || slot == 2 || slot == 3) {
				button = 3;
				p.getPacketSender().sendConfig(43, 3);
				av.setSlot(3);
			}
			break;

		case 82:// Godsword attack inter.
			if (type.equals(AttackVars.CombatSkill.ACCURATE) || slot == 0) {
				button = 2;
				p.getPacketSender().sendConfig(43, 0);
			} else if (type.equals(AttackVars.CombatSkill.AGGRESSIVE) && type2.equals(AttackVars.CombatStyle.SLASH) || slot == 1) {
				button = 3;
				p.getPacketSender().sendConfig(43, 1);
			} else if (type.equals(AttackVars.CombatSkill.AGGRESSIVE) && type2.equals(AttackVars.CombatStyle.CRUSH)) {
				button = 4;
				p.getPacketSender().sendConfig(43, 2);
			} else if (type.equals(AttackVars.CombatSkill.DEFENSIVE) || slot == 2 || slot == 3) {
				button = 5;
				p.getPacketSender().sendConfig(43, 3);
				av.setSlot(3);
			}
			break;

		case 88: // Mace attack inter.
			if (type.equals(AttackVars.CombatSkill.ACCURATE) || slot == 0) {
				button = 2;
				p.getPacketSender().sendConfig(43, 0);
			} else if (type.equals(AttackVars.CombatSkill.AGGRESSIVE) || slot == 1) {
				button = 3;
				p.getPacketSender().sendConfig(43, 1);
			} else if (type.equals(AttackVars.CombatSkill.CONTROLLED)) {
				button = 4;
				p.getPacketSender().sendConfig(43, 2);
			} else if (type.equals(AttackVars.CombatSkill.DEFENSIVE) || slot == 2 || slot == 3) {
				button = 5;
				p.getPacketSender().sendConfig(43, 3);
				av.setSlot(3);
			}
			break;

		case 76: // Granite maul attack inter.
			if (type.equals(AttackVars.CombatSkill.ACCURATE) || slot == 0) {
				button = 2;
				p.getPacketSender().sendConfig(43, 0);
			} else if (type.equals(AttackVars.CombatSkill.AGGRESSIVE) || slot == 1) {
				button = 4;
				p.getPacketSender().sendConfig(43, 1);
			} else if (type.equals(AttackVars.CombatSkill.DEFENSIVE) || slot == 2 || slot == 3) {
				button = 3;
				p.getPacketSender().sendConfig(43, 2);
				av.setSlot(2);
			}
			break;

		case 77: // Bow attack inter.
		case 79: // crossbow attack inter
			if (type2.equals(AttackVars.CombatStyle.RANGE_ACCURATE) || slot == 0) {
				button = 2;
				p.getPacketSender().sendConfig(43, 0);
			} else if (type2.equals(AttackVars.CombatStyle.RANGE_RAPID) || slot == 1) {
				button = 4;
				p.getPacketSender().sendConfig(43, 1);
			} else if (type2.equals(AttackVars.CombatStyle.RANGE_DEFENSIVE) || slot == 2 || slot == 3) {
				button = 3;
				p.getPacketSender().sendConfig(43, 2);
				av.setSlot(2);
			}
			break;

		case 75: // Battleaxe attack inter.
			if (type.equals(AttackVars.CombatSkill.ACCURATE) || slot == 0) {
				button = 2;
				p.getPacketSender().sendConfig(43, 0);
			} else if (type.equals(AttackVars.CombatSkill.AGGRESSIVE) && type2.equals(AttackVars.CombatStyle.SLASH) || slot == 1) {
				button = 5;
				p.getPacketSender().sendConfig(43, 1);
			} else if (type.equals(AttackVars.CombatSkill.AGGRESSIVE) && type2.equals(AttackVars.CombatStyle.CRUSH)) {
				button = 4;
				p.getPacketSender().sendConfig(43, 2);
			} else if (type.equals(AttackVars.CombatSkill.DEFENSIVE) || slot == 2 || slot == 3) {
				button = 3;
				p.getPacketSender().sendConfig(43, 3);
				av.setSlot(3);
			}
			break;

		case 91: // Thrown weapon
			if (type2.equals(AttackVars.CombatStyle.RANGE_ACCURATE) || slot == 0) {
				button = 2;
				p.getPacketSender().sendConfig(43, 0);
			} else if (type2.equals(AttackVars.CombatStyle.RANGE_RAPID) || slot == 1) {
				button = 3;
				p.getPacketSender().sendConfig(43, 1);
			} else if (type2.equals(AttackVars.CombatStyle.RANGE_DEFENSIVE) || slot == 2 || slot == 3) {
				button = 4;
				p.getPacketSender().sendConfig(43, 2);
				av.setSlot(2);
			}
			break;

		case 85: // Spear
			if (type.equals(AttackVars.CombatSkill.ACCURATE) || slot == 0) {
				button = 2;
				p.getPacketSender().sendConfig(43, 0);
			} else if (type.equals(AttackVars.CombatSkill.AGGRESSIVE) || slot == 1) {
				button = 3;
				p.getPacketSender().sendConfig(43, 1);
			} else if (type.equals(AttackVars.CombatSkill.DEFENSIVE) || slot == 2 || slot == 3) {
				button = 4;
				p.getPacketSender().sendConfig(43, 2);
				av.setSlot(2);
			}
			break;

		case 90: // Staff inter
			if (type.equals(AttackVars.CombatSkill.ACCURATE) || slot == 0) {
				button = 1;
				p.getPacketSender().sendConfig(43, 0);
			} else if (type.equals(AttackVars.CombatSkill.AGGRESSIVE) || slot == 1) {
				button = 2;
				p.getPacketSender().sendConfig(43, 1);
			} else if (type.equals(AttackVars.CombatSkill.DEFENSIVE) || slot == 2 || slot == 3) {
				button = 3;
				p.getPacketSender().sendConfig(43, 2);
				av.setSlot(2);
			}
			break;
		}
		configureButton(p, interfaceId, button);
	}

}
