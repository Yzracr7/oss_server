package com.hyperion.game.world.entity.player;

import com.hyperion.game.world.entity.combat.data.magic.StaffsInterface;

public class AttackStyles {

	public static boolean handleAttackStyles(Player player, int interfaceId, int buttonId) {
		switch (interfaceId) {
		case 90:// Staffs inter
		case 85:// wand inter
			switch (buttonId) {
			case 5:// selecting spell
				StaffsInterface.openAutoCastInterface(player);
				break;
			case 9:// Auto retal
			case 24:// Auto retal
				player.getSettings().toggleAutoRetaliate();
				break;
			case 85: //Spec
				player.getSettings().toggleSpecBar();
				break;
			default:
				AttackInterfaceConfig.configureButton(player, interfaceId, buttonId);
				break;
			}
			return true;
		case 92: // Unarmed attack inter.
			switch (buttonId) {
			case 24: // Auto retaliate.
				player.getSettings().toggleAutoRetaliate();
				break;

			default:
				AttackInterfaceConfig.configureButton(player, interfaceId, buttonId);
				break;
			}
			return true;

		case 84: // Spear attack inter.
			switch (buttonId) {
			case 8: // Special attack.
				player.getSettings().toggleSpecBar();
				break;

			case 24: // Auto retaliate.
				player.getSettings().toggleAutoRetaliate();
				break;
			default:
				AttackInterfaceConfig.configureButton(player, interfaceId, buttonId);
				break;
			}
			return true;

		case 93: // Whip attack inter.
			switch (buttonId) {
			case 8: // Special attack.
				player.getSettings().toggleSpecBar();
				break;

			case 24: // Auto retaliate.
				player.getSettings().toggleAutoRetaliate();
				break;

			default:
				AttackInterfaceConfig.configureButton(player, interfaceId, buttonId);
				break;
			}
			return true;

		case 89: // Dagger attack inter.
			switch (buttonId) {
			case 10: // Special attack.
				player.getSettings().toggleSpecBar();
				break;

			case 26: // Auto retaliate.
				player.getSettings().toggleAutoRetaliate();
				break;

			default:
				AttackInterfaceConfig.configureButton(player, interfaceId, buttonId);
				break;
			}
			return true;

		case 82: // Godswords
			switch (buttonId) {
			case 10: // Special attack.
				player.getSettings().toggleSpecBar();
				break;

			case 26: // Auto retaliate.
				player.getSettings().toggleAutoRetaliate();
				break;

			default:
				AttackInterfaceConfig.configureButton(player, interfaceId, buttonId);
				break;
			}
			return true;
		case 87: // Spears
			switch (buttonId) {
			case 26: // Auto retaliate.
				player.getSettings().toggleAutoRetaliate();
				break;

			default:
				AttackInterfaceConfig.configureButton(player, interfaceId, buttonId);
				break;
			}
			return true;

		case 78: // Claw attack inter.
			switch (buttonId) {
			case 10: // Special attack.
				player.getSettings().toggleSpecBar();
				break;

			case 26: // Auto retaliate.
				player.getSettings().toggleAutoRetaliate();
				break;

			default:
				AttackInterfaceConfig.configureButton(player, interfaceId, buttonId);
				break;
			}
			return true;

		case 81: // Longsword/scimitar attack inter.
			switch (buttonId) {
			case 10: // Special attack.
				player.getSettings().toggleSpecBar();
				break;

			case 26: // Auto retaliate.
				player.getSettings().toggleAutoRetaliate();
				break;

			default:
				AttackInterfaceConfig.configureButton(player, interfaceId, buttonId);
				break;
			}
			return true;

		case 88: // Mace attack inter.
			switch (buttonId) {
			case 10: // Special attack.
				player.getSettings().toggleSpecBar();
				break;

			case 26: // Auto retaliate.
				player.getSettings().toggleAutoRetaliate();
				break;

			default:
				AttackInterfaceConfig.configureButton(player, interfaceId, buttonId);
				break;
			}
			return true;

		case 76: // Granite maul attack inter.
			switch (buttonId) {
			case 8: // Special attack.
				player.getSettings().toggleSpecBar();
				break;
			case 24: // Auto retaliate.
				player.getSettings().toggleAutoRetaliate();
				break;
			default:
				AttackInterfaceConfig.configureButton(player, interfaceId, buttonId);
				break;
			}
			return true;

		case 79:// crossbow
			switch (buttonId) {
			case 24:
				player.getSettings().toggleAutoRetaliate();
				break;
			case 8:
				player.getSettings().toggleSpecBar();
				break;
			default:
				AttackInterfaceConfig.configureButton(player, interfaceId, buttonId);
				break;
			}
			return true;

		case 77: // Bow attack inter.
			switch (buttonId) {
			case 8: // Special attack.
				player.getSettings().toggleSpecBar();
				break;
			case 24: // Auto retaliate.
				player.getSettings().toggleAutoRetaliate();
				break;
			default:
				AttackInterfaceConfig.configureButton(player, interfaceId, buttonId);
				break;
			}
			return true;

		case 75: // Battleaxe attack inter.
			switch (buttonId) {
			case 10: // Special attack.
				player.getSettings().toggleSpecBar();
				// player.getSettings().dragonBattleaxe();
				break;
			case 26: // Auto retaliate.
				player.getSettings().toggleAutoRetaliate();
				break;
			default:
				AttackInterfaceConfig.configureButton(player, interfaceId, buttonId);
				break;
			}
			return true;

		case 91: // Thrown weapon
			switch (buttonId) {
			case 8:
				player.getSettings().toggleSpecBar();
				break;
			case 24: // Auto retaliate.
				player.getSettings().toggleAutoRetaliate();
				break;

			default:
				AttackInterfaceConfig.configureButton(player, interfaceId, buttonId);
				break;
			}
			return true;
		}
		return false;
	}
}