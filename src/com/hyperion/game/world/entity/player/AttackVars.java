package com.hyperion.game.world.entity.player;

public class AttackVars {
	
	private CombatSkill skill;
	private CombatStyle style;
	private int slot;

	public AttackVars() {
		setDefault();
	}
	
	private void setDefault() {
		this.skill = CombatSkill.ACCURATE;
		this.style = CombatStyle.CRUSH;
		this.slot = 0;
	}

	public void setSkill(CombatSkill skill) {
		this.skill = skill;
	}

	public void setStyle(CombatStyle style) {
		this.style = style;
	}
	
	public void setSkill(String skill) {
		for (CombatSkill skills : CombatSkill.values()) {
			if (skills.toString().equals(skill)) {
				this.skill = skills;
			}
		}
	}
	
	public void setStyle(String style) {
		for (CombatStyle styles : CombatStyle.values()) {
			if (styles.toString().equals(style)) {
				this.style = styles;
			}
		}
	}
	
	public static String styleToString(CombatStyle style) {
		return style.toString().contains("_") ? style.toString().split("_")[0] : style.toString();
	}

	public CombatSkill getSkill() {
		return skill;
	}

	public CombatStyle getStyle() {
		return style;
	}
	
	public int getSlot() {
		return slot;
	}

	public void setSlot(int slot) {
		this.slot = slot;
	}

	public static enum CombatSkill {
		ACCURATE,
		DEFENSIVE,
		AGGRESSIVE,
		RANGE,
		CONTROLLED;
	}
	
	public static enum CombatStyle {
		STAB,
		SLASH,
		CRUSH,
		MAGIC,
		RANGE_ACCURATE,
		RANGE_RAPID,
		RANGE_DEFENSIVE;
	}
}