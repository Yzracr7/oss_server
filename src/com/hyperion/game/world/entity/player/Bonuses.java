package com.hyperion.game.world.entity.player;

import java.text.DecimalFormat;

import com.hyperion.game.item.Item;
import com.hyperion.game.item.ItemDefinition;


public class Bonuses {

    public enum Bonus {

        STAB, SLASH, CRUSH, MAGIC, RANGED, STAB_DEF, SLASH_DEF, CRUSH_DEF, MAGIC_DEF, RANGED_DEF, STRENGTH, PRAYER;

        public int getValue(Bonus bonus) {
            System.out.println("ORDINAL FOR: " + bonus.toString() + ", " + bonus.ordinal());
            return bonus.ordinal();
        }
    }

    private static final String[] BONUS_NAMES = new String[]{
            "Stab", "Slash", "Crush", "Magic", "Ranged", "Stab", "Slash", "Crush", "Magic", "Ranged", "Strength", "Prayer"
    };

    public static final int[] BONUS_STRING = {
            108, 109, 110, 111, 112, 114, 115, 116, 117, 118, 120, 121
    };

    private static final int SIZE = 12;
    private Player player;
    private int[] bonuses = new int[SIZE];

    public void setWeight(double weight) {
        this.weight = weight;
    }

    private double weight;

    public Bonuses(Player player) {
        this.player = player;
    }

    public void refresh() {
        weight = 0;
        for (int index = 0; index < SIZE; index++) {
            bonuses[index] = 0;
        }

        for (int index = 0; index < SIZE; index++) {
            int item = player.getEquipment().getItemInSlot(index);
            if (item != -1) {
                double[] bonuses = ItemDefinition.forId(item).getBonus();
                if (bonuses == null)
                    continue;
                int len = SIZE;
                for (int i = 0; i < len; i++) {
                    int loop = i;
                    double itemBonus = bonuses[i];
                    this.bonuses[loop] += itemBonus;
                }
                double itemWeight = ItemDefinition.forId(item).getWeight();
                if (isReducer(item))
                    weight -= itemWeight;
                else
                    weight += itemWeight;
            }
        }

        for (Item invyItem : player.getInventory().getItems()) {
            if (invyItem != null) {
                double itemWeight = ItemDefinition.forId(invyItem.getId()).getWeight();
                if (!isReducer(invyItem.getId())) //reducers have no effect on inventory weight
                    weight += itemWeight;
            }
        }

        for (int index = 0; index < SIZE; index++) {
            String string = (BONUS_NAMES[index] + ": " + (bonuses[index] >= 0 ? "+" : "") + bonuses[index]);
            int child_id = 108 + index;
            if (child_id > 117) {
                child_id++;
            }
            player.getPacketSender().modifyText(string, 465, child_id);
        }
        player.getPacketSender().modifyText(df.format(weight) + " kg", 465, 121);
    }

    public void refreshWeight() {
        weight = 0;

        for (int index = 0; index < SIZE; index++) {
            int item = player.getEquipment().getItemInSlot(index);
            if (item != -1) {
                double itemWeight = ItemDefinition.forId(item).getWeight();
                if (isReducer(item))
                    weight -= itemWeight;
                else
                    weight += itemWeight;
            }
        }

        for (Item invyItem : player.getInventory().getItems()) {
            if (invyItem != null) {
                double itemWeight = ItemDefinition.forId(invyItem.getId()).getWeight();
                if (!isReducer(invyItem.getId())) //reducers have no effect on inventory weight
                    weight += itemWeight;
            }
        }
    }

    private final static DecimalFormat df = new DecimalFormat("#.#");

    private static final int[] WEIGHT_REDUCERS = {88, 10069, 10073,
            10663, 10071, 10074, 10664, 10553, 10554, 24210, 24211, 14938,
            14939, 24208, 24209, 14936, 14937, 24206, 24207, 24560, 24561,
            24562, 24563, 24654, 24801, 24802, 24803, 24804, 24805};

    private final static boolean isReducer(int item) {
        for (int REDUCERS : WEIGHT_REDUCERS) {
            if (item == REDUCERS) {
                return true;
            }
        }
        return false;
    }

    public double getWeight() {
        return weight;
    }

    public int[] getBonuses() {
        return bonuses;
    }

    public int getBonus(int i) {
        return bonuses[i];
    }

    public int getBonus(Bonus bonus) {
        return bonuses[bonus.ordinal()];
    }
}
