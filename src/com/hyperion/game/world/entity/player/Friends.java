package com.hyperion.game.world.entity.player;

import java.util.ArrayList;
import java.util.List;

import com.hyperion.game.world.World;
import com.hyperion.utility.TextUtils;

/**
 * Manages friends and ignores.
 * @author Graham
 */
public class Friends {
	
	private Player player;
	
	private List<String> friends;
	private List<String> ignores;
	private int publicStatus = ON;
	private int privateStatus = ON;
	private int tradeStatus = ON;
	
	private static final int ON = 0, FRIENDS = 1, OFF = 2;
	
	private transient static int messageCounter = 0;
	
	public Friends(Player player) {
		this.player = player;
		friends = new ArrayList<String>(200);
		ignores = new ArrayList<String>(100);
		publicStatus = privateStatus = tradeStatus = 0;
	}
	
	public int getNextUniqueId() {
		if(messageCounter >= 16000000) {
			messageCounter = 0;
		}
		return messageCounter++;
	}

	public void refresh() {
		player.getPacketSender().setPrivacyOptions();
		player.getPacketSender().setFriendsListStatus();
		for(String friend : friends) {
			player.getPacketSender().sendFriend(friend, getWorld(friend));
		}
		String[] array = new String[ignores.size()];
		int i = 0;
		for(String ignore : ignores) {
			if(ignore != null) {
				array[i++] = ignore;
			}
		}
		player.getPacketSender().sendIgnores(array);
	}

	public int getWorld(String friend) {
		for(Player p : World.getWorld().getPlayers()) {
			if(p != null) {
				if(p.getName().equalsIgnoreCase(friend)) {
					return 1;
				}
			}
		}
		return 0;
	}

	public void addFriend(String name) {
		Player friend = null;
		if(friends.size() >= 200) {
			player.getPacketSender().sendMessage("Your friends list is full.");
			return;
		}
		if(friends.contains(name)) {
			player.getPacketSender().sendMessage(TextUtils.formatName(name) + " is already on your friends list.");
			return;
		}
		for(Player p : World.getWorld().getPlayers()) {
			if(p != null) {
				if (p.getName().equalsIgnoreCase(name)) {
					friend = p;
				}
			}
		}
		friends.add(name);
		if (friend != null) {
			if (privateStatus != OFF) {
				friend.getFriends().registered(player);
			}
			if (friend.getFriends().getPrivateStatus() == OFF || (friend.getFriends().getPrivateStatus() == FRIENDS && !friend.getFriends().isFriend(player))) {
				return;
			}
			player.getPacketSender().sendFriend(name, getWorld(name));
		}
	}

	private boolean isFriend(Player player) {
		String name = player.getName();
		if(friends.contains(name)) {
			return true;
		}
		return false;
	}

	public int getPrivateStatus() {
		return privateStatus;
	}
	
	public void setPrivateStatus(int status) {
		this.privateStatus = status;
	}

	public void addIgnore(String name) {
		if(ignores.size() >= 100) {
			player.getPacketSender().sendMessage("Your ignore list is full.");
			return;
		}
		if(ignores.contains(name)) {
			player.getPacketSender().sendMessage(TextUtils.formatName(name) + " is already on your ignore list.");
			return;
		}
		ignores.add(name);
	}

	public void removeFriend(String name) {
		Player friend = null;
		for(Player p : World.getWorld().getPlayers()) {
			if(p != null) {
				if (p.getName().equalsIgnoreCase(name)) {
					friend = p;
				}
			}
		}
		if (friend != null) {
			if (privateStatus == FRIENDS) {
				friend.getFriends().unregistered(player);
			}
		}
		friends.remove(name);
	}

	public void removeIgnore(String name) {
		ignores.remove(name);
	}
	
	public List<String> getFriendsList() {
		return friends;
	}
	
	public List<String> getIgnoresList() {
		return ignores;
	}

/*	public void registered() {
		for(Player p : World.getWorld().getPlayers()) {
			if(p != null) {
				p.getFriends().registered(player);
			}
		}
	}*/

	private void registered(Player p) {
		String name = p.getName();
		if(friends.contains(name)) {
			player.getPacketSender().sendFriend(name, getWorld(name));
		}
	}

	public void unregistered() {
		for(Player p : World.getWorld().getPlayers()) {
			if(p != null) {
				p.getFriends().unregistered(player);
			}
		}
	}

	private void unregistered(Player p) {
		String name = p.getName();
		if(friends.contains(name)) {
			player.getPacketSender().sendFriend(name, 0);
		}
	}

	public void sendMessage(String name, byte[] packed) {
		name = name.replaceAll("_", " ");
		for(Player p : World.getWorld().getPlayers()) {
			if(p != null && !p.equals(player)) {
				if (p.getName().equalsIgnoreCase(name)) {
					if (privateStatus == OFF) {
						privateStatus = FRIENDS;
						setPrivacyOption(publicStatus, privateStatus, tradeStatus);
					}
					p.getPacketSender().sendReceivedPrivateMessage(player.getName(), player.getDetails().getRights(), packed);
					player.getPacketSender().sendSentPrivateMessage(name, packed);
					return;
				}
			}
		}
		player.getPacketSender().sendMessage(TextUtils.formatName(name) + " is currently unavailable.");
	}
	
	public void login() {
		if (privateStatus == OFF) {
			return;
		} else if (privateStatus == FRIENDS) {
			for(Player p : World.getWorld().getPlayers()) {
				if(p != null) {
					if (friends.contains(p.getName())) {
						p.getFriends().registered(player);
					}
				}
			}
		} else if (privateStatus == ON) {
			for(Player other : World.getWorld().getPlayers()) {
				if(other != null && other != player
						&& other.getFriends().containsFriend(player.getName())) {
					other.getPacketSender().sendFriend(player.getName(), /*getWorld(player.getName())*/1);
				}
			}
		}
	}
	
	private boolean containsFriend(String name) {
		for(String friendName : friends)  {
			if(friendName.equalsIgnoreCase(name))
				return true;
		}
		return false;
	}
	
	public void setPrivacyOption(int pub, int priv, int trade) {
		System.out.println("pub: " + pub + " priv: " + priv + " trade: " + trade); 
		publicStatus = pub;
		tradeStatus = trade;
		if (priv != privateStatus) {
			if (priv == ON) {
				//registered();
			} else  if (priv == OFF) {
				unregistered();
			} else if (priv == FRIENDS) {
				if (privateStatus == ON) {
					for(Player p : World.getWorld().getPlayers()) {
						if(p != null) {
							if (p.getFriends().getFriendsList().contains(player.getName())) {
								if (!friends.contains(p.getName())) {
									p.getFriends().unregistered(player);
								}
							}
						}
					}
				} else if (privateStatus == OFF) {
					for(Player p : World.getWorld().getPlayers()) {
						if(p != null) {
							if (friends.contains(p.getName())) {
								p.getFriends().registered(player);
							}
						}
					}
				}
			}
			privateStatus = priv;
		}
		player.getPacketSender().setPrivacyOptions();
	}

	public int getPrivacyOption(int option) {
		switch(option) {
			case 0: return publicStatus;
			case 1: return privateStatus;
			case 2: return tradeStatus;
		}
		return 0;
	}
	
	public void setPrivacyOption(int option, int value) {
		switch(option) {
		case 0: 
			publicStatus = value;
			break;
		case 1: 
			privateStatus = value;
			break;
		case 2: 
			tradeStatus = value;
			break;
	}
	}

}
