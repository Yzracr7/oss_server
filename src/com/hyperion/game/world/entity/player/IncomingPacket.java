package com.hyperion.game.world.entity.player;

public class IncomingPacket {

	private int id = -1, length = -1;
	
	public IncomingPacket(int id, int length) {
		this.id = id;
		this.length = length;
	}
	
	public int getId() {
		return id;
	}
	
	public int getLen() {
		return length;
	}
	
	public void setId(int i) {
		this.id = i;
	}
	
	public void setLen(int i) {
		this.length = i;
	}
}
