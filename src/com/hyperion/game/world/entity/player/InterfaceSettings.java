package com.hyperion.game.world.entity.player;

import com.hyperion.game.content.skills.cooking.Cooking;
import com.hyperion.game.content.skills.crafting.Glass;
import com.hyperion.game.content.skills.crafting.Jewellery;
import com.hyperion.game.content.skills.crafting.Leather;
import com.hyperion.game.content.skills.crafting.Spinning;
import com.hyperion.game.content.skills.fletching.MakeBows;
import com.hyperion.game.content.skills.fletching.MakeXbow;
import com.hyperion.game.content.skills.herblore.Herblore;
import com.hyperion.game.content.skills.smithing.Smelting;
import com.hyperion.game.world.entity.player.container.impl.Bank;
import com.hyperion.game.world.shopping.Shop;
import com.hyperion.game.world.shopping.ShopManager;

public class InterfaceSettings {

	/**
	 * The current open inter.
	 */
	private int currentInterface = -1;

	/**
	 * The current open overlay.
	 */
	private int currentOverlay = -1;

	/**
	 * The active enterInstancedRoom amount inter.
	 */
	private int enterAmountInterfaceId = -1;

	/**
	 * The active enterInstancedRoom amount inter button.
	 */
	private int button = -1;

	/**
	 * The active enterInstancedRoom amount id.
	 */
	private int enterAmountId;

	/**
	 * The active enterInstancedRoom amount slot.
	 */
	private int enterAmountSlot;

	/**
	 * The activate enterInstancedRoom amount text.
	 */
	private String enterAmountText;
	
	/**
	 * If the client is focused on the screen
	 */
	private boolean isClientFocused = true;

	/**
	 * The player.
	 */
	private Player player;

	/**
	 * Creates the inter state.
	 */
	public InterfaceSettings(Player player) {
		this.player = player;
	}

	/**
	 * Checks if the specified inter is open.
	 * 
	 * @param id
	 *            The inter id.
	 * @return <code>true</code> if the inter is open, <code>false</code> if
	 *         not.
	 */
	public boolean isInterfaceOpen(int id) {
		return currentInterface == id;
	}

	/**
	 * Gets the current open inter.
	 * 
	 * @return The current open inter.
	 */
	public int getCurrentInterface() {
		return currentInterface;
	}

	/**
	 * Checks if the specified inter is open.
	 * 
	 * @param id
	 *            The inter id.
	 * @return <code>true</code> if the inter is open, <code>false</code> if
	 *         not.
	 */
	public boolean isOverlayOpen(int id) {
		return currentOverlay == id;
	}

	/**
	 * Gets the current open inter.
	 * 
	 * @return The current open inter.
	 */
	public int getCurrentOverlay() {
		return currentOverlay;
	}

	/**
	 * Called when an inter is opened.
	 * 
	 * @param id
	 *            The inter.
	 */
	public void setInterfaceOpened(int id) {
		currentInterface = id;
	}

	/**
	 * Called when an inter is opened.
	 * 
	 * @param id
	 *            The inter.
	 */
	public void setOverlayOpened(int id) {
		currentOverlay = id;
	}

	public void closeInterfaces(boolean inputbox) {
		currentInterface = -1;
		enterAmountInterfaceId = -1;
		button = -1;
		if (player.getVariables().getCloseInterfacesEvent() != null) {
			player.getVariables().getCloseInterfacesEvent().run();
			player.getVariables().setCloseInterfacesEvent(null);
		}
		player.getAttributes().remove("godbookcasket");
		player.getAttributes().remove("forfietDuel");
		player.getAttributes().remove("shopping");
		player.getAttributes().remove("shop");
		if (player.getAttributes().isSet("reportabuse")) {
			player.getAttributes().remove("reportabuse");
			player.getPacketSender().sendBlankClientScript(80);
			
		}
		if (player.getAttributes().isSet("jewlItem")) {
			player.getAttributes().remove("jewlItem");
		}
		if (player.getAttributes().isSet("nextDialogue")) {
			player.getAttributes().set("nextDialogue", 0);
			player.getAttributes().set("previousDialogue", 0);
			player.getAttributes().remove("dialogueNPC");
			player.getAttributes().remove("fixingBarrows");
		}
		player.getPacketSender().sendCloseInterface();
		player.getPacketSender().restoreTabs();
		player.getVariables().setBanking(false);
		if (player.getAttributes().isSet("skipResetFace")) {
			player.getAttributes().remove("skipResetFace");
		} else
			player.setInteractingEntity(null);
		player.getPacketSender().closeChatboxInterface();
		if (inputbox) {
			player.getPacketSender().sendBlankClientScript(101);
		}
		if (player.getInventory().getInterfaceContainer().getInterfaceId() != 149) {
			player.getInventory().getInterfaceContainer().setInterfaceId(149);
			player.getInventory().getInterfaceContainer().setChild(0);
			player.getInventory().getInterfaceContainer().setType(93);
			player.getInventory().refresh();
		}
	}

	public void openEnterTextInterface(int interfaceId, int buttonId, String text) {
		enterAmountInterfaceId = interfaceId;
		button = buttonId;
		player.getPacketSender().displayEnterText(text);
	}

	/**
	 * Called to open the enterInstancedRoom amount inter.
	 * 
	 * @param interfaceId
	 *            The inter id.
	 * @param slot
	 *            The slot.
	 * @param id
	 *            The id.
	 */
	public void openEnterAmountInterface(int interfaceId, int slot, int id, String text) {
		enterAmountInterfaceId = interfaceId;
		enterAmountSlot = slot;
		enterAmountId = id;
		setEnterAmountText(text);
		player.getPacketSender().displayEnterAmount(text);
	}

	/**
	 * Used for skilling
	 * 
	 * @param interfaceId
	 * @param button
	 * @param text
	 */
	public void openEnterAmountInterface(String text, int interfaceId, int interfaceButton) {
		enterAmountInterfaceId = interfaceId;
		button = interfaceButton;
		setEnterAmountText(text);
		player.getPacketSender().displayEnterAmount(text);
	}

	/**
	 * Checks if the enterInstancedRoom amount inter is open.
	 * 
	 * @return <code>true</code> if so, <code>false</code> if not.
	 */
	public boolean isEnterAmountInterfaceOpen() {
		return enterAmountInterfaceId != -1;
	}

	/**
	 * Called when the enterInstancedRoom amount inter is closed.
	 * 
	 * @param amount
	 *            The amount that was entered.
	 */
	public void closeEnterAmountInterface(int amount) {
		try {
			int leatherCraft = player.getAttributes().getInt("leatherCraft");
			int craftType = player.getAttributes().getInt("craftType");
			int cookItem = player.getAttributes().getInt("meatItem");
			int grindItem = player.getAttributes().getInt("herbloreGrindItem");
			int unfinishedPotion = player.getAttributes().getInt("unfinishedPotion");
			int completePotion = player.getAttributes().getInt("completePotion");
			switch (enterAmountInterfaceId) {
			case 311:// Smelting
				Smelting.smeltOre(player, button, amount, true);
				break;
			case 305:
				if (player.getAttributes().get("fletchType") == null) {
					return;
				}
				int logType = (Integer) player.getAttributes().get("fletchType");
				switch (button) {
				case 0:
					MakeBows.cutLog(player, amount, logType, 0, (Boolean) player.getAttributes().get("stringingBow") == null ? false : (Boolean) player.getAttributes().get("stringingBow"), true);
					break;
				case 1:
					MakeBows.cutLog(player, amount, logType, 1, (Boolean) player.getAttributes().get("stringingBow") == null ? false : (Boolean) player.getAttributes().get("stringingBow"), true);
					break;
				case 2:
					MakeBows.cutLog(player, amount, 0, 2, false, true);
					break;
				case 3:
					MakeBows.cutLog(player, amount, 0, 3, false, true);
					break;
				}
				break;
			case 542:// Glass crafting
				switch (button) {
				case 44:// Fish bowl
					Glass.craftGlass(player, amount, 4, true);
					break;
				}
				break;
			case 309:// Crafting - gem cutting
				if (craftType != -1) {
					if (craftType >= 50 && craftType <= 60) { // Cut gem
						Jewellery.cutGem(player, craftType, amount, true);
						break;
					}
				} else if (grindItem != -1) {
					Herblore.grindIngredient(player, amount, true);
					break;
				} else if (unfinishedPotion != -1) {
					Herblore.makeUnfinishedPotion(player, amount, true);
					break;
				} else if (completePotion != -1) {
					Herblore.completePotion(player, amount, true);
					break;
				}
				if (button != -1) {
					switch (button) {
					case 0:
						MakeXbow.createXbow(player, amount, (Integer) player.getAttributes().get("bowType2") == null ? -1 : (Integer) player.getAttributes().get("bowType2"), (Boolean) player.getAttributes().get("stringingBow") == null ? false : (Boolean) player.getAttributes().get("stringingBow"), true);
						break;
					case 1:
						MakeBows.cutLog(player, amount, (Integer) player.getAttributes().get("fletchType") == null ? -1 : (Integer) player.getAttributes().get("fletchType"), (Integer) player.getAttributes().get("bowType") == null ? -1 : (Integer) player.getAttributes().get("bowType"), true, true);
						break;
					}
				}
				break;
			case 304:// Used for skilling
				if (leatherCraft != -1) {
					switch (button) {
					case 5:// Body
						Leather.craftDragonHide(player, amount, 0, leatherCraft, true);
						break;
					case 9:// Vambs
						Leather.craftDragonHide(player, amount, 4, leatherCraft, true);
						break;
					case 13:// chaps
						Leather.craftDragonHide(player, amount, 8, leatherCraft, true);
						break;
					}
				} else if (craftType != -1) {
					if (craftType == 6) {
						switch (button) {
						case 5:// Ball of wool
							Spinning.craftSpinning(player, amount, 0, true);
							break;
						case 9:// Bow string
							Spinning.craftSpinning(player, amount, 1, true);
							break;
						case 13:// Crossbow string
							Spinning.craftSpinning(player, amount, 2, true);
							break;
						}
					}
				}
				break;
			case 307:// Used for skilling
				if (cookItem != -1) {
					Cooking.cookItem(player, amount, true);
				}
				break;
			case 621:// Shopping - inventory
				Shop shop = (Shop) player.getAttributes().get("shop");
				if (shop == null) {
					break;
				}
				ShopManager.getShopManager().sellItem(player, enterAmountSlot, amount);
				break;
			case 620:// Shopping inter
				Shop shopp = (Shop) player.getAttributes().get("shop");
				if (shopp == null) {
					break;
				}
				ShopManager.getShopManager().purchaseItem(player, enterAmountSlot, amount);
				break;
			case 12:// Bank screen - withdraw x
				Bank.withdraw(player, enterAmountSlot, amount, false);
				break;
			case 15:// bank screen - inventory deposit x
				Bank.deposit(player, enterAmountSlot, amount, true);
				break;
			case 109:// Duel inv - offer x
				if (player.getDuelSession() != null) {
					player.getDuelSession().stakeItem(enterAmountId, enterAmountSlot, amount);
				}
				break;
			case 107:// Duel - remove x
				if (player.getDuelSession() != null) {
					player.getDuelSession().removeItem(enterAmountId, enterAmountSlot, amount);
				}
				break;
			case 336:// Trade inventory - trade X.
				if (player.getTradingSession() != null) {
					player.getTradingSession().tradeItem(enterAmountId, enterAmountSlot, amount);
				}
				break;
			case 335:// Trade inter - remove X.
				if (player.getTradingSession() != null) {
					player.getTradingSession().removeItem(enterAmountId, enterAmountSlot, amount);
				}
				break;
			default:
				// player.getFrames().sendMessage("Unhandled enterInstancedRoom amount: " +
				// enterAmountInterfaceId + ", slot: " + enterAmountSlot);
				break;
			}
		} finally {
			enterAmountInterfaceId = -1;
			button = -1;
		}
	}

	/**
	 * Called when the enterInstancedRoom text inter is closed.
	 * 
	 * @param text
	 */
	public void closeEnterTextInterface(String text) {
		try {
			switch (enterAmountInterfaceId) {
			default:
				player.getPacketSender().sendMessage("Unhandled Enter Text: " + enterAmountInterfaceId);
				break;
			}
		} finally {
			enterAmountInterfaceId = -1;
			button = -1;
		}
	}

	/**
	 * @return the enterAmountText
	 */
	public String getEnterAmountText() {
		return enterAmountText;
	}

	/**
	 * @param enterAmountText the enterAmountText to set
	 */
	public void setEnterAmountText(String enterAmountText) {
		this.enterAmountText = enterAmountText;
	}

	/**
	 * @return the isClientFocused
	 */
	public boolean isClientFocused() {
		return isClientFocused;
	}

	/**
	 * @param isClientFocused the isClientFocused to set
	 */
	public void setClientFocused(boolean isClientFocused) {
		this.isClientFocused = isClientFocused;
	}
}
