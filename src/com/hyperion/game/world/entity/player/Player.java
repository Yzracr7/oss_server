package com.hyperion.game.world.entity.player;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;

import com.hyperion.Persistable;
import com.hyperion.cache.definitions.CachedNpcDefinition;
import com.hyperion.game.Constants;
import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.content.clues.AbstractClueScroll;
import com.hyperion.game.content.grandexchange.GrandExchange;
import com.hyperion.game.content.minigames.clanwars.ClanWarsSession;
import com.hyperion.game.content.minigames.duelarena.DuelSession;
import com.hyperion.game.content.miniquests.impl.FightCaveSession;
import com.hyperion.game.content.skills.cooking.Cooking;
import com.hyperion.game.content.skills.crafting.Crafting;
import com.hyperion.game.content.skills.fletching.Fletching;
import com.hyperion.game.content.skills.herblore.Herblore;
import com.hyperion.game.content.skills.smithing.Smelting;
import com.hyperion.game.content.skills.smithing.Smithing;
import com.hyperion.game.content.wilderness.BountyHunter;
import com.hyperion.game.content.wilderness.BountyHunterEmblem;
import com.hyperion.game.item.Item;
import com.hyperion.game.net.Packet;
import com.hyperion.game.net.PacketSender;
import com.hyperion.game.net.protocol.ISAACCipher;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.tickable.impl.DeathTick;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.combat.data.CombatEffects;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Appearance;
import com.hyperion.game.world.entity.masks.ChatMessage;
import com.hyperion.game.world.entity.masks.Hits;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.Hits.HitPriority;
import com.hyperion.game.world.entity.masks.Hits.HitType;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;
import com.hyperion.game.world.entity.player.container.Container;
import com.hyperion.game.world.entity.player.container.Container.ContainerName;
import com.hyperion.game.world.entity.player.container.Container.Type;
import com.hyperion.game.world.entity.player.container.ContainerInterface;
import com.hyperion.game.world.entity.player.container.impl.Bank;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
import com.hyperion.game.world.entity.player.container.impl.TradeSession;
import com.hyperion.game.world.grounditems.GroundItem;
import com.hyperion.game.world.grounditems.GroundItemManager;
import com.hyperion.utility.IoBufferUtils;
import com.hyperion.utility.TextUtils;

/**
 * Represents a player-controller character.
 *
 * @author Graham Edgecombe
 */
public class Player extends Entity implements Persistable {

	public long lastClickTime1;
	public long lastClickTime2;
	public int last_click_time;
	public int clickWarnings;
	public int macro_detection_flags;
	private boolean clientLoadedMapRegion;
	private boolean forceNextMapLoadRefresh;

	/**
	 * The <code>IoSession</code>.
	 */
	private final IoSession session;

	/**
	 * The ISAAC cipher for incoming data.
	 */
	private final ISAACCipher inCipher;

	/**
	 * Represents the unique id.
	 */
	private int uid;

	/**
	 * Gets the uid.
	 *
	 * @return the uid.
	 */
	public int getUid() {
		return uid;
	}

	/**
	 * Sets the uid.
	 *
	 * @param uid
	 *            the uid.
	 */
	public void setUid(int uid) {
		this.uid = uid;
	}

	/**
	 * The ISAAC cipher for outgoing data.
	 */
	private final ISAACCipher outCipher;

	/**
	 * The object to represent all player-data variables
	 */
	private PlayerDetails details;

	/**
	 * The action sender.
	 */
	private final PacketSender packetSender = new PacketSender(this);

	/**
	 * A queue of pending chat messages.
	 */
	private final Queue<ChatMessage> chatMessages = new LinkedList<ChatMessage>();

	/**
	 * The current chat message.
	 */
	private ChatMessage currentChatMessage;

	/**
	 * The name.
	 */
	private String name;

	/**
	 * The name expressed as a long.
	 */
	private long nameLong;

	/**
	 * The password.
	 */
	private String password;

	/**
	 * The members flag.
	 */
	private boolean members = true;

	/**
	 * The player's appearance information.
	 */
	private final Appearance appearance = new Appearance();

	/**
	 * The player's prayer class.
	 */
	private final Prayers prayers = new Prayers(this);

	/**
	 * The player's variables class.
	 */
	private final PlayerVariables playerVariables = new PlayerVariables(this);

	public PlayerVariables getVariables() {
		return playerVariables;
	}

	/**
	 * The player's skill levels.
	 */
	private final Skills skills = new Skills(this);

	/**
	 * The player's settings.
	 */
	private final Settings settings = new Settings(this);

	/**
	 * The player's request manager.
	 */
	private final RequestManager requestManager = new RequestManager(this);

	public RequestManager getRequestManager() {
		return requestManager;
	}

	/**
	 * The player's trading session.
	 */
	private TradeSession tradeSession = null;

	public TradeSession getTradingSession() {
		return tradeSession;
	}

	public void setTradeSession(TradeSession trade) {
		this.tradeSession = trade;
	}

	public GrandExchange getGrandeExchange() {
		return grandeExchange;
	}

	public void setGrandeExchange(GrandExchange grandeExchange) {
		this.grandeExchange = grandeExchange;
	}

	private GrandExchange grandeExchange = new GrandExchange(this);

	public long[] getBound_offer_uids() {
		return bound_offer_uids;
	}

	public void setBound_offer_uids(long[] bound_offer_uids) {
		this.bound_offer_uids = bound_offer_uids;
	}

	private long bound_offer_uids[] = new long[8];

	/**
	 * The player's duel session.
	 */
	private DuelSession duelSession = null;

	public DuelSession getDuelSession() {
		return duelSession;
	}

	public void setDuelSession(DuelSession duel) {
		this.duelSession = duel;
	}

	/**
	 * The player's Clan Wars session.
	 */
	private ClanWarsSession clanWarsSession = null;

	public ClanWarsSession getClanWarsSession() {
		return clanWarsSession;
	}

	public void setClanWarsSession(ClanWarsSession duel) {
		this.clanWarsSession = duel;
	}

	/**
	 * The player's fight cave session.
	 */
	private FightCaveSession caveSession = null;

	public FightCaveSession getFightCaveSession() {
		return caveSession;
	}

	public void setCaveSession(FightCaveSession session) {
		this.caveSession = session;
	}

	/**
	 * The player's Interface settings.
	 */
	private final InterfaceSettings interfaceSettings = new InterfaceSettings(this);

	public InterfaceSettings getInterfaceSettings() {
		return interfaceSettings;
	}

	/**
	 * the player's bonuses.
	 */
	private final Bonuses bonuses = new Bonuses(this);

	public Bonuses getBonuses() {
		return bonuses;
	}

	/**
	 * The player's friends/ignores.
	 */
	private final Friends friends = new Friends(this);

	public Friends getFriends() {
		return friends;
	}

	/**
	 * The player's bank.
	 */
	private final Container bank = new Container(this, Container.Type.ALWAYS_STACKS, ContainerName.BANK, Bank.SIZE,
			new ContainerInterface(12, 7, -1));

	public Container getBank() {
		return bank;
	}

	/**
	 * The player's trade.
	 */
	private final Container tradeItems = new Container(this, Container.Type.NORMAL, ContainerName.TRADE, 28,
			new ContainerInterface(-1, 64212, 90));

	public Container getTrade() {
		return tradeItems;
	}

	/**
	 * The player's inventory.
	 */
	private final Container inventory = new Container(this, Container.Type.NORMAL, ContainerName.INVENTORY, 28,
			new ContainerInterface(149, 0, 93));

	public Container getInventory() {
		return inventory;
	}

	/**
	 * The player's equipment.
	 */
	private final Container equipment = new Container(this, Container.Type.NORMAL, ContainerName.EQUIPMENT,
			Equipment.SIZE, new ContainerInterface(387, 28, 94));

	public Container getEquipment() {
		return equipment;
	}

	/**
	 * The player's duel container.
	 */
	private final Container duel = new Container(this, Container.Type.NORMAL, ContainerName.DUEL, 28,
			new ContainerInterface(-1, -70135, 134));

	public Container getDuel() {
		return duel;
	}

	public void resetActionAttributes() {

		if (playerVariables.getFishing() != null) {
			playerVariables.getFishing().resetFishing();
		}
		if (playerVariables.getCutSession() != null) {
			playerVariables.getCutSession().reset();
		}
		if (playerVariables.getMineSession() != null) {
			playerVariables.getMineSession().reset();
		}
		if (playerVariables.getBonesOnAltarSession() != null) {
			playerVariables.getBonesOnAltarSession().reset();
		}
		Cooking.setCookingItem(this, null);
		Crafting.resetCrafting(this);
		Fletching.setFletchItem(this, null);
		Smelting.resetSmelting(this);
		Smithing.resetSmithing(this);
		Herblore.setHerbloreItem(this, null);
		getAttributes().remove("pickupId");
		getAttributes().remove("pickupitem");
		getAttributes().remove("itemOnObject");
		getAttributes().remove("itemOnObjectItem");
		getAttributes().remove("objectPacket");
		getAttributes().remove("npcPacket");
		getAttributes().remove("type");
		getAttributes().remove("fixingBarrows");
		getAttributes().remove("stationary");
	}

	public int getHighestLevel() {
		int cmb = getSkills().getCombatLevel();
		int wild = getWildLevel();
		int total = cmb + wild;
		if (total > 126) {
			total = 126;
		}
		return total;
	}

	public int getLowestLevel() {
		int cmb = getSkills().getCombatLevel();
		int wild = getWildLevel();
		int total = cmb - wild;
		if (total < 3) {
			total = 3;
		}
		return total;
	}

	/*
	 * Cached details.
	 */
	/**
	 * The cached update block.
	 */
	private Packet cachedUpdateBlock;

	/**
	 * Creates a player based on the details object.
	 *
	 * @param details
	 *            The details object.
	 */
	public Player(PlayerDetails details) {
		super();
		this.setLocation(Constants.START_LOCATION);
		this.setLastKnownRegion(Constants.START_LOCATION);
		this.details = details;
		this.session = details.getSession();
		this.inCipher = details.getInCipher();
		this.outCipher = details.getOutCipher();
		this.name = details.getName();
		this.nameLong = TextUtils.stringToLong(this.name);
		this.password = details.getPassword();
		this.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
		this.setTeleporting(true);
	}

	/**
	 * Gets the player's name expressed as a long.
	 *
	 * @return The player's name expressed as a long.
	 */
	public long getNameAsLong() {
		return nameLong;
	}

	/**
	 * Gets the player's settings.
	 *
	 * @return The player's settings.
	 */
	public Settings getSettings() {
		return settings;
	}

	/**
	 * Checks if there is a cached update block for this cycle.
	 *
	 * @return <code>true</code> if so, <code>false</code> if not.
	 */
	public boolean hasCachedUpdateBlock() {
		return cachedUpdateBlock != null;
	}

	/**
	 * Sets the cached update block for this cycle.
	 *
	 * @param cachedUpdateBlock
	 *            The cached update block.
	 */
	public void setCachedUpdateBlock(Packet cachedUpdateBlock) {
		this.cachedUpdateBlock = cachedUpdateBlock;
	}

	/**
	 * Gets the cached update block.
	 *
	 * @return The cached update block.
	 */
	public Packet getCachedUpdateBlock() {
		return cachedUpdateBlock;
	}

	/**
	 * Resets the cached update block.
	 */
	public void resetCachedUpdateBlock() {
		cachedUpdateBlock = null;
	}

	/**
	 * Gets the current chat message.
	 *
	 * @return The current chat message.
	 */
	public ChatMessage getCurrentChatMessage() {
		return currentChatMessage;
	}

	/**
	 * Sets the current chat message.
	 *
	 * @param currentChatMessage
	 *            The current chat message to set.
	 */
	public void setCurrentChatMessage(ChatMessage currentChatMessage) {
		this.currentChatMessage = currentChatMessage;
	}

	/**
	 * Gets the queue of pending chat messages.
	 *
	 * @return The queue of pending chat messages.
	 */
	public Queue<ChatMessage> getChatMessageQueue() {
		synchronized (chatMessages) {
			return chatMessages;
		}
	}

	/**
	 * Gets the player's appearance.
	 *
	 * @return The player's appearance.
	 */
	public Appearance getAppearance() {
		return appearance;
	}

	/**
	 * Gets the player's skills.
	 *
	 * @return The player's skills.
	 */
	public Skills getSkills() {
		return skills;
	}

	/**
	 * Gets the action sender.
	 *
	 * @return The action sender.
	 */
	public PacketSender getPacketSender() {
		return packetSender;
	}

	public PlayerDetails getDetails() {
		return this.details;
	}

	/**
	 * Gets the incoming ISAAC cipher.
	 *
	 * @return The incoming ISAAC cipher.
	 */
	public ISAACCipher getInCipher() {
		return inCipher;
	}

	/**
	 * Gets the outgoing ISAAC cipher.
	 *
	 * @return The outgoing ISAAC cipher.
	 */
	public ISAACCipher getOutCipher() {
		return outCipher;
	}

	/**
	 * Gets the player's name.
	 *
	 * @return The player's name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Creating a string for the player's name that can be used in php scripts
	 *
	 * @return
	 */
	public String getWebsiteName() {
		return name.replaceAll(" ", "_");
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the player's password.
	 *
	 * @return The player's password.
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the player's password.
	 *
	 * @param pass
	 *            The password.
	 */
	public void setPassword(String pass) {
		this.password = pass;
	}

	/**
	 * Gets the <code>IoSession</code>.
	 *
	 * @return The player's <code>IoSession</code>.
	 */
	public IoSession getSession() {
		return session;
	}

	/**
	 * Checks if this player has a member's account.
	 *
	 * @return <code>true</code> if so, <code>false</code> if not.
	 */
	public boolean isMembers() {
		return members;
	}

	/**
	 * Sets the members flag.
	 *
	 * @param members
	 *            The members flag.
	 */
	public void setMembers(boolean members) {
		this.members = members;
	}

	@Override
	public String toString() {
		return Player.class.getName() + " [name=" + name + " rights=" + details.getRights() + " members=" + members
				+ " index=" + this.getIndex() + "]";
	}

	@Override
	public void deserialize(IoBuffer buf) {
		this.password = IoBufferUtils.getRS2String(buf);
		this.members = buf.getUnsigned() == 1 ? true : false;
		this.uid = buf.getUnsigned();
		settings.setSpecialAmount(buf.get(), false);
		settings.setMagicType(buf.get());
		settings.setPrayerType(buf.get());
		settings.getAttackVars().setSkill(IoBufferUtils.getRS2String(buf));
		settings.getAttackVars().setStyle(IoBufferUtils.getRS2String(buf));
		settings.getAttackVars().setSlot(buf.getInt());
		setLocation(Location.create(buf.getUnsignedShort(), buf.getUnsignedShort(), buf.getUnsigned()));
		playerVariables.setRunningEnergy(buf.getDouble(), false);
		playerVariables.setStarted(buf.get() == 1 ? true : false);

		int[] look = new int[7];
		int[] colors = new int[5];
		for (int i = 0; i < 7; i++) {
			look[i] = buf.getInt();
		}
		appearance.setLookArray(look);
		for (int i = 0; i < 5; i++) {
			colors[i] = buf.getInt();
		}
		appearance.setColoursArray(colors);
		appearance.setGender(buf.get());
		for (int i = 0; i < Equipment.SIZE; i++) {
			int id = buf.getUnsignedShort();
			if (id != 65535) {
				int amt = buf.getInt();
				Item item = new Item(id, amt);
				equipment.set(item, i, false);
			}
		}
		for (int i = 0; i < Skills.SKILL_COUNT; i++) {
			if (i == 5) {
				settings.setPrayerPoints(buf.getDouble());
			}
			skills.setSkill(i, buf.getUnsigned(), buf.getDouble());
		}
		for (int i = 0; i < 28; i++) {
			int id = buf.getUnsignedShort();
			if (id != 65535) {
				int amt = buf.getInt();
				Item item = new Item(id, amt);
				inventory.set(item, i, false);
			}
		}
		if (buf.hasRemaining()) {
			for (int i = 0; i < Bank.SIZE; i++) {
				int id = buf.getUnsignedShort();
				if (id != 65535) {
					int amt = buf.getInt();
					Item item = new Item(id, amt);
					bank.set(item, i, false);
					// }
				}
			}
		}
		if (buf.hasRemaining()) {
			for (int i = 0; i < bound_offer_uids.length; i++) {
				long id = buf.getLong();
				bound_offer_uids[i] = id;
			}
		}
		if (buf.hasRemaining()) {
			friends.setPrivateStatus(buf.getUnsigned());
		}
		if (buf.hasRemaining()) {
			int size = buf.getUnsignedShort();
			for (int i = 0; i < size; i++) {
				getFriends().getFriendsList().add(IoBufferUtils.getRS2String(buf));
			}
		}
		if (buf.hasRemaining()) {
			int size = buf.getUnsignedShort();
			for (int i = 0; i < size; i++) {
				getFriends().getIgnoresList().add(IoBufferUtils.getRS2String(buf));
			}
		}
		if (buf.hasRemaining()) {
			playerVariables.finishedMageArena = buf.get() == 1 ? true : false;
		}
		if (buf.hasRemaining()) {
			playerVariables.finishedRfd = buf.get() == 1 ? true : false;
		}
		if (buf.hasRemaining()) {
			playerVariables.setExpLocked(buf.get() == 1 ? true : false);
		}
		if (buf.hasRemaining()) {
			playerVariables.setDessousKilled(buf.get() == 1 ? true : false);
		}
		if (buf.hasRemaining()) {
			playerVariables.setKamilKilled(buf.get() == 1 ? true : false);
		}
		if (buf.hasRemaining()) {
			playerVariables.setFareedKilled(buf.get() == 1 ? true : false);
		}
		if (buf.hasRemaining()) {
			playerVariables.setDamisKilled(buf.get() == 1 ? true : false);
		}
		if (buf.hasRemaining()) {
			playerVariables.setAhrimsKilled(buf.get() == 1 ? true : false);
		}
		if (buf.hasRemaining()) {
			playerVariables.setDharoksKilled(buf.get() == 1 ? true : false);
		}
		if (buf.hasRemaining()) {
			playerVariables.setKarilsKilled(buf.get() == 1 ? true : false);
		}
		if (buf.hasRemaining()) {
			playerVariables.setVeracsKilled(buf.get() == 1 ? true : false);
		}
		if (buf.hasRemaining()) {
			playerVariables.setToragsKilled(buf.get() == 1 ? true : false);
		}
		if (buf.hasRemaining()) {
			playerVariables.setGuthansKilled(buf.get() == 1 ? true : false);
		}
		if (buf.hasRemaining()) {
			playerVariables.setBrothersKilled(buf.get());
		}
		if (buf.hasRemaining()) {
			playerVariables.setDefenderWave(buf.get());
		}
		if (buf.hasRemaining()) {
			playerVariables.setPestPoints(buf.getInt());
		}
		if (buf.hasRemaining()) {
			playerVariables.setMageArenaPoints(buf.getInt());
		}
		if (buf.hasRemaining()) {
			playerVariables.setVotePoints(buf.getInt());
		}
		if (buf.hasRemaining()) {
			playerVariables.setBarrowPoints(buf.getInt());
		}
		if (buf.hasRemaining()) {
			playerVariables.setPvpPoints(buf.getInt());
		}
		if (buf.hasRemaining()) {
			playerVariables.setFightCavesWave(buf.get());
		}
		if (buf.hasRemaining()) {
			settings.setPrivateChatSplit(buf.get() == 1 ? true : false, false);
		}
		if (buf.hasRemaining()) {
			settings.setAcceptAidEnabled(buf.get() == 1 ? true : false);
		}
		if (buf.hasRemaining()) {
			int[] pins = new int[4];
			for (int i = 0; i < 4; i++) {
				pins[i] = buf.get();
			}
			if (pins[0] != -1) {
				playerVariables.setBankPin(pins);
			}
		}
		if (buf.hasRemaining()) {
			playerVariables.lastPinChange = buf.getLong();
		}
		if (buf.hasRemaining())

		{
			playerVariables.lastDeletionRequest = buf.getLong();
		}
		if (buf.hasRemaining())

		{
			playerVariables.recoveryDelay = buf.get();
		}
		if (buf.hasRemaining())

		{
			playerVariables.setAchievementPoints(buf.getInt());
		}
		if (buf.hasRemaining())

		{
			playerVariables.setTaskAmount(buf.getInt());
		}
		if (buf.hasRemaining())

		{
			playerVariables.setTaskName(IoBufferUtils.getRS2String(buf));
		}

		if (buf.hasRemaining())

		{
			playerVariables.setKendal_killed(buf.getInt() == 1 ? true : false);
		}
		if (buf.hasRemaining())

		{
			playerVariables.setMother_killed(buf.getInt() == 1 ? true : false);
		}
		if (buf.hasRemaining())

		{
			playerVariables.setPestRound(buf.getInt());
		}
		if (buf.hasRemaining())

		{
			playerVariables.setRcPoints(buf.getInt());
		}
		if (buf.hasRemaining())

		{
			playerVariables.setWandWave(buf.getInt());
		}
		if (buf.hasRemaining())

		{
			playerVariables.setDemon_killed(buf.getInt() == 1 ? true : false);
		}
		if (buf.hasRemaining())

		{
			playerVariables.finishedLunars = (buf.getInt() == 1 ? true : false);
		}
		if (buf.hasRemaining())

		{
			playerVariables.setAtJungleDemon(buf.getInt() == 1 ? true : false);
		}
		if (buf.hasRemaining())

		{
			playerVariables.setTotal_clues_finished(buf.getInt());
		}
		if (buf.hasRemaining())

		{
			playerVariables.setSlayerPoints(buf.getInt());
		}
		if (buf.hasRemaining())

		{
			playerVariables.setTotalExp(buf.getInt());
		}
		if (buf.hasRemaining())

		{
			for (int i = 0; i < Achievements.MAX_ACHIEVEMENTS; i++) {
				playerVariables.setAchievementAmount(i, buf.getInt());
			}
		}
		if (buf.hasRemaining())

		{
			for (int i = 0; i < Achievements.MAX_ACHIEVEMENTS; i++) {
				playerVariables.setAchieved(i, buf.get() == 1 ? true : false);
			}
		}
		if (buf.hasRemaining())

		{
			String[] degrades = IoBufferUtils.getRS2String(buf).split(";");
			if (degrades.length == playerVariables.getDegradingCharges().length) {
				for (int i = 0; i < playerVariables.getDegradingCharges().length; i++) {
					String[] degrades_data = degrades[i].split(",");
					playerVariables.set_degrade_charges(Integer.parseInt(degrades_data[0]),
							Integer.parseInt(degrades_data[1]));
				}
			}
		}
		if (buf.hasRemaining())

		{
			getAttributes().set("last_clan", IoBufferUtils.getRS2String(buf));
		}
		if (buf.hasRemaining())

		{
			playerVariables.setMode(buf.getInt());
		}

		if (buf.hasRemaining()) {
			playerVariables.setRecoilUses(buf.getInt());
		}

		if (buf.hasRemaining()) {
			playerVariables.setNoTeleport(buf.get() == 1 ? true : false);
		}
		if (buf.hasRemaining()) {
			playerVariables.setLastMacAddress(IoBufferUtils.getRS2String(buf));
		}
		if (buf.hasRemaining()) {
			playerVariables.setLoyaltyPoints(buf.getInt());
		}
		if (buf.hasRemaining()) {
			playerVariables.setTitle(IoBufferUtils.getRS2String(buf));
		}
		if (buf.hasRemaining()) {
			for (int i = 0; i < playerVariables.getScrollProgression().length; i++) {
				playerVariables.getScrollProgression()[i] = (buf.getInt());
			}
		}
		if (buf.hasRemaining()) {
			for (int i = 0; i < playerVariables.getScrollSteps().length; i++) {
				playerVariables.getScrollSteps()[i] = (buf.getInt());
			}
		}
		if (buf.hasRemaining()) {
			for (int i = 0; i < playerVariables.getClueScrolls().length; i++) {
				String result = IoBufferUtils.getRS2String(buf);
				AbstractClueScroll scroll = null;
				if (result.equals("none")) {
					scroll = null;
				} else {
					try {
						scroll = (AbstractClueScroll) Class.forName(result).newInstance();
					} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
						e.printStackTrace();
					}
				}
				playerVariables.getClueScrolls()[i] = scroll;
			}
		}
		if (buf.hasRemaining()) {
			for (int i = 0; i < Constants.TITLES.length; i++) {
				if (buf.get() == 1)
					playerVariables.unlockTitle(i);
			}
		}
		if (buf.hasRemaining()) {
			getWalkingQueue().setRunningToggled(buf.get() == 1 ? true : false);
		}
		if (buf.hasRemaining()) {
			playerVariables.setSmallPouch(buf.getInt());
		}
		if (buf.hasRemaining()) {
			playerVariables.setMediumPouch(buf.getInt());
		}
		if (buf.hasRemaining()) {
			playerVariables.setLargePouch(buf.getInt());
		}
		if (buf.hasRemaining()) {
			playerVariables.setGiantPouch(buf.getInt());
		}
		if (buf.hasRemaining()) {
			for (int i = 0; i < 4; i++) {
				playerVariables.getGwdKillcount()[i] = buf.getInt();
			}
		}
		if (buf.hasRemaining()) {
			playerVariables.setPlayTime(buf.getLong());
		}
		if (buf.hasRemaining()) {
			playerVariables.setReceivedEasterRewards(buf.getInt() == 1 ? true : false);
		}
		if (buf.hasRemaining()) {
			for (int i = 0; i < 10; i++) {
				playerVariables.getTabStarts()[i] = buf.getInt();
			}
		}
		if (buf.hasRemaining()) {
			playerVariables.setExtraExpTimer(buf.getLong());
		}
		if (buf.hasRemaining()) {
			for (int i = 0; i < playerVariables.killcountNpcs.length; i++) {
				playerVariables.getKillcount()[playerVariables.killcountNpcs[i]] = buf.getInt();
			}
		}
		if (buf.hasRemaining()) {
			playerVariables.setFamiliarId(buf.getInt());
		}
		if (buf.hasRemaining()) {
			settings.setAutoRetaliate(buf.getInt() == 1);
		}
		if (buf.hasRemaining()) {
			getCombatState().setPoisonAmount(buf.getInt());
		}
		if (buf.hasRemaining()) {
			getCombatState().setTbTime(buf.getInt());
		}
		if (buf.hasRemaining()) {
			getSettings().setSkullCycles(buf.getInt());
		}
		if (buf.hasRemaining()) {
			playerVariables.setLastTarget(IoBufferUtils.getRS2String(buf));
		}
		if (buf.hasRemaining()) {
			getVariables().setKillStreak(buf.getInt());
		}
		if (buf.hasRemaining()) {
			getVariables().setHighestKillStreak(buf.getInt());
		}
		if (buf.hasRemaining()) {
			getVariables().setBounties(buf.getInt());
		}
		if (buf.hasRemaining()) {
			getVariables().setCanUseBountyTele(buf.getInt() == 1);
		}
		if (buf.hasRemaining()) {
			getVariables().setKills(buf.getInt());
		}
		if (buf.hasRemaining()) {
			getVariables().setDeaths(buf.getInt());
		}
		if (buf.hasRemaining()) {
			getVariables().setSessionName(IoBufferUtils.getRS2String(buf));
		}
		if (buf.hasRemaining()) {
			getVariables().setVoteCount(buf.getInt());
		}
		if (buf.hasRemaining()) {
			playerVariables.hideRank(buf.getInt() == 1);
		}
		if (buf.hasRemaining()) {
			getVariables().getLootshare().setPoints(buf.getInt());
		}

	}

	@Override
	public void serialize(IoBuffer buf) {
		IoBufferUtils.putRS2String(buf, password);
		buf.put((byte) (members ? 1 : 0));
		buf.put((byte) name.hashCode());
		buf.put((byte) settings.getSpecialAmount());
		buf.put((byte) settings.getMagicType());
		buf.put((byte) settings.getPrayerType());
		IoBufferUtils.putRS2String(buf, settings.getAttackVars().getSkill().toString());
		IoBufferUtils.putRS2String(buf, settings.getAttackVars().getStyle().toString());
		buf.putInt(settings.getAttackVars().getSlot());
		buf.putShort((short) getLocation().getX());
		buf.putShort((short) getLocation().getY());
		buf.put((byte) getLocation().getZ());
		buf.putDouble(getVariables().getRunningEnergy());
		buf.put((byte) (getVariables().hasReceivedStarter() ? 1 : 0));
		int[] look = appearance.getLook();
		int[] colors = appearance.getColors();
		int gender = appearance.gender;
		for (int i = 0; i < 7; i++) {
			buf.putInt(look[i]);
		}
		for (int i = 0; i < 5; i++) {
			buf.putInt(colors[i]);
		}
		buf.put((byte) gender);
		for (int i = 0; i < Equipment.SIZE; i++) {
			Item item = equipment.get(i);
			if (item == null) {
				buf.putShort((short) 65535);
			} else {
				buf.putShort((short) item.getId());
				buf.putInt(item.getCount());
			}
		}
		for (int i = 0; i < Skills.SKILL_COUNT; i++) {
			if (i == 5) {
				buf.putDouble((double) settings.getPrayerPoints());
			}
			buf.put((byte) skills.getLevel(i));
			buf.putDouble((double) skills.getXp(i));
		}
		for (int i = 0; i < 28; i++) {
			Item item = inventory.get(i);
			if (item == null) {
				buf.putShort((short) 65535);
			} else {
				buf.putShort((short) item.getId());
				buf.putInt(item.getCount());
			}
		}
		for (int i = 0; i < Bank.SIZE; i++) {
			Item item = bank.get(i);
			if (item == null) {
				buf.putShort((short) 65535);
			} else {
				buf.putShort((short) item.getId());
				buf.putInt(item.getCount());
			}
		}
		for (int i = 0; i < bound_offer_uids.length; i++) {
			buf.putLong(bound_offer_uids[i]);
		}
		buf.put((byte) friends.getPrivateStatus());
		buf.putShort((short) getFriends().getFriendsList().size());
		for (int i = 0; i < getFriends().getFriendsList().size(); i++) {
			IoBufferUtils.putRS2String(buf, getFriends().getFriendsList().get(i));
		}
		buf.putShort((short) getFriends().getIgnoresList().size());
		for (int i = 0; i < getFriends().getIgnoresList().size(); i++) {
			IoBufferUtils.putRS2String(buf, getFriends().getIgnoresList().get(i));
		}
		buf.put((byte) (getVariables().finishedMageArena ? 1 : 0));
		buf.put((byte) (getVariables().finishedRfd ? 1 : 0));
		buf.put((byte) (getVariables().isExpLocked() ? 1 : 0));
		buf.put((byte) (getVariables().isDessousKilled() ? 1 : 0));
		buf.put((byte) (getVariables().isKamilKilled() ? 1 : 0));
		buf.put((byte) (getVariables().isFareedKilled() ? 1 : 0));
		buf.put((byte) (getVariables().isDamisKilled() ? 1 : 0));
		buf.put((byte) (getVariables().isAhrimsKilled() ? 1 : 0));
		buf.put((byte) (getVariables().isDharoksKilled() ? 1 : 0));
		buf.put((byte) (getVariables().isKarilsKilled() ? 1 : 0));
		buf.put((byte) (getVariables().isVeracsKilled() ? 1 : 0));
		buf.put((byte) (getVariables().isToragsKilled() ? 1 : 0));
		buf.put((byte) (getVariables().isGuthansKilled() ? 1 : 0));
		buf.put((byte) getVariables().getBrothersKilled());
		buf.put((byte) getVariables().getDefenderWave());
		buf.putInt(playerVariables.getPestPoints());
		buf.putInt(playerVariables.getMageArenaPoints());
		buf.putInt(playerVariables.getVotePoints());
		buf.putInt(playerVariables.getBarrowPoints());
		buf.putInt(playerVariables.getPvpPoints());
		buf.put((byte) playerVariables.getFightCavesWave());
		buf.put((byte) (settings.isPrivateChatSplit() ? 1 : 0));
		buf.put((byte) (settings.isAcceptAidEnabled() ? 1 : 0));
		for (int i = 0; i < 4; i++) {
			if (playerVariables.getBankPin() == null) {
				buf.put((byte) -1);
			} else {
				buf.put((byte) playerVariables.getBankPin()[i]);
			}
		}
		buf.putLong(playerVariables.lastPinChange);
		buf.putLong(playerVariables.lastDeletionRequest);
		buf.put((byte) playerVariables.recoveryDelay);
		buf.putInt(playerVariables.getAchievementPoints());
		buf.putInt(playerVariables.getTaskAmount());
		IoBufferUtils.putRS2String(buf, playerVariables.getTaskName());

		buf.putInt(playerVariables.isKendal_killed() ? 1 : 0);
		buf.putInt(playerVariables.isMother_killed() ? 1 : 0);
		buf.putInt(playerVariables.getPestRound());
		buf.putInt(playerVariables.getRcPoints());
		buf.putInt(playerVariables.getWandWave());
		buf.putInt(playerVariables.isDemon_killed() ? 1 : 0);
		buf.putInt(playerVariables.finishedLunars ? 1 : 0);
		buf.putInt(playerVariables.isAtJungleDemon() ? 1 : 0);
		buf.putInt(playerVariables.getTotal_clues_finished());
		buf.putInt(playerVariables.getSlayerPoints());
		buf.putInt(playerVariables.getTotalExp());
		for (int i = 0; i < Achievements.MAX_ACHIEVEMENTS; i++) {
			buf.putInt(playerVariables.getAchievementAmounts()[i]);
		}
		for (int i = 0; i < Achievements.MAX_ACHIEVEMENTS; i++) {
			buf.put((byte) (playerVariables.getAchieved()[i] == true ? 1 : 0));
		}

		String[] degrades = new String[38];// TODO - change wen we add more
		// degradables.
		for (int i = 0; i < degrades.length; i++) {
			degrades[i] = i + "," + playerVariables.getDegradingCharges()[i];
		}
		IoBufferUtils.putRS2String(buf, TextUtils.implode(";", degrades));

		IoBufferUtils.putRS2String(buf,
				(String) (getAttributes().isSet("last_clan") ? getAttributes().get("last_clan") : ""));

		buf.putInt(playerVariables.getMode());

		buf.putInt(playerVariables.getRecoilUses());

		buf.put((byte) (playerVariables.isNoTeleport() == true ? 1 : 0));
		IoBufferUtils.putRS2String(buf, /* getMacAddress() */"");
		buf.putInt(playerVariables.getLoyaltyPoints());
		IoBufferUtils.putRS2String(buf, playerVariables.getTitle());
		for (int i = 0; i < playerVariables.getScrollProgression().length; i++) {
			buf.putInt(playerVariables.getScrollProgression()[i]);
		}
		for (int i = 0; i < playerVariables.getScrollSteps().length; i++) {
			buf.putInt(playerVariables.getScrollSteps()[i]);
		}
		for (int i = 0; i < playerVariables.getClueScrolls().length; i++) {
			AbstractClueScroll scroll = playerVariables.getClueScrolls()[i];
			String clazz = "";
			if (scroll == null) {
				clazz = "none";
			} else {
				clazz = scroll.getClass().getCanonicalName();
			}
			IoBufferUtils.putRS2String(buf, clazz);
		}
		for (int i = 0; i < Constants.TITLES.length; i++) {
			buf.put((byte) (playerVariables.getUnlockedTitles()[i] == true ? 1 : 0));
		}
		buf.put((byte) (getWalkingQueue().isRunningToggled() == true ? 1 : 0));
		buf.putInt(playerVariables.getSmallPouch());
		buf.putInt(playerVariables.getMediumPouch());
		buf.putInt(playerVariables.getLargePouch());
		buf.putInt(playerVariables.getGiantPouch());
		for (int i = 0; i < playerVariables.getGwdKillcount().length; i++) {
			buf.putInt(playerVariables.getGwdKillcount()[i]);
		}
		buf.putLong(playerVariables.getPlayTime());
		buf.putInt(playerVariables.isReceivedEasterRewards() ? 1 : 0);
		for (int i = 0; i < 10; i++) {
			buf.putInt(playerVariables.getTabStarts()[i]);
		}
		buf.putLong(playerVariables.getExtraExpTimer());

		for (int i = 0; i < playerVariables.killcountNpcs.length; i++) {
			buf.putInt(playerVariables.getKillcount()[playerVariables.killcountNpcs[i]]);
		}
		buf.putInt(playerVariables.getFamiliarId());
		buf.putInt(settings.isAutoRetaliate() ? 1 : 0);
		buf.putInt(getCombatState().getPoisonAmount());
		buf.putInt(getCombatState().getTeleblockTime());
		buf.putInt(settings.getSkullCycles());
		IoBufferUtils.putRS2String(buf, getVariables().getLastTarget());
		buf.putInt(getVariables().getKillStreak());
		buf.putInt(getVariables().getHighestKillStreak());
		buf.putInt(getVariables().getBounties());
		buf.putInt(getVariables().canUseBountyTele() ? 1 : 0);
		buf.putInt(getVariables().getKills());
		buf.putInt(getVariables().getDeaths());
		IoBufferUtils.putRS2String(buf, playerVariables.getSessionName());
		buf.putInt(getVariables().getVoteCount());
		buf.putInt(playerVariables.isRankHidden() ? 1 : 0);
		buf.putInt(playerVariables.getLootshare().getPoints());
	}

	@Override
	public int getClientIndex() {
		return this.getIndex() + 32768;
	}

	/**
	 * Heals the player if needed
	 *
	 * @param hitpoints
	 *            The amount to heal
	 */
	@Override
	public void heal(int hitpoints) {
		if (this.getCombatState().isDead()) {
			return;
		}
		if (getSkills().getLevel(Skills.HITPOINTS) >= 99) {
			return;
		}
		final int current = getSkills().getLevel(Skills.HITPOINTS);
		if (current + hitpoints > getSkills().getLevelForXp(Skills.HITPOINTS)) {
			getSkills().setLevel(Skills.HITPOINTS, getSkills().getLevelForXp(Skills.HITPOINTS));
		} else {
			getSkills().setLevel(Skills.HITPOINTS, (current + hitpoints));
		}
	}

	@Override
	public boolean isNPC() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isPlayer() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public Location getCentreLocation() {
		// TODO Auto-generated method stub
		return getLocation();
	}

	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		if (appearance.isNpc()) {
			int id = appearance.getNpcId();
			CachedNpcDefinition def = CachedNpcDefinition.getNPCDefinitions(id);
			if (def != null) {
				return def.size;
			}
		}
		return 1;
	}

	@Override
	public boolean isDestroyed() {
		if (!World.getWorld().isPlayerOnline(this.getName()))
			return true;
		if (getSession() == null)
			return true;
		return false;
	}

	public void processHit(Hit hit) {
		if (getPrimaryHit() == null) {
			this.setPrimaryHit(hit);
			this.sendDamage(hit, hit.getType() == HitType.POISON_DAMAGE);
			this.getUpdateFlags().flag(UpdateFlag.HIT);
		} else {
			this.setSecondaryHit(hit);
			this.sendDamage(hit, hit.getType() == HitType.POISON_DAMAGE);
			this.getUpdateFlags().flag(UpdateFlag.HIT_2);
		}
	}

	public void processQueuedHits() {
		// System.out.println("a");
		/*
		 * Gets the next two hits from the queue.
		 */
		List<Hit> hits = getCombatState().getHitQueue();
		Hit first = null;
		if (hits.size() > 0) {
			for (int i = 0; i < hits.size(); i++) {
				Hit hit = hits.get(i);
				if (hit.getDelay() < 1) {
					first = hit;
					hits.remove(hit);
					break;
				}
			}
		}
		if (first != null) {
			this.setPrimaryHit(first);
			this.sendDamage(first, first.getType() == HitType.POISON_DAMAGE);
			this.getUpdateFlags().flag(UpdateFlag.HIT);
		}

		Hit second = null;
		if (hits.size() > 0) {
			for (int i = 0; i < hits.size(); i++) {
				Hit hit = hits.get(i);
				if (hit.getDelay() < 1) {
					second = hit;
					hits.remove(hit);
					break;
				}
			}
		}
		if (second != null) {
			this.setSecondaryHit(second);
			this.sendDamage(second, second.getType() == HitType.POISON_DAMAGE);
			this.getUpdateFlags().flag(UpdateFlag.HIT_2);
		}

		if (hits.size() > 0) {// tells us we still have more hits
			Iterator<Hit> hitIt = hits.iterator();
			while (hitIt.hasNext()) {
				Hit hit = hitIt.next();
				if (hit.getDelay() > 0) {
					hit.setDelay(hit.getDelay() - 1);
				}
				if (hit.getHitPriority() == HitPriority.LOW_PRIORITY) {
					hitIt.remove();
				}
			}
		}
	}

	@Override
	public void inflictDamage(Hit hit, boolean poison) {
		// TODO Auto-generated method stub
		boolean damageOverZero = hit.getDamage() > 0;
		if (hit.getDamage() > skills.getLevel(3)) {
			hit.setDamage(skills.getLevel(3));
		}
		if (damageOverZero && hit.getDamage() == 0) {
			hit.setType(Hits.HitType.NO_DAMAGE);
		}
		if (poison) {
			System.out.println("handling poison ");
			if (getCombatState().isVenomized()) {
				System.out.println("player is venomized");
				hit.setType(HitType.VENOM);
			} else {
				System.out.println("player is poisoned");
				hit.setType(HitType.POISON_DAMAGE);
			}
		}
		if (hit.getDamage() <= 0) {
			hit.setDamage(0);
			hit.setType(Hits.HitType.NO_DAMAGE);
		}
		boolean redemption = prayers.isPrayerActive("Redemption");
		if (redemption) {
			final int percentage = (int) (getMaxHp() * 0.10);
			if (getHp() <= percentage && !(getHp() <= 0)) {
				playGraphic(436, 0, 0);
				skills.setLevel(5, 0);
				packetSender.sendSkillLevel(5);
				heal((int) (skills.getLevelForXp(5) * 0.25));
			}
		}
		if (getCombatState().getHitQueue().size() >= 4) {
			hit = new Hit(hit.getDamage(), HitPriority.LOW_PRIORITY, hit.getCombatType());
		}
		boolean delay = true;
		if (hit.getOwner() != null && hit.getTarget() != null) {
			if (hit.getOwner().isPlayer() && !hit.getTarget().isNPC()) {
				Player p = (Player) hit.getOwner();
				int pid = this.getAttributes().getInt("random_pid");
				if (p.getAttributes().getInt("random_pid") < pid) {
					delay = false;
				}
			}
		}
		if (delay) {
			getCombatState().getHitQueue().add(hit);
		} else {
			processHit(hit);
		}
	}

	@Override
	public void sendDamage(final Hit hit, boolean poison) {
		// TODO Auto-generated method stub
		skills.setLevel(3, skills.getLevel(3) - hit.getDamage());
		if (skills.getLevel(3) <= 0) {
			skills.setLevel(3, 0);
			if (!getCombatState().isDead()) {
				this.getCombatState().setDead(true);
				MainCombat.endCombat(this, 1);
				World.getWorld().submit(new Tickable(3) {
					@Override
					public void execute() {
						this.stop();
						World.getWorld().submit(new DeathTick(getPlayer(), hit.getOwner()));
						animate(Animation.create(836, AnimationPriority.HIGH));
						CombatEffects.executeRetributionPrayer(getPlayer());
					}
				});
			}
		}
	}

	public Prayers getPrayers() {
		return prayers;
	}

	@Override
	public int getHp() {
		// TODO Auto-generated method stub
		return this.getSkills().getLevel(3);
	}

	@Override
	public int getMaxHp() {
		// TODO Auto-generated method stub
		return this.getSkills().getLevelForXp(3);
	}

	@Override
	public void setHp(int val) {
		// TODO Auto-generated method stub
		this.getSkills().setLevel(3, val);
		packetSender.sendSkillLevel(3);
	}

	@Override
	public boolean isAutoRetaliating() {
		return settings.isAutoRetaliate();
	}

	@Override
	public void resetVariables() {
		getCombatState().getHitQueue().clear();
		getVariables().setRunningEnergy(100, true);
		getWalkingQueue().reset();
		getSettings().resetSpecial();
		getPrayers().deactivateAllPrayers();
		getSkills().setSkillsToNormal();
		setHp(getMaxHp());
		getCombatState().getDamageMap().reset();
		getCombatState().setCharged(false);
		getCombatState().setVenged(false);
		getCombatState().setLastTarget(null);
		getCombatState().setPoisonAmount(0);
		getCombatState().setVenomAmount(0);
		getCombatState().setLastVengeance((long) 0);
		getCombatState().setTbTime(0);
		getCombatState().setTarget(null);
		getCombatState().setHitDelay(1);// one tick.
		getCombatState().setEatingState(true);
		getCombatState().setDrinkingState(true);
		getCombatState().setFreezable(true);
		getCombatState().setFrozen(false);
		getCombatState().setCanMove(true);
		getCombatState().setCurrentAttacker(null);
		getCombatState().setPreviousAttacker(null);
		getAttributes().remove("boltpoison");
		getAttributes().remove("disableheadiconprayers");
		World.getWorld().submit(new Tickable(1) {
			@Override
			public void execute() {
				this.stop();
				animate(65535);
				getCombatState().setDead(false);
			}
		});
		getSettings().setSkullCycles(0);
	}

	public Player getPlayer() {
		return this;
	}

	public Container[] getItemsKeptOnDeath() {
		List<Item> itemsToDrop = new ArrayList<Item>();
		List<Item> itemsToKeep = new ArrayList<Item>();
		inventory.refresh();
		for (Item item : inventory.getItems()) {
			if (item != null) {
				itemsToDrop.add(new Item(item.getId(), item.getCount()));
			}
		}
		equipment.refresh();
		for (Item item : equipment.getItems()) {
			if (item != null) {
				itemsToDrop.add(new Item(item.getId(), item.getCount()));
			}
		}
		int keep = 3;
		if (prayers.isProtectingItem()) {
			keep++;
		}
		if (settings.isSkulled()) {
			keep -= 3;
		}
		if (keep > 0) {
			Collections.sort(itemsToDrop, new Comparator<Item>() {
				@Override
				public int compare(Item arg0, Item arg1) {
					int a0 = arg0.getDefinition().forId(arg0.getId()).getHighAlchValue();
					int a1 = arg1.getDefinition().forId(arg0.getId()).getHighAlchValue();
					return a1 - a0;
				}
			});
			List<Item> toKeep = new ArrayList<Item>();
			for (Item item : itemsToDrop) {
				if (keep > 0) {
					if (item.getCount() == 1) {
						toKeep.add(item);
						keep--;
					} else {
						int smallest = keep < item.getCount() ? keep : item.getCount();
						toKeep.add(new Item(item.getId(), smallest));
						keep -= smallest;

					}
				} else {
					break;
				}
			}
			for (Item k : toKeep) {
				itemsToDrop.remove(k);
				itemsToKeep.add(k);
			}
			for (Item item : itemsToDrop) {
				for (Item k : toKeep) {
					if (!item.getDefinition().isStackable() || k.getId() != item.getId())
						continue;
					if (k.getId() == item.getId()) {
						item.setItemAmount(item.getCount() - k.getCount());
						break;
					}
				}
			}
		}
		Container lostItems = new Container(this, Type.NORMAL, ContainerName.IKOD, 41, null);
		Container savedItems = new Container(this, Type.NORMAL, ContainerName.IKOD, 41, null);
		for (Item droppedItems : itemsToDrop) {
			if (droppedItems != null) {
				lostItems.addItem(droppedItems);
			}
		}
		for (Item keptItems : itemsToKeep) {
			if (keptItems != null) {
				if (BountyHunterEmblem.isEmblem(keptItems.getId()))
					lostItems.addItem(keptItems);
				else
					savedItems.addItem(keptItems);
			}
		}
		return new Container[] { savedItems, lostItems };
	}

	@Override
	public void dropLoot(Entity killer) {
		if (getDetails().isOwner())
			return;

		Player pKiller = killer.isPlayer() ? (Player) killer : null;
		if (pKiller != null) {
			if (!pKiller.getAttributes().isSet("clansession") && pKiller.getDuelSession() == null) {
				if (pKiller.getAttributes().isSet("lastKill")) {
					if ((System.currentTimeMillis() - pKiller.getAttributes().getLong("lastKill")) <= 60000) {
						Achievements.increase(pKiller, 50);
					}
				} else
					pKiller.getAttributes().set("lastKill", System.currentTimeMillis());
				getVariables().increaseDeaths(1);
				pKiller.getVariables().increaseKills(1);
				pKiller.getVariables().increaseKillStreak(1);
				if (getVariables().getKillStreak() >= 5)
					getPacketSender().sendNewsMessage(pKiller.getName() + " just ended " + getName()
							+ "'s killstreak of " + getVariables().getKillStreak() + "!");
				getVariables().resetKillStreak();
			}
		}

		Container[] items = getItemsKeptOnDeath();
		Container itemsKept = items[0];
		Container itemsLost = items[1];

		if (getAttributes().isSet("target")) {
			if (pKiller != null && pKiller.getAttributes().isSet("target")
					&& ((String) pKiller.getAttributes().get("target")).equalsIgnoreCase(getName())) {
				Achievements.increase(pKiller, 48);
				itemsLost = BountyHunter.handleDrop(this, pKiller, itemsLost);
				pKiller.getVariables().setLastTarget(getName());
			}

			// Remove ourselves
			getAttributes().remove("target");
			getPacketSender().sendHintArrow(null);
			World.getWorld().getPlayersInWilderness().remove(getName());
		}

		inventory.clear();
		equipment.clear();
		for (Item item : itemsKept.getItems()) {
			if (item != null) {
				inventory.addItem(item);
			}
		}
		for (Item item : itemsLost.getItems()) {
			if (item != null) {

				if (item.getCount() <= 0) {
					item.setItemAmount(1);
				}
				Item itemToDrop = item;

				boolean playerBound = (!itemToDrop.getDefinition().isTradeable()
						|| itemToDrop.getDefinition().isPlayerBound())
						&& !(itemToDrop.getDefinition().getName().toLowerCase().contains("mysterious emblem"));
				if (playerBound) {
					if (killer.isPlayer() && pKiller != null
							&& item.getDefinition().getName().toLowerCase().contains("chaotic")) {
						// If killed by a player and is chaotic drop 500k
						int amount = 500000 * item.getCount();
						GroundItem groundItem = new GroundItem(new Item(995, amount >= 500000 ? amount : 500000), this,
								pKiller);
						GroundItemManager.create(groundItem, pKiller);
					} else if (getDetails().isDonator() || getAttributes().get("clansession") != null) {
						// if player is a donator or in clan wars keep playerbound in invy
						inventory.addItem(item);
					} else {
						// otherwise playerbounds are dropped normally
						GroundItem groundItem = new GroundItem(new Item(item.getId(), item.getCount()), this);
						GroundItemManager.create(groundItem, this);
					}
				} else {
					if (killer.isNPC()) {
						GroundItem groundItem = new GroundItem(itemToDrop, this, this);
						groundItem.setSpawnDelay(-1);
						GroundItemManager.create(groundItem, this);
					} else if (killer.isPlayer() && pKiller != null && pKiller.getDetails().isIronman()) {
						GroundItem groundItem = new GroundItem(itemToDrop, this, this);
						groundItem.setSpawnDelay(-1);
						GroundItemManager.create(groundItem, this);
					} else if (pKiller != null) {
						GroundItemManager.create(new GroundItem(itemToDrop, this, pKiller), pKiller);
					}
				}
			}
		}
		if (killer.isNPC()) {
			GroundItem groundItem = new GroundItem(new Item(526, 1), this, this);
			groundItem.setSpawnDelay(-1);
			GroundItemManager.create(groundItem, this);
		} else {
			GroundItemManager.create(new GroundItem(new Item(526, 1), this, pKiller), pKiller);
		}
		getInventory().refresh();
		Equipment.setWeapon(this, true);
		getBonuses().refresh();
		World.getWorld().getWorldLoader().savePlayer(this);
	}

	@Override
	public Location getDeathArea() {
		// TODO Auto-generated method stub
		return Constants.RESPAWN_LOCATION;
	}

	@Override
	public void loadMapRegions() {
		boolean wasAtDynamicRegion = isAtDynamicRegion();
		super.loadMapRegions();
		clientLoadedMapRegion = false;
		if (isAtDynamicRegion()) {
			getPacketSender().sendConstructedRegion();
			if (!wasAtDynamicRegion) {
				getLocalNPCs().clear();
			}
		} else {
			getPacketSender().sendMapRegion();
			if (wasAtDynamicRegion)
				getLocalNPCs().clear();
		}
		forceNextMapLoadRefresh = false;
	}

	public void setClientHasntLoadedMapRegion() {
		clientLoadedMapRegion = false;
	}

	public boolean clientHasLoadedMapRegion() {
		return clientLoadedMapRegion;
	}

	public void setClientHasLoadedMapRegion() {
		clientLoadedMapRegion = true;
	}

	public boolean isForceNextMapLoadRefresh() {
		return forceNextMapLoadRefresh;
	}

	public void setForceNextMapLoadRefresh(boolean forceNextMapLoadRefresh) {
		this.forceNextMapLoadRefresh = forceNextMapLoadRefresh;
	}

	public String getMacAddress() {
		String connectedMac = "";
		try {
			if (getSession() == null || getSession().getRemoteAddress() == null
					|| ((InetSocketAddress) getSession().getRemoteAddress()).getAddress() == null)
				return connectedMac;
			((InetSocketAddress) getSession().getRemoteAddress()).getAddress();
			InetAddress a = null;
			try {
				a = InetAddress.getLocalHost();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
			NetworkInterface n = NetworkInterface.getByInetAddress(a);
			byte[] m = n.getHardwareAddress();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < m.length; i++) {
				sb.append(String.format("%02X%s", m[i], (i < m.length - 1) ? "-" : ""));
			}
			connectedMac = sb.toString();
		} catch (SocketException e) {
			e.printStackTrace();
		}
		return connectedMac;
	}

	/**
	 * Takes money away from the player's inventory if possible, and returns true.
	 * Otherwise, false.
	 *
	 * @param amount
	 *            The amount to take
	 * @return
	 */
	public boolean takeMoney(int amount) {
		if (inventory.getItemAmount(995) >= amount)
			if (inventory.deleteItem(995, amount)) {
				inventory.refresh();
				return true;
			}
		return false;
	}

	public void sm(String message) {
		getPacketSender().sendMessage(message);
	}

	public void sendMessage(String message) {
		getPacketSender().sendMessage(message);
	}

}
