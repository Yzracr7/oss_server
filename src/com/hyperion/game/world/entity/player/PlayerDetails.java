package com.hyperion.game.world.entity.player;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.mina.core.session.IoSession;

import com.hyperion.game.Constants;
import com.hyperion.game.net.protocol.ISAACCipher;
import com.hyperion.utility.TextUtils;

/**
 * Contains details about a player (but not the actual <code>Player</code>
 * object itself) that has not logged in yet.
 *
 * @author Graham Edgecombe
 */
public class PlayerDetails {

    /**
     * The session.
     */
    private IoSession session;

    /**
     * The player name.
     */
    private String name;

    /**
     * The player's forum user id.
     */
    private int userId;

    /**
     * forum group
     */
    private int primaryGroup = Rights.OWNER.ordinal();

    /**
     * The secondary groups the user is in
     */
    private List<Integer> secondaryGroups = new ArrayList<>();

    /**
     * The player password.
     */
    private String pass;

    /**
     * The incoming ISAAC cipher.
     */
    private ISAACCipher inCipher;

    /**
     * The outgoing ISAAC cipher.
     */
    private ISAACCipher outCipher;

    /**
     * Crc is outdated for this player.
     */
    private boolean outdated;

    private int clientMode = 464;
    private int gameFrame = 474;
    private int hitmarks = 474;

    private boolean newRegistration, orbsOn, hpbars, newCursors, newMenus, newHits, tweening, hd, hdOnLogin, isCensorOn = true, leftclickAttack;

    private int clientSize;
    private boolean isAdmin, isModerator, isIronman, isDonator, isSuperDonator, isExtremeDonator, isLegendaryDonator;

    private int tokens, totalSpent;
    private int unreadMessages;
    private String currentEmail;


    /**
     * Creates the player details class.
     *
     * @param session   The session.
     * @param name      The name.
     * @param pass      The password.
     * @param uid       The unique id.
     * @param inCipher  The incoming cipher.
     * @param outCipher The outgoing cipher.
     */
    public PlayerDetails(IoSession session, String name, String pass, ISAACCipher inCipher, ISAACCipher outCipher) {
        this.session = session;
        this.name = TextUtils.formatName(name);
        this.pass = pass;
        this.inCipher = inCipher;
        this.outCipher = outCipher;
        this.userId = -1;
    }

    public PlayerDetails(IoSession session, String name, String pass, ISAACCipher inCipher, ISAACCipher outCipher, boolean outdated) {
        this.session = session;
        this.name = TextUtils.formatName(name);
        this.pass = pass;
        this.inCipher = inCipher;
        this.outCipher = outCipher;
        this.userId = -1;
        this.outdated = outdated;
    }

    public PlayerDetails(IoSession session, String name, String pass, ISAACCipher inCipher, ISAACCipher outCipher, boolean outdated, int clientMode, int gameFrame, int hitmarks, boolean orbsOn, boolean hpbars, boolean newCursors, boolean newMenus, boolean newHits, boolean tweening, boolean hd, boolean hdOnLogin, int clientSize, boolean isCensorOn) {
        this.session = session;
        this.name = TextUtils.formatName(name);
        this.pass = pass;
        this.inCipher = inCipher;
        this.outCipher = outCipher;
        this.userId = -1;
        this.outdated = outdated;
        this.clientMode = clientMode;
        this.gameFrame = gameFrame;
        this.hitmarks = hitmarks;
        this.orbsOn = orbsOn;
        this.hpbars = hpbars;
        this.newCursors = newCursors;
        this.newMenus = newMenus;
        this.newHits = newHits;
        this.tweening = tweening;
        this.hd = hd;
        this.hdOnLogin = hd;
        this.clientSize = clientSize;
        this.isCensorOn = true;
    }

    public boolean setDisplayName(String name) {
        if (this.name.equalsIgnoreCase(name)) {
            this.name = name;
            return true;
        }
        return false;
    }

    public int getRights() {
        if (isOwner() || isAdmin()) {
            return 2;
        } else if (isModerator()) {
            return 1;
        } else if (isIronman()) {
            return 3;
        } else if (isSuperDonator()) {
            return 7;
        } else if (isExtremeDonator()) {
            return 5;
        } else if (isLegendaryDonator()) {
            return 8;
        } else if (isDonator()) {
            return 4;
        }
        // 6 - support already done client sided
        //TODO: gfx designerwould be 9
        return 0;
    }

    /**
     * Gets the <code>IoSession</code>.
     *
     * @return The <code>IoSession</code>.
     */
    public IoSession getSession() {
        return session;
    }

    /**
     * Gets the name.
     *
     * @return The name.
     */
    public String getName() {
        return TextUtils.formatName(name);
    }

    public String setName(String name) {
        return this.name = name;
    }

    public long getUsernameAsLong() {
        return TextUtils.stringToLong(getName());
    }

    /**
     * Gets the password.
     *
     * @return The password.
     */
    public String getPassword() {
        return pass;
    }

    /**
     * Gets the incoming ISAAC cipher.
     *
     * @return The incoming ISAAC cipher.
     */
    public ISAACCipher getInCipher() {
        return inCipher;
    }

    /**
     * Gets the outgoing ISAAC cipher.
     *
     * @return The outgoing ISAAC cipher.
     */
    public ISAACCipher getOutCipher() {
        return outCipher;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isOutdated() {
        return outdated;
    }

    public void setOutdated(boolean outdated) {
        this.outdated = outdated;
    }

    public int getPrimaryGroup() {
        return primaryGroup;
    }

    public void setPrimaryGroup(int group) {
        this.primaryGroup = group;
    }

    public int getClientMode() {
        return clientMode;
    }

    public void setClientMode(int clientMode) {
        this.clientMode = clientMode;
    }

    public int getGameframe() {
        return gameFrame;
    }

    public void setGameframe(int gameFrame) {
        this.gameFrame = gameFrame;
    }

    public boolean isOrbsOn() {
        return orbsOn;
    }

    public void setOrbs(boolean orbs) {
        this.orbsOn = orbs;
    }

    public boolean isNewHpbar() {
        return hpbars;
    }

    public void setHpbar(boolean hpbar) {
        this.hpbars = hpbar;
    }

    public boolean isNewCursors() {
        return newCursors;
    }

    public void setCursors(boolean boo) {
        this.newCursors = boo;
    }

    public boolean isNewMenus() {
        return newMenus;
    }

    public void setMenus(boolean boo) {
        this.newMenus = boo;
    }

    public boolean isNewHits() {
        return newHits;
    }

    public void setNewHits(boolean boo) {
        this.newHits = boo;
    }

    public boolean isTweeningOn() {
        return tweening;
    }

    public void setTweening(boolean boo) {
        this.tweening = boo;
    }

    public boolean isLeftclickAttack() {
        return leftclickAttack;
    }

    public void setLeftclickAttack(boolean boo) {
        this.leftclickAttack = boo;
    }

    public boolean isHdOn() {
        return hd;
    }

    public void setHd(boolean boo) {
        this.hd = boo;
    }

    public boolean hdOnLogin() {
        return hd;
    }

    public int getHitmarks() {
        return hitmarks;
    }

    public void setHitmarks(int hitmarks) {
        this.hitmarks = hitmarks;
    }

    public boolean isAdmin() {
        return isAdmin || hasGroupRights(Rights.ADMINISTRATOR);
    }

    /**
     * This method creates a list of all the groups the user is part of by
     * combining the {@link #primaryGroup} with {@link #secondaryGroups}
     *
     * @return
     */
    public List<Integer> getAllGroups() {
        List<Integer> groups = new ArrayList<>();
        groups.add(primaryGroup);
        groups.addAll(secondaryGroups);
        return groups;
    }

    /**
     * If the player has the group rights. This method loops through all rights
     * in {@link #getAllGroups()} and checks if the right matches the one we
     * want
     *
     * @param right The right
     * @return
     */
    public boolean hasGroupRights(Rights right) {
        for (Integer group : getAllGroups()) {
            if (group == right.getMemberGroupId()) {
                return true;
            }
        }
        return false;
    }

    public boolean isOwner() {
        for (String owner : Constants.OWNERS) {
            if (name.equalsIgnoreCase(owner)) {
                return true;
            }
        }
        return false;
    }

    public void setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public boolean isModerator() {
        return isModerator || hasGroupRights(Rights.MODERATOR);
    }

    public void setModerator(boolean isModerator) {
        this.isModerator = isModerator;
    }

    public boolean isIronman() {
        return isIronman || hasGroupRights(Rights.IRONMAN);
    }

    public void setIronman(boolean isIronman) {
        this.isIronman = isIronman;
    }

    public int getUnreadMessages() {
        return unreadMessages;
    }

    public void setUnreadMessages(int unreadMessages) {
        this.unreadMessages = unreadMessages;
    }

    public int getTokenCount() {
        return tokens;
    }

    public void setTokenCount(int c) {
        this.tokens = c;
    }

    public int getTotalSpent() {
        return totalSpent;
    }

    public void setTotalSpent(int c) {
        this.totalSpent = c;
    }

    public String getCurrentEmail() {
        return currentEmail;
    }

    public void setCurrentEmail(String currentEmail) {
        this.currentEmail = currentEmail;
    }

    public boolean isDonator() {
        return isDonator || isSuperDonator() || isExtremeDonator() || isLegendaryDonator();
    }

    public void setDonator(boolean isDonator) {
        this.isDonator = isDonator;
    }

    public boolean isRegularDonator() {
        return isDonator || hasGroupRights(Rights.DONATOR);
    }

    public boolean isSuperDonator() {
        return isSuperDonator || hasGroupRights(Rights.SUPER_DONATOR);
    }

    public void setSuperDonator(boolean isSuperDonator) {
        this.isSuperDonator = isSuperDonator;
    }

    //public boolean isFullscreen() {
    //	return fullscreen;
    //}

    public int getClientSize() {
        return clientSize;
    }

    public void setClientSize(int i) {
        clientSize = i;
    }

    public String getMacAddress() {
        String connectedMac = "";
        try {
            if (getSession() == null || getSession().getRemoteAddress() == null)
                return connectedMac;
            if (((InetSocketAddress) getSession().getRemoteAddress()).getAddress() == null)
                return connectedMac;
            InetAddress a = InetAddress.getLocalHost() != null ? InetAddress.getLocalHost() : null;
            if (a == null)
                return connectedMac;
            NetworkInterface n = NetworkInterface.getByInetAddress(a);
            byte[] m = n.getHardwareAddress();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < m.length; i++) {
                sb.append(String.format("%02X%s", m[i], (i < m.length - 1) ? "-" : ""));
            }
            connectedMac = sb.toString();
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return connectedMac;
    }

    /**
     * @return
     */
    public String getIP() {
        return getSession() != null && getSession().getRemoteAddress() != null ? getSession().getRemoteAddress().toString().substring(1).split(":")[0] : "127.0.0.1";
    }

    /**
     * @return the newRegistration
     */
    public boolean isNewRegistration() {
        return newRegistration;
    }

    /**
     * @param newRegistration the newRegistration to set
     */
    public void setNewRegistration(boolean newRegistration) {
        this.newRegistration = newRegistration;
    }

    /**
     * @return the hdOnLogin
     */
    public boolean isHdOnLogin() {
        return hdOnLogin;
    }

    /**
     * @param hdOnLogin the hdOnLogin to set
     */
    public void setHdOnLogin(boolean hdOnLogin) {
        this.hdOnLogin = hdOnLogin;
    }

    /**
     * @return the secondaryGroups
     */
    public List<Integer> getSecondaryGroups() {
        return secondaryGroups;
    }

    /**
     * @param secondaryGroups the secondaryGroups to set
     */
    public void setSecondaryGroups(List<Integer> secondaryGroups) {
        this.secondaryGroups = secondaryGroups;
    }

    /**
     * @return the isExtremeDonator
     */
    public boolean isExtremeDonator() {
        return isExtremeDonator || hasGroupRights(Rights.EXTREME_DONATOR);
    }

    /**
     * @param isExtremeDonator the isExtremeDonator to set
     */
    public void setExtremeDonator(boolean isExtremeDonator) {
        this.isExtremeDonator = isExtremeDonator;
    }

    /**
     * @return the isLegendaryDonator
     */
    public boolean isLegendaryDonator() {
        return isLegendaryDonator || hasGroupRights(Rights.LEGENDARY_DONATOR);
    }

    /**
     * @param isLegendaryDonator the isLegendaryDonator to set
     */
    public void setLegendaryDonator(boolean isLegendaryDonator) {
        this.isLegendaryDonator = isLegendaryDonator;
    }

    public Rights getMainGroup() {
        return Rights.getRights(getPrimaryGroup());
    }

    public boolean isStaff() {
        return isOwner() || isAdmin() || isModerator();
    }

    public boolean isCensorOn() {
        return isCensorOn;
    }

    public void setCensor(boolean b) {
        this.isCensorOn = b;
    }
}
