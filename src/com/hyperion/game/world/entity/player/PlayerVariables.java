package com.hyperion.game.world.entity.player;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.CopyOnWriteArrayList;

import com.hyperion.game.Constants;
import com.hyperion.game.content.InterfaceTools.InterfaceComponents;
import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.content.clanchat.loot.Lootshare;
import com.hyperion.game.content.clanchat.loot.LootshareStatus;
import com.hyperion.game.content.clues.AbstractClueScroll;
import com.hyperion.game.content.clues.ClueScrollComplexity;
import com.hyperion.game.content.minigames.GameSession;
import com.hyperion.game.content.miniquests.RoundBasedMiniquest;
import com.hyperion.game.content.skills.fishing.FishingSession;
import com.hyperion.game.content.skills.mining.MineSession;
import com.hyperion.game.content.skills.prayer.BonesOnAltarSession;
import com.hyperion.game.content.skills.woodcutting.CutSession;
import com.hyperion.game.item.Item;
import com.hyperion.game.net.Packet;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.masks.ForceMovement;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.game.world.grounditems.GroundItem;
import com.hyperion.game.world.shopping.Shop;

public class PlayerVariables {

	private Player player;

	public PlayerVariables(Player player) {
		this.player = player;
	}

	private int total_clues_finished;
	private int totalExp, voteCount;
	private short[] degradingCharges = new short[38];
	private String taskName, lastAddress = "", sessionName;
	private int taskAmount;
	private int packetsExecuted;
	private int[] bankPin = null;
	private boolean atJungleDemon;
	public boolean finishedRfd, finishedMageArena, finishedLunars, isTasFinished;
	private boolean expLocked;
	private boolean kendal_killed, mother_killed, demon_killed;
	private boolean dessousKilled, kamilKilled, fareedKilled, damisKilled;
	private boolean ahrimsKilled, dharoksKilled, karilsKilled, veracsKilled, toragsKilled, guthansKilled;
	private int wandWave, defenderWave;
	private int mageArenaPoints, votePoints, barrowPoints, pestPoints, slayerPoints, rcPoints, achievementPoints;
	private int pvpPoints, brothersKilled, fightCavesWave = -1, pestRound;
	private double runningEnergy = 100;
	private boolean started;
	private int recoilUses = 0;
	private long playTime;
	private int loyaltyPoints;
	private int smallPouch = 0, mediumPouch = 0, largePouch = 0, giantPouch = 0;
	private boolean receivedEasterRewards = false;
	private String title = "";
	private int[] tabStartSlot = new int[10];
	private long extraExpTimer;
	private int familiarId;
	private String lastTarget = "";
	private int killStreak, highestKillStreak;
	private int bounties;
	private boolean canUseBountyTele;
	private int kills, deaths;
	private boolean hideRank;
	private Lootshare lootshare = new Lootshare(LootshareStatus.DISABLED, 0);

	public boolean isEnabled() {
		return lootshare.getStatus().equals(LootshareStatus.ENABLED);
	}

	/**
	 * @return the lootshare
	 */
	public Lootshare getLootshare() {
		return lootshare;
	}

	/**
	 * The amount of steps the player has done for the scroll type.
	 */
	private int[] scrollProgression = new int[ClueScrollComplexity.values().length];

	/**
	 * The amount of steps the player has to do for the scroll type.
	 */
	private int[] scrollSteps = new int[ClueScrollComplexity.values().length];

	/**
	 * The current clue scroll the player has for the scroll type.
	 */
	private AbstractClueScroll[] clueScrolls = new AbstractClueScroll[ClueScrollComplexity.values().length];

	/**
	 * A queue of packets that are pending.
	 */
	private final Queue<Packet> incomingPackets = new LinkedList<Packet>();
	private GameSession game_session;
	private RoundBasedMiniquest currentRoundMiniquest;
	private Shop skillCapeShop = new Shop(13);
	private long lastAction;
	private Location lastLocation;
	private int foodTimer = 0, potionTimer = 0;
	private int mageArenaStage = 0, rfdStage = 0, lunarStage = 0;
	private boolean banking;

	public boolean isUsing_grand_exchange() {
		return using_grand_exchange;
	}

	public void setUsing_grand_exchange(boolean using_grand_exchange) {
		this.using_grand_exchange = using_grand_exchange;
	}

	private boolean using_grand_exchange;

	private int walkAnimation = -1, standAnimation = -1, runAnimation = -1, turnAnimation = -1, turn180Animation = -1,
			turn90Clockwise = -1, turn90CounterClockwise = -1;
	private ForceMovement nextForceMovement;
	private Runnable closeInterfacesEvent;
	private CopyOnWriteArrayList<NPC> ownedNPCS = new CopyOnWriteArrayList<NPC>();
	private int[] achievementAmounts = new int[Achievements.MAX_ACHIEVEMENTS];
	private boolean[] achieved = new boolean[Achievements.MAX_ACHIEVEMENTS];

	private boolean[] unlockedTitles = new boolean[Constants.TITLES.length];
	private boolean noTeleport = false;
	private String lastMacAddress = "none";

	private int gameMode = 0;
	private final int[] gwdKillcount = new int[4];
	private final int[] killcount = new int[15000];

	private final CopyOnWriteArrayList<InterfaceComponents> interfaceComponents = new CopyOnWriteArrayList<>();

	/*
	 * Skill stuff
	 */
	private FishingSession fishing;
	private CutSession cutSession;
	private MineSession mineSession;
	private BonesOnAltarSession bonesOnAltarSession;

	/*
	 * Bank pin stuff
	 */
	public boolean deletePin;
	public int openStatus = 0;
	public boolean pinCorrect;
	public int pinStatus = 0;
	public int setNewPinAttempt = 0;
	public boolean changingPin = false;
	private boolean usedGodwarsAltar = false;
	public int recoveryDelay = 3;
	public long lastPinChange;
	public long lastDeletionRequest;
	public int[] pinSeq;
	public int[] tempPin1 = new int[4];
	public int[] tempPin2 = new int[4];
	public int[] numbers = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	public int getIcon;

	public int getQuestPoints() {
		int points = 0;
		if (finishedRfd)
			points++;
		if (finishedMageArena)
			points++;
		if (finishedLunars)
			points++;

		if (isDtFinished())
			points++;
		if (isKendal_killed())
			points++;
		if (isMother_killed())
			points++;
		if (isDemon_killed())
			points++;
		if (isTasFinished)
			points++;
		return points;
	}

	public boolean isBanking() {
		return banking;
	}

	public void setBanking(boolean banking) {
		this.banking = banking;
	}

	public boolean isExpLocked() {
		return expLocked;
	}

	public void setExpLocked(boolean expLocked) {
		this.expLocked = expLocked;
	}

	public int getFoodTimer() {
		return foodTimer;
	}

	public void setFoodTimer(int foodTimer) {
		this.foodTimer = foodTimer;
	}

	public int getPotionTimer() {
		return potionTimer;
	}

	public void setPotionTimer(int potionTimer) {
		this.potionTimer = potionTimer;
	}

	/**
	 * Sets the players energy.
	 * 
	 * @param runningEnergy
	 * @return sets the Players energy.
	 */
	public void setRunningEnergy(double runningEnergy, boolean send) {
		this.runningEnergy = runningEnergy;
		if (send) {
			player.getPacketSender().sendEnergy();
		}
	}

	/**
	 * Gets the players Energy.
	 * 
	 * @return The players Energy.
	 */
	public double getRunningEnergy() {
		return runningEnergy;
	}

	public int getRecoilUses() {
		return recoilUses;
	}

	public void setRecoilUses(int uses) {
		this.recoilUses = uses;
	}

	public boolean isNoTeleport() {
		return noTeleport;
	}

	/*
	 * Set whether player can teleport; used for Random Events
	 * 
	 */
	public void setNoTeleport(boolean b) {
		this.noTeleport = b;
	}

	public int getMageArenaPoints() {
		return mageArenaPoints;
	}

	public void setMageArenaPoints(int mageArenaPoints) {
		this.mageArenaPoints = mageArenaPoints;
	}

	public void increaseMageArenaPoints(int amount) {
		this.mageArenaPoints += amount;
	}

	public void deductMageArenaPoints(int amount) {
		if (this.mageArenaPoints - amount < 0) {
			this.mageArenaPoints = 0;
		} else
			this.mageArenaPoints -= amount;
	}

	public int getRfdStage() {
		return rfdStage;
	}

	public void setRfdStage(int rfdStage) {
		this.rfdStage = rfdStage;
	}

	public void increaseRfdStage(int amount) {
		this.rfdStage += amount;
	}

	public int getVotePoints() {
		return votePoints;
	}

	public void setVotePoints(int votePoints) {
		this.votePoints = votePoints;
	}

	public void increaseVotePoints(int amount) {
		this.votePoints += amount;
	}

	public void deductVotePoints(int amt) {
		if (this.votePoints - amt < 0) {
			this.votePoints = 0;
		} else
			this.votePoints -= amt;
	}

	public int getPvpPoints() {
		return pvpPoints;
	}

	public void setPvpPoints(int pvpPoints) {
		this.pvpPoints = pvpPoints;
	}

	public void increasePvpPoints(int amt) {
		this.pvpPoints += amt;
	}

	public void deductPvpPoints(int amt) {
		if (this.pvpPoints - amt < 0) {
			this.pvpPoints = 0;
		} else
			this.pvpPoints -= amt;

	}

	public int getBarrowPoints() {
		return barrowPoints;
	}

	public void setBarrowPoints(int barrowPoints) {
		this.barrowPoints = barrowPoints;
	}

	public void deductBarrowPoints(int amount) {
		if (this.barrowPoints - amount < 0) {
			this.barrowPoints = 0;
		} else
			this.barrowPoints -= amount;
	}

	public void increaseBarrowPoints(int amount) {
		this.barrowPoints += amount;
	}

	public int getMageArenaStage() {
		return mageArenaStage;
	}

	public void setMageArenaStage(int mageArenaStage) {
		this.mageArenaStage = mageArenaStage;
	}

	public void increaseMageArenaStage(int amount) {
		this.mageArenaStage += amount;
	}

	public int getDefenderWave() {
		return defenderWave;
	}

	public void setDefenderWave(int defenderWave) {
		this.defenderWave = defenderWave;
	}

	public void increaseDefenderWave(int amt) {
		this.defenderWave += amt;
	}

	public boolean isDessousKilled() {
		return dessousKilled;
	}

	public void setDessousKilled(boolean dessousKilled) {
		this.dessousKilled = dessousKilled;
	}

	public boolean isKamilKilled() {
		return kamilKilled;
	}

	public void setKamilKilled(boolean kamilKilled) {
		this.kamilKilled = kamilKilled;
	}

	public boolean isFareedKilled() {
		return fareedKilled;
	}

	public void setFareedKilled(boolean fareedKilled) {
		this.fareedKilled = fareedKilled;
	}

	public boolean isDamisKilled() {
		return damisKilled;
	}

	public void setDamisKilled(boolean damisKilled) {
		this.damisKilled = damisKilled;
	}

	public boolean isDtFinished() {
		return (this.isDamisKilled() && this.isKamilKilled() && this.isFareedKilled() && this.isDessousKilled());
	}

	public boolean isAhrimsKilled() {
		return ahrimsKilled;
	}

	public void setAhrimsKilled(boolean ahrimsKilled) {
		this.ahrimsKilled = ahrimsKilled;
	}

	public boolean isDharoksKilled() {
		return dharoksKilled;
	}

	public void setDharoksKilled(boolean dharoksKilled) {
		this.dharoksKilled = dharoksKilled;
	}

	public boolean isKarilsKilled() {
		return karilsKilled;
	}

	public void setKarilsKilled(boolean karilsKilled) {
		this.karilsKilled = karilsKilled;
	}

	public boolean isVeracsKilled() {
		return veracsKilled;
	}

	public void setVeracsKilled(boolean veracsKilled) {
		this.veracsKilled = veracsKilled;
	}

	public boolean isToragsKilled() {
		return toragsKilled;
	}

	public void setToragsKilled(boolean toragsKilled) {
		this.toragsKilled = toragsKilled;
	}

	public boolean isGuthansKilled() {
		return guthansKilled;
	}

	public void setGuthansKilled(boolean guthansKilled) {
		this.guthansKilled = guthansKilled;
	}

	public void resetBrothersKilled() {
		this.ahrimsKilled = false;
		this.dharoksKilled = false;
		this.karilsKilled = false;
		this.toragsKilled = false;
		this.guthansKilled = false;
		this.veracsKilled = false;
		this.brothersKilled = 0;
	}

	public int getBrothersKilled() {
		return brothersKilled;
	}

	public void setBrothersKilled(int brothersKilled) {
		this.brothersKilled = brothersKilled;
	}

	public void increaseBrothersKilled(int amount) {
		this.brothersKilled += amount;
	}

	public Location getLastLocation() {
		return lastLocation;
	}

	public void setLastLocation(Location lastLocation) {
		this.lastLocation = lastLocation;
	}

	public long getLastAction() {
		return lastAction;
	}

	public void setLastAction(long lastAlch) {
		this.lastAction = lastAlch;
	}

	public boolean hasReceivedStarter() {
		return started;
	}

	public void setStarted(boolean started) {
		this.started = started;
	}

	public int getPestPoints() {
		return pestPoints;
	}

	public void setPestPoints(int pestPoints) {
		this.pestPoints = pestPoints;
	}

	public void increasePestPoints(int amt) {
		this.pestPoints += amt;
	}

	public void deductPestPoints(int amt) {
		if (this.pestPoints - amt < 0) {
			this.pestPoints = 0;
		} else
			this.pestPoints -= amt;
	}

	public Shop getSkillCapeShop() {
		return skillCapeShop;
	}

	public void set_skillcape_shop(Item[] stock) {
		this.skillCapeShop.setStock(stock);
	}

	public Queue<Packet> getIncomingPackets() {
		// synchronized(incomingPackets) {
		return incomingPackets;
		// }
	}

	public int getFightCavesWave() {
		return fightCavesWave;
	}

	public void setFightCavesWave(int fightCavesWave) {
		this.fightCavesWave = fightCavesWave;
	}

	public int getWalkAnimation() {
		int anim = CombatAnimations.getWalkAnimation(player);
		if (walkAnimation != -1) {
			return walkAnimation;
		}
		return anim;
	}

	public void setWalkAnimation(int walkAnimation) {
		this.walkAnimation = walkAnimation;
	}

	public int getStandAnimation() {
		int anim = CombatAnimations.getStandAnimation(player);
		if (standAnimation != -1) {
			return standAnimation;
		}
		return anim;
	}

	public void setStandAnimation(int standAnimation) {
		this.standAnimation = standAnimation;
	}

	public int getRunAnimation() {
		int anim = CombatAnimations.getRunAnimation(player);
		if (runAnimation != -1) {
			return runAnimation;
		}
		return anim;
	}

	public void setRunAnimation(int runAnimation) {
		this.runAnimation = runAnimation;
	}

	public int getTurnAnimation() {
		int anim = CombatAnimations.getTurnAnimation(player);
		if (turnAnimation != -1) {
			return turnAnimation;
		}
		return anim;
	}

	public void setTurnAnimation(int turnAnimation) {
		this.turnAnimation = turnAnimation;
	}

	public int getTurn180Animation() {
		int anim = CombatAnimations.getTurn180Animation(player);
		if (turn180Animation != -1) {
			return turn180Animation;
		}
		return anim;
	}

	public void setTurn180Animation(int turn180Animation) {
		this.turn180Animation = turn180Animation;
	}

	public int getTurn90Clockwise() {
		int anim = CombatAnimations.getTurn90ClockwiseAnimation(player);
		if (turn90Clockwise != -1) {
			return turn90Clockwise;
		}
		return anim;
	}

	public void setTurn90Clockwise(int turn90Clockwise) {
		this.turn90Clockwise = turn90Clockwise;
	}

	public int getTurn90CounterClockwise() {
		int anim = CombatAnimations.getTurn90CounterClockwiseAnimation(player);
		if (turn90CounterClockwise != -1) {
			return turn90CounterClockwise;
		}
		return anim;
	}

	public void setTurn90CounterClockwise(int turn90CounterClockwise) {
		this.turn90CounterClockwise = turn90CounterClockwise;
	}

	public boolean isKendal_killed() {
		return kendal_killed;
	}

	public boolean isDemon_killed() {
		return demon_killed;
	}

	public void setKendal_killed(boolean kendal_killed) {
		this.kendal_killed = kendal_killed;
	}

	public void setDemon_killed(boolean demon_killed) {
		this.demon_killed = demon_killed;
	}

	public boolean isMother_killed() {
		return mother_killed;
	}

	public void setMother_killed(boolean mother_killed) {
		this.mother_killed = mother_killed;
	}

	public GameSession getGameSession() {
		return game_session;
	}

	public void setGameSession(GameSession game_session) {
		this.game_session = game_session;
	}

	public int getPestRound() {
		return pestRound;
	}

	public void setPestRound(int pestRound) {
		this.pestRound = pestRound;
	}

	public int getLunarStage() {
		return lunarStage;
	}

	public void setLunarStage(int lunarStage) {
		this.lunarStage = lunarStage;
	}

	public void increase_lunar_stage(int amt) {
		this.lunarStage += amt;
	}

	public boolean isAtJungleDemon() {
		return atJungleDemon;
	}

	public void setAtJungleDemon(boolean atJungleDemon) {
		this.atJungleDemon = atJungleDemon;
	}

	public String getTaskName() {
		if (taskName == null) {
			taskName = "";
		}
		return taskName;
	}

	public int getTotal_clues_finished() {
		return total_clues_finished;
	}

	public void setTotal_clues_finished(int total_clues_finished) {
		this.total_clues_finished = total_clues_finished;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public int getTaskAmount() {
		return taskAmount;
	}

	public void setTaskAmount(int taskAmount) {
		this.taskAmount = taskAmount;
	}

	public void increase_task(int amt) {
		this.taskAmount += amt;
	}

	public void deduct_task(int amt) {
		if (this.taskAmount - amt < 0) {
			this.taskAmount = 0;
		} else
			this.taskAmount -= amt;
	}

	public int getSlayerPoints() {
		return slayerPoints;
	}

	public void setSlayerPoints(int slayerPoints) {
		this.slayerPoints = slayerPoints;
	}

	public void increase_slayer_points(int amt) {
		this.slayerPoints += amt;
	}

	public void deduct_slayer_points(int amt) {
		if (this.slayerPoints - amt < 0) {
			this.slayerPoints = 0;
		} else
			this.slayerPoints -= amt;
	}

	public short[] getDegradingCharges() {
		return degradingCharges;
	}

	public int getDegradedAmount(int index) {
		return degradingCharges[index];
	}

	public void increase_degrade_charges(int index, int amt) {
		this.degradingCharges[index] += amt;
	}

	public void deduct_degrade_charges(int index, int amt) {
		this.degradingCharges[index] -= amt;
	}

	public void set_degrade_charges(int index, int amt) {
		this.degradingCharges[index] = (short) amt;
	}

	public FishingSession getFishing() {
		return fishing;
	}

	public void setFishing(FishingSession fishing) {
		this.fishing = fishing;
	}

	public int getTotalExp() {
		return totalExp;
	}

	public void setTotalExp(int totalExp) {
		this.totalExp = totalExp;
	}

	public void increaseTotalExp(int amt) {
		this.totalExp += amt;
	}

	public void handle_counter(boolean reset) {
		// 553 = xp flag that fades
		if (reset) {
			setTotalExp(0);
			player.getPacketSender().sendConfig(554, getTotalExp());
			return;
		}
		int val = player.getAttributes().getInt("counter");
		if (val == 0) {
			player.getAttributes().set("counter", 1);
		} else {
			player.getAttributes().set("counter", 0);
		}
		val = player.getAttributes().getInt("counter");
		player.getPacketSender().sendConfig(554, getTotalExp());
		player.getPacketSender().sendConfig(555, val <= 0 ? 1 : 0);
	}

	public CutSession getCutSession() {
		return cutSession;
	}

	public void setCutSession(CutSession cutSession) {
		this.cutSession = cutSession;
	}

	public MineSession getMineSession() {
		return mineSession;
	}

	public void setMineSession(MineSession mineSession) {
		this.mineSession = mineSession;
	}

	public int getRcPoints() {
		return rcPoints;
	}

	public void setRcPoints(int rcPoints) {
		this.rcPoints = rcPoints;
	}

	public void increase_rc_points(int amt) {
		this.rcPoints += amt;
	}

	public void decrease_rc_points(int amt) {
		this.rcPoints -= amt;
		if (this.rcPoints < 0) {
			this.rcPoints = 0;
		}
	}

	public int getWandWave() {
		return wandWave;
	}

	public void setWandWave(int wandWave) {
		this.wandWave = wandWave;
	}

	public void increase_wand_wave(int amt) {
		this.wandWave += amt;
	}

	public String getLastAddress() {
		if (lastAddress == null)
			lastAddress = "";
		return lastAddress;
	}

	public void setLastAddress(String ad) {
		this.lastAddress = ad;
	}

	public void refreshLastAddress() {
		if (player.getSession() == null || player.getSession().getRemoteAddress() == null)
			return;
		String address = player.getSession().getRemoteAddress().toString();
		address = address.substring(1).substring(0, address.indexOf(":") - 1);
		this.lastAddress = address;
	}

	public int getVoteCount() {
		return voteCount;
	}

	public void setVoteCount(int voteCount) {
		this.voteCount = voteCount;
	}

	public BonesOnAltarSession getBonesOnAltarSession() {
		return bonesOnAltarSession;
	}

	public void setBonesOnAltarSession(BonesOnAltarSession bonesOnAltarSession) {
		this.bonesOnAltarSession = bonesOnAltarSession;
	}

	public void refreshSpawnedItems() {
		for (int regionId : player.getMapRegionsIds()) {
			List<GroundItem> floorItems = World.getWorld().getRegion(regionId, false).getGroundItems();
			if (floorItems == null)
				continue;
			for (GroundItem item : floorItems) {
				if (!item.isOwnedBy(player) || item.getLocation().getZ() != player.getLocation().getZ())
					continue;
				player.getPacketSender().clearGroundItem(item);
			}
		}
		for (int regionId : player.getMapRegionsIds()) {
			List<GroundItem> floorItems = World.getWorld().getRegion(regionId, false).getGroundItems();
			if (floorItems == null)
				continue;
			for (GroundItem item : floorItems) {
				if (!item.isOwnedBy(player) || item.getLocation().getZ() != player.getLocation().getZ())
					continue;
				player.getPacketSender().sendGroundItem(item);
			}
		}
	}

	public void refreshSpawnedObjects() {
		for (int regionId : player.getMapRegionsIds()) {
			List<GameObject> removedObjects = World.getWorld().getRegion(regionId, false).getRemovedOriginalObjects();
			for (GameObject object : removedObjects)
				player.getPacketSender().removeObject(object);
			List<GameObject> spawnedObjects = World.getWorld().getRegion(regionId, false).getSpawnedObjects();
			for (GameObject object : spawnedObjects) {
				if (object.getPlane() == player.getLocation().getZ())
					player.getPacketSender().createObject(object);
			}
		}
	}

	public ForceMovement getNextForceMovement() {
		return nextForceMovement;
	}

	public void setNextForceMovement(ForceMovement nextForceMovement) {
		this.nextForceMovement = nextForceMovement;
	}

	public void setCloseInterfacesEvent(Runnable closeInterfacesEvent) {
		this.closeInterfacesEvent = closeInterfacesEvent;
	}

	public Runnable getCloseInterfacesEvent() {
		return this.closeInterfacesEvent;
	}

	public RoundBasedMiniquest getCurrentRoundMiniquest() {
		return currentRoundMiniquest;
	}

	public void setCurrentRoundMiniquest(RoundBasedMiniquest currentRoundMiniquest) {
		this.currentRoundMiniquest = currentRoundMiniquest;
	}

	public CopyOnWriteArrayList<NPC> getOwnedNPCS() {
		return ownedNPCS;
	}

	public void addOwnedNPC(NPC npc) {
		ownedNPCS.add(npc);
		if (!npc.getAttributes().isSet("petOwner"))
			npc.getAttributes().set("owner", player);
	}

	public int getPacketsExecuted() {
		return packetsExecuted;
	}

	public void setPacketsExecuted(int packetsExecuted) {
		this.packetsExecuted = packetsExecuted;
	}

	public void increasePacketsExecuted() {
		this.packetsExecuted++;
	}

	public int[] getAchievementAmounts() {
		return achievementAmounts;
	}

	public void setAchievementAmount(int achievement, int amount) {
		this.achievementAmounts[achievement] = amount;
	}

	public void setAchievementPoints(int points) {
		this.achievementPoints = points;
	}

	public int getAchievementPoints() {
		return achievementPoints;
	}

	public boolean[] getAchieved() {
		return achieved;
	}

	public void setAchieved(int achievement, boolean completed) {
		this.achieved[achievement] = completed;
	}

	public boolean[] getUnlockedTitles() {
		return this.unlockedTitles;
	}

	public void unlockTitle(int title) {
		this.unlockedTitles[title] = true;
	}

	public void setMode(int mode) { // 0 = normal 1 = ironman
		this.gameMode = mode;
	}

	public int getMode() {
		return gameMode;
	}

	/**
	 * @return the lastMacAddress
	 */
	public String getLastMacAddress() {
		return lastMacAddress;
	}

	/**
	 * @param lastMacAddress
	 *            the lastMacAddress to set
	 */
	public void setLastMacAddress(String lastMacAddress) {
		this.lastMacAddress = lastMacAddress;
	}

	/**
	 * Adds the {@link #playTime} by 1
	 */
	public void incrementPlayTime() {
		playTime++;
	}

	/**
	 * @return the loyaltyPoints
	 */
	public int getLoyaltyPoints() {
		return loyaltyPoints;
	}

	/**
	 * @param loyaltyPoints
	 *            the loyaltyPoints to set
	 */
	public void setLoyaltyPoints(int loyaltyPoints) {
		this.loyaltyPoints = loyaltyPoints;
	}

	/**
	 * Adds the {@link #loyaltyPoints} by 1
	 */
	public void incrementLoyaltyPoints() {
		loyaltyPoints++;
	}

	public void increaseLoyaltyPoints(int amt) {
		loyaltyPoints += amt;
	}

	public void decreaseLoyaltyPoints(int amt) {
		loyaltyPoints -= amt;
	}

	/**
	 * Gets the player's title.
	 * 
	 * @return The player's title.
	 */
	public String getLastTarget() {
		return lastTarget;
	}

	public void setLastTarget(String lastTarget) {
		this.lastTarget = lastTarget;
	}

	/**
	 * Gets the player's title.
	 * 
	 * @return The player's title.
	 */
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the bankPin
	 */
	public int[] getBankPin() {
		return bankPin;
	}

	/**
	 * @param bankPin
	 *            the bankPin to set
	 */
	public void setBankPin(int[] bankPin) {
		this.bankPin = bankPin;
	}

	/**
	 * @return the scrollProgression
	 */
	public int[] getScrollProgression() {
		return scrollProgression;
	}

	/**
	 * @return the scrollSteps
	 */
	public int[] getScrollSteps() {
		return scrollSteps;
	}

	/**
	 * @return the clueScrolls
	 */
	public AbstractClueScroll[] getClueScrolls() {
		return clueScrolls;
	}

	/**
	 * @param scrollProgression
	 *            the scrollProgression to set
	 */
	public void setScrollProgression(int[] scrollProgression) {
		this.scrollProgression = scrollProgression;
	}

	/**
	 * @param scrollSteps
	 *            the scrollSteps to set
	 */
	public void setScrollSteps(int[] scrollSteps) {
		this.scrollSteps = scrollSteps;
	}

	/**
	 * @param clueScrolls
	 *            the clueScrolls to set
	 */
	public void setClueScrolls(AbstractClueScroll[] clueScrolls) {
		this.clueScrolls = clueScrolls;
	}

	public int[] getTabStarts() {
		return tabStartSlot;
	}

	public void setTabStarts(int[] tabStarts) {
		this.tabStartSlot = tabStarts;
	}

	public int getSmallPouch() {
		return smallPouch;
	}

	public void setSmallPouch(int amount) {
		this.smallPouch = amount;
	}

	public int getMediumPouch() {
		return mediumPouch;
	}

	public void setMediumPouch(int amount) {
		this.mediumPouch = amount;
	}

	public int getLargePouch() {
		return largePouch;
	}

	public void setLargePouch(int amount) {
		this.largePouch = amount;
	}

	public int getGiantPouch() {
		return giantPouch;
	}

	public void setGiantPouch(int amount) {
		this.giantPouch = amount;
	}

	public int[] killcountNpcs = new int[] { 
			
			/*KBD*/
			239, 
			/*CALLISTO*/
			6503, 
			/*VENENATIS*/
			6610, 
			/*ZULRAH*/
			2042, 
			/*VET'ION*/
			6611, 
			/*CRAZY ARCHEOLOGIST*/
			6618, 
			/*CHAOS FANATIC*/
			6619,
			/*CHAOS ELEMENTAL*/
			2054,
			/*SCORPIA*/
			6615
	};

	/**
	 * @return killcount for bosses
	 */
	public int[] getKillcount() {
		return killcount;
	}

	/**
	 * @return the gwdKillcount
	 */
	public int[] getGwdKillcount() {
		return gwdKillcount;
	}

	/**
	 * @return the usedGodwarsAltar
	 */
	public boolean isUsedGodwarsAltar() {
		return usedGodwarsAltar;
	}

	/**
	 * @param usedGodwarsAltar
	 *            the usedGodwarsAltar to set
	 */
	public void setUsedGodwarsAltar(boolean usedGodwarsAltar) {
		this.usedGodwarsAltar = usedGodwarsAltar;
	}

	/**
	 * @return the playTime
	 */
	public long getPlayTime() {
		return playTime;
	}

	/**
	 * @param playTime
	 *            the playTime to set
	 */
	public void setPlayTime(long playTime) {
		this.playTime = playTime;
	}

	public long getExtraExpTimer() {
		return extraExpTimer;
	}

	public void setExtraExpTimer(long time) {
		this.extraExpTimer = time;
	}

	public int getFamiliarId() {
		return familiarId;
	}

	public void setFamiliarId(int id) {
		this.familiarId = id;
	}

	/**
	 * @return
	 */
	public String getFormattedPlaytime() {
		long milliseconds = playTime * 600;
		long days = (milliseconds / 86400000L);
		long hours = ((milliseconds / 3600000L) % 24L);
		long minutes = ((milliseconds / 60000L) % 60L);
		String string = "";
		if (days > 0) {
			String s = days == 1 ? " day " : " days ";
			string += days + s;
		}
		if (hours > 0) {
			String s = hours == 1 ? " hour " : " hours ";
			string += hours + s;
		}
		if (minutes > 0) {
			String s = minutes == 1 ? " min " : " mins ";
			string += minutes + s;
		}
		if (string == "")
			string = "1 minute";
		return string;
	}

	/**
	 * @return the receivedEasterRewards
	 */
	public boolean isReceivedEasterRewards() {
		return receivedEasterRewards;
	}

	/**
	 * @param receivedEasterRewards
	 *            the receivedEasterRewards to set
	 */
	public void setReceivedEasterRewards(boolean receivedEasterRewards) {
		this.receivedEasterRewards = receivedEasterRewards;
	}

	/**
	 * Gets the current text on the inter at the child id
	 * 
	 * @param interfaceId
	 *            The inter
	 * @param childId
	 *            The child id
	 * @return
	 */
	public String getTextAtInterface(int interfaceId, int childId) {
		for (InterfaceComponents component : interfaceComponents) {
			if (component.getInterfaceId() == interfaceId && component.getChildId() == childId) {
				return component.getText();
			}
		}
		return null;
	}

	/**
	 * @param interfaceId
	 *            The id of the inter
	 * @param childId
	 *            The child id of the inter
	 * @param text
	 *            The text over the inter
	 * @return
	 */
	public boolean interfaceRequiresUpdate(int interfaceId, int childId, String text) {
		String currentText = getTextAtInterface(interfaceId, childId);
		if (currentText == null) {
			return true;
		}
		if (currentText.equals(text)) {
			return false;
		}
		return true;
	}

	public void registerInterfaceText(int interfaceId, int childId, String text) {
		for (InterfaceComponents component : interfaceComponents) {
			if (component.getInterfaceId() == interfaceId && component.getChildId() == childId) {
				component.setText(text);
				return;
			}
		}
		addComponent(interfaceId, childId, text);
	}

	public void addComponent(int interfaceId, int childId, String text) {
		interfaceComponents.add(new InterfaceComponents(interfaceId, childId, text));
	}

	public void increaseKillStreak(int i) {
		this.killStreak = killStreak + i;
		if (killStreak > highestKillStreak) {
			this.highestKillStreak = this.killStreak;
			player.getPacketSender()
					.sendMessage("You have broken your previous killstreak record! You are on a killstreak of "
							+ killStreak + " players!");
		} else {
			player.getPacketSender().sendMessage("You are on a killstreak of " + killStreak + " players!");
		}
		if (killStreak >= 5) {
			Achievements.increase(player, 45);
			player.getPacketSender().sendNewsMessage(player.getName() + " is on a killstreak of " + killStreak + "!");
		}
		if (killStreak >= 15)
			Achievements.increase(player, 46);
	}

	public void resetKillStreak() {
		this.killStreak = 0;
	}

	public int getKillStreak() {
		return killStreak;
	}

	public int getHighestKillStreak() {
		return highestKillStreak;
	}

	public void setKillStreak(int amount) {
		killStreak = amount;
	}

	public void setHighestKillStreak(int amount) {
		highestKillStreak = amount;
	}

	public int getBounties() {
		return bounties;
	}

	public void setBounties(int amount) {
		bounties = amount;
	}

	public void increaseBounties(int amount) {
		bounties += amount;
	}

	public void deductBounties(int amt) {
		if (bounties - amt < 0) {
			bounties = 0;
		} else
			bounties -= amt;
	}

	public boolean canUseBountyTele() {
		return canUseBountyTele;
	}

	public void setCanUseBountyTele(boolean b) {
		canUseBountyTele = b;
	}

	public void increaseKills(int amount) {
		kills += amount;
		if (kills >= 50)
			Achievements.increase(player, 47);
	}

	public int getKills() {
		return kills;
	}

	public void setKills(int amount) {
		kills = amount;
	}

	public void increaseDeaths(int amount) {
		deaths += amount;
	}

	public int getDeaths() {
		return deaths;
	}

	public void setDeaths(int amount) {
		deaths = amount;
	}

	public void setSessionName(String s) {
		sessionName = s;
	}

	public String getSessionName() {
		if (sessionName == null) {
			sessionName = "";
		}
		return sessionName;
	}

	public boolean isRankHidden() {
		return hideRank;
	}

	public boolean hideRank(boolean b) {
		return hideRank = b;
	}
}
