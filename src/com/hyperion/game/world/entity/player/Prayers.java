package com.hyperion.game.world.entity.player;

import com.hyperion.game.Constants;
import com.hyperion.game.content.minigames.clanwars.ClanWarsData;
import com.hyperion.game.content.minigames.clanwars.session.WarSession;
import com.hyperion.game.content.miniquests.impl.LunarDiplomacy;
import com.hyperion.game.content.miniquests.impl.RecipeForDisaster;
import com.hyperion.game.content.skills.prayer.PrayerData;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.TeleportAreaLocations;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;

public class Prayers extends PrayerData {

    private Player p;

    private int headIcon = -1;
    private int pkIcon = -1;
    private boolean protectingItem;
    private boolean[] activeNormalPrayers;
    private boolean[] activeCursePrayers;
    private int[] leechBonuses;

    public Prayers(Player player) {
        this.p = player;
        this.activeNormalPrayers = new boolean[28];
        this.activeCursePrayers = new boolean[21];
        this.leechBonuses = new int[11];
    }

    public boolean activatePrayers(int button) {
        int index = getPrayerIndex(button);
        if (index == -1) {
            return false;
        }
        int config = PRAYER_CONFIG[index];
        if (p.getCombatState().isDead()) {
            boolean activePrayer = getRegularActivePrayers()[index];
            if (activePrayer) {
                p.getPacketSender().sendConfig(config, 1);
            } else {
                p.getPacketSender().sendConfig(config, 0);
            }
            return false;
        }
        if ((int) p.getSkills().getLevel(5) <= 0) {
            togglePrayer(index, false);
            p.getPacketSender().sendConfig(config, 0);
            return false;
        }
        if (RecipeForDisaster.atArea(p)) {
            togglePrayer(index, false);
            p.getPacketSender().sendConfig(config, 0);
            return false;
        }
        if (LunarDiplomacy.atArea(p)) {
            togglePrayer(index, false);
            p.getPacketSender().sendConfig(config, 0);
            return false;

        }

        if (TeleportAreaLocations.atBarrelChestArea(p.getLocation())) {
            togglePrayer(index, false);
            p.getPacketSender().sendConfig(config, 0);
            return false;
        }
        if (isHeadIconPrayer(index)) {
            if (p.getAttributes().isSet("disableheadiconprayers")) {
                p.getPacketSender().sendMessage("You've been injured and cannot use protection prayers!");
                togglePrayer(index, false);
                p.getPacketSender().sendConfig(config, 0);
                return false;
            }
        }
        if (p.getDuelSession() != null) {
            if (p.getDuelSession().ruleEnabled(7)) {
                p.getPacketSender().sendMessage("Prayers have been disabled in this duel.");
                togglePrayer(index, false);
                p.getPacketSender().sendConfig(config, 0);
                return false;
            }
        }
        String prayerName = PRAYER_NAME[index];
        if (p.getAttributes().isSet("clansession")) {
            WarSession session = p.getAttributes().get("clansession");
            if (session != null) {
                ClanWarsData.Rules prayerRule = session.getRules()[ClanWarsData.Groups.PRAYER.ordinal()];
                if (prayerRule == ClanWarsData.Rules.PRAYER_OFF) {
                    p.getPacketSender().sendMessage("Prayers have been disabled in this war.");
                    togglePrayer(index, false);
                    p.getPacketSender().sendConfig(config, 0);
                    return false;
                } else if (prayerRule == ClanWarsData.Rules.NO_OVERHEADS && prayerName.contains("Protect from")) {
                    p.getPacketSender().sendMessage("Overheads have been disabled in this war.");
                    togglePrayer(index, false);
                    p.getPacketSender().sendConfig(config, 0);
                    return false;
                } else if (session.getMiscRules()[3] == true && (prayerName.equals("Piety")
                        || prayerName.equals("Chivalry"))) {
                    p.getPacketSender().sendMessage("This prayer is not allowed in F2P mode.");
                    togglePrayer(index, false);
                    p.getPacketSender().sendConfig(config, 0);
                    return false;
                }
            }
        }

        if (index == 24) {// chivarly
            if (p.getSkills().getLevelForXp(1) < 65) {
                togglePrayer(index, false);
                p.getPacketSender().sendConfig(config, 0);
                p.getPacketSender().sendMessage("You need a Defence level of 65 to use Chivarly.");
                return false;
            }
        }
        if (index == 25) {// Piety
            if (p.getSkills().getLevelForXp(1) < 70) {
                togglePrayer(index, false);
                p.getPacketSender().sendConfig(config, 0);
                p.getPacketSender().sendMessage("You need a Defence level of 70 to use Piety.");
                return false;
            }
        }

        int requiredLevel = PRAYER_LVL[index];
        if (p.getSkills().getLevelForXp(5) < requiredLevel) {
            togglePrayer(index, false);
            p.getPacketSender().sendConfig(config, 0);
            p.getPacketSender().sendMessage("You need a Prayer level of " + requiredLevel + " to use " + prayerName + ".");
            return false;
        }
        handlePrayerExecuting(index);
        return true;
    }

    private void handlePrayerExecuting(int prayerIndex) {
        boolean turningOn = !getRegularActivePrayers()[prayerIndex];
        int config = PRAYER_CONFIG[prayerIndex];
        if (!turningOn) {
            togglePrayer(prayerIndex, false);
            p.getPacketSender().sendConfig(config, 0);
        } else {
            togglePrayer(prayerIndex, true);
            p.getPacketSender().sendConfig(config, 1);
        }
        int[] prayersToTurnOff = null;

        switch (prayerIndex) {
            case 0:// Thick Skin
                if (turningOn) {
                    prayersToTurnOff = new int[]{5, 13, 24, 25};
                }
                break;
            case 1:// Burst of Strength
                if (turningOn) {
                    prayersToTurnOff = new int[]{6, 14, 3, 11, 19, 24, 25, 4, 12, 20};
                }
                break;
            case 2:// Clarity of thought
                if (turningOn) {
                    prayersToTurnOff = new int[]{7, 15, 3, 11, 19, 24, 25, 4, 12, 20};
                }
                break;
            case 3:// Sharp eye
                if (turningOn) {
                    prayersToTurnOff = new int[]{11, 19, 24, 25, 4, 12, 20, 14, 15, 6, 7, 1, 2};
                }
                break;
            case 4:// Mystic Will
                if (turningOn) {
                    prayersToTurnOff = new int[]{12, 20, 24, 25, 3, 11, 19, 14, 15, 6, 7, 1, 2};
                }
                break;
            case 5:// Rock Skin
                if (turningOn) {
                    prayersToTurnOff = new int[]{0, 13, 24, 25};
                }
                break;
            case 6:// Superhuman strength
                if (turningOn) {
                    prayersToTurnOff = new int[]{1, 14, 24, 25, 3, 11, 19, 4, 12, 20};
                }
                break;
            case 7:// Improved reflexes
                if (turningOn) {
                    prayersToTurnOff = new int[]{2, 15, 24, 25, 3, 11, 19, 4, 12, 20};
                }
                break;
            case 8:// Rapid restore

                break;
            case 9:// Rapid heal

                break;
            case 10:// Protect 1 item

                break;
            case 11:// Hawk eye
                if (turningOn) {
                    prayersToTurnOff = new int[]{3, 19, 24, 25, 4, 12, 20, 14, 15, 6, 7, 1, 2};
                }
                break;
            case 12:// Mystic lore
                if (turningOn) {
                    prayersToTurnOff = new int[]{4, 20, 3, 11, 19, 14, 15, 6, 7, 1, 2, 24, 25};
                }
                break;
            case 13:// Steel skin
                if (turningOn) {
                    prayersToTurnOff = new int[]{0, 5, 24, 25};
                }
                break;
            case 14:// Ultimate str
                if (turningOn) {
                    prayersToTurnOff = new int[]{1, 6, 3, 11, 19, 24, 25, 4, 12, 20};
                }
                break;
            case 15:// Incredible reflexes
                if (turningOn) {
                    prayersToTurnOff = new int[]{2, 7, 3, 11, 19, 24, 25, 4, 12, 20};
                }
                break;
            case 16:// Protect from magic
                if (turningOn) {
                    setHeadIcon(2);
                    prayersToTurnOff = new int[]{17, 18, 21, 22, 23};
                } else {
                    setHeadIcon(-1);
                }
                break;
            case 17:// Protect from missles
                if (turningOn) {
                    setHeadIcon(1);
                    prayersToTurnOff = new int[]{16, 18, 21, 22, 23};
                } else {
                    setHeadIcon(-1);
                }
                break;
            case 18:// Protect from melee
                if (turningOn) {
                    setHeadIcon(0);
                    prayersToTurnOff = new int[]{16, 17, 21, 22, 23};
                } else {
                    setHeadIcon(-1);
                }
                break;
            case 19:// Eagle Eye
                if (turningOn) {
                    prayersToTurnOff = new int[]{3, 11, 24, 25, 4, 12, 20, 14, 15, 6, 7, 1, 2};
                }
                break;
            case 20:// Mystic might
                if (turningOn) {
                    prayersToTurnOff = new int[]{4, 12, 3, 11, 19, 24, 25, 14, 15, 6, 7, 1, 2};
                }
                break;
            case 21:// Retribution
                if (turningOn) {
                    setHeadIcon(3);
                    prayersToTurnOff = new int[]{16, 18, 17, 22, 23};
                } else {
                    setHeadIcon(-1);
                }
                break;
            case 22:// Redemption
                if (turningOn) {
                    setHeadIcon(5);
                    prayersToTurnOff = new int[]{16, 18, 17, 21, 23};
                } else {
                    setHeadIcon(-1);
                }
                break;
            case 23:// Smite
                if (turningOn) {
                    setHeadIcon(4);
                    prayersToTurnOff = new int[]{16, 18, 17, 21, 22};
                } else {
                    setHeadIcon(-1);
                }
                break;
            case 24:// Chivarly
                if (turningOn) {
                    prayersToTurnOff = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 11, 12, 13, 14, 15, 19, 20, 25};
                }
                break;
            case 25:// Piety
                if (turningOn) {
                    prayersToTurnOff = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 11, 12, 13, 14, 15, 19, 20, 24};
                }
                break;
        }
        if (turningOn) {
            if (prayersToTurnOff != null) {
                activateRegularPrayer(prayerIndex, prayersToTurnOff);
            }

        } else {
            closePrayer(prayerIndex);
        }
        if (getActivatedPrayersCount() > 0 && !p.getAttributes().isSet("isDraining")) {
            executePrayerDraining();
            p.getAttributes().set("isDraining", true);
        }
    }

    public int getActivatedPrayersCount() {
        int count = 0;
        for (boolean curse : activeCursePrayers) {
            if (curse) {
                count++;
            }
        }
        for (boolean normal : activeNormalPrayers) {
            if (normal) {
                count++;
            }
        }
        return count;
    }

    private void executePrayerDraining() {
        World.getWorld().submit(new Tickable(p, 2) {
            @Override
            public void execute() {
                if (p.getCombatState().isDead()) {
                    p.getAttributes().remove("isDraining");
                    this.stop();
                    return;
                }
                if (isPrayersOff()) {
                    p.getAttributes().remove("isDraining");
                    this.stop();
                    return;
                }
                double amountDrain = 0;

                for (int i = 0; i < PRAYER_DRAIN.length; i++) {
                    if (getRegularActivePrayers()[i]) {
                        double drain = PRAYER_DRAIN[i];
                        double bonus = 0.035 * p.getBonuses().getBonus(11);
                        drain = drain * (1 + bonus);
                        drain = 0.6 / drain;
                        amountDrain += drain;
                    }
                }

                p.getSettings().decreasePrayerPoints(amountDrain);
                if (p.getSettings().getPrayerPoints() <= 0) {
                    p.getAttributes().remove("isDraining");
                    this.stop();
                }
            }
        });
    }

    /**
     * Turns off prayers; use indexes.
     *
     * @param prayers
     */
    private void activateRegularPrayer(int index, int[] prayers) {
        for (int i : prayers) {
            p.getPacketSender().sendConfig(PRAYER_CONFIG[i], 0);
            togglePrayer(i, false);
            if (!isHeadIconPrayer(index)) {
                if (isHeadIconPrayer(i)) {
                    setHeadIcon(-1);
                }
            }
        }
    }

    private void closePrayer(int index) {
    }

    public void turnRegularPrayersOff(int[] prays) {
        for (int i : prays) {
            p.getPacketSender().sendConfig(PRAYER_CONFIG[i], 0);
            togglePrayer(i, false);
            if (isHeadIconPrayer(i)) {
                setHeadIcon(-1);
            }
        }
    }

    public void deactivateAllPrayers() {
        setHeadIcon(-1);
        for (int i = 0; i < this.activeNormalPrayers.length - 1; i++) {
            p.getPacketSender().sendConfig(PRAYER_CONFIG[i], 0);
            togglePrayer(i, false);
        }
    }

    private boolean isHeadIconPrayer(int index) {
        if (index == 16 || index == 17 || index == 18 || index == 21 || index == 22 || index == 23) {
            return true;
        }
        return false;
    }

    public void setHeadIcon(int headIcon) {
        this.headIcon = headIcon;
        p.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
    }

    public void setPkIcon(int i) {
        this.pkIcon = i;
        p.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
    }

    public int getPkIcon() {
        return pkIcon;
    }

    public int getHeadIcon() {
        return headIcon;
    }

    public boolean[] getRegularActivePrayers() {
        return activeNormalPrayers;
    }

    public boolean[] getActiveCursePrayers() {
        return activeCursePrayers;
    }

    public boolean isPrayerActive(String name) {
        for (int i = 0; i < PRAYER_NAME.length; i++) {
            if (name.equalsIgnoreCase(PRAYER_NAME[i])) {
                if (getRegularActivePrayers()[i]) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isPrayersOff() {
        for (int i = 0; i < getRegularActivePrayers().length; i++) {
            if (getRegularActivePrayers()[i]) {
                return false;
            }
        }
        for (int i = 0; i < getActiveCursePrayers().length; i++) {
            if (getActiveCursePrayers()[i]) {
                return false;
            }
        }
        return true;
    }

    private void togglePrayer(int indexOfPrayer, boolean active) {
        boolean protectItem = (indexOfPrayer == 10 || (indexOfPrayer == 0));
        if (protectItem) {
            setProtectingItem(active);
        }
        this.activeNormalPrayers[indexOfPrayer] = active;
        if (Constants.DEBUG_MODE)
            p.getPacketSender().sendMessage("Turning " + (active ? "On" : "Off") + " normal prayer: " + indexOfPrayer);
    }

    public boolean isProtectingItem() {
        return protectingItem;
    }

    public void setProtectingItem(boolean protectingItem) {
        this.protectingItem = protectingItem;
    }

}
