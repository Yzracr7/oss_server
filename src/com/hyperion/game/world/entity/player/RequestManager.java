package com.hyperion.game.world.entity.player;

import java.util.ArrayList;
import java.util.List;

import com.hyperion.game.Constants;
import com.hyperion.game.content.clanchat.ClanChat;
import com.hyperion.game.content.minigames.clanwars.ClanWarsSession;
import com.hyperion.game.content.minigames.duelarena.DuelSession;
import com.hyperion.game.world.entity.player.container.impl.TradeSession;

public class RequestManager {

	private List<Player> requests;
	
	/**
	 * Represents the different types of request.
	 * 
	 * @author Graham Edgecombe
	 * 
	 */
	public enum RequestType {
		/**
		 * A trade request.
		 */
		TRADE(":tradereq:", "Sending trade offer..."),
		/**
		 * A duel request.
		 */
		DUEL(":duelreq:", "Sending duel request..."),
		/**
		 * a clan war request.
		 */
		CLANWAR(":chalreq:", "Sending war request...");
		
		/**
		 * The client-side name of the request.
		 */
		private String clientName;
		
		/**
		 * The message sent to us
		 */
		private String sentMsg;

		/**
		 * Creates a type of request.
		 * 
		 * @param clientName
		 *            The name of the request client-side.
		 */
		private RequestType(String clientName, String send) {
			this.clientName = clientName;
			this.sentMsg = send;
		}

		/**
		 * Gets the client name.
		 * 
		 * @return The client name.
		 */
		public String getClientName() {
			return clientName;
		}
		
		public String getSentMessage() {
			return sentMsg;
		}
	}
	
	/**
	 * Holds the different states the manager can be in.
	 * 
	 * @author Graham Edgecombe
	 * 
	 */
	private enum RequestState {

		/**
		 * Nobody has offered a request.
		 */
		NORMAL,
		/**
		 * Somebody has offered some kind of request.
		 */
		REQUESTED,
		/**
		 * The player is participating in an existing request of this type, so
		 * cannot accept new requests at all.
		 */
		PARTICIPATING;
	}
	
	/**
	 * The challenger
	 */
	private Player player;
	
	/**
	 * The current state we are in.
	 */
	private RequestState state = RequestState.NORMAL;
	
	/**
	 * Creates a request manager for player.
	 * @param player the player
	 */
	public RequestManager(Player player) {
		this.player = player;
		this.requests = new ArrayList<Player>();
	}
	
	/**
	 * Sends a new request..
	 * @param other the person we are sending the request to.
	 */
	public void sendRequest(Player other, RequestType type) {
		String message = player.getDetails().getName() + ""+type.getClientName()+"";
		String sentMsg = type.getSentMessage();
		player.setInteractingEntity(other);
		//first check if other is busy..
		if (other.getInterfaceSettings().getCurrentInterface() != -1) {
			player.getPacketSender().sendMessage("That player is busy at the moment.");
			return;
		}
		//check if we are busy..
		if (player.getInterfaceSettings().getCurrentInterface() != -1) {
			player.getPacketSender().sendMessage("You're currently busy.");
			return;
		}
		
		if(type == RequestType.CLANWAR) {
			if(!ClanChat.isClanLeader(player))
				return;
		}
		
		//if who we are requesting has already requested to us, we open our request
		//and set both to busy.
		if (other.getRequestManager().hasRequest(player)) {
			handleRequestTypes(other, type);
			return;
		}
		
		//adds other to our list of requests
		addRequest(other);
		//sends our request to us
		player.getPacketSender().sendMessage(sentMsg);
		//sends other the client message
		other.getPacketSender().sendMessage(message);
	}

	/**
	 * We handle our request types i.e open trade ect
	 * @param type the request type
	 */
	public void handleRequestTypes(Player other, RequestType type) {
		if(Constants.DEBUG_MODE) {
			System.out.println("Request type: " + type.toString());
		}
		if (type.getClientName().equals(":tradereq:")) {
			if(player.getClanWarsSession() == null || other.getClanWarsSession() == null) {
				player.setTradeSession(new TradeSession(player, other));
				other.setTradeSession(new TradeSession(other, player));
			} else {
				player.getPacketSender().sendMessage("You are not allowed to trade in the Clan war arena.");
				other.getPacketSender().sendMessage("You are not allowed to trade in the Clan war arena.");
			}
		} else if (type.getClientName().equals(":duelreq:")) {
			player.setDuelSession(new DuelSession(player, other));
			other.setDuelSession(new DuelSession(other, player));
		} else if (type.getClientName().equals(":chalreq:")) {
			if(player.getClanWarsSession() != null || other.getClanWarsSession() != null) {
				player.getPacketSender().sendMessage("You or your opponent are already in an active war.");
				other.getPacketSender().sendMessage("You or your opponent are already in an active war.");
			} else if (!ClanChat.isClanLeader(player)) {
				player.getPacketSender().sendMessage("You or your opponent are not the leader of their clan.");
				other.getPacketSender().sendMessage("You or your opponent are not the leader of their clan.");
			} else {
				player.setClanWarsSession(new ClanWarsSession(player, other));
				other.setClanWarsSession(new ClanWarsSession(other, player));
			}
		}
		//player.setInteractingEntity(null);
		//other.setInteractingEntity(null);
		player.getRequestManager().resetRequests();
		other.getRequestManager().resetRequests();
	}
	
	/**
	 * Checks if other has sent us a request.
	 * @param other the other player
	 * @return if they did or not
	 */
	private boolean hasRequest(Player other) {
		for (Player p : requests) {
			if (p == null)
				continue;
			if (p.equals(other)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Adds other to the requests list.
	 * @param other other player
	 */
	private void addRequest(Player other) {
		player.getRequestManager().setState(RequestState.REQUESTED);
		requests.add(other);
	}
	
	/**
	 * Resets your request state.
	 */
	private void resetRequests() {
		requests.clear();
		setState(RequestState.NORMAL);
	}
	
	public List<Player> getRequests() {
		return requests;
	}

	public RequestState getState() {
		return state;
	}

	public void setState(RequestState state) {
		this.state = state;
	}
}