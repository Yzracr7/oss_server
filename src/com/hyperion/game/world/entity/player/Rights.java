package com.hyperion.game.world.entity.player;

import com.hyperion.game.Constants;

public enum Rights {

	OWNER(13) {
		@Override
		public boolean hasPrivileges(Player player) {
			return Constants.DEBUG_MODE || player.getDetails().isOwner();
		}
	},

	ADMINISTRATOR(4) {
		@Override
		public boolean hasPrivileges(Player player) {
			return player.getDetails().isAdmin();
		}
	},

	MODERATOR(6) {
		@Override
		public boolean hasPrivileges(Player player) {
			return OWNER.hasPrivileges(player) || ADMINISTRATOR.hasPrivileges(player) || player.getDetails().isModerator();
		}
	},

	SUPPORT(9) {
		@Override
		public boolean hasPrivileges(Player player) {
			return ADMINISTRATOR.hasPrivileges(player) || MODERATOR.hasPrivileges(player) || player.getDetails().hasGroupRights(SUPPORT);
		}
	},

	DESIGNER(10) {
		@Override
		public boolean hasPrivileges(Player player) {
			return player.getDetails().hasGroupRights(DESIGNER);
		}
	},

	LEGENDARY_DONATOR(11) {
		@Override
		public boolean hasPrivileges(Player player) {
			return player.getDetails().isLegendaryDonator();
		}
	},

	EXTREME_DONATOR(7) {
		@Override
		public boolean hasPrivileges(Player player) {
			return LEGENDARY_DONATOR.hasPrivileges(player) || player.getDetails().isExtremeDonator();
		}
	},

	SUPER_DONATOR(12) {
		@Override
		public boolean hasPrivileges(Player player) {
			return LEGENDARY_DONATOR.hasPrivileges(player) || EXTREME_DONATOR.hasPrivileges(player) || player.getDetails().isSuperDonator();
		}
	},

	DONATOR(8) {
		@Override
		public boolean hasPrivileges(Player player) {
			return LEGENDARY_DONATOR.hasPrivileges(player) || EXTREME_DONATOR.hasPrivileges(player) || SUPER_DONATOR.hasPrivileges(player) || player.getDetails().isDonator();
		}
	},

	YOUTUBER(15) {
		@Override
		public boolean hasPrivileges(Player player) {
			return player.getDetails().hasGroupRights(YOUTUBER);
		}
	},

	IRONMAN(14) {
		@Override
		public boolean hasPrivileges(Player player) {
			return player.getDetails().isIronman();
		}
	},

	PLAYER(3) {
		@Override
		public boolean hasPrivileges(Player player) {
			return true;
		}
	},

	BANNED(5) {
		@Override
		public boolean hasPrivileges(Player player) {
			return false;
		}
	};

	/**
	 * This method will get a {@code Rights} {@link Object} based on the
	 * {@link #memberGroupId}
	 * 
	 * @param memberGroupId
	 *            The member group to search for
	 * @return
	 */
	public static final Rights getRights(int memberGroupId) {
		for (Rights right : Rights.values()) {
			if (right.getMemberGroupId() == memberGroupId) {
				return right;
			}
		}
		return null;
	}

	Rights(int memberGroupId) {
		this.memberGroupId = memberGroupId;
	}

	/**
	 * The member group id of this rights group. This is set on the forums per
	 * group.
	 */
	private final int memberGroupId;

	/**
	 * In order for rights to be meaningful, only certain players can have
	 * access to their privileges. This method manages what players must have in
	 * order to access those privileges
	 * 
	 * @param player
	 * @return
	 */
	public abstract boolean hasPrivileges(Player player);

	/**
	 * @return the memberGroupId
	 */
	public int getMemberGroupId() {
		return memberGroupId;
	}
}
