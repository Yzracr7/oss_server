package com.hyperion.game.world.entity.player;

import com.hyperion.game.Constants;
import com.hyperion.game.content.minigames.clanwars.ClanWarsData;
import com.hyperion.game.content.minigames.clanwars.session.WarSession;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.combat.actions.SpecialAttacks;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.masks.animation.AnimationQueue.AnimationPriority;

/**
 * Contains client-side settings.
 * @author Graham Edgecombe
 *
 */
public class Settings {
	
	private Player player;
	
	/**
	 * Withdraw as notes flag.
	 */
	private boolean withdrawAsNotes = false;
	
	/**
	 * Swapping flag.
	 */
	private boolean swapping = true;
	
	private int magicType;

	private double prayerPoints;
	private boolean chat, split, mouse, aid, achievementDiaryTab, autoRetaliate;
	private boolean usingSpecial;
	private boolean usingAntifire;
	private boolean usingSuperAntifire;
	private int specialAmount, skullCycles;
	private AttackVars attackVars;
	
	public Settings(Player player) {
		this.player = player;
		this.setDefaultSettings();
	}
	
	public Settings() {}
	
	private void setDefaultSettings() {
		magicType = 1;
		achievementDiaryTab = false;
		chat = true;
		split = false;
		mouse = true;
		aid = false;
		attackVars = new AttackVars();
		specialAmount = 100;
		prayerPoints = 1;
		skullCycles = 0;
		autoRetaliate = true;
	}
	
	public void refresh() {
		player.getPacketSender().sendInterfaceConfig(261, 51, player.getWalkingQueue().isRunningToggled());
		player.getPacketSender().sendInterfaceConfig(261, 52, !player.getWalkingQueue().isRunningToggled());
		player.getPacketSender().sendConfig(171, !chat ? 1 : 0);
		player.getPacketSender().sendConfig(287, split ? 1 : 0);
		player.getPacketSender().sendInterfaceConfig(261, 55, split);
		player.getPacketSender().sendInterfaceConfig(261, 56, !split);
		if (split) {
			player.getPacketSender().sendBlankClientScript(83, "s");
		}
		player.getPacketSender().sendConfig(170, !mouse ? 1 : 0);
		player.getPacketSender().sendConfig(427, aid ? 1 : 0);
		player.getPacketSender().sendInterfaceConfig(261, 59, aid);
		player.getPacketSender().sendInterfaceConfig(261, 60, !aid);
		player.getPacketSender().sendConfig(172, !autoRetaliate ? 1 : 0);
		if (magicType != 1) {
			player.getPacketSender().sendTab(92, magicType == 2 ? 193 : 430);
		}
		player.getBonuses().refresh();
		setSkullCycles(skullCycles); // This method updates the appearance, so have this last.
	}
	
	public void sendTogglables(boolean sendTab) {
		if (sendTab) {
			player.getPacketSender().sendTab(97, 584);
		}
		if(!sendTab)
			player.getPacketSender().sendMessage(player.getDetails().isOrbsOn() ? ":orbson:" : ":orbsoff:");
		player.getPacketSender().sendInterfaceConfig(584, 22, player.getDetails().isOrbsOn() ? true : false);
		
		if(!sendTab)
		player.getPacketSender().sendMessage(player.getDetails().isNewHpbar() ? ":newhealthbars:" : ":oldhealthbars:");
		player.getPacketSender().sendInterfaceConfig(584, 24, player.getDetails().isNewHpbar() ? true : false);
		
		if(!sendTab)
		player.getPacketSender().sendMessage(player.getDetails().isNewCursors() ? ":newcursorson:" : ":newcursorsoff:");
		player.getPacketSender().sendInterfaceConfig(584, 26, player.getDetails().isNewCursors() ? true : false);
		
		if(!sendTab)
		player.getPacketSender().sendMessage(player.getDetails().isNewMenus() ? ":newmenu:" : ":oldmenu:");
		player.getPacketSender().sendInterfaceConfig(584, 28,  player.getDetails().isNewMenus() ? true : false);
		
		if(!sendTab)
		player.getPacketSender().sendMessage(player.getDetails().isNewHits() ? ":newhits:" : ":oldhits:");
		player.getPacketSender().sendInterfaceConfig(584, 30, player.getDetails().isNewHits() ? true : false);

		if(!sendTab)
			player.getPacketSender().sendMessage(player.getDetails().isLeftclickAttack() ? ":lcaon:" : ":lcaoff:");
		player.getPacketSender().sendInterfaceConfig(584, 32, player.getDetails().isLeftclickAttack() ? true : false);
		
		if(!sendTab)
			player.getPacketSender().sendMessage(player.getDetails().isTweeningOn() ? ":tweeningon:" : ":tweeningoff:");
		player.getPacketSender().sendInterfaceConfig(584, 34, player.getDetails().isTweeningOn() ? true : false);
		
		if(!sendTab)
			player.getPacketSender().sendMessage(player.getDetails().isCensorOn() ? ":censoron:" : ":censoroff:");
		player.getPacketSender().sendInterfaceConfig(584, 36, player.getDetails().isCensorOn() ? true : false);
			
		
		if(!sendTab && player.getAttributes().isSet("refreshFrame")) {
			player.getPacketSender().sendMessage(player.getDetails().getGameframe() == 562 ? ":2frame:" : player.getDetails().getGameframe() == 525 ? ":1frame:" : ":0frame:");
			player.getAttributes().remove("refreshFrame");
		}
		player.getPacketSender().modifyText("Gameframe: <col=6DC736>" + player.getDetails().getGameframe(), 584, 47);
		
		if(!sendTab)
		player.getPacketSender().sendMessage(player.getDetails().getHitmarks() == 634 ? ":634markers:" : player.getDetails().getHitmarks() == 554 ? ":554markers:" : ":oldmarkers:");
		player.getPacketSender().modifyText(" Hitmarks: <col=6DC736>" + player.getDetails().getHitmarks(), 584, 49);
		
		if(!sendTab)
		player.getPacketSender().sendMessage(player.getDetails().isHdOn() ? ":hdon:" : ":hdoff:");
		player.getPacketSender().modifyText("         HD: " + (player.getDetails().isHdOn() == true ? "<col=6DC736>On" : "<col=C73636>Off"), 584, 51);
		
		
		if(!sendTab && player.getAttributes().isSet("refreshFixed")) {
			//if(player.getDetails().getClientSize() > 0) {
			//	player.getPacketSender().sendGameframe(player.getDetails().getGameframe(), true);
			//	player.getPacketSender().sendMessage(":fson2:");
			//} else {
				player.getPacketSender().sendMessage(":fsoff:");
				//this.setGameframe(false);
				player.getPacketSender().sendGameframe(player.getDetails().getGameframe(), false);
				player.getPacketSender().sendGameframe(player.getDetails().getGameframe(), false);
				player.getPacketSender().sendWindowPane(549);
				player.getAttributes().remove("closed_screen");
		//	}
			player.getAttributes().remove("refreshFixed");
		}
		
		if(!sendTab && player.getAttributes().isSet("refreshResizable")) {
			if(player.getDetails().getClientSize() > 0) {
				player.getPacketSender().sendGameframe(player.getDetails().getGameframe(), true);
				player.getPacketSender().sendMessage(":fson:");
			}/* else {
				player.getPacketSender().sendMessage(":fsoff:");
				//this.setGameframe(false);
				player.getPacketSender().sendGameframe(player.getDetails().getGameframe(), false);
				player.getPacketSender().sendGameframe(player.getDetails().getGameframe(), false);
				player.getPacketSender().sendWindowPane(549);
				player.getAttributes().remove("closed_screen");
			}*/
			player.getAttributes().remove("refreshResizable");
		}
		
		
		
		if(!sendTab && player.getAttributes().isSet("refreshFullscreen")) {
				if(player.getDetails().getClientSize() > 0) {
					player.getPacketSender().sendGameframe(player.getDetails().getGameframe(), true);
					player.getPacketSender().sendMessage(":fson2:");
				}
				player.getAttributes().remove("refreshFullscreen");
			}
	}
	
	public void handleTogglableSettings(int button) {
		if (button == 20) {
			player.getPacketSender().sendTab(97, 261);
			return;
		}
		switch (button) {
		case 2:
		case 22:
		case 23:
			player.getDetails().setOrbs(player.getDetails().isOrbsOn() ? false : true);
			break;
		case 3://Hp bars
		case 24:
		case 25:
			player.getDetails().setHpbar(player.getDetails().isNewHpbar() ? false : true);
			break;
		case 4: //New cursors
		case 26:
		case 27:
			player.getDetails().setCursors(player.getDetails().isNewCursors() ? false : true);
			break;

		case 5://Menus
		case 28:
		case 29:
			player.getDetails().setMenus(player.getDetails().isNewMenus() ? false : true);
			break;
		case 6://x10 dmg
		case 30:
		case 31:
			player.getDetails().setNewHits(player.getDetails().isNewHits() ? false : true);
			break;
		case 7://Left-click attack
		case 32:
		case 33:
			player.getDetails().setLeftclickAttack(player.getDetails().isLeftclickAttack() ? false : true);
			break;
		case 8: //Tweening
		case 34:
		case 35:
			player.getDetails().setTweening(player.getDetails().isTweeningOn() ? false : true);
			break;
		case 9: //Middle mouse movement
		case 36:
		case 37:
			player.getDetails().setCensor(true);
			break;
		case 46: //gameframe
			player.getAttributes().set("refreshFrame", true);
			this.setGameframe(false);
			break;
		case 48: //hitmarks
			this.setHitmarks(false);
			break;
		case 50: //HD
			//Send red sprite for this.
			player.getDetails().setHd(player.getDetails().isHdOn() ? false : true);
			if(player.getDetails().getClientSize() == 0) {
				player.getPacketSender().sendWindowPane(549);
				player.getAttributes().remove("closed_screen");
			}
			break;
			
		case 54:
		case 57://fixed
			if(player.getDetails().getClientSize() > 0) {
				player.getDetails().setClientSize(0);	
				player.getAttributes().set("refreshFixed", true);
			}
			//player.getPacketSender().sendMessage("This is currently disabled. Please restart your client.");
			break;
		case 55:
		case 59://resize
			if(player.getDetails().getClientSize() != 1) {
				player.getDetails().setClientSize(1);	
				player.getAttributes().set("refreshResizable", true);
			}

			break;
		case 56:
		case 61:
			player.getPacketSender().sendMessage("Fullscreen is temporarily disabled as we work to make it bug-free.");
			player.getPacketSender().sendMessage("You may use the Resizable screen mode for the time being.");
				//if(player.getDetails().getClientSize() != 2) {
				//	player.getDetails().setClientSize(2);	
				//	player.getAttributes().set("refreshFullscreen", true);
				//	player.getPacketSender().sendMessage("Fullscreen is currently in beta mode. Please report any bugs you find.");
				//}
				
			break;
		//case 6://fonts
		//case 31:
			//this.setNewFonts(!newFonts, false);
		//	break;
		
		}
		sendTogglables(false);
	}
	
	/**
	 * Sets the withdraw as notes flag.
	 * @param withdrawAsNotes The flag.
	 */
	public void setWithdrawAsNotes(boolean withdrawAsNotes) {
		this.withdrawAsNotes = withdrawAsNotes;
	}
	
	/**
	 * Sets the swapping flag.
	 * @param swapping The swapping flag.
	 */
	public void setSwapping(boolean swapping) {
		this.swapping = swapping;
	}
	
	/**
	 * Checks if the player is withdrawing as notes.
	 * @return The withdrawing as notes flag.
	 */
	public boolean isWithdrawingAsNotes() {
		return withdrawAsNotes;
	}
	
	/**
	 * Checks if the player is swapping.
	 * @return The swapping flag.
	 */
	public boolean isSwapping() {
		return swapping;
	}

	public int getMagicType() {
		return magicType;
	}

	public void setMagicType(int magicType) {
		this.magicType = magicType;
	}
	
	public boolean isAchievementDiaryTab() {
		return achievementDiaryTab;
	}
	
	public void setAchievementDiaryTab(boolean b) {
		this.achievementDiaryTab = b;
	}
	
	public boolean isMouseTwoButtons() {
		return mouse;
	}
	
	public boolean isChatEffectsEnabled() {
		return chat;
	}
	
	public boolean isPrivateChatSplit() {
		return split;
	}
	
	public boolean isAcceptAidEnabled() {
		return aid;
	}
	
	public void setMouseTwoButtons(boolean mouse) {
		this.mouse = mouse;
	}
	
	public void setChatEffectsEnabled(boolean chat) {
		this.chat = chat;
	}
	
	public void setPrivateChatSplit(boolean split, boolean send) {
		this.split = split;
		if (split && send) {
			player.getPacketSender().sendClientScript(83, new Object[0], "");
		}
	}
	
	public void setAcceptAidEnabled(boolean aid) {
		this.aid = aid;
	}
	
	public void setAutoRetaliate(boolean autoRetaliate) {
		this.autoRetaliate = autoRetaliate;
	}
	
	public void toggleAutoRetaliate() {
		this.autoRetaliate = !autoRetaliate;
		player.getPacketSender().sendConfig(172, !autoRetaliate ? 1 : 0);
	}

	public boolean isAutoRetaliate() {
		return autoRetaliate;
	}
	
	public AttackVars getAttackVars() {
		return attackVars;
	}

	public boolean isUsingSpecial() {
		return usingSpecial;
	}

	public void setUsingSpecial(boolean usingSpecial) {
		this.usingSpecial = usingSpecial;
	}

	public int getSpecialAmount() {
		return specialAmount;
	}

	public void setSpecialAmount(int specialAmount, boolean refresh) {
		this.specialAmount = specialAmount;
		if (refresh)
			refreshBar();
	}
	
	public void deductSpecialAmount(int amount) {
		this.specialAmount -= amount;
		refreshBar();
	}
	
	public void increaseSpecialAmount(int amount) {
		this.specialAmount += amount;
		refreshBar();
	}
	
	public void resetSpecial() {
		specialAmount = 100;
		usingSpecial = false;
		refreshBar();
	}

	private final int[][] SPECIAL_TEXT_IDS = new int[][] { 
			{ 93, 22 },
			{ 92, 22 },
			{ 91, 22 },
			{ 90, 99 },
			{ 89, 24 },
			{ 88, 24 },
			{ 87, 24 },
			{ 85, 22 },
			{ 84, 22 },
			{ 83, 24 },
			{ 82, 24 },
			{ 81, 24 },
			{ 79, 22 },
			{ 78, 24 },
			{ 77, 22 },
			{ 76, 22 },
			{ 75, 24 }
			};
	
	public void refreshBar() {
		player.getPacketSender().sendConfig(300, specialAmount * 10);
		player.getPacketSender().sendConfig(2441, player.getSettings().usingSpecial ? 1 : 0);
		
		if(player.getAttributes().isSet("attackInterface")) {
			int interfaceId = player.getAttributes().getInt("attackInterface");
			int childId = -1;
			for (int i = 0; i < SPECIAL_TEXT_IDS.length; i ++) {
					if(SPECIAL_TEXT_IDS[i][0] == interfaceId) {
						childId = SPECIAL_TEXT_IDS[i][1];
						break;
					}
				}
			if(interfaceId != -1 && childId != -1) {
				player.getPacketSender().modifyText((usingSpecial? "<col=ffff00>" : "") + "  Special Attack: " + specialAmount + "%", interfaceId, childId);
				if(Constants.DEBUG_MODE)
					player.getPacketSender().sendMessage("InterfaceId: " + interfaceId + ", config: " + childId);
			}
		}
	}
	
	public void toggleSpecBar() {
		int wep = player.getEquipment().getItemInSlot(3);
		int currentPower = player.getSettings().getSpecialAmount();
		int neededPower = SpecialAttacks.getRequiredAmount(wep);
		if (neededPower > currentPower) {
			return;
		}
		if (wep == 4153) {
			Entity victim = player.getCombatState().getLastTarget();
			if (victim == null) {
				player.getPacketSender().sendMessage("Warning: Since the maul's special is an instant attack, it will be wasted when used");
				player.getPacketSender().sendMessage("on a first strike.");
				//return;
			}
			player.setInteractingEntity(victim);
			usingSpecial = true;
			player.getCombatState().setIgnoreCycles(true);
			player.getCombatState().startAttack(victim);
			MainCombat.executeCombatCycles(player, victim);
			usingSpecial = false;
			player.getCombatState().setIgnoreCycles(false);
			return;
		} else if(wep == 18600 || wep == 15486) { 
			if(player.getAttributes().isSet("clansession")) {
				WarSession session = player.getAttributes().get("clansession");
				if(session != null && session.getRules()[ClanWarsData.Groups.SPECIALS.ordinal()] == ClanWarsData.Rules.NO_STAFF_OF_THE_DEAD) {
					player.getPacketSender().sendMessage("Staff of the Dead and Staff of Light specials have been disabled in this war.");
					return;
				}
			}
			player.getPacketSender().sendMessage("You are under protection from all melee damage for 60 seconds.");
			player.animate(Animation.create(10518, AnimationPriority.HIGH));
			player.playGraphic(Graphic.create(2320)); //2321
			player.getAttributes().set("halveMelee", 100); //100 ticks - 60 seconds
			deductSpecialAmount(100);
			usingSpecial = false;
			refreshBar();
			return;
		}
		usingSpecial = usingSpecial ? false : true;
		refreshBar();
	}
	
	public double getPrayerPoints() {
		return prayerPoints;
	}
	
	public void setPrayerPoints(double p) {
		this.prayerPoints = p;
	}
	
	public void decreasePrayerPoints(double modification) {
		int lvlBefore = (int) Math.ceil(prayerPoints);
		if(prayerPoints > 0) {
			prayerPoints = (prayerPoints - modification <= 0 ? 0 : prayerPoints - modification);
		}
		int lvlAfter = (int) Math.ceil(prayerPoints);
		if (lvlBefore - lvlAfter >= 1) {
			player.getSkills().setLevel(5, lvlAfter);
		}
	}
	
	public int getSkullCycles() {
		return skullCycles;
	}
	
	public void renewSkull() {
		setSkullCycles(20);
	}
	
	public boolean isSkulled() {
		return skullCycles > 0;
	}
	
	public void setSkullCycles(int amt) {
		this.skullCycles = amt;
		player.getPrayers().setPkIcon(isSkulled() ? 0 : -1);
	}


	public void setGameframe(boolean loading) {
		int currentFrame = player.getDetails().getGameframe();
		if (loading) {
			return;
		}
		//if(player.getDetails().isFullscreen()) {
		//	player.getPacketSender().sendMessage("You cannot change your gameframe whilst in fullscreen/resizable.");
		//	return;
		//}
		player.getPacketSender().changeGameFrame(currentFrame, currentFrame == 474 ? 525 : currentFrame == 525 ? 562 : 474);
		player.getDetails().setGameframe(currentFrame == 474 ? 525 : currentFrame == 525 ? 562 : 474);
		player.getPacketSender().sendMessage(currentFrame == 474 ? ":1frame:" : currentFrame == 525 ? ":2frame:" : ":0frame:");
	}
	
	public void setHitmarks(boolean loading) {
		int currentMarks = player.getDetails().getHitmarks();
		if (loading) {
			return;
		}
		player.getDetails().setHitmarks(currentMarks == 474 ? 554 : currentMarks == 554 ? 634 : 474);
		player.getPacketSender().sendMessage(currentMarks == 474 ? ":554markers:" : currentMarks == 554 ? ":634markers:" : ":oldmarkers:");
	}

	public boolean isUsingAntifire() {
		return usingAntifire;
	}

	public void setUsingAntifire(boolean usingAntifire) {
		this.usingAntifire = usingAntifire;
	}
	public boolean isUsingSuperAntifire() {
		return usingSuperAntifire;
	}

	public void setUsingSuperAntifire(boolean usingSuperAntifire) {
		this.usingSuperAntifire = usingSuperAntifire;
	}

	public void setPrayerType(byte b) {
		// TODO Auto-generated method stub
		
	}

	public byte getPrayerType() {
		// TODO Auto-generated method stub
		return 0;
	}
}
