package com.hyperion.game.world.entity.player;

import java.util.HashMap;
import java.util.Map;

import com.hyperion.game.Constants;
import com.hyperion.game.content.LevelUp;
import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.TeleportAreaLocations;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.player.container.impl.Equipment.EQUIPMENT;

/**
 * Represents a player's skill and experience levels.
 * 
 * @author Graham Edgecombe
 *
 */
public class Skills {

	public static final int SKILL_COUNT = 23;

	/**
	 * The skill names.
	 */
	public static final String[] SKILL_NAME = { "Attack", "Defence", "Strength", "Hitpoints", "Ranged", "Prayer", "Magic", "Cooking", "Woodcutting", "Fletching", "Fishing", "Firemaking", "Crafting", "Smithing", "Mining", "Herblore", "Agility", "Thieving", "Slayer", "Farming", "Runecrafting", "Hunter", "Construction"};

	/**
	 * Constants for the skill numbers.
	 */
	public static final int ATTACK = 0, DEFENCE = 1, STRENGTH = 2, HITPOINTS = 3, RANGE = 4, PRAYER = 5, MAGIC = 6, COOKING = 7, WOODCUTTING = 8, FLETCHING = 9, FISHING = 10, FIREMAKING = 11, CRAFTING = 12, SMITHING = 13, MINING = 14, HERBLORE = 15, AGILITY = 16, THIEVING = 17, SLAYER = 18, FARMING = 19, RUNECRAFTING = 20, HUNTER = 21, CONSTRUCTION = 22;

	/**
	 * The skill statistic enum.
	 * 
	 * @author Michael
	 */
	public enum Skill {

		ATTACK(Skills.ATTACK),

		DEFENCE(Skills.DEFENCE),

		HITPOINTS(Skills.HITPOINTS),

		RANGE(Skills.RANGE),

		MAGIC(Skills.MAGIC),

		PRAYER(Skills.PRAYER),

		STRENGTH(Skills.STRENGTH),

		COOKING(Skills.COOKING),

		WOODCUTTING(Skills.WOODCUTTING),

		FLETCHING(Skills.FLETCHING),

		FISHING(Skills.FISHING),

		FIREMAKING(Skills.FIREMAKING),

		CRAFTING(Skills.CRAFTING),

		SMITHING(Skills.SMITHING),

		MINING(Skills.MINING),

		HERBLORE(Skills.HERBLORE),

		AGILITY(Skills.AGILITY),

		THIEVING(Skills.THIEVING),

		SLAYER(Skills.SLAYER),

		FARMING(Skills.FARMING),

		RUNECRAFTING(Skills.RUNECRAFTING),

		HUNTER(Skills.HUNTER),

		CONSTRUCTION(Skills.CONSTRUCTION),

		;

		/**
		 * The list of skills.
		 */
		private static Map<Integer, Skill> skills = new HashMap<Integer, Skill>();

		public static Skill skillForId(int skill) {
			return skills.get(skill);
		}

		/**
		 * Populates the skill list.
		 */
		static {
			for (Skill skill : Skill.values()) {
				skills.put(skill.getId(), skill);
			}
		}

		/**
		 * The id of the skill.
		 */
		private int id;

		private Skill(int id) {
			this.id = id;
		}

		/**
		 * @return the id
		 */
		public int getId() {
			return id;
		}
	}

	private transient Player player;

	private static final double MAXIMUM_EXP = 200000000;

	private int level[] = new int[SKILL_COUNT];
	private double xp[] = new double[SKILL_COUNT];
	private transient int tempHealthLevel;

	public Skills() {
	}

	public Skills(Player player) {
		this.player = player;
		for (int i = 0; i < SKILL_COUNT; i++) {
			level[i] = 1;
			xp[i] = 0;
		}
		level[3] = 10;
		xp[3] = 1184;
	}

	public Object readResolve() {
		tempHealthLevel = level[3];
		return this;
	}

	public int getCombatLevel() {
		int attack = getLevelForXp(0);
		int defence = getLevelForXp(1);
		int strength = getLevelForXp(2);
		int hp = getLevelForXp(3);
		int prayer = getLevelForXp(5);
		int ranged = getLevelForXp(4);
		int magic = getLevelForXp(6);
		double combatLevel = (defence + hp + Math.floor(prayer / 2)) * 0.25;
		double warrior = (attack + strength) * 0.325;
		double ranger = ranged * 0.4875;
		double mage = magic * 0.4875;
		return (int) (combatLevel + Math.max(warrior, Math.max(ranger, mage)));
	}

	public int getLevel(int skill) {
		return level[skill];
	}

	public double getXp(int skill) {
		return xp[skill];
	}

	public void setSkillsToNormal() {
		for (int i = 0; i < 7; i++) {
			if (i == 5) {
				player.getSettings().setPrayerPoints((int) getLevelForXp(5));
			}
			level[i] = getLevelForXp(i);
			player.getPacketSender().sendSkillLevel(i);
		}
	}

	/**
	 * Increases a level to the experience for the level.
	 * 
	 * @param skill
	 *            The skill to increase.
	 * @param increment
	 *            The increment amount.
	 */
	public void increaseLevelToMaximum(int skill, int increment) {
		/*
		 * if (player.getCombatState().isDead()) { return; }
		 */
		if (level[skill] > getLevelForXp(skill)) {
			return;
		}
		level[skill] += increment;
		if (level[skill] > getLevelForXp(skill)) {
			level[skill] = getLevelForXp(skill);
		}
		player.getPacketSender().sendSkillLevel(skill);
	}

	/**
	 * Lowers a level
	 * 
	 * @param skill
	 * @param amount
	 */
	public void deductLevel(int skill, int amount) {
		level[skill] = level[skill] - amount;
		if (skill == 5) {
			player.getSettings().setPrayerPoints((int) level[skill] - amount);
		}
		if (level[skill] < 0) {
			level[skill] = 0;
			if (skill == 5) {
				player.getSettings().setPrayerPoints((int) 0);
			}
		}
		player.getPacketSender().sendSkillLevel(skill);
	}

	/**
	 * Decrements a level.
	 * 
	 * @param skill
	 *            The skill to decrement.
	 */
	public void decrementLevel(int skill) {
		level[skill]--;
		if (player.getPacketSender() != null) {
			player.getPacketSender().sendSkillLevel(skill);
		}
	}
	
	public void incrementLevel(int skill) {
		level[skill]++;
		if (player.getPacketSender() != null) {
			player.getPacketSender().sendSkillLevel(skill);
		}
	}

	public void setLevel(int skill, int lvl) {
		level[skill] = lvl;
		if (level[skill] <= 1 && skill != 3 && skill != 5) {
			level[skill] = 1;
		}
		if (skill == 5) {
			player.getSettings().setPrayerPoints((int) lvl);
			if (!player.getCombatState().isDead() && (level[5] <= 0 || lvl == 0)) {
				level[5] = 0;
				player.getPrayers().deactivateAllPrayers();
				player.getPacketSender().sendMessage("You have run out of Prayer points, please recharge your prayer at an altar.");
			}
		} else if (skill == 3) {
			tempHealthLevel = lvl;
		}
		player.getPacketSender().sendSkillLevel(skill);
	}

	public void setXp(int skill, int newXp) {
		xp[skill] = newXp;
	}

	public int getLevelForXp(int skill) {
		double exp = player.getSkills().getXp(skill);
		int points = 0;
		int output = 0;
		for (int lvl = 1; lvl < 100; lvl++) {
			points += Math.floor((double) lvl + 300.0 * Math.pow(2.0, (double) lvl / 7.0));
			output = (int) Math.floor(points / 4);
			if ((output - 1) >= exp) {
				return lvl;
			}
		}
		return 99;
	}

	public int getXpForLevel(int level) {
		int points = 0;
		int output = 0;
		for (int lvl = 1; lvl <= level; lvl++) {
			points += Math.floor(lvl + 300.0 * Math.pow(2.0, lvl / 7.0));
			if (lvl >= level) {
				return output;
			}
			output = (int) Math.floor(points / 4);
		}
		return 0;
	}

	public void addExperience(final int skill, double exp) {
		if (player.getVariables().isExpLocked()) {
			if (skill >= 0 && skill <= 6) {
				return;
			}
		}
		if (skill > 6) {
			exp = exp * (player.getDetails().isIronman() ? Constants.IRONMAN_NONCOMBAT_EXP_MULTIPLER : Constants.NON_COMBAT_EXP_MULTIPLIER) * (player.getVariables().getExtraExpTimer() > 0 ? 2 : 1);
			if(skill == Skills.AGILITY) {
				int gloves = player.getEquipment().getSlotById(EQUIPMENT.GLOVES.ordinal());
				switch(gloves) {
				case 11736:
				case 11738:
				case 11740:
					exp = exp * (gloves == 11736 ? 1.1 : gloves == 11738 ? 1.15 : 1.20);
					break;
				}
			} else if (skill == Skills.WOODCUTTING) {
				int helmet = player.getEquipment().getSlotById(EQUIPMENT.HELMET.ordinal());
				switch(helmet) {
				case 14631:
				case 14662:
				case 14663:
					exp = exp * (helmet == 14631 ? 1.1 : helmet == 14662 ? 1.15 : 1.20);
					break;
				}
			}
		} else if (skill == Skills.PRAYER) {
			exp = exp * (player.getDetails().isIronman() ? Constants.IRONMAN_COMBAT_EXP_MULTIPLIER : (Constants.COMBAT_EXP_MULTIPLIER / 2));
		} else {
			boolean useMultiplier = true;
			if(TeleportAreaLocations.isInPVP(player) && player.getCombatState().getTarget() != null
					&& player.getCombatState().getTarget().isPlayer()) {
				useMultiplier = false;
			}
			exp = 
					exp * (useMultiplier ? 
							(player.getDetails().isIronman() ? Constants.IRONMAN_COMBAT_EXP_MULTIPLIER : Constants.COMBAT_EXP_MULTIPLIER * 1) * (player.getVariables().getExtraExpTimer() > 0 ? 2 : 1) : 1);
		}
		int oldLevel = getLevelForXp(skill);
		int oldBoostedLevel = getLevel(skill) > oldLevel ? getLevel(skill) : -1;
		double oldXp = xp[skill];
		
		xp[skill] += exp;
		
		if (xp[skill] >= MAXIMUM_EXP) {
			if(oldXp < MAXIMUM_EXP) 
				player.getPacketSender().sendNewsMessage(player.getName().substring(0, 1).toUpperCase() + player.getName().substring(1) +" has reached 200M experience in " + Skills.SKILL_NAME[skill] + "!");
			xp[skill] = MAXIMUM_EXP;
		} else if (skill > 6 && xp[skill] > 30_000_000) {
			Achievements.increase(player, 28);
		}
		refresh_total_exp(exp);
		// TODO: level up graphic for 99 = 1690
		int newLevel = getLevelForXp(skill);
		if (oldLevel < 99 && newLevel == 99) {
			player.playGraphic(1690);
		}
		if (newLevel > oldLevel && newLevel <= 99) {
			if (skill != 3) {
				level[skill] = newLevel;
			} else {
				level[3]++;
				tempHealthLevel = level[3];
			}
			if (skill == 5) {
				final int currentLevel = (int) player.getSettings().getPrayerPoints();
				setLevel(5, currentLevel + 1);
			}
			if (player.getAttributes().isSet("stopactions")) {
				World.getWorld().submit(new Tickable(1) {
					@Override
					public void execute() {
						if (player.isDestroyed()) {
							this.stop();
							return;
						}
						if (!player.getAttributes().isSet("stopactions")) {
							this.stop();
							LevelUp.send(player, skill);
							Achievements.checkTotalLevel(player);
							player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
						}
					}
				});
			} else {
				LevelUp.send(player, skill);
				Achievements.checkTotalLevel(player);
				player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
			}
			
			if(skill != 3 && skill != 5) {
				if(oldBoostedLevel > getLevel(skill))
					setLevel(skill, oldBoostedLevel);
			}
		}
		player.getPacketSender().sendSkillLevel(skill);
	}

	public boolean hasMultiple99s() {
		int j = 0;
		for (int i = 0; i < SKILL_COUNT; i++) {
			if (getLevelForXp(i) >= 99) {
				j++;
			}
		}
		return j > 1;
	}

	public int getTotalXp() {
		int total = 0;
		for (int i = 0; i < SKILL_COUNT; i++) {
			total += xp[i];
		}
		return total;
	}

	public int getTotalLevel() {
		int total = 0;
		for (int i = 0; i < SKILL_COUNT; i++) {
			total += getLevelForXp(i);
		}
		return total;
	}

	public int getTempHealthLevel() {
		return tempHealthLevel;
	}

	public void setTempHealth(int i) {
		this.tempHealthLevel = i;
	}

	public void setSkill(int skill, int lvl, double exp) {
		level[skill] = lvl;
		xp[skill] = exp;
	}

	private void refresh_total_exp(double exp) {
		if (exp <= 0) {
			return;
		}
		// xp counter
		player.getPacketSender().sendConfig(553, (int) exp); // float
		player.getVariables().increaseTotalExp((int) exp);
		player.getPacketSender().sendConfig(554, player.getVariables().getTotalExp());
	}
}
