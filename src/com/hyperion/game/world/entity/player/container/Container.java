package com.hyperion.game.world.entity.player.container;

import com.hyperion.game.Constants;
import com.hyperion.game.item.Item;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.impl.Bank;
import com.hyperion.game.world.grounditems.GroundItem;
import com.hyperion.game.world.grounditems.GroundItemManager;
import com.hyperion.utility.game.ChatColors;
import com.hyperion.utility.logger.Log;

public class Container {

	private Player player;
	private short size;
	private Item[] items;
	private Type type;
	private ContainerInterface interfaceContainer;
	private ContainerName containerName;

	/*
	 * Bank stuff
	 */
	private boolean isInserting = false;

	public static enum Type {
		ALWAYS_STACKS, NEVER_STACKS, NORMAL
	}

	public static enum ContainerName {
		BANK, INVENTORY, TRADE, EQUIPMENT, DUEL, IKOD,
	}

	public Container(Player player, Type type, ContainerName name, int size, ContainerInterface containerInterface) {
		this.type = type;
		this.containerName = name;
		this.size = (short) size;
		this.player = player;
		this.interfaceContainer = containerInterface;
		this.items = new Item[size];
	}

	public Container() {
	}

	/**
	 * Refreshes all items in this container.
	 */
	public void refresh() {
		if (getInterfaceContainer() == null) {
			return;
		}
		if (getInterfaceContainer().getInterfaceId() == 12) {
			int tab = player.getAttributes().get("currentTab") != null ? player.getAttributes().getInt("currentTab") : 0;
			player.getPacketSender().sendClientScript(101, new Object[] { "" }, ""); //Close enterInstancedRoom amount inter
			if(tab== 0 && (player.getBank().getSize() - player.getBank().freeSlots()) > 0 && (player.getVariables().getTabStarts()[1] == 0 || player.getVariables().getTabStarts()[1] == -1)) {
				player.getVariables().getTabStarts()[0] = 0;
				if(player.getBank().getSize() != player.getBank().freeSlots()) {
					player.getVariables().getTabStarts()[1] = (player.getBank().getSize() - player.getBank().freeSlots()) - 1;
					player.getVariables().getTabStarts()[2] = (player.getBank().getSize() - player.getBank().freeSlots());
				} else {
					player.getVariables().getTabStarts()[1] = 0;
					player.getVariables().getTabStarts()[2] = 0;
				}
				player.getPacketSender().sendMessage("It seems like your first time using bank tabs. Simply drag an item to the plus tab to speak!");
			}
			
			player.getPacketSender().sendItems(12, 7, 2, Bank.getTabItems(player, tab));
		} else
			player.getPacketSender().sendItems(getInterfaceContainer().getInterfaceId(), getInterfaceContainer().getChildId(), getInterfaceContainer().getType(), items);
		//if(this.containerName == ContainerName.INVENTORY)
			//player.getBonuses().refreshWeight();
		//TODO WIEGHT
	}

	public void refresh(Item item, int slot) {
		if (getInterfaceContainer() == null) {
			return;
		}
		player.getPacketSender().sendItem(getInterfaceContainer().getInterfaceId(), getInterfaceContainer().getChildId(), getInterfaceContainer().getType(), slot, item);
	}

	public void clear() {
		items = new Item[items.length];
		refresh();
	}

	public boolean addItem(int item) {
		return addItem(item, 1, findFreeSlot());
	}

	public boolean addItem(int item, int amount) {
		return addItem(item, amount, findFreeSlot());
	}

	/**
	 * Adds a droppable item to the container. This is for inventory usage only.
	 * If the player has no room for the item, it is dropped at their feet.
	 * 
	 * @param item
	 *            The item
	 */
	public void addDroppable(Item item) {
		if (!addItem(item)) {
			GroundItem groundItem = new GroundItem(item, player);
			GroundItemManager.create(groundItem, player);
			player.getPacketSender().sendMessage("<col=" + ChatColors.MAROON + ">An item has been dropped at your feet.");
		}
		refresh();
	}
	
	public void addDroppable(int itemId, int amount) {
		addDroppable(new Item(itemId, amount));
	}

	// this is used for equiping/unequipping only
	public boolean addItem(int itemId, int amount, int slot) {
		boolean stackable = ItemDefinition.forId(itemId) == null ? false : ItemDefinition.forId(itemId).isStackable();
		if (amount <= 0) {
			return false;
		}
		//World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Added an item " + itemId + "," + amount + "," + slot + " in " + containerName.toString() + "."));
		if (!stackable) {
			if (freeSlots() <= 0) {
				player.getPacketSender().sendMessage("Not enough space to hold this.");
				return false;
			}
			if (items[slot] != null) {
				slot = findFreeSlot();
				if (slot == -1) {
					player.getPacketSender().sendMessage("Not enough space to hold this.");
					return false;
				}
			}
			this.setItem(itemId, 1, slot);
			player.getInventory().refresh();
			return true;
		} else if (stackable) {
			if (hasItem(itemId)) {
				slot = findItem(itemId);
			} else if (freeSlots() <= 0) {
				player.getPacketSender().sendMessage("Not enough space to hold this.");
				return false;
			}
			long newAmount = ((long) amount);
			if (items[slot] != null) {
				newAmount = ((long) amount + items[slot].getCount());
			}
			if (newAmount > Constants.MAX_ITEMS) {
				player.getPacketSender().sendMessage("Not enough space to hold this.");
				return false;
			}
			if (items[slot] != null && items[slot].getId() != itemId) {
				slot = findFreeSlot();
				if (slot == -1) {
					player.getPacketSender().sendMessage("Not enough space to hold this.");
					return false;
				}
			}
			int amountToSet = amount;
			if (items[slot] != null) {
				amountToSet += items[slot].getCount();
			}
			this.setItem(itemId, amountToSet, slot);
			player.getInventory().refresh();
			return true;
		}
		return false;
	}

	/*
	 * This add should be used for banking/inventory/trading/dueling
	 */
	public boolean addItem(Item item) {
		if(item.getCount() <= 0)
			return false;
		if(containerName == ContainerName.BANK)
			 World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Banked " + item.getCount()+ " x " + item.getDefinition().getName() + " (" + item.getId() + ")"));
		
		boolean stackable = item.getDefinition() == null ? false : item.getDefinition().isStackable();
		//boolean addCashToNewSlot = false;
		int availSlots = this.freeSlots();
		//World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Added an item " + item.getId() + "," + item.getCount() + " in " + containerName.toString() + "."));
		if (availSlots <= 0) {
			// TODO - diff message depending on container.
			boolean continueMethod = false;
			if (stackable || type.equals(Type.ALWAYS_STACKS)) {
				for (int i = 0; i < items.length; i++) {
					if (items[i] != null && items[i].getId() == item.getId()) {
						if (items[i].getCount() < Constants.MAX_ITEMS) {
							continueMethod = true;
						}
					}
				}
			} else {
				continueMethod = false;
			}
			if (!continueMethod) {
				player.getPacketSender().sendMessage("You don't have enough inventory space.");
				return false;
			}
		}
		if (stackable || type.equals(Type.ALWAYS_STACKS)) {
			for (int i = 0; i < items.length; i++) {
				if (items[i] != null && items[i].getId() == item.getId()) {
					final int itemCount = item.getCount();
					int amountToAdd = item.getCount();
					long totalCount = ((long) amountToAdd + items[i].getCount());
					/*
					 * if (item.getId() == 995) {//COINS ONLY if (totalCount >
					 * Constants.MAX_ITEMS) { amountToAdd = (Constants.MAX_ITEMS
					 * - items[i].getCount()); if (amountToAdd <= 0) { int
					 * newCoinsSlot = getCoinsSlotWithLessThanMax(-1, false); if
					 * (newCoinsSlot == -1) { addCashToNewSlot = true; break; }
					 * else { //left overs final long toCompare =
					 * (items[newCoinsSlot].getCount() + itemCount); final int
					 * leftOver = (int) (toCompare > Constants.MAX_ITEMS ?
					 * (toCompare - Constants.MAX_ITEMS) : Constants.MAX_ITEMS -
					 * toCompare); if (leftOver > 0 && toCompare >
					 * Constants.MAX_ITEMS) { int newSlotForLeftOvers =
					 * getCoinsSlotWithLessThanMax(newCoinsSlot, true); if
					 * (newSlotForLeftOvers == -1) { newSlotForLeftOvers =
					 * findFreeSlot(); if (newSlotForLeftOvers != -1) { set(new
					 * Item(995, leftOver), newSlotForLeftOvers, true); } } } if
					 * (toCompare > Constants.MAX_ITEMS) { final int amt =
					 * (Constants.MAX_ITEMS - items[newCoinsSlot].getCount());
					 * amountToAdd = (items[newCoinsSlot].getCount() + amt); }
					 * else { amountToAdd = item.getCount() +
					 * items[newCoinsSlot].getCount(); } set(new
					 * Item(items[i].getId(), amountToAdd), newCoinsSlot, true);
					 * return true; } } else { final int test = (int)
					 * (items[i].getCount() + itemCount); set(new
					 * Item(items[i].getId(), items[i].getCount() +
					 * amountToAdd), i, true); final int cashLeftOver = (int)
					 * (test > Constants.MAX_ITEMS ? test - Constants.MAX_ITEMS
					 * : Constants.MAX_ITEMS - test); if (cashLeftOver > 0) {
					 * int newSlotForLeftOvers =
					 * player.getInventory().getCoinsSlotWithLessThanMax(-1,
					 * false); if (newSlotForLeftOvers == -1) {
					 * newSlotForLeftOvers =
					 * player.getInventory().findFreeSlot(); if
					 * (newSlotForLeftOvers != -1) {
					 * player.getInventory().set(new Item(995, cashLeftOver),
					 * newSlotForLeftOvers, true); } } else {//we found coins
					 * that is lower then the max amount. Item invItem =
					 * player.getInventory().get(newSlotForLeftOvers); if
					 * (invItem != null) { int totalToAdd = (invItem.getCount()
					 * + cashLeftOver); if (totalToAdd > Constants.MAX_ITEMS) {
					 * final int leftOver = totalToAdd - Constants.MAX_ITEMS;
					 * totalToAdd = Constants.MAX_ITEMS; if (leftOver > 0) { int
					 * newSlot =
					 * player.getInventory().getCoinsSlotWithLessThanMax(-1,
					 * false); if (newSlot != -1) { Item inventoryItem =
					 * player.getInventory().get(newSlot); if (inventoryItem !=
					 * null) { player.getInventory().set(new Item(995,
					 * inventoryItem.getCount() + leftOver), newSlot, true); } }
					 * else { newSlot = player.getInventory().findFreeSlot(); if
					 * (newSlot != -1) { player.getInventory().set(new Item(995,
					 * leftOver), newSlot, true); } } } }
					 * player.getInventory().set(new Item(995, totalToAdd),
					 * newSlotForLeftOvers, true); } } } } return true; } else {
					 * if (totalCount > Constants.MAX_ITEMS || totalCount < 1) {
					 * return false; } set(new Item(items[i].getId(),
					 * items[i].getCount() + item.getCount()), i, true); }
					 */
					if (item.getId() == 99991) {

					} else {// if item is not coins, we add other items here.
						if (totalCount >= Constants.MAX_ITEMS || totalCount < 1) {
							return false;
						}
						set(new Item(items[i].getId(), items[i].getCount() + item.getCount()), i, false);
					}
					return true;
				}
			}
			/*
			 * if (addCashToNewSlot) { int slot = findFreeSlot(); if(slot == -1)
			 * { return false; } if (item.getCount() > Constants.MAX_ITEMS) {
			 * item.setItemAmount(Constants.MAX_ITEMS); } set(item, slot, true);
			 * return true; }
			 */
			int slot = findFreeSlot();
			if (slot == -1) {
				return false;
			} else {
				/*
				 * if (item.getId() == 995) { if (item.getCount() >
				 * Constants.MAX_ITEMS) {
				 * item.setItemAmount(Constants.MAX_ITEMS); } }
				 */
				set(item, slot, false);
				return true;
			}
		} else {
			int slots = freeSlots();
			if (slots >= item.getCount()) {
				boolean added = false;
				for (int i = 0; i < item.getCount(); i++) {
					set(new Item(item.getId(), 1), findFreeSlot(), false);
					added = true;
				}
				return added;
			} else {
				return false;
			}
		}
	}

	public boolean deleteItem(Item item, boolean actual) {
		return deleteItem(item, actual ? findActualItem(item.getId(), item.getCount()) : findItem(item.getId()));
	}

	public boolean deleteItem(Item item) {
		return deleteItem(item, findItem(item.getId()));
	}

	public boolean deleteItem(Item item, int containerSlot) {
		if (item == null || item.getDefinition() == null || containerSlot == -1) {
			return false;
		}
		if(containerName == ContainerName.BANK)
			World.getWorld().loggingThread.appendLog(new Log(player.getName(), "[Bank] Withdrew " + item.getCount()+ " x " + item.getDefinition().getName() + " (" + item.getId() + ")"));
		//World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Deleted an item " + item.getId() + "," + item.getCount() + " in " + containerName.toString() + "."));
		if (item.getDefinition().isStackable() || type.equals(Type.ALWAYS_STACKS)) {
			Item stack = getSlot(containerSlot);
			if (stack.getCount() > item.getCount()) {
				set(new Item(stack.getId(), stack.getCount() - item.getCount()), containerSlot, false);
				return true;
			} else {
				set(null, containerSlot, false);
				if (this.getContainerName().equals(ContainerName.BANK)) {
					this.arrange();
				}
				return true;
			}
		} else {
			set(null, containerSlot, false);
			if (this.getContainerName().equals(ContainerName.BANK)) {
				this.arrange();
			}
		}
		return true;
	}

	public boolean deleteItem(int item) {
		return deleteItem(item, 1, findItem(item));
	}

	public boolean deleteItem(int item, int amount) {
		return deleteItem(item, amount, findItem(item));
	}

	public boolean deleteItem(int itemId, int amount, int slot) {
		if (slot == -1) {
			return false;
		}
		if (slot < 0 || slot > size)
			return false;
		if (items[slot] == null) {
			return false;
		}
		//World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Deleted an item " + itemId + "," + amount + "," + slot + " in " + containerName.toString() + "."));
		if (items[slot].getId() == itemId && items[slot].getCount() >= amount) {
			items[slot].setItemAmount(items[slot].getCount() - amount);
			if (items[slot].getCount() <= 0) {
				items[slot] = null;
			}
			refresh();
			return true;
		}
		return false;
	}

	/**
	 * Shifts all items to the top left of the container leaving no gaps.
	 */
	public void shift() {
		Item[] old = items;
		items = new Item[size];
		int newIndex = 0;
		for (int i = 0; i < items.length; i++) {
			if (old[i] != null) {
				items[newIndex] = old[i];
				newIndex++;
			}
		}
		refresh();
	}

	/**
	 * Sets an item.
	 * 
	 * @param index
	 *            The position in the container.
	 * @param item
	 *            The item.
	 */
	public boolean set(Item item, int slot, boolean refresh) {
		if (slot < 0 || slot > size)
			return false;
		items[slot] = item;
		if (item != null) {
			items[slot].readResolve();
		} else {
			return false;
		}
		if (refresh) {
			refresh();
		}
		return true;
	}

	private void setItem(int item, int amount, int slot) {
		if (slot < 0 || slot > size)
			return;
		if (item != -1) {
			Item itemToSet = new Item(item, amount);
			items[slot] = itemToSet;
		} else {
			items[slot] = null;
		}
	}

	public boolean replaceItem(int itemToReplace, int itemToAdd) {
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null)
				continue;
			if (items[i].getId() == itemToReplace) {
				if (itemToAdd != -1) {
					items[i].setItemId(itemToAdd);
				} else {
					items[i] = null;
				}
				refresh(items[i], i);
				return true;
			}
		}
		return false;
	}

	public boolean replaceItemInSlot(int itemToReplace, int itemToReplaceSlot, int itemToAdd) {
		Item itemToFind = items[itemToReplaceSlot];
		if (itemToFind != null) {
			if (itemToAdd != -1) {
				itemToFind.setItemId(itemToAdd);
			} else {
				items[itemToReplaceSlot] = null;
			}
			refresh(itemToFind, itemToReplaceSlot);
			return true;
		}
		return false;
	}

	public boolean hasItem(int itemId) {
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null)
				continue;
			if (items[i].getId() == itemId) {
				return true;
			}
		}
		return false;
	}

	public int getCount(int itemId) {
		int count = 0;
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null)
				continue;
			if (items[i].getId() == itemId) {
				count++;
			}
		}
		return count;
	}

	public boolean containsName(String text) {
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null)
				continue;
			ItemDefinition def = items[i].getDefinition();
			if (def != null) {
				if (def.getName().toLowerCase().contains(text)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @param itemId
	 *            The id of the item to find
	 * @param amount
	 *            The amount of the item needed
	 * @return
	 */
	public boolean containsItem(int itemId, int amount) {
		for (int i = 0; i < items.length; i++) {
			Item item = items[i];
			if (item == null) {
				continue;
			}
			if (item.getId() == itemId && item.getCount() >= amount) {
				return true;
			}
		}
		return false;
	}

	public boolean hasItemAmount(int itemId, long amount) {
		int j = 0;
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null)
				continue;
			if (items[i].getId() == itemId) {
				j += items[i].getCount();
			}
		}
		return j >= amount;
	}

	/**
	 * Swaps two items.
	 * 
	 * @param fromSlot
	 *            From slot.
	 * @param toSlot
	 *            To slot.
	 */
	public void swap(int fromSlot, int toSlot) {
		Item temp = get(fromSlot);
		try {
			set(get(toSlot), fromSlot, false);
			set(temp, toSlot, false);
		} finally {
			refresh();
		}
	}

	/*
	 * Bank stuff speak
	 */

	public void insert(int fromId, int toId) {
		if(fromId < 0 || toId < 0) {
			System.out.println("FATAL: -1 on container insert, fromId for player " + player.getName()+ " is " + fromId + " and toId is " + toId);
			player.getPacketSender().sendMessage("Your tabs are bugged and will be reset...");
			Bank.fixTabs(player);
			return;
		}
		Item temp = items[fromId];
		if (toId > fromId) {
			for (int i = fromId; i < toId; i++) {
				set(getSlot(i + 1), i, false);
			}
		} else if (fromId > toId) {
			for (int i = fromId; i > toId; i--) {
				set(getSlot(i - 1), i, false);
			}
		}
		set(temp, toId, false);
		//Removed April 8 for flashing tabs fix
		//Bank.refreshBank(player);
	}

	public void arrange() {
		Item[] oldData = getItems();
		items = new Item[size];
		int ptr = 0;
		for (int i = 0; i < size; i++) {
			if (oldData[i] == null)
				continue;
			if (oldData[i].getId() != -1) {
				items[ptr++] = oldData[i];
			}
		}
		for (int i = ptr; i < size; i++) {
			items[i] = null;
		}
	}

	/**
	 * Gets a slot by id.
	 * 
	 * @param id
	 *            The id.
	 * @return The slot, or <code>-1</code> if it could not be found.
	 */
	public int getSlotById(int id) {
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null)
				continue;
			if (items[i].getId() == id) {
				return i;
			}
		}
		return -1;
	}

	public boolean hasEnoughRoomFor(int item) {
		if (freeSlots() > 0) {
			return true;
		}
		if (freeSlots() <= 0) {
			boolean stackable = ItemDefinition.forId(item).isStackable();
			if (stackable && hasItem(item)) {
				return true;
			}
		}
		return false;
	}

	public int freeSlots() {
		return size - getTotalItems();
	}

	public int findFreeSlot() {
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null || items[i].getId() == -1) {
				return i;
			}
		}
		return -1;
	}

	public int getCoinsSlotWithLessThanMax(int slotToSkip, boolean skip) {
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null)
				continue;
			if (skip) {
				if (i == slotToSkip)
					continue;
			}
			if (items[i].getId() == 995) {
				if (items[i].getCount() < Constants.MAX_ITEMS) {
					return i;
				}
			}
		}
		return -1;
	}

	public int findItem(int itemId) {
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null)
				continue;
			if (items[i].getId() == itemId) {
				return i;
			}
		}
		return -1;
	}

	private int findActualItem(int itemId, int amount) {
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null)
				continue;
			if (items[i].getId() == itemId) {
				if (items[i].getCount() == amount) {
					return i;
				}
			}
		}
		return -1;
	}

	/**
	 * Gets an item.
	 * 
	 * @param index
	 *            The position in the container.
	 * @return The item.
	 */
	public Item get(int index) {
		return items[index];
	}

	public int getItemAmount(int itemId) {
		int j = 0;
		for (int i = 0; i < items.length; i++) {
			if (items[i] != null) {
				if (items[i].getId() == itemId) {
					j += items[i].getCount();
				}
			}
		}
		return j;
	}

	public int getAmountInSlot(int slot) {
		if (items[slot] == null) {
			return 0;
		}
		return items[slot].getCount();
	}

	public int getItemInSlot(int slot) {
		if (items[slot] == null) {
			return -1;
		}
		return items[slot].getId();
	}

	public Item getSlot(int slot) {
		return items[slot];
	}

	public int getTotalItems() {
		int totalItems = 0;
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null)
				continue;
			if (items[i].getCount() > 0) {
				totalItems++;
			}
		}
		return totalItems;
	}

	public Item[] getItems() {
		return items;
	}

	public short getSize() {
		return size;
	}

	public ContainerInterface getInterfaceContainer() {
		return interfaceContainer;
	}

	public void setInterfaceContainer(ContainerInterface interfaceContainer) {
		this.interfaceContainer = interfaceContainer;
	}

	public ContainerName getContainerName() {
		return containerName;
	}

	public void setContainerName(ContainerName containerName) {
		this.containerName = containerName;
	}

	public boolean containsDragonShield() {
		int shield = this.getItemInSlot(5);
		if (shield == 11283 || shield == 1540) {
			return true;
		}
		return false;
	}

/*	public boolean hasArtifacts() {
		for (int i = 0; i < items.length; i++) {
			if (items[i] == null)
				continue;
			if (items[i].getId() >= 14876 && items[i].getId() <= 14909) {
				return true;
			}
		}
		return false;
	}*/

	public boolean isInserting() {
		return isInserting;
	}

	public void setInserting(boolean isInserting) {
		this.isInserting = isInserting;
	}

}
