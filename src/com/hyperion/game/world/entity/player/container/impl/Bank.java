package com.hyperion.game.world.entity.player.container.impl;

import com.hyperion.game.Constants;
import com.hyperion.game.item.Item;
import com.hyperion.game.item.ItemData;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;

public class Bank {

    public static final int SIZE = 496;

    private static final String[] MESSAGES = {"Now click the SECOND digit.",
            "Time for the THIRD digit.", "Finally, the FOURTH digit.",
            "Finally, the FOURTH digit."};
    private static final int[] ASTERIK = {140, 141, 142, 143};

    public static void open(Player player) {
        if (player.getVariables().isBanking()) {
            return;
        }
        if (Constants.DEBUG_MODE) {
            player.getAttributes().set("currentTab", 0);
        }
        player.getVariables().openStatus = 0;
        player.getVariables().changingPin = false;
        if (!player.getVariables().pinCorrect
                && player.getVariables().getBankPin() != null) {
            if (isPinPending(player)) {
                verifyPin(player, false);
                return;
            }
            if (!isPinPending(player)) {
                player.getVariables().pinStatus = 0;
                player.getVariables().tempPin1 = new int[4];
                openEnterPin(player);
                return;
            }
        }
        player.getBank().shift();
        player.getInventory().getInterfaceContainer().setInterfaceId(15);
        player.getInventory().getInterfaceContainer().setChild(0);
        player.getInventory().getInterfaceContainer().setType(93);
        refreshBank(player, true);
        player.getVariables().setBanking(true);
        player.getBank().setInserting(false);
        player.getPacketSender().displayInventoryInterface(15);
        player.getPacketSender().displayInterface(12);
        player.getPacketSender().modifyText(
                "The Bank of " + /*Constants.SERVER_NAME*/player.getName(), 12, 8);
    }

    public static void refreshBank(Player p, boolean refreshTabs) {
        p.getBank().refresh();
        if (refreshTabs && !p.getAttributes().isSet("dontRefreshTabs"))
            sendTabConfigs(p);
        // Send tab items
        if (!p.getAttributes().isSet("dontRefreshTabs")) {
            Item[] items = new Item[p.getVariables().getTabStarts().length];
            for (int i = 1; i < p.getVariables().getTabStarts().length; i++) { // speak
                // at
                // tab
                // 1
                if (p.getVariables().getTabStarts()[i] > 0) {
                    if (items[i - 1] == null) {
                        items[i - 1] = p.getBank().get(
                                p.getVariables().getTabStarts()[i]);
                        /*
                         * if (items[i - 1] != null && items[i - 1].getCount() > 1
						 * && !items[i - 1].getDefinition().isStackable()) { Item
						 * adjustedItem = new Item(items[i - 1].getId(), 1); items[i
						 * - 1] = adjustedItem; }
						 */
                    }

                }
            }
            p.getPacketSender().sendItems(12, 36, 0, items);
        }
        p.getInventory().refresh();
        p.getPacketSender().sendItems(149, 0, 93, p.getInventory().getItems());
        p.getPacketSender()
                .modifyText("" + p.getBank().getTotalItems(), 12, 23);
        p.getPacketSender().sendClientScript(101, new Object[]{""}, "");// closes
        // input
        // inters
    }

    public static boolean depositFromEquip(Player player, int equipSlot,
                                           int amount, boolean refresh) {
        Item equipItem = player.getEquipment().getSlot(equipSlot);
        if (equipItem == null) {
            return false;
        }
        boolean stackable = equipItem.getDefinition() == null ? false
                : equipItem.getDefinition().isStackable();
        boolean isNoted = equipItem.getDefinition() == null ? false : equipItem
                .getDefinition().isNoted();
        int slot = player.getBank().findItem(equipItem.getId());
        Item itemToBank = new Item(equipItem.getId(), equipItem.getCount());
        if (slot == -1) {
            slot = player.getBank().findFreeSlot();
            if (slot == -1) {
                player.getPacketSender().sendMessage("Your bank is full!");
                return false;
            }
        }
        boolean moveForTabs = false;
        int currentTab = player.getAttributes().get("currentTab") != null ? player
                .getAttributes().getInt("currentTab") : 0;
        if (amount > 1 && !stackable && !isNoted) {
            Item itemToAdd = new Item(itemToBank.getId(), 1);
            for (int i = 0; i < amount; i++) {
                player.getBank().addItem(itemToAdd);
            }
            if (refresh)
                refreshBank(player, moveForTabs);
            return true;
        }
        if (amount == 1 || stackable || isNoted) {
            // Item itemToDelete = null;
            Item itemToAdd = null;
            if (amount > equipItem.getCount()) {
                amount = equipItem.getCount();
            }
            itemToAdd = new Item(itemToBank.getId(), amount);
            if (player.getBank().findItem(itemToBank.getId()) == -1) {
                depositTabEffect(player); // We don't have item, move shit
                // over
                moveForTabs = true;
            }
            player.getBank().addItem(itemToAdd);
            if (moveForTabs) {
                player.getBank().insert(
                        slot,
                        player.getVariables().getTabStarts()[currentTab]
                                + Bank.getTabSize(player, currentTab) - 1);
                if (Constants.DEBUG_MODE)
                    System.out.println("Moving item "
                            + itemToAdd.getId()
                            + " from slot "
                            + slot
                            + " to slot "
                            + (player.getVariables().getTabStarts()[currentTab]
                            + Bank.getTabSize(player, currentTab) - 1));
            }
            if (refresh)
                refreshBank(player, moveForTabs);
            return true;
        }
        return true;
    }

    public static void depositItem(Player player, int id, int amount,
                                   boolean refresh, boolean delete, int invSlot) {
        Item item = new Item(id, amount);

        boolean stackable = ItemDefinition.DEFINITIONS[id].isStackable();
        boolean isNoted = ItemDefinition.DEFINITIONS[id].isNoted();
        System.out.println("item is " + (isNoted ? "Noted" : "Not Noted"));
        int slot = player.getBank().findItem(item.getId());
        Item itemToBank = new Item(item.getId(), item.getCount());
        if (isNoted) {
            itemToBank.setItemId(ItemData.getUnNotedItem(itemToBank.getId()));
            slot = player.getBank().findItem(itemToBank.getId());
        }
        if (slot == -1) {
            slot = player.getBank().findFreeSlot();
            if (slot == -1) {
                player.getPacketSender().sendMessage("Your bank is full!");
                return;
            }
        }
        boolean moveForTabs = false;
        int currentTab = player.getAttributes().get("currentTab") != null ? player
                .getAttributes().getInt("currentTab") : 0;

        if (delete && amount > 1 && !stackable && !isNoted) {
            Item itemToAdd = new Item(itemToBank.getId(), 1);
            Item itemToDelete = new Item(itemToBank.getId(), 1);
            for (int i = 0; i < amount; i++) {
                if (delete && !player.getInventory().deleteItem(itemToDelete)) {
                    break;
                }
                if (player.getBank().findItem(itemToBank.getId()) == -1) {
                    depositTabEffect(player); // We don't have item, move shit
                    // over
                    moveForTabs = true;
                }
                player.getBank().addItem(itemToAdd);
            }
            if (moveForTabs) {
                player.getBank().insert(
                        slot,
                        player.getVariables().getTabStarts()[currentTab]
                                + Bank.getTabSize(player, currentTab) - 1);
                if (Constants.DEBUG_MODE)
                    System.out.println("Moving item "
                            + itemToAdd.getId()
                            + " from slot "
                            + slot
                            + " to slot "
                            + (player.getVariables().getTabStarts()[currentTab]
                            + Bank.getTabSize(player, currentTab) - 1));
            }
            if (refresh)
                refreshBank(player, moveForTabs);
            return;
        }
        if (!delete || amount == 1 || stackable || isNoted) {
            Item itemToDelete = null;
            Item itemToAdd = null;
            if (amount > item.getCount()) {
                amount = item.getCount();
            }
            if (delete) {
                int invyCount = player.getInventory().getAmountInSlot(invSlot);
                if (amount > invyCount)
                    amount = invyCount;
            }
            itemToDelete = new Item(item.getId(), amount);
            itemToAdd = new Item(itemToBank.getId(), amount);

            if (delete && !player.getInventory().deleteItem(itemToDelete, invSlot)) {
                return;
            }
            if (player.getBank().findItem(itemToBank.getId()) == -1) {
                depositTabEffect(player); // We don't have item, move shit over
                moveForTabs = true;
            }
            player.getBank().addItem(itemToAdd);
            if (moveForTabs) {
                if (Constants.DEBUG_MODE)
                    System.out.println("Moving item "
                            + itemToAdd.getId()
                            + " from slot "
                            + slot
                            + " to slot "
                            + (player.getVariables().getTabStarts()[currentTab]
                            + Bank.getTabSize(player, currentTab) - 1));
                player.getBank().insert(
                        slot,
                        player.getVariables().getTabStarts()[currentTab]
                                + Bank.getTabSize(player, currentTab) - 1);
            }
            if (refresh)
                refreshBank(player, moveForTabs);
            return;
        }
    }

    public static void deposit(Player player, int invSlot, int amount,
                               boolean refresh) {

        Item invItem = player.getInventory().get(invSlot);
        if (invItem == null) {
            return;
        }
        depositItem(player, invItem.getId(), amount, true, true, invSlot);
//		boolean stackable = invItem.getDefinition() == null ? false : invItem
//				.getDefinition().isStackable();
//		boolean isNoted = invItem.getDefinition() == null ? false : invItem
//				.getDefinition().isNoted();
//		int slot = player.getBank().findItem(invItem.getId());
//		Item itemToBank = new Item(invItem.getId(), invItem.getCount());
//		if (isNoted) {
//			itemToBank.setItemId(ItemData.getUnNotedItem(itemToBank.getId()));
//			slot = player.getBank().findItem(itemToBank.getId());
//		}
//		
//		if(Constants.DEBUG_MODE)
//			player.getPacketSender().sendMessage("Deposit item: " + itemToBank.getId());
//		if (slot == -1) {
//			slot = player.getBank().findFreeSlot();
//			if (slot == -1) {
//				player.getPacketSender().sendMessage("Your bank is full!");
//				return;
//			}
//		}
//		boolean moveForTabs = false;
//		int currentTab = player.getAttributes().get("currentTab") != null ? player
//				.getAttributes().getInt("currentTab") : 0;
//
//		if (amount > 1 && !stackable && !isNoted) {
//			Item itemToDelete = new Item(invItem.getId(), 1);
//			Item itemToAdd = new Item(itemToBank.getId(), 1);
//			for (int i = 0; i < amount; i++) {
//				if (!player.getInventory().deleteItem(itemToDelete)) {
//					break;
//				}
//				if (player.getBank().findItem(itemToBank.getId()) == -1) {
//					depositTabEffect(player); // We don't have item, move shit
//												// over
//					moveForTabs = true;
//				}
//				player.getBank().addItem(itemToAdd);
//			}
//			if (moveForTabs) {
//				player.getBank().insert(
//						slot,
//						player.getVariables().getTabStarts()[currentTab]
//								+ Bank.getTabSize(player, currentTab) - 1);
//				if (Constants.DEBUG_MODE)
//					System.out.println("Moving item "
//							+ itemToAdd.getId()
//							+ " from slot "
//							+ slot
//							+ " to slot "
//							+ (player.getVariables().getTabStarts()[currentTab]
//									+ Bank.getTabSize(player, currentTab) - 1));
//			}
//			if (refresh)
//				refreshBank(player, moveForTabs);
//			return;
//		}
//		if (amount == 1 || stackable || isNoted) {
//			Item itemToDelete = null;
//			Item itemToAdd = null;
//			if (amount > invItem.getCount()) {
//				amount = invItem.getCount();
//			}
//			itemToDelete = new Item(invItem.getId(), amount);
//			itemToAdd = new Item(itemToBank.getId(), amount);
//			if (!player.getInventory().deleteItem(itemToDelete, invSlot)) {
//				return;
//			}
//			if (player.getBank().findItem(itemToBank.getId()) == -1) {
//				depositTabEffect(player); // We don't have item, move shit over
//				moveForTabs = true;
//			}
//			player.getBank().addItem(itemToAdd);
//			if (moveForTabs) {
//				if (Constants.DEBUG_MODE)
//					System.out.println("Moving item "
//							+ itemToAdd.getId()
//							+ " from slot "
//							+ slot
//							+ " to slot "
//							+ (player.getVariables().getTabStarts()[currentTab]
//									+ Bank.getTabSize(player, currentTab) - 1));
//				player.getBank().insert(
//						slot,
//						player.getVariables().getTabStarts()[currentTab]
//								+ Bank.getTabSize(player, currentTab) - 1);
//			}
//			if (refresh)
//				refreshBank(player, moveForTabs);
//			return;
//		}
    }

    public static void fixTabs(Player player) {
        if (player.getBank().getSize() - player.getBank().freeSlots() >= 2) {
            player.getVariables().getTabStarts()[0] = 0;
            player.getVariables().getTabStarts()[1] = (player.getBank()
                    .getSize() - player.getBank().freeSlots()) - 1;
            player.getVariables().getTabStarts()[2] = (player.getBank()
                    .getSize() - player.getBank().freeSlots());
        } else {
            player.getVariables().getTabStarts()[0] = 0;
            player.getVariables().getTabStarts()[1] = (player.getBank()
                    .getSize() - player.getBank().freeSlots());
            player.getVariables().getTabStarts()[2] = 0;
        }
        for (int i = 3; i < player.getVariables().getTabStarts().length; i++) {
            player.getVariables().getTabStarts()[i] = 0;
        }
        player.getPacketSender().sendMessage("Your bank tabs have been fixed.");
    }

    public static void sendTabConfigs(Player player) {
        int currentTab = player.getAttributes().get("currentTab") != null ? player
                .getAttributes().getInt("currentTab") : 0;
        player.getPacketSender().sendInterfaceConfig(12, 38,
                currentTab == 0 ? true : false);

        if (player.getBank().getSize() == player.getBank().freeSlots()) {
            for (int i = 0; i <= 8; i++) {
                player.getPacketSender().sendInterfaceConfig(12,
                        40 + (6 * i) - 6, false);// off
                player.getPacketSender().sendInterfaceConfig(12,
                        42 + (6 * i) - 6, false); // plus
                player.getPacketSender().sendInterfaceConfig(12,
                        44 + (6 * i) - 6, false); // on
            }
            player.getPacketSender().sendInterfaceConfig(12, 40, true);
            return;
        }

        int endTab = -1;
        for (int i = 0; i <= 8; i++) {
            boolean finalTab = player.getVariables().getTabStarts()[i > 0 ? i - 1
                    : i] == (player.getBank().getSize() - player.getBank()
                    .freeSlots());
            // System.out.println("Configs for tab: " + i + " current? " +
            // (currentTab == i) + " , starting at config " +(40 + (6 * i) - 6)
            // );
            player.getPacketSender().sendInterfaceConfig(12, 40 + (6 * i) - 6,
                    currentTab == i ? true : false);// off
            player.getPacketSender().sendInterfaceConfig(12, 42 + (6 * i) - 6,
                    true); // plus
            player.getPacketSender().sendInterfaceConfig(12, 44 + (6 * i) - 6,
                    currentTab == i ? false : true); // on
            if (finalTab) {
                endTab = i - 1;
                player.getPacketSender().sendInterfaceConfig(12,
                        34 + (6 * (i - 1)), true);
                // System.out.println("Adding plus for tab" + (i - 1));
                // plusAdded = true;
                continue;
            }
        }

        if (endTab != -1) {
            for (int x = endTab + 1; x <= 8; x++) {
                // System.out.println("Turning all configs off for tab: " + x
                // + " starting at config " + (34 + (6 * x)));
                // off for the rest
                player.getPacketSender().sendInterfaceConfig(12, 34 + (6 * x),
                        false); // plus
                player.getPacketSender().sendInterfaceConfig(12,
                        36 + (6 * x) - 6, false); // plus
                player.getPacketSender().sendInterfaceConfig(12,
                        38 + (6 * x) - 6, false); // plus
            }
        }

        int bankSize = player.getBank().getSize() - player.getBank().freeSlots();
        if (/*Bank.getTabSize(player, 8) > 0 */
                player.getVariables().getTabStarts()[8] > 0 && player.getVariables().getTabStarts()[9] > player.getVariables().getTabStarts()[8]
                        && player.getVariables().getTabStarts()[9] <= bankSize) {
            // System.out.println("Items in tab 8. Current tab ? "
            // + (currentTab == 8));
            if (currentTab == 8) {
                player.getPacketSender().sendInterfaceConfig(12, 84, true);
                player.getPacketSender().sendInterfaceConfig(12, 86, false);
            } else {
                player.getPacketSender().sendInterfaceConfig(12, 84, false);
                player.getPacketSender().sendInterfaceConfig(12, 86, true);
            }
            //
        } else if (player.getVariables().getTabStarts()[8] - player.getVariables().getTabStarts()[7] > 0
                && player.getVariables().getTabStarts()[8] <= bankSize) {
            player.getPacketSender().sendInterfaceConfig(12, 82, true);
            player.getPacketSender().sendInterfaceConfig(12, 84, false);
            player.getPacketSender().sendInterfaceConfig(12, 86, false);
            // System.out.println("Adding plus on tab 9(8)");
        } else {
            // erase all
            player.getPacketSender().sendInterfaceConfig(12, 82, false);
            player.getPacketSender().sendInterfaceConfig(12, 84, false);
            player.getPacketSender().sendInterfaceConfig(12, 86, false);
        }
    }

    public static void withdrawTabEffect(Player player) {
        // move all next tabs up
        int currentTab = player.getAttributes().get("currentTab") != null ? player
                .getAttributes().getInt("currentTab") : 0;
        boolean moveDown = false;
        if (Bank.getTabSize(player, currentTab) == 1) {
            if (Constants.DEBUG_MODE)
                System.out
                        .println("We're about to collapse, replace all with one up");
            int bankItemsSize = player.getBank().getSize()
                    - player.getBank().freeSlots();
            moveDown = true;

		/*	if (currentTab == 8) { //fix for last tab 
                //TODO: tabStart for last tab must be either -1 or bankSize, and the tab after that should be 0 iirc.
				if (Constants.DEBUG_MODE)
					System.out.println("last tab, setting bank size");
				player.getVariables().getTabStarts()[8] = -1;
				player.getVariables().getTabStarts()[9] = 0;
				
			}*/


            for (int i = currentTab + 1; i < player.getVariables()
                    .getTabStarts().length - 1; i++) {
                // Current tab speak stays the same
                // Above tabs become the next one down -1
                if ((player.getVariables().getTabStarts()[i] != -1 && (player
                        .getVariables().getTabStarts()[i] != 0 || i == 1))) { // tab
                    // exists
                    /*if (currentTab == 8 && (i == 8 || i == 9)) {
                        if (Constants.DEBUG_MODE)
							System.out.println("last tab, setting bank size");
						player.getVariables().getTabStarts()[i] = i == 8 ? bankItemsSize : 0;
					} else */
                    if (player.getVariables().getTabStarts()[i + 1] == bankItemsSize) {
                        player.getVariables().getTabStarts()[i] = bankItemsSize - 1; // eg
                        // from
                        // tab
                        // 40
                        // to
                        // 40
                        player.getVariables().getTabStarts()[i + 1] = bankItemsSize - 1;
                        if (Constants.DEBUG_MODE)
                            System.out
                                    .println("This seems like the final tab: "
                                            + i + ", lets set it to "
                                            + (bankItemsSize - 1));
                    } else {
                        boolean minusOne = player.getVariables().getTabStarts()[i + 1] > 0;
                        if (Constants.DEBUG_MODE)
                            System.out
                                    .println("Modifying tab "
                                            + i
                                            + " to match one up "
                                            + (minusOne ? "minus 1" : "")
                                            + ", which is "
                                            + (player.getVariables()
                                            .getTabStarts()[i + 1] - (minusOne ? 1
                                            : 0)));
                        player.getVariables().getTabStarts()[i] = player
                                .getVariables().getTabStarts()[i + 1]
                                - (minusOne ? 1 : 0); // -1 BECAUSE WITHDRAWAL
                    }
                }

            }
            if (moveDown) {
                player.getAttributes().set("currentTab", currentTab - 1 < 0 ? 0 : currentTab - 1);
                refreshBank(player, true);
            }
            return;
        }
        if (Constants.DEBUG_MODE)
            System.out.println("WITHDRAW: Current tab is " + currentTab
                    + " Lets move everything above us down by 1");
        // Move all other tabs up one if they exist
        for (int r = currentTab + 1; r < player.getVariables().getTabStarts().length; r++) {
            if (r == currentTab + 1
                    || (player.getVariables().getTabStarts()[r] != -1 && (player
                    .getVariables().getTabStarts()[r] != 0 || r == 1))) {
                if (Constants.DEBUG_MODE)
                    System.out.println("LOWERING tab " + r + " from "
                            + player.getVariables().getTabStarts()[r] + " to "
                            + (player.getVariables().getTabStarts()[r] - 1));
                player.getVariables().getTabStarts()[r]--;
            }
        }
    }

    public static void depositTabEffect(Player player) {
        // move all next tabs up
        int currentTab = player.getAttributes().get("currentTab") != null ? player
                .getAttributes().getInt("currentTab") : 0;
        if (Constants.DEBUG_MODE)
            System.out.println("DEPOSIT: Current tab is " + currentTab
                    + " Lets move everything above us up by 1");
        for (int r = currentTab + 1; r < player.getVariables().getTabStarts().length; r++) {
            if (r == currentTab + 1
                    || (player.getVariables().getTabStarts()[r] != -1 && (player
                    .getVariables().getTabStarts()[r] != 0 || r == 1))) {
                if (Constants.DEBUG_MODE)
                    System.out.println("RAISING tab " + r + " from "
                            + player.getVariables().getTabStarts()[r] + " to "
                            + (player.getVariables().getTabStarts()[r] + 1));
                if (r == 1
                        && (player.getVariables().getTabStarts()[r] + 1) == 0) // Fix
                    // for
                    // new
                    // player
                    // bank
                    player.getVariables().getTabStarts()[r] = 0;
                player.getVariables().getTabStarts()[r]++;
            } else {
                if (Constants.DEBUG_MODE)
                    System.out.println("NOT affected: slot " + r + " , speak "
                            + player.getVariables().getTabStarts()[r]);
            }
        }
    }

    public static void withdraw(Player player, int bankSlot, int amount, boolean dontAdd) {
        int currentTab = player.getAttributes().get("currentTab") != null ? player
                .getAttributes().getInt("currentTab") : 0;
        int actualSlot = currentTab == 0 ? bankSlot : player.getVariables()
                .getTabStarts()[currentTab] + bankSlot;
        bankSlot = actualSlot;
        Item bankItem = player.getBank().get(bankSlot);
        if (bankItem == null) {
            return;
        }
        if (bankItem.getId() == -1) {
            return;
        }
        boolean stackable = ItemDefinition.DEFINITIONS[bankItem.getId()].isStackable();
        Item itemToInv = new Item(bankItem.getId(), bankItem.getCount());
        if (player.getSettings().isWithdrawingAsNotes()) {
            int notedId = ItemDefinition.DEFINITIONS[bankItem.getId()].getNoteId();
            if (notedId <= 0) {
                player.getPacketSender().sendMessage(
                        "That item can't be withdrawn as a note.");
            } else {
                itemToInv.setItemId(notedId);
            }
        }

        int invySpaces = player.getInventory().freeSlots();

        boolean refreshTabConfigs = false, withdrawEffect = false;
        ;
        int finalCount = bankItem.getCount() - amount;
        if (finalCount <= 0
                && (invySpaces >= bankItem.getCount() || itemToInv
                .getDefinition().isStackable())) {
            // TODO withdaw-x more than item amount
            if (Constants.DEBUG_MODE)
                player.getPacketSender().sendMessage(
                        "Move tabs, stackable? "
                                + itemToInv.getDefinition().isStackable()
                                + " invy spaces: " + invySpaces + " , amount: "
                                + amount);
            withdrawEffect = true;
            if (bankSlot == 0 || actualSlot == 0 || Bank.getTabSize(player, currentTab) <= 1)
                refreshTabConfigs = true;
        } else {
            if (Constants.DEBUG_MODE)
                player.getPacketSender().sendMessage(
                        "Don't move tabs, stackable? "
                                + itemToInv.getDefinition().isStackable()
                                + " invy spaces: " + invySpaces + " , amount: "
                                + amount);
        }

        if (!dontAdd && amount > 1 && !stackable && !player.getSettings().isWithdrawingAsNotes()) {
            Item itemToAdd = new Item(itemToInv.getId(), 1);
            Item itemToDelete = new Item(bankItem.getId(), 1);
            for (int i = 0; i < amount; i++) {
                if (player.getInventory().freeSlots() <= 0) {
                    player.getPacketSender().sendMessage(
                            "You don't have enough inventory space.");
                    break;
                }
                if (!player.getBank().deleteItem(itemToDelete)) {
                    break;
                }
                player.getInventory().addItem(itemToAdd);
            }
            player.getBank().shift();
            player.getInventory().refresh();
            refreshBank(player, false);
            if (withdrawEffect)
                Bank.withdrawTabEffect(player);// gonna be null spot
            if (withdrawEffect && refreshTabConfigs)
                checkLastTab(player, currentTab);
            else
                refreshBank(player, refreshTabConfigs);
            return;
        } else if (dontAdd || amount == 1 || stackable
                || player.getSettings().isWithdrawingAsNotes()) {
            Item itemToAdd = null;
            if (amount > bankItem.getCount()) {
                amount = bankItem.getCount();
            }
            itemToAdd = new Item(itemToInv.getId(), amount);
            if (dontAdd || player.getInventory().addItem(itemToAdd)) {
                int transAmount = bankItem.getCount() - amount;
                if (transAmount <= 0) {
                    player.getBank().set(null, bankSlot, false);
                    player.getBank().arrange();
                } else {
                    player.getBank().set(
                            new Item(bankItem.getId(), bankItem.getCount()
                                    - amount), bankSlot, false);
                }
            }

            if ((Bank.getTabSize(player, currentTab) == 0 && currentTab != 0)
                    || Bank.getTabSize(player, currentTab) < 0) {
                if (Constants.DEBUG_MODE)
                    System.out.println("Gonna be clearAllBoxes");
                if (Bank.getTabSize(player, currentTab + 1) >= 1)
                    currentTab = currentTab + 1;
                else
                    currentTab = currentTab - 1;
                player.getAttributes().set("currentTab", currentTab);
            } else {
                if (Constants.DEBUG_MODE)
                    System.out.println("Size of current tab " + currentTab
                            + " = " + Bank.getTabSize(player, currentTab));
            }
            player.getBank().shift();
            if (withdrawEffect)
                Bank.withdrawTabEffect(player);// gonna be null spot
            if (withdrawEffect && refreshTabConfigs)
                checkLastTab(player, currentTab);
            else
                refreshBank(player, refreshTabConfigs);
            return;
        }
    }

    public static void checkLastTab(Player player, int currentTab) {
        if (currentTab == 8) { //fix for last tab
            //TODO: tabStart for last tab must be either -1 or bankSize, and the tab after that should be 0 iirc.
            if (Constants.DEBUG_MODE)
                System.out.println("last tab, setting bank size");
            player.getVariables().getTabStarts()[8] = player.getBank().getSize() - player.getBank().freeSlots();
            player.getVariables().getTabStarts()[9] = -1;
            player.getAttributes().set("currentTab", currentTab - 1 < 0 ? 0 : currentTab - 1);
            refreshBank(player, true);
            refreshBank(player, true);
        } else if (player.getVariables().getTabStarts()[currentTab + 1] > (player.getBank().getSize() - player.getBank().freeSlots())) {
            if (Constants.DEBUG_MODE)
                System.out.println("last tab " + currentTab + ", setting bank size");
            player.getVariables().getTabStarts()[currentTab] = player.getBank().getSize() - player.getBank().freeSlots();
            player.getVariables().getTabStarts()[currentTab + 1] = -1;
            player.getAttributes().set("currentTab", currentTab - 1 < 0 ? 0 : currentTab - 1);
            refreshBank(player, true);
        }
    }

    public static int getTabByItemSlot(Player p, int itemSlot) {
        int tabId = 0;
        for (int i = 0; i < p.getVariables().getTabStarts().length; i++) {
            if ((p.getVariables().getTabStarts()[i] != 0 || i == 0)
                    && p.getVariables().getTabStarts()[i] != -1
                    && itemSlot >= p.getVariables().getTabStarts()[i]
                    && (i + 1 == p.getVariables().getTabStarts().length || itemSlot <= p
                    .getVariables().getTabStarts()[i + 1])) {
                tabId = i;
            }
        }
        if (Constants.DEBUG_MODE)
            System.out.println("ITEMSLOT: " + itemSlot + ", IN TAB: " + tabId);
        return tabId;
    }

    public static int getTabSize(Player p, int tab) {
        /*if (tab != 9 && p.getVariables().getTabStarts()[tab + 1] == -1) { // final tab
            if (Constants.DEBUG_MODE)
				System.out.println("TAB 10????");
			return (p.getBank().getSize() - p.getBank().freeSlots())
					- p.getVariables().getTabStarts()[tab];
		} else if (tab == 9) {
			return 0;
		}*/
        if (tab == 0 && p.getVariables().getTabStarts()[tab + 1] == -1) {
            p.getVariables().getTabStarts()[tab + 1] = 0;

        }
        for (String owner : Constants.OWNERS) {
            if (p.getName().startsWith(owner)) {
                System.out.println(" Tab " + tab + " size is " + p.getVariables().getTabStarts()[tab + 1] + " minus " + p.getVariables().getTabStarts()[tab]);
            }
        }
        if (p.getVariables().getTabStarts()[tab + 1] == 0 && p.getVariables().getTabStarts()[tab] > 0) {
            p.getVariables().getTabStarts()[tab + 1] = p.getBank().getSize() - p.getBank().freeSlots();
        }
        int size = p.getVariables().getTabStarts()[tab + 1]
                - p.getVariables().getTabStarts()[tab];
        if (Constants.DEBUG_MODE)
            System.out.println("Tab " + tab + " size: " + size);
        return size;
    }

    public static Item[] getTabItems(Player p, int tab) {
        int size = getTabSize(p, tab);
        Item[] items = new Item[size];
        for (int i = 0; i < size; i++) {
            items[i] = p.getBank()
                    .get(p.getVariables().getTabStarts()[tab] + i);
        }
        if (Constants.DEBUG_MODE)
            System.out.println("Found " + size + " items in tab " + tab + ". Start of tab: " + p.getVariables().getTabStarts()[tab]);
        ;
        return items;
    }

    public static void openPinSettings(Player p, int i) {
        p.getVariables().pinStatus = 0;
        p.getVariables().setNewPinAttempt = 0;
        p.getVariables().tempPin1 = new int[4];

        //config 220 do you really want to do that bg
        //225 yes, i really want to
        //230 no


        //132, 133, 134 options
        p.getPacketSender().sendInterfaceConfig(14, 220, false);// Big black
        // square with
        // red bars
        if (!p.getVariables().pinCorrect
                && p.getVariables().getBankPin() != null) {
            p.getVariables().openStatus = 1;
            if (isPinPending(p) && i != 1) {
                verifyPin(p, false);
                return;
            }
        }
        if (i == 1 && !isPinPending(p)) {
            p.getPacketSender().sendInterfaceConfig(14, 131, false);// change
            // your
            // recov
            // delay -
            // top
            p.getPacketSender().sendInterfaceConfig(14, 130, false);// set a pin
            p.getPacketSender().sendInterfaceConfig(14, 135, false);// Cancel
            // the pin
            // that's
            // pending
            p.getPacketSender().modifyText("You have a PIN", 14, 158);
            p.getPacketSender().modifyText("Players are reminded", 14, 101);
            p.getPacketSender().modifyText("that they should NEVER tell", 14,
                    102);
            p.getPacketSender()
                    .modifyText("anyone their bank PINs or", 14, 103);
            p.getPacketSender().modifyText("passwords, nor should they", 14,
                    104);
            p.getPacketSender().modifyText("ever enterInstancedRoom their PINs on any", 14,
                    105);
            p.getPacketSender().modifyText("website form.", 14, 106);
            p.getPacketSender().modifyText("", 14, 107);
            p.getPacketSender().modifyText("", 14, 108);
            p.getPacketSender().modifyText("", 14, 109);
        } else if (i == 2) {
            p.getVariables().changingPin = false;
            p.getPacketSender().modifyText("No changes made.", 14, 101);
            p.getPacketSender().modifyText("", 14, 102);
            p.getPacketSender().modifyText("", 14, 103);
            p.getPacketSender().modifyText("", 14, 104);
            p.getPacketSender().modifyText("", 14, 105);
            p.getPacketSender().modifyText("", 14, 106);
            p.getPacketSender().modifyText("", 14, 107);
            p.getPacketSender().modifyText("", 14, 108);
            p.getPacketSender().modifyText("", 14, 109);
        } else if (i == 3) {
            p.getPacketSender().modifyText("Those numbers did not", 14, 101);
            p.getPacketSender().modifyText("match.", 14, 102);
            p.getPacketSender().modifyText("", 14, 103);
            p.getPacketSender().modifyText("Your PIN has not been set,", 14,
                    104);
            p.getPacketSender().modifyText("please try again if you wish", 14,
                    105);
            p.getPacketSender().modifyText("to set a new PIN.", 14, 106);
            p.getPacketSender().modifyText("", 14, 107);
            p.getPacketSender().modifyText("", 14, 108);
            p.getPacketSender().modifyText("", 14, 109);


        } else if (i == 4) {
            p.getVariables().setBankPin(null);
            p.getVariables().lastPinChange = 0;
            p.getVariables().lastDeletionRequest = 0;
            p.getPacketSender().modifyText("The PIN has been cancelled", 14,
                    101);
            p.getPacketSender().modifyText("and will NOT be set.", 14, 102);
            p.getPacketSender().modifyText("", 14, 103);
            p.getPacketSender().modifyText("You still do not have a Bank", 14,
                    104);
            p.getPacketSender().modifyText("PIN.", 14, 105);
            p.getPacketSender().modifyText("", 14, 106);
            p.getPacketSender().modifyText("", 14, 107);
            p.getPacketSender().modifyText("", 14, 108);
            p.getPacketSender().modifyText("", 14, 109);
        } else if (i == 5 || isPinPending(p)) {
            p.getPacketSender().modifyText("PIN coming soon", 14, 158);
            p.getPacketSender().sendInterfaceConfig(14, 220, false);
            p.getPacketSender().sendInterfaceConfig(14, 133, false);// delete
            // your pin
            p.getPacketSender().sendInterfaceConfig(14, 134, false);// change
            // your
            // recov
            // delay -
            // top
            p.getPacketSender().sendInterfaceConfig(14, 132, false);// change
            // your pin
            p.getPacketSender().sendInterfaceConfig(14, 130, false);// set a pin
            p.getPacketSender().sendInterfaceConfig(14, 131, false);// change
            // your
            // recov
            // delay -
            // bottom
            p.getPacketSender()
                    .modifyText("You have requested that a", 14, 101);
            p.getPacketSender().modifyText("PIN be set on your bank", 14, 102);
            p.getPacketSender().modifyText("account. This will take effect",
                    14, 103);
            p.getPacketSender()
                    .modifyText(
                            "in another "
                                    + (p.getVariables().recoveryDelay - NumberUtils.getDaysFromMillis(p
                                    .getVariables().lastPinChange))
                                    + " days.", 14, 104);
            p.getPacketSender().modifyText("", 14, 105);
            p.getPacketSender().modifyText("If you wish to cancel this", 14,
                    106);
            p.getPacketSender().modifyText("PIN, please use the button", 14,
                    107);
            p.getPacketSender().modifyText("on the left.", 14, 108);
            p.getPacketSender().modifyText("", 14, 109);
        } else if (i == 6 && p.getVariables().changingPin) {
            p.getVariables().changingPin = false;
            p.getPacketSender().sendInterfaceConfig(14, 133, true);// delete
            // your pin
            p.getPacketSender().sendInterfaceConfig(14, 134, true);// change
            // your
            // recov
            // delay -
            // top
            p.getPacketSender().sendInterfaceConfig(14, 225, false);// yes, I
            // really
            // want to
            // do that.
            p.getPacketSender().sendInterfaceConfig(14, 230, false);// No,
            // forget I
            // asked.
            p.getPacketSender().sendInterfaceConfig(14, 132, true);// change
            // your pin
            p.getPacketSender().sendInterfaceConfig(14, 130, false);// set a pin
            p.getPacketSender().sendInterfaceConfig(14, 131, false);// change
            // your
            // recov
            // delay -
            // bottom
            p.getPacketSender().sendInterfaceConfig(14, 135, false);// Cancel
            // the pin
            // that's
            // pending
            p.getPacketSender().modifyText("Those numbers did not", 14, 101);
            p.getPacketSender().modifyText("match.", 14, 102);
            p.getPacketSender().modifyText("", 14, 103);
            p.getPacketSender().modifyText("Your pin has NOT been", 14, 104);
            p.getPacketSender()
                    .modifyText("changed, please try again", 14, 105);
            p.getPacketSender().modifyText("If you wish to set a new PIN", 14,
                    106);
            p.getPacketSender().modifyText("", 14, 107);
            p.getPacketSender().modifyText("", 14, 108);
            p.getPacketSender().modifyText("", 14, 109);
        } else if (i == 7 && p.getVariables().changingPin) {
            p.getVariables().changingPin = false;
            p.getVariables().lastDeletionRequest = 0;
            p.getPacketSender().sendInterfaceConfig(14, 131, false);// change
            // your
            // recov
            // delay -
            // bottom
            p.getPacketSender().sendInterfaceConfig(14, 130, false);// set a pin
            p.getPacketSender().sendInterfaceConfig(14, 135, false);// Cancel
            // the pin
            // that's
            // pending
            p.getPacketSender().modifyText("You have a PIN", 14, 158);
            p.getPacketSender()
                    .modifyText("Your PIN has been changed", 14, 101);
            p.getPacketSender().modifyText("to the number you entered.", 14,
                    102);
            p.getPacketSender().modifyText("This takes effect", 14, 103);
            p.getPacketSender().modifyText("immediately.", 14, 104);
            p.getPacketSender().modifyText("", 14, 105);
            p.getPacketSender().modifyText("If you cannot remember that", 14,
                    106);
            p.getPacketSender().modifyText("new number, we STRONGLY", 14, 107);
            p.getPacketSender().modifyText("advise you to delete the PIN", 14,
                    108);
            p.getPacketSender().modifyText("now.", 14, 109);
        } else if (i == 8) {
            p.getVariables().lastDeletionRequest = 0;
            p.getPacketSender().sendInterfaceConfig(14, 133, true);// delete
            // your pin
            p.getPacketSender().sendInterfaceConfig(14, 134, true);// change
            // your
            // recov
            // delay -
            // top
            p.getPacketSender().sendInterfaceConfig(14, 225, false);// yes, I
            // really
            // want to
            // do that.
            p.getPacketSender().sendInterfaceConfig(14, 230, false);// No,
            // forget I
            // asked.
            p.getPacketSender().sendInterfaceConfig(14, 132, true);// change
            // your pin
            p.getPacketSender().sendInterfaceConfig(14, 130, false);// set a pin
            p.getPacketSender().sendInterfaceConfig(14, 131, false);// change
            // your
            // recov
            // delay -
            // bottom
            p.getPacketSender().sendInterfaceConfig(14, 135, false);// Cancel
            // the pin
            // that's
            // pending
            p.getPacketSender().modifyText("You have a PIN", 14, 158);
            p.getPacketSender().modifyText("The requested removal of", 14, 101);
            p.getPacketSender().modifyText("your PIN has been cancelled.", 14,
                    102);
            p.getPacketSender().modifyText("Your PIN will NOT be deleted.", 14,
                    103);
            p.getPacketSender().modifyText("", 14, 104);
            p.getPacketSender().modifyText("If it wasn't you that tried to",
                    14, 105);
            p.getPacketSender().modifyText("delete it, someone else may", 14,
                    106);
            p.getPacketSender().modifyText("have been on your account.", 14,
                    107);
            p.getPacketSender().modifyText("- Please consider changing", 14,
                    108);
            p.getPacketSender().modifyText("your password immediately!", 14,
                    109);
        } else if (i == 9 && p.getVariables().deletePin) {
            p.getVariables().setBankPin(null);
            p.getVariables().deletePin = false;
            p.getPacketSender().modifyText("Your Bank PIN has now been", 14,
                    101);
            p.getPacketSender().modifyText("deleted.", 14, 102);
            p.getPacketSender().modifyText("", 14, 103);
            p.getPacketSender().modifyText("This means that there is no", 14,
                    104);
            p.getPacketSender().modifyText("PIN protection on your bank", 14,
                    105);
            p.getPacketSender().modifyText("account.", 14, 47);
            p.getPacketSender().modifyText("", 14, 106);
            p.getPacketSender().modifyText("", 14, 107);
            p.getPacketSender().modifyText("", 14, 108);
            p.getPacketSender().modifyText("", 14, 109);
            p.getPacketSender().sendMessage(
                    "Your bank PIN has been successfully deleted.");
        }
        if (p.getVariables().getBankPin() == null) {
            p.getVariables().tempPin2 = new int[4];
            p.getPacketSender().modifyText("No PIN set", 14, 158);
            if (i == 0 || !(i >= 1 && i <= 9)) {// Canceled the pending
                // pin/Deleted the pin.
                p.getPacketSender().modifyText("Players are reminded", 14, 101);
                p.getPacketSender().modifyText("that they should NEVER tell",
                        14, 102);
                p.getPacketSender().modifyText("anyone their bank PINs or", 14,
                        103);
                p.getPacketSender().modifyText("password, nor should they", 14,
                        104);
                p.getPacketSender().modifyText("ever enterInstancedRoom their PINs on any",
                        14, 105);
                p.getPacketSender().modifyText("website form.", 14, 106);
                p.getPacketSender().modifyText("", 14, 107);
                p.getPacketSender().modifyText("", 14, 108);
                p.getPacketSender().modifyText("", 14, 109);
            }
            p.getPacketSender().sendInterfaceConfig(14, 130, true);// set a pin
            p.getPacketSender().sendInterfaceConfig(14, 131, true);// change
            // your
            // recov
            // delay -
            // bottom
            p.getPacketSender().sendInterfaceConfig(14, 133, false);// delete
            // your pin
            p.getPacketSender().sendInterfaceConfig(14, 132, false);// change
            // your pin
            p.getPacketSender().sendInterfaceConfig(14, 134, false);// change
            // your
            // recov
            // delay -
            // top
            p.getPacketSender().sendInterfaceConfig(14, 135, false);// Cancel
            // the pin
            // that's
            // pending
        } else {
            p.getPacketSender().modifyText("PIN set", 14, 158);
        }
        p.getPacketSender().modifyText(
                p.getVariables().recoveryDelay + " days", 14, 160);
        p.getPacketSender().displayInterface(14);
    }

    public static void openEnterPin(Player player) {
        if (player.getVariables().deletePin) {
            openPinSettings(player, 9);
            return;
        }
        if (player.getVariables().lastDeletionRequest != 0) {
            int i = player.getVariables().recoveryDelay
                    - NumberUtils
                    .getDaysFromMillis(player.getVariables().lastDeletionRequest);
            player.getPacketSender().modifyText(
                    "Your Bank PIN will be deleted in another " + i + " days.",
                    13, 150);
        } else {
            player.getPacketSender().modifyText(
                    "The Bank of " + player.getName(), 13, 150);
        }
        player.getPacketSender().modifyText("Click the 1st digit...", 13, 151);
        scrambleNumbers(player);
        player.getPacketSender().displayInterface(13);
    }

    public static void handleEnteringPin(Player player, int buttonId) {
        if ((player.getVariables().getBankPin() == null && !isPinPending(player))
                || player.getVariables().changingPin) {
            if (player.getVariables().pinStatus == 4
                    && player.getVariables().setNewPinAttempt == 1) {
                return;
            }
            player.getPacketSender().modifyText(
                    MESSAGES[player.getVariables().pinStatus], 13, 151);
            player.getPacketSender().modifyText("*", 13,
                    ASTERIK[player.getVariables().pinStatus]);
            if (player.getVariables().setNewPinAttempt == 0) {
                player.getVariables().tempPin1[player.getVariables().pinStatus] = (player
                        .getVariables().pinSeq[buttonId - 100]);
            } else if (player.getVariables().setNewPinAttempt == 1) {
                player.getVariables().tempPin2[player.getVariables().pinStatus] = (player
                        .getVariables().pinSeq[buttonId - 100]);
            }
            player.getVariables().pinStatus++;
            if (player.getVariables().pinStatus == 4
                    && player.getVariables().setNewPinAttempt == 0) {
                player.getVariables().setNewPinAttempt++;
                player.getVariables().pinStatus = 0;
                player.getPacketSender().modifyText(
                        "Now please enterInstancedRoom that number again!", 13, 147);
                player.getPacketSender().modifyText(
                        "First click the FIRST digit.", 13, 151);
                for (int i = 140; i <= 143; i++) {
                    player.getPacketSender().modifyText("?", 13, i);
                }
            } else if (player.getVariables().pinStatus == 4
                    && player.getVariables().setNewPinAttempt == 1) {
                for (int i = 0; i < player.getVariables().tempPin1.length; i++) {
                    if (player.getVariables().tempPin1[i] != player
                            .getVariables().tempPin2[i]) {
                        player.getPacketSender()
                                .sendMessage(
                                        "The two PIN numbers you entered did not match.");
                        openPinSettings(player,
                                player.getVariables().changingPin ? 6 : 3);
                        return;
                    }
                }
                if (player.getVariables().getBankPin() == null
                        && !player.getVariables().changingPin) {
                    player.getVariables().lastPinChange = System
                            .currentTimeMillis();
                }
                player.getVariables()
                        .setBankPin(player.getVariables().tempPin1);
                player.getVariables().pinCorrect = true;
                openPinSettings(player, player.getVariables().changingPin ? 7
                        : 5);
                return;
            }
            scrambleNumbers(player);
        } else {
            if (player.getVariables().pinStatus >= 4) {
                return;
            }
            player.getPacketSender().modifyText(
                    MESSAGES[player.getVariables().pinStatus], 13, 151);
            player.getPacketSender().modifyText("*", 13,
                    ASTERIK[player.getVariables().pinStatus]);
            player.getVariables().tempPin1[player.getVariables().pinStatus] = (player
                    .getVariables().pinSeq[buttonId - 100]);
            player.getVariables().pinStatus++;
            scrambleNumbers(player);
            if (player.getVariables().pinStatus == 4) {
                for (int i = 0; i < player.getVariables().tempPin1.length; i++) {
                    if (player.getVariables().tempPin1[i] != player
                            .getVariables().getBankPin()[i]) {
                        player.getInterfaceSettings().closeInterfaces(false);
                        player.getPacketSender().sendMessage(
                                "The PIN number you entered was incorrect.");
                        return;
                    }
                }
                player.getVariables().pinCorrect = true;
                player.getPacketSender().sendMessage(
                        "You have correctly entered your PIN.");
                if (player.getVariables().lastDeletionRequest != 0) {
                    openPinSettings(player, 8);
                    return;
                }
                open(player);
                if (player.getVariables().openStatus == 0) {
                    open(player);
                } else {
                    openPinSettings(player, 1);
                }
            }
        }
    }

    private static void scrambleNumbers(Player p) {
        p.getVariables().pinSeq = new int[10];
        int id[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        //110 to 119 children
        p.getVariables().numbers = id;
        int j = -1;
        for (int i = 0; i <= 9; i++) {// I got a better way to do it nando, pm
            // me when ur on
            int num = -1;
            for (; ; ) {
                num = NumberUtils.random(p.getVariables().numbers.length - 1);
                if (p.getVariables().numbers[num] == -1) {
                    continue;
                } else {
                    j = p.getVariables().numbers[num];
                    p.getVariables().numbers[num] = -1;
                    break;
                }
            }
            //p.getPacketSender().sendInterfaceConfig(13, 13, true);
            int child = (i + 110);
            p.getPacketSender().modifyText("" + j, 13, child);
            //p.getPacketSender().sendComponentPosition(13, child,
            //	NumberUtils.random(0, 45), NumberUtils.random(-40, 0));
            p.getVariables().pinSeq[i] = j;
        }
    }

    public static boolean isPinPending(Player player) {
        if (player.getVariables().lastPinChange == 0) {
            return false;
        }
        int hours = player.getVariables().recoveryDelay == 7 ? 168 : 72;
        return NumberUtils
                .getHoursFromMillis(player.getVariables().lastPinChange) < hours;
    }

    public static void verifyPin(Player player, boolean verified) {
        int i = player.getVariables().recoveryDelay
                - NumberUtils
                .getDaysFromMillis(player.getVariables().lastPinChange);
        if (!verified) {
            player.getPacketSender().sendInterfaceConfig(14, 220, true);
            player.getPacketSender().sendInterfaceConfig(14, 225, true);// yes,
            // I
            // really
            // want
            // to do
            // that.
            player.getPacketSender().sendInterfaceConfig(14, 230, true);// No,
            // forget
            // I
            // asked.
            player.getPacketSender().sendInterfaceConfig(14, 162, true);// Do
            // you
            // really
            // wish
            // to
            // set a
            // PIN
            // on
            // your
            // bank
            // account?
            player.getPacketSender().modifyText(
                    "A PIN will be set on your account in another " + i
                            + " days.", 14, 162);
            player.getPacketSender().modifyText(
                    "Yes, I asked for this. I want this PIN.", 14, 225);
            player.getPacketSender().modifyText(
                    "No, I didn't ask for this. Cancel it.", 14, 230);
            player.getPacketSender().displayInterface(14);
            return;
        }
        player.getVariables().pinCorrect = true;
        player.getPacketSender().sendMessage("Your PIN is still pending.");
        if (player.getVariables().openStatus == 0) {
            open(player);
        } else {
            openPinSettings(player, 1);
        }
    }

    public static void displayFirstConfirmation(Player p) {
        p.getPacketSender().sendInterfaceConfig(14, 220, true);
        p.getPacketSender().sendInterfaceConfig(14, 220, true);// Big black
        // square with
        // red bars
        p.getPacketSender().sendInterfaceConfig(14, 225, true);// yes, I really
        // want to do
        // that.
        p.getPacketSender().sendInterfaceConfig(14, 230, true);// No, forget I
        // asked.
        p.getPacketSender().sendInterfaceConfig(14, 162, true);// Do you really
        // wish to set a
        // PIN on your
        // bank account?
        p.getPacketSender().modifyText(
                "Do you really wish to set a PIN on your bank account?", 14,
                162);
        p.getPacketSender().modifyText(
                "Yes, I really want a bank PIN. I will never forget it!", 14,
                225);
        p.getPacketSender().modifyText("No, I might forget it!", 14, 230);
    }

    public static void changePinDelay(Player p) {
        String s = p.getVariables().getBankPin() == null ? "But you" : "";
        String s1 = p.getVariables().getBankPin() == null ? "haven't got one..."
                : "";
        p.getVariables().recoveryDelay = p.getVariables().recoveryDelay == 3 ? 7
                : 3;
        p.getPacketSender().modifyText(
                p.getVariables().recoveryDelay + " days", 14, 160);
        p.getPacketSender().modifyText("Your recovery delay has", 14, 101);
        p.getPacketSender().modifyText(
                "now been set to " + p.getVariables().recoveryDelay + " days.",
                14, 102);
        p.getPacketSender().modifyText("", 14, 103);
        p.getPacketSender().modifyText("You would have to wait this", 14, 104);
        p.getPacketSender().modifyText("long to delete your PIN if", 14, 105);
        p.getPacketSender().modifyText("you forgot it. " + s, 14, 106);
        p.getPacketSender().modifyText(s1, 14, 107);
    }

    public static void cancelPendingPin(Player p) {
        if (p.getVariables().getBankPin() == null || !isPinPending(p)) {
            return;
        }
        p.getVariables().pinCorrect = true;
        openPinSettings(p, 4);
    }

    public static void forgotPin(Player p) {
        int i = p.getVariables().recoveryDelay
                - NumberUtils
                .getDaysFromMillis(p.getVariables().lastDeletionRequest);
        p.getInterfaceSettings().closeInterfaces(false);
        if (p.getVariables().lastDeletionRequest == 0) {
            p.getVariables().lastDeletionRequest = System.currentTimeMillis();
            i = p.getVariables().recoveryDelay
                    - NumberUtils
                    .getDaysFromMillis(p.getVariables().lastDeletionRequest);
            p.getPacketSender().modifyText(
                    "Since you don't know your PIN, it will be deleted in another "
                            + i, 212, 0);
            p.getPacketSender()
                    .modifyText(
                            "days. If you wish to cancel this change, you may do so by entering",
                            212, 1);
            p.getPacketSender()
                    .modifyText(
                            "your PIN correctly next time you attempt to use your Bank.",
                            212, 2);
            p.getPacketSender().sendChatboxInterface(212);
        } else {
            p.getPacketSender().modifyText(
                    "You have already requested that your PIN be deleted.",
                    211, 0);
            p.getPacketSender().modifyText(
                    "This will take effect after another " + i + " days.", 211,
                    1);
            p.getPacketSender().sendChatboxInterface(211);
        }
    }

    public static void changePin(Player p) {
        if (p.getVariables().getBankPin() == null
                || p.getVariables().changingPin || isPinPending(p)) {
            return;
        }
        p.getVariables().tempPin2 = new int[4];
        p.getVariables().changingPin = true;
        p.getPacketSender().sendInterfaceConfig(14, 220, true);
        p.getPacketSender().sendInterfaceConfig(14, 220, true);// Big black
        // square with
        // red bars
        p.getPacketSender().sendInterfaceConfig(14, 225, true);// yes, I really
        // want to do
        // that.
        p.getPacketSender().sendInterfaceConfig(14, 230, true);// No, forget I
        // asked.
        p.getPacketSender().sendInterfaceConfig(14, 162, true);// Do you really
        // wish to set a
        // PIN on your
        // bank account?
        p.getPacketSender().modifyText(
                "Do you really wish to change your Bank PIN?", 14, 162);
        p.getPacketSender().modifyText("Yes, I am ready for a new one.", 14,
                225);
        p.getPacketSender().modifyText(
                "No thanks, I'll stick to my current one.", 14, 230);
    }

    public static void deletePin(Player p) {
        if (p.getVariables().getBankPin() == null || isPinPending(p)) {
            return;
        }
        p.getVariables().deletePin = true;
        p.getPacketSender().sendInterfaceConfig(14, 220, true);
        p.getPacketSender().sendInterfaceConfig(14, 220, true);// Big black
        // square with
        // red bars
        p.getPacketSender().sendInterfaceConfig(14, 225, true);// yes, I really
        // want to do
        // that.
        p.getPacketSender().sendInterfaceConfig(14, 230, true);// No, forget I
        // asked.
        p.getPacketSender().sendInterfaceConfig(14, 162, true);// Do you really
        // wish to set a
        // PIN on your
        // bank account?
        p.getPacketSender().modifyText(
                "Do you really wish to delete your Bank PIN?", 14, 162);
        p.getPacketSender().modifyText("Yes, I don't need a PIN anymore.", 14,
                225);
        p.getPacketSender().modifyText(
                "No thanks, I'd rather keep the extra security.", 14, 230);
    }
}
