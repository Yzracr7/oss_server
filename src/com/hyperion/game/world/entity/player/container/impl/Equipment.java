package com.hyperion.game.world.entity.player.container.impl;

import com.hyperion.game.Constants;
import com.hyperion.game.content.Greegree;
import com.hyperion.game.content.Skillcape;
import com.hyperion.game.content.minigames.duelarena.DuelArenaData;
import com.hyperion.game.item.Item;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.item.RequirementHandler;
import com.hyperion.game.world.entity.combat.MainCombat;
import com.hyperion.game.world.entity.combat.WeaponInterfaces;
import com.hyperion.game.world.entity.combat.data.magic.StaffsInterface;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.player.AttackInterfaceConfig;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.utility.TextUtils;

public class Equipment {

    public static final int SIZE = 14;

    public enum EQUIPMENT {
        HELMET,
        CAPE,
        AMULET,
        WEAPON,
        PLATE,
        SHIELD,
        UNKOWN,
        LEGS,
        UNKOWN2,
        GLOVES,
        BOOTS,
        UNKOWN3,
        RING,
        ARROW;

        EQUIPMENT() {
        }
    }

    public static boolean equipItem(Player player, int itemId, int slot, boolean screen) {
        Item invItem = player.getInventory().get(slot);
        if (invItem == null) {
            return false;
        }
        ItemDefinition definition = ItemDefinition.forId(itemId);
        if (definition == null) {
            return false;
        }
        if (definition.getEquipmentSlot() == -1) {
            return false;
        }
        int wep = player.getEquipment().getItemInSlot(3);
        int shield = player.getEquipment().getItemInSlot(5);
        int s = definition.getEquipmentSlot();
        if (s == -1) {
            return false;
        }
        int amount = player.getInventory().getAmountInSlot(slot);
        boolean stackable = definition.isStackable();
        boolean twoHanded = definition.isDoubleHanded();
        if (definition.getEquipmentSlot() == -1) {
            player.getPacketSender().sendMessage("You can't wear that.");
            return false;
        }

        if (!checkSkills(player, itemId)) {
            return false;
        }
        if (hasQuestRequirement(player, itemId)) {
            return false;
        }
        if (!checkDonatorRequirements(player, itemId)) {
            return false;
        }
        if (duelRuleActive(player, s, twoHanded)) {
            return false;
        }
        if (twoHanded) {
            if (player.getInventory().freeSlots() < getNeeded2HSlots(player)) {
                player.getPacketSender().sendMessage("Not enough space in your inventory.");
                return false;
            }
        }
        if (!player.getInventory().deleteItem(invItem, slot)) {
            return false;
        }
        if (twoHanded && shield != -1) {
            if (!unequipItem(player, -1, 5, screen)) {
                return false;
            }
        }
        if (s == 5) {
            if (wep != -1) {
                if (ItemDefinition.forId(wep).isDoubleHanded()) {
                    if (!unequipItem(player, -1, 3, screen)) {
                        return false;
                    }
                }
            }
        }
        Item equipItem = player.getEquipment().get(s);
        if (equipItem != null) {
            if (equipItem.getId() != itemId) {
                if (!player.getInventory().addItem(equipItem.getId(), equipItem.getCount(), slot)) {
                    return false;
                }
            } else if (stackable && equipItem.getId() == itemId) {
                amount = equipItem.getCount() + amount;
            } else {
                player.getInventory().addItem(equipItem.getId(), equipItem.getCount(), slot);
            }
        }
        player.getEquipment().set(new Item(invItem.getId(), amount), s, false);
        player.getEquipment().refresh();
        player.getInventory().refresh();
        if (screen) {
            player.getPacketSender().sendItems(-1, 1, 98, player.getInventory().getItems());
            player.getPacketSender().sendItems(465, 103, 95, player.getEquipment().getItems());
        }
        if (s == EQUIPMENT.CAPE.ordinal())
            player.getPacketSender().sendConfig(313, (Skillcape.isCape(invItem.getId()) ? 511 : 255));
        player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
        if (s == EQUIPMENT.WEAPON.ordinal()) {
            Greegree.handleGreegree(player, invItem.getId(), true);
            setWeapon(player, true);
            StaffsInterface.cancel(player, true);
        }
        if (player.getCombatState().getTarget() != null) {
            MainCombat.endCombat(player, 1);
            //TODO: check old wep combat timer vs new one
            //player.getCombatState().deductCombatCycles(1); //Switching
        }
        player.getBonuses().refresh();
        return true;
    }


    public static boolean unequipItem(Player p, int itemId, int slot, boolean screen) {
        return unequip(p, itemId, slot, screen, false);
    }

    public static boolean unequipToBank(Player p, int itemId, int slot, boolean screen) {
        return unequip(p, itemId, slot, screen, true);
    }

    private static boolean unequip(Player p, int itemId, int slot, boolean screen, boolean forBankDeposit) {
        Item equipped_item = p.getEquipment().get(slot);
        if (equipped_item == null) {
            return false;
        }
        if (itemId != -1) {
            if (equipped_item.getId() != itemId) {
                System.out.println("Attempted to unequip item but ID did not match - equipped: " + equipped_item.getId() + ", packet sent: " + itemId);
                return false;
            }
        }
        if (p.getCombatState().isDead()) {
            return false;
        }
        if (p.getAttributes().isSet("stopActions")) {
            return false;
        }
        if (!forBankDeposit) {
            if (p.getInventory().addItem(equipped_item)) {
                p.getInventory().refresh();
                p.getEquipment().set(null, slot, false);
                p.getEquipment().refresh();
                if (screen) {
                    p.getPacketSender().sendItems(-1, 1, 98, p.getInventory().getItems());
                    p.getPacketSender().sendItems(465, 103, 95, p.getEquipment().getItems());
                }
                if (slot == EQUIPMENT.CAPE.ordinal())
                    p.getPacketSender().sendConfig(313, (Skillcape.isCape(equipped_item.getId()) ? 255 : 511));
                p.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
                p.getBonuses().refresh();
                p.setInteractingEntity(null);
                if (slot == EQUIPMENT.WEAPON.ordinal()) {
                    Greegree.handleGreegree(p, equipped_item.getId(), false);
                    setWeapon(p, true);
                }
                if (p.getCombatState().getTarget() != null) {
                    MainCombat.endCombat(p, 1);
                    StaffsInterface.cancel(p, true);
                }
                return true;
            }
        } else {
            if (Bank.depositFromEquip(p, slot, equipped_item.getCount(), false)) {
                p.getEquipment().set(null, slot, false);
                p.getEquipment().refresh();
                if (screen) {
                    p.getPacketSender().sendItems(-1, 1, 98, p.getInventory().getItems());
                    p.getPacketSender().sendItems(465, 103, 95, p.getEquipment().getItems());
                }
                if (slot == EQUIPMENT.CAPE.ordinal())
                    p.getPacketSender().sendConfig(313, (Skillcape.isCape(equipped_item.getId()) ? 255 : 511));
                p.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
                p.getBonuses().refresh();
                p.setInteractingEntity(null);
                if (slot == EQUIPMENT.WEAPON.ordinal()) {
                    Greegree.handleGreegree(p, equipped_item.getId(), false);
                    setWeapon(p, true);
                }
                if (p.getCombatState().getTarget() != null) {
                    MainCombat.endCombat(p, 1);
                    StaffsInterface.cancel(p, true);
                }
                return true;
            }
        }
        return false;
    }

    public static void setWeapon(Player player, boolean set_interface_configs) {
        int tabId = 86;
        Item item = player.getEquipment().get(3);
        if (item == null) {
            player.getPacketSender().sendTab(tabId, 92);
            player.getPacketSender().modifyText("Unarmed", 92, 0);
            AttackInterfaceConfig.setButtonForAttackStyle(player, 92);
            return;
        } else {
            player.getPacketSender().modifyText(item.getDefinition().getName(), 92, 0);
        }
        ItemDefinition definition = item.getDefinition();
        if (definition == null) {
            return;
        }
        String weapon = definition.getName();
        WeaponInterfaces.WeaponInterface weapon_interface = null;
        for (int index = 0; index < WeaponInterfaces.WeaponInterface.values().length; index++) {
            if (WeaponInterfaces.WeaponInterface.values()[index] != null) {
                String weapon_interface_name = WeaponInterfaces.WeaponInterface.values()[index].toString();
                weapon_interface_name = (weapon_interface_name.replace("_", " "));
                weapon_interface_name = (weapon_interface_name.replace("'", ""));
                String weapon_name = (weapon.replace("_", " "));
                weapon_name = (weapon_name.replace("'", ""));
                if (weapon_name.equalsIgnoreCase(weapon_interface_name) || weapon_name.toLowerCase().contains(weapon_interface_name.toLowerCase())) {
                    weapon_interface = WeaponInterfaces.WeaponInterface.values()[index];
                }
            }
        }
        if (weapon_interface == null) {
            return;
        }
        int interfaceId = weapon_interface.getInterfaceId();
        if (interfaceId != -1) {
            player.getAttributes().set("attackInterface", interfaceId);
        }
        if (set_interface_configs) {
           int child = weapon_interface.getNameLineId();
            player.getPacketSender().sendTab(tabId, interfaceId);
            boolean set = weapon_interface.getSpecialBar() > 0;
            AttackInterfaceConfig.setButtonForAttackStyle(player, interfaceId);
            player.getPacketSender().sendInterfaceConfig(interfaceId, child, set);
            player.getSettings().setUsingSpecial(false);
            player.getSettings().refreshBar();
        }
        player.getPacketSender().sendTab(tabId, weapon_interface.getInterfaceId());
        player.getPacketSender().modifyText(weapon, weapon_interface.getInterfaceId(), 0);
    }

    private static int getNeeded2HSlots(Player player) {
        int shield = player.getEquipment().getItemInSlot(5);
        int weapon = player.getEquipment().getItemInSlot(3);
        if ((shield != -1 && weapon == -1) || (shield == -1 && weapon != -1) || (shield == -1 && weapon == -1)) {
            return 0;
        }
        return 1;
    }

    private static boolean checkSkills(Player player, int item) {
        RequirementHandler.Requirement[] lvl = RequirementHandler.getItemRequirements(item);
        if (lvl != null) {
            for (int index = 0; index < 23; index++) {
                if (player.getSkills().getLevelForXp(index) < lvl[index].getLevel()) {
                    player.getPacketSender().sendMessage("You're not high enough level to use this item.");
                    player.getPacketSender().sendMessage("You need " + lvl[index].getLevel() + " " + Skills.SKILL_NAME[index] + " to wear this item.");
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean hasQuestRequirement(Player player, int item) {
        for (String owner : Constants.OWNERS) {
            if (/*item == 4587 || */(item >= 4024 && item <= 4031) && !player.getName().toLowerCase().equals(owner)) {
                if (!player.getVariables().isDemon_killed()) {
                    player.getPacketSender().sendMessage("You must complete Monkey Madness to wield this item");
                    return true;
                }
            }
        }
        if (item == 9813 || item == 9814)
            if (player.getVariables().getQuestPoints() < Constants.QUEST_POINTS) {
                player.getPacketSender().sendMessage("You must have " + Constants.QUEST_POINTS + "to wear this cape.");
                return true;
            }
        return false;
    }


    private static boolean checkDonatorRequirements(Player player, int item) {
        if (player.getDetails().isLegendaryDonator())
            return true;

        if ((item >= 9069 && item <= 9074 || (item == 8957 || item == 8964)) && !player.getDetails().isLegendaryDonator()) {
            player.getPacketSender().sendMessage("You must be a Legendary donator to wear this item.");
            return false;
        } else if (player.getDetails().isLegendaryDonator())
            return true;

        if ((item >= 9069 && item <= 9074 || (item == 8954 || item == 8961)) && !player.getDetails().isExtremeDonator()) {
            player.getPacketSender().sendMessage("You must be an Extreme donator to wear this item.");
            return false;
        } else if (player.getDetails().isExtremeDonator())
            return true;

        if ((item == 8952 || item == 8959) && !player.getDetails().isSuperDonator()) {
            player.getPacketSender().sendMessage("You must be a Super donator to wear this item.");
            return false;
        } else if (player.getDetails().isSuperDonator())
            return true;

        if ((item == 8953 || item == 8960) && !player.getDetails().isRegularDonator()) {
            player.getPacketSender().sendMessage("You must be a Regular donator to wear this item.");
            return false;
        }

        return true;
    }

    private static boolean duelRuleActive(Player player, int s, boolean twoHanded) {
        if (player.getDuelSession() == null) {
            return false;
        }
        String state = player.getDuelSession().getState().name();
        System.out.println("state: " + state);
        if (state.contains("COMMENCE") || state.contains("COUNT")) {
            for (int i = 0; i < DuelArenaData.DUEL_ARENA_GEAR_RULES.length; i++) {
                if (player.getDuelSession().ruleEnabled(DuelArenaData.DUEL_ARENA_GEAR_RULES[i])
                        && s == DuelArenaData.DUEL_ARENA_GEAR_SLOTS[i]) {
                    String name = TextUtils.formatName(EQUIPMENT.values()[s].name().toLowerCase());
                    if (name == "Legs")
                        name = "Platelegs";
                    else if (name != "Gloves")
                        name = name + "s";
                    player.getPacketSender().sendMessage(name + " have been disabled in this duel.");
                    return true;

                }/* else if (s == 3 && player.getDuelSession().ruleEnabled(16) && twoHanded) {
                    player.getPacketSender().sendMessage("You cannot equip two-handed weapons in this duel.");
					return true;
				}*/
            }
        }
            /*int[] slot = {2, 3, 4, 5, 7, 9, 10, 12, 13, 0, 1};
            int[] rule = {13, 14, 15, 16, 17, 18, 19, 20, 21, 11, 12};
			for (int j = 0; j < rule.length; j++) {
				if (player.getDuelSession().ruleEnabled(rule[j])) {
					if (j == 20) {
						continue;
					}
					if (s == slot[j]) {
						player.getPacketSender().sendMessage("You cannot equip that item in this duel.");
						return true;
					}
				}
			}*/
        return false;
    }

    public static void displayEquipmentScreen(Player p) {
        if (!p.getCombatState().isOutOfCombat("You can't do this during combat!", true)) {
            return;
        }
        if (p.getAttributes().isSet("stopActions")) {
            return;
        }
        p.getWalkingQueue().reset();
        p.getPacketSender().clearMapFlag();
        Object[] opts = new Object[]{"", "", "", "", "", "", "", "", "Wear<col=ff9040>", -1, 0, 7, 4, 98, 22020096};
        p.getPacketSender().displayInterface(465);
        p.getBonuses().refresh();
        p.getPacketSender().displayInventoryInterface(336);
        p.getPacketSender().sendClientScript(150, opts, "IviiiIsssssssss");
        p.getPacketSender().sendAccessMask(1278, 336, 0, 0, 28);
        p.getPacketSender().sendItems(-1, 1, 98, p.getInventory().getItems());
        p.getPacketSender().sendItems(465, 103, 95, p.getEquipment().getItems());
    }
}
