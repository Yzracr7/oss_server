package com.hyperion.game.world.entity.player.container.impl;

public class Inventory {

	/**
	 * The size of the inventory container.
	 */
	public static final int SIZE = 28;
}
