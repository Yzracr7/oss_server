package com.hyperion.game.world.entity.player.container.impl;

import com.hyperion.game.Constants;
import com.hyperion.game.item.Item;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.game.world.entity.player.container.Container;
import com.hyperion.utility.TextUtils;
import com.hyperion.utility.logger.Log;

/**
 * @author Nando
 */
public class TradeSession {

    private enum TRADE_STATE {
        FIRST_SCREEN_UNACCEPTED,
        FIRST_SCREEN_ACCEPTED,
        SECOND_SCREEN_UNACCEPTED,
        SECOND_SCREEN_ACCEPTED
    }

    private Player player1, player2;
    private TRADE_STATE state = TRADE_STATE.FIRST_SCREEN_UNACCEPTED;
    private boolean tradeModified;


    public TradeSession(Player p1, Player p2) {
        this.player1 = p1;
        this.player2 = p2;
        open();
    }

    private void open() {
        if (player1.getDetails().isIronman()) {
            player1.getPacketSender().sendChatboxDialogue(true, "As an ironman, you cannot do this.");
            player2.getPacketSender().sendChatboxDialogue(true, "You can't trade an ironman.");
            player1.setTradeSession(null);
            player2.setTradeSession(null);
            return;
        }
        if (player2.getDetails().isIronman()) {
            player1.getPacketSender().sendChatboxDialogue(true, "You can't trade an ironman.");
            player2.getPacketSender().sendChatboxDialogue(true, "As an ironman, you cannot do this.");
            player1.setTradeSession(null);
            player2.setTradeSession(null);
            return;
        }
        player1.getPacketSender().displayInterface(335);
        player1.getPacketSender().displayInventoryInterface(336);
        player1.getPacketSender().sendClientScript(150, new Object[]{-2, 0, 7, 4, 80, 21954610}, "iiiiii");
        player1.getPacketSender().sendClientScript(150, new Object[]{"", "", "", "", "Remove-X", "Remove-All", "Remove-10", "Remove-5", "Remove", -1, 0, 7, 4, 81, 21954608}, "IviiiIsssssssss");
        player1.getPacketSender().sendClientScript(150, new Object[]{"", "", "", "", "Offer-X", "Offer-All", "Offer-10", "Offer-5", "Offer", -1, 0, 7, 4, 82, 22020096}, "IviiiIsssssssss");
        player1.getPacketSender().modifyText("", 335, 56);
        player1.getPacketSender().sendAccessMask(1278, 335, 48, 0, 28);
        player1.getPacketSender().sendAccessMask(1026, 335, 50, 0, 28);
        player1.getPacketSender().sendAccessMask(1278, 336, 0, 0, 28);
        player1.getPacketSender().modifyText("Trading with: " + player2.getDetails().getName(), 335, 16);
        player1.getPacketSender().modifyText(player2.getDetails().getName() + " has " + player2.getInventory().freeSlots() + " free inventory slots.", 335, 20);
        refresh();
        player1.getVariables().setCloseInterfacesEvent(new Runnable() {
            @Override
            public void run() {
                declinedTrade();
            }
        });
    }

    private void refresh() {
        player1.getPacketSender().modifyText(player2.getDetails().getName() + " has " + player2.getInventory().freeSlots() + " free inventory slots.", 335, 20);
        player2.getPacketSender().modifyText(player1.getDetails().getName() + " has " + player1.getInventory().freeSlots() + " free inventory slots.", 335, 20);
        player1.getPacketSender().sendItems(-1, 1, 81, getTradeContainer().getItems());
        player2.getPacketSender().sendItems(-2, 335 << 16 | 50, 80, getTradeContainer().getItems());
        player1.getPacketSender().sendItems(-1, 1, 82, player1.getInventory().getItems());
        player1.getInventory().refresh();
        player1.getPacketSender().closeChatboxInterface();
    }

    public boolean tradeItem(int invItemId, int slot, int amount) {
        Item item = player1.getInventory().getSlot(slot);
        if (item == null) {
            return false;
        }
        if (invItemId != item.getId()) {
            return false;
        }
        int itemId = item.getId();
        boolean stackable = ItemDefinition.forId(itemId) == null ? false : ItemDefinition.forId(itemId).isStackable();
        int tradeSlot = getTradeContainer().findItem(itemId);
        if (amount <= 0 || !allowedToWithdrawOrDeposit()) {
            return false;
        }
        if (!player1.getDetails().hasGroupRights(Rights.OWNER) && (ItemDefinition.forId(itemId).isPlayerBound() || !ItemDefinition.forId(itemId).isTradeable())) {
            player1.getPacketSender().sendMessage("You cannot trade that item.");
            return false;
        }
        if (!stackable) {
            tradeSlot = getTradeContainer().findFreeSlot();
            if (tradeSlot == -1) {
                return false;
            }
            for (int i = 0; i < amount; i++) {
                tradeSlot = getTradeContainer().findFreeSlot();
                if (!player1.getInventory().deleteItem(itemId) || tradeSlot == -1) {
                    break;
                }
                getTradeContainer().addItem(itemId, 1);
            }
            if (hasAccepted() || player2.getTradingSession().hasAccepted()) {
                this.state = TRADE_STATE.FIRST_SCREEN_UNACCEPTED;
                player2.getTradingSession().setState(TRADE_STATE.FIRST_SCREEN_UNACCEPTED);
                player1.getPacketSender().modifyText("", 335, 56);
                player2.getPacketSender().modifyText("", 335, 56);
            }
            refresh();
            return true;
        } else if (stackable) {
            if (amount > player1.getInventory().getAmountInSlot(slot)) {
                amount = player1.getInventory().getAmountInSlot(slot);
            }
            tradeSlot = getTradeContainer().findItem(itemId);
            if (tradeSlot == -1 || getTradeContainer().getAmountInSlot(tradeSlot) + amount > Constants.MAX_ITEMS) {
                tradeSlot = getTradeContainer().findFreeSlot();
                if (tradeSlot == -1) {
                    return false;
                }
            }
            Item itemToTrade = new Item(item.getId(), amount);
            if (player1.getInventory().deleteItem(itemToTrade, slot)) {
                getTradeContainer().addItem(itemToTrade);
                if (hasAccepted() || player2.getTradingSession().hasAccepted()) {
                    this.state = TRADE_STATE.FIRST_SCREEN_UNACCEPTED;
                    player2.getTradingSession().setState(TRADE_STATE.FIRST_SCREEN_UNACCEPTED);
                    player1.getPacketSender().modifyText("", 335, 56);
                    player2.getPacketSender().modifyText("", 335, 56);
                }
                refresh();
                return true;
            }
        }
        return false;
    }

    public void removeItem(int tradeItemId, int slot, int amount) {
        Item item = getTradeContainer().getSlot(slot);
        if (!allowedToWithdrawOrDeposit()) {
            return;
        }
        if (item == null) {
            return;
        }
        if (tradeItemId != item.getId()) {
            return;
        }
        int itemId = item.getId();
        int tradeSlot = getTradeContainer().findItem(itemId);
        boolean stackable = ItemDefinition.forId(itemId).isStackable();
        Item removeItem = new Item(itemId, item.getCount());
        if (tradeSlot == -1 || removeItem == null) {
            return;
        }
        if (!stackable) {
            Item itemToInv = new Item(removeItem.getId(), 1);
            for (int i = 0; i < amount; i++) {
                tradeSlot = getTradeContainer().findItem(itemId);
                if (tradeSlot == -1) {//TODO - might have to add check if we can delete the trading item from trade container.
                    break;
                }
                if (player1.getInventory().addItem(itemToInv)) {
                    getTradeContainer().set(null, tradeSlot, false);
                    player1.getPacketSender().tradeWarning2(tradeSlot);
                    player2.getPacketSender().tradeWarning(tradeSlot);
                }
            }
            if (hasAccepted() || player2.getTradingSession().hasAccepted()) {
                this.state = TRADE_STATE.FIRST_SCREEN_UNACCEPTED;
                player2.getTradingSession().setState(TRADE_STATE.FIRST_SCREEN_UNACCEPTED);
                player1.getPacketSender().modifyText("", 335, 56);
                player2.getPacketSender().modifyText("", 335, 56);
            }
            refresh();
        } else {
            if (amount > removeItem.getCount()) {
                amount = removeItem.getCount();
            }
            Item itemToInv = new Item(removeItem.getId(), amount);
            if (player1.getInventory().addItem(itemToInv)) {
                removeItem.setItemAmount(removeItem.getCount() - amount);
                Item finalItem = null;
                if (removeItem.getCount() > 0) {
                    finalItem = new Item(removeItem.getId(), removeItem.getCount());
                }
                getTradeContainer().set(finalItem, slot, false);
                player1.getPacketSender().tradeWarning2(slot);
                player2.getPacketSender().tradeWarning(slot);
            }
        }
        if (hasAccepted() || player2.getTradingSession().hasAccepted()) {
            this.state = TRADE_STATE.FIRST_SCREEN_UNACCEPTED;
            player2.getTradingSession().setState(TRADE_STATE.FIRST_SCREEN_UNACCEPTED);
            player1.getPacketSender().modifyText("", 335, 56);
            player2.getPacketSender().modifyText("", 335, 56);
        }
        refresh();
        tradeModified = true;
    }

    public void accept() {
        TradeSession trade = player1.getTradingSession();
        if (trade == null || player2 == null || player2.getTradingSession() == null) {
            System.out.println("Accept trade null");
            return;
        }
        long finalAmount = 0;
        if (state.equals(TRADE_STATE.FIRST_SCREEN_UNACCEPTED)) {
            for (int i = 0; i < trade.getTradeContainer().getItems().length; i++) {
                finalAmount = 0;
                if (trade.getTradeContainer().get(i) != null) {
                    long tradeAmount = trade.getTradeContainer().get(i).getCount();
                    long p2InvenAmount = player2.getInventory().getAmountInSlot(i);
                    finalAmount = (long) (tradeAmount + p2InvenAmount);
                    if (finalAmount >= Integer.MAX_VALUE) {
                        player1.getPacketSender().sendMessage("Other player has too many of item: " + ItemDefinition.forId(trade.getTradeContainer().get(i).getId()).getName() + ".");
                        return;
                    }
                }
            }
            for (int i = 0; i < trade.getTradeContainer().getItems().length; i++) {
                finalAmount = 0;
                if (player2.getTradingSession().getTradeContainer().get(i) != null) {
                    long p2tradeAmount = player2.getTradingSession().getTradeContainer().getAmountInSlot(i);
                    long invenAmount = player1.getInventory().getAmountInSlot(i);
                    finalAmount = (long) (p2tradeAmount + invenAmount);
                    if (finalAmount >= Integer.MAX_VALUE) {
                        player1.getPacketSender().sendMessage("You have too many of item: " + ItemDefinition.forId(player2.getTradingSession().getTradeContainer().getSlot(i).getId()).getName() + ".");
                        return;
                    }
                }
            }
            if (player2.getTradingSession().getTradeContainer().getTotalItems() > player1.getInventory().freeSlots()) {
                player1.getPacketSender().sendMessage("You don't have enough inventory space for this trade.");
                return;
            }
            if (trade.getTradeContainer().getTotalItems() > player2.getInventory().freeSlots()) {
                player1.getPacketSender().sendMessage("Other player doesn't have enough inventory space for this trade.");
                return;
            }
            this.state = TRADE_STATE.FIRST_SCREEN_ACCEPTED;
        }
        if (state.equals(TRADE_STATE.FIRST_SCREEN_ACCEPTED)) {
            player1.getPacketSender().modifyText("Waiting for other player...", 335, 56);
            player2.getPacketSender().modifyText("Other player has accepted...", 335, 56);
            if (player2.getTradingSession().getState().equals(TRADE_STATE.FIRST_SCREEN_ACCEPTED)) {
                displayConfirmation();
                player2.getTradingSession().displayConfirmation();
            }
            return;
        }
        if (state.equals(TRADE_STATE.SECOND_SCREEN_UNACCEPTED)) {
            this.state = TRADE_STATE.SECOND_SCREEN_ACCEPTED;
            player1.getPacketSender().modifyText("Waiting for other player...", 334, 33);
            player2.getPacketSender().modifyText("Other player has accepted...", 334, 33);
            if (player2.getTradingSession().getState().equals(TRADE_STATE.SECOND_SCREEN_ACCEPTED)) {
                boolean empty = TextUtils.getTradeItemLogDisplay(player1).equalsIgnoreCase("nothing");
                if (!empty) {
                    World.getWorld().loggingThread.appendLog(
                            new Log(player1.getName(),
                                    "Traded " + TextUtils.getTradeItemLogDisplay(player1) +
                                            ", to player " + player2.getName() +
                                            " for " + TextUtils.getTradeItemLogDisplay(player2)));
                    World.getWorld().loggingThread.appendLog(
                            new Log(player2.getName(),
                                    "Traded " + TextUtils.getTradeItemLogDisplay(player2) +
                                            ", to player " + player1.getName() +
                                            " for " + TextUtils.getTradeItemLogDisplay(player1)));
                }
                player1.getVariables().setCloseInterfacesEvent(null);
                player2.getVariables().setCloseInterfacesEvent(null);
                completeTrade();
                player2.getTradingSession().completeTrade();
                player1.getInterfaceSettings().closeInterfaces(false);
                player2.getInterfaceSettings().closeInterfaces(false);
                player1.setTradeSession(null);
                player2.setTradeSession(null);
                player1.getPacketSender().sendMessage("You accept the trade.");
                player2.getPacketSender().sendMessage("You accept the trade.");
                World.getWorld().getWorldLoader().savePlayer(player1);
                World.getWorld().getWorldLoader().savePlayer(player2);
            }
        }
    }

    public void decline(boolean button_clicked) {
        if (button_clicked) {
            player1.getVariables().setCloseInterfacesEvent(null);
            player2.getVariables().setCloseInterfacesEvent(null);
            declinedTrade();
        }
        player1.getInterfaceSettings().closeInterfaces(false);
        player2.getInterfaceSettings().closeInterfaces(false);
    }

    private void declinedTrade() {
        player1.getPacketSender().sendMessage("You decline the trade.");
        player2.getPacketSender().sendMessage("Other player declined the trade.");
        giveBack();
        player1.setTradeSession(null);
        if (player2.getTradingSession() != null) {
            player2.getTradingSession().giveBack();
            player2.setTradeSession(null);
            player2.getInterfaceSettings().closeInterfaces(false);
        }
    }

    private void displayConfirmation() {
        TradeSession trade = player1.getTradingSession();
        this.state = TRADE_STATE.SECOND_SCREEN_UNACCEPTED;
        int mySize = trade.getTradeContainer().getTotalItems();
        int othersSize = player2.getTradingSession().getTradeContainer().getTotalItems();
        boolean othersSmallText = othersSize > 16;
        if (othersSmallText) {
            player1.getPacketSender().sendInterfaceConfig(334, 41, true);
            player1.getPacketSender().modifyText(player2.getTradingSession().getLeftSideSmallStrings(), 334, 41);
            player1.getPacketSender().sendInterfaceConfig(334, 42, true);
            player1.getPacketSender().modifyText(player2.getTradingSession().getRightSideSmallStrings(), 334, 42);
        } else {
            player1.getPacketSender().sendInterfaceConfig(334, 40, true);
            player1.getPacketSender().modifyText(player2.getTradingSession().getItemList(), 334, 40);
        }
        boolean mySmallText = mySize > 16;
        if (!mySmallText) {
            player1.getPacketSender().sendInterfaceConfig(334, 37, true);
            player1.getPacketSender().modifyText(getItemList(), 334, 37);
        } else {
            player1.getPacketSender().sendInterfaceConfig(334, 38, true);
            player1.getPacketSender().sendInterfaceConfig(334, 39, true);
            player1.getPacketSender().modifyText(getLeftSideSmallStrings(), 334, 38);
            player1.getPacketSender().modifyText(getRightSideSmallStrings(), 334, 39);
        }
        player1.getPacketSender().displayInterface(334);
        player1.getPacketSender().modifyText("Trading with: <br>" + player2.getDetails().getName(), 334, 44);
        if (player2.getTradingSession().isTradeModified()) {
            player1.getPacketSender().sendInterfaceConfig(334, 46, true);
        }
        player1.getPacketSender().sendClientScript(142, new Object[]{1, 7, 4, 334 << 16 | 18}, "Iiii");
        player2.getPacketSender().sendClientScript(142, new Object[]{1, 7, 4, 334 << 16 | 18}, "Iiii");
    }

    private void completeTrade() {
        Item[] p2Items = player2.getTradingSession().getTradeContainer().getItems();
        for (int i = 0; i < p2Items.length; i++) {
            if (p2Items[i] != null) {
                if (player1.getInventory().addItem(p2Items[i])) {
                    player2.getTrade().set(null, i, false);
                }
            }
        }
        player1.getInventory().refresh();
    }

    private void giveBack() {
        TradeSession session = player1.getTradingSession();
        if (session != null) {
            Container container = session.getTradeContainer();
            if (container != null) {
                Item[] items = container.getItems();
                for (int i = 0; i < items.length; i++) {
                    if (items[i] != null) {
                        if (player1.getInventory().addItem(items[i])) {
                            player1.getTrade().set(null, i, false);
                        }
                    }
                }
            }
        }
        player1.getInventory().refresh();
    }

    public String getLeftSideSmallStrings() {
        Item[] items = player1.getTradingSession().getTradeContainer().getItems();
        String leftSide = "";
        for (int i = 0; i < 16; i++) {
            if (items[i] != null) {
                leftSide = leftSide + "<col=FF9040>" + items[i].getDefinition().getName();
                if (items[i].getCount() > 1) {
                    leftSide = leftSide + "<col=FFFFFF> x <col=FFFFFF>" + items[i].getCount() + "<br>";
                } else {
                    leftSide = leftSide + "<br>";
                }
            }
        }
        if (leftSide.equals("")) {
            leftSide = "<col=FFFFFF>Absolutely nothing!";
        }
        return leftSide;
    }

    public String getRightSideSmallStrings() {
        Item[] items = player1.getTradingSession().getTradeContainer().getItems();
        String rightString = "";
        for (int i = 16; i < 28; i++) {
            if (items[i] != null) {
                rightString = rightString + "<col=FF9040>" + items[i].getDefinition().getName();
                if (items[i].getCount() > 1) {
                    rightString = rightString + "<col=FFFFFF> x <col=FFFFFF>" + items[i].getCount() + "<br>";
                } else {
                    rightString = rightString + "<br>";
                }
            }
        }
        if (rightString.equals("")) {
            rightString = "<col=FFFFFF>Absolutely nothing!";
        }
        return rightString;
    }

    public String getItemList() {
        Item[] items = player1.getTradingSession().getTradeContainer().getItems();
        String list = "";
        for (int i = 0; i < 28; i++) {
            if (items[i] != null) {
                list = list + "<col=FF9040>" + items[i].getDefinition().getName();
                if (items[i].getCount() > 1) {
                    list = list + "<col=FFFFFF> x <col=FFFFFF>" + items[i].getCount() + "<br>";
                } else {
                    list = list + "<br>";
                }
            }
        }
        if (list.equals("")) {
            list = "<col=FFFFFF>Absolutely nothing!";
        }
        return list;
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public Container getTradeContainer() {
        return player1.getTrade();
    }

    public boolean isTradeModified() {
        return tradeModified;
    }

    private boolean allowedToWithdrawOrDeposit() {
        return state.equals(TRADE_STATE.FIRST_SCREEN_UNACCEPTED) || state.equals(TRADE_STATE.FIRST_SCREEN_ACCEPTED);
    }

    private boolean hasAccepted() {
        return state.equals(TRADE_STATE.SECOND_SCREEN_ACCEPTED) || state.equals(TRADE_STATE.FIRST_SCREEN_ACCEPTED);
    }

    public TRADE_STATE getState() {
        return state;
    }

    public void setState(TRADE_STATE state) {
        this.state = state;
    }
}
