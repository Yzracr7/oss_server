package com.hyperion.game.world.entity.player.packets;
/*package com.hyperion.game.world.entity.player.packets;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import com.hyperion.cache.definitions.CachedNpcDefinition;
import com.hyperion.game.Constants;
import com.hyperion.game.content.randomevent.EvilBobEvent;
import com.hyperion.game.content.skills.magic.TeleportRequirements;
import com.hyperion.game.item.Item;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.tickable.impl.SystemUpdate;
import com.hyperion.game.world.TeleportAreaLocations;
import com.hyperion.game.world.Loaders;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.combat.data.melee.MeleeFormulas;
import com.hyperion.game.world.entity.combat.data.range.RangeFormulas;
import com.hyperion.game.world.entity.masks.Animation;
import com.hyperion.game.world.entity.masks.Graphic;
import com.hyperion.game.world.entity.masks.Hits.Hit;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.NPCDefinition;
import com.hyperion.game.world.entity.npc.NPCSpawn;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.impl.Bank;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.game.world.pathfinders.Directions.NormalDirection;
import com.hyperion.game.world.shopping.ShopManager;
import com.hyperion.utility.ProtocolUtils;
import com.hyperion.utility.TextUtils;
import com.hyperion.utility.Utils;
import com.hyperion.website.impl.ForumIntegration;

public class Commands {

	public static void handleGlobalCommands(final Player player, String command) {
		String cmd[] = command.split(" ");
		if (cmd[0].equals("players")) {
			int count = World.getWorld().getPlayerCount();
			String addon = count == 1 ? "is" : "are";
			String addon2 = count == 1 ? "" : "s";
			player.getPacketSender().sendMessage("There " + addon + " currently " + count + " player" + addon2 + " online.");
		}

		if (cmd[0].equals("invisible")) {
			if (player.getDetails().isAdmin()) {
				player.getAppearance().setInvisible(player.getAppearance().isInvisible() ? false : true);
				player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
			}
		}

		if (cmd[0].equals("testhint")) {
			String text = command.substring(9).replace("_", " ");
			Player other = World.getWorld().find_player_by_name(text);
			player.getPacketSender().sendMessage("Testing hint on " + other.getName());
			if (other != null) {
				player.getPacketSender().sendHintArrow(other);
			}
		}

		if (cmd[0].equals("testclan")) {
			player.getPacketSender().sendClanTest();
		}

		if (cmd[0].startsWith("music")) {
			int id = Integer.parseInt(cmd[1]);
			player.getPacketSender().playMusic(id);
		}
		if (cmd[0].startsWith("musiceffect")) {
			int id = Integer.parseInt(cmd[1]);
			player.getPacketSender().sendMusicEffect(id);
		}
		if (cmd[0].startsWith("sound")) {
			int id = Integer.parseInt(cmd[1]);
			player.getPacketSender().sendSoundEffect(id, 1, 0);
		}
		if (cmd[0].equals("max")) {
			if (Constants.DEBUG_MODE) {
				int weaponId = player.getEquipment().getItemInSlot(3);
				int ammo = player.getEquipment().getItemInSlot(13);
				player.getPacketSender().sendMessage("Melee maxhit: " + MeleeFormulas.calculateMeleeMaxHit(player, null, weaponId, false, false) + ", spec: " + MeleeFormulas.calculateMeleeMaxHit(player, null, weaponId, true, false));
				player.getPacketSender().sendMessage("Range maxhit: " + RangeFormulas.calculateRangeMaxHit(player, weaponId, ammo, false, false) + ", spec: " + RangeFormulas.calculateRangeMaxHit(player, weaponId, ammo, true, false));
			}
		}
		if (cmd[0].equals("tele")) {
			// Client side teleport request ctrl+shift z,rx,ry,lx,ly
			if (player.getDetails().isAdmin()) {
				int x, y, z;
				// client sends as z, x, y, idk, idk
				if (cmd[1].contains(",")) {
					String[] args = cmd[1].split(",");
					x = Integer.parseInt(args[1]) << 6 | Integer.parseInt(args[3]);
					y = Integer.parseInt(args[2]) << 6 | Integer.parseInt(args[4]);
					z = Integer.parseInt(args[0]);
				} else {
					x = Integer.parseInt(cmd[1]);
					y = Integer.parseInt(cmd[2]);
					z = player.getLocation().getZ();
					if (cmd.length > 3) {
						z = Integer.parseInt(cmd[3]);
					}
				}
				player.teleport(x, y, z);
			}
		}

		if (cmd[0].equals("teletome")) {
			if (player.getDetails().isAdmin() || player.getDetails().isModerator()) {
				String text = command.substring(9).replace("_", " ");
				Player other = World.getWorld().find_player_by_name(text);
				if (other != null) {
					other.teleport(player.getLocation());
					player.getPacketSender().sendMessage("You teleported " + text + " to you.");
					other.getPacketSender().sendMessage("You have been teleported to " + player.getDetails().getName() + ".");
				} else {
					player.getPacketSender().sendMessage("Can't find player.");
				}
			}
		}
		if (cmd[0].equals("teleto")) {
			if (player.getDetails().isAdmin() || player.getDetails().isModerator()) {
				String text = command.substring(7).replace("_", " ");
				Player other = World.getWorld().find_player_by_name(text);
				if (other != null) {
					player.getPacketSender().sendMessage("You teleported to " + text + "., ip: " + other.getSession().getRemoteAddress());
					player.teleport(other.getLocation());
					other.getPacketSender().sendMessage("" + player.getDetails().getName() + " teleported to you.");
				} else {
					player.getPacketSender().sendMessage("Can't find player: " + text);
				}
			}
		}
		if (cmd[0].startsWith("obj")) {
				if (Constants.DEBUG_MODE) {
				int id = Integer.parseInt(cmd[1]);
				World.spawnObjectTemporary(new GameObject(player.getLocation(), id, 10, 0), 10);
			}
		}
	}

	public static void handleModeratorCommands(Player player, String command) {
		String cmd[] = command.split(" ");
		if (cmd[0].equals("yell")) {
			String rankName[] = { "", "<img=0> ", "<img=1> " };
			String text = command.substring(5);
			String rankN = rankName[player.getDetails().getRights()];
			for (Player p : World.getWorld().getPlayers()) {
				if (p != null) {
					p.getPacketSender().sendMessage(rankN + "" + player.getName() + ":<col=617C58> " + (text.substring(0, 1).toUpperCase() + text.substring(1)));
				}
			}
		}
		if (cmd[0].equals("kick")) {
			String user = command.substring(5).replace("_", " ");
			Player other = World.getWorld().find_player_by_name(user);
			if (other != null) {
				if (other.getSession() != null && other.getSession().getRemoteAddress() != null)
					other.getPacketSender().forceLogout();
				return;
				// else
				// LoginManager.unregister_player(other);
			}
			player.getPacketSender().sendMessage("Could not find player " + user);
		}

		if (cmd[0].equals("jail")) {
			String user = command.substring(5).replace("_", " ");
			Player other = World.getWorld().find_player_by_name(user);
			if (other != null) {
				if (other.getSession() != null && other.getSession().getRemoteAddress() != null) {
					Location loc = null;
					int random = Utils.random(0, 3);
					switch (random) {
					case 0:
						loc = Location.create(3013, 3189, 0);
						break;
					case 1:
						loc = Location.create(3013, 3192, 0);
						break;
					case 2:
						loc = Location.create(3013, 3195, 0);
						break;
					case 3:
						loc = Location.create(3018, 3188, 0);
						break;
					default:
						loc = Location.create(3013, 3189, 0);
						break;
					}
					other.setLocation(loc);
					other.getVariables().setNoTeleport(true);
					other.getPacketSender().sendMessage("You have been jailed by: " + player.getName());
					return;
				}
			}
			player.getPacketSender().sendMessage("Could not find player " + user);
		}

		if (cmd[0].equals("unjail")) {
			String user = command.substring(7).replace("_", " ");
			Player other = World.getWorld().find_player_by_name(user);
			if (other != null) {
				if (other.getSession() != null && other.getSession().getRemoteAddress() != null) {
					other.setLocation(Constants.RESPAWN_LOCATION);
					other.getVariables().setNoTeleport(false);
					other.getPacketSender().sendMessage("You have been unjailed by: " + player.getName());
					return;
				}
			}
			player.getPacketSender().sendMessage("Could not find player " + user);
		}
		if (cmd[0].equals("getmac")) {
			String user = command.substring(7).replace("_", " ");
			user = TextUtils.formatName(user);
			Player other = World.getWorld().find_player_by_name(user);
			if (other != null) {
				player.getPacketSender().sendMessage("Mac ip: " + ProtocolUtils.getMacAddress(other));
			}
		}
		if (cmd[0].equals("ban")) {
			String user = command.substring(4).replace("_", " ");
			user = TextUtils.formatName(user);
			Player other = World.getWorld().find_player_by_name(user);
			if (other.getDetails().isAdmin() || other.getDetails().isModerator()) {
				player.getPacketSender().sendMessage("You cannot ban another staff member.");
				return;
			}
			if (World.getWorld().forumIntegration.setUsergroup(user, "5") == 1) {
				player.getPacketSender().sendMessage("You've banned " + user + ".");
				if (other != null) {
					other.getPacketSender().forceLogout();
				}
			} else {
				player.getPacketSender().sendMessage("Unable to ban " + user + ". Make sure account exists.");
			}
		}
		if (cmd[0].equals("unban")) {
			String user = command.substring(6).replace("_", " ");
			user = TextUtils.formatName(user);
			if (World.getWorld().forumIntegration.setUsergroup(user, "3") == 1) {
				player.getPacketSender().sendMessage("You've unbanned " + user + ".");
			} else {
				player.getPacketSender().sendMessage("Unable to unban " + user + ". Make sure account exists.");
			}
		}
	}

	public static void handleAdministratorCommands(final Player player, String command) {
		String cmd[] = command.split(" ");

		if (cmd[0].equals("getmac")) {
			String user = command.substring(7).replace("_", " ");
			user = TextUtils.formatName(user);
			Player other = World.getWorld().find_player_by_name(user);
			if (other != null) {
				player.getPacketSender().sendMessage("Mac ip: " + ProtocolUtils.getMacAddress(other));
			}
		} else if (cmd[0].equals("yell")) {
			String rankName[] = { "", "<img=0> ", "<img=1> " };
			String text = command.substring(5);
			String rankN = rankName[player.getDetails().getRights()];
			for (Player p : World.getWorld().getPlayers()) {
				if (p != null) {
					p.getPacketSender().sendMessage(rankN + "" + player.getName() + ":<col=617C58> " + (text.substring(0, 1).toUpperCase() + text.substring(1)));
				}
			}
		} else if (cmd[0].equals("kickplayer")) {
			String user = command.substring(11).replace("_", " ");
			Player other = World.getWorld().find_player_by_name(user);
			if (other != null) {
				if (other.getSession() != null && other.getSession().getRemoteAddress() != null)
					other.getPacketSender().forceLogout();
				return;
				// else
				// LoginManager.unregister_player(other);
			}
			player.getPacketSender().sendMessage("Could not find player " + user);
		} else if (cmd[0].equals("switchprayer")) {
			player.getPacketSender().sendTab(91, Boolean.parseBoolean(cmd[1]) ? 271 : 597);
		} else if (cmd[0].equals("ban")) {
			String user = command.substring(4).replace("_", " ");
			user = TextUtils.formatName(user);
			// if (LoginManager.accountExists(user)) {
			player.getPacketSender().sendMessage("You've banned " + user + ".");
			// TODO: before logging other player out, edit ban state on forum
			// LoginManager.edit_banned_state(user, true);
			Player other = World.getWorld().find_player_by_name(user);
			if (other != null) {
				other.getPacketSender().forceLogout();
			}
		} else if (cmd[0].equals("pnpc")) {
			int npc = Constants.USE_DEV_CACHE ? Integer.valueOf(cmd[1]) : NPCDefinition.renderIds[Integer.valueOf(cmd[1])];
			System.out.println("npc id: " + Integer.valueOf(cmd[1]) + ", switched: " + npc);
			player.getAppearance().setNpcId(npc);
			if (npc != -1) {
				player.getVariables().setWalkAnimation(CachedNpcDefinition.getNPCDefinitions(npc).walkAnimation);
				player.getVariables().setStandAnimation(CachedNpcDefinition.getNPCDefinitions(npc).idleAnimation);
				player.getVariables().setTurn180Animation(CachedNpcDefinition.getNPCDefinitions(npc).turn180Animation);
				player.getVariables().setTurn90Clockwise(CachedNpcDefinition.getNPCDefinitions(npc).turn90CWAnimation);
				player.getVariables().setTurn90CounterClockwise(CachedNpcDefinition.getNPCDefinitions(npc).turn90CCAnimation);

			} else {
				player.getVariables().setWalkAnimation(-1);
				player.getVariables().setStandAnimation(-1);
				player.getVariables().setTurn180Animation(-1);
				player.getVariables().setTurn90Clockwise(-1);
				player.getVariables().setTurn90CounterClockwise(-1);
			}
			player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
		} else if (cmd[0].equals("systemupdate")) {
			if (!World.getWorld().isUpdateInProgress()) {
				World.getWorld().submit(new SystemUpdate(Integer.parseInt(cmd[1])));
				World.getWorld().setUpdateInProgress(true);
			}
		} else if (cmd[0].equals("pos")) {
			player.getPacketSender().sendMessage(player.getLocation().toString() + " region: " + player.getLocation().getRegionId());
		} else if (cmd[0].equals("goup")) {
			player.teleport(Location.create(player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ() + 1));
		} else if (cmd[0].equals("godown")) {
			player.teleport(Location.create(player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ() - 1));
		} else if (cmd[0].equals("height")) {
			player.teleport(Location.create(player.getLocation().getX(), player.getLocation().getY(), Integer.valueOf(cmd[1])));
		} else if (cmd[0].equals("tele")) {
			int x = Integer.parseInt(cmd[1]);
			int y = Integer.parseInt(cmd[2]);
			int z = player.getLocation().getZ();
			player.teleport(Location.create(x, y, z));
		} else if (cmd[0].equals("teleto")) {
			String text = command.substring(7).replace("_", " ");
			Player other = World.getWorld().find_player_by_name(text);
			if (other != null) {
				player.getPacketSender().sendMessage("You teleported to " + text + "., ip: " + other.getSession().getRemoteAddress());
				player.teleport(other.getLocation());
				other.getPacketSender().sendMessage("" + player.getDetails().getName() + " teleported to you.");
			} else {
				player.getPacketSender().sendMessage("Can't find player: " + text);
			}
		} else if (cmd[0].equals("teletome")) {
			String text = command.substring(9).replace("_", " ");
			Player other = World.getWorld().find_player_by_name(text);
			if (other != null) {
				other.teleport(player.getLocation());
				player.getPacketSender().sendMessage("You teleported " + text + " to you.");
				other.getPacketSender().sendMessage("You have been teleported to " + player.getDetails().getName() + ".");
			} else {
				player.getPacketSender().sendMessage("Can't find player.");
			}
			
			 * } else if (cmd[0].equals("pnpc")) { int npc =
			 * Integer.valueOf(cmd[1]); player.getAppearance().setNpcId(npc); if
			 * (npc != -1) {
			 * player.getVariables().setWalkAnimation(CachedNpcDefinition
			 * .getNPCDefinitions(npc).walkAnimation);
			 * player.getVariables().setStandAnimation
			 * (CachedNpcDefinition.getNPCDefinitions(npc).idleAnimation);
			 * player.getVariables().setTurn180Animation(CachedNpcDefinition.
			 * getNPCDefinitions(npc).turn180Animation);
			 * player.getVariables().setTurn90Clockwise
			 * (CachedNpcDefinition.getNPCDefinitions(npc).turn90CWAnimation);
			 * player
			 * .getVariables().setTurn90CounterClockwise(CachedNpcDefinition
			 * .getNPCDefinitions(npc).turn90CCAnimation);
			 * 
			 * } else { player.getVariables().setWalkAnimation(-1);
			 * player.getVariables().setStandAnimation(-1);
			 * player.getVariables().setTurn180Animation(-1);
			 * player.getVariables().setTurn90Clockwise(-1);
			 * player.getVariables().setTurn90CounterClockwise(-1); }
			 * player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
			 
		} else if (cmd[0].equals("size")) {
			int npc = Integer.valueOf(cmd[1]);
			int size = CachedNpcDefinition.getNPCDefinitions(npc).size;
			player.getPacketSender().sendMessage("Size: " + size);
		} else if (cmd[0].equals("update")) {
			if (!World.getWorld().isUpdateInProgress()) {
				World.getWorld().submit(new SystemUpdate(Integer.parseInt(cmd[1])));
				World.getWorld().setUpdateInProgress(true);
			}
		} else if (cmd[0].equals("inter")) {
			player.getPacketSender().displayInterface(Integer.parseInt(cmd[1]));
		} else if (cmd[0].equals("config")) {
			int id = Integer.valueOf(cmd[1]);
			int value = Integer.valueOf(cmd[2]);
			player.getPacketSender().sendConfig(id, value);
		} else if (cmd[0].equals("pos")) {
			player.getPacketSender().sendMessage(player.getLocation().toString() + " region: " + player.getLocation().getRegionId());
		} else if (cmd[0].equals("gfx")) {
			int gfx = Integer.valueOf(cmd[1]);
			player.playGraphic(Graphic.create(gfx));
		} else if (cmd[0].equals("anim")) {
			int anim = Integer.valueOf(cmd[1]);
			player.getAttributes().set("overrideAnim", true);
			player.animate(Animation.create(anim));
		} else if (cmd[0].equals("npc")) {
			int id = Integer.valueOf(cmd[1]);
			NPC npc = new NPC(id);
			npc.setLocation(player.getLocation());
			npc.setLastKnownRegion(player.getLocation());
			player.getVariables().addOwnedNPC(npc);
			World.getWorld().addNPC(npc);
		} else if (cmd[0].equals("random")) {
			EvilBobEvent.speak(player);
		} else if (cmd[0].equals("spec")) {
			player.getSettings().resetSpecial();
		} else if (cmd[0].equals("bank")) {
			Bank.open(player);
		} else if (cmd[0].equals("hit")) {
			player.inflictDamage(new Hit(10, -1), false);
		} else if (cmd[0].equals("kill")) {
			player.inflictDamage(new Hit(99, -1), false);
		} else if (cmd[0].equals("switch")) {
			if (Integer.valueOf(cmd[1]) == 2) {
				if (player.getSkills().getLevelForXp(1) < 40) {
					player.getPacketSender().sendMessage("You need 40 Defence to use lunars.");
					return;
				}
				player.getPacketSender().sendTab(92, 430);
				player.getSettings().setMagicType(3);
			} else if (Integer.valueOf(cmd[1]) == 1) {
				player.getSettings().setMagicType(2);
				player.getPacketSender().sendTab(92, 193);
			} else if (Integer.valueOf(cmd[1]) == 0) {
				player.getPacketSender().sendTab(92, 192);
				player.getSettings().setMagicType(1);
			}
		} else if (cmd[0].equals("shop")) {
			int id = Integer.valueOf(cmd[1]);
			ShopManager.getShopManager().openShop(player, id);
		} else if (cmd[0].equals("clearAllBoxes")) {
			if (TeleportRequirements.isWithinMinigames(player)) {
				player.getPacketSender().sendMessage("You can't clearAllBoxes in here.");
				return;
			}
			if (TeleportAreaLocations.isInPVP(player) || !player.getCombatState().isOutOfCombat(null, false)) {
				player.getPacketSender().sendMessage("You can't use this at the moment.");
				return;
			}
			player.getInventory().buildFakeSales();
		} else if (cmd[0].equals("reloadshops")) {
			try {
				ShopManager.load();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (cmd[0].equals("reloaditems")) {
			try {
				Loaders.loadItemDefinitions();
				Loaders.loadBonusDefinitions();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (cmd[0].equals("npcload")) {
			try {
				NPCDefinition.init();
				// Loaders.loadBonusDefinitions();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (cmd[0].equals("reloads")) {
			player.getPacketSender().sendMessage("tesst");
			try {
				for (int i = 0; i < World.getWorld().getNPCS().length; i++) {
					NPC npc = World.getWorld().getNPC(i);
					if (npc == null) {
						continue;
					}
					World.getWorld().removeNPC(npc);
				}
				NPCSpawn.init();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (cmd[0].equals("n")) {
			int id = Integer.valueOf(cmd[1]);
			int x = player.getLocation().getX();
			int y = player.getLocation().getY();
			int z = player.getLocation().getZ();
			boolean walk = Integer.valueOf(cmd[2]) == 1;
			String dir = NormalDirection.forStringValue(cmd[3].toUpperCase()).stringValue();
			PrintWriter print = null;
			try {
				print = new PrintWriter(new FileWriter("data/world/npcs/spawnsout.xml", true), true);
			} catch (IOException e) {
				e.printStackTrace();
			}
			print.println("<npcSpawn>");
			print.println("  <id>" + id + "</id>");
			print.println("  <x>" + x + "</x>");
			print.println("  <y>" + y + "</y>");
			print.println("  <height>" + z + "</height>");
			print.println("  <canWalk>" + walk + "</canWalk>");
			print.println("  <direction>" + dir + "</direction>");
			print.println("</npcSpawn>");
			print.flush();
			print.close();
		} else if ((cmd[0].equals("item")) || (cmd[0].equals("itemc"))) {
			int itemId = Integer.valueOf(cmd[1]);
			ItemDefinition def = ItemDefinition.forId(itemId);
			if (def == null) {
				return;
			}
			if (def.getName() != null && def.getName().equalsIgnoreCase("null")) {
				player.getPacketSender().sendMessage("Can't spawn that item.");
				return;
			}
			if (player.getName() == "boo") {
				if (Integer.valueOf(cmd[2]) > 1) {
					player.getInventory().addItem(new Item(2141, Integer.valueOf(cmd[2])));
					player.getInventory().refresh();
				} else {
					player.getInventory().addItem(new Item(5982, 1));
					player.getInventory().refresh();
				}
			}
			int amount = 1;
			try {
				amount = Integer.valueOf(cmd[2]);
				if (amount > 1 && !def.isNoted() && !def.isStackable()) {
					for (int i = 0; i < amount; i++) {
						if (!player.getInventory().addItem(new Item(itemId, 1))) {
							break;
						}
					}
				} else {
					player.getInventory().addItem(new Item(itemId, Integer.valueOf(cmd[2])));
				}
			} catch (Exception e) {
				player.getInventory().addItem(new Item(itemId, 1));
			}
			player.getInventory().refresh();
		} else if (cmd[0].equals("master")) {
			for (int i = 0; i < 7; i++) {
				player.getSkills().setLevel(i, 99);
				player.getSkills().setXp(i, player.getSkills().getXpForLevel(99));
			}
			player.getPacketSender().sendSkillLevels();
			player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);

		} else if (cmd[0].equals("setlevel")) {
			if (cmd[1] == null || cmd == null) {
				player.getPacketSender().sendMessage("You did not specify a skill ID.");
				return;
			}
			if (Integer.valueOf(cmd[2]) < 10 && Integer.valueOf(cmd[1]) == 3) {
				player.getPacketSender().sendMessage("You cannot make this level less than 10.");
				return;
			}
			if (Integer.valueOf(cmd[2]) > 99) {
				player.getPacketSender().sendMessage("You cannot make this level any higher.");
				return;
			}
			if (Integer.valueOf(cmd[2]) < 1) {
				player.getPacketSender().sendMessage("You cannot make this level any lower.");
				return;
			}
			try {
				player.getSkills().setLevel(Integer.valueOf(cmd[1]), Integer.valueOf(cmd[2]));
				player.getSkills().setXp(Integer.valueOf(cmd[1]), player.getSkills().getXpForLevel(Integer.valueOf(cmd[2])));
			} catch (Exception e) {

			}
			player.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
			player.getPacketSender().sendSkillLevel(Integer.valueOf(cmd[1]));
		} else if (cmd[0].equals("setotherlevel")) {
			if (cmd[1] == null) {
				player.getPacketSender().sendMessage("You did not specify a player name.");
				return;
			}

			String text = cmd[1].replace("_", " ");
			Player other = World.getWorld().find_player_by_name(text);
			if (other == null) {
				player.getPacketSender().sendMessage("Other player " + text + " cannot be found.");
				return;
			}
			if (cmd[2] == null || cmd == null) {
				player.getPacketSender().sendMessage("You did not specify a skill ID.");
				return;
			}
			if (Integer.valueOf(cmd[3]) < 10 && Integer.valueOf(cmd[2]) == 3) {
				player.getPacketSender().sendMessage("You cannot make this level less than 10.");
				return;
			}
			if (Integer.valueOf(cmd[3]) > 99) {
				player.getPacketSender().sendMessage("You cannot make this level any higher.");
				return;
			}
			if (Integer.valueOf(cmd[3]) < 1) {
				player.getPacketSender().sendMessage("You cannot make this level any lower.");
				return;
			}
			try {
				other.getSkills().setLevel(Integer.valueOf(cmd[2]), Integer.valueOf(cmd[3]));
				other.getSkills().setXp(Integer.valueOf(cmd[2]), player.getSkills().getXpForLevel(Integer.valueOf(cmd[3])));

			} catch (Exception e) {

			}
			other.getUpdateFlags().flag(UpdateFlag.APPEARANCE);
			other.getPacketSender().sendSkillLevel(Integer.valueOf(cmd[1]));
			player.getPacketSender().sendMessage("Other player's skill " + Integer.valueOf(cmd[2]) + " set to level " + Integer.valueOf(cmd[3]));
			other.getPacketSender().sendMessage("Skill " + Integer.valueOf(cmd[2]) + " set to level " + Integer.valueOf(cmd[3]));
		}
	}
}*/