package com.hyperion.game.world.entity.player.packets;

import com.hyperion.cache.definitions.CachedObjectDefinition;
import com.hyperion.game.Constants;
import com.hyperion.game.content.CrystalChest;
import com.hyperion.game.content.minigames.warriorsguild.WarriorsGuild;
import com.hyperion.game.content.skills.cooking.Cooking;
import com.hyperion.game.content.skills.crafting.Crafting;
import com.hyperion.game.content.skills.prayer.BonesOnAltar;
import com.hyperion.game.content.skills.smithing.Smithing;
import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.game.world.grounditems.GroundItem;
import com.hyperion.game.world.grounditems.GroundItemManager;

public class ItemPackets {

    public static void handleItemPickup(Player player, GroundItem item) {
        player.getAttributes().remove("pickupId");
        player.getAttributes().remove("pickupitem");
        GroundItemManager.pickup_item(item, player);
    }

    public static void handleItemOnObject(Player player, GameObject realObject, Item item) {
        GameObject object = realObject.copy();
        if (!Constants.USE_DEV_CACHE) {
            object.setId(CachedObjectDefinition.renderIdsViseVersa[object.getId()]);
        }
        player.getAttributes().remove("itemOnObject");
        player.getAttributes().remove("itemOnObjectItem");
        int slot = player.getInventory().getSlotById(item.getId());
        if (Cooking.isCooking(player, item.getId(), object)) {
            return;
        }
        if (Crafting.wantsToCraftOnObject(player, item.getId(), object)) {
            return;
        }
        if (Smithing.wantsToSmith(player, object, item)) {
            return;
        }
        if (WarriorsGuild.canAnimate(player, object, item.getId())) {
            return;
        }
        int id = object.getId();
        switch (id) {
            case 172: // Crystal Chest
                if (player.getInventory().hasItem(989)) {
                    CrystalChest.open(player);
                } else {
                    player.sendMessage("You must use a crystal key on the chest.");
                }
                break;
            case 8985: //uncook evil bob
                if (player.getInventory().deleteItem(6202)) {
                    player.animate(883);
                    player.getInventory().addItem(6200);
                }
                break;
            case 2643:
                if (player.getInventory().deleteItem(444)) {
                    player.getInventory().addItem(2357);
                    player.getInventory().refresh();
                }
                break;
            case 3830://KQ Lair - tunnel
                if (item.getId() == 954) {
                    player.teleport(3507, 9494, 0);
                } else {
                    player.getPacketSender().sendMessage("You need Rope to go down there.");
                }
                break;
            case 411:
            case 409:
            case 2640:
                BonesOnAltar.bones_on_altar_check(player, object, item.getId(), slot);
                break;
        }
    }

    public static void handleItemOnItem(Player player, int usedItem, int usedOnItem) {
        int itemOne = usedItem;
        int itemTwo = usedOnItem;
        for (int i = 0; i < 2; i++) {
            if (i == 1) {
                itemOne = usedOnItem;
                itemTwo = usedItem;
            }
            if (itemOne == 987 && itemTwo == 985) {//crystal keys
                player.getInventory().deleteItem(itemOne);
                player.getInventory().deleteItem(itemTwo);
                player.getInventory().addItem(new Item(989, 1));
                player.getInventory().refresh();
                return;
            } else if (itemOne == 11286 && itemTwo == 1540) {//visage
                player.getInventory().deleteItem(itemOne);
                player.getInventory().deleteItem(itemTwo);
                player.getInventory().addItem(new Item(11283, 1));
                player.getInventory().refresh();
                return;
            } else if (itemOne == 2366 && itemTwo == 2368) {//dragon sq
                player.getInventory().deleteItem(2366);
                player.getInventory().deleteItem(2368);
                player.getInventory().addItem(new Item(1187, 1));
                player.getInventory().refresh();
                return;
            }
        }
    }
}
