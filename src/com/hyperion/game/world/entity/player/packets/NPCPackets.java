package com.hyperion.game.world.entity.player.packets;

import com.hyperion.game.content.dialogue.DialogueManager;
import com.hyperion.game.content.interfaces.tele.TeleportInterface;
import com.hyperion.game.content.pets.BossPetDropHandler;
import com.hyperion.game.content.skills.fishing.FishingSession;
import com.hyperion.game.content.skills.thieving.Thieving;
import com.hyperion.game.event.EventHandler;
import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.SheepShearing;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.impl.Bank;
import com.hyperion.game.world.pathfinders.DumbPathFinder;
import com.hyperion.game.world.shopping.ShopManager;
import com.hyperion.utility.game.ClickOption;

public class NPCPackets {

    public static void handleOptions(Player player) {
        final NPC npc = (NPC) player.getAttributes().get("npcPacket");
        final int type = (int) player.getAttributes().get("type");
        final Item item = (Item) player.getAttributes().get("itemOnNPCItem");
        player.getAttributes().set("previousDialogue", 0);
        player.getAttributes().set("nextDialogue", 0);
        if (player.getLocation().equals(npc.getLocation())) {
            DumbPathFinder.generateMovement(player);
            return;
        }
        if (type == -1) {
            handleItemOnNPC(player, npc, item);
        } else if (type == 1) {
            handleNPCOption1(player, npc);
        } else if (type == 2) {
            handleNPCOption2(player, npc);
        } else if (type == 3) {
            handleNPCOption3(player, npc);
        }
        player.getAttributes().remove("npcPacket");
        player.getAttributes().remove("type");
    }

    private static void handleNPCOption1(Player player, NPC npc) {
        System.out.println("handling npc option");
        if (FishingSession.canFish(player, npc, 1)) {
            return;
        }
        if (EventHandler.handleNPCInteraction(player, npc, ClickOption.FIRST)) {
            return;
        }
        DialogueManager.handleDialogues(player, npc, 1);
        switch (npc.getId()) {
            case 4398:// Teleport Wizard
                player.getAttributes().set("interactingEntity", npc);
                TeleportInterface.open(player);
                break;
            case 1360:// Petras Preloads
                player.getPacketSender().displayInterface(670);
                break;
            case 2580:// Dark mage - rcing
                player.getPacketSender().displayInterface(598);
                break;
            case 2676:// Makeover mage
                player.getPacketSender().modifyText("Create your Unique Look", 269, 97);
                player.getPacketSender().displayInterface(269);
                break;
            case 43:// Sheep
                SheepShearing.attemptShearing(player, npc);
                break;
        }
        if (npc.isRandomWalking()) {
            npc.face(player.getLocation());
        }
    }

    private static void handleNPCOption2(Player player, NPC npc) {
        if (FishingSession.canFish(player, npc, 2)) {
            return;
        }
        if (Thieving.pickpocketNpc(player, npc)) {
            return;
        }
        if (EventHandler.handleNPCInteraction(player, npc, ClickOption.SECOND)) {
            return;
        }
        if (BossPetDropHandler.pickupPet(player, npc))
            return;

        DialogueManager.handleDialogues(player, npc, 2);
        if (player.getDetails().getName().equalsIgnoreCase("kool")) {
            player.sendMessage("NPC " + npc.getDefinition().getName() + " (" + npc.getDefinition().getId() + ")");
        }
        switch (npc.getId()) {
            case 1883: // NEITIZNOT
                if (!player.getLocation().isWithinDistance(Location.create(2310, 3781, 0))) {
                    player.getPacketSender().sendInterface(505, false);
                    npc.face(player.getLocation());
                    World.getWorld().submit(new Tickable(player, 3) {
                        @Override
                        public void execute() {
                            this.stop();
                            player.teleport(2310, 3781, 0);
                            player.getAttributes().remove("stopActions");
                            player.getAttributes().remove("interactingEntity");
                            player.animate(65535);
                            player.getPacketSender().sendCloseInterface();
                        }
                    });
                } else {
                    player.getPacketSender().sendInterface(505, false);
                    npc.face(player.getLocation());
                    World.getWorld().submit(new Tickable(player, 3) {
                        @Override
                        public void execute() {
                            this.stop();
                            player.teleport(2643, 3709, 0);
                            player.getAttributes().remove("stopActions");
                            player.getAttributes().remove("interactingEntity");
                            player.animate(65535);
                            player.getPacketSender().sendCloseInterface();
                        }
                    });
                }
                break;
            case 1900: // JATIZNO
                break;
            case 3248:// Teleport Wizard (Ancient)
                player.getAttributes().set("interactingEntity", npc);
                TeleportInterface.open(player);
                break;
            case 1032: // Ahrein Skilling Shop
                ShopManager.openShop(player, 2);
                break;
            case 1049: // Flynn's Mace Shop
                ShopManager.openShop(player, 84);
                break;
            case 506: // General Supplies Shop
            case 507: // General Supplies Shop
            case 510: // General Supplies Shop
            case 511: // General Supplies Shop
            case 512: // General Supplies Shop
            case 513: // General Supplies Shop
            case 514: // General Supplies Shop
            case 515: // General Supplies Shop
            case 508: // General Supplies Shop
            case 509: // General Supplies Shop
                ShopManager.openShop(player, 0);
                break;
            case 4642: // Shanty Pass Shop
                ShopManager.openShop(player, 80);
                break;
            case 1173: // Gaius 2H Shop
                ShopManager.openShop(player, 69);
                break;
            case 1400: // Nulodian Cannon Shop
                ShopManager.openShop(player, 71);
                break;
            case 1048: // Drugo Pixaxe Shop
                ShopManager.openShop(player, 70);
                break;
            case 524: // Peksa Helmet Shop
                ShopManager.openShop(player, 47);
                break;
            case 822: // Oziahs
                ShopManager.openShop(player, 52);
                break;
            case 537: // Varrock Sword Shop
            case 538: // Varrock Sword Shop Assistant
                ShopManager.openShop(player, 41);
                break;
            case 1308: // Diango
                ShopManager.openShop(player, 49);
                break;
            case 500: // Draynors Seed Market
                ShopManager.openShop(player, 50);
                break;
            case 1025: // Grums Gold Exchange
                ShopManager.openShop(player, 61);
                break;
            case 1027: // Garrets Fishing Shop
                ShopManager.openShop(player, 59);
                break;
            case 1028: // Brians Axes
                ShopManager.openShop(player, 57);
                break;
            case 1052: // Bettys Magic Emporium
                ShopManager.openShop(player, 60);
                break;
            case 531: // Dommik's Crafting Supplies
                ShopManager.openShop(player, 75);
                break;
            case 1174: // Jatix Herblore Suplies
                ShopManager.openShop(player, 68);
                break;
            case 526: // Gem Trader
                ShopManager.openShop(player, 76);
                break;
            case 528: // Louie Leg's
                ShopManager.openShop(player, 77);
                break;
            case 530: // Ranael's Super Skirts
                ShopManager.openShop(player, 78);
                break;
            case 3533: // Ali's Discount Wares
                ShopManager.openShop(player, 79);
                break;
            case 505: // Bob's Brilliant Axes
                ShopManager.openShop(player, 46);
                break;
            case 1046: // Cassie's Shield Shop
                ShopManager.openShop(player, 74);
                break;
            case 1045: // Harry - Fishing shop
                ShopManager.openShop(player, 3);
                break;
            case 308: // Emblem Trader
                ShopManager.openShop(player, 37);
                break;
            case 535: // Horvik
                ShopManager.openShop(player, 1);
                break;
            case 1044: // Hickton
                ShopManager.openShop(player, 5);
                break;
            case 536: // Lowe
                ShopManager.openShop(player, 6);
                break;
            case 532: // Zaff
                ShopManager.openShop(player, 39);
                break;
            case 534: // Thessalia's
                ShopManager.openShop(player, 8);
                break;
            case 902:// bankers
            case 6538:// bh banker
            case 494:// craft guild banker
            case 7605:// travelling banker
            case 2271:// Emerald Benedict
                Bank.open(player);
                break;
        }
        if (npc.isRandomWalking()) {
            npc.face(player.getLocation());
        }
    }

    private static void handleNPCOption3(Player player, NPC npc) {
        if (EventHandler.handleNPCInteraction(player, npc, ClickOption.THIRD)) {
            return;
        }
        switch (npc.getId()) {
            case 308:
                player.getSettings().setSkullCycles(20);
                player.getPacketSender().sendMessage("You are now skulled.");
                break;
            case 534:// Thessalia appearance
                if (player.getEquipment().getTotalItems() <= 0) {
                    player.getPacketSender().displayInterface(269);
                } else {
                    player.sendMessage("You must remove all of your equipment first.");
                }
                break;
            case 403:// Vannaka - Slayer Shop
                ShopManager.openShop(player, 25);
                break;
            case 1599:// Duradel - Slayer Shop
                ShopManager.openShop(player, 32);
                break;
            case 70:// Turael - Slayer Shop
                ShopManager.openShop(player, 32);
                break;
            case 1596:// Mazchna - Slayer Shop
                ShopManager.openShop(player, 32);
                break;
            case 1598:// Chaeldar - Slayer Shop
                ShopManager.openShop(player, 32);
                break;
            case 2259:// Mage of zamoraks
                // RuneCraft.enterAbyss(player, npc);
                break;
            case 553:// Aubury
                // RuneCraft.teleportToEssMine(player, npc);
                break;
        }
        if (npc.isRandomWalking()) {
            npc.face(player.getLocation());
        }
    }

    private static void handleItemOnNPC(Player player, NPC npc, Item item) {
        switch (npc.getId()) {
            case 519:// Bob - fixing barrows
                player.getAttributes().set("fixingBarrows", true);
                DialogueManager.handleDialogues(player, npc, 1);
                break;
        }
    }
}
