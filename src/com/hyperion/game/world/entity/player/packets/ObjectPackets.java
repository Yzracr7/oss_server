package com.hyperion.game.world.entity.player.packets;

import com.hyperion.cache.definitions.CachedObjectDefinition;
import com.hyperion.game.Constants;
import com.hyperion.game.content.CrystalChest;
import com.hyperion.game.content.Ladders;
import com.hyperion.game.content.Scoreboard;
import com.hyperion.game.content.clues.ClueScrollManagement;
import com.hyperion.game.content.dialogue.impl.ladders.HandleUpdownOption;
import com.hyperion.game.content.dialogue.impl.objects.GrandTreeDialogue;
import com.hyperion.game.content.grandexchange.inter.GrandExchangeMainInterface;
import com.hyperion.game.content.minigames.barrows.Barrows;
import com.hyperion.game.content.minigames.fightcaves.FightCaves;
import com.hyperion.game.content.skills.agility.Shortcuts;
import com.hyperion.game.content.skills.agility.courses.BarbarianCourse;
import com.hyperion.game.content.skills.agility.courses.GnomeCourse;
import com.hyperion.game.content.skills.agility.courses.WildernessCourse;
import com.hyperion.game.content.skills.agility.obstacles.ClimbOver;
import com.hyperion.game.content.skills.agility.rooftops.AlKharidRooftop;
import com.hyperion.game.content.skills.agility.rooftops.DraynorRooftop;
import com.hyperion.game.content.skills.crafting.Crafting;
import com.hyperion.game.content.skills.crafting.Spinning;
import com.hyperion.game.content.skills.mining.MineSession;
import com.hyperion.game.content.skills.mining.MiningData;
import com.hyperion.game.content.skills.prayer.PrayerAltars;
import com.hyperion.game.content.skills.runecrafting.RCEntry;
import com.hyperion.game.content.skills.runecrafting.Runecrafting;
import com.hyperion.game.content.skills.smithing.Smelting;
import com.hyperion.game.content.skills.smithing.Smithing;
import com.hyperion.game.content.skills.smithing.SmithingData;
import com.hyperion.game.content.skills.thieving.StallAction;
import com.hyperion.game.content.skills.thieving.Thieving;
import com.hyperion.game.content.skills.woodcutting.CutSession;
import com.hyperion.game.event.EventHandler;
import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.masks.ForceMovement;
import com.hyperion.game.world.entity.npc.impl.zulrah.ZulrahBossRoom;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;
import com.hyperion.game.world.entity.player.container.impl.Bank;
import com.hyperion.game.world.gameobjects.*;
import com.hyperion.game.world.shopping.ShopManager;
import com.hyperion.utility.Debugging;
import com.hyperion.utility.game.ClickOption;

public class ObjectPackets {

    public static void handleOption(Player player) {
        final GameObject object = (GameObject) player.getAttributes().get("objectPacket");
        final int type = (int) player.getAttributes().get("type");
        player.getAttributes().remove("objectPacket");
        player.getAttributes().remove("type");
        if (type == 1) {
            handleObjectPacket1(player, object);
        } else if (type == 2 || type == 10) {
            handleObjectPacket2(player, object);
        } else if (type == 3) {
            handleObjectPacket3(player, object);
        }
        if (Constants.DEBUG_MODE) {
            player.getPacketSender().sendMessage("Object click " + type + " - " + "id: " + object.getId() + " loc: " + object.getLocation() + " face: " + object.getRotation() + " type:" + object.getType());
        }
    }

    private static void handleObjectPacket1(final Player player, GameObject realObject) {
        GameObject object = realObject;
        if (!Constants.USE_DEV_CACHE) {
            object.setId(CachedObjectDefinition.renderIdsViseVersa[object.getId()]);
        }
        int id = object.getId();
        Location loc = object.getLocation();
        final int x = loc.getX();
        final int y = loc.getY();
        final int z = loc.getZ();
        Location playerLoc = player.getCentreLocation();
        final int playerX = playerLoc.getX();
        final int playerY = playerLoc.getY();
        player.face(loc);
        if (player.getName().equalsIgnoreCase("woo") || player.getName().equalsIgnoreCase("trees")) {
            player.sendMessage("[Object Option 1] " + id + " : " + "{x]" + x + " [y]" + y);
        }
        if (ClimbOver.isClimbableObject(player, object) || Crafting.wantsToCraftOnObject(player, -1, object) || RCEntry.enter_altar(player, object) || Runecrafting.craft(player, object) || CutSession.attempt_woodcut(player, object) || MineSession.attempt_mine(player, object) || GrandTreeDialogue.operate(player, object) || HandleUpdownOption.operate(player, object) || GnomeCourse.isGnomeCourse(player, object) || BarbarianCourse.isBarbarianCourse(player, object) || WildernessCourse.isWildernessCourse(player, object) || EventHandler.handleObjectInteraction(player, object, ClickOption.FIRST) || ObjectManager.executeLever(player, object) || DoorManager.handleDoor(player, object) || LadderHandler.handleladder(player, object.getId(), object.getLocation()) || ClueScrollManagement.confirmPerformance(player, object, ClickOption.FIRST, true) || PrayerAltars.isRestoringPrayerAtAltar(player, object) || WildernessObelisks.usingObelisk(player, object)) {
            return;
        }
        if (DraynorRooftop.isDraynorRooftopObject(object)) {
            if (object.getId() == 10073) {
                DraynorRooftop rooftop = new DraynorRooftop();
                rooftop.start(player, object);
                return;
            } else if (player.getAttributes().get("draynor_rooftop") != null && player.getAttributes().get("draynor_rooftop_stage") != null) {
                DraynorRooftop rooftop = player.getAttributes().get("draynor_rooftop");
                rooftop.handleObjectAction(player, object);
                return;
            }
        }
        if (AlKharidRooftop.isAlkharidRooftopObject(object)) {
            if (object.getId() == 10093) {
                AlKharidRooftop rooftop = new AlKharidRooftop();
                rooftop.start(player, object);
                return;
            } else if (player.getAttributes().get("alharid_rooftop") != null && player.getAttributes().get("alharid_rooftop_stage") != null) {
                AlKharidRooftop rooftop = player.getAttributes().get("alharid_rooftop");
                rooftop.handleObjectAction(player, object);
                return;
            }
        }
        if (Barrows.spawnBrother(player, object)) {
            return;
        }
        switch (id) {
            case 20667: // Ahrim
            case 20669: // Guthan
            case 20670: // Karils
            case 20671: // Torag
            case 20672: // Verac
            case 20668: // Dha
                Barrows.exitArea(player, object);
                break;
            case 10061:
                GrandExchangeMainInterface.open(player, false);
                break;
            case 11833:
                FightCaves.enter(player);
                break;
            case 10060:
            case 12759:
            case 30389:
                Bank.open(player);
                break;
            case 16511: // Edge dung to moss giants pipe
                if (object.getLocation().isWithinDistance(Location.create(3150, 9906, 0))) {

                } else if (object.getLocation().isWithinDistance(Location.create(3153, 9906, 0))) {

                }
                break;
            case 26756:
                if (object.getLocation().isWithinDistance(Location.create(3095, 3500, 0))) {
                    Scoreboard.openPVPScoreboard(player);
                }
                break;
            case 6659:
                if (object.getLocation().isWithinDistance(Location.create(3225, 9539, 0)))
                    player.teleport(Location.create(3219, 9533, 2));
                break;
            case 6658:
                if (object.getLocation().isWithinDistance(Location.create(3219, 9533, 2)))
                    player.teleport(Location.create(3226, 9542, 0));
                break;
            case 4781:
                if (object.getLocation().isWithinDistance(Location.create(2764, 9103, 0)))
                    Ladders.executeLadder(player, false, 2764, 2703, 0);
                break;
            case 4780:
                if (object.getLocation().isWithinDistance(Location.create(2763, 2703, 0)))
                    Ladders.executeLadder(player, true, 2764, 9103, 0);
                break;
            case 15572:
                if (object.getLocation().isWithinDistance(Location.create(2921, 2721, 0)))
                    player.sendMessage("I should probably not go down that snake pit.");
                // Ladders.executeLadder(player, true, 2893, 9907, 0);
                break;
            case 17384:
                if (object.getLocation().isWithinDistance(Location.create(2892, 3507, 0)))
                    Ladders.executeLadder(player, true, 2893, 9907, 0);
                break;
            case 21725:
                if (object.getLocation().isWithinDistance(Location.create(2635, 9514, 0)))
                    player.teleport(Location.create(2636, 9510, 2));
                break;
            case 21726:
                if (object.getLocation().isWithinDistance(Location.create(2635, 9511, 2)))
                    player.teleport(Location.create(2636, 9517, 0));
                break;
            case 21724:
                if (object.getLocation().isWithinDistance(Location.create(2644, 9593, 2)))
                    player.teleport(Location.create(2649, 9591, 0));
                break;
            case 21722:
                if (object.getLocation().isWithinDistance(Location.create(2648, 9592, 0)))
                    player.teleport(Location.create(2643, 9595, 2));
                break;
            case 20877:
                if (object.getLocation().isWithinDistance(Location.create(2129, 5647, 0)))
                    player.teleport(Location.create(2026, 5610, 0));
                break;
            case 28687:
                if (object.getLocation().isWithinDistance(Location.create(2129, 5647, 0)))
                    player.teleport(Location.create(2026, 5610, 0));
                break;
            case 28686:
                if (object.getLocation().isWithinDistance(Location.create(2026, 5612, 0)))
                    player.teleport(Location.create(2128, 5647, 0));
                break;
            case 3757:
                if (object.getLocation().isWithinDistance(Location.create(2904, 3645, 0)))
                    player.teleport(Location.create(2907, 10019, 0));
                break;
            case 3758:
                if (object.getLocation().equals(Location.create(2906, 10036, 0))) {
                    player.teleport(Location.create(2908, 3654, 0));
                } else if (object.getLocation().isWithinDistance(Location.create(2907, 10017, 0))) {
                    player.teleport(Location.create(2904, 3643, 0));
                }
                break;
            case 3761:
                if (object.getLocation().isWithinDistance(Location.create(2823, 10048, 0)))
                    player.teleport(Location.create(2827, 3646, 0));
                break;
            case 3762:
                player.teleport(Location.create(2823, 10050, 0));
                break;
            case 10068:
                if (object.getLocation().isWithinDistance(new Location(2214, 3056, 0))) {
                    ZulrahBossRoom game_instance = new ZulrahBossRoom();
                    game_instance.enterInstancedRoom(player);
                }
                break;
            case 23969: // Falador Mine Back Entrance Stairs Exit
                if (object.getLocation().isWithinDistance(new Location(3060, 9775, 0))) {
                    Ladders.executeLadder(player, false, 3061, 3376, 0);
                    return;
                }
                break;
            case 16665: // Legends Dungeon Exit
                if (object.getLocation().isWithinDistance(new Location(2724, 9774, 0))) {
                    Ladders.executeLadder(player, true, 2723, 3374, 0);
                    return;
                }
                break;
            case 16664: // Falador Mine Back Entrance Stairs
                if (object.getLocation().isWithinDistance(new Location(3060, 3377, 0))) {
                    Ladders.executeLadder(player, true, 3058, 9775, 0);
                    return;
                } else if (object.getLocation().isWithinDistance(new Location(2724, 3374, 0))) {
                    Ladders.executeLadder(player, true, 2727, 9774, 0);
                    return;
                }
                break;
            case 25938: // Seers Craft House Ladder Up
                if (object.getLocation().isWithinDistance(new Location(2715, 3470, 0))) {
                    Ladders.executeLadder(player, false, 2715, 3471, 1);
                    return;
                }
                break;
            case 26118: // Seers Craft House Ladder Up Second Floor
                if (object.getLocation().isWithinDistance(new Location(2715, 3472, 1))) {
                    Ladders.executeLadder(player, false, 2714, 3472, 3);
                    return;
                }
                break;
            case 26119: // Seers Craft House Ladder Roof Dwn
                if (object.getLocation().isWithinDistance(new Location(2715, 3472, 3))) {
                    Ladders.executeLadder(player, true, 2714, 3472, 1);
                    return;
                }
                break;
            case 25939: // Seers Craft House Ladder Floor 1 Dwn
                if (object.getLocation().isWithinDistance(new Location(2715, 3470, 1))) {
                    Ladders.executeLadder(player, true, 2715, 3471, 0);
                    return;
                }
                break;
            case 1738: // South Port Sarim Dungeon Trapdoor
                if (object.getLocation().isWithinDistance(new Location(3008, 3150, 0))) {
                    Ladders.executeLadder(player, true, 3009, 9550, 0);
                    return;
                }
                break;
            case 9558: // {Ladder}
                // Rimmington Phials House Up
                if (object.getLocation().isWithinDistance(new Location(2948, 3213, 0))) {
                    Ladders.executeLadder(player, false, 2949, 3213, 1);
                    return;
                }
                // Port Sarim Jail House
                if (object.getLocation().isWithinDistance(new Location(3010, 3184, 0))) {
                    Ladders.executeLadder(player, false, 3011, 3184, 1);
                    return;
                }
                break;
            case 9559: // {Ladder}
                // Rimmington Phials House Down
                if (object.getLocation().isWithinDistance(new Location(2948, 3213, 1))) {
                    Ladders.executeLadder(player, true, 2949, 3213, 0);
                    return;
                }
                // Port Sarim Jail House
                if (object.getLocation().isWithinDistance(new Location(3010, 3184, 1))) {
                    Ladders.executeLadder(player, true, 3011, 3184, 0);
                    return;
                }
                break;
            case 60: // {Ladder}
                // Port Sarim House
                if (object.getLocation().isWithinDistance(new Location(3013, 3244, 0))) {
                    Ladders.executeLadder(player, false, 3013, 3245, 0);
                    return;
                }
                break;
            case 30178: // Kraken Cove Exit
                if (object.getLocation().isWithinDistance(new Location(2276, 9987, 0))) {
                    player.teleport(Location.create(2278, 3610, 0));
                }
                break;
            case 9582: // Axe Shop Port Sarim Up
                if (object.getLocation().isWithinDistance(new Location(3026, 3248, 0)) && player.getLocation().getY() < 3255) {
                    Ladders.executeLadder(player, false, 3026, 3247, 1);
                    return;
                } else if (object.getLocation().isWithinDistance(new Location(3023, 3261, 0)) && player.getLocation().getY() >= 3257) {
                    Ladders.executeLadder(player, false, 3025, 3261, 1);
                    return;
                }
                break;
            case 9584: // Axe Shop Port Sarim Down
                if (object.getLocation().isWithinDistance(new Location(3026, 3248, 1)) && player.getLocation().getY() < 3255) {
                    Ladders.executeLadder(player, true, 3025, 3248, 0);
                    return;
                } else if (object.getLocation().isWithinDistance(new Location(3024, 3261, 1)) && player.getLocation().getY() >= 3257) {
                    Ladders.executeLadder(player, true, 3024, 3260, 0);
                    return;
                }
                break;
            case 16670: // Draynor Morgans Wise Old Mans House
                if (object.getLocation().isWithinDistance(new Location(3090, 3251, 0))) {
                    Ladders.executeLadder(player, false, 3093, 3251, 1);
                    return;
                } else if (object.getLocation().isWithinDistance(new Location(3092, 3251, 1))) {
                    Ladders.executeLadder(player, true, 3089, 3251, 0);
                    return;
                }
                break;
            case 15645: // Draynor Morgans House Stairs Bottom
                if (object.getLocation().isWithinDistance(new Location(3099, 3266, 0))) {
                    Ladders.executeLadder(player, false, 3102, 3267, 1);
                    return;
                }
                break;
            case 15648: // Draynor Morgans House Stairs Top
                if (object.getLocation().isWithinDistance(new Location(3101, 3267, 1))) {
                    Ladders.executeLadder(player, true, 3098, 3267, 0);
                    return;
                }
                break;

            case 16679: //{Ladder}
                // Port Sarim House
                if (object.getLocation().isWithinDistance(new Location(3011, 3235, 1))) {
                    Ladders.executeLadder(player, true, 3012, 3235, 0);
                    return;
                }
                // Lumbridge Outside Castle Floor 3
                if (object.getLocation().isWithinDistance(new Location(3229, 3213, 2)) && player.getLocation().getY() < 3220) {
                    Ladders.executeLadder(player, true, 3229, 3214, 1);
                    return;
                } else if (object.getLocation().isWithinDistance(new Location(3229, 3224, 2))) {
                    Ladders.executeLadder(player, true, 3229, 3223, 1);
                    return;
                }
                break;
            case 16684: // Lumbridge Outside Castle Floor 2
                if (object.getLocation().isWithinDistance(new Location(3204, 3207, 0))) {
                    Ladders.executeLadder(player, false, 3205, 3209, 1);
                    return;
                } else if (object.getLocation().isWithinDistance(new Location(3204, 3229, 0))) {
                    Ladders.executeLadder(player, false, 3206, 3229, 1);
                    return;
                }
                break;
            case 16683:
                // Lumbridge Outside Castle 1
                if (object.getLocation().isWithinDistance(new Location(3229, 3224, 0)) && player.getLocation().getY() > 3220) {
                    Ladders.executeLadder(player, false, 3229, 3223, 1);
                    return;
                } else if (object.getLocation().isWithinDistance(new Location(3229, 3213, 0)) && player.getLocation().getY() < 3220) {
                    Ladders.executeLadder(player, false, 3229, 3214, 1);
                    return;
                }
                // Port Sarim House
                else if (object.getLocation().getX() == 3011 && object.getLocation().getY() == 3235) {
                    Ladders.executeLadder(player, false, 3012, 3235, 1);
                    return;
                }
                break;
            case 16671: // Lumbridge Stairs Floor 1
                if (object.getLocation().isWithinDistance(new Location(3204, 3207, 0))) {
                    Ladders.executeLadder(player, false, 3205, 3209, 1);
                    return;
                } else if (object.getLocation().isWithinDistance(new Location(3204, 3229, 0))) {
                    Ladders.executeLadder(player, false, 3206, 3229, 1);
                    return;
                }
                break;
            case 16672: // Lumbridge Stairs Floor 2
                if (object.getLocation().isWithinDistance(new Location(3204, 3207, 0))) {
                    Ladders.executeLadder(player, false, 3205, 3209, 1);
                    return;
                } else if (object.getLocation().isWithinDistance(new Location(3204, 3229, 0))) {
                    Ladders.executeLadder(player, false, 3206, 3229, 1);
                    return;
                }
                break;
            case 16673: // Lumbridge Stairs Floor 3
                if (object.getLocation().isWithinDistance(new Location(3205, 3208, 2))) {
                    Ladders.executeLadder(player, true, 3205, 3209, 1);
                    return;
                } else if (object.getLocation().isWithinDistance(new Location(3205, 3229, 2))) {
                    Ladders.executeLadder(player, true, 3206, 3228, 1);
                    return;
                }
                break;
            case 14880: // Lumbridge Caste Basement Entrance
                if (object.getLocation().isWithinDistance(player.getLocation())) {
                    Ladders.executeLadder(player, true, 3210, 9616, 0);
                    return;
                }
                break;
            case 17385: // {Ladder}
                // South Port Sarim Dungeon Exit
                if (object.getLocation().isWithinDistance(new Location(3008, 9550, 0))) {
                    Ladders.executeLadder(player, false, 3009, 3150, 0);
                    return;
                }
                // Lumbridge Caste Basement Exit
                if (object.getLocation().isWithinDistance(new Location(3209, 9616, 0))) {
                    Ladders.executeLadder(player, false, 3210, 3216, 0);
                    return;
                }
                break;
            case 18988: // KBD exit ladder
                if (object.getLocation().isWithinDistance(player.getLocation())) {
                    Ladders.executeLadder(player, false, 3017, 3850, 0);
                    return;
                }
                break;
            case 18987: // KBD Entrance ladder
                if (object.getLocation().isWithinDistance(player.getLocation())) {
                    Ladders.executeLadder(player, true, 3069, 10255, 0);
                    return;
                }
                break;
            case 5492:// Ham Entrance
                if (object.getLocation().isWithinDistance(player.getLocation())) {
                    Ladders.executeLadder(player, true, 3149, 9652, 0);
                    return;
                }
                break;
            case 5493: // Ham Lair Exit Ladder
                if (object.getLocation().isWithinDistance(player.getLocation())) {
                    Ladders.executeLadder(player, false, 3165, 3251, 0);
                    return;
                }
                break;
            case 6552: // DT Altar
                player.animate(645);
                player.getSettings().setMagicType(player.getSettings().getMagicType() == 2 ? 1 : 2);
                player.sendMessage(player.getSettings().getMagicType() == 2 ? "You feel a strange drain upon your memory.." : "You feel a strange wisdom fill your mind..");
                player.getPacketSender().sendTab(92, player.getSettings().getMagicType() == 2 ? 665 : 193);
                return;
            case 6481: // DT Pyramid
            case 678:
            case 26766:
            case 30177:
                if (player.getLocation().withinActualDistance(new Location(object.getLocation().getX(), object.getLocation().getY(), 0), 2)) {
                    Shortcuts.crawlIn(player, object);
                    return;
                }
                break;
            case 23569: // Tav Dung / Cerubus Cave Entrance
                player.sendMessage("Cerubus lair is coming soon!");
                return;
            case 16510: // Tav Spikes
                if (player.getLocation().withinActualDistance(new Location(2879, 9813, 0), 2)) {
                    Shortcuts.jumpTavSpikes(player, object);
                    return;
                }
                break;
            case 16509: // Tav Pipe
                if (player.getLocation().withinActualDistance(new Location(2887, 9799, 0), 2)) {
                    Shortcuts.tavPipe(player, object);
                    return;
                } else {
                    if (player.getLocation().withinActualDistance(new Location(2890, 9799, 0), 2)) {
                        Shortcuts.tavPipe(player, object);
                        return;
                    }
                }
                break;
            case 17387: // Falador mine main exit
                if (player.getLocation().withinActualDistance(new Location(3020, 9850, 0), 2)) {
                    Ladders.executeLadder(player, false, 3018, 3450, 0);
                    return;
                } else if (object.getLocation().isWithinDistance(Location.create(2892, 9907, 0))) {
                    Ladders.executeLadder(player, false, 2893, 3507, 0);
                    return;
                }
                break;
            case 11867: // Falador Mine Main Entrance
                if (player.getLocation().withinActualDistance(new Location(3018, 3450, 0), 2)) {
                    Ladders.executeLadder(player, true, 3020, 9850, 0);
                    return;
                }
                break;
            case 2141: // Rellekka Cave Exit
                if (player.getLocation().withinActualDistance((new Location(2809, 10001, 0)), 3)) {
                    player.teleport(new Location(2796, 3615, 0));
                    return;
                }
                break;
            case 2123: // Rellekka Cave Entrance
                if (player.getLocation().withinActualDistance((new Location(2797, 3614, 0)), 3)) {
                    player.teleport(new Location(2808, 10002, 0));
                    return;
                }
                break;

            case 16542: // Relleka Log Balance North
                if (player.getLocation().getY() == 3592 || !player.getAttributes().equals("stopActions")) {
                    player.getAttributes().set("stopActions", true);
                    player.getWalkingQueue().reset();
                    player.getWalkingQueue().addStep(2722, 3596);
                    player.getWalkingQueue().finish();
                    World.getWorld().submit(new Tickable(1) {
                        @Override
                        public void execute() {
                            this.stop();
                            player.getPacketSender().sendMessage("You walk carefully across the slippery log...");
                            BarbarianCourse.execute_force_walking(player, 762, 2722, 3596, 1, 3, true, 5, "... and make it safely to the other side.");
                            player.getAttributes().remove("stopActions");
                        }
                    });
                }
                break;
            case 16540: // Relleka Log Balance South
                if (player.getLocation().getY() == 3596 || !player.getAttributes().equals("stopActions")) {
                    player.getAttributes().set("stopActions", true);
                    player.getWalkingQueue().reset();
                    player.getWalkingQueue().addStep(2722, 3592);
                    player.getWalkingQueue().finish();
                    World.getWorld().submit(new Tickable(1) {
                        @Override
                        public void execute() {
                            this.stop();
                            player.getPacketSender().sendMessage("You walk carefully across the slippery log...");
                            BarbarianCourse.execute_force_walking(player, 762, 2722, 3592, 1, 3, true, 5, "... and make it safely to the other side.");
                            player.getAttributes().remove("stopActions");
                        }
                    });
                }
                break;
            case 16529: // EDGEVILLE GE TUNNEL SHORTCUT
            case 16530: // EDGEVILLE GE TUNNEL SHORTCUT
                Shortcuts.edgeExchangePip(player, object);
                return;
            case 24222: // FALADOR WALL CLIMB
                if (player.getSkills().getLevel(Skills.AGILITY) > 5) {
                    if (player.getLocation().getX() > object.getLocation().getX()) {
                        Location target = Location.create(object.getLocation().getX() - 1, object.getLocation().getY(), object.getPlane());
                        BarbarianCourse.execute_force_movement(player, target, 4853, -1, 2, ForceMovement.WEST, 30, 60, true, 1, null);
                        player.getSkills().addExperience(Skills.AGILITY, 1.2);
                    } else {
                        Location target = Location.create(object.getLocation().getX() + 1, object.getLocation().getY(), object.getPlane());
                        BarbarianCourse.execute_force_movement(player, target, 4853, -1, 2, ForceMovement.EAST, 30, 60, true, 1, null);
                        player.getSkills().addExperience(Skills.AGILITY, 1.2);
                    }
                } else {
                    player.sendMessage("You need an agility level of 5 to navigate this obstacle.");
                }
                return;
            case 16680: // TAVERLY DUNGEON ENTRANCE
                if (player.getLocation().withinActualDistance(new Location(2884, 3396, 0), 2)) {
                    Ladders.executeLadder(player, true, 2884, 9796, 0);
                    return;
                }
                break;
            case 25349: // Barbarian Anvil
                player.getPacketSender().sendChatboxDialogue(true, "You do not have permission to use this anvil.");
                break;
            case 2097: // Varrock Anvil
                for (int i = 0; i < SmithingData.BARS.length; i++) {
                    if (player.getInventory().hasItem(SmithingData.BARS[i])) {
                        Smithing.wantsToSmith(player, object, new Item(SmithingData.BARS[i]));
                        return;
                    }
                }
                player.getPacketSender().sendChatboxDialogue(true, "You have no bars to use.");
                break;

            case 5946: // Lumb Cave Exit Rope
                if (player.getLocation().withinActualDistance(new Location(3168, 9572, 0), 2)) {
                    Ladders.executeLadder(player, false, 3169, 3173, 0);
                    return;
                }
                break;
            case 18270: // Ardy cave south east
                if (player.getLocation().withinActualDistance(new Location(2696, 3283, 0), 2)) {
                    player.teleport(2696, 9684, 0);
                    return;
                }
                break;
            case 18354: // Ardy cave south east exit
                if (player.getLocation().withinActualDistance(new Location(2696, 9683, 0), 2)) {
                    player.teleport(2697, 3283, 0);
                    return;
                }
                break;
            case 5947: // LUMB SWAMP DARK HOLE
                if (player.getLocation().withinActualDistance(new Location(3169, 3173, 0), 2)) {
                    if (!player.getInventory().hasItem(954) || !player.getInventory().hasItem(33)) {
                        player.getPacketSender().sendChatboxDialogue(true, "A lit candle and a rope is required to enterInstancedRoom this hole.");
                        player.sendMessage("A lit candle and a rope is required to enterInstancedRoom this hole.");
                        return;
                    } else {
                        Ladders.executeLadder(player, true, 3168, 9572, 0);
                        return;
                    }
                } else {
                    player.sendMessage("You must have a rope to enterInstancedRoom this hole.");
                }
                break;
            case 733: // Webs
                ObjectManager.slashWebs(player, object);
                break;
            case 172: // Crystal Chest
                if (player.getInventory().hasItem(989)) {
                    CrystalChest.open(player);
                } else {
                    player.sendMessage("You must have a crystal key to open this chest.");
                }
                break;
            case 25808: // Bank
            case 2094: // Bank
            case 7409: // Bank
            case 6943: // Edge Bank Booth && Gnome Tree Bank
            case 2693: // Shanty Pass Chest
            case 19051: // Bank Lands End
            case 4483: // Castle Wars Bank
            case 3194: // Duel Arena Bank
                Bank.open(player);
                break;
            case 23271: // Ditch
                WildernessDitch.jump(player, object);
                break;
            case 78:// Sparkling pool going in
                if (!player.getVariables().finishedMageArena) {
                    player.getPacketSender().sendMessage("You need to complete Mage Arena to enterInstancedRoom.");
                    break;
                }
                player.teleport(2509, 4689, 0);
                break;
            case 2879:// Sparkling pool going out
                player.teleport(2542, 4718, 0);
                break;
            default:
                Debugging.print("Unknown Object Action : " + object.toString());
                break;
        }
    }

    private static void handleObjectPacket2(final Player player, GameObject realObject) {
        //Debugging.print("HANDLE OBJET PACKET 2 : " + realObject.toString());
        GameObject object = realObject.copy();
        if (!Constants.USE_DEV_CACHE) {
            object.setId(CachedObjectDefinition.renderIdsViseVersa[object.getId()]);
        }
        int id = object.getId();
        if (object.getName() != null) {
            if (object.getName().equalsIgnoreCase("bank booth")) {
                Bank.open(player);
                return;
            }
        }
        if (EventHandler.handleObjectInteraction(player, object, ClickOption.SECOND)) {
            return;
        }
        if (MiningData.getRockIndex(id) != -1) {
            MiningData.prospectRocks(player, object);
            return;
        }
        if (StallAction.handle(player, realObject)) {
            return;
        }
        switch (id) {

            case 16684: //Lumbridge Castle tower floor 1
                if (player.getLocation().equals(new Location(3229, 3223, 1))) {
                    Ladders.executeLadder(player, false, 3229, 3223, 2);
                } else if (player.getLocation().equals(new Location(3229, 3214, 1))) {
                    Ladders.executeLadder(player, false, 3229, 3214, 2);
                }
                break;
            case 6943://bank booth
                Bank.open(player);
                break;
            case 7236:
                Thieving.thieveSafe(player, object);
                break;
            case 12309:// Gloves chest
                if (player.getDetails().isDonator()) {
                    ShopManager.getShopManager().openShop(player, 27);
                } else {
                    player.getPacketSender().sendMessage("Upgrade to Gold members to access this shop.");
                }
                break;
            case 16469:
            case 24009:
                Smelting.displaySmeltOptions(player, object);
                break;
            case 1738:// warrior guild stairs - going down
                Ladders.executeLadder(player, true, 2840, 3539, 0);
                break;
            case 1739: // stairs 2 - going up
                Ladders.executeLadder(player, false, 2840, 3539, 2);
                break;
            case 25824:// Spinning
            case 14889:// Spinning
                Spinning.displaySpinningInterface(player, object);
                break;
            case 30389: // G.E Bank Booth
            case 10060: // G.E Bank Booth
            case 27663: // Clan wars bank chest
                Bank.open(player);
                break;
            default:
                //Debugging.print("2: " + object.toString());
                break;
        }
    }

    private static void handleObjectPacket3(final Player player, GameObject realObject) {
        GameObject object = realObject.copy();
        if (!Constants.USE_DEV_CACHE) {
            object.setId(CachedObjectDefinition.renderIdsViseVersa[object.getId()]);
        }
        int id = object.getId();
        Location loc = object.getLocation();
        final int x = loc.getX();
        final int y = loc.getY();
        final int z = loc.getZ();

        if (EventHandler.handleObjectInteraction(player, object, ClickOption.THIRD)) {
            return;
        }
        switch (id) {
            case 5492: // HAM DUNGEON TRAP DOOR ENTRANCE
                player.sendMessage("Attempting to pick lock..trees.");
                break;
            case 1748:// Ladder
                if (z == 1) {
                    if (x == 2466 && y == 3495) {
                        Ladders.executeLadder(player, true, x, y + 1, 0);
                    }
                }
                break;
            case 1739: // stairs 2 down
                Ladders.executeLadder(player, true, 2840, 3539, 0);
                break;
            case 12309:// Gloves chest
                if (player.getVariables().finishedRfd) {
                    ShopManager.getShopManager().openShop(player, 19);
                } else {
                    if (player.getVariables().getRfdStage() == 2 || player.getVariables().getRfdStage() == 3) {
                        ShopManager.getShopManager().openShop(player, 18);
                    } else {
                        player.getPacketSender().sendMessage("You can't view this shop yet.");
                    }
                }
                break;
            case 16684: //Lumbridge Castle tower floor 1
                if (player.getLocation().equals(new Location(3229, 3223, 1))) {
                    Ladders.executeLadder(player, true, 3229, 3223, 0);
                } else if (player.getLocation().equals(new Location(3229, 3214, 1))) {
                    Ladders.executeLadder(player, true, 3229, 3214, 0);
                }
                break;
            default:
                Debugging.print("Object packet 3: " + object.getId());
                break;
        }
    }
}
