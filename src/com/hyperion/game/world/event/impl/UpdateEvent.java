package com.hyperion.game.world.event.impl;

import com.hyperion.game.world.event.Event;

/**
 * A tick which starts player update tasks.
 * @author Graham Edgecombe
 *
 */
public class UpdateEvent extends Event {

	/**
	 * The cycle time, in milliseconds.
	 */
	public static final int CYCLE_TIME = 600;
	
	/**
	 * Creates the update event to cycle every 600 milliseconds.
	 */
	public UpdateEvent() {
		super(CYCLE_TIME);
	}

	@Override
	public void execute() {
		/*World.getWorld().getTickableManager().pulse();

		List<Task> tickTasks = new ArrayList<Task>();
		List<Task> updateTasks = new ArrayList<Task>();
		List<Task> resetTasks = new ArrayList<Task>();
		for(NPC npc : World.getWorld().getNPCS()) {
			if (npc != null) {
				tickTasks.add(new NPCTickTask(npc));
				resetTasks.add(new NPCResetTask(npc));
			}
		}
		for (Player player : World.getWorld().getPlayers()) {
			if (player != null) {
				if(!player.isDestroyed()) {
					tickTasks.add(new PlayerTickTask(player));
					updateTasks.add(new ConsecutiveTask(new PlayerUpdateTask(player), new NPCUpdateTask(player)));
					resetTasks.add(new PlayerResetTask(player));
				}
			}
		}
		//ticks can no longer be parallel due to region code
		Task tickTask = new ConsecutiveTask(tickTasks.toArray(new Task[0]));
		Task updateTask = new ParallelTask(updateTasks.toArray(new Task[0]));
		Task resetTask = new ParallelTask(resetTasks.toArray(new Task[0]));
		World.getWorld().submit(new ConsecutiveTask(tickTask, updateTask, resetTask));*/
	}
}
