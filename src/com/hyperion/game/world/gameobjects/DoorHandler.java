package com.hyperion.game.world.gameobjects;

import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * Basic door system, adios doors.cfg!
 *
 * @author trees
 * @author stuart
 */
public class DoorHandler {

    /**
     * Id's of each door object
     */
    private static final int[] DOORS = {3, 24057, 1967, 1968, 2625, 3746, 3745, 3725, 3726, 24318, 59, 27485, 27486, 27443, 11780, 3507, 2623, 23972, 15056, 1804, 11773, 14910, 9398, 1569, 13001, 12986, 12987, 25819, 1535, 1540, 11775, 11720, 24050,/*DoubleGate*/2392,/*DoubleGate*/2391,/*DoubleGate*/1727,/*DoubleGate*/1728,/*DoubleGate*/1558,/*DoubleGate*/1560};

    /**
     * Map with the Location being mapped to the door, only doors which have
     * been opened / closed to save a little bit of memory
     */
    private static Map<Location, DoorObject> doors = new HashMap<Location, DoorObject>();

    /**
     * Is the given id an door?
     *
     * @param id the id to check
     * @return true yes false no
     */
    private static boolean isDoor(int id) {
        for (int door : DOORS) {
            if (id == door) {
                return true;
            }
        }
        return false;
    }

    /**
     * Handles opening and closing of doors, returns true if it was handled
     * otherwise false nothing changed
     *
     * @param player   the player opening the door
     * @param objectId the object id being clicked
     * @param objLoc   the objects location
     * @return handled or not
     */
    public static boolean handleDoor(Player player, int objectId,
                                     Location objLoc) {
        if (isDoor(objectId)) {
            player.sendMessage("[Object is a door]");
            DoorObject door = doors.get(objLoc);
            if (door == null) {
                GameObject mapObject = World.getObjectWithId(objLoc, objectId);
                if (mapObject != null) {
                    door = new DoorObject(mapObject);
                    doors.put(objLoc, door);
                } else {
                    return false;
                }
            }
            if (door.isOpen()) {
                World.spawnObject(new GameObject(objLoc, objectId, 0, door.getMapObject().getRotation() - 4));
                door.setOpen(false);
            } else {
                World.spawnObject(new GameObject(objLoc, objectId, 0, door.getMapObject().getRotation() - 1));
                door.setOpen(true);
            }
        }
        return false;
    }

}