package com.hyperion.game.world.gameobjects;

import com.hyperion.Logger;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.doors.Door;
import com.hyperion.game.world.gameobjects.doors.Door.DoorType;
import com.hyperion.utility.XStreamUtil;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DoorManager {

    private static Map<Location, Door> doors = new HashMap<Location, Door>();

    @SuppressWarnings("unchecked")
    public static void init() {
        try {
            List<Door> list = (List<Door>) XStreamUtil.getXStream().fromXML(new FileInputStream("data/world/objects/doors.xml"));
            for (Door d : list) {
                if (d.getType() == DoorType.NORMAL || d.getType() == DoorType.NORMALFORCE) {
                    doors.put(d.getOpenLocation(), d);
                    doors.put(d.getClosedLocation(), d);
                } else {
                    doors.put(d.getOpenLocation(), d);
                    doors.put(d.getClosedLocation(), d);
                    doors.put(d.getSecondOpenLocation(), d);
                    doors.put(d.getSecondClosedLocation(), d);
                }
            }
            Logger.getInstance().warning("Loaded " + list.size() + " doors.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Handles the door click
     *
     * @param player
     * @param object
     * @return
     */
    public static boolean handleDoor(final Player player, final GameObject object) {
        final Door door = doors.get(object.getLocation());
        if (door == null) {
            return false;
        }
        System.out.println("Door is not null attempting to handle");
        if (door.isOpen()) {
            System.out.println(door.getType());
            switch (door.getType()) {
                case NORMAL:
                    GameObject openedDoor = World.getObjectWithId(door.getOpenLocation(), door.getOpenId());
                    if (openedDoor != null) {
                        World.removeObject(openedDoor);
                    }
                    GameObject closedDoor = new GameObject(door.getClosedLocation(), door.getClosedId(), door.getClosedType(), door.getClosedFace());
                    World.spawnObject(closedDoor);
                    door.redoOpenState();
                    break;
                case NORMALFORCE:
                    // Do nothing.
                    break;
                case DOUBLE:
                    // remove current doors, which are open.
                    GameObject firstOpenDoor = World.getObjectWithId(door.getOpenLocation(), door.getOpenId());
                    if (firstOpenDoor != null) {
                        World.removeObject(firstOpenDoor);
                    }
                    GameObject secondOpenDoor = World.getObjectWithId(door.getSecondOpenLocation(), door.getSecondaryOpenId());
                    if (secondOpenDoor != null) {
                        World.removeObject(secondOpenDoor);
                    }

                    // add default doors, which are closed.

                    // 1st one
                    GameObject firstClosedDoor = new GameObject(door.getClosedLocation(), door.getClosedId(), door.getClosedType(), door.getClosedFace());
                    World.spawnObject(firstClosedDoor);

                    // 2nd one
                    GameObject secondClosedDoor = new GameObject(door.getSecondClosedLocation(), door.getSecondaryClosedId(), door.getSecondaryClosedType(), door.getSecondaryClosedFace());
                    World.spawnObject(secondClosedDoor);
                    door.redoOpenState();
                    break;
                case DOUBLEFORCE:
                    // TODO: Nothing.
                    break;
            }
        } else {
            switch (door.getType()) {
                case NORMAL:
                    GameObject singleClosedDoor = World.getObjectWithId(door.getClosedLocation(), door.getClosedId());
                    if (singleClosedDoor != null) {
                        World.removeObject(singleClosedDoor);
                    }
                    GameObject openedDoor = new GameObject(door.getOpenLocation(), door.getOpenId(), door.getOpenType(), door.getOpenFace());
                    World.spawnObject(openedDoor);
                    door.redoOpenState();
                    break;
                case NORMALFORCE:
                    if (door.getClosedFace() == 0) { // east and west door
                        Location west = Location.create(door.getClosedLocation().getX() - 1, door.getClosedLocation().getY(), door.getClosedLocation().getZ());
                        Location east = Location.create(door.getClosedLocation().getX(), door.getClosedLocation().getY(), door.getClosedLocation().getZ());
                        player.getAttributes().set("stopActions", true);
                        // player.getWalkingQueue().reset();
                        if (player.getLocation().equals(east))
                            player.getWalkingQueue().addStep(west);
                        else
                            player.getWalkingQueue().addStep(east);

                        final GameObject singleClosedDoor1 = World.getObjectWithId(door.getClosedLocation(), door.getClosedId());
                        if (singleClosedDoor1 != null) {
                            World.removeObject(singleClosedDoor1);
                        }
                        final GameObject openedDoor1 = new GameObject(door.getOpenLocation(), door.getOpenId(), door.getOpenType(), door.getOpenFace());
                        World.spawnObject(openedDoor1);

                        player.getWalkingQueue().finish();
                        World.getWorld().submit(new Tickable(2) {
                            @Override
                            public void execute() {
                                World.removeObject(openedDoor1);
                                World.spawnObject(singleClosedDoor1);
                                player.getAttributes().remove("stopActions");

                                this.stop();
                            }
                        });
                    }
                    break;
                case DOUBLE:
                    GameObject closedDoor = World.getObjectWithId(door.getClosedLocation(), door.getClosedId());
                    if (closedDoor != null) {
                        World.removeObject(closedDoor);
                    }
                    GameObject secondaryClosedDoor = World.getObjectWithId(door.getSecondClosedLocation(), door.getSecondaryClosedId());
                    if (secondaryClosedDoor != null) {
                        World.removeObject(secondaryClosedDoor);
                    }
                    // new doors

                    // 1st
                    GameObject firstNewOpenedDoor = new GameObject(door.getOpenLocation(), door.getOpenId(), door.getOpenType(), door.getOpenFace());
                    World.spawnObject(firstNewOpenedDoor);
                    // 2nd
                    GameObject secondNewOpenDoor = new GameObject(door.getSecondOpenLocation(), door.getSecondaryOpenId(), door.getSecondaryOpenType(), door.getSecondaryOpenFace());
                    World.spawnObject(secondNewOpenDoor);
                    door.redoOpenState();
                    break;
            }

        }
        return true;
    }
}