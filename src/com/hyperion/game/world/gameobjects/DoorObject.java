package com.hyperion.game.world.gameobjects;

/**
 * Door object which holds the map object and the status of the door
 */
public final class DoorObject {

    private GameObject mapObject;
    private boolean isOpen = false;

    /**
     * Create a new LadderObject
     *
     * @param mapObject the MapObject corresponding to this Door
     */
    public DoorObject(GameObject mapObject) {
        this.mapObject = mapObject;
    }

    /**
     * Set the door open or closed
     *
     * @param isOpen true open false closed
     */
    public void setOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    /**
     * Get if this door is open or closed
     *
     * @return true open false closed
     */
    public boolean isOpen() {
        return isOpen;
    }

    /**
     * The MapObject holding details of the door
     *
     * @return MapObject
     */
    public GameObject getMapObject() {
        return mapObject;
    }

}