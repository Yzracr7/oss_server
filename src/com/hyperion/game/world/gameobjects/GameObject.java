package com.hyperion.game.world.gameobjects;

import com.hyperion.cache.definitions.CachedObjectDefinition;
import com.hyperion.game.content.skills.mining.MiningData;
import com.hyperion.game.content.skills.woodcutting.WoodcuttingData;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.Projectiles;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.region.Region;

public class GameObject {

    /**
     * The location.
     */
    private Location location;

    /**
     * The definition.
     */
    private CachedObjectDefinition definition;

    /**
     * The type.
     */
    private int type;

    /**
     * The name
     */
    private String name;

    /**
     * The objects interactions
     */
    private String[] interactions;

    /**
     * The rotation.
     */
    private int rotation;

    /**
     * object id
     */
    private int id;

    private boolean rangable;

    /*
     * For resource objects such as trees, health until it runs out.
     */
    private int health;

    private boolean alive;

    /**
     * Creates the game object.
     *
     * @param location The location.
     * @param type     The type.
     * @param rotation The rotation.
     */
    public GameObject(Location location, int objId, int type, int rotation) {
        if (objId == -1) {
            this.location = location;
            this.id = objId;
            this.type = type;
            this.rotation = rotation;
            return;
        }
        this.id = objId;
        this.type = type;
        this.rotation = rotation;
        this.location = location;
        this.definition = CachedObjectDefinition.forId(objId);
        if (this.definition == null) {
            return;
        }
        this.name = this.definition.name;
        this.interactions = this.definition.getInteractions();
        this.rangable = this.definition.rangableObject();
//		regenerateHealth();
    }

    /**
     * Gets the location.
     *
     * @return The location.
     */
    public Location getLocation() {
        return location;
    }

    public int getPlane() {
        return location.getZ();
    }

    /**
     * Gets the definition.
     *
     * @return The definition.
     */
    public CachedObjectDefinition getDefinition() {
        return definition;
    }

    /**
     * Gets the type.
     *
     * @return The type.
     */
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    /**
     * Gets the rotation.
     *
     * @return The rotation.
     */
    public int getRotation() {
        return rotation;
    }

    public void setRotation(int rotation) {
        this.rotation = rotation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Location getCentreLocation() {
        return Location.create(getLocation().getX() + (getWidth() / 2), getLocation().getY() + (getHeight() / 2), getLocation().getZ());
    }

    public int getHeight() {
        if (definition == null) {
            return 1;
        }
        return definition.sizeY;
    }

    public int getWidth() {
        if (definition == null) {
            return 1;
        }
        return definition.sizeX;
    }

    public int getSizeX(int rotation) {
        if (rotation == 1 || rotation == 3) {
            return definition.sizeY;
        } else {
            return definition.sizeX;
        }
    }

    public int getSizeY(int rotation) {
        if (rotation == 1 || rotation == 3) {
            return definition.sizeX;
        } else {
            return definition.sizeY;
        }
    }

    public int getSizeX() {
        return definition.sizeX;
    }

    public int getSizeY() {
        return definition.sizeY;
    }

    public GameObject copy() {
        GameObject obj = new GameObject(location, id, type, rotation);
        return obj;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getInteractions() {
        return interactions;
    }

    public void setInteractions(String[] interactions) {
        this.interactions = interactions;
    }

    @Override
    public String toString() {
        return "ID: " + getId() + ", Name: " + getName() + ", Loc: " + getLocation() + ", direction: " + getRotation() + ", type: " + getType() + ", region: " + location.getRegionId() + ".";
    }

    public boolean isRangable() {
        System.out.println("Rangable ? " + "ID: " + getId() + ", Name: " + getName() + ", Loc: " + getLocation() + ", direction: " + getRotation() + ", type: " + getType() + ", region: " + location.getRegionId() + ".");
        return rangable;
    }

    public void setRangable(boolean rangable) {
        this.rangable = rangable;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getHealth() {
        return this.health;
    }

    public void deductHealth(int amt) {
        int newHealth = health - amt;
        this.health = newHealth <= 0 ? 0 : newHealth;
    }

    public void setAlive(boolean a) {
        this.alive = a;
    }

    public boolean isAlive() {
        return this.alive;
    }

    public void regenerateHealth() {
        int rockIndex = MiningData.getRockIndex(this.getId());
        int treeIndex = WoodcuttingData.getIndex(this);
        this.health = treeIndex != -1 ? WoodcuttingData.getHealth(this) : this.name.toLowerCase().contains("s99tar") ? 100 : rockIndex != -1 ? MiningData.ROCK_HEALTH[rockIndex] : -1;
        this.alive = (treeIndex != -1 || this.name.toLowerCase().contains("rocks"));
    }
}