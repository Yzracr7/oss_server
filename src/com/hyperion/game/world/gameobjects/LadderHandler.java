package com.hyperion.game.world.gameobjects;

import com.hyperion.game.content.Ladders;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.player.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * Basic ladder system, adios ladders.cfg!
 *
 * @author trees
 * @author stuart
 */
public class LadderHandler {

    /**
     * Id's of each ladder object
     */
    private static final int[] LADDERS = {2641, 16679, 17384, 17385, 1579, 11806, 881};

    /**
     * Map with the Location being mapped to the ladder, only ladders which have
     * been opened / closed to save a little bit of memory
     */
    private static Map<Location, LadderObject> ladders = new HashMap<Location, LadderObject>();

    /**
     * Is the given id an ladder?
     *
     * @param id the id to check
     * @return true yes false no
     */
    private static boolean isladder(int id) {
        for (int ladder : LADDERS) {
            if (id == ladder) {
                return true;
            }
        }
        return false;
    }

    /**
     * Handles opening and closing of ladders, returns true if it was handled
     * otherwise false nothing changed
     *
     * @param player   the player opening the ladder
     * @param objectId the object id being clicked
     * @param objLoc   the objects location
     * @return handled or not
     */
    public static boolean handleladder(Player player, int objectId,
                                       Location objLoc) {
        if (isladder(objectId)) {
            LadderObject ladder = ladders.get(objLoc);
            switch (objectId) {
                case 1579: // Edgeville Trapdoor
                    Ladders.executeLadder(player, true, 3096, 9867, 0);
                    break;
                case 17384: // Hill Giant Area Entrance
                    Ladders.executeLadder(player, true, 3117, 9852, 0);
                    break;
                case 2641: // Monestary Ladder Up
                    if (player.getLocation().withinActualDistance(new Location(3057, 3483, 0), 5)) {
                        Ladders.executeLadder(player, false, 3058, 3483, 1);
                    } else if (player.getLocation().withinActualDistance(new Location(3046, 3483, 0), 5)) {
                        Ladders.executeLadder(player, false, 3045, 3483, 1);
                    }
                    break;
                case 16679: // Monestary Ladder Down
                    if (player.getLocation().withinActualDistance(new Location(3057, 3483, 1), 5)) {
                        Ladders.executeLadder(player, true, 3058, 3483, 0);
                    } else if (player.getLocation().withinActualDistance(new Location(3046, 3483, 1), 5)) {
                        Ladders.executeLadder(player, true, 3045, 3483, 0);
                    }
                    break;
                case 17385: // Edgeville Dungeon Exit
                    if (player.getLocation().withinActualDistance(new Location(3116, 9852, 0), 2)) {
                        Ladders.executeLadder(player, false, 3115, 3452, 0);
                    } else if (player.getLocation().withinActualDistance(new Location(3096, 3468, 0), 2)) {
                        Ladders.executeLadder(player, false, 3096, 3468, 0);
                    } else if (player.getLocation().withinActualDistance(new Location(2884, 9797, 0), 4)) {
                        Ladders.executeLadder(player, false, 2884, 3396, 0);
                    }
                    break;
                case 881: // Manhole
                    if (player.getLocation().withinActualDistance(new Location(3237, 3458, 0), 2)) {
                        Ladders.executeLadder(player, true, 3237, 9858, 0);
                    }
                    break;
                case 11806: // Sewer Ladder Exit
                    if (player.getLocation().withinActualDistance(new Location(3237, 9858, 0), 2)) {
                        Ladders.executeLadder(player, false, 3237, 3459, 0);
                    }
                    break;
                default:
                    player.sendMessage("Ladder exists but location is null");
                    break;
            }
        }
        return false;
    }

    /**
     * ladder object which holds the map object and the status of the ladder
     */
    private static final class LadderObject {

        private GameObject mapObject;

        private boolean traverse_up = false;

        /**
         * Create a new LadderObject
         *
         * @param mapObject the MapObject corresponding to this ladder
         */
        public LadderObject(GameObject mapObject) {
            this.mapObject = mapObject;
        }

        /**
         * Set the ladder to put you up or down
         *
         * @param isOpen true open false closed
         */
        public void setTraverse_up(boolean isOpen) {
            this.traverse_up = isOpen;
        }

        /**
         * Get if this ladder to put you up or down
         *
         * @return true open false closed
         */
        public boolean isTraverse_up() {
            return traverse_up;
        }

        /**
         * The MapObject holding details of the ladder
         *
         * @return MapObject
         */
        public GameObject getMapObject() {
            return mapObject;
        }

    }
}