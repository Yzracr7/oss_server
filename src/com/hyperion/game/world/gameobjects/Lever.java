package com.hyperion.game.world.gameobjects;

import com.hyperion.game.world.Location;

public class Lever {

	private Location leverLocation;
	private boolean inUse;
	private int id;
	private Location faceLocation;
	private Location teleLocation;
	private String endMessage = "";
	
	public Lever(int id, Location loc, Location faceLocation, Location teleLoc, String endMessage) {
		this.id = id;
		this.leverLocation = loc;
		this.faceLocation = faceLocation;
		this.teleLocation = teleLoc;
		this.inUse = false;
		this.endMessage = endMessage;
	}
	
	public Location getTeleLocation() {
		return teleLocation;
	}

	public Location getFaceLocation() {
		return faceLocation;
	}

	public int getId() {
		return id;
	}

	public boolean isInUse() {
		return inUse;
	}

	public void setInUse(boolean inUse) {
		this.inUse = inUse;
	}

	public Location getLeverLocation() {
		return leverLocation;
	}

	public String getEndMessage() {
		return endMessage;
	}

	public void setEndMessage(String endMessage) {
		this.endMessage = endMessage;
	}
}