package com.hyperion.game.world.gameobjects;

import com.hyperion.cache.definitions.CachedObjectDefinition;
import com.hyperion.game.Constants;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.combat.data.CombatAnimations;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author Nando
 */
public class ObjectManager {

    public static List<Lever> levers = new ArrayList<Lever>();

    public ObjectManager() throws Exception {
        // DoorManager.init();
        // if (PACK_OBJECT_SPAWNS)
        //loadObjects();
        loadLevers();
    }

    public static boolean loadObjects() {
        // IF THIS DOESN'T WORK: only 1 thing to do...
        // Load the removeObjects into an arraylist
        // everytime player loads a new region remove them, I guess...
        String line = "";
        String token = "";
        String token2 = "";
        String token2_2 = "";
        String[] token3 = new String[10];
        boolean EndOfFile = false;
        @SuppressWarnings("unused")
        int ReadMode = 0;
        BufferedReader characterfile = null;
        try {
            characterfile = new BufferedReader(new FileReader("./data/objects.cfg"));
        } catch (FileNotFoundException fileex) {
            return false;
        }
        try {
            line = characterfile.readLine();
        } catch (IOException ioexception) {
            if (characterfile != null)
                try {
                    characterfile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            return false;
        }
        int amount = 0;
        while (EndOfFile == false && line != null) {
            line = line.trim();
            int spot = line.indexOf("=");

            if (spot > -1) {
                token = line.substring(0, spot);
                token = token.trim();
                token2 = line.substring(spot + 1);
                token2 = token2.trim();
                token2_2 = token2.replaceAll("\t\t", "\t");
                token2_2 = token2_2.replaceAll("\t\t", "\t");
                token2_2 = token2_2.replaceAll("\t\t", "\t");
                token2_2 = token2_2.replaceAll("\t\t", "\t");
                token2_2 = token2_2.replaceAll("\t\t", "\t");
                token2_2 = token2_2.replaceAll("\t\t", "\t");
                token2_2 = token2_2.replaceAll("\t\t", "\t");
                token3 = token2_2.split("\t");
                if (token.equals("object")) {
                    amount++;
                    int id = Integer.parseInt(token3[0]);
                    Location location = Location.create(Integer.parseInt(token3[1]), Integer.parseInt(token3[2]), Integer.parseInt(token3[3]));
                    int face = Integer.parseInt(token3[4]);
                    int type = Integer.parseInt(token3[5]);
                    int spawn = Integer.parseInt(token3[6]);
                    // packRegionObjectSpawns(id, type, face,
                    // location.getRegionId(), location, delete == 0);
                    // if(delete == 0) {
                    // World.getWorld().removeObject(new GameObject(location,
                    // id, type, face));
                    // } else
                    // World.getWorld().spawnObject(new GameObject(location, id,
                    // type, face));
                    GameObject object = new GameObject(location, id, type, face);
                    if (spawn == 1) {
                        World.spawnObject(object);
                    } else {
                        // World.removeObject(object);
                        int region = location.getRegionId();
                        if (World.getWorld().getObjectsToDelete().get(region) == null)
                            World.getWorld().getObjectsToDelete().put(region, new CopyOnWriteArrayList<GameObject>());
                        World.getWorld().getObjectsToDelete().get(region).add(object);
                    }
                }
            } else {
                if (line.equals("[ENDOFLIST]")) {
                    try {
                        characterfile.close();
                        System.out.println("Loaded " + amount + " custom object spawns.");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            try {
                line = characterfile.readLine();
            } catch (IOException ioexception1) {
                EndOfFile = true;
            }
        }
        try {
            characterfile.close();
        } catch (IOException ioexception) {
        }
        return false;
    }

    public void loadLevers() {
        //System.out.println("Loading levers");
        Lever outsideMageBank = new Lever(5959, Location.create(3090, 3956, 0), Location.create(3089, 3956, 0), Location.create(2539, 4712, 0), "");
        levers.add(outsideMageBank);
        Lever insideMageBank = new Lever(5960, Location.create(2539, 4712, 0), Location.create(2539, 4711, 0), Location.create(3090, 3956, 0), "... and teleport out of the mage's cave.");
        levers.add(insideMageBank);
        Lever toEdge = new Lever(1815, Location.create(3153, 3923, 0), Location.create(3152, 3923, 0), Location.create(3091, 3475, 0), "");
        levers.add(toEdge);
        Lever fromEdge = new Lever(26761, Location.create(3091, 3475, 0), Location.create(3091, 3475, 0), Location.create(3153, 3923, 0), "");
        levers.add(fromEdge);
        Lever toKbd = new Lever(1816, Location.create(3067, 10253, 0), Location.create(3067, 10251, 0), Location.create(2271, 4680, 0), "");
        levers.add(toKbd);
        Lever fromKbd = new Lever(1817, Location.create(2271, 4680, 0), Location.create(2271, 4679, 0), Location.create(3067, 10253, 0), "");
        levers.add(fromKbd);
        Lever outsideMageArena = new Lever(9706, Location.create(3104, 3956, 0), Location.create(3104, 3956, 0), Location.create(3105, 3951, 0), "");
        levers.add(outsideMageArena);
        Lever insideMageArena = new Lever(9707, Location.create(3105, 3952, 0), Location.create(3105, 3952, 0), Location.create(3105, 3956, 0), "");
        levers.add(insideMageArena);
    }

    /**
     * Handles levers all around the game map
     *
     * @param player The player
     * @param object The object
     */
    public static boolean executeLever(final Player player, final GameObject object) {
        for (Lever lever : levers) {
            if (lever.getId() == object.getId() && lever.getLeverLocation().equals(object.getLocation())) {
                final Lever l = lever;
                player.getPacketSender().sendMessage("You pull the lever...");
                player.getAttributes().set("stopActions", true);
                player.face(l.getFaceLocation());
                player.animate(2140);
                player.getInterfaceSettings().closeInterfaces(true);
                player.getWalkingQueue().reset();
                // To kbd is different i think..
                if (!object.getLocation().equals(Location.create(3067, 10252, 0))) {
                    World.replaceResourceObject(object, 2, player, 161);
                }
                World.getWorld().submit(new Tickable(player, 2) {
                    @Override
                    public void execute() {
                        this.stop();
                        if (player.getCombatState().isTeleBlocked()) {
                            player.getAttributes().remove("stopActions");
                            player.getAttributes().remove("stopMovement");
                            player.getPacketSender().sendMessage("A teleport block has been cast on you!");
                            return;
                        }
                        player.animate(714);
                        player.playGraphic(301, 0, 80);
                        World.getWorld().submit(new Tickable(player, 4) {
                            @Override
                            public void execute() {
                                this.stop();
                                player.teleport(l.getTeleLocation());
                                player.animate(65535);
                                if (!l.getEndMessage().isEmpty()) {
                                    player.getPacketSender().sendMessage(l.getEndMessage());
                                }
                                World.getWorld().submit(new Tickable(player, 2) {
                                    @Override
                                    public void execute() {
                                        this.stop();
                                        player.getAttributes().remove("stopActions");
                                    }
                                });
                            }
                        });
                    }
                });
                return true;
            }
        }
        return false;
    }

    /**
     * Slash webs
     *
     * @param player
     * @param originalWebs
     */
    public static void slashWebs(Player player, GameObject originalWebs) {
        player.animate(CombatAnimations.getAttackAnim(player, player.getEquipment().getItemInSlot(3)));
        int chance = NumberUtils.random(3);
        if (chance < 2) {
            player.getPacketSender().sendMessage("You fail to cut through it.");
            return;
        }
        player.getPacketSender().sendMessage("You slash the web apart.");
        final GameObject slashedWebs = new GameObject(originalWebs.getLocation(), Constants.USE_DEV_CACHE ? 734 : CachedObjectDefinition.renderIds[734], originalWebs.getType(), originalWebs.getRotation());
        World.spawnObjectTemporary(slashedWebs, 100);
    }

    public static void deleteObjectsForRegion(int region) {
        if (World.getWorld().getObjectsToDelete().get(region) == null)
            return;
        System.out.println("Removing objects: " + region);
        for (GameObject toRemove : World.getWorld().getObjectsToDelete().get(region)) {
            // System.out.println("Removing object id " + toRemove.getId() +
            // " at " + toRemove.getLocation().toString());
            World.removeObject(toRemove);
            World.getWorld().getObjectsToDelete().get(region).remove(toRemove);
        }
        // if(World.getWorld().getObjectsToDelete().get(region).size() <= 0)
        World.getWorld().getObjectsToDelete().get(region).clear();
    }
}
