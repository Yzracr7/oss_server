package com.hyperion.game.world.gameobjects;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.masks.ForceMovement;
import com.hyperion.game.world.entity.masks.UpdateFlags;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;

public class Obstacle {

    /**
     * Edgeville Grand Exchange Tunnel Shortcut.. ect
     *
     * @param player
     */
    public static void executeEdgevilleTunnelObstacle(Player player) {
        Location loc = player.getLocation();
        player.getPacketSender().sendMessage("You squeeze into the pipe...");
        execute_force_movement(player, Location.create(loc.getX(), loc.getY(), 0), 10580, -1, 0, ForceMovement.EAST, 30, 60, false, -1, null);
        execute_force_walking(player, 844, loc.getX() + 1, loc.getY(), 3, 1, true, -1, null);
        execute_teleport(player, -1, Location.create(3142, 3513, 0), 5, 1, -1, null);
    }

    protected static void execute_force_walking(final Player player, final int animation, final int endX, final int endY, int delayTillStart, final int delayTillDone, final boolean removeAttribute, final double exp, final String endMessage) {
        player.getAttributes().set("stopActions", true);
        if (delayTillStart != -1) {
            World.getWorld().submit(new Tickable(delayTillStart) {
                @Override
                public void execute() {
                    this.stop();
                    if (animation != -1) {
                        player.getVariables().setWalkAnimation(animation);
                        player.getVariables().setRunAnimation(animation);
                        player.getVariables().setStandAnimation(animation);
                        player.getVariables().setTurn180Animation(animation);
                        player.getVariables().setTurn90Clockwise(animation);
                        player.getVariables().setTurn90CounterClockwise(animation);
                        player.getVariables().setTurnAnimation(animation);
                        player.getUpdateFlags().flag(UpdateFlags.UpdateFlag.APPEARANCE);
                    }
                    player.getWalkingQueue().reset();
                    player.getWalkingQueue().addStep(endX, endY);
                    player.getWalkingQueue().finish();
                    World.getWorld().submit(new Tickable(delayTillDone) {
                        @Override
                        public void execute() {
                            this.stop();
                            player.getVariables().setWalkAnimation(-1);
                            player.getVariables().setRunAnimation(-1);
                            player.getVariables().setStandAnimation(-1);
                            player.getVariables().setTurn180Animation(-1);
                            player.getVariables().setTurn90Clockwise(-1);
                            player.getVariables().setTurn90CounterClockwise(-1);
                            player.getVariables().setTurnAnimation(-1);
                            player.getUpdateFlags().flag(UpdateFlags.UpdateFlag.APPEARANCE);
                            if (removeAttribute) {
                                player.getAttributes().remove("stopActions");
                            }
                            if (endMessage != null) {
                                player.getPacketSender().sendMessage(endMessage);
                            }
                            if (exp != -1) {
                                player.getSkills().addExperience(Skills.AGILITY, exp);
                            }
                        }
                    });
                }
            });
        } else {
            if (animation != -1) {
                player.getVariables().setWalkAnimation(animation);
                player.getVariables().setRunAnimation(animation);
                player.getVariables().setStandAnimation(animation);
                player.getVariables().setTurn180Animation(animation);
                player.getVariables().setTurn90Clockwise(animation);
                player.getVariables().setTurn90CounterClockwise(animation);
                player.getVariables().setTurnAnimation(animation);
                player.getUpdateFlags().flag(UpdateFlags.UpdateFlag.APPEARANCE);
            }
            player.getWalkingQueue().reset();
            player.getWalkingQueue().addStep(endX, endY);
            player.getWalkingQueue().finish();
            World.getWorld().submit(new Tickable(delayTillDone) {
                @Override
                public void execute() {
                    this.stop();
                    player.getVariables().setWalkAnimation(-1);
                    player.getVariables().setRunAnimation(-1);
                    player.getVariables().setStandAnimation(-1);
                    player.getVariables().setTurn180Animation(-1);
                    player.getVariables().setTurn90Clockwise(-1);
                    player.getVariables().setTurn90CounterClockwise(-1);
                    player.getVariables().setTurnAnimation(-1);
                    player.getUpdateFlags().flag(UpdateFlags.UpdateFlag.APPEARANCE);
                    if (removeAttribute) {
                        player.getAttributes().remove("stopActions");
                    }
                    if (endMessage != null) {
                        player.getPacketSender().sendMessage(endMessage);
                    }
                    if (exp != -1) {
                        player.getSkills().addExperience(Skills.AGILITY, exp);
                    }
                }
            });
        }
    }

    protected static void execute_teleport(final Player player, final int animation, final Location target, int delayTillStart, final int delayTillDone, final double exp, final String endMessage) {
        player.getAttributes().set("stopActions", true);
        if (delayTillStart != -1) {
            World.getWorld().submit(new Tickable(delayTillStart) {
                @Override
                public void execute() {
                    this.stop();
                    player.animate(animation);
                    World.getWorld().submit(new Tickable(delayTillDone) {
                        @Override
                        public void execute() {
                            this.stop();
                            player.teleport(target);
                            player.getAttributes().remove("stopActions");
                            if (endMessage != null) {
                                player.getPacketSender().sendMessage(endMessage);
                            }
                            if (exp != -1) {
                                player.getSkills().addExperience(Skills.AGILITY, exp);
                            }
                        }
                    });
                }
            });
        } else {
            player.animate(animation);
            World.getWorld().submit(new Tickable(delayTillDone) {
                @Override
                public void execute() {
                    this.stop();
                    player.teleport(target);
                    player.getAttributes().remove("stopActions");
                    if (endMessage != null) {
                        player.getPacketSender().sendMessage(endMessage);
                    }
                    if (exp != -1) {
                        player.getSkills().addExperience(Skills.AGILITY, exp);
                    }
                }
            });
        }
    }

    protected static void execute_force_movement(final Player player, final Location target, final int animation, int delayTillStart, final int delayTillDone, final int dir, final int first_speed, final int second_speed, final boolean removeAttribute, final double exp, final String endMessage) {
        player.getAttributes().set("stopActions", true);
        if (delayTillStart != -1) {
            World.getWorld().submit(new Tickable(delayTillStart) {
                @Override
                public void execute() {
                    this.stop();
                    player.animate(animation);
                    player.getVariables().setNextForceMovement(new ForceMovement(player.getLocation(), first_speed, target, second_speed, dir));
                    player.getUpdateFlags().flag(UpdateFlags.UpdateFlag.FORCE_MOVEMENT);
                    World.getWorld().submit(new Tickable(delayTillDone) {
                        @Override
                        public void execute() {
                            this.stop();
                            player.teleport(target);
                            if (animation == 9908) {
                                player.animate(-1);
                            }
                            if (removeAttribute) {
                                player.getAttributes().remove("stopActions");
                            }
                            if (endMessage != null) {
                                player.getPacketSender().sendMessage(endMessage);
                            }
                            if (exp != -1) {
                                player.getSkills().addExperience(Skills.AGILITY, exp);
                            }
                        }
                    });
                }
            });
        } else {
            player.animate(animation);
            player.getVariables().setNextForceMovement(new ForceMovement(player.getLocation(), first_speed, target, second_speed, dir));
            player.getUpdateFlags().flag(UpdateFlags.UpdateFlag.FORCE_MOVEMENT);
            World.getWorld().submit(new Tickable(delayTillDone) {
                @Override
                public void execute() {
                    this.stop();
                    player.teleport(target);
                    player.animate(-1);
                    if (removeAttribute) {
                        player.getAttributes().remove("stopActions");
                    }
                    if (endMessage != null) {
                        player.getPacketSender().sendMessage(endMessage);
                    }
                    if (exp != -1) {
                        player.getSkills().addExperience(Skills.AGILITY, exp);
                    }
                }
            });
        }
    }
}
