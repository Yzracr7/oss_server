package com.hyperion.game.world.gameobjects;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.masks.ForceMovement;
import com.hyperion.game.world.entity.masks.UpdateFlags.UpdateFlag;
import com.hyperion.game.world.entity.player.Player;
/**
 * @author Nando
 */
public class WildernessDitch {

	public static void jump(final Player player, final GameObject ditch) {
		player.getAttributes().set("stopActions", true);
		final Location playerLoc = player.getLocation();
		player.animate(6132);
		Location before_ditch = Location.create(ditch.getLocation().getX(), ditch.getLocation().getY() - 1, 0);
		Location after_ditch = Location.create(ditch.getLocation().getX(), ditch.getLocation().getY() + 2, 0);
		final Location toTile = playerLoc.equals(before_ditch) ? after_ditch : before_ditch;
		final int dir = playerLoc.getY() >= 3523 ? ForceMovement.SOUTH : ForceMovement.NORTH;
		player.getVariables().setNextForceMovement(new ForceMovement(playerLoc, 33, toTile, 60, dir));
	    player.getUpdateFlags().flag(UpdateFlag.FORCE_MOVEMENT);
	    World.getWorld().submit(new Tickable(2) {
			@Override
			public void execute() {
				player.getAttributes().remove("stopActions");
				player.teleport(toTile);
				this.stop();
			}
		});
	}
}
