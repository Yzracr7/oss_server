package com.hyperion.game.world.gameobjects;

import java.util.List;

import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.utility.NumberUtils;

public class WildernessObelisks {

	public WildernessObelisks() {
		
	}
	
	private static final int ACTIVATED_ID = 14825;
	private static boolean[] obeliskActivated = new boolean[6];
	
	/*
	 * These are the MIDDLE of the platform, since the activators are all 2 squares from the center.
	 */
	private static final int[][] OBELISK_LOCATIONS = {
		{2980, 3866, 0}, // 44 wild
		{3035, 3732, 0}, // 27 wild
		{3106, 3794, 0}, // 35 wild
		{3156, 3620, 0}, // 13 wild
		{3227, 3667, 0}, // 19 wild
		{3307, 3916, 0}, // 50 wild
	};
	
	private static final int[] OBELISK_ID = {
		14826,// 44 wild
		14827,// 27 wild
		14828,// 35 wild
		14829,// 13 wild
		14830,// 19 wild
		14831,// 50 wild
	};
	
	public static boolean usingObelisk(Player player, GameObject object) {
		int index = object.getId() - 14826;
		if (index > OBELISK_ID.length || index < 0) {
			return false;
		}
		activateObelisk(player, index);
		return true;
	}
	
	private static void activateObelisk(final Player player, final int index) {
		if (obeliskActivated[index]) {
			return;
		}
		obeliskActivated[index] = true;
		final Location[] obeliskLocations = getLocations(index);
		int randomized = NumberUtils.random(4);
		for (int i = 0; i < 4; i++) {
			GameObject oldObelisks = World.getObjectWithId(obeliskLocations[i], OBELISK_ID[index]);
			if (oldObelisks != null) {
				World.spawnObjectTemporary(new GameObject(oldObelisks.getLocation(), ACTIVATED_ID, oldObelisks.getType(), oldObelisks.getRotation()), 7 + randomized);
			}
		}
		World.getWorld().submit(new Tickable(7 + randomized) {
			@Override
			public void execute() {
				this.stop();
				int randomOb = index;
				while(randomOb == index) {
					// While loop so if the random one is the same one, it picks a new one
					randomOb = (int) (Math.random() * OBELISK_ID.length);
				}
				final int random = randomOb;
				for (int regionId : player.getMapRegionsIds()) {
					List<Integer> playersIndexes = World.getWorld().getRegion(regionId, false).getPlayerIndexes();
					if (playersIndexes == null)
						continue;
					for (Integer playerIndex : playersIndexes) {
						Player p = World.getWorld().getPlayer(playerIndex);
						if (p.getLocation().inArea(OBELISK_LOCATIONS[index][0] - 1, OBELISK_LOCATIONS[index][1] - 1, OBELISK_LOCATIONS[index][0] + 1, OBELISK_LOCATIONS[index][1] + 1)) {
							if (p.getCombatState().isTeleBlocked()) {
								p.getPacketSender().sendMessage("You're teleblocked and cannot travel with obelisks.");
								continue;
							}
							p.getAttributes().set("stopActions", true);
							for (int i = 0; i < 2; i++) {
								for (Location graphicLocs : getGfxLocations(index, i)) {
									player.executeRegionStillGraphic(graphicLocs, 342);
								}
							}
							p.animate(1816);
							final Player p2 = p;
							World.getWorld().submit(new Tickable(2) {
								@Override
								public void execute() {
									this.stop();
									p2.teleport(Location.create((OBELISK_LOCATIONS[random][0] - 1) + NumberUtils.random(2), (OBELISK_LOCATIONS[random][1] - 1) + NumberUtils.random(2), 0));
									p2.animate(-1);
									World.getWorld().submit(new Tickable(1) {
										@Override
										public void execute() {
											this.stop();
											p2.getAttributes().remove("stopActions");
										}
									});
								}
							});
						}
					}
				}
				obeliskActivated[index] = false;
			}
		});
	}
	
	private static Location[] getLocations(int index) {
		Location[] loc = new Location[4];
		int x = OBELISK_LOCATIONS[index][0];
		int y = OBELISK_LOCATIONS[index][1];
		loc[0] = Location.create(x - 2, y - 2, 0); // SW
		loc[1] = Location.create(x - 2, y + 2, 0); // NW
		loc[2] = Location.create(x + 2, y + 2, 0); // NE
		loc[3] = Location.create(x + 2, y - 2, 0); // SE
		return loc;
	}
	
	private static Location[] getGfxLocations(int index, int i) {
		Location[] loc = new Location[4];
		int x = OBELISK_LOCATIONS[index][0];
		int y = OBELISK_LOCATIONS[index][1];
		loc[0] = Location.create(x - i, y - i, 0); // SW
		loc[1] = Location.create(x - i, y + i, 0); // NW
		loc[2] = Location.create(x + i, y + i, 0); // NE
		loc[3] = Location.create(x + i, y - i, 0); // SE
		return loc;
	}
}
