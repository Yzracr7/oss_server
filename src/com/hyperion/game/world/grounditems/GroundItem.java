package com.hyperion.game.world.grounditems;

import com.hyperion.game.item.Item;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.player.Player;

public class GroundItem extends Item {

	private Location location;
	private String owner;
	private int spawnDelay = 100;
	private int removalDelay = 100;
	private boolean isGlobal;
	private boolean registered = true;

	public GroundItem(Item item, Player owner) {
		super(item.getId(), item.getCount());
		this.owner = owner.getName();
		this.location = owner.getLocation();
		this.registered = true;
	}

	public GroundItem(Item item, Location location, Player owner) {
		super(item.getId(), item.getCount());
		this.location = location;
		this.owner = owner.getName();
		this.registered = true;
	}

	public GroundItem(Item item, Entity dropUnder, Player owner) {
		super(item.getId(), item.getCount());
		this.owner = owner.getName();
		this.location = dropUnder.getLocation();
		this.registered = true;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(Player player) {
		this.owner = player.getName();
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location loc) {
		this.location = loc;
	}

	public boolean isGlobal() {
		return isGlobal;
	}

	public GroundItem setGlobal(boolean isGlobal) {
		this.isGlobal = isGlobal;
		return this;
	}

	/**
	 * Sets the item's registered value.
	 * 
	 * @param registered
	 *            The registered value to set.
	 */
	public void setRegistered(boolean registered) {
		this.registered = registered;
	}

	/**
	 * Gets the items registered value.
	 * 
	 * @return The items registered value.
	 */
	public boolean isRegistered() {
		return registered;
	}

	public boolean isOwnedBy(Player player) {
		return isGlobal() || owner.equalsIgnoreCase(player.getName());
	}

	public Item getItem() {
		return this;
	}

	/**
	 * The amount of ticks that must pass until this item is visible. If the
	 * {@link #spawnDelay} is -1, this item will be visible automatically on
	 * registration.
	 * 
	 * @return the timer
	 */
	public int getSpawnDelay() {
		return spawnDelay;
	}

	/**
	 * @param timer
	 *            the timer to set
	 */
	public GroundItem setSpawnDelay(int timer) {
		this.spawnDelay = timer;
		return this;
	}

	/**
	 * After this amount of ticks, this item will be removed from the game
	 * world. If the {@link #removalDelay} is -1, it will not be removed from
	 * the world until a player picks it up.
	 * 
	 * @return the removalDelay
	 */
	public int getRemovalDelay() {
		return removalDelay;
	}

	/**
	 * @param removalDelay
	 *            the removalDelay to set
	 */
	public GroundItem setRemovalDelay(int removalDelay) {
		this.removalDelay = removalDelay;
		return this;
	}
}
