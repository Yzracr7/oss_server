package com.hyperion.game.world.grounditems;

import com.hyperion.game.Constants;
import com.hyperion.game.item.Item;
import com.hyperion.game.tickable.Tickable;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.region.Region;
import com.hyperion.utility.logger.Log;

/**
 * @author Nando
 */
public class GroundItemManager {

    public static void pickup_item(GroundItem item, Player player) {
        if (item.getCount() <= 0) {
            player.getPacketSender().sendMessage("You have found a server bug. Please report it to a staff member. Item: " + item.getId() + ", count: " + item.getCount());
            return;
        }
        Location loc = item.getLocation();
        int regionId = loc.getRegionId();
        final GroundItem real_item = World.getWorld().getRegion(regionId, false).getGroundItem(item.getId(), loc, player);
        if (real_item == null)
            return;
        if (player.getInventory().addItem(real_item.getItem())) {
            player.getInventory().refresh();
            remove_item(real_item);
        }

        World.getWorld().loggingThread.appendLog(new Log(player.getName(), "Picked up " + item.getCount() + " x " + item.getDefinition().getName() + " (" + item.getId() + ") at " + item.getLocation().toString() + ". Item owner: " + item.getOwner()));
        if (!item.getOwner().equalsIgnoreCase(player.getName()))
            World.getWorld().loggingThread.appendLog(new Log(item.getOwner(), player.getName() + " picked up " + item.getCount() + " x " + item.getId() + ") at " + item.getLocation().toString() + ". Item owner: " + item.getOwner()));
    }

    public static void create(final GroundItem item, final Player player) {
        int id = item.getId();
        if (id > 4705 && id < 4760) {
            for (int q = 0; q < Constants.BROKEN_BARROWS.length; q++) {
                if (Constants.BROKEN_BARROWS[q][0] == id) {
                    id = Constants.BROKEN_BARROWS[q][1];
                    break;
                }
            }
        }
        if (id != item.getId())
            item.setItemId(id);
        if (item.getDefinition().isStackable()) {
            final GroundItem floorItem = World.getWorld().getRegion(item.getLocation().getRegionId(), false).getGroundItem(item.getId(), item.getLocation(), player);
            if (floorItem != null) {
                long existingItemCount = (long) floorItem.getCount();
                long newItemCount = (long) item.getCount();
                long total = existingItemCount + newItemCount;
                long remainder = 0;
                if (total > Constants.MAX_ITEMS) {
                    total = existingItemCount + (Constants.MAX_ITEMS - existingItemCount);
                    remainder = (existingItemCount + newItemCount) - Constants.MAX_ITEMS;
                }
                floorItem.setItemAmount((int) total);
                if (remainder > 0) {
                    if (player != null) {
                        player.getInventory().addItem(new Item(floorItem.getId(), (int) remainder));
                        player.getInventory().refresh();
                    }
                }
                player.getPacketSender().clearGroundItem(floorItem);
                player.getPacketSender().sendGroundItem(floorItem);
            } else {
                register_item(item, player);
            }
            return;
        }
        register_item(item, player);
    }

    private static void register_item(final GroundItem item, final Player player) {
        final Region region = World.getWorld().getRegion(item.getLocation().getRegionId(), false);
        region.getGroundItemsSafe().add(item);
        if (player != null) {
            player.getPacketSender().sendGroundItem(item);
            int timer = item.getSpawnDelay();
            //if (player.getDetails().isDonator()) {
            if (item.getDefinition().isPlayerBound()) {
                timer = 480;
            }
            //}
            if (item.getSpawnDelay() == -1) {
                if (!player.getDetails().isIronman()) {
                    public_stage(item);
                    return;
                } else {
                    removal_stage(item);
                }
            }
            if (!player.getDetails().isIronman()) {
                World.getWorld().submit(new Tickable(timer) {
                    @Override
                    public void execute() {
                        public_stage(item);
                        this.stop();
                    }
                });
            } else {
                removal_stage(item);
            }
        }
    }

    private static void public_stage(final GroundItem item) {
        int regionId = item.getLocation().getRegionId();
        final Region region = World.getWorld().getRegion(regionId, false);
        if (!region.getGroundItemsSafe().contains(item))
            return;
        Player owner = item.getOwner() != null ? World.getWorld().find_player_by_name(item.getOwner()) : null;
        if (item.getDefinition().isPlayerBound() || !item.getDefinition().isTradeable()) {
            region.getGroundItemsSafe().remove(item);
            if (owner != null) {
                if (owner.getMapRegionsIds().contains(regionId) && owner.getLocation().getZ() == item.getLocation().getZ())
                    owner.getPacketSender().clearGroundItem(item);
            }
            return;
        } else {
            item.setGlobal(true);
            for (Player players : World.getWorld().getPlayers()) {
                if (players == null || players == owner || players.getLocation().getZ() != item.getLocation().getZ() || !players.getMapRegionsIds().contains(regionId)) {
                    continue;
                }
                players.getPacketSender().sendGroundItem(item);
            }
            removal_stage(item);
        }
    }

    private static void removal_stage(final GroundItem item) {
        int removalDelay = item.getRemovalDelay();
        if (item.getRemovalDelay() == -1) {
            return;
        }
        if (item.getDefinition().isPlayerBound())
            removalDelay = 300;
        World.getWorld().submit(new Tickable(removalDelay) {
            @Override
            public void execute() {
                int regionId = item.getLocation().getRegionId();
                Region region = World.getWorld().getRegion(regionId, false);
                if (!region.getGroundItemsSafe().contains(item))
                    return;
                region.getGroundItemsSafe().remove(item);
                for (Player players : World.getWorld().getPlayers()) {
                    if (players == null || players.getLocation().getZ() != item.getLocation().getZ() || !players.getMapRegionsIds().contains(regionId)) {
                        continue;
                    }
                    players.getPacketSender().clearGroundItem(item);
                }
                this.stop();
            }
        });
    }

    private static void remove_item(GroundItem item) {
        int regionId = item.getLocation().getRegionId();
        Region region = World.getWorld().getRegion(regionId, false);
        if (!region.getGroundItemsSafe().contains(item))
            return;
        item.setRegistered(false);
        region.getGroundItemsSafe().remove(item);
        for (Player p2 : World.getWorld().getPlayers()) {
            if (p2 == null || p2.getLocation().getZ() != item.getLocation().getZ() || !p2.getMapRegionsIds().contains(regionId))
                continue;
            p2.getPacketSender().clearGroundItem(item);
        }
    }
}
