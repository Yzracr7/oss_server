package com.hyperion.game.world.lists;

public class RSWorld {
	
	private final String activity;
	
	private final int country;
	
	private final int flag;
	
	private final String ip;
	
	private final int location;
	
	private final String region;
	
	private final int id;
	
	public RSWorld(int id, int location, int flag, String activity,
			String ip, String region, int country) {
		this.id = id;
		this.location = location;
		this.flag = flag;
		this.activity = activity;
		this.ip = ip;
		this.region = region;
		this.country = country;
	}

	public String getActivity() {
		return activity;
	}

	public int getCountry() {
		return country;
	}

	public int getFlag() {
		return flag;
	}

	public String getIp() {
		return ip;
	}

	public int getLocation() {
		return location;
	}

	public String getRegion() {
		return region;
	}

	public int getId() {
		return id;
	}

}
