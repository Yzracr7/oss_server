package com.hyperion.game.world.lists;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import com.hyperion.game.net.Packet;
import com.hyperion.game.net.PacketBuilder;

/**
 * @author Sean
 */
public class WorldListManager extends WorldConstants {

	private static List<RSWorld> worldList = new ArrayList<RSWorld>();

	static {
		worldList.add(new RSWorld(1, 0, FLAG_MEMBERS, "Main Server", "127.0.0.1", "US", 0));
		//worldList.add(new RSWorld(2, 0, FLAG_NON_MEMBERS, "F2P Server", "127.0.0.1", "US", 0));
	}

	public static ByteBuffer getData(boolean worldConfiguration, boolean worldStatus) {
		ByteBuffer buffer = ByteBuffer.allocate(10240);
		buffer.put((byte) (worldStatus ? 1 : 0));
		buffer.put((byte) (worldConfiguration ? 1 : 0));
		if (worldConfiguration) {
			populateConfiguration(buffer);
		}
		if (worldStatus) {
			populateStatus(buffer);
		}
		buffer.flip();
		ByteBuffer finalBuffer = ByteBuffer.allocate(buffer.limit() + 3);
		finalBuffer.put((byte) 0);
		finalBuffer.putShort((short) buffer.limit());
		finalBuffer.put(buffer);
		finalBuffer.flip();
		return finalBuffer;
	}
	
	public static Packet sendWorlds(boolean worldConfiguration, boolean worldStatus) {
		ByteBuffer bb = WorldListManager.getData(worldConfiguration, worldStatus);
		PacketBuilder pb = new PacketBuilder();
		pb.put((byte[]) bb.flip().array());
		Packet p = pb.toPacket();
		return p;
	}

	private static void populateConfiguration(ByteBuffer buffer) {
		putSmart(buffer, worldList.size());
		setCountry(buffer);
		putSmart(buffer, 0);
		putSmart(buffer, (worldList.size() + 1));
		putSmart(buffer, worldList.size());
		for (RSWorld w : worldList) {
			putSmart(buffer, w.getId());
			buffer.put((byte) w.getLocation());
			buffer.putInt(w.getFlag());
			putJagString(buffer, w.getActivity());
			putJagString(buffer, w.getIp());
		}
		buffer.putInt(0x94DA4A87);
	}

	private static void populateStatus(ByteBuffer buffer) {
		for (RSWorld w : worldList) {
			putSmart(buffer, w.getId());
			//buffer.putShort((short) World.getWorld().getAmountOfPlayersInWorld(w.getId()));
		}
	}

	private static void putJagString(ByteBuffer buffer, String string) {
		buffer.put((byte) 0);
		buffer.put(string.getBytes());
		buffer.put((byte) 0);
	}

	private static void putSmart(ByteBuffer buffer, int value) {
		if ((value ^ 0xffffffff) > -129) {
			buffer.put((byte) value);
		} else {
			buffer.putShort((short) value);
		}
	}

	private static void setCountry(ByteBuffer buffer) {
		for (RSWorld w : worldList) {
			putSmart(buffer, w.getCountry());
			putJagString(buffer, w.getRegion());
		}
	}
}
