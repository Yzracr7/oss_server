package com.hyperion.game.world.pathfinders;

import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.pathfinders.Directions.NormalDirection;
import com.hyperion.utility.NumberUtils;

/**
 * @author Killamess
 */
public class DumbPathFinder {
	
	public static boolean blockedNorth(Location loc, Entity entity) {
		return !PrimitivePathFinder.canMove(entity, loc, NormalDirection.NORTH, entity.getSize(), false);
	}
	
	public static boolean blockedEast(Location loc, Entity entity) {
		return !PrimitivePathFinder.canMove(entity, loc, NormalDirection.EAST, entity.getSize(), false);
	}
	
	public static boolean blockedSouth(Location loc, Entity entity) {
		return !PrimitivePathFinder.canMove(entity, loc, NormalDirection.SOUTH, entity.getSize(), false);
	}
	
	public static boolean blockedWest(Location loc, Entity entity) {
		return !PrimitivePathFinder.canMove(entity, loc, NormalDirection.WEST, entity.getSize(), false);
    }
	
	public static boolean blockedNorthEast(Location loc, Entity entity) {
		return !PrimitivePathFinder.canMove(entity, loc, NormalDirection.NORTH_EAST, entity.getSize(), false);
    }
	
	public static boolean blockedNorthWest(Location loc, Entity entity) {
		return !PrimitivePathFinder.canMove(entity, loc, NormalDirection.NORTH_WEST, entity.getSize(), false);
    }
	
	public static boolean blockedSouthEast(Location loc, Entity entity) {
		return !PrimitivePathFinder.canMove(entity, loc, NormalDirection.SOUTH_EAST, entity.getSize(), false);
    }
	
	public static boolean blockedSouthWest(Location loc, Entity entity) {
		return !PrimitivePathFinder.canMove(entity, loc, NormalDirection.SOUTH_WEST, entity.getSize(), false);
    }
	
	public static void generateMovement(Entity entity) {
		Location loc = entity.getLocation();
		int dir = -1;
		if (!blockedNorth(loc, entity)) {
			dir = 0;
		} else if (!blockedEast(loc, entity)) {
			dir = 4;
		} else if (!blockedSouth(loc, entity)) {
			dir = 8;
		} else if (!blockedWest(loc, entity)) {
			dir = 12; 
		}
		int random = NumberUtils.random(3);
		boolean found = false;
		if (random == 0) {
			if (!blockedNorth(loc, entity)) {
				entity.execute_path(new SizedPathFinder(), loc.getX(), loc.getY() + 1);
				found = true;
			}
		} else if (random == 1) {
			if (!blockedEast(loc, entity)) {
				entity.execute_path(new SizedPathFinder(), loc.getX() + 1, loc.getY());
				found = true;
			}
		} else if (random == 2) {
			if (!blockedSouth(loc, entity)) {
				entity.execute_path(new SizedPathFinder(), loc.getX(), loc.getY() - 1);
				found = true;
			}
		} else if (random == 3) {
			if (!blockedWest(loc, entity)) {
				entity.execute_path(new SizedPathFinder(), loc.getX() - 1, loc.getY());
				found = true;
			}
		}
		if (!found) {
			if (dir == 0) {
				entity.execute_path(new SizedPathFinder(), loc.getX(), loc.getY() + 1);
			} else if (dir == 4) {
				entity.execute_path(new SizedPathFinder(), loc.getX() + 1, loc.getY());
			} else if (dir == 8) {
				entity.execute_path(new SizedPathFinder(), loc.getX(), loc.getY() - 1);
			} else if (dir == 12) {
				entity.execute_path(new SizedPathFinder(), loc.getX() - 1, loc.getY());
			}
		}
	}
}
