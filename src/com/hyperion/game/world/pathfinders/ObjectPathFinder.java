package com.hyperion.game.world.pathfinders;

import com.hyperion.cache.definitions.CachedObjectDefinition;
import com.hyperion.game.Constants;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;

/**
 * @author Nando
 */
public class ObjectPathFinder {

    /**
     * Returns a pathstate to an object.
     *
     * @param player
     * @param obj
     * @return
     */
    public static PathState executePath(Player player, GameObject obj) {
        int walkToData = 0;
        int type = -2;
        int direction;
        int sizeX;
        int sizeY;
        if ((obj.getType() == 10 || obj.getType() == 11 || obj.getType() == 22)) {
            type = -1;
            int rotation = obj.getRotation();
            CachedObjectDefinition def = obj.getDefinition();
            if (def == null) {
                sizeX = 1;
                sizeY = 1;
                walkToData = 0;
            } else {
                if (rotation == 1 || rotation == 3) {
                    sizeY = def.sizeY;
                    sizeX = def.sizeX;
                } else {
                    sizeX = def.sizeX;
                    sizeY = def.sizeY;
                }
                walkToData = def.surroundings;
            }

            /**
             * TEMPORARY
             * The definitions are nulling and this bellow
             * is preventing an invalid size with specific
             * object definitions
             */
            switch (obj.getId()) {
                case 10357:
                    sizeX = 5;
                    sizeY = 5;
                    break;
                case 678:
                    sizeX = 5;
                    sizeY = 5;
                    break;
                case 20672:
                case 677:
                case 26766:
                case 2141:
                case 2123:
                    sizeX = 3;
                    sizeY = 3;
                    break;
                case 27501:
                case 11601:
                case 16671:
                case 11833:
                case 15645:
                case 15648:
                case 20671:
                case 20772:
                case 20720:
                    sizeX = 2;
                    sizeY = 2;
                    break;
                case 11800:
                case 24009:
                case 30108:
                case 3192:
                case 6659:
                case 16664:
                case 30177:
                    sizeX = 3;
                    sizeY = 3;
                    break;
                case 11729:
                case 11732:
                case 11734:
                case 11730:
                case 11733:
                case 4031:
                case 16672:
                    sizeX = 2;
                    sizeY = 2;
                    break;
                case 10077:
                case 20877:
                case 21722:
                    sizeX = 2;
                    sizeY = 2;
                    rotation = -2;
                    break;
                case 1579:
                    rotation = 0;
                    sizeX = 1;
                    sizeY = 1;
                    break;
            }

            if (rotation != 0)
                walkToData = (walkToData << rotation & 0xf) + (walkToData >> 4 - rotation);
            direction = 0x2;
        } else {
            sizeX = sizeY = 1;
            type = obj.getType();
            direction = obj.getRotation();
        }
        if (obj.getId() == 12309 || obj.getId() == 16469) {
            sizeX = 2;
        }


        int finalX = obj.getLocation().getX();
        int finalY = obj.getLocation().getY();

        if (Constants.DEBUG_MODE) {
            System.out.println("[Object Path] : type" + "[" + obj.getType() + "] rot[" + direction + "] sizeX[" + sizeX + "] sizeY[" + sizeY + "] sur[" + walkToData + "]");
        }
        return player.execute_path(new VariablePathFinder(type, walkToData, direction, sizeX, sizeY), finalX, finalY);
    }
}
