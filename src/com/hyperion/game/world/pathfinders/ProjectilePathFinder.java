package com.hyperion.game.world.pathfinders;

import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.entity.combat.data.CombatConstants.CombatType;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.game.world.pathfinders.Directions.NormalDirection;

/**
 * @author Ziotic src
 */
public class ProjectilePathFinder {

    public static boolean hasLineOfSight(Location start, Location end, boolean debug) {
        Location currentTile = start;
        NormalDirection localDirection = null;
        NormalDirection localDirectionInverse = null;
        while (currentTile != end) {
            NormalDirection globalDirection = Directions.directionFor(currentTile, end);
            if (globalDirection == null) {
                return true;
            }
            Location nextTile = currentTile.transform(Directions.DIRECTION_DELTA_X[globalDirection.intValue()], Directions.DIRECTION_DELTA_Y[globalDirection.intValue()], 0);
            localDirection = Directions.directionFor(currentTile, nextTile);
            localDirectionInverse = Directions.directionFor(nextTile, currentTile);
            GameObject currentObject = World.getWallObject(currentTile);
            GameObject nextObject = World.getWallObject(nextTile);
            if (currentObject != null) {
                if (nextObject != null) {
                    if (!currentTile.canMove(localDirection, 1, false) || !nextTile.canMove(localDirectionInverse, 1, false))
                        break;
                } else {
                    if (!currentTile.canMove(localDirection, 1, false) || !nextTile.canMove(localDirectionInverse, 1, false))
                        break;
                }
            } else if (nextObject != null) {
                if (!currentTile.canMove(localDirection, 1, false) || !nextTile.canMove(localDirectionInverse, 1, false))
                    break;
            }
            if (currentTile.canMove(localDirection, 1, false) && currentTile.canMove(localDirectionInverse, 1, false)) {
                currentTile = nextTile;
                continue;
            } else {
                boolean solid = (World.getMask(nextTile.getX(), nextTile.getY(), nextTile.getZ()) & 0x20000) != 0;
                boolean solid2 = (World.getMask(currentTile.getX(), currentTile.getY(), currentTile.getZ()) & 0x20000) != 0;
                if (!solid && !solid2) {
                    currentTile = nextTile;
                    continue;
                } else
                    break;
            }
        }
        return currentTile == end;
    }

    public static boolean hasLineOfSight(Entity entity, Entity target, boolean debug) {
        CombatType combatType = entity.getCombatState().getCombatType();
        int z = entity.getCoverage().center().getZ();
        Location start_loc = Location.create(entity.getCoverage().center().getX(), entity.getCoverage().center().getY(), z);
        Location end_loc = Location.create(target.getCoverage().center().getX(), target.getCoverage().center().getY(), z);
        Location currentTile = start_loc;
        NormalDirection localDirection = null;
        NormalDirection localDirectionInverse = null;
        boolean projectileCheck = !combatType.equals(CombatType.MELEE);
        while (currentTile != end_loc) {
            NormalDirection globalDirection = Directions.directionFor(currentTile, end_loc);
            if (globalDirection == null) {
                return true;
            }
            Location nextTile = currentTile.transform(Directions.DIRECTION_DELTA_X[globalDirection.intValue()], Directions.DIRECTION_DELTA_Y[globalDirection.intValue()], 0);
            localDirection = Directions.directionFor(currentTile, nextTile);
            localDirectionInverse = Directions.directionFor(nextTile, currentTile);
            GameObject currentObject = World.getWallObject(currentTile);
            GameObject nextObject = World.getWallObject(nextTile);
            if (projectileCheck) {
                if (currentObject != null && !currentObject.isRangable()) {
                    if (nextObject != null && !nextObject.isRangable()) {
                        if (!currentTile.canMove(localDirection, 1, false) || !nextTile.canMove(localDirectionInverse, 1, false))
                            break;
                    } else {
                        if (!currentTile.canMove(localDirection, 1, true) || !nextTile.canMove(localDirectionInverse, 1, false))
                            break;
                    }
                } else if (nextObject != null) {
                    if (!currentTile.canMove(localDirection, 1, false) || !nextTile.canMove(localDirectionInverse, 1, false))
                        break;
                }
                if (currentTile.canMove(localDirection, 1, false) && currentTile.canMove(localDirectionInverse, 1, false)) {
                    currentTile = nextTile;
                    continue;
                } else {
                    boolean solid = (World.getClipedOnlyMask(nextTile.getX(), nextTile.getY(), nextTile.getZ()) & 0x20000) != 0;
                    boolean solid2 = (World.getClipedOnlyMask(currentTile.getX(), currentTile.getY(), currentTile.getZ()) & 0x20000) != 0;
                    if (!solid && !solid2) {
                        currentTile = nextTile;
                        continue;
                    } else
                        break;
                }
            } else {
                if (currentObject != null || nextObject != null) {
                    if (!currentTile.canMove(localDirection, 1, false) || !nextTile.canMove(localDirectionInverse, 1, false))
                        break;
                }
                if (currentTile.canMove(localDirection, 1, false) && currentTile.canMove(localDirectionInverse, 1, false)) {
                    currentTile = nextTile;
                    continue;
                } else {
                    boolean solid = (World.getClipedOnlyMask(nextTile.getX(), nextTile.getY(), nextTile.getZ()) & 0x20000) != 0;
                    boolean solid2 = (World.getClipedOnlyMask(currentTile.getX(), currentTile.getY(), currentTile.getZ()) & 0x20000) != 0;
                    if (!solid && !solid2) {
                        currentTile = nextTile;
                        continue;
                    } else {
                        break;
                    }
                }
            }
        }
        return currentTile == end_loc;
    }
}
