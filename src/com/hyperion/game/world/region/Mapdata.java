package com.hyperion.game.world.region;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class Mapdata {
    /**
     * Each list contains the region and 4 pieces of data.
     */
    private static Map[] mapLists = new Map[40000];

    /**
     * Constructs a new MapData class.
     */
    public Mapdata() {
        try {
			loadMapAreaData();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    /**
     * Load the map data into memory for faster load time.
     * @throws FileNotFoundException 
     */
    private void loadMapAreaData() throws Exception {
        for (int i = 0; i < 40000; i++) {
            try {
                File file = new File("./data/mapdata/" + i + ".txt");
                if (file.exists()) {
                    Map list = mapLists[i] = new Map();
                    BufferedReader in = new BufferedReader(new FileReader("./data/mapdata/" + i + ".txt"));
                    String str;
                    int regionId = 0;
                    while ((str = in.readLine()) != null) {
                        if (!str.equals("")) {
                            list.data[regionId++] = Integer.parseInt(str.trim());
                        }
                    }
                    in.close();
                    in = null;
                }
                file = null;
            } catch (Exception e) {
            	e.printStackTrace();
            }
        }
        System.out.println("Loaded mapdata");
    }

    /**
     * Returns the four pieces of map data from a region.
     * @param myRegion The region to get data from.
     * @return Returns the four mapdata.
     */
    public int[] getData(int myRegion) {
    	if(mapLists[myRegion] == null) {
	        //System.out.println("Missing map data: " + myRegion);
	        return new int[4];
    	}
    	return mapLists[myRegion].data;
    }
}