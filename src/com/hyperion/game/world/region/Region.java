package com.hyperion.game.world.region;

import com.hyperion.cache.Cache;
import com.hyperion.cache.definitions.CachedObjectDefinition;
import com.hyperion.cache.stream.InputStream;
import com.hyperion.game.RS2Server;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.gameobjects.GameObject;
import com.hyperion.game.world.gameobjects.ObjectManager;
import com.hyperion.game.world.grounditems.GroundItem;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Region {

    private static final int[] OBJECT_SLOTS = new int[]{0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3};
    private static final int OBJECT_SLOT_FLOOR = 2;

    private int regionId;
    protected RegionMap map;
    protected RegionMap clipedOnlyMap;

    private volatile int loadMapStage;

    private List<Integer> playersIndexes;
    private List<Integer> npcsIndexes;
    private List<GameObject> spawnedObjects;
    protected List<GameObject> removedOriginalObjects;
    protected GameObject[][][][] objects;
    private List<GroundItem> groundItems;

    private boolean loadedNPCSpawns;
    private boolean loadedObjectSpawns;
    private boolean loadedItemSpawns;

    /**
     * Creates a region.
     */
    public Region(int id) {
        /*if(id == 12086)
            this.regionId = 13363;
		else if(id == 12085)
			this.regionId = 13362;
		else if(id == 12341)
			this.regionId = 11319;
		else*/
        if (id == 257)
            System.out.println("257!!!!!!!!!!!!!!!!!!!");
        this.regionId = id;
        this.spawnedObjects = new CopyOnWriteArrayList<GameObject>();
        this.removedOriginalObjects = new CopyOnWriteArrayList<GameObject>();
    }

    public void checkLoadMap() {

        if (!RS2Server.getEngine().isRunning())
            return;
        World.getWorld().getEngine().submitMisc(new Runnable() {
            public void run() {
                try {
                    if (getLoadMapStage() == 0) {
                        setLoadMapStage(1);
                        if (regionId == 257)
                            System.out.println("257 STAGE 1!!!!!!!!!!!!!!!!!!!");
                        loadRegionMap();
                        setLoadMapStage(2);
                        if (regionId == 257)
                            System.out.println("257 STAGE 2!!!!!!!!!!!!!!!!!!!");
                        if (!isLoadedObjectSpawns()) {
                            try {
                                loadObjectSpawns();
                                setLoadedObjectSpawns(true);
                            } catch (ConcurrentModificationException c) {
                                System.out.println("concurrent on object spawns: " + c);
                            }
                        }
                        if (!isLoadedNPCSpawns()) {
                            loadNPCSpawns();
                            setLoadedNPCSpawns(true);
                        }
                        if (!isLoadedItemSpawns()) {
                            loadItemSpawns();
                            setLoadedItemSpawns(true);
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Exception loading region shit: " + e);
                }
            }
        });
    }

    private void loadNPCSpawns() {
        //NPCSpawns.init(regionId);
        //System.out.println("Load regional npc spawns here============================");
    }

    private void loadObjectSpawns() {
        // Only remove objects are loaded here.
        ObjectManager.deleteObjectsForRegion(regionId);
        //ObjectManager.loadSpawns(regionId);
       // ObjectSpawns.loadObjectSpawns(regionId);
    }

    private void loadItemSpawns() {
        // ItemSpawns.loadItemSpawns(regionId);
    }

    /**
     * Unload's map from memory.
     */
    public void unloadMap() {
        if (getLoadMapStage() == 2 && (playersIndexes == null || playersIndexes.isEmpty()) && (npcsIndexes == null || npcsIndexes.isEmpty())) {
            objects = null;
            map = null;
            setLoadMapStage(0);
        }
    }

    public RegionMap forceGetRegionMapClipedOnly() {
        if (clipedOnlyMap == null)
            clipedOnlyMap = new RegionMap(regionId, true);
        return clipedOnlyMap;
    }

    public RegionMap forceGetRegionMap() {
        if (map == null)
            map = new RegionMap(regionId, false);
        return map;
    }

    public RegionMap getRegionMap() {
        return map;
    }

    public int getMask(int plane, int localX, int localY) {
        if (map == null || getLoadMapStage() != 2) {
            if (this.regionId == 257)
                System.out.println("map null or load state not 2: " + getLoadMapStage());
            return -1; // cliped tile
        }
        return map.getMasks()[plane][localX][localY];
    }

    public int getMaskClipedOnly(int plane, int localX, int localY) {
        if (clipedOnlyMap == null || getLoadMapStage() != 2)
            return -1; // cliped tile
        return clipedOnlyMap.getMasks()[plane][localX][localY];
    }

    public void setMask(int plane, int localX, int localY, int mask) {
        if (map == null || getLoadMapStage() != 2)
            map = new RegionMap(regionId, false);
        //return; // cliped tile
        if (localX >= 64 || localY >= 64 || localX < 0 || localY < 0) {
            Location tile = Location.create(map.getRegionX() + localX, map.getRegionY() + localY, plane);
            int regionId = tile.getRegionId();
            int newRegionX = (regionId >> 8) * 64;
            int newRegionY = (regionId & 0xff) * 64;
            World.getWorld().getRegion(tile.getRegionId(), false).setMask(plane, tile.getX() - newRegionX, tile.getY() - newRegionY, mask);
            return;
        }
        map.setMask(plane, localX, localY, mask);
    }

    public void clip(GameObject object, int x, int y) {
        if (x == 3231 && y == 3432 || x == 3231 && y == 3433) {
            System.out.println("fff sno" + object);
            return;
        }
        // System.out.println("Clip : " + object.getId() + " " + x + " " + y);
        if (map == null)
            map = new RegionMap(regionId, false);
        if (clipedOnlyMap == null)
            clipedOnlyMap = new RegionMap(regionId, true);
        int plane = object.getPlane();
        int type = object.getType();
        int rotation = object.getRotation();
        if (x < 0 || y < 0 || x >= map.getMasks()[plane].length || y >= map.getMasks()[plane][x].length)
            return;
        CachedObjectDefinition objectDefinition = CachedObjectDefinition.forId(object.getId()); // load
        // here
        int sizeX;
        int sizeY;
        if (rotation != 1 && rotation != 3) {
            sizeX = objectDefinition.sizeX;
            sizeY = objectDefinition.sizeY;
        } else {
            sizeX = objectDefinition.sizeY;
            sizeY = objectDefinition.sizeX;
        }
        if (type >= 0 && type <= 3) {
            if (objectDefinition.clipType != 0) {
                map.addWall(plane, x, y, type, rotation, objectDefinition.walkable);
                if (objectDefinition.walkable)
                    clipedOnlyMap.addWall(plane, x, y, type, rotation, objectDefinition.walkable);
            }
        } else if (type >= 9 && type <= 11) {
            if (objectDefinition.clipType != 0) {
                map.addObject(plane, x, y, sizeX, sizeY, objectDefinition.walkable);
                if (objectDefinition.walkable)
                    clipedOnlyMap.addObject(plane, x, y, sizeX, sizeY, objectDefinition.walkable);
            }
        } else if (type == 22) {
            if (objectDefinition.clipType == 1) {
                map.addMask(plane, x, y, 0x200000);
            }
        }
    }

    public void unclip(int plane, int x, int y) {
        if (map == null)
            map = new RegionMap(regionId, false);
        if (clipedOnlyMap == null)
            clipedOnlyMap = new RegionMap(regionId, true);
        map.setMask(plane, x, y, 0);
    }

    private void unclip(GameObject object, int x, int y) {
        if (map == null)
            map = new RegionMap(regionId, false);
        if (clipedOnlyMap == null)
            clipedOnlyMap = new RegionMap(regionId, true);
        int plane = object.getPlane();
        int type = object.getType();
        int rotation = object.getRotation();
        if (x < 0 || y < 0 || x >= map.getMasks()[plane].length || y >= map.getMasks()[plane][x].length)
            return;
        CachedObjectDefinition objectDefinition = CachedObjectDefinition.forId(object.getId()); // load
        int sizeX;
        int sizeY;
        if (rotation != 1 && rotation != 3) {
            sizeX = objectDefinition.sizeX;
            sizeY = objectDefinition.sizeY;
        } else {
            sizeX = objectDefinition.sizeY;
            sizeY = objectDefinition.sizeX;
        }
        if (type >= 0 && type <= 3) {
            if (objectDefinition.clipType != 0) {
                map.removeWall(plane, x, y, type, rotation, objectDefinition.walkable);
                if (objectDefinition.walkable)
                    clipedOnlyMap.removeWall(plane, x, y, type, rotation, objectDefinition.walkable);
            }
        } else if (type >= 9 && type <= 11) {
            if (objectDefinition.clipType != 0) {
                map.removeObject(plane, x, y, sizeX, sizeY, objectDefinition.walkable);
                if (objectDefinition.walkable)
                    clipedOnlyMap.removeObject(plane, x, y, sizeX, sizeY, objectDefinition.walkable);
            }
        } else if (type == 22) {
            if (objectDefinition.clipType == 1) {
                map.removeMask(plane, x, y, 0x200000);
            }
        }
    }

    public void loadRegionMap() {
        /*
         * boolean reloadRegion = false; if (regionId == 12342) reloadRegion =
         * true;
         */
        int regionX = (regionId >> 8) * 64;
        int regionY = (regionId & 0xff) * 64;
        int landArchiveId = Cache.getCacheFileManagers()[5].getContainerId("l" + ((regionX >> 3) / 8) + "_" + ((regionY >> 3) / 8));
        if (landArchiveId == -1) {
            System.out.println("[" + landArchiveId + "][" + ("l" + ((regionX >> 3) / 8) + "_" + ((regionY >> 3) / 8)) + "] Land archive data for id is null");
        }
        byte[] landContainerData = landArchiveId == -1 ? null : Cache.getCacheFileManagers()[5].getFileData(landArchiveId, 0, World.getWorld().mapdata.getData(regionId));
        int mapArchiveId = Cache.getCacheFileManagers()[5].getContainerId("m" + ((regionX >> 3) / 8) + "_" + ((regionY >> 3) / 8));
        byte[] mapContainerData = mapArchiveId == -1 ? null : Cache.getCacheFileManagers()[5].getFileData(mapArchiveId, 0);
        byte[][][] mapSettings = mapContainerData == null ? null : new byte[4][64][64];

        // System.out.println("Land[" + landArchiveId + "]");
        // System.out.println("Map[" + mapArchiveId + "]");
        // System.out.println("MapSettings[" + mapSettings.toString() + "]");

        if (mapContainerData != null) {
            InputStream mapStream = new InputStream(mapContainerData);
            for (int plane = 0; plane < 4; plane++) {
                for (int x = 0; x < 64; x++) {
                    for (int y = 0; y < 64; y++) {
                        while (true) {
                            int value = mapStream.readUnsignedByte();
                            if (value == 0) {
                                break;
                            } else if (value == 1) {
                                mapStream.skip(1);
                                break;
                            } else if (value <= 49) {
                                mapStream.skip(1);
                            } else if (value <= 81) {
                                mapSettings[plane][x][y] = (byte) (value - 49);
                            }
                        }
                    }
                }
            }
            for (int plane = 0; plane < 4; plane++) {
                for (int x = 0; x < 64; x++) {
                    for (int y = 0; y < 64; y++) {
                        if ((mapSettings[plane][x][y] & 0x1) == 1) {
                            int realPlane = plane;
                            if ((mapSettings[1][x][y] & 2) == 2)
                                realPlane--;
                            if (realPlane >= 0)
                                forceGetRegionMap().addUnwalkable(realPlane, x, y);
                        }
                    }
                }
            }
        } else {
            for (int plane = 0; plane < 4; plane++) {
                for (int x = 0; x < 64; x++) {
                    for (int y = 0; y < 64; y++) {
                        forceGetRegionMap().addUnwalkable(plane, x, y);
                    }
                }
            }
        }
        if (landContainerData != null) {
            InputStream landStream = new InputStream(landContainerData);
            int objectId = -1;
            int incr;
            while ((incr = landStream.readSmart2()) != 0) {
                objectId += incr;
                int location = 0;
                int incr2;
                while ((incr2 = landStream.readUnsignedSmart()) != 0) {
                    location += incr2 - 1;
                    int localX = (location >> 6 & 0x3f);
                    int localY = (location & 0x3f);
                    int plane = location >> 12;
                    int objectData = landStream.readUnsignedByte();
                    int type = objectData >> 2;
                    int rotation = objectData & 0x3;
                    if (localX < 0 || localX >= 64 || localY < 0 || localY >= 64)
                        continue;
                    int objectPlane = plane;
                    if (mapSettings != null && (mapSettings[1][localX][localY] & 2) == 2)
                        objectPlane--;
                    if (objectPlane < 0 || objectPlane >= 4 || plane < 0 || plane >= 4)
                        continue;
                    Location loc = Location.create(localX + regionX, localY + regionY, objectPlane);
                    // if(!World.getWorld().getDeleteObjects().contains(loc))
                    spawnObject(new GameObject(loc, objectId, type, rotation), objectPlane, localX, localY, true);
                }
            }
        }
        /*
         * if(reloadRegion) { World.getWorld().submit(new Tickable(1) {
         *
         * @Override public void execute() {
         *
         * this.stop(); } }); }
         */
        // add a task to reiterate this method in like 2 seconds


        //if (Constants.DEBUG_MODE && landContainerData == null && landArchiveId !=
        //  -1) //&&  DynamicRegion.getMapKeys(regionId) != null)
        //  Logger.g(this, "Missing xteas for region " + regionId + ".");
    }

    public void spawnObject(GameObject object, int plane, int localX, int localY, boolean original) {
        if (objects == null)
            objects = new GameObject[4][64][64][4];
        if (object.getType() > OBJECT_SLOTS.length)
            object.setType(10);
        int slot = OBJECT_SLOTS[object.getType()];
        if (original) {
            objects[plane][localX][localY][slot] = object;
            clip(object, localX, localY);
        } else {
            GameObject spawned = getSpawnedObjectWithSlot(plane, localX, localY, slot);
            // found non original object on this slot. removing it since we
            // replacing with a new non original
            if (spawned != null) {
                spawnedObjects.remove(spawned);
                // unclips non orignal old object which had been cliped so can
                // clip the new non original
                unclip(spawned, localX, localY);
            }
            GameObject removed = getRemovedObjectWithSlot(plane, localX, localY, slot);
            // there was a original object removed. lets readd it
            if (removed != null) {
                object = removed;
                removedOriginalObjects.remove(object);
                // adding non original object to this place
            } else if (objects[plane][localX][localY][slot] != object) {
                if (plane == object.getPlane()) {
                    spawnedObjects.add(object);
                    // unclips orignal old object which had been cliped so can
                    // clip
                    // the new non original
                    if (objects[plane][localX][localY][slot] != null) {
                        unclip(objects[plane][localX][localY][slot], localX, localY);
                    }
                }
            } else if (spawned == null) {
                // if (Settings.DEBUG)
                // Logger.log(this,"Requested object to spawn is already spawned.(Shouldnt happen)");
                return;
            }
            // clips spawned object(either original or non original)
            clip(object, localX, localY);
            for (Player p2 : World.getWorld().getPlayers()) {
                if (p2 == null || !p2.getMapRegionsIds().contains(regionId))
                    continue;
                p2.getPacketSender().createObject(object);
            }
        }
    }

    public void removeObject(GameObject object, int plane, int localX, int localY) {
        if (objects == null)
            objects = new GameObject[4][64][64][4];
        int slot = OBJECT_SLOTS[object.getType()];
        GameObject removed = getRemovedObjectWithSlot(plane, localX, localY, slot);
        if (removed != null) {
            removedOriginalObjects.remove(object);
            clip(removed, localX, localY);
        }
        GameObject original = null;
        // found non original object on this slot. removing it since we
        // replacing with real one or none if none
        GameObject spawned = getSpawnedObjectWithSlot(plane, localX, localY, slot);
        if (spawned != null) {
            object = spawned;
            spawnedObjects.remove(object);
            unclip(object, localX, localY);
            if (objects[plane][localX][localY][slot] != null) {// original
                // unclips non original to clip original above
                // System.out.println("orig: " +
                // objects[plane][localX][localY][slot].toString());
                clip(objects[plane][localX][localY][slot], localX, localY);
                original = objects[plane][localX][localY][slot];
            }
            // found original object on this slot. removing it since requested
        } else if (objects[plane][localX][localY][slot] != null && objects[plane][localX][localY][slot].getId() == object.getId()) { // removes
            // original
            GameObject original_spawned = objects[plane][localX][localY][slot];
            if (original_spawned.getType() == object.getType()) {
                if (original_spawned.getRotation() == object.getRotation()) {
                    if (original_spawned.getLocation().equals(object.getLocation())) {
                        unclip(object, localX, localY);
                        removedOriginalObjects.add(object);
                    }
                }
            }
        } else {
            // System.out.println("Requested object to remove wasnt found.(Shouldnt happen): ");
            // if (Settings.DEBUG)
            // Logger.log(this,"Requested object to remove wasnt found.(Shouldnt happen)");
            return;
        }
        for (Player p2 : World.getWorld().getPlayers()) {
            if (p2 == null || !p2.getMapRegionsIds().contains(regionId))
                continue;
            if (original != null) {
                p2.getPacketSender().createObject(original);
            } else {
                p2.getPacketSender().removeObject(object);
            }
        }
    }

    public GameObject getStandartObject(int plane, int x, int y) {
        return getObjectWithSlot(plane, x, y, OBJECT_SLOT_FLOOR);
    }

    public GameObject getObjectWithType(int plane, int x, int y, int type) {
        GameObject object = getObjectWithSlot(plane, x, y, OBJECT_SLOTS[type]);
        return object != null && object.getType() == type ? object : null;
    }

    private GameObject getObjectWithSlot(int plane, int x, int y, int slot) {
        if (objects == null)
            return null;
        GameObject o = getSpawnedObjectWithSlot(plane, x, y, slot);
        if (o == null) {
            if (getRemovedObjectWithSlot(plane, x, y, slot) != null)
                return null;
            return objects[plane][x][y][slot];
        }
        return o;
    }

    private GameObject getSpawnedObjectWithSlot(int plane, int x, int y, int slot) {
        for (GameObject object : spawnedObjects) {
            if (object.getLocation().getXInRegion() == x && object.getLocation().getYInRegion() == y && object.getPlane() == plane && OBJECT_SLOTS[object.getType()] == slot)
                return object;
        }
        return null;
    }

    private GameObject getRemovedObjectWithSlot(int plane, int x, int y, int slot) {
        for (GameObject object : removedOriginalObjects) {
            if (object.getLocation().getXInRegion() == x && object.getLocation().getYInRegion() == y && object.getPlane() == plane && OBJECT_SLOTS[object.getType()] == slot)
                return object;
        }
        return null;
    }

    public GameObject[] getAllObjects(int plane, int x, int y) {
        if (objects == null)
            return null;
        return objects[plane][x][y];
    }

    public List<GameObject> getAllObjects() {
        if (objects == null)
            return null;
        List<GameObject> list = new ArrayList<GameObject>();
        for (int z = 0; z < 4; z++) {
            for (int x = 0; x < 64; x++) {
                for (int y = 0; y < 64; y++) {
                    if (objects[z][x][y] == null)
                        continue;
                    for (GameObject o : objects[z][x][y]) {
                        if (o != null) {
                            list.add(o);
                        }
                    }
                }
            }
        }
        return list;
    }

    public boolean containsObjectWithId(int plane, int x, int y, int id) {
        GameObject object = getObjectWithId(plane, x, y, id);
        return object != null && object.getId() == id;
    }

    public GameObject getWallObject(int plane, int x, int y) {
        if (objects == null)
            return null;
        for (GameObject object : removedOriginalObjects) {
            if (object.getType() >= 0 && object.getType() <= 3 && object.getLocation().getXInRegion() == x && object.getLocation().getYInRegion() == y && object.getPlane() == plane)
                return null;
        }
        for (int i = 0; i < 4; i++) {
            GameObject object = objects[plane][x][y][i];
            if (object != null && object.getType() >= 0 && object.getType() <= 3) {
                GameObject spawned = getSpawnedObjectWithSlot(plane, x, y, OBJECT_SLOTS[object.getType()]);
                return spawned == null ? object : null;
            }
        }
        for (GameObject object : spawnedObjects) {
            if (object.getLocation().getXInRegion() == x && object.getLocation().getYInRegion() == y && object.getPlane() == plane && object.getType() >= 0 && object.getType() <= 3)
                return object;
        }
        return null;
    }

    public GameObject getObjectWithId(int plane, int x, int y, int id) {
        if (objects == null) {
            return null;
        }
        for (GameObject object : removedOriginalObjects) {
            if (object.getId() == id && object.getLocation().getXInRegion() == x && object.getLocation().getYInRegion() == y && object.getPlane() == plane)
                return null;
        }
        for (int i = 0; i < 4; i++) {
            GameObject object = objects[plane][x][y][i];
            if (object != null && object.getId() == id) {
                GameObject spawned = getSpawnedObjectWithSlot(plane, x, y, OBJECT_SLOTS[object.getType()]);
                if (spawned == null)
                    return object;
                else
                    return spawned;
                // return spawned == null ? object : spawned;
            }
        }
        for (GameObject object : spawnedObjects) {
            if (object.getLocation().getXInRegion() == x && object.getLocation().getYInRegion() == y && object.getPlane() == plane && object.getId() == id) {
                return object;
            }
        }
        return null;
    }

    public GameObject getObjectWithId(int id, int plane) {
        if (objects == null)
            return null;
        for (GameObject object : spawnedObjects) {
            if (object.getId() == id && object.getPlane() == plane)
                return object;
        }
        for (int x = 0; x < 64; x++) {
            for (int y = 0; y < 64; y++) {
                for (int slot = 0; slot < objects[plane][x][y].length; slot++) {
                    GameObject object = objects[plane][x][y][slot];
                    if (object != null && object.getId() == id)
                        return object;
                }
            }
        }
        return null;
    }

    public List<GameObject> getSpawnedObjects() {
        return spawnedObjects;
    }

    public List<GameObject> getRemovedOriginalObjects() {
        return removedOriginalObjects;
    }

    public void setId(int id) {
        this.regionId = id;
    }

    public int getId() {
        return regionId;
    }

    public int getRotation(int plane, int x, int y) {
        return 0;
    }

    /**
     * Get's ground item with specific id on the specific location in this
     * region.
     */
    public GroundItem getGroundItem(int id, Location tile, Player player) {
        if (groundItems == null)
            return null;
        for (GroundItem item : groundItems) {
            if (!item.isOwnedBy(player)) {
                continue;
            }
            if (item.getId() == id && tile.getX() == item.getLocation().getX() && tile.getY() == item.getLocation().getY() && tile.getZ() == item.getLocation().getZ())
                return item;
        }
        return null;
    }

    /**
     * Return's list of ground items that are currently loaded. List may be null
     * if there's no ground items. Modifying given list is prohibited.
     *
     * @return
     */
    public List<GroundItem> getGroundItems() {
        return groundItems;
    }

    /**
     * Return's list of ground items that are currently loaded. This method
     * ensures that returned list is not null. Modifying given list is
     * prohibited.
     *
     * @return
     */
    public List<GroundItem> getGroundItemsSafe() {
        if (groundItems == null)
            groundItems = new CopyOnWriteArrayList<GroundItem>();
        return groundItems;
    }

    public List<Integer> getPlayerIndexes() {
        return playersIndexes;
    }

    public int getPlayerCount() {
        return playersIndexes == null ? 0 : playersIndexes.size();
    }

    public List<Integer> getNPCsIndexes() {
        return npcsIndexes;
    }

    public void addPlayerIndex(int index) {
        // creates list if doesnt exist
        if (playersIndexes == null)
            playersIndexes = new CopyOnWriteArrayList<Integer>();
        playersIndexes.add(index);
    }

    public void addNPCIndex(int index) {
        // creates list if doesnt exist
        if (npcsIndexes == null)
            npcsIndexes = new CopyOnWriteArrayList<Integer>();
        npcsIndexes.add(index);
    }

    public void removePlayerIndex(Integer index) {
        if (playersIndexes == null) // removed region example cons or dung
            return;
        playersIndexes.remove(index);
    }

    public boolean removeNPCIndex(Object index) {
        if (npcsIndexes == null) // removed region example cons or dung
            return false;
        return npcsIndexes.remove(index);
    }

    public int getLoadMapStage() {
        return loadMapStage;
    }

    public void setLoadMapStage(int loadMapStage) {
        this.loadMapStage = loadMapStage;
    }

    public boolean isLoadedObjectSpawns() {
        return loadedObjectSpawns;
    }

    public void setLoadedObjectSpawns(boolean loadedObjectSpawns) {
        this.loadedObjectSpawns = loadedObjectSpawns;
    }

    public boolean isLoadedNPCSpawns() {
        return loadedNPCSpawns;
    }

    public void setLoadedNPCSpawns(boolean loadedNPCSpawns) {
        this.loadedNPCSpawns = loadedNPCSpawns;
    }

    public boolean isLoadedItemSpawns() {
        return loadedItemSpawns;
    }

    public void setLoadedItemSpawns(boolean loadedItemSpawns) {
        this.loadedItemSpawns = loadedItemSpawns;
    }
}
