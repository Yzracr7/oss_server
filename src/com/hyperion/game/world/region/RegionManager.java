package com.hyperion.game.world.region;


import java.util.HashMap;
import java.util.Map;

/**
 * Manages the world regions.
 * @author Graham Edgecombe
 *
 */
public class RegionManager {
	
	/**
	 * The region size.
	 */
	private static final int REGION_SIZE = 16;
	
	/**
	 * The lower data that splits the region in half.
	 */
	@SuppressWarnings("unused")
	private static final int LOWER_BOUND = REGION_SIZE / 2 - 1;
	
	/**
	 * The active (loaded) region map.
	 */
	private Map<Integer, Region> activeRegions = new HashMap<Integer, Region>();

	public final Map<Integer, Region> getRegions() {
		return activeRegions;
	}
	
	/**
	 * Gets a region by its x and y coordinates.
	 * @param x The x coordinate.
	 * @param y The y coordinate.
	 * @return The region.
	 */
	public final Region getRegion(int id, boolean load) {
		Region region = activeRegions.get(id);
		if (region == null) {
			region = new Region(id);
			activeRegions.put(id, region);
		}
		if (load) {
			region.checkLoadMap();
		}
		return region;
	}
}
