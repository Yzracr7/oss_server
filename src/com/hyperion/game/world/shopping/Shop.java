package com.hyperion.game.world.shopping;

import com.hyperion.game.item.Item;

public class Shop {

	private int shopId;
	private String shopName;
	private boolean canSellBack;
	private boolean isGeneralShop;
	private int currency;
	private Item[] stock;
	
	public Shop(int id) {
		this.shopId = id;
	}
	
	/**
	 * 
	 * @return the shop id
	 */
	public int getId() {
		return shopId;
	}

	public void setId(int id) {
		this.shopId = id;
	}
	
	/**
	 * 
	 * @return the shop's name
	 */
	public String getName() {
		return shopName;
	}
	
	public void setName(String name) {
		this.shopName = name;
	}

	/**
	 * General shop
	 * @return if its a general shop
	 */
	public boolean isGeneralShop() {
		return isGeneralShop;
	}
	
	public int getCurrency() {
		return currency;
	}

	public void setStock(Item[] stock) {
		this.stock = stock;
	}
	
	public Item[] getStock() {
		return stock;
	}
	
	public Item getStockItem(int slot) {
		return stock[slot];
	}
	
	public int getItemInSlot(int slot) {
		return stock[slot].getId();
	}
	
	public int findItem(int itemId) {
		for (int i = 0; i < stock.length; i++) {
			if (stock[i].getId() == itemId) {
				return i;
			}
		}
		return -1;
	}
	
	public Item getSlot(int slot) {
		return stock[slot];
	}
	
	public int findFreeSlot() {
		for (int i = 0; i < stock.length; i++) {
			if (stock[i].getId() <= 0) {
				return i;
			}
		}
		return -1;
	}

	public boolean canSellBack() {
		return shopId != 36 && canSellBack;
	}
}