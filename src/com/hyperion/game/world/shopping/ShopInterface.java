package com.hyperion.game.world.shopping;

import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.player.Player;

public interface ShopInterface {

	/**
	 * Opens a shop.
	 * 
	 * @param player
	 * @param id
	 */
	public void openShop(Player player, int id);

	/**
	 * Purchases an item from a shop.
	 * 
	 * @param player
	 * @param slot
	 * @param amount
	 */
	public void purchaseItem(Player player, int slot, int amount);

	/**
	 * Sells an item to a shop.
	 * 
	 * @param player
	 * @param slot
	 * @param amount
	 */
	public void sellItem(Player player, int slot, int amount);

	/**
	 * Check value of item.
	 * 
	 * @param slot
	 */
	public void value(Player player, Item item, int type);

	/**
	 * Currency name.
	 * 
	 * @return
	 */
	public String getCurrencyName();

	/**
	 * Gets item prices depending on shop.
	 * 
	 * @param item
	 * @return
	 */
	public int getItemPriceForShop(Item item);

	/**
	 * Gets item price for selling.
	 * 
	 * @param item
	 * @return
	 */
	public int getSellPriceForShop(Item item);

	/**
	 * can sell
	 * 
	 * @param player
	 * @param item
	 * @return
	 */
	public boolean canSellToShop(Player player, Item item);

	/**
	 * The quantity of the item
	 * 
	 * @param item
	 *            The item
	 * @return
	 */
	public int getItemQuantity(Item item);
}
