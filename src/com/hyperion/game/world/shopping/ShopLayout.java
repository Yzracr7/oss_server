package com.hyperion.game.world.shopping;

import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.player.Player;


public abstract class ShopLayout implements ShopInterface {
	
	@Override
	public void openShop(Player player, int id) {
		Shop shop = ShopManager.shopsList.get(id);
		if (shop == null) {
			return;
		}
		player.getAttributes().set("shop", shop);
		player.getAttributes().set("shopping", true);
		String name = shop.getName();
		player.getPacketSender().modifyText(name, 300, 76);
		player.getPacketSender().sendItems(301, 0, 93, player.getInventory().getItems());
		player.getPacketSender().sendItems(300, 75, 93, shop.getStock());
		player.getPacketSender().displayInterface(300);
		player.getPacketSender().displayInventoryInterface(301);
	}
	
	@Override
	public void purchaseItem(Player player, int slot, int amount) {
		//TODO 
	}
	
	@Override
	public void sellItem(Player player, int slot, int amount) {
		//TODO 
	}
	
	@Override
	public void value(Player player, Item item, int type) {
		
	}
	
	@Override
	public String getCurrencyName() {
		return null;
	}
	
	@Override
	public int getItemPriceForShop(Item item) {
		return 1;
	}
	
	@Override
	public int getSellPriceForShop(Item item) {
		return 1;
	}
	
	@Override
	public boolean canSellToShop(Player player, Item item) {
		Shop shop = (Shop) player.getAttributes().get("shop");
		if (shop == null) {
			return false;
		}
		if (!shop.isGeneralShop() && !shop.canSellBack()) {
			player.getPacketSender().sendMessage("You cannot sell items to this shop.");
			return false;
		}
		if (item.getDefinition().getName().equalsIgnoreCase("coins")) {
			player.getPacketSender().sendMessage("You can't sell coins.");
			return false;
		}
		if (item.getDefinition().getName().equalsIgnoreCase("tokkul")) {
			player.getPacketSender().sendMessage("You can't sell tokkul.");
			return false;
		}
		if (item.getDefinition().getName().equalsIgnoreCase("tokens")) {
			player.getPacketSender().sendMessage("You can't sell tokens.");
			return false;
		}
		if (item.getDefinition().isPlayerBound()) {
			player.getPacketSender().sendMessage("You can't sell this item.");
			return false;
		}
		boolean sellableItem = false;
		for (Item items : shop.getStock()) {
			if (items.getId() == item.getId()) {
				sellableItem = true;
			}
		}
		if (!sellableItem) {
			player.getPacketSender().sendMessage("You can't sell this item here.");
			return false;
		}
		return true;
	}

	@Override
	public int getItemQuantity(Item item) {
		return 1;
	}
}
