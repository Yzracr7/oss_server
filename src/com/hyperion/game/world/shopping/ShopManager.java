package com.hyperion.game.world.shopping;

import java.io.FileInputStream;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.hyperion.Logger;
import com.hyperion.game.Constants;
import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.container.impl.Equipment;
import com.hyperion.game.world.shopping.impl.AchievementExchange;
import com.hyperion.game.world.shopping.impl.BarrowsExchange;
import com.hyperion.game.world.shopping.impl.BountyHunterShop;
import com.hyperion.game.world.shopping.impl.DonatorShop;
import com.hyperion.game.world.shopping.impl.LoyaltyPointExchange;
import com.hyperion.game.world.shopping.impl.PestExchange;
import com.hyperion.game.world.shopping.impl.RuneCraftExchange;
import com.hyperion.game.world.shopping.impl.SkillCapeShop;
import com.hyperion.game.world.shopping.impl.SlayerExchange;
import com.hyperion.game.world.shopping.impl.TokkulShop;
import com.hyperion.game.world.shopping.impl.VoteShop;
import com.hyperion.game.world.shopping.impl.WarriorsGuildExchange;
import com.hyperion.utility.NumberUtils;
import com.hyperion.utility.XStreamUtil;

public class ShopManager {

    public static List<Shop> shopsList;
    private static ConcurrentHashMap<Integer, ShopLayout> customShops = new ConcurrentHashMap<Integer, ShopLayout>();
    private static final ShopManager shopManager = new ShopManager();

    @SuppressWarnings("unchecked")
    public static void load() throws Exception {
        shopsList = (List<Shop>) XStreamUtil.getXStream().fromXML(new FileInputStream("./data/world/shopdata/shops.xml"));
        //Logger.getInstance().warning("Loaded " + shopsList.size() + " shops.");
    }

    public static ShopManager getShopManager() {
        return shopManager;
    }

    public Shop getShop(int id) {
        return shopsList.get(id);
    }

    public static void openShop(Player player, int id) {
        Achievements.increase(player, 10);
        player.getAttributes().remove("currentshop");
        ShopLayout customShop = null;
        if (customShops.get(id) == null) {
            if (id == 12) {
                customShop = new SkillCapeShop();
            } else if (id == 13) {
                customShop = new TokkulShop();
            } else if (id == 14) {
                customShop = new BarrowsExchange();
            } else if (id == 22) {
                customShop = new PestExchange();
            } else if (id == 24) {
                customShop = new WarriorsGuildExchange();
            } else if (id == 29) {
                customShop = new RuneCraftExchange();
            } else if (id == 32) {
                customShop = new SlayerExchange();
            } else if (id == 33) {
                customShop = new AchievementExchange();
            } else if (id == 35) {
                customShop = new VoteShop();
            } else if (id == 37) {
                customShop = new BountyHunterShop();
            } else if (id == 38) {
                customShop = new DonatorShop();
            } else if (id == 39) {
                customShop = new LoyaltyPointExchange();
            }

            if (customShop != null) {
                customShops.putIfAbsent(id, customShop);
                if (Constants.DEBUG_MODE)
                    Logger.getInstance().warning("Added new custom shop to our list: " + id);
            }
        } else {
            customShop = customShops.get(id);
            if (Constants.DEBUG_MODE)
                Logger.getInstance().warning("Custom shop exists: " + id);
        }
        if (customShop != null) {
            Shop shopInstance = shopsList.get(id);
            if (player.getDetails().isIronman() && (shopInstance.isGeneralShop() || shopInstance.canSellBack())) {
                player.getPacketSender().sendChatboxDialogue(true, "As an ironman, you cannot do this.");
                return;
            }
            player.getPacketSender().sendInterfaceConfig(300, 94, false); //Bounties
            customShop.openShop(player, id);
            return;
        }
        Shop shop = shopsList.get(id);
        if (shop == null) {
            return;
        }
        if (player.getDetails().isIronman() && (shop.isGeneralShop() || shop.canSellBack()) && shop.getId() != 34 && shop.getId() != 36) { // 34
            // thieving
            // shop,
            // 36
            // ironman
            // shop
            player.getPacketSender().sendChatboxDialogue(true, "As an ironman, you cannot do this.");
            return;
        }
        player.getAttributes().set("shop", shop);
        player.getAttributes().set("shopping", true);
        String name = shop.getName();
        player.getPacketSender().modifyText(name, 300, 76);
        player.getPacketSender().sendInterfaceConfig(300, 94, false); //Bounties
        player.getPacketSender().sendItems(301, 0, 93, player.getInventory().getItems());
        player.getPacketSender().sendItems(300, 75, 93, shop.getStock());
        player.getPacketSender().displayInterface(300);
        player.getPacketSender().displayInventoryInterface(301);
        player.getPacketSender().modifyText("Right-click the item you wish to purchase.", 300, 77);
    }

    public void purchaseItem(Player player, int slot, int amount) {
        ShopLayout currentShop = (ShopLayout) player.getAttributes().get("currentshop");
        if (currentShop != null) {
            currentShop.purchaseItem(player, slot, amount);
            return;
        }
        Item item = null;
        if (slot < 0 || slot > 40) {
            return;
        }
        Shop shop = (Shop) player.getAttributes().get("shop");
        item = shop.getStockItem(slot);
        if (item == null || item.getCount() < 0) {
            return;
        }
        if (player.getDetails().isIronman()) {
            if (shop.canSellBack()) {
                player.getPacketSender().sendChatboxDialogue(true, "As an ironman, you cannot do this.");
                return;
            }
        }
        if (shop.getId() == 34) {
            player.getPacketSender().sendChatboxDialogue(true, "You can't buy items from this shop.");
            return;
        }
        boolean stackable = item.getDefinition().isStackable();
        int amountToAdd = amount;
        int itemPrice = item.getDefinition().getGeneralPrice();
        long totalPrice = (itemPrice * amountToAdd);
        if (totalPrice > Integer.MAX_VALUE || totalPrice < 0) {
            amountToAdd = player.getInventory().getItemAmount(995) / itemPrice;
        }
        if (itemPrice <= 0) {
            itemPrice = 1;
        }
        if (!player.getInventory().hasItemAmount(995, itemPrice)) {
            player.getPacketSender().sendMessage("You don't have enough coins to purchase this item.");
            return;
        }
        Item itemToAdd = new Item(item.getId(), amountToAdd);
        if (!stackable) {
            if (itemToAdd.getCount() > player.getInventory().freeSlots()) {
                itemToAdd.setItemAmount(player.getInventory().freeSlots());
            }
            if (itemToAdd.getCount() <= 0) {
                player.getPacketSender().sendMessage("Not enough space in your inventory.");
                return;
            }
            totalPrice = (itemPrice * itemToAdd.getCount());
            if (amount > 1) {
                if (shop.getStockItem(slot).getId() != item.getId() || shop.getStockItem(slot).getCount() < 0) {
                    return;
                }
                boolean hasEnough = player.getInventory().hasItemAmount(995, (int) totalPrice);
                if (!hasEnough) {
                    itemToAdd.setItemAmount(player.getInventory().getItemAmount(995) / itemPrice);
                    totalPrice = (itemPrice * itemToAdd.getCount());
                    if (itemToAdd.getCount() <= 0) {
                        player.getPacketSender().sendMessage("You don't have enough coins to purchase this.");
                        return;
                    }
                }
                for (int i = 0; i < itemToAdd.getCount(); i++) {
                    if (!player.getInventory().addItem(new Item(itemToAdd.getId(), 1))) {
                        player.getPacketSender().sendMessage("You didn't have enough inventory space to purchase the full amount.");
                        break;
                    } else {
                        player.getInventory().deleteItem(new Item(995, (int) itemPrice));
                    }
                }
                refreshShopping(player);
            } else if (amount == 1) {
                if (shop.getStockItem(slot).getId() != item.getId() || shop.getStockItem(slot).getCount() < 0) {
                    return;
                }
                boolean hasEnough = player.getInventory().hasItemAmount(995, (int) itemPrice);
                if (!hasEnough) {
                    player.getPacketSender().sendMessage("You don't have enough coins to purchase this.");
                    return;
                }
                itemToAdd = new Item(item.getId(), 1);
                if (!player.getInventory().addItem(itemToAdd)) {
                    player.getPacketSender().sendMessage("You didn't have enough inventory space to purchase the full amount.");
                    return;
                } else {
                    player.getInventory().deleteItem(new Item(995, (int) itemPrice));
                }
                refreshShopping(player);
            }
        } else if (stackable) {
            if (player.getInventory().freeSlots() == -1 && player.getInventory().findItem(item.getId()) == -1) {
                player.getPacketSender().sendMessage("Not enough space in your inventory.");
                return;
            }
            boolean moneyShort = false;
            if (!player.getInventory().hasItemAmount(995, totalPrice)) {
                moneyShort = true;
                itemToAdd.setItemAmount(player.getInventory().getItemAmount(995) / itemPrice);
                totalPrice = (itemPrice * itemToAdd.getCount());
                if (itemToAdd.getCount() < 1) {
                    player.getPacketSender().sendMessage("You don't have enough coins to purchase that item.");
                    return;
                }
                if (player.getInventory().deleteItem(new Item(995, (int) totalPrice))) {
                    player.getInventory().addItem(itemToAdd);
                    if (moneyShort) {
                        player.getPacketSender().sendMessage("You didn't have enough coins to purchase the full amount.");
                    }
                }
            } else if (player.getInventory().getItemAmount(995) >= totalPrice && itemPrice > 0) {
                if (player.getInventory().addItem(itemToAdd)) {
                    player.getInventory().deleteItem(new Item(995, (int) totalPrice));
                }
            }
            refreshShopping(player);
        }
    }

    public void sellItem(Player player, int slot, int amount) {
        ShopLayout currentShop = (ShopLayout) player.getAttributes().get("currentshop");
        if (currentShop != null) {
            currentShop.sellItem(player, slot, amount);
            return;
        }
        if (slot < 0 || slot > 28) {
            return;
        }
        Item invItem = player.getInventory().getSlot(slot);
        if (invItem == null) {
            return;
        }
        if (invItem.getDefinition() == null) {
            return;
        }
        if (invItem.getDefinition().isPlayerBound() || !invItem.getDefinition().isTradeable()) {
            player.getPacketSender().sendMessage("You can't sell this.");
            return;
        }
        Shop shop = (Shop) player.getAttributes().get("shop");
        if (shop == null) {
            return;
        }
        boolean isIronManShop = false;
        if (shop.getId() == 36) {
            isIronManShop = true;
        }
        if (!isIronManShop && !shop.canSellBack()) {
            return;
        }
        if (player.getDetails().isIronman() && !isIronManShop) {
            if (shop.isGeneralShop() || !shop.canSellBack()) {
                player.getPacketSender().sendMessage("As an ironman, you cannot do this.");
                return;
            }
        }
        if (!player.getDetails().isIronman() && !canSellItemToShop(player, invItem, shop)) {
            return;
        }
        if (isIronManShop && !canSellItemToIronmanShop(player, invItem, shop)) {
            return;
        }
        boolean stackable = invItem.getDefinition().isStackable();
        if (stackable) {
            if (player.getInventory().freeSlots() <= 0 && !player.getInventory().hasItem(995)) {
                player.getPacketSender().sendMessage("Not enough space in your inventory.");
                return;
            }
        }
        int amountToAdd = amount;
        int itemId = invItem.getId();
        int custom = getCustomPrice(itemId);
        int base = (invItem.getDefinition().getHighAlchValue() * 2);
        int subTractPercentage = (int) ((double) base * 0.20);
        int itemPrice = custom != -1 ? custom : (invItem.getDefinition().getHighAlchValue() - subTractPercentage);
        Item itemToDelete = new Item(itemId, amountToAdd);
        int inventoryCount = stackable ? invItem.getCount() : player.getInventory().getItemAmount(itemId);

        int equipBonus = -1;
        if (shop.getId() == 34) { //Thieving shop
            int capeId = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.CAPE.ordinal());
            if (capeId == 15347 || capeId == 15349) {
                equipBonus = capeId == 15347 ? 500 : 1000;
                player.getPacketSender().sendMessage("Your receive " + equipBonus + " extra GP from your equipment bonus.");
            }
        }

        if (equipBonus != -1) {
            itemPrice = itemPrice + equipBonus;
        }

        if (itemToDelete.getCount() > inventoryCount) {
            itemToDelete.setItemAmount(inventoryCount);
        }
        long totalPrice = (itemPrice * itemToDelete.getCount());
        if (itemToDelete.getCount() == 1) {
            if (player.getInventory().deleteItem(itemToDelete)) {
                player.getInventory().addItem(new Item(995, itemPrice));
            }
            ShopManager.refreshShopping(player);
        } else if (itemToDelete.getCount() > 1 && stackable) {
            if (player.getInventory().deleteItem(itemToDelete)) {
                player.getInventory().addItem(new Item(995, (int) totalPrice));
            }
            ShopManager.refreshShopping(player);
        } else {
            itemToDelete = new Item(itemId, 1);
            for (int i = 0; i < amountToAdd; i++) {
                if (!player.getInventory().deleteItem(itemToDelete)) {
                    break;
                }
                player.getInventory().addItem(new Item(995, itemPrice));
            }
            refreshShopping(player);
        }
    }

    public void valueItem(Player player, Item item, int type) {
        if (item.getDefinition() == null) {
            return;
        }
        ShopLayout currentShop = (ShopLayout) player.getAttributes().get("currentshop");
        if (currentShop != null) {
            if (type == 0) {// buying
                currentShop.value(player, item, 0);
            } else if (type == 1) {// selling
                currentShop.value(player, item, 1);
            }
        } else if (currentShop == null) {// coins currency shop
            int custom = getCustomPrice(item.getId());
            if (type == 0) {// buying
                int buyprice = custom != -1 ? custom : item.getDefinition().getGeneralPrice();
                player.getPacketSender().sendMessage("" + item.getDefinition().getName() + ": currently costs " + NumberUtils.format(buyprice) + " coins.");
            } else if (type == 1) {// selling
                Shop shop = (Shop) player.getAttributes().get("shop");
                if (shop == null) {
                    return;
                }
                boolean isIronManShop = false;
                if (shop.getId() == 36) {
                    isIronManShop = true;
                }
                if (!(isIronManShop && player.getDetails().isIronman()) && !canSellItemToShop(player, item, shop)) {
                    return;
                } else if (isIronManShop && player.getDetails().isIronman() && !canSellItemToIronmanShop(player, item, shop)) {
                    return;
                }
                int base = (item.getDefinition().getHighAlchValue() * 2);
                int subTractPercentage = (int) ((double) base * 0.20);
                int sellprice = custom != -1 ? custom : (item.getDefinition().getHighAlchValue() - subTractPercentage);
                int equipBonus = -1;
                if (shop.getId() == 34) { //Thieving shop
                    int capeId = player.getEquipment().getItemInSlot(Equipment.EQUIPMENT.CAPE.ordinal());
                    if (capeId == 9777 || capeId == 9778) {
                        equipBonus = capeId == 9778 ? 500 : 1000;
                    }
                }

                player.getPacketSender().sendMessage("" + item.getDefinition().getName() + ": currently sells for " + NumberUtils.format(sellprice)
                        + " coins" + (equipBonus != -1 ? " + " + equipBonus + " bonus coins." : "."));
            }
        }
    }

    public int getCustomPrice(int itemId) {
        int customPrice = -1;
        switch (itemId) {
            case 1897: //chocolate cake
                customPrice = 2250;
            case 1635: //gold ring
                customPrice = 2950;
            case 950: //silk
                customPrice = 3500;
            case 7650: //silver dust
                customPrice = 5250;
            case 1613: //red topaz
                customPrice = 6500;
            case 7462: //barrows gloves
                customPrice = 200000;
            case 7461: //dragon gloves
                customPrice = 150000;
            case 7460:  //rune gloves
                customPrice = 100000;
            case 7459: //addy gloves
                customPrice = 25000;
            case 7458: //Mith gloves
                customPrice = 15000;
            case 7457: //black gloves
                customPrice = 10000;
        }
        return customPrice;
    }

    public static void refreshShopping(Player player) {
        player.getInventory().refresh();
        player.getPacketSender().sendItems(301, 0, 93, player.getInventory().getItems());
        // player.getFrames().sendItems(300, 75, 93, shop.getStock());
    }

    private boolean canSellItemToShop(Player player, Item item, Shop shop) {
        if (!shop.canSellBack()) {
            player.getPacketSender().sendMessage("You can't sell items to this shop.");
            return false;
        }
        if (item.getDefinition().getName().equalsIgnoreCase("coins")) {
            player.getPacketSender().sendMessage("You can't sell coins.");
            return false;
        }
        if (item.getDefinition().getName().equalsIgnoreCase("tokkul")) {
            player.getPacketSender().sendMessage("You can't sell tokkul.");
            return false;
        }
        if (item.getDefinition().isPlayerBound()) {
            player.getPacketSender().sendMessage("You can't sell this item.");
            return false;
        }
        boolean sellableItem = false;
        for (Item items : shop.getStock()) {
            if (items.getId() == item.getId()) {
                sellableItem = true;
            }
        }
        if (!sellableItem && shop.getId() != 0) {
            player.getPacketSender().sendMessage("You can't sell this item here.");
            return false;
        }
        return true;
    }

    private boolean canSellItemToIronmanShop(Player player, Item item, Shop shop) {
        if (item.getDefinition().getName().equalsIgnoreCase("coins")) {
            player.getPacketSender().sendMessage("You can't sell coins.");
            return false;
        }
        if (item.getDefinition().getName().equalsIgnoreCase("tokkul")) {
            player.getPacketSender().sendMessage("You can't sell tokkul.");
            return false;
        }
        if (item.getDefinition().isPlayerBound()) {
            player.getPacketSender().sendMessage("You can't sell this item.");
            return false;
        }
        return true;
    }
}