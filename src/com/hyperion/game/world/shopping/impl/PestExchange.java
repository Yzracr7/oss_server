package com.hyperion.game.world.shopping.impl;

import java.text.NumberFormat;

import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.shopping.Shop;
import com.hyperion.game.world.shopping.ShopLayout;
import com.hyperion.game.world.shopping.ShopManager;

public class PestExchange extends ShopLayout {

    @Override
    public void openShop(Player player, int id) {
        super.openShop(player, id);
        player.getAttributes().set("currentshop", this);
        player.getPacketSender().modifyText("You currently have " + NumberFormat.getInstance().format(player.getVariables().getPestPoints()) + " " + getCurrencyName() + ".", 300, 77);
    }

    @Override
    public void purchaseItem(Player player, int slot, int amount) {
        Item item = null;
        if (slot < 0 || slot > 40) {
            return;
        }
        Shop shop = (Shop) player.getAttributes().get("shop");
        item = shop.getStockItem(slot);
        if (item == null || item.getCount() < 0 || item.getId() < 1) {
            return;
        }
        int wandWave = player.getVariables().getWandWave();
        if (item.getId() == 6910) {//Apprentice wand
            if (wandWave < 1) {
                player.getPacketSender().sendMessage("You need to obtain a Beginner wand first!");
                return;
            }
        } else if (item.getId() == 6912) {//Teacher wand
            if (wandWave < 2) {
                player.getPacketSender().sendMessage("You need to obtain an Apprentice wand first!");
                return;
            }
        } else if (item.getId() == 6914) {//Master wand
            if (wandWave < 2) {
                player.getPacketSender().sendMessage("You need to obtain a Teacher wand first!");
                return;
            }
        }
        boolean stackable = item.getDefinition().isStackable();
        int amountToAdd = amount;
        int itemPrice = getItemPriceForShop(item);
        long totalPrice = (itemPrice * amountToAdd);
        if (totalPrice > Integer.MAX_VALUE || totalPrice < 0) {
            amountToAdd = player.getVariables().getPestPoints() / itemPrice;
        }
        if (itemPrice <= 0) {
            itemPrice = 1;
        }
        Item itemToAdd = new Item(item.getId(), amountToAdd);
        if (!stackable) {
            if (itemToAdd.getCount() > player.getInventory().freeSlots()) {
                itemToAdd.setItemAmount(player.getInventory().freeSlots());
            }
            if (itemToAdd.getCount() <= 0) {
                player.getPacketSender().sendMessage("Not enough space in your inventory.");
                return;
            }
            totalPrice = (itemPrice * itemToAdd.getCount());
            if (amount > 1) {
                if (shop.getStockItem(slot).getId() != item.getId() || shop.getStockItem(slot).getCount() < 0) {
                    return;
                }
                boolean hasEnough = player.getVariables().getPestPoints() >= (int) totalPrice;
                if (!hasEnough) {
                    itemToAdd.setItemAmount(player.getVariables().getPestPoints() / itemPrice);
                    totalPrice = (itemPrice * itemToAdd.getCount());
                    if (itemToAdd.getCount() <= 0) {
                        player.getPacketSender().sendMessage("You don't have enough " + getCurrencyName() + " to purchase this.");
                        return;
                    }
                }
                for (int i = 0; i < itemToAdd.getCount(); i++) {
                    if (!player.getInventory().addItem(new Item(itemToAdd.getId(), 1))) {
                        player.getPacketSender().sendMessage("You didn't have enough inventory space to purchase the full amount.");
                        break;
                    } else {
                        player.getVariables().deductPestPoints((int) itemPrice);
                        if (itemToAdd.getId() == 6908) {//beginner
                            if (wandWave == 0) {
                                player.getVariables().increase_wand_wave(1);
                            }
                        } else if (itemToAdd.getId() == 6910) {//apprentice
                            if (wandWave == 1) {
                                player.getVariables().increase_wand_wave(1);
                            }
                        } else if (itemToAdd.getId() == 6912) {//teacher
                            if (wandWave == 2) {
                                player.getVariables().increase_wand_wave(1);
                            }
                        } else if (itemToAdd.getId() == 6914) {//master
                            if (wandWave == 3) {
                                player.getVariables().increase_wand_wave(1);
                            }
                        }
                    }
                }
                ShopManager.refreshShopping(player);
            } else if (amount == 1) {
                if (shop.getStockItem(slot).getId() != item.getId() || shop.getStockItem(slot).getCount() < 0) {
                    return;
                }
                boolean hasEnough = player.getVariables().getPestPoints() >= (int) itemPrice;
                if (!hasEnough) {
                    player.getPacketSender().sendMessage("You don't have enough " + getCurrencyName() + " to purchase this.");
                    return;
                }
                itemToAdd = new Item(item.getId(), 1);
                if (!player.getInventory().addItem(itemToAdd)) {
                    player.getPacketSender().sendMessage("You didn't have enough inventory space to purchase the full amount.");
                    return;
                } else {
                    player.getVariables().deductPestPoints(itemPrice);
                    if (itemToAdd.getId() == 6908) {//beginner
                        if (wandWave == 0) {
                            player.getVariables().increase_wand_wave(1);
                        }
                    } else if (itemToAdd.getId() == 6910) {//apprentice
                        if (wandWave == 1) {
                            player.getVariables().increase_wand_wave(1);
                        }
                    } else if (itemToAdd.getId() == 6912) {//teacher
                        if (wandWave == 2) {
                            player.getVariables().increase_wand_wave(1);
                        }
                    } else if (itemToAdd.getId() == 6914) {//master
                        if (wandWave == 3) {
                            player.getVariables().increase_wand_wave(1);
                        }
                    }
                }
                ShopManager.refreshShopping(player);
            }
        } else if (stackable) {
            if (player.getInventory().freeSlots() == -1 && player.getInventory().findItem(item.getId()) == -1) {
                player.getPacketSender().sendMessage("Not enough space in your inventory.");
                return;
            }
            boolean moneyShort = false;
            if (player.getVariables().getPestPoints() < totalPrice) {
                moneyShort = true;
                itemToAdd.setItemAmount(player.getVariables().getPestPoints() / itemPrice);
                totalPrice = (itemPrice * itemToAdd.getCount());
                if (itemToAdd.getCount() < 1) {
                    player.getPacketSender().sendMessage("You don't have enough " + getCurrencyName() + " to purchase that item.");
                    return;
                }
                if (player.getInventory().addItem(itemToAdd)) {
                    player.getVariables().deductPestPoints((int) totalPrice);
                    if (moneyShort) {
                        player.getPacketSender().sendMessage("You didn't have enough " + getCurrencyName() + " to purchase the full amount.");
                    }
                }
            } else if (player.getVariables().getPestPoints() >= totalPrice && itemPrice > 0) {
                if (player.getInventory().addItem(itemToAdd)) {
                    player.getVariables().deductPestPoints((int) totalPrice);
                    if (moneyShort) {
                        player.getPacketSender().sendMessage("You didn't have enough " + getCurrencyName() + " to purchase the full amount.");
                    }
                }
            }
            ShopManager.refreshShopping(player);
        }
        player.getPacketSender().modifyText("You currently have " + NumberFormat.getInstance().format(player.getVariables().getPestPoints()) + " " + getCurrencyName() + ".", 300, 77);
    }

    @Override
    public void sellItem(Player player, int slot, int amount) {
        if (slot < 0 || slot > 28) {
            return;
        }
        Item invItem = player.getInventory().getSlot(slot);
        if (invItem == null) {
            return;
        }
        if (!canSellToShop(player, invItem)) {
            return;
        }
        boolean stackable = invItem.getDefinition().isStackable();
        int amountToAdd = amount;
        int itemId = invItem.getId();
        int itemPrice = getSellPriceForShop(invItem);
        Item itemToDelete = new Item(itemId, amountToAdd);
        int inventoryCount = stackable ? invItem.getCount() : player.getInventory().getItemAmount(itemId);
        if (itemToDelete.getCount() > inventoryCount) {
            itemToDelete.setItemAmount(inventoryCount);
        }
        long totalPrice = (itemPrice * itemToDelete.getCount());
        if (itemToDelete.getCount() == 1) {
            if (player.getInventory().deleteItem(itemToDelete)) {
                player.getVariables().increasePestPoints(itemPrice);
            }
            ShopManager.refreshShopping(player);
        } else if (itemToDelete.getCount() > 1 && stackable) {
            if (player.getInventory().deleteItem(itemToDelete)) {
                player.getVariables().increasePestPoints((int) totalPrice);
            }
            ShopManager.refreshShopping(player);
        }
    }

    @Override
    public void value(Player player, Item item, int type) {
        if (type == 0) {//Buying
            int buyprice = getItemPriceForShop(item);
            player.getPacketSender().sendMessage("" + item.getDefinition().getName() + ": currently costs " + NumberFormat.getInstance().format(buyprice) + " " + getCurrencyName() + ".");
        } else if (type == 1) {//Selling
            if (canSellToShop(player, item)) {
                int sellprice = getSellPriceForShop(item);
                player.getPacketSender().sendMessage("" + item.getDefinition().getName() + ": currently sells for " + NumberFormat.getInstance().format(sellprice) + " " + getCurrencyName() + ".");
            }
        }
    }

    @Override
    public String getCurrencyName() {
        return "Pest points";
    }

    @Override
    public int getItemPriceForShop(Item item) {
        switch (item.getDefinition().getName().toLowerCase()) {
            case "master wand":
            case "mage's book":
            case "penance skirt":
            case "fighter hat":
            case "ranger hat":
            case "healer hat":
            case "fighter torso":
            case "infinity hat":
            case "infinity top":
            case "infinity bottoms":
            case "infinity boots":
            case "infinity gloves":
            case "void knight top":
            case "void knight robe":
            case "void knight gloves":
            case "void mage helm":
            case "void ranger helm":
            case "void melee helm":
                return 1000;
            case "chaotic rapier":
            case "chaotic longsword":
            case "chaotic maul":
            case "chaotic staff":
            case "chaotic crossbow":
            case "chaotic kiteshield":
            case "eagle-eye kiteshield":
            case "farseer kiteshield":
                return 10000;
            case "gravite rapier":
            case "gravite longsword":
            case "gravite 2h sword":
            case "gravite staff":
            case "gravite shortbow":
            case "longbow sight":
                return 2000;
            case "beginner wand":
                return 200;
            case "apprentice wand":
                return 400;
            case "teacher wand":
                return 600;
            case "ava's accumulator":
                return 25;
            case "arcane stream necklace":
            case "bonecrusher":
                return 3500;
        }
        return 1;
    }

    @Override
    public int getSellPriceForShop(Item item) {
        int base = (item.getDefinition().getHighAlchValue() * 2);
        int subTractPercentage = (int) ((double) base * 0.20);
        int price = (item.getDefinition().getHighAlchValue() - subTractPercentage);
        return price;
    }

    @Override
    public boolean canSellToShop(Player player, Item item) {
        Shop shop = (Shop) player.getAttributes().get("shop");
        if (!shop.isGeneralShop() && !shop.canSellBack()) {
            player.getPacketSender().sendMessage("You cannot sell items to this shop.");
            return false;
        }
        if (item.getDefinition().getName().equalsIgnoreCase("coins")) {
            player.getPacketSender().sendMessage("You can't sell coins.");
            return false;
        }
        if (item.getDefinition().getName().equalsIgnoreCase("tokkul")) {
            player.getPacketSender().sendMessage("You can't sell tokkul.");
            return false;
        }
        if (item.getDefinition().isPlayerBound()) {
            player.getPacketSender().sendMessage("You can't sell this item.");
            return false;
        }
        boolean sellableItem = false;
        for (Item items : shop.getStock()) {
            if (items.getId() == item.getId()) {
                sellableItem = true;
            }
        }
        if (!sellableItem) {
            player.getPacketSender().sendMessage("You can't sell this item here.");
            return false;
        }
        return true;
    }
}
