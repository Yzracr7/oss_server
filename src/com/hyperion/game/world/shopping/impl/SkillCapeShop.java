package com.hyperion.game.world.shopping.impl;

import java.text.NumberFormat;

import com.hyperion.game.Constants;
import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.shopping.Shop;
import com.hyperion.game.world.shopping.ShopLayout;
import com.hyperion.game.world.shopping.ShopManager;

public class SkillCapeShop extends ShopLayout {

    private static int[] untrimmedCapes = {
            9747, //attack
            9753, //def
            9750, //str
            9768, //hp
            9756, //ranging
            9759, //prayer
            9762, //mage
            9801, //cooking
            9807, //woodcutting
            9783, //fletching
            9798, //fishing
            9804, //firemaking
            9780, //crafting
            9795, //smithing
            9792, //mining
            9774, //herblore
            9771, //agility
            9777, //thieving
            9786, //slayer
            9810, //farming
            9765, //runecrafting
            9948, //hunter
            9789, //construction
            9813, //quest point
    };

    private static boolean has99InSkill(Player player, int slot) {
        if (slot == 23) //quest cape
            return player.getVariables().getQuestPoints() >= Constants.QUEST_POINTS;
        if (player.getSkills().getLevelForXp(slot) >= 99) {
            return true;
        }
        return false;
    }

    private static int getSlotForHoodId(int hood) {
        switch (hood) {
            case 9749:
                return 0;
            case 9755:
                return 1;
            case 9752:
                return 2;
            case 9770:
                return 3;
            case 9758:
                return 4;
            case 9761:
                return 5;
            case 9764:
                return 6;
            case 9803:
                return 7;
            case 9809:
                return 8;
            case 9785:
                return 9;
            case 9800:
                return 10;
            case 9806:
                return 11;
            case 9782:
                return 12;
            case 9797:
                return 13;
            case 9794:
                return 14;
            case 9776:
                return 15;
            case 9773:
                return 16;
            case 9779:
                return 17;
            case 9788:
                return 18;
            case 9812:
                return 19;
            case 9767:
                return 20;
            case 9950:
                return 21;
            case 9791:
                return 22;
            case 9814:
                return 23;
        }
        return -1;
    }

    @Override
    public void openShop(Player player, int id) {
        Shop shop = ShopManager.shopsList.get(id);
        if (shop == null) {
            return;
        }
        Item[] hoodsToShow = new Item[40];
        int index = 0;
        for (Item hoods : shop.getStock()) {
            if (hoods == null) {
                continue;
            }
            if (!has99InSkill(player, getSlotForHoodId(hoods.getId()))) {
                continue;
            }
            hoodsToShow[index] = hoods;
            index++;
        }
        player.getVariables().getSkillCapeShop().setId(12);
        player.getVariables().getSkillCapeShop().setName(shop.getName());
        player.getVariables().set_skillcape_shop(hoodsToShow);
        player.getAttributes().set("shop", player.getVariables().getSkillCapeShop());
        player.getAttributes().set("shopping", true);
        player.getAttributes().set("currentshop", this);
        String name = player.getVariables().getSkillCapeShop().getName();
        player.getPacketSender().modifyText(name, 300, 76);
        player.getPacketSender().sendItems(301, 0, 93, player.getInventory().getItems());
        player.getPacketSender().sendItems(300, 75, 93, player.getVariables().getSkillCapeShop().getStock());
        player.getPacketSender().displayInterface(300);
        player.getPacketSender().displayInventoryInterface(301);
    }

    @Override
    public void purchaseItem(Player player, int slot, int amount) {
        Item item = null;
        if (slot < 0 || slot > 41) {
            return;
        }
        Shop shop = (Shop) player.getAttributes().get("shop");
        item = shop.getStockItem(slot);
        if (item == null || item.getCount() < 0 || item.getId() < 1) {
            return;
        }
        boolean stackable = item.getDefinition().isStackable();
        int amountToAdd = amount;
        int itemPrice = getItemPriceForShop(item);
        long totalPrice = (itemPrice * amountToAdd);
        if (totalPrice > Integer.MAX_VALUE || totalPrice < 0) {
            amountToAdd = player.getInventory().getItemAmount(995) / itemPrice;
        }
        if (itemPrice <= 0) {
            itemPrice = 1;
        }
        if (!player.getInventory().hasItemAmount(995, itemPrice)) {
            player.getPacketSender().sendMessage("You don't have enough coins to purchase this item.");
            return;
        }
        Item itemToAdd = new Item(item.getId(), amountToAdd);
        if (!stackable) {
            if (itemToAdd.getCount() > player.getInventory().freeSlots()) {
                itemToAdd.setItemAmount(player.getInventory().freeSlots());
            }
            if (itemToAdd.getCount() <= 0) {
                player.getPacketSender().sendMessage("Not enough space in your inventory.");
                return;
            }
            totalPrice = (itemPrice * itemToAdd.getCount());
            if (amount > 1) {
                if (shop.getStockItem(slot).getId() != item.getId() || shop.getStockItem(slot).getCount() < 0) {
                    return;
                }
                boolean hasEnough = player.getInventory().hasItemAmount(995, (int) totalPrice);
                if (!hasEnough) {
                    itemToAdd.setItemAmount(player.getInventory().getItemAmount(995) / itemPrice);
                    totalPrice = (itemPrice * itemToAdd.getCount());
                    if (itemToAdd.getCount() <= 0) {
                        player.getPacketSender().sendMessage("You don't have enough coins to purchase this.");
                        return;
                    }
                }
                for (int i = 0; i < itemToAdd.getCount(); i++) {
                    if (player.getInventory().freeSlots() < 3) {
                        player.getPacketSender().sendMessage("Not enough space in your inventory.");
                        break;
                    }
                    if (!player.getInventory().addItem(new Item(itemToAdd.getId(), 1))) {
                        player.getPacketSender().sendMessage("You didn't have enough inventory space to purchase the full amount.");
                        break;
                    } else {
                        player.getInventory().deleteItem(new Item(995, (int) itemPrice));
                        player.getInventory().addItem(new Item(untrimmedCapes[getSlotForHoodId(item.getId())], 1));
                        player.getInventory().addItem(new Item(untrimmedCapes[getSlotForHoodId(item.getId())] + 1, 1));
                    }
                }
                ShopManager.refreshShopping(player);
            } else if (amount == 1) {
                if (shop.getStockItem(slot).getId() != item.getId() || shop.getStockItem(slot).getCount() < 0) {
                    return;
                }
                boolean hasEnough = player.getInventory().hasItemAmount(995, (int) itemPrice);
                if (!hasEnough) {
                    player.getPacketSender().sendMessage("You don't have enough coins to purchase this.");
                    return;
                }
                if (player.getInventory().freeSlots() < 3) {
                    player.getPacketSender().sendMessage("Not enough space in your inventory.");
                    return;
                }
                itemToAdd = new Item(item.getId(), 1);
                if (!player.getInventory().addItem(itemToAdd)) {
                    player.getPacketSender().sendMessage("You didn't have enough inventory space to purchase the full amount.");
                    return;
                } else {
                    player.getInventory().deleteItem(new Item(995, (int) itemPrice));
                    player.getInventory().addItem(new Item(untrimmedCapes[getSlotForHoodId(item.getId())], 1));
                    player.getInventory().addItem(new Item(untrimmedCapes[getSlotForHoodId(item.getId())] + 1, 1));
                }
                ShopManager.refreshShopping(player);
            }
        } else if (stackable) {
            if (player.getInventory().freeSlots() == -1 && player.getInventory().findItem(item.getId()) == -1) {
                player.getPacketSender().sendMessage("Not enough space in your inventory.");
                return;
            }
            boolean moneyShort = false;
            if (!player.getInventory().hasItemAmount(995, totalPrice)) {
                moneyShort = true;
                itemToAdd.setItemAmount(player.getInventory().getItemAmount(995) / itemPrice);
                totalPrice = (itemPrice * itemToAdd.getCount());
                if (itemToAdd.getCount() < 1) {
                    player.getPacketSender().sendMessage("You don't have enough coins to purchase that item.");
                    return;
                }
                if (player.getInventory().deleteItem(new Item(995, (int) totalPrice))) {
                    player.getInventory().addItem(itemToAdd);
                    player.getInventory().addItem(new Item(untrimmedCapes[getSlotForHoodId(item.getId())], 1));
                    if (slot != 23)
                        player.getInventory().addItem(new Item(untrimmedCapes[getSlotForHoodId(item.getId())] + 1, 1));
                    if (moneyShort) {
                        player.getPacketSender().sendMessage("You didn't have enough coins to purchase the full amount.");
                    }
                }
            } else if (player.getInventory().getItemAmount(995) >= totalPrice && itemPrice > 0) {
                if (player.getInventory().addItem(itemToAdd)) {
                    player.getInventory().deleteItem(new Item(995, (int) totalPrice));
                    player.getInventory().addItem(new Item(untrimmedCapes[getSlotForHoodId(item.getId())], 1));
                    if (slot != 23)
                        player.getInventory().addItem(new Item(untrimmedCapes[getSlotForHoodId(item.getId())] + 1, 1));
                }
            }
            ShopManager.refreshShopping(player);
        }
    }

    @Override
    public void sellItem(Player player, int slot, int amount) {
        if (slot < 0 || slot > 28) {
            return;
        }
        Item invItem = player.getInventory().getSlot(slot);
        if (invItem == null) {
            return;
        }
        if (!canSellToShop(player, invItem)) {
            return;
        }
    }

    @Override
    public void value(Player player, Item item, int type) {
        if (type == 0) {//Buying
            int buyprice = getItemPriceForShop(item);
            player.getPacketSender().sendMessage("" + item.getDefinition().getName() + ": currently costs " + NumberFormat.getInstance().format(buyprice) + " coins.");
        } else if (type == 1) {//Selling
            if (!canSellToShop(player, item)) {
                return;
            }
        }
    }

    @Override
    public String getCurrencyName() {
        return "coins";
    }

    @Override
    public int getItemPriceForShop(Item item) {
        return item.getDefinition().getGeneralPrice();
    }
}
