package com.hyperion.game.world.shopping.impl;

import java.text.NumberFormat;

import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.shopping.Shop;
import com.hyperion.game.world.shopping.ShopLayout;
import com.hyperion.game.world.shopping.ShopManager;

public class TokkulShop extends ShopLayout {

    @Override
    public void openShop(Player player, int id) {
        super.openShop(player, id);
        player.getAttributes().set("currentshop", this);
    }

    @Override
    public void purchaseItem(Player player, int slot, int amount) {
        Item item = null;
        if (slot < 0 || slot > 40) {
            return;
        }
        Shop shop = (Shop) player.getAttributes().get("shop");
        item = shop.getStockItem(slot);
        if (item == null || item.getCount() < 0 || item.getId() < 1) {
            return;
        }
        boolean stackable = item.getDefinition().isStackable();
        int amountToAdd = amount;
        int itemPrice = getItemPriceForShop(item);
        long totalPrice = (itemPrice * amountToAdd);
        if (totalPrice > Integer.MAX_VALUE || totalPrice < 0) {
            amountToAdd = player.getInventory().getItemAmount(6529) / itemPrice;
        }
        if (itemPrice <= 0) {
            itemPrice = 1;
        }
        if (!player.getInventory().hasItemAmount(6529, itemPrice)) {
            player.getPacketSender().sendMessage("You don't have enough " + getCurrencyName() + " to purchase this item.");
            return;
        }
        Item itemToAdd = new Item(item.getId(), amountToAdd);
        if (!stackable) {
            if (itemToAdd.getCount() > player.getInventory().freeSlots()) {
                itemToAdd.setItemAmount(player.getInventory().freeSlots());
            }
            if (itemToAdd.getCount() <= 0) {
                player.getPacketSender().sendMessage("Not enough space in your inventory.");
                return;
            }
            totalPrice = (itemPrice * itemToAdd.getCount());
            if (amount > 1) {
                if (shop.getStockItem(slot).getId() != item.getId() || shop.getStockItem(slot).getCount() < 0) {
                    return;
                }
                boolean hasEnough = player.getInventory().hasItemAmount(6529, (int) totalPrice);
                if (!hasEnough) {
                    itemToAdd.setItemAmount(player.getInventory().getItemAmount(6529) / itemPrice);
                    totalPrice = (itemPrice * itemToAdd.getCount());
                    if (itemToAdd.getCount() <= 0) {
                        player.getPacketSender().sendMessage("You don't have enough " + getCurrencyName() + " to purchase this.");
                        return;
                    }
                }
                for (int i = 0; i < itemToAdd.getCount(); i++) {
                    if (!player.getInventory().addItem(new Item(itemToAdd.getId(), 1))) {
                        player.getPacketSender().sendMessage("You didn't have enough inventory space to purchase the full amount.");
                        break;
                    } else {
                        player.getInventory().deleteItem(new Item(6529, (int) itemPrice));
                    }
                }
                ShopManager.refreshShopping(player);
            } else if (amount == 1) {
                if (shop.getStockItem(slot).getId() != item.getId() || shop.getStockItem(slot).getCount() < 0) {
                    return;
                }
                boolean hasEnough = player.getInventory().hasItemAmount(6529, (int) itemPrice);
                if (!hasEnough) {
                    player.getPacketSender().sendMessage("You don't have enough " + getCurrencyName() + " to purchase this.");
                    return;
                }
                itemToAdd = new Item(item.getId(), 1);
                if (!player.getInventory().addItem(itemToAdd)) {
                    player.getPacketSender().sendMessage("You didn't have enough inventory space to purchase the full amount.");
                    return;
                } else {
                    player.getInventory().deleteItem(new Item(6529, (int) itemPrice));
                }
                ShopManager.refreshShopping(player);
            }
        } else if (stackable) {
            if (player.getInventory().freeSlots() == -1 && player.getInventory().findItem(item.getId()) == -1) {
                player.getPacketSender().sendMessage("Not enough space in your inventory.");
                return;
            }
            boolean moneyShort = false;
            if (!player.getInventory().hasItemAmount(6529, totalPrice)) {
                moneyShort = true;
                itemToAdd.setItemAmount(player.getInventory().getItemAmount(6529) / itemPrice);
                totalPrice = (itemPrice * itemToAdd.getCount());
                if (itemToAdd.getCount() < 1) {
                    player.getPacketSender().sendMessage("You don't have enough " + getCurrencyName() + " to purchase that item.");
                    return;
                }
                if (player.getInventory().deleteItem(new Item(6529, (int) totalPrice))) {
                    player.getInventory().addItem(itemToAdd);
                    if (moneyShort) {
                        player.getPacketSender().sendMessage("You didn't have enough " + getCurrencyName() + " to purchase the full amount.");
                    }
                }
            } else if (player.getInventory().getItemAmount(6529) >= totalPrice && itemPrice > 0) {
                if (player.getInventory().addItem(itemToAdd)) {
                    player.getInventory().deleteItem(new Item(6529, (int) totalPrice));
                }
            }
            ShopManager.refreshShopping(player);
        }
    }

    @Override
    public void sellItem(Player player, int slot, int amount) {
        if (slot < 0 || slot > 28) {
            return;
        }
        Item invItem = player.getInventory().getSlot(slot);
        if (invItem == null) {
            return;
        }
        if (!canSellToShop(player, invItem)) {
            return;
        }
        boolean stackable = invItem.getDefinition().isStackable();
        if (stackable) {
            if (player.getInventory().freeSlots() <= 0 && !player.getInventory().hasItem(6529)) {
                player.getPacketSender().sendMessage("Not enough space in your inventory.");
                return;
            }
        }
        int amountToAdd = amount;
        int itemId = invItem.getId();
        int base = (getItemPriceForShop(invItem) * 2);
        int subTractPercentage = (int) ((double) base * 0.20);
        int itemPrice = (getItemPriceForShop(invItem) - subTractPercentage);
        Item itemToDelete = new Item(itemId, amountToAdd);
        int inventoryCount = stackable ? invItem.getCount() : player.getInventory().getItemAmount(itemId);
        if (itemToDelete.getCount() > inventoryCount) {
            itemToDelete.setItemAmount(inventoryCount);
        }
        long totalPrice = (itemPrice * itemToDelete.getCount());
        if (itemToDelete.getCount() == 1) {
            if (player.getInventory().deleteItem(itemToDelete)) {
                player.getInventory().addItem(new Item(6529, itemPrice));
            }
            ShopManager.refreshShopping(player);
        } else if (itemToDelete.getCount() > 1 && stackable) {
            if (player.getInventory().deleteItem(itemToDelete)) {
                player.getInventory().addItem(new Item(6529, (int) totalPrice));
            }
            ShopManager.refreshShopping(player);
        } else {
            itemToDelete = new Item(itemId, 1);
            for (int i = 0; i < amountToAdd; i++) {
                if (!player.getInventory().deleteItem(itemToDelete)) {
                    break;
                }
                player.getInventory().addItem(new Item(6529, itemPrice));
            }
            ShopManager.refreshShopping(player);
        }
    }

    @Override
    public void value(Player player, Item item, int type) {
        if (type == 0) {//Buying
            int buyprice = getItemPriceForShop(item);
            player.getPacketSender().sendMessage("" + item.getDefinition().getName() + ": currently costs " + NumberFormat.getInstance().format(buyprice) + " tokkul.");
        } else if (type == 1) {//Selling
            if (!canSellToShop(player, item)) {
                return;
            }
            int base = (getItemPriceForShop(item) * 2);
            int subTractPercentage = (int) ((double) base * 0.20);
            int sellprice = (getItemPriceForShop(item) - subTractPercentage);
            player.getPacketSender().sendMessage("" + item.getDefinition().getName() + ": currently sells for " + NumberFormat.getInstance().format(sellprice) + " tokkul.");
        }
    }

    @Override
    public String getCurrencyName() {
        return "Tokkul";
    }

    @Override
    public int getItemPriceForShop(Item item) {
        switch (item.getId()) {
            case 6571://Onyx
                return 300000;
            case 6568://Obby cape
                return 90000;
            case 6528://Obby maul
                return 75000;
            case 6527://dunno
                return 45000;
            case 6526://obby staff
                return 52500;
            case 6525://dunno
                return 37500;
            case 6524://Obby shield
                return 67500;
            case 6523://Dunno
                return 60000;
            case 6522://Obby rings
                return 85;
        }
        return item.getDefinition().getGeneralPrice();
    }
}
