package com.hyperion.game.world.shopping.impl;

import java.text.NumberFormat;

import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.shopping.Shop;
import com.hyperion.game.world.shopping.ShopLayout;
import com.hyperion.game.world.shopping.ShopManager;

public class VoteShop extends ShopLayout {

	@Override
	public void openShop(Player player, int id) {
		super.openShop(player, id);
		player.getAttributes().set("currentshop", this);
		player.getPacketSender().modifyText("You currently have " + NumberFormat.getInstance().format(player.getVariables().getVotePoints()) + " " + getCurrencyName() + ".", 300, 77);
	}

	@Override
	public void purchaseItem(Player player, int slot, int amount) {
		Item item = null;
		if (slot < 0 || slot > 40) {
			return;
		}
		Shop shop = (Shop) player.getAttributes().get("shop");
		item = shop.getStockItem(slot);
		if (item == null || item.getCount() < 0 || item.getId() < 1) {
			return;
		}
		boolean stackable = item.getDefinition().isStackable();
		int amountToAdd = amount;
		int itemPrice = getItemPriceForShop(item);
		long totalPrice = (itemPrice * amountToAdd);
		if (totalPrice > Integer.MAX_VALUE || totalPrice < 0) {
			amountToAdd = player.getVariables().getVotePoints() / itemPrice;
		}
		if (itemPrice <= 0) {
			itemPrice = 1;
		}
		amountToAdd = amountToAdd * getItemQuantity(item);
		Item itemToAdd = new Item(item.getId(), amountToAdd);
		//System.err.println("[itemPrice=" + itemPrice + ", points=" + player.getVariables().getVotePoints() + ", totalPrice=" + totalPrice + ", stackable=" + stackable +", amount=" + amount + ", amountToAdd=" + amountToAdd + ", cAmount=" + getItemQuantity(itemToAdd) + "]");
		if (!stackable) {
			if (itemToAdd.getCount() > player.getInventory().freeSlots()) {
				itemToAdd.setItemAmount(player.getInventory().freeSlots());
			}
			if (itemToAdd.getCount() <= 0) {
				player.getPacketSender().sendMessage("Not enough space in your inventory.");
				return;
			}
			totalPrice = (itemPrice * itemToAdd.getCount());
			if (amount > 1) {
				if (shop.getStockItem(slot).getId() != item.getId() || shop.getStockItem(slot).getCount() < 0) {
					return;
				}
				boolean hasEnough = player.getVariables().getVotePoints() >= (int) totalPrice;
				if (!hasEnough) {
					itemToAdd.setItemAmount(player.getVariables().getVotePoints() / itemPrice);
					totalPrice = (itemPrice * itemToAdd.getCount());
					if (itemToAdd.getCount() <= 0) {
						player.getPacketSender().sendMessage("You don't have enough " + getCurrencyName() + " to purchase this.");
						return;
					}
				}
				for (int i = 0; i < itemToAdd.getCount(); i++) {
					if (!player.getInventory().addItem(new Item(itemToAdd.getId(), 1))) {
						player.getPacketSender().sendMessage("You didn't have enough inventory space to purchase the full amount.");
						break;
					} else {
						player.getVariables().deductVotePoints(itemPrice);
					}
				}
				ShopManager.refreshShopping(player);
			} else if (amount == 1) {
				if (shop.getStockItem(slot).getId() != item.getId() || shop.getStockItem(slot).getCount() < 0) {
					return;
				}
				boolean hasEnough = player.getVariables().getVotePoints() >= (int) itemPrice;
				if (!hasEnough) {
					player.getPacketSender().sendMessage("You don't have enough " + getCurrencyName() + " to purchase this.");
					return;
				}
				itemToAdd = new Item(item.getId(), 1);
				if (!player.getInventory().addItem(itemToAdd)) {
					player.getPacketSender().sendMessage("You didn't have enough inventory space to purchase the full amount.");
					return;
				} else {
					player.getVariables().deductVotePoints(itemPrice);
				}
				ShopManager.refreshShopping(player);
			}
		} else if (stackable) {
			if (player.getInventory().freeSlots() == -1 && player.getInventory().findItem(item.getId()) == -1) {
				player.getPacketSender().sendMessage("Not enough space in your inventory.");
				return;
			}
			if (player.getVariables().getVotePoints() < totalPrice) {
				for (int i = 0; i < amount; i++) {
					if (player.getVariables().getVotePoints() >= itemPrice) {
						if (player.getInventory().addItem(itemToAdd = new Item(itemToAdd.getId(), getItemQuantity(item)))) {
							player.getVariables().deductVotePoints(itemPrice);
						}
					} else {
						player.getPacketSender().sendMessage("You didn't have enough " + getCurrencyName() + " to purchase the full amount.");
						break;
					}
				}
			} else if (player.getVariables().getVotePoints() >= totalPrice && itemPrice > 0) {
				if (player.getInventory().addItem(itemToAdd)) {
					player.getVariables().deductVotePoints((int) totalPrice);
				}
			}
			ShopManager.refreshShopping(player);
		}
		player.getPacketSender().modifyText("You currently have " + NumberFormat.getInstance().format(player.getVariables().getVotePoints()) + " " + getCurrencyName() + ".", 300, 77);
	}

	@Override
	public void value(Player player, Item item, int type) {
		if (type == 0) {// Buying
			int buyprice = getItemPriceForShop(item);
			if (buyprice == 1) {
				buyprice = item.getDefinition().getGeneralPrice();
				player.getPacketSender().sendMessage("" + item.getDefinition().getName() + ": currently costs " + NumberFormat.getInstance().format(buyprice) + " Coins.");
			} else {
				player.getPacketSender().sendMessage("" + item.getDefinition().getName() + ": currently costs " + NumberFormat.getInstance().format(buyprice) + " " + getCurrencyName() + ".");
			}
		} else if (type == 1) {// Selling
			if (canSellToShop(player, item)) {
				int sellprice = getSellPriceForShop(item);
				player.getPacketSender().sendMessage("" + item.getDefinition().getName() + ": currently sells for " + NumberFormat.getInstance().format(sellprice) + " " + getCurrencyName() + ".");
			}
		}
	}

	@Override
	public int getItemPriceForShop(Item item) {
		switch (item.getId()) {
		case 995: // 1m coins
			return 40;
		case 15273: // 50 rocktails
			return 20;
		case 15220: // 0xBerserker ring (i)
			return 200;
		case 2572: // 0xRing of wealth
			return 40;
		case 15486: // 0x Staff of light
			return 200;
		case 15441: // 0x Abyssal whip
		case 15442: // 0x Abyssal whip
		case 15443: // 0x Abyssal whip
		case 15444: // 0x Abyssal whip
			return 220;
		case 15701: // 0x Dark bow
		case 15702: // 0x Dark bow
		case 15703: // 0x Dark bow
		case 15704: // 0x Dark bow
			return 160;
		default:
			System.err.println("case " + item.getId() + ": // " + item.getCount() + "x " + item.getDefinition().getName());
			return -1;
		}
	}

	@Override
	public String getCurrencyName() {
		return "Vote Points";
	}

	@Override
	public void sellItem(Player player, int slot, int amount) {
		player.getPacketSender().sendMessage("You cannot sell items to this shop.");
	}

	@Override
	public int getItemQuantity(Item item) {
		switch (item.getId()) {
		case 995:
			return 1_000_000;
		case 15273:
			return 50;
		}
		return 1;
	}

}
