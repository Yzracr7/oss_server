package com.hyperion.game.world.shopping.impl;

import java.text.NumberFormat;

import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.shopping.Shop;
import com.hyperion.game.world.shopping.ShopLayout;
import com.hyperion.game.world.shopping.ShopManager;

public class WarriorsGuildExchange extends ShopLayout {

    @Override
    public void openShop(Player player, int id) {
        super.openShop(player, id);
        player.getAttributes().set("currentshop", this);
        player.getPacketSender().modifyText("You currently have " + NumberFormat.getInstance().format(player.getInventory().getItemAmount(8851)) + " " + getCurrencyName() + ".", 300, 77);
    }

    @Override
    public void purchaseItem(Player player, int slot, int amount) {
        Item item = null;
        if (slot < 0 || slot > 40) {
            return;
        }
        Shop shop = (Shop) player.getAttributes().get("shop");
        item = shop.getStockItem(slot);
        if (item == null || item.getCount() < 0 || item.getId() < 1) {
            return;
        }
        //int defenderWave = player.getVariables().getDefenderWave();
    /*	if (item.getId() == 8845) {//Iron
			if (defenderWave < 1) {
				player.getPacketSender().sendMessage("You need to obtain a Bronze defender first!");
				return;
			}
		} else if (item.getId() == 8846) {//Steel
			if (defenderWave < 2) {
				player.getPacketSender().sendMessage("You need to obtain an Iron defender first!");
				return;
			}
		} else if (item.getId() == 8847) {//blk
			if (defenderWave < 3) {
				player.getPacketSender().sendMessage("You need to obtain a Steel defender first!");
				return;
			}
		} else if (item.getId() == 8848) {//mith
			if (defenderWave < 4) {
				player.getPacketSender().sendMessage("You need to obtain a Black defender first!");
				return;
			}
		} else if (item.getId() == 8849) {//addy
			if (defenderWave < 5) {
				player.getPacketSender().sendMessage("You need to obtain a Mithril defender first!");
				return;
			}
		} else if (item.getId() == 8850) {//rune
			if (defenderWave < 6) {
				player.getPacketSender().sendMessage("You need to obtain a Adamant defender first!");
				return;
			}
		}*/
        boolean stackable = item.getDefinition().isStackable();
        int amountToAdd = amount;
        int itemPrice = getItemPriceForShop(item);
        long totalPrice = (itemPrice * amountToAdd);
        if (totalPrice > Integer.MAX_VALUE || totalPrice < 0) {
            amountToAdd = player.getInventory().getItemAmount(8851) / itemPrice;
        }
        if (itemPrice <= 0) {
            itemPrice = 1;
        }
        if (!player.getInventory().hasItemAmount(8851, itemPrice)) {
            player.getPacketSender().sendMessage("You don't have enough " + getCurrencyName() + " to purchase this item.");
            return;
        }
        Item itemToAdd = new Item(item.getId(), amountToAdd);
        if (!stackable) {
            if (itemToAdd.getCount() > player.getInventory().freeSlots()) {
                itemToAdd.setItemAmount(player.getInventory().freeSlots());
            }
            if (itemToAdd.getCount() <= 0) {
                player.getPacketSender().sendMessage("Not enough space in your inventory.");
                return;
            }
            totalPrice = (itemPrice * itemToAdd.getCount());
            if (amount > 1) {
                if (shop.getStockItem(slot).getId() != item.getId() || shop.getStockItem(slot).getCount() < 0) {
                    return;
                }
                boolean hasEnough = player.getInventory().hasItemAmount(8851, (int) totalPrice);
                if (!hasEnough) {
                    itemToAdd.setItemAmount(player.getInventory().getItemAmount(8851) / itemPrice);
                    totalPrice = (itemPrice * itemToAdd.getCount());
                    if (itemToAdd.getCount() <= 0) {
                        player.getPacketSender().sendMessage("You don't have enough " + getCurrencyName() + " to purchase this.");
                        return;
                    }
                }
                for (int i = 0; i < itemToAdd.getCount(); i++) {
                    if (!player.getInventory().addItem(new Item(itemToAdd.getId(), 1))) {
                        player.getPacketSender().sendMessage("You didn't have enough inventory space to purchase the full amount.");
                        break;
                    } else {
                        player.getInventory().deleteItem(new Item(8851, (int) itemPrice));
					/*	if (itemToAdd.getId() == 8844) {//bronze
							if (defenderWave == 0) {
								player.getVariables().increaseDefenderWave(1);
							}
						} else if (itemToAdd.getId() == 8845) {//Iron
							if (defenderWave == 1) {
								player.getVariables().increaseDefenderWave(1);
							}
						} else if (itemToAdd.getId() == 8846) {//Steel
							if (defenderWave == 2) {
								player.getVariables().increaseDefenderWave(1);
							}
						} else if (itemToAdd.getId() == 8847) {//blk
							if (defenderWave == 3) {
								player.getVariables().increaseDefenderWave(1);
							}
						} else if (itemToAdd.getId() == 8848) {//mith
							if (defenderWave == 4) {
								player.getVariables().increaseDefenderWave(1);
							}
						} else if (itemToAdd.getId() == 8849) {//addy
							if (defenderWave == 5) {
								player.getVariables().increaseDefenderWave(1);
							}
						} else if (itemToAdd.getId() == 8850) {//rune
							if (defenderWave == 6) {
								player.getVariables().increaseDefenderWave(1);
							}
						}*/
                    }
                }
                ShopManager.refreshShopping(player);
            } else if (amount == 1) {
                if (shop.getStockItem(slot).getId() != item.getId() || shop.getStockItem(slot).getCount() < 0) {
                    return;
                }
                boolean hasEnough = player.getInventory().hasItemAmount(8851, (int) itemPrice);
                if (!hasEnough) {
                    player.getPacketSender().sendMessage("You don't have enough " + getCurrencyName() + " to purchase this.");
                    return;
                }
                itemToAdd = new Item(item.getId(), 1);
                if (!player.getInventory().addItem(itemToAdd)) {
                    player.getPacketSender().sendMessage("You didn't have enough inventory space to purchase the full amount.");
                    return;
                } else {
                    player.getInventory().deleteItem(new Item(8851, (int) itemPrice));
					/*if (itemToAdd.getId() == 8844) {//bronze
						if (defenderWave == 0) {
							player.getVariables().increaseDefenderWave(1);
						}
					} else if (itemToAdd.getId() == 8845) {//Iron
						if (defenderWave == 1) {
							player.getVariables().increaseDefenderWave(1);
						}
					} else if (itemToAdd.getId() == 8846) {//Steel
						if (defenderWave == 2) {
							player.getVariables().increaseDefenderWave(1);
						}
					} else if (itemToAdd.getId() == 8847) {//blk
						if (defenderWave == 3) {
							player.getVariables().increaseDefenderWave(1);
						}
					} else if (itemToAdd.getId() == 8848) {//mith
						if (defenderWave == 4) {
							player.getVariables().increaseDefenderWave(1);
						}
					} else if (itemToAdd.getId() == 8849) {//addy
						if (defenderWave == 5) {
							player.getVariables().increaseDefenderWave(1);
						}
					} else if (itemToAdd.getId() == 8850) {//rune
						if (defenderWave == 6) {
							player.getVariables().increaseDefenderWave(1);
						}
					}*/
                }
                ShopManager.refreshShopping(player);
                player.getPacketSender().modifyText("You currently have " + NumberFormat.getInstance().format(player.getInventory().getItemAmount(8851)) + " " + getCurrencyName() + ".", 300, 77);
            }
        } else if (stackable) {
            if (player.getInventory().freeSlots() == -1 && player.getInventory().findItem(item.getId()) == -1) {
                player.getPacketSender().sendMessage("Not enough space in your inventory.");
                return;
            }
            boolean moneyShort = false;
            if (!player.getInventory().hasItemAmount(8851, totalPrice)) {
                moneyShort = true;
                itemToAdd.setItemAmount(player.getInventory().getItemAmount(8851) / itemPrice);
                totalPrice = (itemPrice * itemToAdd.getCount());
                if (itemToAdd.getCount() < 1) {
                    player.getPacketSender().sendMessage("You don't have enough " + getCurrencyName() + " to purchase that item.");
                    return;
                }
                if (player.getInventory().deleteItem(new Item(8851, (int) totalPrice))) {
                    player.getInventory().addItem(itemToAdd);
					/*if (itemToAdd.getId() == 8844) {//bronze
						if (defenderWave == 0) {
							player.getVariables().increaseDefenderWave(1);
						}
					} else if (itemToAdd.getId() == 8845) {//Iron
						if (defenderWave == 1) {
							player.getVariables().increaseDefenderWave(1);
						}
					} else if (itemToAdd.getId() == 8846) {//Steel
						if (defenderWave == 2) {
							player.getVariables().increaseDefenderWave(1);
						}
					} else if (itemToAdd.getId() == 8847) {//blk
						if (defenderWave == 3) {
							player.getVariables().increaseDefenderWave(1);
						}
					} else if (itemToAdd.getId() == 8848) {//mith
						if (defenderWave == 4) {
							player.getVariables().increaseDefenderWave(1);
						}
					} else if (itemToAdd.getId() == 8849) {//addy
						if (defenderWave == 5) {
							player.getVariables().increaseDefenderWave(1);
						}
					} else if (itemToAdd.getId() == 8850) {//rune
						if (defenderWave == 6) {
							player.getVariables().increaseDefenderWave(1);
						}
					}*/
                    if (moneyShort) {
                        player.getPacketSender().sendMessage("You didn't have enough " + getCurrencyName() + " to purchase the full amount.");
                    }
                }
            } else if (player.getInventory().getItemAmount(8851) >= totalPrice && itemPrice > 0) {
                if (player.getInventory().addItem(itemToAdd)) {
				/*	if (itemToAdd.getId() == 8844) {//bronze
						if (defenderWave == 0) {
							player.getVariables().increaseDefenderWave(1);
						}
					} else if (itemToAdd.getId() == 8845) {//Iron
						if (defenderWave == 1) {
							player.getVariables().increaseDefenderWave(1);
						}
					} else if (itemToAdd.getId() == 8846) {//Steel
						if (defenderWave == 2) {
							player.getVariables().increaseDefenderWave(1);
						}
					} else if (itemToAdd.getId() == 8847) {//blk
						if (defenderWave == 3) {
							player.getVariables().increaseDefenderWave(1);
						}
					} else if (itemToAdd.getId() == 8848) {//mith
						if (defenderWave == 4) {
							player.getVariables().increaseDefenderWave(1);
						}
					} else if (itemToAdd.getId() == 8849) {//addy
						if (defenderWave == 5) {
							player.getVariables().increaseDefenderWave(1);
						}
					} else if (itemToAdd.getId() == 8850) {//rune
						if (defenderWave == 6) {
							player.getVariables().increaseDefenderWave(1);
						}
					}*/
                    player.getInventory().deleteItem(new Item(8851, (int) totalPrice));
                }
            }
            ShopManager.refreshShopping(player);
            player.getPacketSender().modifyText("You currently have " + NumberFormat.getInstance().format(player.getInventory().getItemAmount(8851)) + " " + getCurrencyName() + ".", 300, 77);
        }
    }

    @Override
    public void sellItem(Player player, int slot, int amount) {
        if (slot < 0 || slot > 28) {
            return;
        }
        Item invItem = player.getInventory().getSlot(slot);
        if (invItem == null) {
            return;
        }
        boolean stackable = invItem.getDefinition().isStackable();
        if (stackable) {
            if (player.getInventory().freeSlots() <= 0 && !player.getInventory().hasItem(8851)) {
                player.getPacketSender().sendMessage("Not enough space in your inventory.");
                return;
            }
        }
        int amountToAdd = amount;
        int itemId = invItem.getId();
        int base = (invItem.getDefinition().getHighAlchValue() * 2);
        int subTractPercentage = (int) ((double) base * 0.20);
        int itemPrice = (invItem.getDefinition().getHighAlchValue() - subTractPercentage);
        Item itemToDelete = new Item(itemId, amountToAdd);
        int inventoryCount = stackable ? invItem.getCount() : player.getInventory().getItemAmount(itemId);
        if (itemToDelete.getCount() > inventoryCount) {
            itemToDelete.setItemAmount(inventoryCount);
        }
        long totalPrice = (itemPrice * itemToDelete.getCount());
        if (itemToDelete.getCount() == 1) {
            if (player.getInventory().deleteItem(itemToDelete)) {
                player.getInventory().addItem(new Item(8851, itemPrice));
            }
            ShopManager.refreshShopping(player);
        } else if (itemToDelete.getCount() > 1 && stackable) {
            if (player.getInventory().deleteItem(itemToDelete)) {
                player.getInventory().addItem(new Item(8851, (int) totalPrice));
            }
            ShopManager.refreshShopping(player);
        } else {
            itemToDelete = new Item(itemId, 1);
            for (int i = 0; i < amountToAdd; i++) {
                if (!player.getInventory().deleteItem(itemToDelete)) {
                    break;
                }
                player.getInventory().addItem(new Item(8851, itemPrice));
            }
            ShopManager.refreshShopping(player);
            player.getPacketSender().modifyText("You currently have " + NumberFormat.getInstance().format(player.getInventory().getItemAmount(8851)) + " " + getCurrencyName() + ".", 300, 77);
        }
    }

    @Override
    public void value(Player player, Item item, int type) {
        if (type == 0) {//Buying
            int buyprice = getItemPriceForShop(item);
            player.getPacketSender().sendMessage("" + item.getDefinition().getName() + ": currently costs " + NumberFormat.getInstance().format(buyprice) + " " + getCurrencyName() + ".");
        } else if (type == 1) {//Selling
            if (canSellToShop(player, item)) {
                int sellprice = getSellPriceForShop(item);
                player.getPacketSender().sendMessage("" + item.getDefinition().getName() + ": currently sells for " + NumberFormat.getInstance().format(sellprice) + " " + getCurrencyName() + ".");
            }
        }
    }

    @Override
    public String getCurrencyName() {
        return " Warrior guild tokens";
    }

    @Override
    public int getItemPriceForShop(Item item) {
        switch (item.getDefinition().getName().toLowerCase()) {
            case "helm of neitiznot (charged)":
                return 200;
            case "fighter torso":
                return 600;
            case "ava's accumulator":
                return 120;
            case "rune defender":
                return 400;
            case "adamant defender":
                return 300;
            case "mithril defender":
                return 200;
            case "black defender":
                return 100;
            case "steel defender":
                return 50;
            case "iron defender":
                return 10;
        }
        return 1;
    }

    @Override
    public int getSellPriceForShop(Item item) {
        int base = (item.getDefinition().getHighAlchValue() * 2);
        int subTractPercentage = (int) ((double) base * 0.20);
        int price = (item.getDefinition().getHighAlchValue() - subTractPercentage);
        return price;
    }

    @Override
    public boolean canSellToShop(Player player, Item item) {
        Shop shop = (Shop) player.getAttributes().get("shop");
        if (!shop.isGeneralShop() && !shop.canSellBack()) {
            player.getPacketSender().sendMessage("You cannot sell items to this shop.");
            return false;
        }
        if (item.getDefinition().getName().equalsIgnoreCase("coins")) {
            player.getPacketSender().sendMessage("You can't sell coins.");
            return false;
        }
        if (item.getDefinition().getName().equalsIgnoreCase("tokkul")) {
            player.getPacketSender().sendMessage("You can't sell tokkul.");
            return false;
        }
        if (item.getDefinition().isPlayerBound()) {
            player.getPacketSender().sendMessage("You can't sell this item.");
            return false;
        }
        boolean sellableItem = false;
        for (Item items : shop.getStock()) {
            if (items.getId() == item.getId()) {
                sellableItem = true;
            }
        }
        if (!sellableItem) {
            player.getPacketSender().sendMessage("You can't sell this item here.");
            return false;
        }
        return true;
    }
}
