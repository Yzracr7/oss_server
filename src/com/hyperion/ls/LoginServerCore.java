package com.hyperion.ls;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import com.hyperion.ls.util.LoginServerBuffer;

public class LoginServerCore {
	
	public static final String PASSWORD = "e1fds51sd6g416cb32ch48hd1s3f";
	private int world;
	private Socket socket;
	private OutputStream outputStream;
	public InputStream inputStream;
	public LoginServerEngine engine;
	
	public LoginServerCore(int world) {
		this.world = world;
		boolean connectionFailed = false;
		try {
			socket = new Socket("77784.32.73.102", 4600);//84.32.73.102
			socket.setSoTimeout(30000);
			socket.setTcpNoDelay(true);
			socket.setKeepAlive(true);
			outputStream = socket.getOutputStream();
			inputStream = socket.getInputStream();
		} catch(Exception e) {
			System.out.println("Error while connecting to login server.");
			connectionFailed = true;
		}
		if(connectionFailed)
			return;
		engine = new LoginServerEngine(this);
		engine.getPacketSender().sendVerification();
		
	}
	
	public void write(LoginServerBuffer stream) {
		stream.buf[0] = (byte) ((stream.pos - 2) >> 8);
		stream.buf[1] = (byte) (stream.pos - 2 & 0xff);
		LoginServerBuffer transformedBuffer = new LoginServerBuffer(stream.pos);
		transformedBuffer.putBytes(stream.buf, 0, stream.pos);
		try {
			outputStream.write(transformedBuffer.buf, 0, transformedBuffer.pos);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

}
