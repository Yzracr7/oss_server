package com.hyperion.ls;

import com.hyperion.ls.packet.LoginServerPacketHandler;


public class LoginServerEngine implements Runnable {
	
	private LoginServerPacketHandler packetHandler;
	private LoginServerSender packetSender;

	public LoginServerEngine(LoginServerCore lsc) {
		setPacketHandler(new LoginServerPacketHandler(lsc));
		setPacketSender(new LoginServerSender(lsc));
		Thread thread = new Thread(this);
		thread.start();
	}
	
	public void run() {
		try {
			for(;;) {
				getPacketHandler().check();
				Thread.sleep(1000);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public LoginServerSender getPacketSender() {
		return packetSender;
	}

	public void setPacketSender(LoginServerSender packetSender) {
		this.packetSender = packetSender;
	}

	public LoginServerPacketHandler getPacketHandler() {
		return packetHandler;
	}

	public void setPacketHandler(LoginServerPacketHandler packetHandler) {
		this.packetHandler = packetHandler;
	}
	
}
