package com.hyperion.ls;

import com.hyperion.ls.util.LoginServerBuffer;

public class LoginServerSender {

	private LoginServerCore core;
	
	public LoginServerSender(LoginServerCore lsc) {
		core = lsc;
	}
	
	public void sendVerification() {
		LoginServerBuffer buffer = new LoginServerBuffer();
		buffer.putPacket(1);
		buffer.putString(LoginServerCore.PASSWORD);
		core.write(buffer);
	}
	
	public void requestFriendList(String name) {
		LoginServerBuffer buffer = new LoginServerBuffer();
		buffer.putPacket(2);
		buffer.putString(name);
		core.write(buffer);
	}
	
	public void friendAdded(String name, String friend) {
		LoginServerBuffer buffer = new LoginServerBuffer();
		buffer.putPacket(3);
		buffer.putString(name);
		buffer.putString(friend);
		core.write(buffer);
	}
	
	public void friendDeleted(String name, String friend) {
		LoginServerBuffer buffer = new LoginServerBuffer();
		buffer.putPacket(4);
		buffer.putString(name);
		buffer.putString(friend);
		core.write(buffer);
	}
	
	public void playerRegistered(String name) {
		LoginServerBuffer buffer = new LoginServerBuffer();
		buffer.putPacket(5);
		buffer.putString(name);
		core.write(buffer);
	}
	
	public void playerUnregistered(String name) {
		LoginServerBuffer buffer = new LoginServerBuffer();
		buffer.putPacket(6);
		buffer.putString(name);
		core.write(buffer);
	}
	
	public void sendPm(String name, String message, int rights, String sender) {
		LoginServerBuffer buffer = new LoginServerBuffer();
		buffer.putPacket(7);
		buffer.putString(name);
		buffer.putString(message);
		buffer.putByte(rights);
		buffer.putString(sender);
		core.write(buffer);
	}

}
