package com.hyperion.ls.packet;

import com.hyperion.ls.util.LoginServerBuffer;


public interface LoginServerIncomingPacket {

	public void handlePacket(LoginServerBuffer stream);
	
}
