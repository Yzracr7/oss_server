package com.hyperion.ls.packet;

import java.io.IOException;
import java.io.InputStream;

import com.hyperion.ls.LoginServerCore;
import com.hyperion.ls.packet.impl.*;
import com.hyperion.ls.util.LoginServerBuffer;


public class LoginServerPacketHandler {
	
	private LoginServerCore core;
	private static LoginServerIncomingPacket[] incomingPackets = new LoginServerIncomingPacket[30];
	
	public LoginServerPacketHandler(LoginServerCore lsc) {
		core = lsc;
	}
	
	public boolean check() throws IOException {
		try {
			InputStream is = core.inputStream;
			if(is.available() == 0)
				return false;
			int packetSize = is.read() << 8 | is.read();
			LoginServerBuffer buffer = new LoginServerBuffer();
			is.read(buffer.buf, 0, packetSize);
			int packetId = buffer.getUByte();
			if(incomingPackets[packetId] != null) {
				incomingPackets[packetId].handlePacket(buffer);
				return true;
			} else {
				System.out.println("unhandled packet="+packetId);
				return false;
			}
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	static {
		incomingPackets[1] = new AuthorizingPacket();
		incomingPackets[2] = new FriendListRefreshPacket();
		incomingPackets[3] = new SendPmPacket();
	}
	
}
