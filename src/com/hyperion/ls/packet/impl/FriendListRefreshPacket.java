package com.hyperion.ls.packet.impl;

import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.ls.packet.LoginServerIncomingPacket;
import com.hyperion.ls.util.LoginServerBuffer;

public class FriendListRefreshPacket implements LoginServerIncomingPacket {

	public void handlePacket(LoginServerBuffer stream) {
		String playerName = stream.getString();
		int amt = stream.getUByte();
		String[] names = new String[amt];
		int[] worlds = new int[amt];
		for(int i = 0; i < amt; i++) {
			names[i] = stream.getString();
			worlds[i] = stream.getUByte();
		}
		synchronized(World.getWorld().getPlayers()) {
			Player player = World.getWorld().find_player_by_name(playerName);
			if (player == null)
				return;
			/*for(int i = 0; i < names.length; i++) {
				long name = TextUtils.stringToLong(names[i]);
				if(worlds[i] > 0)
					player.getFrames().sendFriend(name, worlds[i]);
				else
					player.getFrames().sendFriend(name, 0);
			}*/
		}
	}

}
