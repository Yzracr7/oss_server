package com.hyperion.ls.packet.impl;

import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.ls.packet.LoginServerIncomingPacket;
import com.hyperion.ls.util.LoginServerBuffer;
import com.hyperion.utility.ProtocolUtils;
import com.hyperion.utility.TextUtils;


public class SendPmPacket implements LoginServerIncomingPacket {

	public void handlePacket(LoginServerBuffer stream) {
		String receiver = stream.getString();
		String message = stream.getString();
		String sender = stream.getString();
		System.out.println(receiver+":"+message+":"+sender);
		long nameAsLong = TextUtils.stringToLong(sender);
		int rights = stream.getUByte();
		byte[] packed = new byte[message.length() + 10];
		ProtocolUtils.textPack(packed, message);
		synchronized(World.getWorld().getPlayers()) {
			Player player = World.getWorld().find_player_by_name(receiver);
			if(player == null)
				return;
			//player.getFrames().sendRecievedPrivateMessage(sender, rights, message);
			//player.getFrames().sendReceivedPrivateMessage(nameAsLong, rights, message);
		}
	}
}
