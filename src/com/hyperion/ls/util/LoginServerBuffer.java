package com.hyperion.ls.util;

public class LoginServerBuffer {
	
    public int pos;
    public byte[] buf;
    
    public final int getUByteS() {
    	return (128 - (buf[pos++]) & 0xff);
    }
    
    public final int getLEShort() {
    	pos += 2;
    	int i = ((((buf[pos - 1]) & 0xff) << 8) + ((buf[pos - 2]) & 0xff));
    	if (i > 32767)
    		i -= 65536;
    	return i;
    }
    
    public final int getUByte() {
    	return ((buf[pos++]) & 0xff);
    }
    
    public final int getInt() {
    	pos += 4;
    	return (((buf[pos - 1]) & 0xff)
    		 + (((buf[pos - 2]) & 0xff) << 8)
    		 + (((buf[pos - 4]) & 0xff) << 24)
    		 + (((buf[pos - 3]) & 0xff) << 16));
    }
    
    public final void putByteC(int i) {
    	buf[pos++] = (byte) -i;
    }
    
    public final int getInt1() {
    	pos += 4;
    	return (((buf[pos - 2]) & 0xff) << 24) 
    		 + (((buf[pos - 1]) & 0xff) << 16)
    		 + (((buf[pos - 4]) & 0xff) << 8)
		     + (((buf[pos - 3]) & 0xff));
    }
    
    public final int getInt2() {
    	pos += 4;
    	return (((buf[pos - 2]) & 0xff)
    		 + (((buf[pos - 1]) & 0xff) << 8)
    		 + (((buf[pos - 3]) & 0xff) << 24)
    		 + (((buf[pos - 4]) & 0xff) << 16));
    }
    
    public final void putString(String string) {
    	System.arraycopy(string.getBytes(), 0, buf, pos, string.length());
    	pos += string.length();
    	buf[pos++] = (byte) 0;
    }
    
    public final int getShort() {
    	pos += 2;
    	int i_9_ = ((((buf[pos - 2]) & 0xff) << 8) + ((buf[pos - 1]) & 0xff));
    	if (i_9_ > 32767)
    		i_9_ -= 65536;
    	return i_9_;
    }
    
    public final void getBytes(byte[] buffer, int off, int len) {
    	for (int id = off; len + off > id; id++)
    		buffer[id] = (buf[pos++]);
    }
    
    public final int getULEShort() {
    	pos += 2;
    	return (((buf[pos - 2]) & 0xff) + ((buf[pos - 1]) << 8 & 0xff00));
    }
    
    public final int getSmart() {
    	int i_13_ = ((buf[pos]) & 0xff);
    	if (i_13_ < 128)
    		return getUByte() - 64;
    	return getUShort() - 49152;
    }
    
    public final int method428() {
    	int i_14_ = 0;
    	int i_15_;
    	for (i_15_ = (buf[pos++]); i_15_ < 0; i_15_ = (buf[pos++]))
    		i_14_ = (i_15_ & 0x7f | i_14_) << 7;
    	return i_15_ | i_14_;
    }
    
    public final int getLEShortA() {
    	pos += 2;
    	int i_16_ = (((buf[pos - 2]) - 128 & 0xff) + (((buf [pos - 1]) & 0xff) << 8));
    	if (i_16_ > 32767)
    		i_16_ -= 65536;
    	return i_16_;
    }
    
    public final void putLong(long l) {
    	buf[pos++] = (byte) (int) (l >> 56);
    	buf[pos++] = (byte) (int) (l >> 48);
		buf[pos++] = (byte) (int) (l >> 40);
		buf[pos++] = (byte) (int) (l >> 32);
		buf[pos++] = (byte) (int) (l >> 24);
		buf[pos++] = (byte) (int) (l >> 16);
		buf[pos++] = (byte) (int) (l >> 8);
		buf[pos++] = (byte) (int) l;
    }
    
    public final void putByte(int i) {
    	buf[pos++] = (byte) i;
    }
    
    public final int getLEInt() {
    	pos += 4;
    	return (((buf[pos - 4]) & 0xff)
    		 + (((buf[pos - 2]) & 0xff) << 16)
    		 + (((buf[pos - 1]) & 0xff) << 24)
    		 + (((buf[pos - 3]) & 0xff) << 8));
    }
    
    public final void decodeXTEA(int[] buf, int off, int len) {
    	int i_19_ = pos;
    	pos = off;
    	int i_20_ = (len - off) / 8;
    	for (int i_21_ = 0; i_21_ < i_20_; i_21_++) {
    		int i_22_ = getInt();
    		int i_23_ = -957401312;
    		int i_24_ = -1640531527;
    		int i_25_ = getInt();
    		int i_26_ = 32;
    		while (i_26_-- > 0) {
    			i_25_ -= (i_23_ + buf[(i_23_ & 0x1bbf) >>> 11] ^ i_22_ + (i_22_ << 4 ^ i_22_ >>> 5));
    			i_23_ -= i_24_;
    			i_22_ -= (i_25_ + (i_25_ << 4 ^ i_25_ >>> 5) ^ i_23_ + buf[i_23_ & 0x3]);
		    }
    		pos -= 8;
    		putInt(i_22_);
    		putInt(i_25_);
    	}
    	pos = i_19_;
    }
    
    public final void putLEShort(int i) {
    	buf[pos++] = (byte) i;
    	buf[pos++] = (byte) (i >> 8);
    }
    
    public final void putLEInt(int i_5_) {
    	buf[pos++] = (byte) i_5_;
	    buf[pos++] = (byte) (i_5_ >> 8);
	    buf[pos++] = (byte) (i_5_ >> 16);
	    buf[pos++] = (byte) (i_5_ >> 24);
    }
    
    public final void putInt1(int i_47_) {
    	buf[pos++] = (byte) (i_47_ >> 8);
    	buf[pos++] = (byte) i_47_;
    	buf[pos++] = (byte) (i_47_ >> 24);
    	buf[pos++] = (byte) (i_47_ >> 16);
    }
    
    public final void putInt2(int i_28_) {
		buf[pos++] = (byte) (i_28_ >> 16);
	    buf[pos++] = (byte) (i_28_ >> 24);
	    buf[pos++] = (byte) i_28_;
	    buf[pos++] = (byte) (i_28_ >> 8);
    }
    
    public final byte getByte() {
    	return (buf[pos++]);
    }
    
    public final int getULEShortA() {
    	pos += 2;
    	return (((buf[pos - 2]) - 128 & 0xff) + (((buf[pos - 1]) & 0xff) << 8));
    }
    
    public final byte getByteA() {
    	return (byte) ((buf[pos++]) - 128);
    }
    
    public final int getUByteA() {
    	return ((buf[pos++]) - 128 & 0xff);
    }
    
    public final int getUSmart() {
    	int i = buf[pos] & 0xff;
    	if (i >= 128)
    		return getUShort() - 32768;
    	return getUByte();
    }
    
    public final void putByteA(int i) {
    	buf[pos++] = (byte) (i + 128);
    }
    
    public final int getTriByte() {
    	pos += 3;
    	return (((buf[pos - 3]) & 0xff) << 16)
		     + (((buf[pos - 2]) & 0xff) << 8)
		      + ((buf[pos - 1]) & 0xff);
    }
    
    public final void getBytesA(byte[] buffer, int off, int len) {
    	for (int i_31_ = off; len + off > i_31_; i_31_++)
    		buffer[i_31_] = (byte) (buf[pos++] - 128);
    }
    
    public final void putShortA(int i) {
    	buf[pos++] = (byte) (i >> 8);
    	buf[pos++] = (byte) (i + 128);
    }
    
    public final int getUShortA() {
    	pos += 2;
    	return ((((buf[pos - 2]) & 0xff) << 8) + ((buf[pos - 1]) - 128 & 0xff));
    }
    
    public final byte getByteS() {
    	return (byte) (-(buf[pos++])+ 128);
    }
    
    public final int getUByteC() {
    	return (-(buf[pos++]) & 0xff);
    }
    
    public final long getLong() {
    	long l = (long) getInt() & 0xffffffffL;
		long l_32_ = (long) getInt() & 0xffffffffL;
		return l_32_ + (l << 32);
    }
    
    public final int getTriByte2() {
    	pos += 3;
    	return (((buf[pos - 2]) & 0xff) << 16)
		     + (((buf[pos - 3]) & 0xff) << 8)
		      + ((buf[pos - 1]) & 0xff);
    }
    
    public final void putLEShortA(int i) {
    	buf[pos++] = (byte) (i + 128);
    	buf[pos++] = (byte) (i >> 8);
    }
    
    public final void putSizeByte(int i_33_) {
    	buf[(pos - i_33_ + -1)] = (byte) i_33_;
    }
    
    public final String getString() {
    	int i = pos;
    	while ((buf[pos++]) != 0) {
    		/* clearAllBoxes */
    	}
    	return new String(buf, i, pos - i - 1);
    }
    
    private final void putShort(int i_48_) {
    	buf[pos++] = (byte) (i_48_ >> 8);
    	buf[pos++] = (byte) i_48_;
    }
    
    public final void putByteS(int i) {
    	buf[pos++] = (byte) (128 - i);
    }
    
    public final void putSmart(int i) {
    	if (i >= 0 && i < 128)
    		putByte(i);
    	else {
    		if (i >= 0 && i < 32768)
    			putShort(i + 32768);
    		else
    			throw new IllegalArgumentException();
    	}
    }
    
    public LoginServerBuffer(byte[] is) {
    	buf = is;
    	pos = 0;
    }
    
    public LoginServerBuffer(int len) {
    	buf = new byte[len];
    	pos = 0;
    }
    
    public LoginServerBuffer() {
    	buf = new byte[5000];
    	pos = 0;
    }
    
    private final void putInt(int i) {
    	buf[pos++] = (byte) (i >> 24);
    	buf[pos++] = (byte) (i >> 16);
    	buf[pos++] = (byte) (i >> 8);
    	buf[pos++] = (byte) i;
    }
    
    public final void putBytes(byte[] buffer, int off, int len) {
    	for (int i_51_ = off; i_51_ < len + off; i_51_++)
    		buf[pos++] = buffer[i_51_];
    }
    
    public final void putTriByte(int i) {
    	buf[pos++] = (byte) (i >> 16);
    	buf[pos++] = (byte) (i >> 8);
    	buf[pos++] = (byte) i;
    }
    
    public final int getUShort() {
    	pos += 2;
    	return ((((buf[pos - 2]) & 0xff) << 8)
		       + ((buf[pos - 1]) & 0xff));
    }

    public void putPacket(int i) {
    	buf[2] = (byte) i;
    	pos = 3;
    }
    
}