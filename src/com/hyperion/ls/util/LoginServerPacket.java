package com.hyperion.ls.util;

public class LoginServerPacket {
	
	public int packetId;
	public LoginServerBuffer buffer;
	
	public LoginServerPacket(LoginServerBuffer b) {
		buffer = b;
		packetId = b.getUByte();
	}

}
