package com.hyperion.sql;

import com.hyperion.Logger;
import com.hyperion.game.Constants;

import java.sql.Connection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SQLController {

    public static final ExecutorService SQL_SERVICE = Executors.newSingleThreadExecutor();

    public static void toggle() {
        if (Constants.SERVER_HOSTED) {
            MySQLProcessor.terminate();
            CONTROLLER = null;
            DATABASES = null;
        } else if (!Constants.SERVER_HOSTED) {
            init();
        }
    }

    private static SQLController CONTROLLER;

    public static void init() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }
        CONTROLLER = new SQLController();
    }

    public static SQLController getController() {
        return CONTROLLER;
    }


    public enum Database {
        SMITE_IPB
    }

    private static SQLDatabase[] DATABASES = new SQLDatabase[2];

    public SQLDatabase getDatabase(Database database) {
        return DATABASES[database.ordinal()];
    }


    public SQLController() {
        long startup = System.currentTimeMillis();
        DATABASES = new SQLDatabase[]{
                new SQLDatabase("www.smite.io", 3306, "Smite_ipb", "hooligan", "D,SpMUPT~o7;")
        };
        Logger.getInstance().warning("Connected to smite database : www.smite.io");
        MySQLProcessor.process();
    }

    private static class MySQLProcessor {

        private static boolean running;

        private static void terminate() {
            running = false;
        }

        public static void process() {
            if (running) {
                return;
            }
            running = true;
            SQL_SERVICE.submit(new Runnable() {
                public void run() {
                    try {
                        while (running) {
                            if (!Constants.SERVER_HOSTED) {
                                terminate();
                                return;
                            }
                            for (SQLDatabase database : DATABASES) {

                                if (!database.active) {
                                    continue;
                                }

                                if (database.connectionAttempts >= 5) {
                                    database.active = false;
                                }

                                Connection connection = database.getConnection();
                                try {
                                    connection.createStatement().execute("/* ping */ SELECT 1");
                                } catch (Exception e) {
                                    database.createConnection();
                                }
                            }
                            Thread.sleep(25000);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
