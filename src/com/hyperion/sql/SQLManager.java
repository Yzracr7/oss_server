package com.hyperion.sql;

import com.hyperion.game.Constants;
import com.hyperion.game.discord.Discord;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.Skills;

import java.sql.*;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class SQLManager {

    private static final String IP_ADDRESS = "www.smite.io";

    private static List<DatabaseQueue> databases = new ArrayList<DatabaseQueue>();

    private static DatabaseQueue highscoresDatabase;

    static {
        databases.add(setHighscoresDatabase(new DatabaseQueue(IP_ADDRESS, "highscore_access", "$SAd}P0[Py0#", "highscores_smite")));
    }

    public static void main(String[] strings) {
        String subDatabase = "hs_users";
        highscoresDatabase.addToQueue("DELETE FROM " + subDatabase + " WHERE username='snowman'");
        processQuery();
    }

    /**
     * @param player Puts the player's highscore information in a queue to be updated
     */
    public static void updateHighscores(Player player) {
        if (player.getDetails().isIronman()) {
            return;
        }
        String subDatabase = "hs_users";
        //remove entry
        highscoresDatabase.addToQueue("DELETE FROM " + subDatabase + " WHERE username='" + player.getDetails().getName() + "'");

        if (!canBeInHighscores(player)) {
            return;
        }
        //add entry
        String query = "INSERT INTO " + subDatabase + " (";
        for (int skillId = 0; skillId < Skills.SKILL_NAME.length; skillId++) {
            query += (Skills.SKILL_NAME[skillId].toLowerCase() + "_xp, ");
        }
        query += "username, ";
        //query += "ironman, ";
        query += "total_level, ";
        query += "overall_xp, ";
        query += "combat_level)";
        query += " VALUES (";
        for (int skillId = 0; skillId < Skills.SKILL_NAME.length; skillId++) {
            query += ((int) player.getSkills().getXp(skillId)) + ", ";
        }
        query += "'" + player.getDetails().getName() + "', ";
        //query += (player.getDetails().isIronman() ? 1 : 0)+", ";
        query += player.getSkills().getTotalLevel() + ", ";
        query += player.getSkills().getTotalXp() + ", ";
        query += player.getSkills().getCombatLevel() + ")";
        highscoresDatabase.addToQueue(query);
    }

    public static boolean canBeInHighscores(Player player) {
        if (player.getDetails().isOwner() || player.getDetails().isAdmin()) {
            return false;
        }
        return true;
    }

    /**
     * Process through the querys
     */
    public static void processQuery() {
        for (DatabaseQueue database : databases) {
            database.run();
        }
    }

    public static class DatabaseQueue extends Database {

        private Deque<String> queue = new LinkedList<String>();
        private static final int QUERY_LIMIT = 50;
        private boolean isBusy;

        public DatabaseQueue(String host, String user, String pass,
                             String database) {
            super(host, user, pass, database);
        }

        public void addToQueue(String query) {
            if (query.toLowerCase().startsWith("select")) {
                System.err.println("DatabaseQueue is only for UPDATE, DELETE, and Insert Statements.");
                return;
            }
            queue.add(query);
        }

        @Override
        public void run() {
            if (isBusy || queue.isEmpty())
                return;
            isBusy = true;
            if (!connect())
                return;
            int processed = 0;
            while (!isFinished(processed)) {
                try {
                    if (Constants.SERVER_HOSTED) {
                        executeUpdate(getDeque().poll());
                        processed++;
                    } else {
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            isBusy = false;
            destroyAll();
        }

        public boolean isFinished(int processed) {
            return processed >= QUERY_LIMIT || getDeque().isEmpty();
        }

        public Deque<String> getDeque() {
            return queue;
        }
    }

    public static class Database extends Thread {

        private Connection conn;
        private Statement stmt;

        private String host;
        private String user;
        private String pass;
        private String database;

        public Database(String host, String user, String pass, String database) {
            this.host = host;
            this.user = user;
            this.pass = pass;
            this.database = database;
        }

        public boolean connect() {
            try {
                conn = DriverManager.getConnection("jdbc:mysql://" + host + ":3306/" + database, user, pass);
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        }

        public PreparedStatement prepare(String query) throws SQLException {
            return conn.prepareStatement(query);
        }

        public int executeUpdate(String query) {
            try {
                //Discord.speak("Updating highscores...");
                System.out.println("Executing database query to smite.io");
                stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                return stmt.executeUpdate(query);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            return -1;
        }

        public ResultSet executeQuery(String query) {
            try {
                //System.out.println("Executing database query to smite.io");
                stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                return stmt.executeQuery(query);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            return null;
        }

        public void destroyAll() {
            try {
                conn.close();
                stmt.close();
                conn = null;
                stmt = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public Connection getConn() {
            return conn;
        }
    }

    public static DatabaseQueue getHighscoresDatabase() {
        return highscoresDatabase;
    }

    public static DatabaseQueue setHighscoresDatabase(DatabaseQueue highscoresDatabase) {
        SQLManager.highscoresDatabase = highscoresDatabase;
        return highscoresDatabase;
    }

    //private final static String verify_statement = "SELECT `members_display_name`,  `members_pass_hash`, `members_pass_salt`, "
    //		+ "`member_group_id`, `mgroup_others`, `msg_count_new`, `tokens`, `totalspent`, `email`"
    //		+ " FROM `members` WHERE `members_display_name` LIKE ?";
}