package com.hyperion.sql.login;

import com.hyperion.Server;
import com.hyperion.game.Constants;
import com.hyperion.game.Constants.ReturnCodes;
import com.hyperion.game.content.clanchat.ClanChat;
import com.hyperion.game.content.minigames.Settings;
import com.hyperion.game.content.wilderness.BountyHunter;
import com.hyperion.game.net.PacketBuilder;
import com.hyperion.game.world.World;
import com.hyperion.game.world.entity.player.Player;
import com.hyperion.game.world.entity.player.PlayerDetails;
import com.hyperion.game.world.entity.player.Rights;
import com.hyperion.sql.SQLController;
import com.hyperion.sql.SQLDatabase;
import com.hyperion.sql.SQLManager;
import com.hyperion.sql.util.BCrypt;
import com.hyperion.utility.TextUtils;
import com.hyperion.utility.json.JsonManager;
import com.hyperion.utility.json.impl.PunishmentLoader;
import com.hyperion.utility.json.sub.Punishment.PunishmentType;
import org.apache.mina.core.future.IoFuture;
import org.apache.mina.core.future.IoFutureListener;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

/**
 * Login manager for webhost like database checks
 *
 * @author trees
 */
public class LoginManager {

    /**
     * Loads a player's game in the work service.
     *
     * @param pd The player's details.
     */
    public static void load_game(final PlayerDetails pd) {
        int code = ReturnCodes.LOGIN_OK;
        String username = pd.getName();
        String password = pd.getPassword();

        if (username.length() < 3 || username.length() > 12 || !TextUtils.isValidName(username)) {
            code = ReturnCodes.INVALID_PASSWORD;
        } else if (World.getWorld().isUpdateInProgress()) {
            code = Constants.ReturnCodes.GAME_UPDATED_RELOAD;
        } else if (World.getWorld().isPlayerOnline(username)) {
            code = Constants.ReturnCodes.ALREADY_ONLINE;
        } else if (pd.isOutdated()) {
            code = Constants.ReturnCodes.GAME_UPDATED_RELOAD;
        }
        if (JsonManager.getSingleton().getLoader(PunishmentLoader.class).isPunished(pd.getIP(), PunishmentType.IPBAN)
                || JsonManager.getSingleton().getLoader(PunishmentLoader.class).isPunished(username, PunishmentType.IPBAN)
                || JsonManager.getSingleton().getLoader(PunishmentLoader.class).isPunished(username, PunishmentType.BAN)
                || JsonManager.getSingleton().getLoader(PunishmentLoader.class).isPunished(pd.getMacAddress(), PunishmentType.MACBAN)) {
            code = ReturnCodes.BANNED;
        }
        if (Constants.SERVER_HOSTED) {
            SQLDatabase rsc = SQLController.getController().getDatabase(SQLController.Database.SMITE_IPB);
            try {
                String selectSQL = "SELECT * FROM core_members WHERE name = '" + pd.getName() + "'";
                PreparedStatement preparedStatement = rsc.getConnection().prepareStatement(selectSQL);
                ResultSet rs = preparedStatement.executeQuery(selectSQL);
                while (rs.next()) {
                    int id = rs.getInt("member_id");
                    String user = rs.getString("name");
                    String hash = rs.getString("members_pass_hash");
                    String salt = rs.getString("members_pass_salt");
                    String email = rs.getString("email");
                    int group = rs.getInt("member_group_id");
                    int other_group = rs.getInt("mgroup_others");
                    int warning_level = rs.getInt("warn_level");
                    String timezone = rs.getString("timezone");
                    if (JsonManager.getSingleton().getLoader(PunishmentLoader.class).isPunished(pd.getIP(), PunishmentType.IPBAN)
                            || JsonManager.getSingleton().getLoader(PunishmentLoader.class).isPunished(username, PunishmentType.IPBAN)
                            || JsonManager.getSingleton().getLoader(PunishmentLoader.class).isPunished(username, PunishmentType.BAN)
                            || JsonManager.getSingleton().getLoader(PunishmentLoader.class).isPunished(pd.getMacAddress(), PunishmentType.MACBAN)) {
                        code = ReturnCodes.BANNED;
                        closeSession(pd, code);
                        return;
                    }
                    // 2 = OK
                    // 3 = Invalid Password
                    // 4 = Disabled Account
                    // 5 = Already Logged in
                    // 6 = Smite Updated Reload
                    // if (group != 4 && (Constants.BETA_PLAYER_ONLY_ACCESS && group != 16)) {
                    //  code = 4;
                    // System.out.println("[Login Not Permitted] : " + username + "'s group '" + group + "' was rejected from logging in.");
                    //closeSession(pd, code);
                    // return;  } else
                    if (BCrypt.checkpw(pd.getPassword(), hash) && warning_level < 10) {
                        System.out.println("Successful login usr[" + username + "]g[" + group + "]id[" + id + "][timzn]" + timezone + " [eml]" + email + (warning_level > 0 ? " [blkmrks]" + warning_level : ""));// + " [group]" + group + " [hash]" + (hash.length() > 10 ? "true" : "false") + " [salt]" + (salt.length() > 10 ? "true" : "false"));
                        register_player(pd, group, other_group);
                        return;
                    } else if (user.equalsIgnoreCase(pd.getName())) {
                        code = ReturnCodes.INVALID_PASSWORD;
                        closeSession(pd, code);
                        return;
                    } else {
                        code = ReturnCodes.UPDATE_IN_PROGRESS;
                        System.out.println("[Login] : " + username + " does not exist");
                        closeSession(pd, code);
                        return;
                    }
                }
                if (!rs.next()) {
                    code = ReturnCodes.INVALID_PASSWORD;
                    closeSession(pd, code);
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (code == ReturnCodes.LOGIN_OK) {
            register_player(pd);
            return;
        }
        closeSession(pd, code);
    }

    private static void closeSession(PlayerDetails pd, int code) {
        pd.getSession().write(new PacketBuilder().put((byte) code).toPacket()).addListener(new IoFutureListener<IoFuture>() {
            @Override
            public void operationComplete(IoFuture future) {
                future.getSession().close(false);
            }
        });
    }

    private static void register_player(PlayerDetails pd, int group, int other_group) {
        if (other_group == 4 || other_group == 10 || group == 10) {
            pd.setAdmin(true);
        } else if (group == 13) {
            pd.setSuperDonator(true);
        } else if (group == 14) {
            pd.isLegendaryDonator();
        } else if (group == 6) {
            pd.setModerator(true);
        }
        register_player(pd);
    }

    private static void register_player(PlayerDetails pd) {
        setGroups(pd);
        Player player = new Player(pd);
        player.getSession().setAttribute("player", player);
        World.getWorld().getWorldLoader().loadPlayer(player);
        register_into_game(player);
    }

    private static void register_into_game(final Player player) {
        int returnCode = Constants.ReturnCodes.LOGIN_OK;

        if (World.getWorld().isPlayerOnline(player.getName()))
            returnCode = Constants.ReturnCodes.ALREADY_ONLINE;
        else if (World.getWorld().addPlayer(player) == -1)
            returnCode = Constants.ReturnCodes.WORLD_FULL;

        player.getSession().write(new PacketBuilder().put((byte) returnCode).put((byte) player.getDetails().getRights()).put((byte) 0).putShort(player.getIndex()).put((byte) 1).toPacket()).addListener(new IoFutureListener<IoFuture>() {
            @Override
            public void operationComplete(IoFuture future) {
                player.getAttributes().set("sessionConnected", true);
                if (player != null && player.getSession() != null && player.getSession().getRemoteAddress() != null) {
                    player.getPacketSender().sendLogin();
                    World.getWorld().addPing(player.getName());
                } else if (player.getSession().getRemoteAddress() == null) {
                    System.out.println("no remote address for " + player.getName() + ", do something here");
                }

                if (Constants.ENABLE_LOGIN_SERVER) {
                    Server.loginServer.engine.getPacketSender().playerRegistered(player.getName());
                }
            }
        });
        if (returnCode == 2) {
            //System.out.println("" + player.getName() + " has logged in. [" + World.getWorld().getPlayerCount() + "]");
            //LogsManager.send_login_logs(player.getName(), player.getDetails().getIP(), player.getDetails().getMacAddress());
        }
    }

    public static void unregister_player(final Player player) {
        World.getWorld().getEngine().submitSQL(new Runnable() {
            public void run() {
                if (player != null)
                    SQLManager.updateHighscores(player);
            }
        });
        player.getInterfaceSettings().closeInterfaces(false);
        player.getVariables().refreshLastAddress();
        player.getFriends().unregistered();
        Settings.handle_logouts(player);
        World.getWorld().unregister_player_npcs(player);
        World.updateEntityRegion(player, true);
        if (player.getAttributes().isSet("wildy"))
            World.getWorld().getPlayersInWilderness().remove(player.getName());
        if (player.getAttributes().isSet("target")) {
            Player target = World.getWorld().find_player_by_name(player.getAttributes().get("target"));
            if (target != null) {
                BountyHunter.targetLeft(target, false);
            }
        } else if (World.getWorld().getPossibleTargets().contains(player.getName()))
            World.getWorld().getPossibleTargets().remove(player.getName());

        if (player.getClanWarsSession() != null) {
            player.getClanWarsSession().declineWar(false);
        }
        if (player.getAttributes().isSet("clan_chat_id")) {
            ClanChat.leave(player, true);
        }
        World.getWorld().removePlayer(player);
        World.getWorld().setLogoutPing(player.getName());
        player.getSession().close(false);
        System.out.println("" + player.getName() + " has logged out. [" + World.getWorld().getPlayerCount() + "]");
        if (Constants.ENABLE_LOGIN_SERVER) {
            Server.loginServer.engine.getPacketSender().playerUnregistered(player.getName());
        }
        World.getWorld().getWorldLoader().savePlayer(player);
    }

    public static void setGroups(PlayerDetails details) {
        List<Integer> rights = details.getAllGroups();
        if (rights == null) {
            return;
        }
        for (Integer rightId : rights) {
            Rights right = Rights.getRights(rightId);
            if (right == null) {
                continue;
            }
            switch (right) {
                case OWNER:
                case ADMINISTRATOR:
                    details.setAdmin(true);
                    break;
                case MODERATOR:
                    details.setModerator(true);
                    break;
                case DONATOR:
                    details.setDonator(true);
                    break;
                case SUPER_DONATOR:
                    details.setSuperDonator(true);
                    break;
                case EXTREME_DONATOR:
                    details.setExtremeDonator(true);
                    break;
                case LEGENDARY_DONATOR:
                    details.setLegendaryDonator(true);
                    break;
                case IRONMAN:
                    details.setIronman(true);
                    break;
                default:
                    details.setPrimaryGroup(Rights.PLAYER.getMemberGroupId());
                    break;
            }
        }
    }
}