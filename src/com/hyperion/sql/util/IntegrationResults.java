package com.hyperion.sql.util;

/**
 * SQL RESULT RETURNS
 * <p>
 * These are in ordinal dont change the order;
 */

public enum IntegrationResults {
    SUCCESSFUL, INVALID_CREDENTIALS, UNREGISTERED_MEMBER, CONNECTION_ERROR
}
