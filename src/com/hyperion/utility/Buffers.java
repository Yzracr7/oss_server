package com.hyperion.utility;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;

/**
 * Buffer utility class.
 * @author Graham Edgecombe
 *
 */
public class Buffers {

	/**
	 * Reads a null terminated string from a byte buffer.
	 * @param buffer The buffer.
	 * @return The string.
	 */
	public static String readString(ByteBuffer buffer) {
		StringBuilder bldr = new StringBuilder();
		while(buffer.hasRemaining()) {
			byte b = buffer.get();
			if(b == 0) {
				break;
			}
			bldr.append((char) b);
		}
		return bldr.toString();
	}
	
	public static void writeString(RandomAccessFile raf, String s) {
		try {
			byte[] stringBytes = s.getBytes();
			for (int i = 0; i < s.length(); i++) {
				raf.writeByte(stringBytes[i]);
			}
			raf.writeByte(0);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static int getMediumInt(ByteBuffer buffer) {
		return ((buffer.get() & 0xff) << 16) | ((buffer.get() & 0xff) << 8) |
				(buffer.get() & 0xff);
	}
	
	public static int getSmartInt(ByteBuffer buffer) {
		if ((buffer.get(buffer.position()) ^ 0xffffffff) <= -1) {
			int i = buffer.getShort() & 0xffff;
			if (i == 32767) {
				return -1;
			}
			
			return i;
		}
		return buffer.getInt() & 0x7fffffff;
	}

}
