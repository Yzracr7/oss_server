package com.hyperion.utility;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.regex.Pattern;

import com.hyperion.Logger;
import com.hyperion.game.world.entity.player.Player;

public class Censor {

    private static ArrayList<FilterWord> filter = new ArrayList<FilterWord>();

    public static void load() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("data/censor.txt"));
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.equals(""))
                    continue;
                String[] split = line.split(":");
                String word = split[0];
                String replacement = "";
                if (split.length == 2) {
                    replacement = split[2];
                } else if (split.length == 1) {
                    for (int i = 0; i < word.length(); i++) {
                        replacement += "*";
                    }
                }
                filter.add(new FilterWord(word, replacement, Pattern.compile("(.*)(?i)" + split[0] + "(.*)")));
            }
            Logger.getInstance().warning("Loaded " + filter.size() + " censor words.");
        } catch (Exception e) {
            System.out.println("Error loading censored words!");
        }
    }

    public static String filter(String s) {
        for (FilterWord word : filter) {
            if (word.matches(s)) {
                s = word.filter(s);
            }
        }
        return s;
    }

    public static String filterChat(String s, Player player) {
        String name = player != null ? player.getName() : "";
        for (FilterWord word : filter) {
            if (word.matches(s) && !(name != "" && name.contains(word.toString()))) {
                s = word.filter(s);
            }
        }
        return s;
    }

    public static class FilterWord {

        private String replacement;

        private String word;

        private Pattern pattern;

        public FilterWord(String string, String replacement, Pattern pattern) {
            this.word = string;
            this.replacement = replacement;
            this.pattern = pattern;
        }

        public String filter(String s) {
            return s.replaceAll("(?i)" + word, replacement);
        }

        public String getWord() {
            return word;
        }

        public String getReplacement() {
            return replacement;
        }

        public boolean matches(String s) {
            return pattern.matcher(s).matches();
        }
    }
}

/*package com.hyperion.utility;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class Censor {

	private static ArrayList<String> filter = new ArrayList<String>();

	static String[] aStringArray638 = { "fuck", "bastard", "lesbian", "prostitut", "spastic", "vagina", "retard", "arsehole", "asshole", "tosser", "homosex", "hetrosex", "hitler", "urinate" };
	static String[] aStringArray639 = { "shit", "lesbo", "phuck", "bitch", "penis", "whore", "bisex", "sperm", "rapist", "shag", "slag", "slut", "clit", "cunt", "piss", "nazi", "urine" };
	static String[] aStringArray640 = { "wank", "naked", "fag", "niga", "nige", "gay", "rape", "cock", "homo", "twat", "arse", "crap", "poo" };

	private static int anInt641;
	private static int[] anIntArray642 = new int[1000];
	private static int[] anIntArray643 = new int[1000];
	private static String[] aStringArray644 = new String[1000];
	private static int[] anIntArray645 = new int[1000];
	private static int[] anIntArray646 = new int[1000];

	public static void load() {
		try {
			BufferedReader reader = new BufferedReader(new FileReader("data/censor.txt"));
			String line;
			try {
				while ((line = reader.readLine()) != null) {
					if (line.equals(""))
						continue;
					String[] split = line.split(":");
					String word = split[0];
					filter.add(word);
				}
			} finally {
				reader.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String filter(String s) {
		s = method386(s, true);
		return s;
	}

	public static class FilterWord {

		private String replacement;

		private String word;

		private Pattern pattern;

		public FilterWord(String string, String replacement, Pattern pattern) {
			this.word = string;
			this.replacement = replacement;
			this.pattern = pattern;
		}

		public String filter(String s) {
			return s.replaceAll("(?i)" + word, replacement);
		}

		public String getWord() {
			return word;
		}

		public String getReplacement() {
			return replacement;
		}

		public boolean matches(String s) {
			return pattern.matcher(s).matches();
		}
	}

	private static boolean is_similar_char(char c1, char c2) {
		if (c1 == c2)
			return true;
		if (c2 == 'i' && (c1 == 'l' || c1 == '1' || c1 == '!' || c1 == '|' || c1 == ':' || c1 == '\u00a6' || c1 == ';'))
			return true;
		if (c2 == 's' && (c1 == '5' || c1 == '$'))
			return true;
		if (c2 == 'a' && (c1 == '4' || c1 == '@'))
			return true;
		if (c2 == 'c' && (c1 == '(' || c1 == '<' || c1 == '['))
			return true;
		if (c2 == 'o' && c1 == '0')
			return true;
		if (c2 == 'u' && c1 == 'v')
			return true;
		return false;
	}

	private static String method386(String formatted_string, boolean bool) {
		for (int tries = 0; tries < 2; tries++) {
			String orig_string = formatted_string;
			anInt641 = 0;
			int i_37_ = 0;
			for (int i_38_ = 0; i_38_ < formatted_string.length(); i_38_++) {
				char c = formatted_string.charAt(i_38_);
				if (c >= 'A' && c <= 'Z')
					c = (char) (c + 'a' - 'A');
				if (bool && c == '@' && i_38_ + 4 < formatted_string.length() && formatted_string.charAt(i_38_ + 4) == '@')
					i_38_ += 4;
				else {
					int i_39_;
					if (c >= 'a' && c <= 'z' || c >= '0' && c <= '9')
						i_39_ = 0;
					else if (c == '\'')
						i_39_ = 1;
					else if (c == '\r' || c == ' ' || c == '.' || c == ',' || c == '-' || c == '(' || c == ')' || c == '?' || c == '!')
						i_39_ = 2;
					else
						i_39_ = 3;
					int i_40_ = anInt641;
					for (int i_41_ = 0; i_41_ < i_40_; i_41_++) {
						if (i_39_ == 3) {
							if (anIntArray646[i_41_] > 0 && (anIntArray646[i_41_] < (anIntArray642[i_41_] + (aStringArray644[i_41_].length() / 2)))) {
								anIntArray646[anInt641] = anIntArray646[i_41_] + 1;
								anIntArray645[anInt641] = anIntArray645[i_41_];
								anIntArray643[anInt641] = anIntArray643[i_41_] + 1;
								anIntArray642[anInt641] = anIntArray642[i_41_];
								aStringArray644[anInt641++] = aStringArray644[i_41_];
								anIntArray646[i_41_] = -anIntArray646[i_41_];
							}
						} else {
							char c_42_ = aStringArray644[i_41_].charAt(anIntArray643[i_41_]);
							if (is_similar_char(c, c_42_)) {
								anIntArray643[i_41_]++;
								if (anIntArray646[i_41_] < 0)
									anIntArray646[i_41_] = -anIntArray646[i_41_];
							} else if ((c == ' ' || c == '\r') && anIntArray642[i_41_] == 0)
								anIntArray643[i_41_] = 99999;
							else {
								char c_43_ = aStringArray644[i_41_].charAt(anIntArray643[i_41_] - 1);
								if (i_39_ == 0 && !is_similar_char(c, c_43_))
									anIntArray643[i_41_] = 99999;
							}
						}
					}
					if (i_39_ >= 2)
						i_37_ = 1;
					if (i_39_ <= 2) {
						for (int i_44_ = 0; i_44_ < filter.size(); i_44_++) {
							String check = filter.get(i_44_);
							if (check != null) {
								if (is_similar_char(c, check.charAt(0))) {
									anIntArray646[anInt641] = 1;
									anIntArray645[anInt641] = i_38_;
									anIntArray643[anInt641] = 1;
									anIntArray642[anInt641] = 1;
									aStringArray644[anInt641++] = check;
								}
							}
						}
						
						 * for (int i_44_ = 0; i_44_ < aStringArray638.length;
						 * i_44_++) { if (is_similar_char(c,
						 * aStringArray638[i_44_].charAt(0))) {
						 * anIntArray646[anInt641] = 1; anIntArray645[anInt641]
						 * = i_38_; anIntArray643[anInt641] = 1;
						 * anIntArray642[anInt641] = 1;
						 * aStringArray644[anInt641++] = aStringArray638[i_44_];
						 * } }
						 
						
						 * for (int i_45_ = 0; i_45_ < aStringArray639.length;
						 * i_45_++) { if (is_similar_char(c,
						 * aStringArray639[i_45_].charAt(0))) {
						 * anIntArray646[anInt641] = 1; anIntArray645[anInt641]
						 * = i_38_; anIntArray643[anInt641] = 1;
						 * anIntArray642[anInt641] = i_37_;
						 * aStringArray644[anInt641++] = aStringArray639[i_45_];
						 * } }
						 
						if (i_37_ == 1) {
							
							 * for (int i_46_ = 0; i_46_ <
							 * aStringArray640.length; i_46_++) { if
							 * (is_similar_char(c,
							 * aStringArray640[i_46_].charAt(0))) {
							 * anIntArray646[anInt641] = 1;
							 * anIntArray645[anInt641] = i_38_;
							 * anIntArray643[anInt641] = 1;
							 * anIntArray642[anInt641] = 1;
							 * aStringArray644[anInt641++] =
							 * aStringArray640[i_46_]; } }
							 
						}
						if (i_39_ == 0)
							i_37_ = 0;
					}
					for (int i_47_ = 0; i_47_ < anInt641; i_47_++) {
						if (anIntArray643[i_47_] >= aStringArray644[i_47_].length()) {
							if (anIntArray643[i_47_] < 99999) {
								String string_48_ = "";
								for (int i_49_ = 0; i_49_ < formatted_string.length(); i_49_++) {
									if (i_49_ < anIntArray645[i_47_] || i_49_ > i_38_)
										string_48_ += formatted_string.charAt(i_49_);
									else
										string_48_ += "*";
								}
								formatted_string = string_48_;
							}
							anInt641--;
							for (int i_50_ = i_47_; i_50_ < anInt641; i_50_++) {
								anIntArray642[i_50_] = anIntArray642[i_50_ + 1];
								anIntArray643[i_50_] = anIntArray643[i_50_ + 1];
								aStringArray644[i_50_] = aStringArray644[i_50_ + 1];
								anIntArray645[i_50_] = anIntArray645[i_50_ + 1];
								anIntArray646[i_50_] = anIntArray646[i_50_ + 1];
							}
							i_47_--;
						}
					}
				}
			}
			if (formatted_string.equalsIgnoreCase(orig_string))
				break;
		}
		return formatted_string;
	}
}*/