package com.hyperion.utility;

import com.hyperion.cache.Cache;

public class Huffman {

	private static byte[] bitSizes;
	private static int[] chatMasks;
	private static int[] decryptKeys;

	public static void loadHuffman() {
		// byte[] is = CacheManager.getDefinitionsCacheManager().getArchive(10,
		// 0).getData();
		// byte[] is = UpdateServer.readFile(10, 0);
		byte[] is = Cache.getCacheFileManagers()[10].getFileData(Cache.getCacheFileManagers()[10].getContainerId("huffman"), 0);
		int i = is.length;
		bitSizes = is;
		int[] is_30_ = new int[33];
		chatMasks = new int[i];
		int i_31_ = 0;
		decryptKeys = new int[8];
		for (int i_32_ = 0; i > i_32_; i_32_++) {
			int i_33_ = is[i_32_];
			if (i_33_ != 0) {
				int i_34_ = 1 << -i_33_ + 32;
				int i_35_ = is_30_[i_33_];
				chatMasks[i_32_] = i_35_;
				int i_36_;
				if ((i_35_ & i_34_) != 0)
					i_36_ = is_30_[i_33_ - 1];
				else {
					i_36_ = i_34_ | i_35_;
					for (int i_37_ = i_33_ - 1; i_37_ >= 1; i_37_--) {
						int i_38_ = is_30_[i_37_];
						if (i_38_ != i_35_)
							break;
						int i_39_ = 1 << -i_37_ + 32;
						if ((i_38_ & i_39_) == 0)
							is_30_[i_37_] = i_39_ | i_38_;
						else {
							is_30_[i_37_] = is_30_[i_37_ - 1];
							break;
						}
					}
				}
				is_30_[i_33_] = i_36_;
				for (int i_40_ = i_33_ + 1; i_40_ <= 32; i_40_++) {
					if (i_35_ == is_30_[i_40_])
						is_30_[i_40_] = i_36_;
				}
				int i_41_ = 0;
				for (int i_42_ = 0; i_42_ < i_33_; i_42_++) {
					int i_43_ = -2147483648 >>> i_42_;
					if ((i_43_ & i_35_) != 0) {
						if (decryptKeys[i_41_] == 0)
							decryptKeys[i_41_] = i_31_;
						i_41_ = decryptKeys[i_41_];
					} else
						i_41_++;
					if (decryptKeys.length <= i_41_) {
						int[] is_44_ = new int[decryptKeys.length * 2];
						for (int i_45_ = 0; i_45_ < decryptKeys.length; i_45_++)
							is_44_[i_45_] = decryptKeys[i_45_];
						decryptKeys = is_44_;
					}
					i_43_ >>>= 1;
				}
				if (i_41_ >= i_31_)
					i_31_ = i_41_ + 1;
				decryptKeys[i_41_] = i_32_ ^ 0xffffffff;
			}
		}
		System.out.println("Loaded huffman.");
	}

	public static String textUnpack(byte packedData[], int off, int size) {
		if (size == 0)
			return "";
		int offset = 1;
		int length = packedData[0] & 0xff;
		try {
			int charsDecoded = 0;
			int keyIndex = 0;
			StringBuilder sb = new StringBuilder();
			for (;;) {
				byte textByte = packedData[offset++];
				if (textByte >= 0) {
					keyIndex++;
				} else {
					keyIndex = decryptKeys[keyIndex];
				}
				int charId;
				if ((charId = decryptKeys[keyIndex]) < 0) {
					sb.append((char) (byte) (charId ^ 0xffffffff));
					if (length <= ++charsDecoded) {
						break;
					}
					keyIndex = 0;
				}
				if ((textByte & 0x40) != 0) {
					keyIndex = decryptKeys[keyIndex];
				} else {
					keyIndex++;
				}
				if ((charId = decryptKeys[keyIndex]) < 0) {
					sb.append((char) (byte) (charId ^ 0xffffffff));
					if (++charsDecoded >= length) {
						break;
					}
					keyIndex = 0;
				}
				if ((0x20 & textByte) == 0) {
					keyIndex++;
				} else {
					keyIndex = decryptKeys[keyIndex];
				}
				if ((charId = decryptKeys[keyIndex]) < 0) {
					sb.append((char) (byte) (charId ^ 0xffffffff));
					if (length <= ++charsDecoded) {
						break;
					}
					keyIndex = 0;
				}
				if ((0x10 & textByte) == 0) {
					keyIndex++;
				} else {
					keyIndex = decryptKeys[keyIndex];
				}
				if ((charId = decryptKeys[keyIndex]) < 0) {
					sb.append((char) (byte) (charId ^ 0xffffffff));
					if (length <= ++charsDecoded) {
						break;
					}

					keyIndex = 0;
				}
				if ((0x8 & textByte) != 0) {
					keyIndex = decryptKeys[keyIndex];
				} else {
					keyIndex++;
				}
				if ((charId = decryptKeys[keyIndex]) < 0) {
					sb.append((char) (byte) (charId ^ 0xffffffff));
					if (++charsDecoded >= length) {
						break;
					}
					keyIndex = 0;
				}
				if ((0x4 & textByte) == 0) {
					keyIndex++;
				} else {
					keyIndex = decryptKeys[keyIndex];
				}
				if ((charId = decryptKeys[keyIndex]) < 0) {
					sb.append((char) (byte) (charId ^ 0xffffffff));
					if (length <= ++charsDecoded) {
						break;
					}
					keyIndex = 0;
				}
				if ((textByte & 0x2) != 0) {
					keyIndex = decryptKeys[keyIndex];
				} else {
					keyIndex++;
				}
				if ((charId = decryptKeys[keyIndex]) < 0) {
					sb.append((char) (byte) (charId ^ 0xffffffff));
					if (length <= ++charsDecoded) {
						break;
					}
					keyIndex = 0;
				}
				if ((textByte & 0x1) != 0) {
					keyIndex = decryptKeys[keyIndex];
				} else {
					keyIndex++;
				}
				if ((charId = decryptKeys[keyIndex]) < 0) {
					sb.append((char) (byte) (charId ^ 0xffffffff));
					if (++charsDecoded >= length) {
						break;
					}
					keyIndex = 0;
				}
			}
			return sb.toString();
		} catch (Exception e) {
		}
		return "Error";
	}

	/**
	 * Unpacks text.
	 * 
	 * @param packedData
	 *            The packet text.
	 * @param size
	 *            The length.
	 * @return The string.
	 */
	public static String textUnpack(byte packedData[], int size) {
		if (size == 0)
			return "";
		int offset = 1;
		int length = packedData[0] & 0xff;
		try {
			int charsDecoded = 0;
			int keyIndex = 0;
			StringBuilder sb = new StringBuilder();
			for (;;) {
				byte textByte = packedData[offset++];
				if (textByte >= 0) {
					keyIndex++;
				} else {
					keyIndex = decryptKeys[keyIndex];
				}
				int charId;
				if ((charId = decryptKeys[keyIndex]) < 0) {
					sb.append((char) (byte) (charId ^ 0xffffffff));
					if (length <= ++charsDecoded) {
						break;
					}
					keyIndex = 0;
				}
				if ((textByte & 0x40) != 0) {
					keyIndex = decryptKeys[keyIndex];
				} else {
					keyIndex++;
				}
				if ((charId = decryptKeys[keyIndex]) < 0) {
					sb.append((char) (byte) (charId ^ 0xffffffff));
					if (++charsDecoded >= length) {
						break;
					}
					keyIndex = 0;
				}
				if ((0x20 & textByte) == 0) {
					keyIndex++;
				} else {
					keyIndex = decryptKeys[keyIndex];
				}
				if ((charId = decryptKeys[keyIndex]) < 0) {
					sb.append((char) (byte) (charId ^ 0xffffffff));
					if (length <= ++charsDecoded) {
						break;
					}
					keyIndex = 0;
				}
				if ((0x10 & textByte) == 0) {
					keyIndex++;
				} else {
					keyIndex = decryptKeys[keyIndex];
				}
				if ((charId = decryptKeys[keyIndex]) < 0) {
					sb.append((char) (byte) (charId ^ 0xffffffff));
					if (length <= ++charsDecoded) {
						break;
					}

					keyIndex = 0;
				}
				if ((0x8 & textByte) != 0) {
					keyIndex = decryptKeys[keyIndex];
				} else {
					keyIndex++;
				}
				if ((charId = decryptKeys[keyIndex]) < 0) {
					sb.append((char) (byte) (charId ^ 0xffffffff));
					if (++charsDecoded >= length) {
						break;
					}
					keyIndex = 0;
				}
				if ((0x4 & textByte) == 0) {
					keyIndex++;
				} else {
					keyIndex = decryptKeys[keyIndex];
				}
				if ((charId = decryptKeys[keyIndex]) < 0) {
					sb.append((char) (byte) (charId ^ 0xffffffff));
					if (length <= ++charsDecoded) {
						break;
					}
					keyIndex = 0;
				}
				if ((textByte & 0x2) != 0) {
					keyIndex = decryptKeys[keyIndex];
				} else {
					keyIndex++;
				}
				if ((charId = decryptKeys[keyIndex]) < 0) {
					sb.append((char) (byte) (charId ^ 0xffffffff));
					if (length <= ++charsDecoded) {
						break;
					}
					keyIndex = 0;
				}
				if ((textByte & 0x1) != 0) {
					keyIndex = decryptKeys[keyIndex];
				} else {
					keyIndex++;
				}
				if ((charId = decryptKeys[keyIndex]) < 0) {
					sb.append((char) (byte) (charId ^ 0xffffffff));
					if (++charsDecoded >= length) {
						break;
					}
					keyIndex = 0;
				}
			}
			return sb.toString();
		} catch (Exception e) {
		}
		return "Error";
	}

	public static void textPack(byte packedData[], int bit, int offset, String text) {
		if (text.length() > 80) {
			text = text.substring(0, 80);
		}
		int length = text.length();
		int key = 0;
		int bitPosition = bit << 3;
		int srcOffset = offset;
		for (; length > srcOffset; srcOffset++) {
			int textByte = 0xff & text.getBytes()[srcOffset];
			int mask = chatMasks[textByte];
			int size = bitSizes[textByte];
			if (size == 0)
				return;
			int destOffset = bitPosition >> 3;
			int bitOffset = bitPosition & 0x7;
			key &= -bitOffset >> 31;
			bitPosition += size;
			int byteSize = (((bitOffset + size) - 1) >> 3) + destOffset;
			bitOffset += 24;
			packedData[destOffset] = (byte) (key = (key | (mask >>> bitOffset)));
			if (byteSize < destOffset) {
				destOffset++;
				bitOffset -= 8;
				packedData[destOffset] = (byte) (key = mask >>> bitOffset);
				if (byteSize > destOffset) {
					destOffset++;
					bitOffset -= 8;
					packedData[destOffset] = (byte) (key = mask >>> bitOffset);
					if (byteSize > destOffset) {
						bitOffset -= 8;
						destOffset++;
						packedData[destOffset] = (byte) (key = mask >>> bitOffset);
						if (destOffset < byteSize) {
							bitOffset -= 8;
							destOffset++;
							packedData[destOffset] = (byte) (key = mask << -bitOffset);
						}
					}
				}
			}
		}
	}

	public static void textPack(byte packedData[], String text) {
		if (text.length() > 80) {
			text = text.substring(0, 80);
		}
		int length = text.length();
		int key = 0;
		int bitPosition = 1 << 3;
		int srcOffset = 0;
		packedData[0] = (byte) length;
		for (; length > srcOffset; srcOffset++) {
			int textByte = 0xff & text.getBytes()[srcOffset];
			int mask = chatMasks[textByte];
			int size = bitSizes[textByte];
			int destOffset = bitPosition >> 3;
			int bitOffset = bitPosition & 0x7;
			key &= (-bitOffset >> 31);
			bitPosition += size;
			int byteSize = (((bitOffset + size) - 1) >> 3) + destOffset;
			bitOffset += 24;
			packedData[destOffset] = (byte) (key = (key | (mask >>> bitOffset)));
			if (byteSize > destOffset) {
				destOffset++;
				bitOffset -= 8;
				packedData[destOffset] = (byte) (key = mask >>> bitOffset);
				if (byteSize > destOffset) {
					destOffset++;
					bitOffset -= 8;
					packedData[destOffset] = (byte) (key = mask >>> bitOffset);
					if (byteSize > destOffset) {
						bitOffset -= 8;
						destOffset++;
						packedData[destOffset] = (byte) (key = mask >>> bitOffset);
						if (destOffset < byteSize) {
							bitOffset -= 8;
							destOffset++;
							packedData[destOffset] = (byte) (key = mask << -bitOffset);
						}
					}
				}
			}
		}
	}

}
