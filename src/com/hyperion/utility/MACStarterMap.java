package com.hyperion.utility;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import com.hyperion.game.Constants;

public class MACStarterMap {
	private static MACStarterMap INSTANCE = new MACStarterMap();

	public static MACStarterMap getSingleton() {
		return INSTANCE;
	}

	private static List<String> macstarters = new ArrayList<String>();
	private static final String path = Constants.FILES_PATH + "MACstarters.ini";

	private static File macmap = new File(path);

	public static void init() {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(macmap));
			String s;
			while ((s = reader.readLine()) != null) {
				macstarters.add(s);
			}
			reader.close();
			//System.out.println("Loaded MAC Starter map, There are " + macstarters.size() + " MAC Addresses in Configuration");
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	private void save() {
		BufferedWriter bf;
		try {
			clearMapFile();
			bf = new BufferedWriter(new FileWriter(path, true));
			for (String mac : macstarters) {
				bf.write(mac);
				bf.newLine();
			}
			bf.flush();
			bf.close();
		} catch (IOException e) {
			System.err.println("Error saving MAC starter map!");
		}
	}

	private void clearMapFile() {
		PrintWriter writer;
		try {
			writer = new PrintWriter(macmap);
			writer.print("");
			writer.close();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void addMAC(String MAC) {
		if (getCount(MAC) >= Constants.MAX_STARTER_PER_MAC)
			return;
		macstarters.add(MAC);
		save();
	}

	public int getCount(String MAC) {
		if(MAC == "")
			return 0;
		int count = 0;
		for (String i : macstarters) {
			if (i.equals(MAC))
				count++;
		}
		return count;
	}
}