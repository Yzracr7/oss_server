package com.hyperion.utility;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.hyperion.cache.Cache;
import com.hyperion.game.world.region.Mapdata;


public class MapdataDumper {
	
	public static Mapdata mapdata = new Mapdata();

	public static void main(String[] args) throws IOException {
		Cache.init();
		for (int id = 0; id < 40000; id++) {
			try {
				dumpMapData(id);
			} catch (OutOfMemoryError e) {
				continue;
			}
		}
	}

	public static void dumpMapData(int id) {
		int[] xtea_keys = mapdata.getData(id);
		int absX = (id >> 8) * 64;
		int absY = (id & 0xff) * 64;
		System.out.println("doing: "+id);
		byte[] objectmapbytes = Cache.getCacheFileManagers()[5].getFileData(Cache.getCacheFileManagers()[5].getContainerId("l"+ ((absX >> 3) / 8) +"_"+ ((absY >> 3) / 8)), 0, xtea_keys);
		if (objectmapbytes == null)
			return;
		try {
			File file = new File("./data/valid/" + id + ".txt");
			if (file.exists())
				return;
			BufferedWriter bf = new BufferedWriter(new FileWriter(file));
			bf.write(String.valueOf(xtea_keys[0]));
			bf.newLine();
			bf.flush();
			bf.write(String.valueOf(xtea_keys[1]));
			bf.newLine();
			bf.flush();
			bf.write(String.valueOf(xtea_keys[2]));
			bf.newLine();
			bf.flush();
			bf.write(String.valueOf(xtea_keys[3]));
			bf.newLine();
			bf.flush();
			bf.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}