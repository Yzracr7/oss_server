package com.hyperion.utility;

import java.text.NumberFormat;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class NumberUtils {

    public static final Random random = new Random();

    /**
     * Gets a random index for a int[]
     *
     * @param ids
     * @return
     */
    public static int getRandomIndex(int ids[]) {
        return ids[random(ids.length)];
    }

    /**
     * Gets a gaussian distributed randomized value between 0 and the {@code maximum} value.
     * <br>The mean (average) is maximum / 2.
     *
     * @param meanModifier The modifier used to determine the mean.
     * @param r            The random instance.
     * @param maximum      The maximum value.
     * @return The randomized value.
     */
    public static double getGaussian(double meanModifier, double maximum) {
        Random random = new Random();
        double mean = maximum * meanModifier;
        double deviation = mean * 1.79;
        double value = 0;
        do {
            value = Math.floor(mean + random.nextGaussian() * deviation);
        } while (value < 0 || value > maximum);
        return value;
    }

    /**
     * Format Time Millis
     *
     * @param l
     * @return
     */
    public static String formatMillis(final long l) {
        final long hr = TimeUnit.MILLISECONDS.toHours(l);
        final long min = TimeUnit.MILLISECONDS.toMinutes(l - TimeUnit.HOURS.toMillis(hr));
        final long sec = TimeUnit.MILLISECONDS.toSeconds(l - TimeUnit.HOURS.toMillis(hr) - TimeUnit.MINUTES.toMillis(min));
        final long ms = TimeUnit.MILLISECONDS.toMillis(l - TimeUnit.HOURS.toMillis(hr) - TimeUnit.MINUTES.toMillis(min) - TimeUnit.SECONDS.toMillis(sec));
        return String.format("%02d:%02d:%02d.%03d", hr, min, sec, ms);
    }

    /**
     * Returns a random integer with min as the inclusive lower data and max as
     * the exclusive upper data.
     *
     * @param min The inclusive lower data.
     * @param max The exclusive upper data.
     * @return Random integer min <= n < max.
     */
    public static int random(int min, int max) {
        int n = Math.abs(max - min);
        return Math.min(min, max) + (n == 0 ? 0 : random.nextInt(n));
    }

    public static Double random(Double min, Double max) {
        double random = new Random().nextDouble();
        double result = min + (random * (max - min));
        return result;
    }

    public static final double getRandomDouble(double maxValue) {
        return (Math.random() * (maxValue + 1));
    }

    public static int random(int range) {
        return (int) (Math.random() * (range + 1));
    }

    public static int getDaysFromMillis(long time) {
        int seconds = (int) ((System.currentTimeMillis() - time) / 1000);
        int minutes = seconds / 60;
        int hours = minutes / 60;
        return (hours / 24);
    }

    public static int getHoursFromMillis(long time) {
        int seconds = (int) ((System.currentTimeMillis() - time) / 1000);
        int minutes = seconds / 60;
        return (minutes / 60);
    }

    public int getHours(long ms) {
        return (int) ((ms / (1000 * 60 * 60)) % 24);
    }

    public int getMinutes(long ms) {
        return (int) ((ms / (1000 * 60)) % 60);
    }

    public int getSeconds(long ms) {
        return (int) ((ms / 1000) % 60);
    }

    public static String getMinutesDisplay(int timer) {
        String display = "";
        int minutes = (timer / 60);
        int seconds = (timer - minutes * 60);
        if (minutes > 0) {
            display += minutes + " minutes ";
        } else {
            display += seconds + "s";
        }
        return display;
    }

    public static String format(int price) {
        return price_with_commas(price);
    }

    public static String price_with_commas(int price) {
        return NumberFormat.getInstance().format(price);
    }
}
