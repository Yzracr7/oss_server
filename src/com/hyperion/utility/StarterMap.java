package com.hyperion.utility;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import com.hyperion.Logger;
import com.hyperion.game.Constants;

public class StarterMap {
    private static StarterMap INSTANCE = new StarterMap();

    public static StarterMap getSingleton() {
        return INSTANCE;
    }

    private static List<String> starters = new ArrayList<String>();
    private static final String path = Constants.FILES_PATH + "starters.ini";

    private static File map = new File(path);

    public static void init() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(map));
            String s;
            while ((s = reader.readLine()) != null) {
                starters.add(s);
            }
            reader.close();
            //Logger.getInstance().warning("Loaded Starter map, There are " + starters.size() + " IP's in Configuration");
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void save() {
        BufferedWriter bf;
        try {
            clearMapFile();
            bf = new BufferedWriter(new FileWriter(path, true));
            for (String ip : starters) {
                bf.write(ip);
                bf.newLine();
            }
            bf.flush();
            bf.close();
        } catch (IOException e) {
            System.err.println("Error saving starter map!");
        }
    }

    private void clearMapFile() {
        PrintWriter writer;
        try {
            writer = new PrintWriter(map);
            writer.print("");
            writer.close();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void addIP(String ip) {
        String[] array = ip.split(":");
        String ipNoPort = array[0].substring(1); // substring 1 to remove
        // backslash
        if (getCount(ipNoPort) >= Constants.MAX_STARTER_PER_IP)
            return;
        starters.add(ipNoPort);
        save();
    }

    public int getCount(String ip) {
        if (ip == "")
            return 0;
        int count = 0;
        String[] array = ip.split(":");
        String ipNoPort = array[0].substring(1); // substring 1 to remove
        // backslash
        for (String i : starters) {
            if (i.equals(ipNoPort))
                count++;
        }
        return count;
    }
}