package com.hyperion.utility;

public class StringUtil {
	
	public static final String[] charData = {
		";,lijI|.",
		"-\\/{} ",
		"\"[]?abcdefghkmnopqrstuvwxyz~+$%*",
		"ABCDEFGHJKLMNOPQRSTUVWXYZ<>=_@#^"
	};
	public static int getCharLength(Character character) {
		int pointInCharData = 0;
		for(int i = 0; i < charData.length; i++) {
			if(charData[i].contains(character.toString())){
				pointInCharData = i + 1;
			}
		}
		return pointInCharData;
	}
	public static int getStringLength(String s) {
		int length = 0;
		for(char c : s.toCharArray()) {
			length += getCharLength(c);
		}
		return length;
	}
	public static String getModifiedString(String modifiedString) {
		while(modifiedString.contains("@")) {
			try {
				modifiedString = 
					modifiedString.replace(modifiedString.
						substring(modifiedString.indexOf('@'), 
							modifiedString.indexOf('@')+5), "");
			} catch(StringIndexOutOfBoundsException e) {
				System.err.println("You should refrain from using @s in your messages");
				e.printStackTrace();
			}
		}
		return modifiedString;
	}
	public static int getIndexOfSplit(String s, int split) {
		int i = 0;
		String modifiedString = s;
		while(modifiedString.contains("@")) {
			try {
				modifiedString = modifiedString.replace(modifiedString.substring(modifiedString.indexOf('@'), modifiedString.indexOf('@')+5), "");
				i += 5;
			} catch(StringIndexOutOfBoundsException e) {
				System.err.println("You should refrain from using @s in your messages");
			}
		}
		char[] c = s.toCharArray();
		for(; i < c.length; i++) {
			if(split > 0)
				split -= getCharLength(c[i]);
			else if(!(c[i] == ' '))
				continue;
			else
				return i;
		}
		return i;
	}
}