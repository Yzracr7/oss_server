package com.hyperion.utility;

import java.util.List;
import java.util.Random;

import com.hyperion.game.content.achievements.Achievements;
import com.hyperion.game.item.Item;
import com.hyperion.game.world.entity.player.Player;

/**
 * Text utility class.
 * 
 * @author Graham Edgecombe
 *
 */
public class TextUtils {

	
	public static final String vowels[] = { "a", "e", "i", "o", "u" };
	
	/**
	 * An array of valid characters in a long username.
	 */
	private static final char VALID_CHARS[] = { '_', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '+', '=', ':', ';', '.', '>', '<', ',', '"', '[', ']', '|', '?', '/', '`' };

	public final static String saltChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUV0123456789~`!@#$%^&*()_+=-[]{}:;/.,<>?";
	
	/**
	 * Packed text translate table.
	 */
	//public static final char XLATE_TABLE[] = { ' ', 'e', 't', 'a', 'o', 'i', 'h', 'n', 's', 'r', 'd', 'l', 'u', 'm', 'w', 'c', 'y', 'f', 'g', 'p', 'b', 'v', 'k', 'x', 'j', 'q', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' ', '!', '?', '.', ',', ':', ';', '(', ')', '-', '&', '*', '\\', '\'', '@', '#', '+', '=', '\243', '$', '%', '"', '[', ']' };

	public static String implode(String glue, String[] array) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < array.length; i++) {
			builder.append(array[i]);
			if (i < array.length - 1) {
				builder.append(glue);
			}
		}
		return builder.toString();
	}
	
	public static boolean isAlphaNumeric(String s){
	    String pattern= "^[a-zA-Z0-9_]*$";
	        if(s.matches(pattern)){
	            return true;
	        }
	        return false;   
	}
	
	public static String shuffle(Random random, String inputString)
	{
	    // Convert your string into a simple char array:
	    char a[] = inputString.toCharArray();

	    // Scramble the letters using the standard Fisher-Yates shuffle, 
	    for( int i=0 ; i<a.length-1 ; i++ )
	    {
	        int j = random.nextInt(a.length-1);
	        // Swap letters
	        char temp = a[i]; a[i] = a[j];  a[j] = temp;
	    }       

	    return new String( a );
	}

	public static String implode(String glue, int[] array) {
		StringBuilder builder = new StringBuilder();
		if (array == null) {
			return "";
		}
		for (int i = 0; i < array.length; i++) {
			builder.append(array[i]);
			if (i < array.length - 1) {
				builder.append(glue);
			}
		}
		return builder.toString();
	}

	public static String implode(List<?> array, String glue) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < array.size(); i++) {
			builder.append(array.get(i));
			if (i < array.size() - 1) {
				builder.append(glue);
			}
		}
		return builder.toString();
	}

	public static String getItemLogDisplay(Player player, Item[] items) {
		String list = "";
		int count = 0;
		for (int i = 0; i < 28; i++) {
			if (items[i] != null && items[i].getId() != -1) {
				count++;
				list = list + "" + items[i].getDefinition().getName();
				if (items[i].getCount() > 1) {
					list = list + " x " + items[i].getCount() + ", ";
					if(items[i].getId() == 995 && items[i].getCount() >= 50_000_000) {
						Achievements.increase(player, 52);
					}
				} else {
					list = list + ", ";
				}
			}
		}
		if (list.equals("")) {
			list = "Nothing";
		}
		return count > 0 ? list.substring(0, list.length()-2) : list;
	}

	public static String getTradeItemLogDisplay(Player player) {
		Item[] items = player.getTradingSession().getTradeContainer().getItems();
		String list = "";
		int count = 0;
		for (int i = 0; i < 28; i++) {
			if (items[i] != null && items[i].getId() != -1) {
				count++;
				list = list + "" + items[i].getDefinition().getName();
				if (items[i].getCount() > 1) {
					list = list + " x " + items[i].getCount() + ", ";
				} else {
					list = list + ", ";
				}
			}
		}
		if (list.equals("")) {
			list = "Nothing";
			return list;
		}
		return count > 0 ? list.substring(0, list.length() - 2) : list;
	}

	/**
	 * Checks if a name is valid.
	 * 
	 * @param name
	 *            The name.
	 * @return <code>true</code> if so, <code>false</code> if not.
	 */
	public static boolean isValidName(String name) {
		return formatNameForProtocol(name).matches("[a-z0-9_]+");
	}

	/**
	 * Converts a name to a long.
	 * 
	 * @param s
	 *            The name.
	 * @return The long.
	 */
	public static long stringToLong(String s) {
		long l = 0L;
		for (int i = 0; i < s.length() && i < 12; i++) {
			char c = s.charAt(i);
			l *= 37L;
			if (c >= 'A' && c <= 'Z')
				l += (1 + c) - 65;
			else if (c >= 'a' && c <= 'z')
				l += (1 + c) - 97;
			else if (c >= '0' && c <= '9')
				l += (27 + c) - 48;
		}
		while (l % 37L == 0L && l != 0L)
			l /= 37L;
		return l;
	}

	/**
	 * Converts a long to a name.
	 * 
	 * @param l
	 *            The long.
	 * @return The name.
	 */
	public static String longToName(long l) {
		int i = 0;
		char ac[] = new char[12];
		while (l != 0L) {
			long l1 = l;
			l /= 37L;
			ac[11 - i++] = VALID_CHARS[(int) (l1 - l * 37L)];
		}
		return new String(ac, 12 - i, i);
	}

	/**
	 * Formats a name for use in the protocol.
	 * 
	 * @param s
	 *            The name.
	 * @return The formatted name.
	 */
	public static String formatNameForProtocol(String s) {
		if(s.length() <= 0)
			return s;
		return s.toLowerCase().replace(" ", "_");
	}

	/**
	 * Formats a name for display.
	 * 
	 * @param s
	 *            The name.
	 * @return The formatted name.
	 */
	public static String formatName(String s) {
		// return /*fixName(*/(s.trim()).replace(" ", "_");
		if(s.length() <= 0)
			return s;
		if (s.toLowerCase() == s) {
			s = Character.toUpperCase(s.charAt(0)) + s.substring(1);
		}
		return s;
	}

	/**
	 * Method that fixes capitalization in a name.
	 * 
	 * @param s
	 *            The name.
	 * @return The formatted name.
	 */
	public static String fixName(final String s) {
		if (s.length() > 0) {
			final char ac[] = s.toCharArray();
			for (int j = 0; j < ac.length; j++)
				if (ac[j] == '_') {
					ac[j] = ' ';
					if ((j + 1 < ac.length) && (ac[j + 1] >= 'a') && (ac[j + 1] <= 'z')) {
						ac[j + 1] = (char) ((ac[j + 1] + 65) - 97);
					}
				}

			if ((ac[0] >= 'a') && (ac[0] <= 'z')) {
				ac[0] = (char) ((ac[0] + 65) - 97);
			}
			return new String(ac);
		} else {
			return s;
		}
	}
	
    /**
     * Capitalizes the first character of {@code str}. Any leading or trailing
     * whitespace in the string should be trimmed before using this method.
     *
     * @param str
     *            the string to capitalize.
     * @return the capitalized string.
     */
    public static String capitalize(String str) {
        return str.substring(0, 1).toUpperCase().concat(str.substring(1, str.length()));
    }
}
