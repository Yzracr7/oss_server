package com.hyperion.utility;

import java.text.*;
import java.util.*;
import java.util.TimeZone;

public class TimeSettings {

	public static String getDate() {
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("America/New_York"));
		SimpleDateFormat df = new SimpleDateFormat("MM.dd.yyyy h:mm:ss a '-' zzzz");
		df.setTimeZone(cal.getTimeZone());
		return df.format(cal.getTime());
	}
}
