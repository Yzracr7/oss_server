package com.hyperion.utility;

import com.hyperion.game.world.entity.player.Player;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Utils {

    public static final byte[] DIRECTION_DELTA_X = new byte[]{-1, 0, 1, -1, 1, -1, 0, 1};
    public static final byte[] DIRECTION_DELTA_Y = new byte[]{1, 1, 1, 0, 0, -1, -1, -1};

    public static final int getFaceDirection(int xOffset, int yOffset) {
        return ((int) (Math.atan2(-xOffset, -yOffset) * 2607.5945876176133)) & 0x3fff;
    }

    public static int direction(int dx, int dy) {
        if (dx < 0) {
            if (dy < 0) {
                return 5;
            } else if (dy > 0) {
                return 0;
            } else {
                return 3;
            }
        } else if (dx > 0) {
            if (dy < 0) {
                return 7;
            } else if (dy > 0) {
                return 2;
            } else {
                return 4;
            }
        } else {
            if (dy < 0) {
                return 2;
            } else if (dy > 0) {
                return 0;
            } else {
                return -1;
            }
        }
    }

    public static Player getOfflineAccount(String name) {
        return null;
    }

    /**
     * Gets all of the classes in a directory
     *
     * @param directory The directory to iterate through
     * @return The list of classes
     */
    public static List<Object> getClassesInDirectory(String directory) {
        List<Object> classes = new ArrayList<Object>();
        for (File file : new File("./bin/" + directory.replace(".", "/")).listFiles()) {
            if (file.getName().contains("$")) {
                continue;
            }
            try {
                Object objectEvent = (Class.forName(directory + "." + file.getName().replace(".class", "")).newInstance());
                classes.add(objectEvent);
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return classes;
    }

    /**
     * Gets all of the sub directories of a folder
     *
     * @return
     */
    public static List<String> getSubDirectories(Class<?> clazz) {
        String directory = "./bin/" + clazz.getPackage().getName().replace(".", "/");
        File file = new File(directory);
        String[] directories = file.list(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });
        List<String> subdirectories = Arrays.asList(directories);
        return subdirectories;
    }

    public static final int getMoveDirection(int xOffset, int yOffset) {
        if (xOffset < 0) {
            if (yOffset < 0)
                return 5;
            else if (yOffset > 0)
                return 0;
            else
                return 3;
        } else if (xOffset > 0) {
            if (yOffset < 0)
                return 7;
            else if (yOffset > 0)
                return 2;
            else
                return 4;
        } else {
            if (yOffset < 0)
                return 6;
            else if (yOffset > 0)
                return 1;
            else
                return -1;
        }
    }

    /**
     * @param cmd The command array
     * @return
     */
    public static String getCommandText(String[] cmd) {
        String text = "";
        for (int i = 0; i < cmd.length; i++) {
            if (i == 0) {
                continue;
            }
            text += cmd[i] + " ";
        }
        return text.trim();
    }

    /**
     * Gets the completed string from the given index
     *
     * @param cmd   The command array
     * @param index The index to begin
     * @return
     */
    public static String getCompleted(String[] cmd, int index) {
        StringBuilder sb = new StringBuilder();
        for (int i = index; i < cmd.length; i++) {
            if (i == cmd.length - 1 || cmd[i + 1].startsWith("+")) {
                return sb.append(cmd[i]).toString();
            }
            sb.append(cmd[i]).append(" ");
        }
        return "null";
    }

    /**
     * Finds out if a certain event should happen, and if it should, return
     * true;
     *
     * @param chance The chance of the event happening
     * @return If the event should happen
     */
    public static final boolean percentageChance(int chance) {
        return (Math.random() * 100) < chance;
    }

    public static final int random(int min, int max) {
        final int n = Math.abs(max - min);
        return Math.min(min, max) + (n == 0 ? 0 : random(n));
    }

    public static final int random(int maxValue) {
        if (maxValue == 0)
            return 0;
        return new Random().nextInt(maxValue);
    }

}