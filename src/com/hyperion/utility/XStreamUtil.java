package com.hyperion.utility;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import com.hyperion.game.content.skills.fishing.FishSpot;
import com.hyperion.game.content.skills.fishing.FishSpot.Option;
import com.hyperion.game.content.skills.fishing.FishSpot.Requirement;
import com.hyperion.game.item.Item;
import com.hyperion.game.item.ItemDefinition;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.npc.NPC;
import com.hyperion.game.world.entity.npc.NPCDefinition;
import com.hyperion.game.world.entity.player.Skills.Skill;
import com.hyperion.game.world.gameobjects.doors.Door;
import com.hyperion.game.world.pathfinders.Directions;
import com.hyperion.game.world.shopping.Shop;
import com.thoughtworks.xstream.XStream;

public class XStreamUtil {

	private XStreamUtil() {
	}

	public static XStream getXStream() {
		return xstream;
	}

	private static final XStream xstream = new XStream();
	
	static {
		/*
		 * Set up our aliases.
		 */
		xstream.alias("itemDefinition", ItemDefinition.class);
		xstream.alias("npcDefinition", NPCDefinition.class);
		xstream.alias("npc", NPC.class);
		xstream.alias("item", Item.class);
		xstream.alias("shop", Shop.class);
		xstream.alias("fish", FishSpot.Fish.class);
		xstream.alias("spot", FishSpot.class);
		xstream.alias("option", Option.class);
		xstream.alias("requirement", Requirement.class);
		xstream.alias("skillRequirement", Skill.class);
		xstream.alias("door", Door.class);
		xstream.alias("location", Location.class);
		xstream.alias("direction", Directions.NormalDirection.class);
	}
	
	public static void writeXML(Object object, File file) throws IOException {
		FileOutputStream out = new FileOutputStream(file);
		try {
			xstream.toXML(object, out);
			out.flush();
		} finally {
			out.close();
		}
	}

	@SuppressWarnings("unchecked")
	public static <T> T readXML(File file) throws IOException {
		FileInputStream in = new FileInputStream(file);
		try {
			return (T) xstream.fromXML(in);
		} finally {
			in.close();
		}
	}
}
