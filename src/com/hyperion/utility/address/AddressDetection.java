package com.hyperion.utility.address;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;

/**
 * Address detection
 *
 * @author trees
 */
public class AddressDetection {

    public static void main(String arg[]) {
        try {
            InetAddress address = InetAddress.getLocalHost();
            System.out.println("Found mac address " + getMacAddress(address));
        } catch (Exception e) {
            System.out.println("[Address Exception] : " + e);
        }
    }

    private static String getMacAddress(InetAddress ip) {
        String address = null;
        try {

            NetworkInterface network = NetworkInterface.getByInetAddress(ip);
            byte[] mac = network.getHardwareAddress();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < mac.length; i++) {
                sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
            }
            address = sb.toString();
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return address;
    }

    private static boolean isVMMac(byte[] mac) {
        if (null == mac) return false;
        byte invalidMacs[][] = {
                {0x00, 0x05, 0x69},
                {0x00, 0x1C, 0x14},
                {0x00, 0x0C, 0x29},
                {0x00, 0x50, 0x56},
                {0x08, 0x00, 0x27},
                {0x0A, 0x00, 0x27},
                {0x00, 0x03, (byte) 0xFF},
                {0x00, 0x15, 0x5D}
        };
        for (byte[] invalid : invalidMacs) {
            if (invalid[0] == mac[0] && invalid[1] == mac[1] && invalid[2] == mac[2]) return true;
        }
        return false;
    }
}