package com.hyperion.utility.game;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 27, 2015
 */
public enum ClickOption {

	FIRST, SECOND, THIRD

}
