package com.hyperion.utility.game;


/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public abstract class InputAction<T> {
	
	public InputAction(String text) {
		this.text = text;
	}

	/**
	 * Handles the input being received by the server.
	 * 
	 * @param input
	 *            The input data received, type T
	 */
	public abstract void handleInput(T input);

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * The text displayed on the input menu
	 */
	private final String text;

}
