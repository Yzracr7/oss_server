package com.hyperion.utility.game;

import com.hyperion.game.Constants;
import com.hyperion.game.world.Location;
import com.hyperion.game.world.entity.Entity;
import com.hyperion.game.world.pathfinders.DumbPathFinder;
import com.hyperion.game.world.pathfinders.PrimitivePathFinder;
import com.hyperion.game.world.pathfinders.Directions.NormalDirection;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @author trees
 * @since Mar 24, 2015
 */
public class Tile {


    /*
     * Gets the surrounding tile of another entity while factoring the current entity's size
     */
    public static Location getSurroundingTile(Entity entity, Entity other) {
        int dir = -1;
        if (!DumbPathFinder.blockedNorth(other.getLocation(), entity)) {
            dir = 0;
        } else if (!DumbPathFinder.blockedSouth(other.getLocation(), entity)) {
            dir = 4;
        } else if (!DumbPathFinder.blockedWest(other.getLocation(), entity)) {
            dir = 12;
        } else if (!DumbPathFinder.blockedEast(other.getLocation(), entity)) {
            dir = 8;
        }
        if (Constants.DEBUG_MODE)
            System.out.println("Surrounding tile dir: " + dir);
        boolean foundPath = false;
        int targX = other.getLocation().getX();
        int targY = other.getLocation().getY();
        if (!foundPath) {
            if (dir == 0) {
                targY += entity.getSize();
            } else if (dir == 4) {
                targY -= entity.getSize();
            } else if (dir == 8) {
                targX += entity.getSize();
            } else if (dir == 12) {
                targX -= entity.getSize();
            }
        }
        return Location.create(targX, targY, other.getLocation().getZ());
    }

    public static NormalDirection hasLocationToMove(Entity entity, Entity other) {

        boolean onlyEven = true;
        NormalDirection dir;
        for (int i = 0; i < 8; i++) {
            dir = NormalDirection.forIntValue(i);

            //Blocked SE/SW/NE/NW etc
            if (onlyEven && dir.npcIntValue() % 2 != 0 && dir.npcIntValue() != 0)
                continue;

            if (PrimitivePathFinder.canMove(entity, other.getLocation(),
                    dir, entity.getSize(), false)) {
                if (Constants.DEBUG_MODE)
                    System.out.println("has loc to move: " + dir);
                return dir;
            }
        }

        return null;
    }

    /*
     * Gets the surrounding tile of an entity
     */
    public static Location getSurroundingTile(Entity entity) {
        int dir = -1;
        if (!DumbPathFinder.blockedNorth(entity.getLocation(), entity)) {
            //	if(entity.getFaceLocation() != Location.create(entity.getLocation().getX(), entity.getLocation().getY() + 1, entity.getLocation().getZ()))
            dir = 0;
        } else if (!DumbPathFinder.blockedSouth(entity.getLocation(), entity)) {
            //if(entity.getFaceLocation() != Location.create(entity.getLocation().getX(), entity.getLocation().getY() - 1, entity.getLocation().getZ()))
            dir = 4;
        } else if (!DumbPathFinder.blockedWest(entity.getLocation(), entity)) {
            //if(entity.getFaceLocation() != Location.create(entity.getLocation().getX() - 1, entity.getLocation().getY(), entity.getLocation().getZ()))
            dir = 12;
        } else if (!DumbPathFinder.blockedEast(entity.getLocation(), entity)) {
            //if(entity.getFaceLocation() != Location.create(entity.getLocation().getX() + 1, entity.getLocation().getY(), entity.getLocation().getZ()))
            dir = 8;
        }
        boolean foundPath = false;
        int targX = entity.getLocation().getX();
        int targY = entity.getLocation().getY();
        if (!foundPath) {
            if (dir == 0) {
                targY += 1;
            } else if (dir == 4) {
                targY -= 1;
            } else if (dir == 8) {
                targX += 1;
            } else if (dir == 12) {
                targX -= 1;
            }
        }
        return Location.create(targX, targY, entity.getLocation().getZ());
    }

    /*
     * Gets a free tile within the surrounding locations based on the entity's size
     */
    public static Location getFreeTile(Entity entity, Location loc) {
        int dir = -1;
        if (!DumbPathFinder.blockedNorth(loc, entity)) {
            //if(entity.getFaceLocation() != Location.create(entity.getLocation().getX(), entity.getLocation().getY() + 1, entity.getLocation().getZ()))
            dir = 0;
        } else if (!DumbPathFinder.blockedSouth(loc, entity)) {
            //if(entity.getFaceLocation() != Location.create(loc.getX(), loc.getY() - 1, loc.getZ()))
            dir = 4;
        } else if (!DumbPathFinder.blockedWest(loc, entity)) {
            //if(entity.getFaceLocation() != Location.create(loc.getX() - 1, loc.getY(), loc.getZ()))
            dir = 12;
        } else if (!DumbPathFinder.blockedEast(loc, entity)) {
            //if(entity.getFaceLocation() != Location.create(loc.getX() + 1, loc.getY(), loc.getZ()))
            dir = 8;
        }
        boolean foundPath = false;
        int targX = loc.getX();
        int targY = loc.getY();
        if (!foundPath) {
            if (dir == 0) {
                targY += 1;
            } else if (dir == 4) {
                targY -= 1;
            } else if (dir == 8) {
                targX += 1;
            } else if (dir == 12) {
                targX -= 1;
            }
        }
        return Location.create(targX, targY, loc.getZ());
    }


}
