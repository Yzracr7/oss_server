package com.hyperion.utility.json;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.hyperion.Logger;
import com.hyperion.utility.Utils;

/**
 * This class manages all json subimplementations
 * 
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class JsonManager {

	/**
	 * This method will initialize all json loaders and add them to the
	 * {@link #loaders} list
	 */
	public void initialize() {
		for (Object clazz : Utils.getClassesInDirectory(JsonManager.class.getPackage().getName() + ".impl")) {
			JsonLoader<?> loader = (JsonLoader<?>) clazz;
			loader.initialize();
			loaders.add(loader);
		}
		loaded = true;
		Logger.getInstance().warning("All json loaders have finished.");
	}

	/**
	 * Gets a loader from the list of prepared loaders {@link #loaders}
	 * 
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T> T getLoader(Class<T> clazz) {
		for (Iterator<JsonLoader<?>> it$ = loaders.iterator(); it$.hasNext();) {
			JsonLoader<?> loader = it$.next();
			if (loader.getClass().getSimpleName().equalsIgnoreCase(clazz.getSimpleName())) {
				return (T) loader;
			}
		}
		return null;
	}

	/**
	 * @return the instance
	 */
	public static JsonManager getSingleton() {
		return INSTANCE;
	}
	
	/**
	 * If we have finished loading
	 */
	public static boolean loaded = false;

	/**
	 * The list of all json loaders
	 */
	private final List<JsonLoader<?>> loaders = new ArrayList<>();

	/**
	 * The instance of this class
	 */
	private static final JsonManager INSTANCE = new JsonManager();

}
