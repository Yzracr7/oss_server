package com.hyperion.utility.json.impl;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.google.gson.reflect.TypeToken;
import com.hyperion.game.Constants;
import com.hyperion.game.tickable.impl.PunishmentTick;
import com.hyperion.utility.json.JsonLoader;
import com.hyperion.utility.json.sub.Punishment;
import com.hyperion.utility.json.sub.Punishment.PunishmentType;

/**
 * @author Tyluur<itstyluur@gmail.com>
 * @since Mar 23, 2015
 */
public class PunishmentLoader extends JsonLoader<Punishment> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.utility.json.JsonLoader#initialize()
	 */
	@Override
	public void initialize() {
		getPunishments().clear();

		List<Punishment> punishments = generateList();
		for (Punishment punishment : punishments) {
			getPunishments().add(punishment);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.utility.json.JsonLoader#getFileLocation()
	 */
	@Override
	public String getFileLocation() {
		return Constants.FILES_PATH + "punishments.json";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hyperion.utility.json.JsonLoader#load()
	 */
	@Override
	protected List<Punishment> load() {
		List<Punishment> list = null;
		String dataRead = null;
		try {
			File file = getDataFile();
			FileReader reader = new FileReader(file);
			char[] chars = new char[(int) file.length()];
			reader.read(chars);
			dataRead = new String(chars);
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		list = json.fromJson(dataRead, new TypeToken<List<Punishment>>() {
		}.getType());
		return list;
	}

	/**
	 * Adds the punishment to the server
	 *
	 * @param key
	 *            The key of the punishment
	 * @param type
	 *            The type of punishmnt
	 * @param milliseconds
	 *            The time in milliseconds it will be applied for
	 */
	public void addPunishment(String key, PunishmentType type, long milliseconds) {
		synchronized (PunishmentTick.LOCK_OBJECT) {
			Punishment punishment = new Punishment(key, type, System.currentTimeMillis() + milliseconds);
			List<Punishment> punishments = generateList();
			punishments.add(punishment);
			punishment.onAdd();
			save(punishments);
			initialize();
		}
	}

	/**
	 * Forces the removal of a punishment
	 *
	 * @param key
	 *            The key of the punishment
	 * @param type
	 *            The type of punishment
	 */
	public boolean removePunishment(String key, PunishmentType type) {
		synchronized (PunishmentTick.LOCK_OBJECT) {
			List<Punishment> punishments = getPunishments();
			ListIterator<Punishment> it = punishments.listIterator();
			boolean found = false;
			while (it.hasNext()) {
				Punishment punishment = it.next();
				if (punishment.getType() == type) {
					if (punishment.getType() == PunishmentType.IPBAN) {
						String[] split = punishment.getKey().split(":");
						String name = split[0];
						if (name.equalsIgnoreCase(key)) {
							punishment.onRemove();
							it.remove();
							found = true;
						}
					} else if (key.replaceAll("_", " ").equalsIgnoreCase(punishment.getKey().replaceAll("_", " "))) {
						punishment.onRemove();
						it.remove();
						found = true;
					}
				}
			}
			if (found) {
				save(punishments);
				initialize();
			}
			return found;
		}
	}

	/**
	 * Finds out if the punishment exists
	 *
	 * @param key
	 *            The key to look for
	 * @param type
	 *            The type of punishment
	 * @return
	 */
	public boolean isPunished(String key, PunishmentType type) {
		synchronized (PunishmentTick.LOCK_OBJECT) {
			Iterator<Punishment> it = punishments.iterator();
			while (it.hasNext()) {
				Punishment punishment = it.next();
				if (punishment.getType() == type) {
					if (type == PunishmentType.IPBAN) {
						String[] split = punishment.getKey().split(":");
						if(key.contains(".")) { //We are checking IPban solely for IP
							String ip = split[1];
							if (ip.equalsIgnoreCase(key)) {
								return true;
							}
						} else {
							String name = split[0];
							if (name.equalsIgnoreCase(key)) {
								return true;
							}
							//Checking a player's name in IP ban list to deny that as well
						}
						
					} else if (key.replaceAll(" ", "_").equalsIgnoreCase(punishment.getKey().replaceAll(" ", "_"))) {
						return true;
					}
				}
			}
			return false;
		}
	}

	/**
	 * Gets the amount of time left for the punishmnete
	 * @param key The key
	 * @param type The type of punishment
	 * @return
	 */
	public long getPunishmentTime(String key, PunishmentType type) {
		long time = -1;
		if (!isPunished(key, type)) {
			return time;
		}
		Iterator<Punishment> it = punishments.iterator();
		while (it.hasNext()) {
			Punishment punishment = it.next();
			if (punishment.getType() == type) {
				time = punishment.getDuration();
			}
		}
		return time;
	}

	/**
	 * @return the punishments
	 */
	public List<Punishment> getPunishments() {
		return punishments;
	}

	/**
	 * The list of punishments
	 */
	private final List<Punishment> punishments = new ArrayList<>();
}
