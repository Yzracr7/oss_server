package com.hyperion.utility.logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.hyperion.game.Constants;
import com.hyperion.utility.DateCalculation;

public class LoggingThread implements Runnable {

	private static final String DIRECTORY = Constants.FILES_PATH + "logs/";
	private List<Log> logList = new ArrayList<Log>();
	private Thread thread = new Thread(this);
	private FileWriter fw;
	private final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	public LoggingThread() {
		thread.start();
		thread.setPriority(Thread.MAX_PRIORITY);
	}

	@Override
	public void run() {
		try {
			for (;;) {
				synchronized (logList) {
					if (logList.size() > 0) {
						Date date = new Date();
						Log log = logList.remove(0);
						fw = new FileWriter(getFile(log), true);
						try {
							fw.write(dateFormat.format(date) + " - " + log.getText() + "\r\n");
						} finally {
							fw.close();
						}
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void appendLog(Log log) {
		synchronized (logList) {
			logList.add(log);
		}
	}
/*
	private static List<Log> getList() {
		return logList;
	}
*/
	/**
	 * @return the directory
	 */
	private static String getDirectory() {
		StringBuilder directory = new StringBuilder(DIRECTORY);
		directory.append(DateCalculation.getMonthName() + "/");
		directory.append(DateCalculation.getNumberName(DateCalculation.getWeekNumber()) + "_Week/");
		return directory.toString();
	}

	private static File getFile(Log log) {
		File file = new File(getDirectory() + log.getUsername() + ".txt");
		if (!file.exists()) {
			try {
				file.getParentFile().mkdirs();
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return file;
	}

}
