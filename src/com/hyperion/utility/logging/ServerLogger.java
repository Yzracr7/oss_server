package com.hyperion.utility.logging;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Date;

/**
 *
 * @author Tyluur <itstyluur@gmail.com>
 * @since Dec 14, 2013
 */
public class ServerLogger extends PrintStream {

	public ServerLogger(OutputStream out) {
		super(out);
	}

	@Override
	public void print(boolean message) {
		Throwable throwable = new Throwable();
		String name = throwable.getStackTrace()[2].getFileName().replaceAll(".java", "");
		String line = String.valueOf(throwable.getStackTrace()[2].getLineNumber());
		log(name + ":" + line, "" + message);
	}

	@Override
	public void print(int message) {
		Throwable throwable = new Throwable();
		String name = throwable.getStackTrace()[2].getFileName().replaceAll(".java", "");
		String line = String.valueOf(throwable.getStackTrace()[2].getLineNumber());
		log(name + ":" + line, "" + message);
	}

	@Override
	public void print(String message) {
		Throwable throwable = new Throwable();
		String name = throwable.getStackTrace()[2].getFileName().replaceAll(".java", "");
		String line = String.valueOf(throwable.getStackTrace()[2].getLineNumber());
		log(name + ":" + line, "" + message);
	}

	/**
	 * Printing out the text
	 * 
	 * @param className
	 *            The class name
	 * @param text
	 *            The text
	 */
	private void log(String className, String text) {
		super.print("[" + className + "][" + getDate() + "]" + text);
	}

	/**
	 * Gets the date in a formatted string.
	 * 
	 * @return The date
	 */
	private String getDate() {
		String[] split = new Date().toString().split(" ");
		String result = "";
		for (int index = 0; index < split.length; index++) {
			String s = split[index];
			if (index >= 4) {
				break;
			}
			result += s + " ";
		}
		return result.trim();
	}

}
